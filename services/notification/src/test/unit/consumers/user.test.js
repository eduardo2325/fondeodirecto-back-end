const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const consumer = require('../../../api/consumers/user');
const helperFixtures = require('../fixtures/consumers/user');
const fixturesNotification = require('../../functional/fixtures/notification');
const log = require('/var/lib/core/js/log').prototype;

const mailer = require('../../../lib/mailer');

const td = require('testdouble');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/User consumer', () => {
  let messageCallback;
  let errorCallback;
  let sendCallback;

  beforeEach(() => {
    messageCallback = td.func('log.message');
    errorCallback = td.func('log.error');
    sendCallback = td.func('mailer.send');

    td.replace(log, 'message', messageCallback);
    td.replace(log, 'error', errorCallback);
    td.replace(mailer, 'send', sendCallback);
  });

  afterEach(() => {
    td.reset();
    sandbox.restore();
  });

  // test once and avoid duplicating code/tests
  describe('_send/All', () => {
    const mailSendError = new Error('Mail send failed');

    const singleParams = {
      partial: 'example-tpl',
      body: { foo: 'bar' },
      title: 'Untitled',
      guid: 'x-123'
    };

    it('should reject if mailer.send() fails', () => {
      td.when(mailer.send(
        td.matchers.isA(String), // partial
        td.matchers.isA(Object), // body
        td.matchers.isA(String), // recipient
        td.matchers.isA(String), // title
        td.matchers.isA(String))) // guid
        .thenReject(mailSendError);

      return consumer._send({}, {
        ...singleParams,
        body: {
          ...singleParams.body,
          email: 'foo@candy.bar'
        }
      })
        .should.be.rejected
        .then(err => {
          err.should.be.equal(mailSendError);
        });
    });

    it('should work with multiple recipients', () => {
      td.replace(consumer, '_send', td.func('consumer._send'));
      td.when(consumer._send(
        td.matchers.isA(Object), // event
        td.matchers.isA(Object) // params
      )).thenResolve();

      return consumer._sendAll({}, {
        ...singleParams,
        body: {
          ...singleParams.body,
          recipients: [
            [ 'Foo bar', 'foo@candy.bar' ]
          ]
        }
      })
        .should.be.fulfilled;
    });
  });

  describe('Dummy event', () => {
    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
    });

    it('should show dummy event log message', () => {
      const validDummyEvent = fixturesNotification.validDummyEvent();

      return consumer
        .dummyEvent(validDummyEvent)
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0][1].should.be.eql(validDummyEvent);
        });
    });

    it('should consume dummy event', () => {
      const validDummyEvent = fixturesNotification.validDummyEvent();

      sandbox.stub(consumer, 'dummyEvent').callsFake(() => Promise.resolve(validDummyEvent));
      return consumer.dummyEvent(validDummyEvent)
        .then(() => {
          consumer.dummyEvent.calledOnce.should.be.true;
          consumer.dummyEvent.args[0][0].message.should.be.eql(validDummyEvent.message);
        });
    });
  });

  describe('invitationCreated', () => {
    const fixtures = helperFixtures.newUserInvitation;
    const { invitationEvent, mailSendParams, investorMailSendParams, mailSendError, logMessageParams, logErrorParams,
      eventWrongRole, logWrongRoleMessageParams, mailWrongRoleSendParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invitationCreated(invitationEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send default mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invitationCreated(eventWrongRole)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logWrongRoleMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailWrongRoleSendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send company mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invitationCreated(invitationEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send investor mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      invitationEvent.value.body.company_role = 'INVESTOR';

      return consumer.invitationCreated(invitationEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(investorMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('resendInvitation', () => {
    const fixtures = helperFixtures.newUserInvitation;
    const { invitationEvent, investorMailSendParams, mailSendError, logMessageParams, logErrorParams,
      eventWrongRole, logWrongRoleMessageParams, mailWrongRoleSendParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.resendInvitation(invitationEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(investorMailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send default mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.resendInvitation(eventWrongRole)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logWrongRoleMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailWrongRoleSendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.resendInvitation(invitationEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(investorMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('investorInvitationCreated', () => {
    const fixtures = helperFixtures.investorInvitationCreated;
    const { logMessageParams, mailSendParams, mailSendError, logErrorParams,
      investorInvitationCreatedEvent } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.investorInvitationCreated(investorInvitationCreatedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.investorInvitationCreated(investorInvitationCreatedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('passwordChanged', () => {
    const fixtures = helperFixtures.passwordChanged;
    const { passwordChangedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.passwordChanged(passwordChangedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.passwordChanged(passwordChangedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('recoverPassword', () => {
    const fixtures = helperFixtures.recoverPassword;
    const { recoverPasswordEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.recoverPassword(recoverPasswordEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.recoverPassword(recoverPasswordEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceCreated', () => {
    const fixtures = helperFixtures.invoiceCreated;
    const { invoiceCreatedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceCreated(invoiceCreatedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceCreated(invoiceCreatedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceApproved', () => {
    const fixtures = helperFixtures.invoiceApproved;
    const { invoiceApprovedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceApproved(invoiceApprovedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceApproved(invoiceApprovedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });


  describe('invoiceFundRequest', () => {
    const fixtures = helperFixtures.invoiceFundRequest;
    const { invoiceFundRequestEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceFundRequest(invoiceFundRequestEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceFundRequest(invoiceFundRequestEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceRejected', () => {
    const fixtures = helperFixtures.invoiceRejected;
    const { invoiceRejectedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceRejected(invoiceRejectedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceRejected(invoiceRejectedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('fundRequestedInvoiceRejected', () => {
    const fixtures = helperFixtures.fundRequestedInvoiceRejected;
    const { fundRequestedInvoiceRejectedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.fundRequestedInvoiceRejected(fundRequestedInvoiceRejectedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.fundRequestedInvoiceRejected(fundRequestedInvoiceRejectedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('fundRequestedInvoiceApproved', () => {
    const fixtures = helperFixtures.fundRequestedInvoiceApproved;
    const { fundRequestedInvoiceApprovedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.fundRequestedInvoiceApproved(fundRequestedInvoiceApprovedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams[0]);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.fundRequestedInvoiceApproved(fundRequestedInvoiceApprovedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams[0]);
          mailer.send.args[1].should.be.eql(mailSendParams[1]);
          log.error.called.should.be.false;
        });
    });
  });

  describe('publishedInvoiceRejected', () => {
    const fixtures = helperFixtures.publishedInvoiceRejected;
    const { publishedInvoiceRejectedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.publishedInvoiceRejected(publishedInvoiceRejectedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.publishedInvoiceRejected(publishedInvoiceRejectedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('withdrawCreated', () => {
    const fixtures = helperFixtures.withdrawCreated;
    const { withdrawCreatedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.withdrawCreated(withdrawCreatedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.withdrawCreated(withdrawCreatedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('depositCreated', () => {
    const fixtures = helperFixtures.depositCreated;
    const { depositCreatedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.depositCreated(depositCreatedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.depositCreated(depositCreatedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('pendingTransactionApproved', () => {
    const fixtures = helperFixtures.pendingTransactionApproved;
    const { pendingDepositTransactionApprovedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams, pendingWithdrawTransactionApprovedEvent, withdrawMailSendParams,
      withdrawLogMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.pendingTransactionApproved(pendingDepositTransactionApprovedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send deposit mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.pendingTransactionApproved(pendingDepositTransactionApprovedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send withdraw mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.pendingTransactionApproved(pendingWithdrawTransactionApprovedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(withdrawLogMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(withdrawMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('pendingTransactionRejected', () => {
    const fixtures = helperFixtures.pendingTransactionRejected;
    const { pendingDepositTransactionRejectedEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams, pendingWithdrawTransactionRejectedEvent, withdrawMailSendParams,
      withdrawLogMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.pendingTransactionRejected(pendingDepositTransactionRejectedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send deposit mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.pendingTransactionRejected(pendingDepositTransactionRejectedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send withdraw mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.pendingTransactionRejected(pendingWithdrawTransactionRejectedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(withdrawLogMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(withdrawMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('clientInvoicePaymentCreated', () => {
    const fixtures = helperFixtures.clientInvoicePaymentCreated;
    const { clientInvoicePaymentCreatedEvent, mailSendParams, mailSendError,
      logMessageParams, logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.clientInvoicePaymentCreated(clientInvoicePaymentCreatedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.clientInvoicePaymentCreated(clientInvoicePaymentCreatedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceExpired', () => {
    const fixtures = helperFixtures.invoiceExpired;
    const { invoiceExpiredEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceExpired(invoiceExpiredEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceExpired(invoiceExpiredEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('companyRoleSuspensionUpdated', () => {
    const fixtures = helperFixtures.companyRoleSuspensionUpdated;
    const { cxcSuspendedEvent, cxcUnsuspendedEvent, cxpSuspendedEvent, cxpUnsuspendedEvent,
      mailSendError, logErrorParams, mailCXCSuspendParams, mailCXCUnsuspendParams,
      mailCXPSuspendParams, mailCXPUnsuspendParams, guid } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.companyRoleSuspensionUpdated(cxcSuspendedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', cxcSuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailCXCSuspendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send cxc suspended mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.companyRoleSuspensionUpdated(cxcSuspendedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', cxcSuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailCXCSuspendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send cxc unsuspended mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.companyRoleSuspensionUpdated(cxcUnsuspendedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', cxcUnsuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailCXCUnsuspendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send cxp suspended mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.companyRoleSuspensionUpdated(cxpSuspendedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', cxpSuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailCXPSuspendParams);
          log.error.called.should.be.false;
        });
    });

    it('should send cxp unsuspended mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.companyRoleSuspensionUpdated(cxpUnsuspendedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', cxpUnsuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailCXPUnsuspendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('investorRoleSuspensionUpdated', () => {
    const fixtures = helperFixtures.investorRoleSuspensionUpdated;
    const { investorSuspendedEvent, investorUnsuspendedEvent, mailSendError, logErrorParams,
      mailInvestorSuspendedParams, mailInvestorUnsuspendedParams, guid } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.investorRoleSuspensionUpdated(investorSuspendedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', investorSuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailInvestorSuspendedParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send cxp suspended mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.investorRoleSuspensionUpdated(investorSuspendedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', investorSuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailInvestorSuspendedParams);
          log.error.called.should.be.false;
        });
    });

    it('should send cxp unsuspended mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.investorRoleSuspensionUpdated(investorUnsuspendedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith('Consuming user event', investorUnsuspendedEvent, 'Event', guid);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailInvestorUnsuspendedParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoicePaymentDue', () => {
    const fixtures = helperFixtures.invoicePaymentDue;
    const { invoicePaymentDueEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoicePaymentDue(invoicePaymentDueEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoicePaymentDue(invoicePaymentDueEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceCompleted', () => {
    const fixtures = helperFixtures.invoiceCompleted;
    const {
      invoiceCompletedEvent,
      cxpMailSendParams,
      cxcMailSendParams,
      investorMailSendParams,
      mailSendError,
      logMessageParams,
      logErrorParams
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceCompleted(invoiceCompletedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledThrice.should.be.true;
          mailer.send.args[0].should.be.eql(cxpMailSendParams);
          mailer.send.args[1].should.be.eql(cxcMailSendParams);
          mailer.send.args[2].should.be.eql(investorMailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceCompleted(invoiceCompletedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledThrice.should.be.true;
          mailer.send.args[0].should.be.eql(cxpMailSendParams);
          mailer.send.args[1].should.be.eql(cxcMailSendParams);
          mailer.send.args[2].should.be.eql(investorMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceLost', () => {
    const fixtures = helperFixtures.invoiceLost;
    const { invoiceLostEvent, cxcMailSendParams, investorMailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceLost(invoiceLostEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(cxcMailSendParams);
          mailer.send.args[1].should.be.eql(investorMailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceLost(invoiceLostEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(cxcMailSendParams);
          mailer.send.args[1].should.be.eql(investorMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoiceLatePayment', () => {
    const fixtures = helperFixtures.invoiceLatePayment;
    const { invoiceLatePaymentEvent, cxcMailSendParams, investorMailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoiceLatePayment(invoiceLatePaymentEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(cxcMailSendParams);
          mailer.send.args[1].should.be.eql(investorMailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoiceLatePayment(invoiceLatePaymentEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(cxcMailSendParams);
          mailer.send.args[1].should.be.eql(investorMailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('companyProposed', () => {
    const fixtures = helperFixtures.companyProposed;
    const { companyProposedEvent, mailSendParams, mailSendError,
      logMessageParams, logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.companyProposed(companyProposedEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.companyProposed(companyProposedEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('agreementAccepted', () => {
    const fixtures = helperFixtures.agreementAccepted;
    const { guid, title, agreementAcceptedBody, agreementAcceptedEvent } = fixtures;

    it('should work as expected', () => {
      td.when(mailer.send('contract-agreement', agreementAcceptedBody, agreementAcceptedBody.email, title, guid))
        .thenResolve();

      return consumer.agreementAccepted(agreementAcceptedEvent)
        .should.be.fulfilled
        .then(() => {
          sendCallback = td.explain(sendCallback);
          sendCallback.callCount.should.be.equal(1);
        });
    });
  });

  // most notifications are generic, so they _can_ be resolved within a loop (this is also true for fixtures)

  const NOTIFY_SPECS = [
    { partial: 'notify-fund-request', event: 'notifyFundRequest' },
    { partial: 'notify-invoice-approved', event: 'notifyInvoiceApproved' },
    { partial: 'notify-invoice-published', event: 'notifyInvoicePublished' },
    { partial: 'notify-fund-approved', event: 'notifyFundApproved' }
  ];

  NOTIFY_SPECS.forEach(test => {
    describe(test.event, () => {
      const { guid, title, body, event } = helperFixtures[test.event];

      it('should work as expected', () => {
        td.when(mailer.send(test.partial, body, body.email, title, guid))
          .thenResolve();

        return consumer[test.event](event)
          .should.be.fulfilled
          .then(() => {
            sendCallback = td.explain(sendCallback);
            sendCallback.callCount.should.be.equal(1);
          });
      });
    });
  });

  describe('notifyNewInvoices', () => {
    const fixtures = helperFixtures.notifyNewInvoices;
    const { notifyNewInvoicesEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.notifyNewInvoices(notifyNewInvoicesEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.notifyNewInvoices(notifyNewInvoicesEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });

  describe('invoicePaymentNotification', () => {
    const fixtures = helperFixtures.invoicePaymentNotification;
    const { invoicePaymentNotificationEvent, mailSendParams, mailSendError, logMessageParams,
      logErrorParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should reject if mailer fails', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(mailSendError));

      return consumer.invoicePaymentNotification(invoicePaymentNotificationEvent)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.calledOnce.should.be.true;
          log.error.args[0].should.be.eql(logErrorParams);
          err.should.be.equal(mailSendError);
        });
    });

    it('should send mail', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return consumer.invoicePaymentNotification(invoicePaymentNotificationEvent)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailSendParams);
          log.error.called.should.be.false;
        });
    });
  });
});
