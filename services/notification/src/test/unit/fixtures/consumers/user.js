const invitation = {
  token: 'token',
  email: 'test@user.com',
  username: 'test user',
  company_role: 'COMPANY'
};
const user = {
  id: 1,
  username: 'User Name',
  email: 'user@email.com'
};

const investorUser = {
  name: 'User Name',
  email: 'user@email.com',
  token: 'token'
};

const investorUserBody = {
  username: 'User Name',
  email: 'user@email.com',
  token: 'token'
};

const company = {
  id: 1,
  name: 'A company',
  rfc: 'rfc'
};
const invoice = {
  id: 1,
  number: 'ABC123'
};
const guid = 'testguid';
const invitationEvent = {
  value: {
    type: 'InvitationCreated',
    body: invitation,
    guid
  }
};
const reason = 'A problem';
const pendingDepositTransaction = {
  company_id: company.id,
  type: 'DEPOSIT',
  id: 1
};
const pendingWithdrawTransaction = {
  company_id: company.id,
  type: 'WITHDRAW',
  id: 1
};
const passwordChangedEvent = {
  value: {
    type: 'PasswordChanged',
    body: user,
    guid
  }
};
const recoverPasswordEvent = {
  value: {
    type: 'RecoverPassword',
    body: user,
    guid
  }
};
const invoiceCreatedEvent = {
  value: {
    type: 'InvoiceApproved',
    body: {
      company_name: company.name,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      emails: user.email
    },
    guid
  }
};
const invoiceApprovedEvent = {
  value: {
    type: 'InvoiceApproved',
    body: {
      client_name: company.name,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      emails: user.email
    },
    guid
  }
};
const invoicePaymentNotificationEvent = {
  value: {
    type: 'InvoicePaymentNotification',
    body: {
      client_name: company.name,
      invoice_id: invoice.id,
      emails: user.email
    },
    guid
  }
};
const invoiceFundRequestEvent = {
  value: {
    type: 'InvoiceFundRequest',
    body: {
      invoices: [
        { id: 1, operation_id: 10 }
      ],
      investor_name: company.name,
      emails: user.email
    },
    guid
  }
};
const investorInvitationCreatedEvent = {
  value: {
    type: 'InvestorInvitationCreated',
    body: {
      emails: [ 'investor-test@fondeodirecto.com' ].join(),
      name: 'John Doe',
      token: '1234567'
    },
    guid
  }
};
const invoiceRejectedEvent = {
  value: {
    type: 'InvoiceRejected',
    body: {
      client_name: company.name,
      invoice_number: invoice.number,
      reason: 'Valid reason',
      emails: user.email
    },
    guid
  }
};
const publishedInvoiceRejectedEvent = {
  value: {
    type: 'PublishedInvoiceRejected',
    body: {
      invoice_number: invoice.number,
      reason: 'Valid reason',
      emails: user.email
    },
    guid
  }
};
const fundRequestedInvoiceRejectedEvent = {
  value: {
    type: 'FundRequestedInvoiceRejected',
    body: {
      invoice_number: invoice.number,
      reason: 'Valid reason',
      emails: user.email
    },
    guid
  }
};
const fundRequestedInvoiceApprovedEvent = {
  value: {
    type: 'FundRequestedInvoiceApproved',
    body: {
      invoice_number: invoice.number,
      invoice_expiration: new Date().toString(),
      cxc_emails: user.email,
      cxp_emails: user.email,
      investor_emails: user.email
    },
    guid
  }
};
const withdrawCreatedEvent = {
  value: {
    type: 'WithdrawCreated',
    body: {
      emails: user.email,
      company_name: company.name,
      company_id: company.id,
      amount: 100
    },
    guid
  }
};
const depositCreatedEvent = {
  value: {
    type: 'DepositCreated',
    body: {
      emails: user.email,
      company_name: company.name,
      company_id: company.id,
      amount: 100
    },
    guid
  }
};
const pendingDepositTransactionApprovedEvent = {
  value: {
    type: 'PendingTransactionApproved',
    body: {
      emails: user.email,
      type: pendingDepositTransaction.type,
      amount: '100.00'
    },
    guid
  }
};
const pendingWithdrawTransactionApprovedEvent = {
  value: {
    type: 'PendingTransactionApproved',
    body: {
      emails: user.email,
      type: pendingWithdrawTransaction.type,
      amount: '100.00'
    },
    guid
  }
};
const pendingDepositTransactionRejectedEvent = {
  value: {
    type: 'PendingTransactionRejected',
    body: {
      emails: user.email,
      type: pendingDepositTransaction.type,
      amount: '100.00',
      reason
    },
    guid
  }
};
const pendingWithdrawTransactionRejectedEvent = {
  value: {
    type: 'PendingTransactionRejected',
    body: {
      emails: user.email,
      type: pendingWithdrawTransaction.type,
      amount: '100.00',
      reason
    },
    guid
  }
};
const clientInvoicePaymentCreatedEvent = {
  value: {
    type: 'ClientInvoicePaymentCreated',
    body: {
      emails: user.email,
      client_name: company.name,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      amount: 100
    },
    guid
  }
};
const agreementAcceptedEvent = {
  value: {
    type: 'AgreementAccepted',
    body: {
      id: 1,
      role: 'CXC',
      rfc: 'XMPL880410260',
      path: 'SOME_S3_URL',
      recipients: [
        [ 'someone', 'foo@candy.bar' ]
      ],
      agreed_at: {
        day: 1,
        month: 'may',
        time: '16:20'
      },
      username: 'someone',
      email: 'foo@candy.bar',
      contract: 'Contrato de prueba'
    },
    guid
  }
};
const companyProposedEvent = {
  value: {
    type: 'CompanyProposed',
    body: {
      emails: user.email,
      client_name: company.name,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      amount: 100
    },
    guid
  }
};
const invoiceExpiredEvent = {
  value: {
    type: 'InvoiceExpired',
    body: {
      client_name: company.name,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      emails: user.email
    },
    guid
  }
};
const cxcSuspendedEvent = {
  value: {
    type: 'CompanyRoleSuspensionUpdated',
    body: {
      role: 'CXC',
      suspended: true,
      emails: user.email
    },
    guid
  }
};
const invoicePaymentDueEvent = {
  value: {
    type: 'InvoicePaymentDue',
    body: {
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      emails: user.email
    },
    guid
  }
};
const invoiceCompletedEvent = {
  value: {
    type: 'invoiceCompleted',
    body: {
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxc_emails: user.email,
      cxp_emails: user.email,
      investor_emails: user.email
    },
    guid
  }
};
const invoiceLostEvent = {
  value: {
    type: 'InvoiceLost',
    body: {
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxc_emails: user.email,
      investor_emails: user.email
    },
    guid
  }
};
const invoiceLatePaymentEvent = {
  value: {
    type: 'InvoiceLatePayment',
    body: {
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxc_emails: user.email,
      investor_emails: user.email
    },
    guid
  }
};
const cxcUnsuspendedEvent = {
  value: {
    type: 'CompanyRoleSuspensionUpdated',
    body: {
      role: 'CXC',
      suspended: false,
      emails: user.email
    },
    guid
  }
};
const cxpSuspendedEvent = {
  value: {
    type: 'CompanyRoleSuspensionUpdated',
    body: {
      role: 'CXP',
      suspended: true,
      emails: user.email
    },
    guid
  }
};
const cxpUnsuspendedEvent = {
  value: {
    type: 'CompanyRoleSuspensionUpdated',
    body: {
      role: 'CXP',
      suspended: false,
      emails: user.email
    },
    guid
  }
};
const investorSuspendedEvent = {
  value: {
    type: 'InvestorRoleSuspensionUpdated',
    body: {
      suspended: true,
      emails: user.email
    },
    guid
  }
};
const investorUnsuspendedEvent = {
  value: {
    type: 'InvestorRoleSuspensionUpdated',
    body: {
      suspended: false,
      emails: user.email
    },
    guid
  }
};

const notifyNewInvoicesEvent = {
  value: {
    type: 'NotifyNewInvoices',
    users: {
      investors: [
        investorUser
      ]
    },
    guid
  }
};

const mailSendError = new Error('Mail send failed');
const invitationWrongRole = {
  token: 'token',
  email: 'test@user.com',
  username: 'test user',
  company_role: 'UNKNOWN'
};
const eventWrongRole = {
  value: {
    type: 'InvitationCreated',
    body: invitationWrongRole,
    guid
  }
};

function notifyEvent(type) {
  const body = {
    recipients: [
      [ 'someone', 'foo@candy.bar' ]
    ],
    username: 'someone',
    email: 'foo@candy.bar'
  };

  return {
    title: 'Test',
    event: {
      value: {
        type,
        body,
        guid
      }
    },
    mailSendError,
    guid,
    body
  };
}

const notifyInvoiceApprovedBody = {
  recipients: [
    [ 'someone', 'foo@candy.bar' ]
  ],
  username: 'someone',
  email: 'foo@candy.bar',
  invoices: [ invoice ]
};

const notifyInvoiceApprovedEvent = {
  title: 'Test',
  event: {
    value: {
      title: 'NotifyInvoiceApproved',
      body: notifyInvoiceApprovedBody
    },
    guid
  },
  mailSendError,
  guid,
  body: notifyInvoiceApprovedBody
};

module.exports = {
  newUserInvitation: {
    invitationEvent,
    eventWrongRole,
    mailSendParams: [
      'invitation',
      invitation,
      invitation.email,
      'Bienvenido a Fondeo Directo',
      guid
    ],
    mailWrongRoleSendParams: [
      'invitation',
      invitationWrongRole,
      invitation.email,
      'Bienvenido a Fondeo Directo',
      guid
    ],
    investorMailSendParams: [
      'investor-invitation',
      invitation,
      invitation.email,
      'Bienvenido a Fondeo Directo',
      guid
    ],
    mailSendError,
    logMessageParams: [
      'Consuming user event',
      invitationEvent,
      'Event',
      guid
    ],
    logWrongRoleMessageParams: [
      'Consuming user event',
      eventWrongRole,
      'Event',
      guid
    ],
    logErrorParams: [
      'Error sending invitation',
      guid,
      mailSendError
    ]
  },
  passwordChanged: {
    passwordChangedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending changed password email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'password-changed',
      user,
      user.email,
      'Tu contraseña ha sido actualizada',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      passwordChangedEvent,
      'Event',
      guid
    ]
  },
  recoverPassword: {
    recoverPasswordEvent,
    mailSendError,
    logErrorParams: [
      'Error sending password token email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'recover-password',
      user,
      user.email,
      'Recuperar contraseña',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      recoverPasswordEvent,
      'Event',
      guid
    ]
  },
  invoiceCreated: {
    invoiceCreatedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice created email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-created',
      invoiceCreatedEvent.value.body,
      user.email,
      `Acción requerida: ${company.name} ha subido una nueva factura a Fondeo Directo`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceCreatedEvent,
      'Event',
      guid
    ]
  },
  invoiceApproved: {
    invoiceApprovedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice approved email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-approved',
      invoiceApprovedEvent.value.body,
      user.email,
      `¡Enhorabuena!  ${company.name} ha aprobado tu factura ${invoice.number}, ¡A buscar Fondeo!`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceApprovedEvent,
      'Event',
      guid
    ]
  },
  invoiceFundRequest: {
    invoiceFundRequestEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice fund request email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-fund-request',
      invoiceFundRequestEvent.value.body,
      user.email,
      `Acción requerida:  ${invoiceFundRequestEvent.value.body.investor_name} ha solicitado Fondear una factura`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceFundRequestEvent,
      'Event',
      guid
    ]
  },
  investorInvitationCreated: {
    investorInvitationCreatedEvent,
    mailSendError,
    logMessageParams: [
      'Consuming user event',
      investorInvitationCreatedEvent,
      'Event',
      guid
    ],
    mailSendParams: [
      'investor-invitation-full',
      {
        emails: [ 'investor-test@fondeodirecto.com' ].join(),
        name: 'John Doe',
        token: '1234567'
      },
      [ 'investor-test@fondeodirecto.com' ].join(),
      'Bienvenido a Fondeo Directo',
      guid
    ],
    logErrorParams: [
      'Error sending investor invitation email',
      guid,
      mailSendError
    ]
  },
  invoiceRejected: {
    invoiceRejectedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice rejected email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-rejected',
      invoiceRejectedEvent.value.body,
      user.email,
      `¡Atención! ${company.name} ha rechazado tu factura ${invoice.number}`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceRejectedEvent,
      'Event',
      guid
    ]
  },
  publishedInvoiceRejected: {
    publishedInvoiceRejectedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending published invoice rejected email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'published-invoice-rejected',
      publishedInvoiceRejectedEvent.value.body,
      user.email,
      `¡Atención! Tu factura ${invoice.number} ha sido removida del Market`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      publishedInvoiceRejectedEvent,
      'Event',
      guid
    ]
  },
  fundRequestedInvoiceRejected: {
    fundRequestedInvoiceRejectedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending fund requested invoice rejected email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'fund-requested-invoice-rejected',
      fundRequestedInvoiceRejectedEvent.value.body,
      user.email,
      '¡Atención! Tu fondeo fue cancelado',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      fundRequestedInvoiceRejectedEvent,
      'Event',
      guid
    ]
  },
  fundRequestedInvoiceApproved: {
    fundRequestedInvoiceApprovedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending fund requested invoice approved email',
      guid,
      mailSendError
    ],
    mailSendParams: [ [
      'cxc-fund-requested-invoice-approved',
      fundRequestedInvoiceApprovedEvent.value.body,
      user.email,
      `¡Buenas noticias! Tu factura ${invoice.number} ha sido fondeada`,
      guid
    ], [
      'investor-fund-requested-invoice-approved',
      fundRequestedInvoiceApprovedEvent.value.body,
      user.email,
      'Tu fondeo fue procesado',
      guid
    ] ],
    logMessageParams: [
      'Consuming user event',
      fundRequestedInvoiceApprovedEvent,
      'Event',
      guid
    ]
  },
  withdrawCreated: {
    withdrawCreatedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending withdraw created email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'withdraw-created',
      withdrawCreatedEvent.value.body,
      user.email,
      `Acción requerida: ${company.name} ha solicitado un retiro a Fondeo Directo`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      withdrawCreatedEvent,
      'Event',
      guid
    ]
  },
  depositCreated: {
    depositCreatedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending deposit created email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'deposit-created',
      depositCreatedEvent.value.body,
      user.email,
      `Acción requerida: ${company.name} ha realizado un depósito a Fondeo Directo`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      depositCreatedEvent,
      'Event',
      guid
    ]
  },
  pendingTransactionApproved: {
    pendingDepositTransactionApprovedEvent,
    pendingWithdrawTransactionApprovedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending approved pending transaction email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'deposit-approved',
      pendingDepositTransactionApprovedEvent.value.body,
      user.email,
      'Tu depósito ha sido procesado',
      guid
    ],
    withdrawMailSendParams: [
      'withdraw-approved',
      pendingWithdrawTransactionApprovedEvent.value.body,
      user.email,
      'Tu retiro de fondos ha sido procesado',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      pendingDepositTransactionApprovedEvent,
      'Event',
      guid
    ],
    withdrawLogMessageParams: [
      'Consuming user event',
      pendingWithdrawTransactionApprovedEvent,
      'Event',
      guid
    ]
  },
  pendingTransactionRejected: {
    pendingDepositTransactionRejectedEvent,
    pendingWithdrawTransactionRejectedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending rejected pending transaction email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'deposit-rejected',
      pendingDepositTransactionRejectedEvent.value.body,
      user.email,
      '¡Atención!  Tu depósito no pudo ser procesado',
      guid
    ],
    withdrawMailSendParams: [
      'withdraw-rejected',
      pendingWithdrawTransactionRejectedEvent.value.body,
      user.email,
      '¡Atención!  Tu retiro no pudo ser procesado',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      pendingDepositTransactionRejectedEvent,
      'Event',
      guid
    ],
    withdrawLogMessageParams: [
      'Consuming user event',
      pendingWithdrawTransactionRejectedEvent,
      'Event',
      guid
    ]
  },
  clientInvoicePaymentCreated: {
    clientInvoicePaymentCreatedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending client invoice payment created email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'client-invoice-payment-created',
      clientInvoicePaymentCreatedEvent.value.body,
      user.email,
      `Acción requerida: ${company.name} ha notificado el pago de una factura`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      clientInvoicePaymentCreatedEvent,
      'Event',
      guid
    ]
  },
  companyProposed: {
    companyProposedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending company proposed email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'company-proposed',
      companyProposedEvent.value.body,
      user.email,
      'Nueva propuesta recibida',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      companyProposedEvent,
      'Event',
      guid
    ]
  },
  invoiceExpired: {
    invoiceExpiredEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice expired email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-expired',
      invoiceExpiredEvent.value.body,
      user.email,
      `¡Atención! Tu factura ${invoice.number} ha expirado`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceExpiredEvent,
      'Event',
      guid
    ]
  },
  agreementAccepted: {
    guid,
    title: '¡Muchas gracias! Has aceptado el Contrato de prueba',
    mailSendError,
    agreementAcceptedBody: agreementAcceptedEvent.value.body,
    agreementAcceptedEvent
  },
  notifyFundRequest: notifyEvent('notifyFundRequest'),
  notifyInvoiceApproved: notifyInvoiceApprovedEvent,
  notifyInvoicePublished: notifyEvent('notifyInvoicePublished'),
  notifyFundApproved: notifyEvent('notifyFundApproved'),
  companyRoleSuspensionUpdated: {
    cxcSuspendedEvent,
    cxcUnsuspendedEvent,
    cxpSuspendedEvent,
    cxpUnsuspendedEvent,
    mailSendError,
    guid,
    logErrorParams: [
      'Error sending company role suspension update email',
      guid,
      mailSendError
    ],
    mailCXCSuspendParams: [
      'cxc-suspended',
      cxcSuspendedEvent.value.body,
      user.email,
      '¡Atención! Funcionalidades limitadas',
      guid
    ],
    mailCXCUnsuspendParams: [
      'cxc-unsuspended',
      cxcUnsuspendedEvent.value.body,
      user.email,
      '¡Atención! Funcionalidades restauradas',
      guid
    ],
    mailCXPSuspendParams: [
      'cxp-suspended',
      cxpSuspendedEvent.value.body,
      user.email,
      '¡Atención! Funcionalidades limitadas',
      guid
    ],
    mailCXPUnsuspendParams: [
      'cxp-unsuspended',
      cxpUnsuspendedEvent.value.body,
      user.email,
      '¡Atención! Funcionalidades restauradas',
      guid
    ]
  },
  investorRoleSuspensionUpdated: {
    investorSuspendedEvent,
    investorUnsuspendedEvent,
    mailSendError,
    guid,
    logErrorParams: [
      'Error sending investor role suspension update email',
      guid,
      mailSendError
    ],
    mailInvestorSuspendedParams: [
      'investor-suspended',
      investorSuspendedEvent.value.body,
      user.email,
      '¡Atención! Funcionalidades limitadas',
      guid
    ],
    mailInvestorUnsuspendedParams: [
      'investor-unsuspended',
      investorUnsuspendedEvent.value.body,
      user.email,
      '¡Atención! Funcionalidades restauradas',
      guid
    ]
  },
  invoicePaymentDue: {
    invoicePaymentDueEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice payment due email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-payment-due',
      invoicePaymentDueEvent.value.body,
      user.email,
      `¡Atención! El pago de la factura ${invoice.number} ha vencido`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoicePaymentDueEvent,
      'Event',
      guid
    ]
  },
  invoiceCompleted: {
    invoiceCompletedEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice completed email',
      guid,
      mailSendError
    ],
    cxpMailSendParams: [
      'cxp-invoice-completed',
      invoiceCompletedEvent.value.body,
      user.email,
      `El pago de la factura ${invoice.number} ha sido confirmado`,
      guid
    ],
    cxcMailSendParams: [
      'cxc-invoice-completed',
      invoiceCompletedEvent.value.body,
      user.email,
      `¡Buenas noticias! Tu factura ${invoice.number} ha sido pagada`,
      guid
    ],
    investorMailSendParams: [
      'investor-invoice-completed',
      invoiceCompletedEvent.value.body,
      user.email,
      '¡Buenas noticias! Tu fondeo se ha completado',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceCompletedEvent,
      'Event',
      guid
    ]
  },
  invoiceLost: {
    invoiceLostEvent,
    mailSendError,
    logErrorParams: [
      'Error sending lost invoice email',
      guid,
      mailSendError
    ],
    cxcMailSendParams: [
      'cxc-lost-invoice',
      invoiceLostEvent.value.body,
      user.email,
      `¡Buenas noticias! Tu factura ${invoice.number} ha sido pagada`,
      guid
    ],
    investorMailSendParams: [
      'investor-lost-invoice',
      invoiceLostEvent.value.body,
      user.email,
      `¡Buenas noticias! Tu factura ${invoice.number} ha sido pagada`,
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceLostEvent,
      'Event',
      guid
    ]
  },
  invoiceLatePayment: {
    invoiceLatePaymentEvent,
    mailSendError,
    logErrorParams: [
      'Error sending late payment invoice email',
      guid,
      mailSendError
    ],
    cxcMailSendParams: [
      'cxc-late-payment-invoice',
      invoiceLatePaymentEvent.value.body,
      user.email,
      `¡Buenas noticias! Tu factura ${invoice.number} ha sido pagada`,
      guid
    ],
    investorMailSendParams: [
      'investor-late-payment-invoice',
      invoiceLatePaymentEvent.value.body,
      user.email,
      '¡Buenas noticias! Tu Fondeo se ha completado',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoiceLatePaymentEvent,
      'Event',
      guid
    ]
  },
  notifyNewInvoices: {
    notifyNewInvoicesEvent,
    mailSendError,
    logErrorParams: [
      'Error sending new invoices email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'new-invoices',
      investorUserBody,
      investorUserBody.email,
      'Tenemos nuevas facturas para ti',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      notifyNewInvoicesEvent,
      'Event',
      guid
    ]
  },
  invoicePaymentNotification: {
    invoicePaymentNotificationEvent,
    mailSendError,
    logErrorParams: [
      'Error sending invoice approved email',
      guid,
      mailSendError
    ],
    mailSendParams: [
      'invoice-payment-notification',
      invoicePaymentNotificationEvent.value.body,
      user.email,
      'Recordatorio de pago',
      guid
    ],
    logMessageParams: [
      'Consuming user event',
      invoicePaymentNotificationEvent,
      'Event',
      guid
    ]
  }
};
