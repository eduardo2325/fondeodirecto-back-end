const factory = require('../factories/notification');

const guid = 'dff8ca29-9652-409d-998c-6df5a1933674';

const BLANK_PDF = `${__dirname}/blank.pdf`;

const id = 1;
const now = {
  day: 1,
  month: 'mayo',
  time: '16:20'
};
const invoice = {
  id: 123,
  cxc_name: 'CXC NAME',
  cxc_rfc: 'XXXX01234567',
  cxp_name: 'CXP NAME',
  cxp_rfc: 'YYYY01234567',
  investor_name: 'INVESTOR NAME',
  investor_rfc: 'ZZZZ98719873',
  uuid: 'ABCD-12345',
  total: 5000,
  number: 'A-23',
  business_name: 'Test'
};

// it would be: investor, cxc, cxp
const targetUser = {
  email: 'foo@candy.bar',
  name: 'FOO'
};

const produceParams = {
  recipients: [
    [ targetUser.name, targetUser.email ]
  ]
};

class NotificationFixtures {
  genericProduce() {
    return {
      id,
      now,
      guid,
      invoice,
      targetUser,
      produceParams
    };
  }

  agreementAccepted() {
    return {
      recipients: [
        [ targetUser.name, targetUser.email ]
      ],
      agreed_at: now,
      contract: 'TEST',
      username: 'someone',
      attachments: [ {
        filename: 'test.pdf',
        path: BLANK_PDF
      } ]
    };
  }

  notifyFundRequest() {
    return {
      recipients: [
        [ targetUser.name, targetUser.email ]
      ],
      attachments: [ {
        filename: `Anexo_A_${invoice.investor_rfc}_${id}.pdf`,
        path: BLANK_PDF
      } ],
      investor_name: targetUser.name,
      requested_at: now
    };
  }

  notifyInvoicePublished() {
    return {
      recipients: [
        [ targetUser.name, targetUser.email ]
      ],
      attachments: [ {
        filename: `Anexo_A_${invoice.cxc_rfc}_${id}.pdf`,
        path: BLANK_PDF
      } ],
      invoice_id: invoice.id,
      client_name: invoice.cxc_name,
      cxc_name: targetUser.name,
      published_at: now
    };
  }

  notifyInvoiceApproved() {
    return {
      recipients: [
        [ targetUser.name, targetUser.email ]
      ],
      attachments: [ {
        filename: `Anexo_${invoice.cxp_rfc}_${id}.pdf`,
        path: BLANK_PDF
      } ],
      invoices: [ invoice ],
      approved_at: now
    };
  }

  notifyFundApproved() {
    return {
      recipients: [
        [ targetUser.name, targetUser.email ]
      ],
      attachments: [ {
        filename: `Anexo_A_${invoice.cxp_rfc}_${id}.pdf`,
        path: BLANK_PDF
      } ],
      invoice_id: invoice.id,
      mailerFlagFideicomiso: false,
      client_name: invoice.cxp_name,
      cxp_name: targetUser.name,
      nombre_proveedor: 'PROVEEDOR',
      invoice_number: '123',
      fecha_expedicion: new Date().toString(),
      fecha_pago: new Date().toString(),
      fund_at: now
    };
  }

  notifyFundApprovedFideicomiso() {
    return {
      recipients: [
        [ targetUser.name, targetUser.email ]
      ],
      attachments: [ {
        filename: `Anexo_A_${invoice.cxp_rfc}_${id}.pdf`,
        path: BLANK_PDF
      } ],
      invoice_id: invoice.id,
      mailerFlagFideicomiso: true,
      client_name: invoice.cxp_name,
      cxp_name: targetUser.name,
      nombre_proveedor: 'PROVEEDOR',
      invoice_number: '123',
      fecha_expedicion: new Date().toString(),
      fecha_pago: new Date().toString(),
      fund_at: now
    };
  }

  validDummyEvent() {
    return factory.createDummyEvent();
  }
  validEventWithTopic() {
    return factory.validEventWithTopic();
  }
  validEventWithInvalidTopic() {
    return factory.validEventWithInvalidTopic();
  }
}

module.exports = new NotificationFixtures();
