const _ = require('lodash');
const moment = require('/var/lib/core/js/moment');
const factory = require('../factories/user');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';

class InvitationFixtures {
  validInvitation() {
    return factory.createInvitation('token', 'new user', 'user@fondeodirecto.com');
  }

  validInvestorInvitation() {
    return { token: 'FN9P23F3O',
      name: 'John Doe',
      email: 'investor-test@fondeodirecto.com'
    };
  }

  guid() {
    return guid;
  }

  newUserEmailParams(body) {
    return [
      'invitation',
      body,
      body.email,
      'Bienvenido a Fondeo Directo',
      guid
    ];
  }

  newInvestorEmailParams(invitation) {
    return [
      'investor-invitation-full',
      { name: invitation.name, token: invitation.token, emails: invitation.email },
      invitation.email,
      'Bienvenido a Fondeo Directo',
      guid
    ];
  }

  validUser() {
    return factory.createUser('Valid User', 'valid@fondeodirecto.com');
  }

  validInvoice() {
    return factory.createInvoice('A client');
  }

  validMovement() {
    return factory.createMovement(100);
  }

  pendingDepositTransaction() {
    return factory.pendingTransaction('DEPOSIT');
  }

  pendingWithdrawTransaction() {
    return factory.pendingTransaction('WITHDRAW');
  }

  validClientInvoicePayment() {
    return factory.clientInvoicePayment('Client name', 1, 'A1');
  }

  validCompanyProposed() {
    return factory.companyProposed('Client name', 1, 'A1');
  }

  passwordChangedEmailParams(body) {
    return [
      'password-changed',
      body,
      body.email,
      'Tu contraseña ha sido actualizada',
      guid
    ];
  }

  recoverPasswordEmailParams(body) {
    return [
      'recover-password',
      body,
      body.email,
      'Recuperar contraseña',
      guid
    ];
  }

  invoiceCreatedParams(body) {
    const title = `Acción requerida: ${body.company_name} ha subido una nueva factura a Fondeo Directo`;

    return [
      'invoice-created',
      body,
      body.emails,
      title,
      guid
    ];
  }

  invoiceApprovedParams(body) {
    const title = `¡Enhorabuena!  ${body.client_name} ha aprobado tu factura ${body.invoice_number}, ¡A buscar Fondeo!`;

    return [
      'invoice-approved',
      body,
      body.emails,
      title,
      guid
    ];
  }

  invoiceFundRequestParams(body) {
    const title = `Acción requerida:  ${body.investor_name} ha solicitado Fondear una factura`;

    return [
      'invoice-fund-request',
      body,
      body.emails,
      title,
      guid
    ];
  }

  invoiceRejectedParams(body) {
    const title = `¡Atención! ${body.client_name} ha rechazado tu factura ${body.invoice_number}`;

    return [
      'invoice-rejected',
      body,
      body.emails,
      title,
      guid
    ];
  }

  publishedInvoiceRejectedParams(body) {
    const title = `¡Atención! Tu factura ${body.invoice_number} ha sido removida del Market`;

    return [
      'published-invoice-rejected',
      body,
      body.emails,
      title,
      guid
    ];
  }

  fundRequestedInvoiceRejectedParams(body) {
    const title = '¡Atención! Tu fondeo fue cancelado';

    return [
      'fund-requested-invoice-rejected',
      body,
      body.emails,
      title,
      guid
    ];
  }

  fundRequestedInvoiceApprovedParams(body) {
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido fondeada`;
    const investorTitle = 'Tu fondeo fue procesado';
    const cloneBody = _.cloneDeep(body);

    let expiration = cloneBody.invoice_expiration;

    expiration = new Date(expiration);
    expiration = moment(expiration).format('LL');

    cloneBody.invoice_expiration = expiration;

    return [ [
      'cxc-fund-requested-invoice-approved',
      cloneBody,
      cloneBody.cxc_emails,
      cxcTitle,
      guid
    ], [
      'investor-fund-requested-invoice-approved',
      cloneBody,
      cloneBody.investor_emails,
      investorTitle,
      guid
    ] ];
  }

  withdrawCreatedParams(body) {
    const title = `Acción requerida: ${body.company_name} ha solicitado un retiro a Fondeo Directo`;

    return [
      'withdraw-created',
      body,
      body.emails,
      title,
      guid
    ];
  }

  depositCreatedParams(body) {
    const title = `Acción requerida: ${body.company_name} ha realizado un depósito a Fondeo Directo`;

    return [
      'deposit-created',
      body,
      body.emails,
      title,
      guid
    ];
  }

  pendingDepositTransactionParams(body) {
    const title = 'Tu depósito ha sido procesado';

    return [
      'deposit-approved',
      body,
      body.emails,
      title,
      guid
    ];
  }

  pendingWithdrawTransactionParams(body) {
    const title = 'Tu retiro de fondos ha sido procesado';

    return [
      'withdraw-approved',
      body,
      body.emails,
      title,
      guid
    ];
  }

  pendingDepositTransactionRejectParams(body) {
    const title = '¡Atención!  Tu depósito no pudo ser procesado';

    return [
      'deposit-rejected',
      body,
      body.emails,
      title,
      guid
    ];
  }

  pendingWithdrawTransactionRejectParams(body) {
    const title = '¡Atención!  Tu retiro no pudo ser procesado';

    return [
      'withdraw-rejected',
      body,
      body.emails,
      title,
      guid
    ];
  }

  clientInvoicePaymentCreatedParams(body) {
    const title = `Acción requerida: ${body.client_name} ha notificado el pago de una factura`;

    return [
      'client-invoice-payment-created',
      body,
      body.emails,
      title,
      guid
    ];
  }

  companyProposedParams(body) {
    const title = 'Nueva propuesta recibida';

    return [
      'company-proposed',
      body,
      body.emails,
      title,
      guid
    ];
  }

  invoiceExpiredParams(body) {
    const title = `¡Atención! Tu factura ${body.invoice_number} ha expirado`;

    return [
      'invoice-expired',
      body,
      body.emails,
      title,
      guid
    ];
  }

  companyRoleSuspension(role) {
    return factory.roleSuspension(role, true);
  }

  companyRoleUnsuspension(role) {
    return factory.roleSuspension(role, false);
  }

  roleUnsuspensionParams(body) {
    const title = '¡Atención! Funcionalidades restauradas';

    return [
      body.role.toLowerCase() + '-unsuspended',
      body,
      body.emails,
      title,
      guid
    ];
  }

  roleSuspensionParams(body) {
    const title = '¡Atención! Funcionalidades limitadas';

    return [
      body.role.toLowerCase() + '-suspended',
      body,
      body.emails,
      title,
      guid
    ];
  }

  invoicePaymentDueParams(body) {
    const title = `¡Atención! El pago de la factura ${body.invoice_number} ha vencido`;

    return [
      'invoice-payment-due',
      body,
      body.emails,
      title,
      guid
    ];
  }

  validNewInvoicesEvent() {
    return {
      username: 'investorUser',
      email: 'investor@fondeodirecto.com',
      token: 'X23425A'
    };
  }

  notifyNewInvoicesParams(body) {
    // console.log(body);
    return [
      'new-invoices',
      body,
      body.email,
      'Tenemos nuevas facturas para ti',
      guid
    ];
  }

  invoiceCompletedParams(body) {
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const cxpTitle = `El pago de la factura ${body.invoice_number} ha sido confirmado`;
    const investorTitle = '¡Buenas noticias! Tu fondeo se ha completado';
    const cloneBody = _.cloneDeep(body);

    return [ [
      'cxp-invoice-completed',
      cloneBody,
      cloneBody.cxp_emails,
      cxpTitle,
      guid
    ], [
      'cxc-invoice-completed',
      cloneBody,
      cloneBody.cxc_emails,
      cxcTitle,
      guid
    ], [
      'investor-invoice-completed',
      cloneBody,
      cloneBody.investor_emails,
      investorTitle,
      guid
    ] ];
  }

  invoiceLostParams(body) {
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const investorTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const cloneBody = _.cloneDeep(body);

    return [ [
      'cxc-lost-invoice',
      cloneBody,
      cloneBody.cxc_emails,
      cxcTitle,
      guid
    ], [
      'investor-lost-invoice',
      cloneBody,
      cloneBody.investor_emails,
      investorTitle,
      guid
    ] ];
  }

  invoiceLatePaymentParams(body) {
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const investorTitle = '¡Buenas noticias! Tu Fondeo se ha completado';
    const cloneBody = _.cloneDeep(body);

    return [ [
      'cxc-late-payment-invoice',
      cloneBody,
      cloneBody.cxc_emails,
      cxcTitle,
      guid
    ], [
      'investor-late-payment-invoice',
      cloneBody,
      cloneBody.investor_emails,
      investorTitle,
      guid
    ] ];
  }

  invoicePaymentNotificationParams(body) {
    const title = 'Recordatorio de pago';

    return [
      'invoice-payment-notification',
      body,
      body.emails,
      title,
      guid
    ];
  }

  validInvoices() {
    const invoices = [
      { id: 10, operation_id: 53 },
      { id: 23, operation_id: 100 }
    ];

    return factory.createFundRequest(invoices);
  }
}

module.exports = new InvitationFixtures();
