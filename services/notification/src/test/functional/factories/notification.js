class NotificationFactory {
  createDummyEvent() {
    return {
      message: {
        type: 'dummyEvent',
        body: {
          username: 'SYSTEM'
        },
        guid: 'dummy'
      },
      key: 'DLMD'
    };
  }

  validEventWithTopic() {
    return {
      topic: 'user',
      message: {
        type: 'dummyEvent',
        body: {
          username: 'SYSTEM'
        },
        guid: 'dummy'
      },
      key: 'DLMD'
    };
  }

  validEventWithInvalidTopic() {
    return {
      topic: 'invalidTopic',
      message: {
        type: 'dummyEvent',
        body: {
          username: 'SYSTEM'
        },
        guid: 'dummy'
      },
      key: 'DLMD'
    };
  }
}

module.exports = new NotificationFactory();
