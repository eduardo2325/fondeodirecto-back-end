class InvitationFactory {
  createInvitation(token, username, email) {
    return {
      token,
      username,
      email,
      company_role: 'COMPANY'
    };
  }

  createUser(username, email) {
    return {
      id: 1,
      username,
      email
    };
  }

  createInvoice(client_name) {
    return {
      id: 1,
      client_name,
      invoice_number: 1,
      emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      cxc_emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      cxp_emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      investor_emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      investor_name: 'An investor',
      company_name: 'A company',
      invoice_expiration: new Date().toString()
    };
  }

  createFundRequest(invoices) {
    return {
      emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      invoices,
      investor_name: 'An investor'
    };
  }

  createMovement(amount) {
    return {
      id: 1,
      amount
    };
  }

  pendingTransaction(type) {
    return {
      id: 1,
      type
    };
  }

  roleSuspension(role, suspended) {
    return {
      role,
      suspended,
      emails: 'first@fondeodirecto.com, second@fondeodirecto.com'
    };
  }

  clientInvoicePayment(clientName, invoiceId, invoiceNumber) {
    return {
      emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      client_name: clientName,
      invoice_id: invoiceId,
      invoice_number: invoiceNumber
    };
  }

  companyProposed(businessName, proposerName) {
    return {
      emails: 'first@fondeodirecto.com, second@fondeodirecto.com',
      business_name: businessName,
      proposer_name: proposerName
    };
  }
}

module.exports = new InvitationFactory();
