const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../helpers/consumers/user');
const wait = require('../../helpers/wait');
const mailer = require('../../../../lib/mailer');
const log = require('/var/lib/core/js/log').prototype;

const fixturesNotification = require('../../fixtures/notification');

const fixtures = require('../../fixtures/user');

chai.should();
chai.use(chaiAsPromised);

describe('functional/User consumer', () => {
  let guid = '';

  before(() => {
    guid = fixtures.guid();

    return helper.setup();
  });

  after(() => helper.teardown());

  afterEach(() => sandbox.restore());

  describe('Dummy Event', () => {
    it('should consume dummy event', () => {
      const fixture = fixturesNotification.validEventWithTopic();

      sandbox.stub(log, 'message').callsFake(() => true);

      return helper
        .produceDummyEvent(fixture)
        .then(wait())
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0][1].value.should.be.eql(fixture.message);
        });
    });
  });

  describe('invitationCreated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvitation = fixtures.validInvitation();
      const mailParams = fixtures.newUserEmailParams(validInvitation);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvitationCreated(validInvitation, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvitationCreated' is received", () => {
      const validInvitation = fixtures.validInvitation();
      const mailParams = fixtures.newUserEmailParams(validInvitation);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvitationCreated(validInvitation, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('resendInvitation', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvitation = fixtures.validInvitation();
      const mailParams = fixtures.newUserEmailParams(validInvitation);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceResendInvitation(validInvitation, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'ResendInvitation' is received", () => {
      const validInvitation = fixtures.validInvitation();
      const mailParams = fixtures.newUserEmailParams(validInvitation);

      sandbox.spy(mailer, 'send');

      return helper
        .produceResendInvitation(validInvitation, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('passwordChanged', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validUser = fixtures.validUser();
      const mailParams = fixtures.passwordChangedEmailParams(validUser);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .producePasswordChanged(validUser, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PasswordChanged' is received", () => {
      const validUser = fixtures.validUser();
      const mailParams = fixtures.passwordChangedEmailParams(validUser);

      sandbox.spy(mailer, 'send');

      return helper
        .producePasswordChanged(validUser, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('recoverPassword', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validUser = fixtures.validUser();
      const mailParams = fixtures.recoverPasswordEmailParams(validUser);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceRecoverPassword(validUser, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'RecoverPassword' is received", () => {
      const validUser = fixtures.validUser();
      const mailParams = fixtures.recoverPasswordEmailParams(validUser);

      sandbox.spy(mailer, 'send');

      return helper
        .produceRecoverPassword(validUser, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoiceCreated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceCreatedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceCreated(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoiceApproved' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceCreatedParams(validInvoice);


      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceCreated(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoiceApproved', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceApprovedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceApproved(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoiceApproved' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceApprovedParams(validInvoice);


      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceApproved(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoiceFundRequest', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoices = fixtures.validInvoices();
      const mailParams = fixtures.invoiceFundRequestParams(validInvoices);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceFundRequest(validInvoices, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoiceFundRequest' is received", () => {
      const validInvoices = fixtures.validInvoices();
      const mailParams = fixtures.invoiceFundRequestParams(validInvoices);


      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceFundRequest(validInvoices, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoiceRejected', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceRejectedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceRejected(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoiceRejected' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceRejectedParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceRejected(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('publishedInvoiceRejected', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.publishedInvoiceRejectedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .producePublishedInvoiceRejected(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PublishedInvoiceRejected' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.publishedInvoiceRejectedParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .producePublishedInvoiceRejected(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('fundRequestedInvoiceRejected', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.fundRequestedInvoiceRejectedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceFundRequestedInvoiceRejected(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PublishedInvoiceRejected' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.fundRequestedInvoiceRejectedParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceFundRequestedInvoiceRejected(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('fundRequestedInvoiceApproved', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.fundRequestedInvoiceApprovedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceFundRequestedInvoiceApproved(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
        });
    });

    it("should send an email when the event 'PublishedInvoiceApproved' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.fundRequestedInvoiceApprovedParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceFundRequestedInvoiceApproved(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
        });
    });
  });

  describe('withdrawCreated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validWithdraw = fixtures.validMovement();
      const mailParams = fixtures.withdrawCreatedParams(validWithdraw);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceWithdrawCreated(validWithdraw, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'WithdrawCreated' is received", () => {
      const validWithdraw = fixtures.validMovement();
      const mailParams = fixtures.withdrawCreatedParams(validWithdraw);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceWithdrawCreated(validWithdraw, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('depositCreated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validDeposit = fixtures.validMovement();
      const mailParams = fixtures.depositCreatedParams(validDeposit);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceDepositCreated(validDeposit, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'DepositCreated' is received", () => {
      const validDeposit = fixtures.validMovement();
      const mailParams = fixtures.depositCreatedParams(validDeposit);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceDepositCreated(validDeposit, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('pendingTransactionApproved', () => {
    it('should catch error when mailer.send return a reject', () => {
      const pendingTransaction = fixtures.pendingDepositTransaction();
      const mailParams = fixtures.pendingDepositTransactionParams(pendingTransaction);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceTransactionApproved(pendingTransaction, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PendingTransactionApproved' is received and type is DEPOSIT", () => {
      const pendingTransaction = fixtures.pendingDepositTransaction();
      const mailParams = fixtures.pendingDepositTransactionParams(pendingTransaction);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceTransactionApproved(pendingTransaction, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PendingTransactionApproved' is received and type is WITHDRAW", () => {
      const pendingTransaction = fixtures.pendingWithdrawTransaction();
      const mailParams = fixtures.pendingWithdrawTransactionParams(pendingTransaction);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceTransactionApproved(pendingTransaction, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('pendingTransactionRejected', () => {
    it('should catch error when mailer.send return a reject', () => {
      const pendingTransaction = fixtures.pendingDepositTransaction();
      const mailParams = fixtures.pendingDepositTransactionRejectParams(pendingTransaction);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceTransactionRejected(pendingTransaction, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PendingTransactionRejected' is received and type is DEPOSIT", () => {
      const pendingTransaction = fixtures.pendingDepositTransaction();
      const mailParams = fixtures.pendingDepositTransactionRejectParams(pendingTransaction);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceTransactionRejected(pendingTransaction, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'PendingTransactionRejected' is received and type is WITHDRAW", () => {
      const pendingTransaction = fixtures.pendingWithdrawTransaction();
      const mailParams = fixtures.pendingWithdrawTransactionRejectParams(pendingTransaction);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceTransactionRejected(pendingTransaction, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('clientInvoicePaymentCreated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const payment = fixtures.validClientInvoicePayment();
      const mailParams = fixtures.clientInvoicePaymentCreatedParams(payment);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceClientInvoicePaymentCreated(payment, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'ClientInvoicePaymentCreated' is received", () => {
      const payment = fixtures.validClientInvoicePayment();
      const mailParams = fixtures.clientInvoicePaymentCreatedParams(payment);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceClientInvoicePaymentCreated(payment, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoiceExpired', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceExpiredParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceExpired(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoiceExpired' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceExpiredParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceExpired(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('companyRoleSuspensionUpdated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const event = fixtures.companyRoleSuspension('CXC');
      const mailParams = fixtures.roleSuspensionParams(event);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceCompanyRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'CompanyRoleSuspensionUpdated' for suspended CXC", () => {
      const event = fixtures.companyRoleSuspension('CXC');
      const mailParams = fixtures.roleSuspensionParams(event);

      sandbox.spy(mailer, 'send');

      return helper
        .produceCompanyRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'CompanyRoleSuspensionUpdated' for unsuspended CXC", () => {
      const event = fixtures.companyRoleUnsuspension('CXC');
      const mailParams = fixtures.roleUnsuspensionParams(event);

      sandbox.spy(mailer, 'send');

      return helper
        .produceCompanyRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'CompanyRoleSuspensionUpdated' for suspended CXP", () => {
      const event = fixtures.companyRoleSuspension('CXP');
      const mailParams = fixtures.roleSuspensionParams(event);

      sandbox.spy(mailer, 'send');

      return helper
        .produceCompanyRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'CompanyRoleSuspensionUpdated' for unsuspended CXP", () => {
      const event = fixtures.companyRoleUnsuspension('CXP');
      const mailParams = fixtures.roleUnsuspensionParams(event);

      sandbox.spy(mailer, 'send');

      return helper
        .produceCompanyRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('investorRoleSuspensionUpdated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvitation = fixtures.validInvestorInvitation();
      const mailParams = fixtures.newInvestorEmailParams(validInvitation);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvestorInvitationCreated(validInvitation, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvestorInvitationCreated' is received", () => {
      const validInvitation = fixtures.validInvestorInvitation();
      const mailParams = fixtures.newInvestorEmailParams(validInvitation);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvestorInvitationCreated(validInvitation, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('investorRoleSuspensionUpdated', () => {
    it('should catch error when mailer.send return a reject', () => {
      const event = fixtures.companyRoleSuspension('INVESTOR');
      const mailParams = fixtures.roleSuspensionParams(event);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvestorRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvestorRoleSuspensionUpdated' for suspended INVESTOR", () => {
      const event = fixtures.companyRoleSuspension('INVESTOR');
      const mailParams = fixtures.roleSuspensionParams(event);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvestorRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvestorRoleSuspensionUpdated' for unsuspended INVESTOR", () => {
      const event = fixtures.companyRoleUnsuspension('INVESTOR');
      const mailParams = fixtures.roleUnsuspensionParams(event);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvestorRoleSuspension(event, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoicePaymentDue', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoicePaymentDueParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoicePaymentDue(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoicePaymentDue' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoicePaymentDueParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoicePaymentDue(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('invoiceCompleted', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceCompletedParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceCompleted(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledThrice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
          mailer.send.args[2].should.be.eql(mailParams[2]);
        });
    });

    it("should send an email when the event 'InvoiceCompleted' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceCompletedParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceCompleted(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledThrice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
          mailer.send.args[2].should.be.eql(mailParams[2]);
        });
    });
  });

  describe('invoiceLost', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceLostParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceLost(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
        });
    });

    it("should send an email when the event 'InvoiceLost' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceLostParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceLost(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
        });
    });
  });

  describe('invoiceLatePayment', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceLatePaymentParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoiceLatePayment(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
        });
    });

    it("should send an email when the event 'InvoiceLatePayment' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoiceLatePaymentParams(validInvoice);

      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoiceLatePayment(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledTwice.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams[0]);
          mailer.send.args[1].should.be.eql(mailParams[1]);
        });
    });
  });

  describe('companyProposed', () => {
    it('should catch error when mailer.send return a reject', () => {
      const proposed = fixtures.validCompanyProposed();
      const mailParams = fixtures.companyProposedParams(proposed);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceCompanyProposed(proposed, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'ClientInvoicePaymentCreated' is received", () => {
      const proposed = fixtures.validCompanyProposed();
      const mailParams = fixtures.companyProposedParams(proposed);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceCompanyProposed(proposed, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  describe('notifyNewInvoices', () => {
    it('should catch error when mailer.send return a reject', () => {
      const notifyNewInvoices = fixtures.validNewInvoicesEvent();
      const mailParams = fixtures.notifyNewInvoicesParams(notifyNewInvoices);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceNotifyNewInvoices(notifyNewInvoices, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'NotifyNewInvoices' is received", () => {
      const notifyNewInvoices = fixtures.validNewInvoicesEvent();
      const mailParams = fixtures.notifyNewInvoicesParams(notifyNewInvoices);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.resolve());

      return helper
        .produceNotifyNewInvoices(notifyNewInvoices, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });

  const { guid: GUID, produceParams } = fixturesNotification.genericProduce();

  // generic _produce method
  describe('_produce', () => {
    // we just test this out because all methods are generic,
    // also, we avoid duplicating code just for matter-of-testing
    // all latter cases are tested for success, while this catch-only
    it('should catch error when mailer.send return a reject', () => {
      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper.produceAgreementAccepted(produceParams, GUID)
        .then(wait()).should.be.fulfilled;
    });
  });

  // this applies for all three roles, once they accept their respective agreement
  // all related users-by-role will be notified, a copy is also sent to the administrator
  describe('agreementAccepted', () => {
    it('should work as expected', () => {
      return helper.produceAgreementAccepted(fixturesNotification.agreementAccepted(), GUID)
        .then(wait()).should.be.fulfilled;
    });
  });

  // when a investor wants to fund a published invoice, the admin is also notified
  describe('notifyFundRequest', () => {
    it('should work as expected', () => {
      return helper.produceNotifyFundRequest(fixturesNotification.notifyFundRequest(), GUID)
        .then(wait()).should.be.fulfilled;
    });
  });

  // when the cxp approves an invoice from the marketplace, the admin is also notified
  describe('notifyInvoiceApproved', () => {
    it('should work as expected', () => {
      return helper.produceNotifyInvoiceApproved(fixturesNotification.notifyInvoiceApproved(), GUID)
        .then(wait()).should.be.fulfilled;
    });
  });

  // finally, once the FD-admin fund the approved invoice, the cxp is also notified
  describe('notifyFundApproved', () => {
    it('should work as expected', () => {
      return helper.produceNotifyFundApproved(fixturesNotification.notifyFundApproved(), GUID)
        .then(wait()).should.be.fulfilled;
    });
    it('should work as expected with fideicomiso flag on true', () => {
      return helper.produceNotifyFundApproved(fixturesNotification.notifyFundApprovedFideicomiso(), GUID)
        .then(wait()).should.be.fulfilled;
    });
  });

  describe('invoicePaymentNotification', () => {
    it('should catch error when mailer.send return a reject', () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoicePaymentNotificationParams(validInvoice);

      sandbox.stub(mailer, 'send').callsFake(() => Promise.reject(new Error('error')));

      return helper
        .produceInvoicePaymentNotification(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });

    it("should send an email when the event 'InvoiceApproved' is received", () => {
      const validInvoice = fixtures.validInvoice();
      const mailParams = fixtures.invoicePaymentNotificationParams(validInvoice);


      sandbox.spy(mailer, 'send');

      return helper
        .produceInvoicePaymentNotification(validInvoice, guid)
        .then(wait())
        .then(() => {
          mailer.send.calledOnce.should.be.true;
          mailer.send.args[0].should.be.eql(mailParams);
        });
    });
  });
});
