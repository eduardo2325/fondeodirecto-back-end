const ConsumerHelper = require('../consumer');

class NotificationConsumerHelper extends ConsumerHelper {
  setup() {
    return super.setup();
  }

  dummyEventCreated() {
    const event = {
      topic: 'notification',
      message: {
        type: 'DummyEvent',
        body: {
          username: 'SYSTEM'
        },
        guid: 'dummy'
      },
      key: 'DLMD'
    };

    return this.produce(event);
  }
}

module.exports = new NotificationConsumerHelper();
