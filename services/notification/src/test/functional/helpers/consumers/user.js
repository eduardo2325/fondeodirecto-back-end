const ConsumerHelper = require('../consumer');

class UserConsumerHelper extends ConsumerHelper {
  setup() {
    return super.setup();
  }

  _produce(type, body, guid) {
    const event = {
      topic: 'user',
      message: {
        type,
        body,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceDummyEvent(event) {
    return this.produce(event);
  }

  produceInvitationCreated(invitation, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvitationCreated',
        body: invitation,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvestorInvitationCreated(invitation, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvestorInvitationCreated',
        body: {
          emails: [ invitation.email ].join(),
          name: invitation.name,
          token: invitation.token
        },
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceResendInvitation(invitation, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'ResendInvitation',
        body: invitation,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  producePasswordChanged(user, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'PasswordChanged',
        body: user,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceRecoverPassword(user, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'RecoverPassword',
        body: user,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceCreated(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'invoiceCreated',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceApproved(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceApproved',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceFundRequest(validInvoices, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceFundRequest',
        body: validInvoices,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceRejected(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceRejected',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  producePublishedInvoiceRejected(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'PublishedInvoiceRejected',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceFundRequestedInvoiceRejected(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'FundRequestedInvoiceRejected',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceFundRequestedInvoiceApproved(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'FundRequestedInvoiceApproved',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceWithdrawCreated(withdraw, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'WithdrawCreated',
        body: withdraw,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceDepositCreated(deposit, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'DepositCreated',
        body: deposit,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceTransactionApproved(pendingTransaction, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'PendingTransactionApproved',
        body: pendingTransaction,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceTransactionRejected(pendingTransaction, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'PendingTransactionRejected',
        body: pendingTransaction,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceClientInvoicePaymentCreated(payment, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'ClientInvoicePaymentCreated',
        body: payment,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceCompanyProposed(proposed, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'CompanyProposed',
        body: proposed,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceExpired(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceExpired',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceCompanyRoleSuspension(roleSuspensionUpdate, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'CompanyRoleSuspensionUpdated',
        body: roleSuspensionUpdate,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvestorRoleSuspension(roleSuspensionUpdate, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvestorRoleSuspensionUpdated',
        body: roleSuspensionUpdate,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoicePaymentDue(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoicePaymentDue',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceCompleted(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceCompleted',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceLost(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceLost',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceInvoiceLatePayment(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoiceLatePayment',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceNotifyNewInvoices(invoices, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'NotifyNewInvoices',
        users: {
          investors: [
            {
              name: 'investorUser',
              email: 'investor@fondeodirecto.com',
              token: 'X23425A'
            }
          ]
        },
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  produceAgreementAccepted(body, guid) {
    return this._produce('AgreementAccepted', body, guid);
  }

  produceNotifyFundRequest(body, guid) {
    return this._produce('NotifyFundRequest', body, guid);
  }

  produceNotifyInvoicePublished(body, guid) {
    return this._produce('NotifyInvoicePublished', body, guid);
  }

  produceNotifyInvoiceApproved(body, guid) {
    return this._produce('NotifyInvoiceApproved', body, guid);
  }

  produceNotifyFundApproved(body, guid) {
    return this._produce('NotifyFundApproved', body, guid);
  }

  produceInvoicePaymentNotification(invoice, guid) {
    const event = {
      topic: 'user',
      message: {
        type: 'InvoicePaymentNotification',
        body: invoice,
        guid
      },
      key: 1
    };

    return this.produce(event);
  }

  teardown() {
    return super.teardown();
  }
}

module.exports = new UserConsumerHelper();
