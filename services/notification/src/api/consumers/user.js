const BaseConsumer = require('/var/lib/core/js/kafka/base-consumer');
const Logger = require('/var/lib/core/js/log');
const moment = require('/var/lib/core/js/moment');
const log = new Logger(module);
const mailer = require('../../lib/mailer');
const template = role => {
  switch (role) {
  case 'INVESTOR':
    return 'investor-invitation';
  case 'COMPANY':
  default:
    return 'invitation';
  }
};

class UserConsumer extends BaseConsumer {
  constructor() {
    super('user');
  }

  _send(event, { partial, body, title, guid }) {
    log.message('Consuming user event', event, 'Event', guid);

    return Promise.resolve()
      .then(() => mailer.send(partial, body, body.email, title, guid))
      .catch(err => {
        log.error(`Error sending ${partial} email`, guid, err);
        throw err;
      });
  }

  _sendAll(event, params) {
    return Promise.all(params.body.recipients.map(userData => {
      return this._send(event, {
        ...params,
        body: {
          ...params.body,
          email: userData[1],
          username: userData[0]
        }
      });
    }));
  }

  dummyEvent(event) {
    return Promise.resolve()
      .then(() => log.message('Dummy event', event, 'consumed'));
  }

  invitationCreated(event) {
    const { value } = event;
    const { body, guid } = value;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      template(body.company_role),
      body,
      body.email,
      'Bienvenido a Fondeo Directo',
      guid
    ).catch(err => {
      log.error('Error sending invitation', guid, err);
      throw err;
    });
  }

  resendInvitation(event) {
    const { value } = event;
    const { body, guid } = value;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      template(body.company_role),
      body,
      body.email,
      'Bienvenido a Fondeo Directo',
      guid
    ).catch(err => {
      log.error('Error sending invitation', guid, err);
      throw err;
    });
  }

  passwordChanged(event) {
    const { value } = event;
    const { body, guid } = value;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'password-changed',
      body,
      body.email,
      'Tu contraseña ha sido actualizada',
      guid
    ).catch(err => {
      log.error('Error sending changed password email', guid, err);
      throw err;
    });
  }

  recoverPassword(event) {
    const { value } = event;
    const { body, guid } = value;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'recover-password',
      body,
      body.email,
      'Recuperar contraseña',
      guid
    ).catch(err => {
      log.error('Error sending password token email', guid, err);
      throw err;
    });
  }

  invoiceCreated(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `Acción requerida: ${body.company_name} ha subido una nueva factura a Fondeo Directo`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-created',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice created email', guid, err);
      throw err;
    });
  }

  invoiceApproved(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `¡Enhorabuena!  ${body.client_name} ha aprobado tu factura ${body.invoice_number}, ¡A buscar Fondeo!`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-approved',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice approved email', guid, err);
      throw err;
    });
  }

  notifyFundRequest(event) {
    return this._sendAll(event, {
      ...event.value,
      partial: 'notify-fund-request',
      title: '¡En hora buena! Tu solicitud de fondear una factura fue recibida.'
    });
  }

  notifyInvoicePublished(event) {
    return this._sendAll(event, {
      ...event.value,
      partial: 'notify-invoice-published',
      title: '¡En hora buena! Tu factura ha sido publicada.'
    });
  }

  notifyInvoiceApproved(event) {
    const title = event.value.body.invoices.length > 1 ? '¡En hora buena! Tus facturas han sido aprobadas.' :
      '¡En hora buena! Tu factura ha sido aprobada.';

    return this._sendAll(event, {
      ...event.value,
      partial: 'notify-invoice-approved',
      title
    });
  }

  notifyFundApproved(event) {
    return this._sendAll(event, {
      ...event.value,
      partial: 'notify-fund-approved',
      title: `¡Atención! La factura ${event.value.body.invoice_number} ha sido fondeada.`
    });
  }

  invoiceFundRequest(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `Acción requerida:  ${body.investor_name} ha solicitado Fondear una factura`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-fund-request',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice fund request email', guid, err);
      throw err;
    });
  }

  invoiceRejected(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `¡Atención! ${body.client_name} ha rechazado tu factura ${body.invoice_number}`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-rejected',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice rejected email', guid, err);
      throw err;
    });
  }

  publishedInvoiceRejected(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `¡Atención! Tu factura ${body.invoice_number} ha sido removida del Market`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'published-invoice-rejected',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending published invoice rejected email', guid, err);
      throw err;
    });
  }

  fundRequestedInvoiceRejected(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = '¡Atención! Tu fondeo fue cancelado';

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'fund-requested-invoice-rejected',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending fund requested invoice rejected email', guid, err);
      throw err;
    });
  }

  fundRequestedInvoiceApproved(event) {
    const { value } = event;
    const { body, guid } = value;
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido fondeada`;
    const investorTitle = 'Tu fondeo fue procesado';

    let expiration = new Date(body.invoice_expiration);

    expiration = moment(expiration);
    expiration = expiration.format('LL');
    body.invoice_expiration = expiration;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'cxc-fund-requested-invoice-approved',
      body,
      body.cxc_emails,
      cxcTitle,
      guid
    ).then(() => {
      return mailer.send(
        'investor-fund-requested-invoice-approved',
        body,
        body.investor_emails,
        investorTitle,
        guid
      );
    }).catch(err => {
      log.error('Error sending fund requested invoice approved email', guid, err);
      throw err;
    });
  }

  withdrawCreated(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `Acción requerida: ${body.company_name} ha solicitado un retiro a Fondeo Directo`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'withdraw-created',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending withdraw created email', guid, err);
      throw err;
    });
  }

  depositCreated(event) {
    const { value } = event;
    const { body, guid } = value;

    const account = body.mailerFlagFideicomiso ? 'Fideicomiso' : 'Fondeo Directo';
    const title = `Acción requerida: ${body.company_name} ha realizado un depósito a ${account}`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'deposit-created',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending deposit created email', guid, err);
      throw err;
    });
  }

  pendingTransactionApproved(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = {
      withdraw: 'Tu retiro de fondos ha sido procesado',
      deposit: 'Tu depósito ha sido procesado'
    };
    const type = body.type.toLowerCase();
    const mail = type + '-approved';

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      mail,
      body,
      body.emails,
      title[type],
      guid
    ).catch(err => {
      log.error('Error sending approved pending transaction email', guid, err);
      throw err;
    });
  }

  pendingTransactionRejected(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = {
      withdraw: '¡Atención!  Tu retiro no pudo ser procesado',
      deposit: '¡Atención!  Tu depósito no pudo ser procesado'
    };
    const type = body.type.toLowerCase();
    const mail = type + '-rejected';

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      mail,
      body,
      body.emails,
      title[type],
      guid
    ).catch(err => {
      log.error('Error sending rejected pending transaction email', guid, err);
      throw err;
    });
  }

  clientInvoicePaymentCreated(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `Acción requerida: ${body.client_name} ha notificado el pago de una factura`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'client-invoice-payment-created',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending client invoice payment created email', guid, err);
      throw err;
    });
  }

  invoiceExpired(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `¡Atención! Tu factura ${body.invoice_number} ha expirado`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-expired',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice expired email', guid, err);
      throw err;
    });
  }

  companyRoleSuspensionUpdated(event) {
    const { value } = event;
    const { body, guid } = value;
    const { role, suspended, emails } = body;
    const suspendedTitle = '¡Atención! Funcionalidades limitadas';
    const unsuspendedTitle = '¡Atención! Funcionalidades restauradas';
    const mail = `${role.toLowerCase()}-${suspended ? 'suspended' : 'unsuspended'}`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      mail,
      body,
      emails,
      suspended ? suspendedTitle : unsuspendedTitle,
      guid
    ).catch(err => {
      log.error('Error sending company role suspension update email', guid, err);
      throw err;
    });
  }

  investorInvitationCreated(event) {
    const { value } = event;
    const { body, guid } = value;
    const { emails } = body;

    const mail = 'investor-invitation-full';

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      mail,
      body,
      emails,
      'Bienvenido a Fondeo Directo',
      guid
    ).catch(err => {
      log.error('Error sending investor invitation email', guid, err);
      throw err;
    });
  }

  investorRoleSuspensionUpdated(event) {
    const { value } = event;
    const { body, guid } = value;
    const { suspended, emails } = body;
    const suspendedTitle = '¡Atención! Funcionalidades limitadas';
    const unsuspendedTitle = '¡Atención! Funcionalidades restauradas';
    const mail = `investor-${suspended ? 'suspended' : 'unsuspended'}`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      mail,
      body,
      emails,
      suspended ? suspendedTitle : unsuspendedTitle,
      guid
    ).catch(err => {
      log.error('Error sending investor role suspension update email', guid, err);
      throw err;
    });
  }

  invoicePaymentDue(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = `¡Atención! El pago de la factura ${body.invoice_number} ha vencido`;

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-payment-due',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice payment due email', guid, err);
      throw err;
    });
  }

  invoiceCompleted(event) {
    const { value } = event;
    const { body, guid } = value;
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const cxpTitle = `El pago de la factura ${body.invoice_number} ha sido confirmado`;
    const investorTitle = '¡Buenas noticias! Tu fondeo se ha completado';

    log.message('Consuming user event', event, 'Event', guid);

    return Promise.all([
      mailer.send(
        'cxp-invoice-completed',
        body,
        body.cxp_emails,
        cxpTitle,
        guid
      ),
      mailer.send(
        'cxc-invoice-completed',
        body,
        body.cxc_emails,
        cxcTitle,
        guid
      ),
      mailer.send(
        'investor-invoice-completed',
        body,
        body.investor_emails,
        investorTitle,
        guid
      ) ])
      .catch(err => {
        log.error('Error sending invoice completed email', guid, err);
        throw err;
      });
  }

  invoiceLost(event) {
    const { value } = event;
    const { body, guid } = value;
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const investorTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;

    log.message('Consuming user event', event, 'Event', guid);

    return Promise.all([
      mailer.send(
        'cxc-lost-invoice',
        body,
        body.cxc_emails,
        cxcTitle,
        guid
      ),
      mailer.send(
        'investor-lost-invoice',
        body,
        body.investor_emails,
        investorTitle,
        guid
      ) ])
      .catch(err => {
        log.error('Error sending lost invoice email', guid, err);
        throw err;
      });
  }

  invoiceLatePayment(event) {
    const { value } = event;
    const { body, guid } = value;
    const cxcTitle = `¡Buenas noticias! Tu factura ${body.invoice_number} ha sido pagada`;
    const investorTitle = '¡Buenas noticias! Tu Fondeo se ha completado';

    log.message('Consuming user event', event, 'Event', guid);

    return Promise.all([
      mailer.send(
        'cxc-late-payment-invoice',
        body,
        body.cxc_emails,
        cxcTitle,
        guid
      ),
      mailer.send(
        'investor-late-payment-invoice',
        body,
        body.investor_emails,
        investorTitle,
        guid
      ) ])
      .catch(err => {
        log.error('Error sending late payment invoice email', guid, err);
        throw err;
      });
  }

  companyProposed(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = 'Nueva propuesta recibida';

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'company-proposed',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending company proposed email', guid, err);
      throw err;
    });
  }

  agreementAccepted(event) {
    return this._sendAll(event, {
      ...event.value,
      partial: 'contract-agreement',
      title: `¡Muchas gracias! Has aceptado el ${event.value.body.contract}`
    });
  }

  notifyNewInvoices(event) {
    const { value } = event;
    const { users, guid } = value;

    const title = 'Tenemos nuevas facturas para ti';
    const body = {};

    const promises = users.investors.map(investor => {
      body.username = investor.name;
      body.email = investor.email;
      body.token = investor.token;

      return mailer.send(
        'new-invoices',
        body,
        body.email,
        title,
        guid
      ).catch(err => {
        log.error('Error sending new invoices email', guid, err);
        throw err;
      });
    });

    log.message('Consuming user event', event, 'Event', guid);

    return Promise.all(promises);
  }

  invoicePaymentNotification(event) {
    const { value } = event;
    const { body, guid } = value;
    const title = 'Recordatorio de pago';

    log.message('Consuming user event', event, 'Event', guid);

    return mailer.send(
      'invoice-payment-notification',
      body,
      body.emails,
      title,
      guid
    ).catch(err => {
      log.error('Error sending invoice approved email', guid, err);
      throw err;
    });
  }
}

module.exports = new UserConsumer();
