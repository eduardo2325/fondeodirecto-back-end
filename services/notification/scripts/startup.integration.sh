#!/bin/bash

set -e

export MAILDEV=1

wait-for-it -t 0 kafka:9092

if [ ! -z "${INTERACTIVE:-}" ]; then
  echo ""
  echo "Welcome to the interactive mode, please run \`make dev\`"
  echo "or whatever you want to debug this service. Enjoy your time!"
  echo ""

  tail -f /dev/null
else
  make dev
fi
