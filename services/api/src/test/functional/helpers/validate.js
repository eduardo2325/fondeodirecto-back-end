const validator = require('is-my-json-valid');

require('chai').should();

function validate(schema) {
  return (value) => {
    const validation = validator(schema);

    return Promise.resolve()
      .then(() => validation(value))
      .then(result => {
        result.should.be.eql(true, JSON.stringify(validation.errors));
        return value;
      })
      .catch(/* istanbul ignore next */ e => {
        const fixedSchema = JSON.stringify(schema, null, 2);
        const fixedValue = JSON.stringify(value, null, 2);

        const error = `E_INVALID (${e.message}) ${fixedSchema} --- ${fixedValue}`;

        e.message = error;

        throw e;
      });
  };
}

module.exports = validate;
