const superagent = require('superagent-defaults')();
const userFixtures = require('../fixtures/user');
const sessionCredentials = userFixtures.validAdminUser();
const cxcSessionCredentials = userFixtures.validCxcUser();
const cxcSessionCredentialsTwo = userFixtures.validCxcUserTwo();
const cxpSessionCredentials = userFixtures.validCxpUser();
const cxpSessionCredentialsCaseTwo =  userFixtures.validCxpUserCase2();
const investorSessionCredentials = userFixtures.validInvestorUser();
const investorSessionCredentials2 = userFixtures.validInvestorUserCase2();

class Api {
  constructor(host = 'http://localhost:3000', prefix = '/api/') {
    this.client = superagent.set('Content-Type', 'application/json')
      .timeout({ response: 5000 });
    this.baseUrl = host + prefix;
    this.loggedIn = false;
  }

  request(method, url, data = {}, query = {}, isUrlencoded = false) {
    const client = this.client;
    const baseUrl = this.baseUrl;

    return new Promise(function(resolve, reject) {
      const request = client[method](baseUrl + url);

      if (isUrlencoded) {
        request.set('Content-Type', 'application/x-www-form-urlencoded');
      }

      if (query) {
        request.query(query);
      }

      request
        .send(data)
        .end(function(err, result) {
          if (err) {
            return reject(err);
          }

          // Quickfix to follow prevoius agent structure
          const response = {
            status: result.status,
            data: result.body
          };

          return resolve(response);
        });
    });
  }

  requestWithFile(method, url, file, data = {}, fieldName = '') {
    if (!fieldName || fieldName === 'files') {
      throw new Error('Please specify a fieldName for the uploaded file');
    }

    const client = this.client;
    const baseUrl = this.baseUrl;

    return new Promise(function(resolve, reject) {
      const request = client[method](baseUrl + url);

      if (file) {
        request.attach(fieldName, file);
      }

      if (data) {
        Object.keys(data).forEach(key => {
          if (data[key]) {
            request.field(key, data[key]);
          }
        });
      }

      request
        .timeout({ response: 15000 })
        .end(function(err, result) {
          if (err) {
            return reject(err);
          }

          // Quickfix to follow prevoius agent structure
          const response = {
            status: result.status,
            data: result.body
          };

          return resolve(response);
        });
    });
  }

  setToken(token) {
    this.client.set('Authorization', `Bearer ${token}`);
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  login(req) {
    return this.request('post', 'v1/login', req || sessionCredentials)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }

  cxcLogin() {
    return this.request('post', 'v1/login', cxcSessionCredentials)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }


  cxcLoginTwo() {
    return this.request('post', 'v1/login', cxcSessionCredentialsTwo)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }

  cxpLogin() {
    return this.request('post', 'v1/login', cxpSessionCredentials)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }

  cxpLoginCase2() {
    return this.request('post', 'v1/login', cxpSessionCredentialsCaseTwo)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }

  investorLogin() {
    return this.request('post', 'v1/login', investorSessionCredentials)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }


  investorLogin2() {
    return this.request('post', 'v1/login', investorSessionCredentials2)
      .then(response => {
        if (response.data.data) {
          this.setToken(response.data.data.token.token);
          this.loggedIn = true;
        }

        return response;
      });
  }

  logout() {
    return this.request('get', 'v1/logout').then(response => {
      this.loggedIn = false;

      return response;
    });
  }

  me() {
    return this.request('get', 'v1/me');
  }

  createCompany(company) {
    return this.request('post', 'v1/companies', company, null, true);
  }

  companyExists(rfc) {
    return this.request('get', 'v1/companies/' + rfc + '/exists');
  }

  approveCompany(id) {
    return this.request('post', `v1/companies/${id}/agreement`);
  }

  updateCompany(rfc, company) {
    return this.request('put', 'v1/companies/' + rfc, company);
  }

  updateCompanyOperationCost(rfc, operationCost) {
    return this.request('put', `v1/companies/${rfc}/operation_cost`, operationCost);
  }

  updateInvestorOperationCost(rfc, operationCost) {
    return this.request('put', `v1/companies/investors/${rfc}/operation_cost`, operationCost);
  }

  getCompany(rfc) {
    return this.request('get', 'v1/companies/' + rfc);
  }

  getCompanies(page_size, page, order_by, order_desc) {
    return this.request('get', 'v1/companies', undefined, {
      page_size,
      page,
      order_by,
      order_desc
    });
  }

  getInvestorCompanies(page_size, page, order_by, order_desc) {
    return this.request('get', 'v1/companies/investors', undefined, {
      page_size,
      page,
      order_by,
      order_desc
    });
  }

  getBankInfo(clabe) {
    return this.request('get', 'v1/clabe/' + clabe );
  }

  getBankInfoByClabe(clabe) {
    return this.request('get', 'v1/clabe/' + clabe + '/exists');
  }

  getInvitation(token) {
    return this.request('get', 'v1/users/invitations/' + token);
  }

  registration(data, token) {
    return this.request('post', 'v1/users/invitations/' + token, data);
  }

  getUserRoles() {
    return this.request('get', 'v1/users/roles');
  }

  deleteUser(email) {
    return this.request('delete', 'v1/users/' + email);
  }

  checkEmail(email) {
    return this.request('get', 'v1/users/check/' + email);
  }

  getCompanyUsers(rfc, order_by, order_desc) {
    return this.request('get', 'v1/companies/' + rfc + '/users', undefined, {
      order_by,
      order_desc
    });
  }

  getOperationCost(rfc) {
    return this.request('get', `v1/companies/${rfc}/operation_cost`);
  }

  getInvestorOperationCost(rfc) {
    return this.request('get', `v1/companies/investors/${rfc}/operation_cost`);
  }

  resendInvitation(email) {
    return this.request('post', 'v1/users/invites/' + email + '/send');
  }

  addUser(rfc, data) {
    return this.request('post', 'v1/companies/' + rfc + '/users/invite', data);
  }

  getFactorajeAndCashSummaries(rfc, data) {
    return this.request('post', 'v1/companies/investors/' + rfc + '/summaries', data);
  }

  registerInvestor(data) {
    return this.request('post', 'v1/companies/register-investor', data);
  }

  addInvestorUser(rfc, data) {
    return this.request('post', 'v1/companies/investors/' + rfc + '/users/invite', data);
  }

  createInvestor(company) {
    return this.request('post', 'v1/companies/investors', company, null, true);
  }

  changePassword(data) {
    return this.request('post', 'v1/users/change_password', data);
  }

  recoverPassword(data) {
    return this.request('post', 'v1/users/recover_password', data);
  }

  validateRecoverToken(token) {
    return this.request('get', 'v1/users/recover_password?token=' + token);
  }

  resetPassword(token, data) {
    return this.request('post', 'v1/users/reset_password/' + token, data);
  }

  downloadTracker() {
    return this.request('get', 'v1/admin/download-tracker');
  }

  findExcelTransactions() {
    return this.request('get', 'v1/admin/find-excel-transactions');
  }

  generateExcelTransactions(data) {
    return this.request('post', 'v1/admin/generate-excel-transactions', data);
  }

  excelBankReports() {
    return this.request('get', 'v1/admin/excel-transaction-files');
  }

  findExcelTransactionsFromBankReportId(id) {
    return this.request('get', 'v1/admin/excel-transaction-files/' + id);
  }

  bulkApprove(data) {
    return this.request('put', 'v1/admin/bulk-approve', data);
  }

  uploadInvoice(file) {
    return this.requestWithFile('post', 'v1/invoices', file, null, 'invoice');
  }

  createInvestorInvitation(data) {
    return this.request('post', 'v1/companies/invite-investor', data);
  }

  storeUserEvent(data) {
    return this.request('post', 'v1/statistics/store-user-event', data);
  }

  checkToken(data) {
    return this.request('post', 'v1/statistics/check-token', data);
  }

  getOperations(page_size, page, order_by, order_desc) {
    return this.request('get', 'v1/operations', undefined, {
      page_size,
      page,
      order_by,
      order_desc
    });
  }

  getPayedOperations(page_size, page, order_by, order_desc) {
    return this.request('get', 'v1/operations/payed', undefined, {
      page_size,
      page,
      order_by,
      order_desc
    });
  }

  getOperation(id) {
    return this.request('get', 'v1/operations/' + id);
  }

  getAdminOperations(query) {
    return this.request('get', 'v1/admin/operations', undefined, query);
  }

  getInvoices(page_size, page, order_by, order_desc) {
    return this.request('get', 'v1/invoices', undefined, {
      page_size,
      page,
      order_by,
      order_desc
    });
  }

  getInvoice(id) {
    return this.request('get', 'v1/invoices/' + id);
  }

  approve(data) {
    return this.request('post', 'v1/invoices/approve', data);
  }

  reject(id, data) {
    return this.request('put', 'v1/invoices/' + id + '/reject', data);
  }

  publish(data) {
    return this.request('post', 'v1/invoices/publish', data);
  }

  publishSummary(data) {
    return this.request('post', 'v1/invoices/publish/summary', data);
  }

  fund(data) {
    return this.request('post', 'v1/invoices/fund/checkout', data);
  }

  invoiceCompleted(id, data) {
    return this.request('put', 'v1/invoices/' + id + '/completed', data);
  }

  lost(id, data) {
    return this.request('put', 'v1/invoices/' + id + '/lost', data);
  }

  latePayment(id, data) {
    return this.request('put', 'v1/invoices/' + id + '/late_payment', data);
  }

  rejectPublished(id, data) {
    return this.request('put', 'v1/invoices/' + id + '/reject_published', data);
  }

  rejectFunded(id, data) {
    return this.request('put', 'v1/invoices/' + id + '/reject_funded', data);
  }

  approveFund(id) {
    return this.request('put', 'v1/invoices/' + id + '/approve_fund');
  }

  getInvoiceEstimate(id) {
    return this.request('get', `v1/invoices/${id}/estimate`);
  }

  getInvoiceDetail(id) {
    return this.request('get', `v1/invoices/${id}/detail`);
  }

  getFundEstimate(id) {
    return this.request('get', `v1/invoices/${id}/fund_estimate`);
  }

  getInvoiceXml(id) {
    return this.request('get', `v1/invoices/${id}/xml`);
  }

  getMarketplace(query) {
    return this.request('get', 'v1/marketplace/invoices', undefined, query);
  }

  withdraw(data) {
    return this.request('post', 'v1/investor/withdraw', data);
  }

  getCompanyBalance(id) {
    return this.request('get', `v1/companies/${id}/balance`);
  }

  getInvestorFundEstimate(id) {
    return this.request('get', `v1/invoices/${id}/investor_fund_estimate`);
  }

  getPendingTransactions(rfc) {
    return this.request('get', `v1/companies/${rfc}/pending_transactions`);
  }

  getInvestorProfitEstimate(id) {
    return this.request('get', `v1/invoices/${id}/investor_profit_estimate`);
  }

  getInvestorFundDetail(id) {
    return this.request('get', `v1/invoices/${id}/investor_fund_detail`);
  }

  getInvoicePaymentSummary(id) {
    return this.request('get', `v1/invoices/${id}/payment_summary`);
  }

  getAdminInvoiceDetail(id) {
    return this.request('get', `v1/admin/invoices/${id}`);
  }

  bulkDelete(data) {
    return this.request('put', 'v1/invoices/bulk-delete', data);
  }

  deposit(data) {
    const url = 'v1/investor/deposit';
    const requestData = {
      amount: data.amount,
      deposit_date: data.deposit_date
    };
    const { file } = data;

    return this.requestWithFile('post', url, file, requestData, 'receipt');
  }

  approveTransaction(id) {
    return this.request('put', `v1/transactions/${id}/approve`);
  }

  rejectTransaction(id, data) {
    return this.request('put', `v1/transactions/${id}/reject`, data);
  }

  getInvestorInvoices(page_size, page, order_by, order_desc) {
    return this.request('get', 'v1/investor/invoices', undefined, {
      page_size,
      page,
      order_by,
      order_desc
    });
  }

  getAdminInvoices(query) {
    return this.request('get', 'v1/admin/invoices', undefined, query);
  }

  updateCompanyRoleSuspension(id, data) {
    return this.request('put', `v1/companies/${id}/role/suspension`, data);
  }

  updateInvestorRoleSuspension(id, data) {
    return this.request('put', `v1/companies/investors/${id}/role/suspension`, data);
  }

  createClientInvoicePayment(invoiceId, data) {
    const url = `v1/invoices/${invoiceId}/payment_request`;
    const requestData = {
      amount: data.amount,
      payment_date: data.payment_date
    };

    return this.requestWithFile('post', url, data.file, requestData, 'receipt');
  }

  getClientInvoicePayment(id) {
    return this.request('get', `v1/invoices/${id}/payment_request`);
  }

  proposeCompany(data) {
    return this.request('put', 'v1/companies/propose', data);
  }

  getInvestorTransactions(investorCompanyId, data) {
    return this.request('post', `v1/companies/investors/${investorCompanyId}/transactions`, data);
  }

  getInvoicesAvailability(data) {
    return this.request('post', 'v1/invoices/availability', data);
  }

  updateCompanyPrepayment(id, data) {
    return this.request('put', `v1/companies/${id}/prepayment`, data);
  }
}

module.exports = new Api();
