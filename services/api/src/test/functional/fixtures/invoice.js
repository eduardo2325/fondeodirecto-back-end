const _ = require('lodash');
const factory = require('../factories/invoice');

const invoiceFixtures = require('/var/lib/core/integration_fixtures/invoice');
const pendingTransactionFixtures = require('/var/lib/core/integration_fixtures/pendingTransaction');

const today = new Date();

class InvoiceFixtures {
  validXml(version = '32') {
    return factory.validXml(version);
  }

  validXmlLongStamp(version = '32') {
    return factory.validXmlLongStamp(version);
  }

  invalidXml(version = '32') {
    return factory.invalidXml(version);
  }

  wrongCompany(version = '32') {
    return factory.wrongCompany(version);
  }

  wrongClient(version = '32') {
    return factory.wrongClient(version);
  }

  sameClientCompanyRfc(version = '32') {
    return factory.sameClientCompanyRfc(version);
  }

  invalidNombreV33() {
    return factory.invalidNombreV33();
  }

  invalidAddenda() {
    return factory.invalidAddenda();
  }

  notXml() {
    return factory.notXml();
  }

  validBulkApproveData() {
    return {
      bank_report_id: 1000,
      pending_transactions: [
        { id: 50032 },
        { id: 50034 }
      ]
    };
  }

  validInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-UNIQUE' }).id;
  }

  validPublishInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-PUBLISH' }).id;
  }

  invalidInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-INVALID' }).id;
  }

  validRejectInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-REJECT' }).id;
  }

  validFundEstimateInInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-INVOICEESTIMATE' }).id;
  }

  validFundEstimateInCompanyId() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-COMPANYESTIMATE' }).id;
  }

  validEstimateCompanyId() {
    return _.find(invoiceFixtures, { uuid: '97c63627-0965-4a11-8012-d8b65aa9a757' }).id;
  }

  validInvoiceFromOtherCxc() {
    return _.find(invoiceFixtures, { uuid: 'b9d7628a-09d3-44bf-9407-d5466d0a70f2' }).id;
  }

  invalidFundEstimateId() {
    return _.find(invoiceFixtures, { uuid: '97c63627-0965-5a21-8012-d8b65ba9a757' }).id;
  }

  expiredInvoice() {
    return _.find(invoiceFixtures, { uuid: 'aae9d77c-b824-4050-981b-736092bbccee' });
  }

  validFundRequestInvoiceId() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d78' }).id;
  }

  validFundRequestInvoiceIdForFideicomiso() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d11' }).id;
  }

  validFundRequestInvoiceIdForFideicomisoTotal() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d11' }).total;
  }

  validFundRequestInvoiceIdForExactAmoun() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d79' }).id;
  }

  yesterdayFundRequestInvoice() {
    return _.find(invoiceFixtures, { uuid: '3be0321d-7c13-409e-b1f7-52a94f9a8c20' });
  }

  validFundRequestInvoiceTotal() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d78' }).total;
  }

  validFundRequestInvoiceTotalForExactAmount() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d79' }).total;
  }

  validTooExpensiveFundRequestInvoiceId() {
    return _.find(invoiceFixtures, { uuid: 'b9a2ed62-e43e-4cdf-bd6e-c6cc2e6d9d78' }).id;
  }

  validFundRequestedInvoice() {
    return _.find(invoiceFixtures, { uuid: 'b9a5ed62-e43e-4ccf-bd5e-d6cf2d6d9d78' }).id;
  }

  validMoralFundRequestedInvoice() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e43e-4ccf-bd5e-d6cf2d6d9d78' }).id;
  }

  validInvoicePaymentSummary() {
    return _.find(invoiceFixtures, { uuid: '3be0322d-2a14-408e-b1f7-52a94f9a8c20' }).id;
  }

  validApprovedInvoice() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-COMPANYESTIMATE' }).id;
  }

  validFundedInvoiceWithZeroReserveId() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d78' }).id;
  }

  validInvoiceSummary() {
    const values = _.find(invoiceFixtures, { uuid: '3be0322d-2a14-408e-b1f7-52a94f9a8c20' });

    return {
      payment_summary: {
        iva: 16000,
        total: 100000,
        interest: 2458.33,
        commission: 81.94,
        reserve: 10000,
        fund_payment: 87459.73,
        expiration_payment: 10000,
        operation_cost: 2540.27,
        fund_total: 97459.73 },
      financial_summary: {
        fund_date: values.fund_date.toString(),
        expiration: values.expiration.toString(),
        operation_term: 59,
        gain: 2458.33,
        gain_percentage: 2.46,
        annual_gain: 15
      }
    };
  }

  operationCost() {
    return factory.operationCost(100000, 2540.28, 97459.72);
  }

  invalidInvoiceCXP() {
    return _.find(invoiceFixtures, { uuid: '97c63627-0965-5a21-8012-d8b65ba9a753' });
  }

  validInvoiceCXP() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-UNIQUE' });
  }

  validMarketInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '97c63625-0963-4a11-8012-d8b65aa9a757' }).id;
  }

  invalidMarketInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '97c63625-0965-4a11-8012-d8b65aa9a757' }).id;
  }

  validFundedInvoiceId() {
    return _.find(invoiceFixtures, { uuid: '11j53625-9264-9h55-0742-d8b60eh3g0392' }).id;
  }

  validExpiredInvoiceId() {
    return _.find(invoiceFixtures, { uuid: 'k02s3625-9264-9h55-2832-d8b60eh0sr233' }).id;
  }

  fundedInvoices() {
    return _.filter(invoiceFixtures, { investor_rfc: 'AAZJ87184749069' });
  }

  unexcitingInvoiceId() {
    return 0;
  }


  validInvoiceIdForPendingTransaction() {
    return _.find(pendingTransactionFixtures, { id: 50006 }).data.invoice_id;
  }

  unexcitingPendingTransactionId() {
    return 999999;
  }

  unexcitingInvoiceIdForPendingTransaction() {
    return _.find(pendingTransactionFixtures, { id: 50007 }).data.invoice_id;
  }

  validNotRelatedToInvestorInvoiceId() {
    return _.find(pendingTransactionFixtures, { id: 50008 }).data.invoice_id;
  }

  validPendingTransactionFundToReject() {
    return _.find(pendingTransactionFixtures, { id: 50011 });
  }

  investorFundEstimate() {
    return factory.investorFundEstimate(100000, 3708.33, 370.83, 103337.5);
  }

  physicalInvestorFundEstimate() {
    return factory.physicalInvestorFundEstimate(100000, 3708.33, 741.67, 250, 102716.66);
  }

  investorProfitEstimate() {
    return factory.investorProfitEstimate(3708.33, 3.71, 15);
  }

  validPublishedIdToReject() {
    return _.find(invoiceFixtures, { uuid: 'd9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78' }).id;
  }

  validPublishedIdToRejectForCxc() {
    return _.find(invoiceFixtures, { uuid: 'e9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78' }).id;
  }

  validFundRequestedIdToReject() {
    return _.find(invoiceFixtures, { uuid: 'f9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78' }).id;
  }

  validFundedInvoiceIdToPayWithJPGReceipt() {
    return _.find(invoiceFixtures, { uuid: '28h53625-0963-8s44-8012-d8b05fr9a757' }).id;
  }

  validFundedInvoiceIdToPayWithPNGReceipt() {
    return _.find(invoiceFixtures, { uuid: '93h53625-0963-9h55-8012-d8b60eh4a757' }).id;
  }

  validFundedInvoiceIdToPayWithPDFReceipt() {
    return _.find(invoiceFixtures, { uuid: '97k28425-0963-4a11-8012-d8b65ls8e757' }).id;
  }

  validFundedInvoiceIdToPayWithoutReceipt() {
    return _.find(invoiceFixtures, { uuid: '83j60371-1750-0a63-0166-d8b65ls8k825' }).id;
  }

  validPaymentInProcessInvoiceIdWithReceipt() {
    return _.find(invoiceFixtures, { uuid: '94f53625-9361-9h55-0825-d8b60eh4g926' }).id;
  }

  validPaymentInProcessInvoiceIdWithoutReceipt() {
    return _.find(invoiceFixtures, { uuid: '50d68425-1750-0a63-8012-d8b65ls8b820' }).id;
  }

  validAdminInvoiceDetailWithoutExpirationDate() {
    return _.find(invoiceFixtures, { uuid: '3be0322d-1b13-408e-b1f7-52a94f9a8c20' }).id;
  }

  validAdminInvoiceDetailWithoutInvestor() {
    return _.find(invoiceFixtures, { uuid: 'e9a2ed62-e43f-3cdf-bd6e-d6cc1a6d7d78' }).id;
  }

  validAdminInvoiceDetail() {
    return _.find(invoiceFixtures, { uuid: '3be0322d-2a14-408e-b1f7-52a94f9a8c20' }).id;
  }

  approvedInvoiceWithSuspendedCXP() {
    return _.find(invoiceFixtures, { uuid: '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-SUSPENDEDCXP' }).id;
  }

  validPaymentInProcessId() {
    return _.find(invoiceFixtures, { uuid: '02h63625-9264-9h55-0742-d8b60eh4s825' }).id;
  }

  validPaymentInProcessInvoice() {
    return _.find(invoiceFixtures, { uuid: '02h63625-9264-9h55-0742-d8b60eh4s825' });
  }

  validPaymentDueInvoice() {
    return _.find(invoiceFixtures, { uuid: '97b63675-1323-1c31-8012-b7b65ca9a757' }).id;
  }

  validPaymentDueToLatePaymentInvoice() {
    return _.find(invoiceFixtures, { uuid: '93b6a675-1323-1c31-8012-b7b65ca9a757' }).id;
  }

  validInvoiceCompletedRequest(remove) {
    const data = {
      cxp_payment: 100,
      cxp_payment_date: today,
      cxc_payment: 30,
      investor_payment: 50,
      fondeo_payment_date: today
    };

    return _.omit(data, remove);
  }

  validInvoiceLostData(remove) {
    const lostData = {
      cxc_payment: 30,
      investor_payment: 50,
      payment_date: today
    };

    const data = _.omit(lostData, remove);

    return data;
  }

  invalidInvoiceLostNegativeInvestorPayment() {
    return {
      cxc_payment: 30,
      investor_payment: -50,
      payment_date: today
    };
  }

  invalidInvoiceLostNegativeCxcPayment() {
    return {
      cxc_payment: -30,
      investor_payment: 50,
      payment_date: today
    };
  }

  invalidInvoiceLostDate() {
    return {
      cxc_payment: 30,
      investor_payment: 50,
      payment_date: 'today'
    };
  }

  validInvoiceLatePaymentData(remove) {
    const lostData = {
      cxc_payment: 30,
      investor_payment: 50,
      cxp_payment: 70,
      fondeo_payment_date: today,
      cxp_payment_date: today
    };

    const data = _.omit(lostData, remove);

    return data;
  }

  invalidInvoiceLatePaymentNegativeInvestorPayment() {
    return {
      cxc_payment: 30,
      investor_payment: -50,
      cxp_payment: 70,
      fondeo_payment_date: today,
      cxp_payment_date: today
    };
  }

  invalidInvoiceLatePaymentNegativeCxcPayment() {
    return {
      cxc_payment: -30,
      investor_payment: 50,
      cxp_payment: 70,
      fondeo_payment_date: today,
      cxp_payment_date: today
    };
  }

  invalidInvoiceLatePaymentNegativeCxpPayment() {
    return {
      cxc_payment: 30,
      investor_payment: 50,
      cxp_payment: -70,
      payment_date: today,
      cxp_payment_date: today
    };
  }

  invalidInvoiceLatePaymentDate() {
    return {
      cxc_payment: 30,
      investor_payment: 50,
      cxp_payment: 70,
      fondeo_payment_date: 'today',
      cxp_payment_date: today
    };
  }

  invalidInvoiceLatePaymentCxpDate() {
    return {
      cxc_payment: 30,
      investor_payment: 50,
      cxp_payment: 70,
      fondeo_payment_date: today,
      cxp_payment_date: 'today'
    };
  }

  validPageSizeLimit() {
    return {
      page_size: 2,
      page: 0
    };
  }

  invalidPageSizeLimit() {
    return {
      page_size: -1,
      page: 0,
      order_by: 'status'
    };
  }

  invalidPage() {
    return {
      page_size: 2,
      page: -1,
      order_by: 'status'
    };
  }

  invalidOrderBy() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'email'
    };
  }

  invalidOrderDesc() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'status',
      order_desc: 'something'
    };
  }

  validOrderBy() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'client_name',
      order_desc: false
    };
  }

  validOrderDesc() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'client_name',
      order_desc: true
    };
  }

  validClientName() {
    return {
      client_name: 'plex'
    };
  }

  validCompanyName() {
    return {
      company_name: 'CompanyUsers'
    };
  }

  validInvestorName() {
    return {
      investor_name: 'Zensure'
    };
  }

  validTotal() {
    return {
      min_total: 80,
      max_total: 100
    };
  }

  invalidMaxTotal() {
    return {
      min_total: 80,
      max_total: 'something'
    };
  }

  invalidMinTotal() {
    return {
      min_total: 'something',
      max_total: 100
    };
  }

  validDateRange() {
    const start_date = new Date();
    const end_date = new Date();

    start_date.setHours(0, 0, 0, 0);
    start_date.setDate(start_date.getDate());
    end_date.setHours(0, 0, 0, 0);
    end_date.setDate(end_date.getDate() + 89);

    return {
      start_date,
      end_date
    };
  }

  validExpirationDateRange() {
    const { start_date, end_date } = this.validDateRange();

    return {
      start_expiration_date: start_date,
      end_expiration_date: end_date
    };
  }

  validFundDateRange() {
    const { start_date, end_date } = this.validDateRange();

    return {
      start_fund_date: start_date,
      end_fund_date: end_date
    };
  }

  noOneMatchDateRange() {
    const date = new Date();

    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() + 1);

    return {
      start_date: date,
      end_date: date
    };
  }

  invalidStartDate() {
    const end_date = new Date();

    end_date.setHours(0, 0, 0, 0);
    end_date.setDate(end_date.getDate() + 89);

    return {
      start_date: 'something',
      end_date
    };
  }

  invalidEndDate() {
    const start_date = new Date();

    start_date.setHours(0, 0, 0, 0);
    start_date.setDate(start_date.getDate() + 15);

    return {
      start_date,
      end_date: 'something'
    };
  }

  validOperationTerm() {
    const invoice = _.find(invoiceFixtures, { uuid: 'b9a6ed22-e43e-4ccf-bd5e-d6cf2d6d9d78' });
    const fundDay = invoice.fund_date;
    const expiration = invoice.expiration;
    const oneDay = 24 * 60 * 60 * 1000;

    fundDay.setHours(0, 0, 0, 0);
    expiration.setHours(0, 0, 0, 0);

    return Math.round(Math.abs((expiration.getTime() - fundDay.getTime()) / oneDay));
  }

  invalidFundStartDate() {
    const { start_date, end_date } = this.invalidStartDate();

    return {
      start_fund_date: start_date,
      end_fund_date: end_date
    };
  }

  invalidFundEndDate() {
    const { start_date, end_date } = this.invalidEndDate();

    return {
      start_fund_date: start_date,
      end_fund_date: end_date
    };
  }

  invalidExpirationStartDate() {
    const { start_date, end_date } = this.invalidStartDate();

    return {
      start_expiration_date: start_date,
      end_expiration_date: end_date
    };
  }

  invalidExpirationEndDate() {
    const { start_date, end_date } = this.invalidEndDate();

    return {
      start_expiration_date: start_date,
      end_expiration_date: end_date
    };
  }

  validStatusArray() {
    return {
      status: [
        'approved',
        'rejected'
      ]
    };
  }

  invalidStatusArray() {
    return {
      status: [ 'invalid' ]
    };
  }

  totalInvoicesCount() {
    // Added padding to consider invoices created by tests
    return invoiceFixtures.length + 50;
  }

  validFundedInvoiceWithFirstScenarioFee() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d80' }).id;
  }

  validFundedInvoiceWithSecondScenarioFee() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d81' }).id;
  }

  validFundedInvoiceWithThirdScenarioFee() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d82' }).id;
  }

  validFundedInvoiceWithFideicomisoFixedFee() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d94' }).id;
  }

  validFundedInvoiceWithFideicomisoVariableFee() {
    return _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d93' }).id;
  }

  validAdminInvoiceFirstScenarioDetail() {
    return {
      operation_summary:
      { fund_date: 'Wed Jun 20 2018 00:00:00 GMT+0000 (UTC)',
        expiration: 'Mon Sep 17 2018 00:00:00 GMT+0000 (UTC)',
        operation_term: 89,
        commission: 30.9,
        fee: 92.71,
        earnings_fd: 123.61
      },
      cxc_payment:
      { annual_cost: 15,
        interest: 927.08,
        interest_percentage: 3.71,
        reserve: 2500,
        reserve_percentage: 10,
        fd_commission: 30.9,
        fd_commission_percentage: 0.12,
        total: 25000,
        fund_payment: 21542.02,
        expiration_payment: 2500
      },
      investor_payment:
      { investor_name: 'Zum Zum',
        fund_date: 'Wed Jun 20 2018 00:00:00 GMT+0000 (UTC)',
        fund_total: 25000,
        earnings: 927.08,
        earnings_percentage: 3.71,
        gain: 834.37,
        gain_percentage: 3.34,
        fee: 92.71,
        fee_percentage: 0.37,
        isr: 0,
        total_payment: 25834.37,
        include_isr: false
      }
    };
  }

  validAdminInvoiceSecondScenarioDetail() {
    return {
      operation_summary:
      { fund_date: 'Wed Jun 20 2018 00:00:00 GMT+0000 (UTC)',
        expiration: 'Mon Sep 17 2018 00:00:00 GMT+0000 (UTC)',
        operation_term: 89,
        commission: 61.81,
        fee: 350,
        earnings_fd: 411.81
      },
      cxc_payment:
      { annual_cost: 15,
        interest: 1854.17,
        interest_percentage: 3.71,
        reserve: 5000,
        reserve_percentage: 10,
        fd_commission: 61.81,
        fd_commission_percentage: 0.12,
        total: 50000,
        fund_payment: 43084.02,
        expiration_payment: 5000
      },
      investor_payment:
      { investor_name: 'Zum Zum',
        fund_date: 'Wed Jun 20 2018 00:00:00 GMT+0000 (UTC)',
        fund_total: 50000,
        earnings: 1854.17,
        earnings_percentage: 3.71,
        gain: 1504.17,
        gain_percentage: 3.01,
        fee: 350,
        fee_percentage: 0.7,
        isr: 0,
        total_payment: 51504.17,
        include_isr: false
      }
    };
  }

  validAdminInvoiceThirdScenarioDetail() {
    return {
      operation_summary:
      { fund_date: 'Wed Jun 20 2018 00:00:00 GMT+0000 (UTC)',
        expiration: 'Mon Sep 17 2018 00:00:00 GMT+0000 (UTC)',
        operation_term: 89,
        commission: 185.42,
        fee: 556.25,
        earnings_fd: 741.67
      },
      cxc_payment:
      { annual_cost: 15,
        interest: 5562.5,
        interest_percentage: 3.71,
        reserve: 15000,
        reserve_percentage: 10,
        fd_commission: 185.42,
        fd_commission_percentage: 0.12,
        total: 150000,
        fund_payment: 129252.08,
        expiration_payment: 15000
      },
      investor_payment:
      { investor_name: 'Zum Zum',
        fund_date: 'Wed Jun 20 2018 00:00:00 GMT+0000 (UTC)',
        fund_total: 150000,
        earnings: 5562.5,
        earnings_percentage: 3.71,
        gain: 5006.25,
        gain_percentage: 3.34,
        fee: 556.25,
        fee_percentage: 0.37,
        isr: 0,
        total_payment: 155006.25,
        include_isr: false
      }
    };
  }

  validInvoicesToBulkDelete() {
    return { invoices: [ { id: 50040 }, { id: 50041 } ] };
  }


  invalidInvoicesToBulkDelete() {
    return { invoices: [ { id: 50037 }, { id: 50038 } ] };
  }

  validFideicomisoFirstCaseInvoiceDetail() {
    return {
      operation_summary: {
        fee: 350
      },
      investor_payment: {
        fideicomiso_fee: '150.00',
        fideicomiso_fee_percentage: '0.30'
      }
    };
  }

  validFideicomisoSecondCaseInvoiceDetail() {
    return {
      operation_summary: {
        fee: 92.71
      },
      investor_payment: {
        fideicomiso_fee: '27.81',
        fideicomiso_fee_percentage: '0.11'
      }
    };
  }

  investorFundEstimateFirstScenario() {
    return {
      total: 25000,
      earnings: 927.08,
      commission: 92.71,
      perception: 25834.37,
      isr: 0,
      include_isr: false
    };
  }

  investorFundEstimateSecondScenario() {
    return {
      total: 50000,
      earnings: 1854.17,
      commission: 350,
      perception: 51504.17,
      isr: 0,
      include_isr: false
    };
  }

  investorFundEstimateThirdScenario() {
    return {
      total: 150000,
      earnings: 5562.5,
      commission: 556.25,
      perception: 155006.25,
      isr: 0,
      include_isr: false
    };
  }

  investorFundEstimateFideicomisoFixedFee() {
    return {
      total: 50000,
      earnings: 1854.17,
      commission: 500,
      perception: 51354.17,
      isr: 0,
      include_isr: false
    };
  }

  investorFundEstimateFideicomisoVariableFee() {
    return {
      total: 25000,
      earnings: 927.08,
      commission: 120.52,
      perception: 25806.56,
      isr: 0,
      include_isr: false
    };
  }

  mixedAvailableInvoicesRequest() {
    const data = {};
    const uuids = [ '8c309c42-27a8-4311-8604-2b1f0fcbe154', 'a55a6cdd-53cd-4495-af3a-40026b1b1990',
      'f5a4445a-d187-4416-b68f-ff66a96fb45d' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = true;

    return data;
  }

  availableInvoicesFundRequest() {
    const data = {};
    const uuids = [ '8c309c42-27a8-4311-8604-2b1f0fcbe154', 'a55a6cdd-53cd-4495-af3a-40026b1b1990' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = true;

    return data;
  }

  anotherAvailableInvoicesFundRequest() {
    const data = {};
    const uuids = [ 'c9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = true;

    return data;
  }

  oneAvailableInvoicesFundRequest() {
    const data = {};
    const uuids = [ '02h63625-9264-9h55-0742-d8b60eh4s825' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = true;

    return data;
  }

  availableInvoicesRequest() {
    const data = {};
    const uuids = [ '8c309c42-27a8-4311-8604-2b1f0fcbe154', 'a55a6cdd-53cd-4495-af3a-40026b1b1990' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = false;

    return data;
  }

  unavailableInvoicesRequest() {
    const data = {};
    const uuids = [ 'f5a4445a-d187-4416-b68f-ff66a96fb45d' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = false;

    return data;
  }

  sameAmountInvoicesRequest() {
    const data = {};
    const uuids = [ 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d79', 'b9a2ed62-e43e-4cdf-bd5e-c6cf2d6d9d78' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = true;

    return data;
  }

  notEnoughBalanceRequest() {
    const data = {};
    const uuids = [ 'b9a2ed62-e43e-4cdf-bd6e-c6cc2e6d9d78' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);
    data.fund = true;

    return data;
  }

  validPublishInvoicesRequest() {
    const data = {};
    const uuids = [ '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-PUBLISH', 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d97' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }

  oneValidPublishInvoiceRequest() {
    const data = {};
    const uuids = [ 'c9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78', '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-PUBLISH' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }

  invalidDatePublishInvoiceRequest() {
    const data = {};
    const uuids = [ '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-INVALIDDATETOPUBLISH' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }

  invalidPublishInvoicesRequest() {
    const data = {};
    const uuids = [ 'c9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }

  unrelatedInvoiceRequest() {
    const data = {};
    const uuids = [ '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-INVALID' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }

  suspendedCxpPublishInvoiceRequest() {
    const data = {};
    const uuids = [ '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-SUSPENDEDCXP' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }

  validApproveInvoicesRequest() {
    const data = {};
    const uuids = [ '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-UNIQUE', '7A6A559B-CFC0-47D5-8B00-AB45A0D8DFF7-APPROVE' ];

    data.invoices = factory.invoicesAvailability(invoiceFixtures, uuids);

    return data;
  }
}

module.exports = new InvoiceFixtures();
