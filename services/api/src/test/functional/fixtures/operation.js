
class OperationFixtures {
  someOperationId() {
    return 50124;
  }

  nonExistentOperationId() {
    return 0;
  }
  totalOperationsCount() {
    return 152;
  }
  validMoralFundRequestedOperation() {
    return 50058;
  }

  validPageSizeLimit() {
    return {
      page_size: 2,
      page: 0
    };
  }

  invalidOrderBy() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'email'
    };
  }

  invalidPageSizeLimit() {
    return {
      page_size: -1,
      page: 0,
      order_by: 'status'
    };
  }

  invalidOrderDesc() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'status',
      order_desc: 'something'
    };
  }

  validOrderBy() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'client_name',
      order_desc: false
    };
  }

  validClientName() {
    return {
      client_name: 'plex'
    };
  }
  validOrderDesc() {
    return {
      page_size: 2,
      page: 0,
      order_by: 'client_name',
      order_desc: true
    };
  }
  validCompanyName() {
    return {
      company_name: 'CompanyUsers'
    };
  }

  validInvestorName() {
    return {
      investor_name: 'Zensure'
    };
  }

  validStatusArray() {
    return {
      status: [
        'fund_requested',
        'funded'
      ]
    };
  }
}

module.exports = new OperationFixtures();
