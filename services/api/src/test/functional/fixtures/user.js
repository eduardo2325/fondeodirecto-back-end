const factory = require('../factories/user');
const tokenFixtures = require('/var/lib/core/integration_fixtures/token');
const userFixtures = require('/var/lib/core/integration_fixtures/user');
const _ = require('lodash');

class UserFixtures {
  validRegistrationToken() {
    const token = _.find(tokenFixtures, { type: 'registration', token: 'validtoken' });

    return token;
  }

  validDeletedRegistrationToken() {
    const token = _.find(tokenFixtures, { type: 'registration', token: 'deleteduser' });

    return token;
  }

  deletableRegistrationToken() {
    const token = _.find(tokenFixtures, { type: 'registration', token: 'deletetoken' });

    return token;
  }

  invalidRegistrationToken() {
    return {
      token: '0355936221',
      email: '0987654321'
    };
  }

  unapprovedCompanyUser() {
    const user = _.find(userFixtures, { name: 'Unapproved company user' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validUser() {
    const user = _.find(userFixtures, { name: 'validuser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validDeletedUser() {
    const user = _.find(userFixtures, { name: 'DeletedCxpUser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validAdminUser() {
    return factory.user('admin', '12345678', 'dev@fondeodirecto.com');
  }

  validCxcUser() {
    const user = _.find(userFixtures, { name: 'CxcUser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validCxcUserTwo() {
    const user = _.find(userFixtures, { name: 'ChangePasswordUser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  suspendedCXCUser() {
    const user = _.find(userFixtures, { name: 'SuspendedCXC' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validCxpUser() {
    const user = _.find(userFixtures, { name: 'CxpUser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }


  validCxpUserCase2() {
    const user = _.find(userFixtures, { name: 'Lorem CxP Dos User' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validCxpUser2() {
    const user = _.find(userFixtures, { name: 'CpcUser2' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  unexistentExcelReportId() {
    return 9999;
  }

  validPendingTransactionsForExcelBank() {
    return {
      // todo: fixtures
      pending_transactions: [
        { id: 50011 },
        { id: 50009 }
      ]
    };
  }

  validBankReportId() {
    return 1;
  }

  suspendedCXPUser() {
    const user = _.find(userFixtures, { name: 'SuspendedCXP' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validInvestorUser() {
    const user = _.find(userFixtures, { name: 'investorUser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }


  validInvestorUserCase2() {
    const user = _.find(userFixtures, { name: 'Lorem Investor Moral' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validFideicomisoInvestorUser() {
    const user = _.find(userFixtures, { name: 'Fideicomiso Investor' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  exactAmountInvestorUser() {
    const user = _.find(userFixtures, { name: 'exactAmountFundInvestor' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  fourMonthsInvestorUser() {
    const user = _.find(userFixtures, { name: 'Four Months Investor' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  suspendedInvestorUser() {
    const user = _.find(userFixtures, { name: 'SuspendedINVESTOR' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validPhysicalInvestorUser() {
    const user = _.find(userFixtures, { name: 'physicalInvestor' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }


  zeroBalanceInvestor() {
    const user = _.find(userFixtures, { name: 'zeroBalanceInvestor' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  invalidUser() {
    return factory.user('unknown', '', 'unknown@fondeodirecto.com');
  }

  deletableUser() {
    const user = _.find(userFixtures, { name: 'deleteuser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  deletableUserWithSession() {
    const user = _.find(userFixtures, { name: 'deleteuserWithSession' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validResendUser() {
    const user = _.find(userFixtures, { name: 'validuser' });

    return factory.user(user.name, user.decrypted_password, user.email);
  }

  validResendToken() {
    const token = _.find(tokenFixtures, { type: 'registration', token: 'resendToken' });

    return token;
  }

  wrongPasswordConfirmation(reset) {
    let user = _.find(userFixtures, { name: 'ChangePasswordUser' });

    const method = reset ? 'resetPassword' : 'changePassword';
    const params = {
      resetPassword: [
        'asdfqwer1234'
      ],
      changePassword: [
        user.decrypted_password,
        'asdfqwer1234'
      ]
    };

    user = factory[method].apply(null, params[method]);
    user.confirmation_password = 'something else';

    return user;
  }

  wrongActualPassword() {
    return factory.changePassword('not the real password', 'asdfqwer1234');
  }

  validNewPasswordData(reset) {
    const user = _.find(userFixtures, { name: 'ChangePasswordUser' });
    const method = reset ? 'resetPassword' : 'changePassword';
    const params = {
      resetPassword: [
        'asdfqwer1234'
      ],
      changePassword: [
        user.decrypted_password,
        'asdfqwer1234'
      ]
    };

    return factory[method].apply(null, params[method]);
  }

  validChangePasswordUser() {
    return _.find(userFixtures, { name: 'ChangePasswordUser' });
  }

  wrongEmail() {
    return { email: 'anemailthatdoesnotexist@fondeodirecto.com' };
  }

  expiredRecoverPasswordToken() {
    const token = _.find(tokenFixtures, { type: 'recover_password', token: 'expiredPasswordToken' });

    return token;
  }

  validRecoverPasswordToken() {
    const token = _.find(tokenFixtures, { type: 'recover_password', token: 'changePasswordToken' });

    return token;
  }
}

module.exports = new UserFixtures();
