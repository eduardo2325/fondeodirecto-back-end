const _ = require('lodash');

const companyFixtures = require('/var/lib/core/integration_fixtures/company');
const transactionFixtures = require('/var/lib/core/integration_fixtures/pendingTransaction');
const existingUser = require('../fixtures/session').credential();
const moment = require('/var/lib/core/js/moment');
const oneMonthAgo = new Date();
const twoMonthsAgo = new Date();
const today = new Date();

oneMonthAgo.setHours(0, 0, 0, 0);
oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1, 3);
twoMonthsAgo.setHours(0, 0, 0, 0);
twoMonthsAgo.setMonth(twoMonthsAgo.getMonth() - 2, 3);

const user = {
  name: 'John Doe',
  email: 'john@fondeodirecto.com',
  type: 'cxc'
};
const operationCost = {
  fd_commission: 1,
  annual_cost: 2,
  reserve: 3
};
const investorOperationCost = {
  fee: 250,
  variable_fee_percentage: 10
};
const companyDataBase = {
  id: 20000,
  rfc: 'XMPL' + Math.random().toString().substr(2, 8),
  name: 'Alcorp',
  business_name: 'Albert Corporation',
  holder: 'Albert Anstein',
  clabe: '002090700355936432',
  description: 'description',
  title_cxc: 'title cxc'
};
const investorData = {
  taxpayer_type: 'moral',
  fee: 5,
  variable_fee_percentage: 10,
  ...companyDataBase
};

const companyData = {
  ...companyDataBase,
  ...operationCost,
  active_cxc: 'false',
  active_cxp: 'true',
  prepayment: true
};

const fideicomisoCompanyDataBase = {
  id: 20001,
  rfc: 'XMPL' + Math.random().toString().substr(2, 8),
  name: 'Fideicomiso name',
  business_name: 'Fideicomiso Corporation',
  holder: 'Fideicomiso Holder',
  clabe: '002090700355936432',
  description: 'description',
  title_cxc: 'title cxc',
  isFideicomiso: true
};

const fideInvestorData = {
  taxpayer_type: 'moral',
  fee: 5,
  variable_fee_percentage: 10,
  fideicomiso_fee: 260,
  fideicomiso_variable_fee: 12,
  ...fideicomisoCompanyDataBase
};

class CompanyFixtures {
  validRfc(isUnique) {
    return isUnique ? `XMPL${(Math.random() + 1).toString().substr(2, 8)}` : 'FODA900806965';
  }

  validCompanyId() {
    return 10038;
  }

  invalidRfc() {
    return 'CUPU800825569';
  }

  invalidCompanyId() {
    return 0;
  }

  validClabe() {
    return '002090700355936431';
  }

  invalidClabe() {
    return '0355936221';
  }

  subtractMonthsToCurrentMoment(nMonths) {
    const subtractedMoment = moment().subtract(nMonths, 'months');

    return {
      month: subtractedMoment.month(),
      year: subtractedMoment.year()
    };
  }

  invitationSend() {
    return {
      name: 'John Doe',
      company_name: 'John Doe Inc.',
      email: 'john-doe-invitation@fondeodirecto.com',
      fee: 230,
      variable_fee_percentage: 13.7
    };
  }

  notValidInvestorInvitationEmail() {
    return {
      token: 'registerInvestorByInvitationToken',
      taxpayer_type: 'moral',
      name: 'John Doe',
      email: 'emailDONOTMATCHToken@fondeodirecto.com',
      password: '123456',
      phone: '123567',
      age: 30,
      address: 'address info',
      rfc: 'XMPL56647600',
      company_name: 'John Doe',
      company_business_name: 'John Doe S.A de C.V'
    };
  }

  moralInvestorByInvitation() {
    return {
      token: 'registerInvestorByInvitationToken',
      taxpayer_type: 'moral',
      name: 'John Doe',
      email: 'investor-invitation@fondeodirecto.com',
      password: '123456',
      phone: '123567',
      age: 30,
      address: 'address info',
      rfc: 'XMPL56647604',
      company_name: 'John Doe',
      company_business_name: 'John Doe S.A de C.V'
    };
  }

  physicalInvestorByInvitation() {
    return {
      token: 'registerPhysicalInvestorByInvitationToken',
      taxpayer_type: 'physical',
      name: 'Physical investor invited',
      email: 'investor-invitation-physical@fondeodirecto.com',
      password: '123456',
      phone: '123567',
      age: 30,
      address: 'address info',
      rfc: 'XMPL56646604',
      company_name: '',
      company_business_name: ''
    };
  }

  investorCompanyFactory(remove) {
    const data = _.omit(investorData, remove);

    if (data.rfc) {
      data.rfc = this.validRfc(true);
    }

    return data;
  }

  companyFactory(remove) {
    const data = _.omit(companyData, remove);

    if (data.rfc) {
      data.rfc = this.validRfc(true);
    }

    return data;
  }

  fideicomisoInvestorCompanyFactory(remove) {
    const data = _.omit(fideInvestorData, remove);

    if (data.rfc) {
      data.rfc = this.validRfc(true);
    }

    return data;
  }

  longCompanyRfc() {
    const data = this.companyFactory();

    data.rfc += 'XTRA';

    return data;
  }

  shortCompanyRfc() {
    const data = this.companyFactory();

    data.rfc = data.rfc.substr(0, 8);

    return data;
  }

  companyWithoutDescription() {
    const data = this.companyFactory([ 'description' ]);

    data.clabe = '002090700755113333';

    return data;
  }

  duplicatedClabeCompany() {
    const data = this.companyFactory();

    data.clabe = this.notUniqueClabe();

    return data;
  }

  companyFactoryWrongRfc() {
    const data = this.companyFactory();

    data.rfc = 'AAZJ87112130000';

    return data;
  }

  companyFactoryInvalidId() {
    const data = this.companyFactory();

    data.id = 0;

    return data;
  }

  companyFactoryWithDecimal() {
    const data = this.companyFactory();

    Object.keys(operationCost).forEach(k => {
      data[k] = data[k].toFixed(2);
    });

    return data;
  }

  validName() {
    return {
      id: 10027,
      name: 'Alcorp2'
    };
  }

  validBusinessName() {
    return {
      id: 10027,
      business_name: 'Albert Corporation2'
    };
  }

  validHolder() {
    return {
      id: 10027,
      holder: 'Albert Anstein2'
    };
  }

  validClabeValue() {
    return {
      id: 10027,
      clabe: '002090700352636131'
    };
  }

  validDescriptionValue() {
    return {
      id: 10027,
      description: 'new description for test'
    };
  }

  validCompanyId() {
    return _.find(companyFixtures, { name: 'Voratak' }).id;
  }

  operationCosts() {
    return operationCost;
  }

  investorOperationCosts() {
    return investorOperationCost;
  }

  companyOperationCost(remove) {
    const data = _.omit(operationCost, remove);

    return { operation_cost: data };
  }

  investorOperationCost(remove) {
    const data = _.omit(investorOperationCost, remove);

    return { operation_cost: data };
  }

  validInvestorId() {
    return _.find(companyFixtures, { name: 'Nexgene' }).id;
  }

  validXmlRecipientId() {
    return _.find(companyFixtures, { name: 'Recipient company' }).id;
  }

  validCompanyWithInvitations() {
    return _.find(companyFixtures, { name: 'CompanyUsersInvitations' }).id;
  }

  validCompanyWithInvitationsAndUsers() {
    return _.find(companyFixtures, { name: 'CompanyUsers' }).id;
  }

  validCompanyWithBalanceNoPendingWithdraw() {
    return _.find(companyFixtures, { name: 'Zum Zum' });
  }

  validCompanyWithBalancePendingWithdraw() {
    const company = _.find(companyFixtures, { name: 'Dogtown' });
    const transactions = _.filter(transactionFixtures, {
      company_id: company.id,
      type: 'WITHDRAW',
      status: 'PENDING'
    });

    company.transactions = transactions;

    return company;
  }

  validInvestorWithPendingTransactions() {
    const company = _.find(companyFixtures, { name: 'Dogtown' });
    const transactions = _.filter(transactionFixtures, i => {
      return i.company_id === company.id && i.status === 'PENDING'
        && i.data.invoice_id === undefined;
    });

    company.transactions = transactions;

    return company;
  }

  validCompanyWithPendingTransactions() {
    const company = _.find(companyFixtures, { name: 'Namebox' });
    const transactions = _.filter(transactionFixtures, {
      company_id: company.id,
      status: 'PENDING'
    });

    company.transactions = transactions;

    return company;
  }

  validInvestorIdWithBalance() {
    return _.find(companyFixtures, { name: 'Zum Zum' }).id;
  }

  exactAmountInvestor() {
    return _.find(companyFixtures, { name: 'Exact Amount' }).id;
  }

  fourMonthsInvestor() {
    return _.find(companyFixtures, { name: 'Four Months Investor' }).id;
  }

  validFideicomisoInvestorIdWithBalance() {
    return _.find(companyFixtures, { name: 'Fideicomiso Investor' }).id;
  }

  validInvestorIdWithBalanceAndPendingTransacitons() {
    return _.find(companyFixtures, { name: 'ERNG' }).id;
  }

  notUniqueClabe() {
    return _.find(companyFixtures, { name: 'Test Directo' }).clabe;
  }

  userUniqueFactory(remove) {
    let data = {
      ...user
    };

    data.email = `reallyreallyuniqueemail+${Math.random()}@fondeodirecto.com`;
    data = _.omit(data, remove);

    return data;
  }

  investorUniqueFactory(remove) {
    const data = this.userUniqueFactory(remove);

    data.email = data.email.replace('+', '_investor+');

    delete data.type;

    return data;
  }

  validCompanyToAddUser() {
    return _.find(companyFixtures, { name: 'AddUserToCompany' }).id;
  }

  validCompanyToAddInvestor() {
    return _.find(companyFixtures, { name: 'AddInvestorToCompany' }).id;
  }

  existingUser() {
    const data = {
      email: existingUser.email,
      name: 'Existing John',
      type: 'cxc'
    };

    return data;
  }

  noSuspensionsCompany() {
    return _.find(companyFixtures, { name: 'No suspensions' });
  }

  suspendedCXCCompany() {
    return _.find(companyFixtures, { name: 'Suspended CXC' });
  }

  suspendCXCRole() {
    return {
      role: 'CXC',
      suspended: true
    };
  }

  unsuspendCXCRole() {
    return {
      role: 'CXC',
      suspended: false
    };
  }

  suspendCXPRole() {
    return {
      role: 'CXP',
      suspended: true
    };
  }

  unsuspendCXPRole() {
    return {
      role: 'CXP',
      suspended: false
    };
  }

  invalidSuspendRole() {
    return {
      role: 'CXX',
      suspended: true
    };
  }

  noSuspensionsInvestor() {
    return _.find(companyFixtures, { name: 'No suspensions INVESTOR' });
  }

  suspendedInvestor() {
    return _.find(companyFixtures, { name: 'Suspended INVESTOR' });
  }

  suspendInvestorRole() {
    return {
      role: 'INVESTOR',
      suspended: true
    };
  }

  unsuspendInvestorRole() {
    return {
      role: 'INVESTOR',
      suspended: false
    };
  }

  proposeCompany(remove) {
    const propose = {
      business_name: 'business_name',
      type: 'Proveedor',
      contact_name: 'contact_name',
      position: 'position',
      email: 'email',
      phone: 'phone'
    };

    const data = _.omit(propose, remove);

    return data;
  }

  validStatementRequest() {
    return {
      year: today.getFullYear(),
      month: oneMonthAgo.getMonth()
    };
  }

  validStatementFideicomisoRequest() {
    return {
      year: today.getFullYear(),
      month: twoMonthsAgo.getMonth()
    };
  }

  invalidStatementRequest(remove) {
    const data = this.validStatementRequest();

    return _.omit(data, remove);
  }

  investorUser() {
    return _.find(companyFixtures, { name: 'Zum Zum' }).id;
  }

  fideicomisoInvestor() {
    return _.find(companyFixtures, { name: 'Fideicomiso Investor' }).id;
  }

  noPrepaymentCompany() {
    return _.find(companyFixtures, { name: 'CxP No Prepayment Company' });
  }

  grantPrepayment() {
    return {
      prepayment: true
    };
  }

  revokePrepayment() {
    return {
      prepayment: false
    };
  }

  prepaymentCompanyData() {
    const prepaymentCompanyData = _.cloneDeep(companyData);

    prepaymentCompanyData.prepayment = true;
    return prepaymentCompanyData;
  }
}

module.exports = new CompanyFixtures();
