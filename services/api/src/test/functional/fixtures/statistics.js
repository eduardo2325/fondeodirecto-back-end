class StatisticsFixtures {
  nonExistentToken() {
    return {
      token: 'nonexistenttoken',
      event_type: 'register',
      statistics_type: 'register-investor-by-invitation'
    };
  }

  validInvestorInvitation() {
    return {
      token: 'registerInvestorStatistics',
      event_type: 'register',
      statistics_type: 'register-investor-by-invitation'
    };
  }
  nonExistentMetric() {
    return {
      token: 'registerInvestorStatistics',
      event_type: 'register',
      statistics_type: 'unexistent-statistic'
    };
  }

  validNewInvoiceNotification() {
    return {
      token: 'newInvoiceStatistics',
      event_type: 'click',
      statistics_type: 'investor-clicked-new-invoice-notification'
    };
  }
}

module.exports = new StatisticsFixtures();
