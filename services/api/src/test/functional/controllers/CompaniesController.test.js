const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const API = require('../helpers/api');
const _ = require('lodash');
const validate = require('../helpers/validate');
const consumer = require('../helpers/consumer');

const errorCodes = require('../../../config/error-codes');
const fixtures = require('../fixtures/company');

const companySchema = require('../schemas/company');
const companyInvestorSchema = require('../schemas/companyInvestor');
const companiesSchema = require('../schemas/companies');

const companyUsersSchema = require('../schemas/company-users');
const newCompanyUserSchema = require('../schemas/new-company-user');
const bankInfoSchema = require('../schemas/bankInfo');
const invitationCreatedSchema = require('../schemas/events/invitation-created');
const companyOperationCostSchema = require('../schemas/companyOperationCost');
const investorOperationCostSchema = require('../schemas/investorOperationCost');
const registerInvestorByInvitationSchema = require('../schemas/registerInvestorByInvitationSchema');
const companyRoleSuspensionUpdated = require('../schemas/events/company-role-suspension-updated');
const investorRoleSuspensionUpdated = require('../schemas/events/investor-role-suspension-updated');
const proposedCompany = require('../schemas/events/proposed-company');
const existsSchema = require('../schemas/exists');
const balanceSchema = require('../schemas/balance');
const errorSchema = require('../schemas/error');
const successSchema = require('../schemas/success');
const investorInvitationCreatedSchema = require('../schemas/investorInvitationCreated');
const investorInvitationCreated = require('../schemas/events/investor-invitation-created');
const investorTransactionsSchema = require('../schemas/investorTransactions');

chai.should();
chai.use(chaiAsPromised);

function reThrow(e) {
  /* istanbul ignore next */
  /* eslint-disable-next-line */
  console.log(`E_FAILURE ${e.message}\n${e.stack.split('\n')[1] || e.toString()}`);
  throw e;
}

describe('functional/Companies controller', () => {
  beforeEach(() => {
    if (!API.isLoggedIn()) {
      return API.login();
    }

    return Promise.resolve();
  });

  describe('create', () => {
    it('should return error if not authenticated', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.createCompany(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.createCompany(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.createCompany(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.createCompany(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if rfc is not included', () => {
      const company = fixtures.companyFactory([ 'rfc' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('rfc');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if rfc is not unique', () => {
      const company = fixtures.companyFactory([ 'rfc' ]);

      company.rfc = fixtures.validRfc();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('rfc');
          errorData.error.code.should.be.eql(errorCodes.Server.Unassigned);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if name is not included', () => {
      const company = fixtures.companyFactory([ 'name' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('name');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if business_name is not included', () => {
      const company = fixtures.companyFactory([ 'business_name' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('business_name');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if clabe is not included', () => {
      const company = fixtures.companyFactory([ 'clabe' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('clabe');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if clabe is not the correct format', () => {
      const company = fixtures.companyFactory();

      company.clabe = fixtures.invalidClabe();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('clabe');
          errorData.error.code.should.be.eql(errorCodes.Company[errorData.error.message]);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if anual_cost is not included', () => {
      const company = fixtures.companyFactory([ 'annual_cost' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('annual_cost');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if reserve is not included', () => {
      const company = fixtures.companyFactory([ 'reserve' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('reserve');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if fd_commission is not included', () => {
      const company = fixtures.companyFactory([ 'fd_commission' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('fd_commission');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if anual_cost is negative', () => {
      const company = fixtures.companyFactory([ 'annual_cost' ]);

      company.annual_cost = -1;

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('annual_cost');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if reserve is negative', () => {
      const company = fixtures.companyFactory([ 'reserve' ]);

      company.reserve = -1;

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('reserve');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if fd_commission is negative', () => {
      const company = fixtures.companyFactory([ 'fd_commission' ]);

      company.fd_commission = -1;

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('fd_commission');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return a company object if there was no issue with the information', () => {
      const company = fixtures.companyFactory();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return an error if rfc is too long', () => {
      const company = fixtures.longCompanyRfc();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('rfc');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if rfc is short', () => {
      const company = fixtures.shortCompanyRfc();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('rfc');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return company object with duplicated clabe', () => {
      const company = fixtures.duplicatedClabeCompany();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return a company object without description', () => {
      const company = fixtures.companyFactory([ 'description' ]);

      return API.createCompany(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return a company object if there was no issue with the information even though are decimals', () => {
      const company = fixtures.companyFactoryWithDecimal();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return a company object with prepayment privilege', () => {
      const company = fixtures.prepaymentCompanyData();

      return API.createCompany(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });
  });

  describe('exists', () => {
    it('should return a exists object false if the company does not exists', () => {
      const rfc = fixtures.invalidRfc();

      return API.companyExists(rfc)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyExists');

          return validate(existsSchema)(responseData.data);
        });
    });

    it('should return a exists object if the company exists', () => {
      const rfc = fixtures.validRfc();

      return API.companyExists(rfc)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyExists');

          return validate(existsSchema)(responseData.data);
        });
    });
  });

  describe('update', () => {
    it('should return error if not authenticated', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.updateCompany(company.id, company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.updateCompany(company.id, company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.updateCompany(company.id, company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const company = fixtures.companyFactory();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.updateCompany(company.id, company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if company does not exists', () => {
      const company = fixtures.companyFactoryInvalidId();

      return API.updateCompany(company.id, company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return a company object updated with the new name', () => {
      const company = fixtures.validName();

      return API.updateCompany(company.id, company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          responseData.data.name.should.be.equal(company.name);

          return validate(companySchema)(responseData.data);
        });
    });

    it('should return a company object updated with the new business name', () => {
      const company = fixtures.validBusinessName();

      return API.updateCompany(company.id, company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          responseData.data.business_name.should.be.equal(company.business_name);

          return validate(companySchema)(responseData.data);
        });
    });

    it('should return a company object updated with the new holder value', () => {
      const company = fixtures.validHolder();

      return API.updateCompany(company.id, company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          responseData.data.holder.should.be.equal(company.holder);

          return validate(companySchema)(responseData.data);
        });
    });

    it('should return a company object updated with the new clabe value', () => {
      const company = fixtures.validClabeValue();

      return API.updateCompany(company.id, company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          responseData.data.clabe.should.be.equal(company.clabe);

          return validate(companySchema)(responseData.data);
        });
    });

    it('should return a company object updated with the new clabe value', () => {
      const company = fixtures.validDescriptionValue();

      return API.updateCompany(company.id, company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          responseData.data.description.should.be.equal(company.description);

          return validate(companySchema)(responseData.data);
        });
    });
  });

  describe('updateCompanyOperationCost', () => {
    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.updateCompanyOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.updateCompanyOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.updateCompanyOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.updateCompanyOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\‘t have the same company role as you requested', () => {
      const id = fixtures.validInvestorId();
      const operationCost = fixtures.companyOperationCost();

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('company');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        }).catch(reThrow);
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();
      const operationCost = fixtures.companyOperationCost();

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('company');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if annual_cost is not present', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost([ 'annual_cost' ]);

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.annual_cost');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        }).catch(reThrow);
    });

    it('should return an error if reserve is not present', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost([ 'reserve' ]);

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.reserve');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if fd_commission is not present', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost([ 'fd_commission' ]);

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.fd_commission');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if annual_cost is negative', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost([ 'annual_cost' ]);

      operationCost.annual_cost = -1;

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.annual_cost');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if reserve is negative', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost([ 'reserve' ]);

      operationCost.reserve = -1;

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.reserve');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if fd_commission is negative', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost([ 'fd_commission' ]);

      operationCost.fd_commission = -1;

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.fd_commission');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return the operation costs', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.companyOperationCost();
      const result = fixtures.operationCosts();

      return API.updateCompanyOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.data.annual_cost.should.be.equal(result.annual_cost);
          responseData.data.reserve.should.be.equal(result.reserve);
          responseData.data.fd_commission.should.be.equal(result.fd_commission);

          return validate(companyOperationCostSchema)(responseData.data);
        }).catch(reThrow);
    });
  });

  describe('updateInvestorOperationCost', () => {
    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.updateInvestorOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.updateInvestorOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.updateInvestorOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.updateInvestorOperationCost())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\‘t have the same company role as you requested', () => {
      const id = fixtures.validCompanyId();
      const operationCost = fixtures.investorOperationCost();

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('company');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();
      const operationCost = fixtures.investorOperationCost();

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('company');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if fee is not present', () => {
      const id = fixtures.validInvestorId();
      const operationCost = fixtures.investorOperationCost([ 'fee' ]);

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.fee');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        }).catch(reThrow);
    });

    it('should return an error if fee is negative', () => {
      const id = fixtures.validInvestorId();
      const operationCost = fixtures.investorOperationCost([ 'fee' ]);

      operationCost.fee = -1;

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.fee');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        }).catch(reThrow);
    });

    it('should return an error if variable_fee_percentage is not present', () => {
      const id = fixtures.validInvestorId();
      const operationCost = fixtures.investorOperationCost([ 'variable_fee_percentage' ]);

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.variable_fee_percentage');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        }).catch(reThrow);
    });

    it('should return an error if variable_fee_percentage is negative', () => {
      const id = fixtures.validInvestorId();
      const operationCost = fixtures.investorOperationCost([ 'variable_fee_percentage' ]);

      operationCost.variable_fee_percentage = -1;

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.path.should.be.equal('operation_cost.variable_fee_percentage');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        }).catch(reThrow);
    });

    it('should return the operation costs', () => {
      const id = fixtures.validInvestorId();
      const operationCost = fixtures.investorOperationCost();
      const result = fixtures.investorOperationCosts();

      return API.updateInvestorOperationCost(id, operationCost)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.data.fee.should.be.equal(result.fee);
          responseData.data.variable_fee_percentage.should.be.equal(result.variable_fee_percentage);

          return validate(investorOperationCostSchema)(responseData.data);
        });
    });
  });

  describe('getCompany', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.invalidCompanyId();

      return API.logout()
        .then(() => API.getCompany(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const id = fixtures.invalidCompanyId();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getCompany(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const id = fixtures.invalidCompanyId();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getCompany(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const id = fixtures.invalidCompanyId();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getCompany(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();

      return API.getCompany(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return a full company if the company exists', () => {
      const id = fixtures.validCompanyId();

      return API.getCompany(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');

          return validate(companySchema)(responseData.data);
        });
    });

    it('should return a full investor company if the company exists', () => {
      const id = fixtures.validInvestorId();

      return API.getCompany(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');

          return validate(companyInvestorSchema)(responseData.data);
        });
    });
  });

  describe('getCompanies', () => {
    let totalCompanies = 0;
    let companiesList = {};

    before(() => {
      return API.login()
        .then(() => {
          return API.getCompanies(100);
        })
        .then(response => {
          const responseData = response.data;

          totalCompanies = responseData.data.total_companies;
          companiesList = _.sortBy(responseData.data.companies, [ 'name' ]);
        });
    });

    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an array of companies with page_size limits', () => {
      return API.getCompanies(2, 0)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.data.companies.length.should.be.equal(2);
          responseData.data.total_companies.should.be.equal(totalCompanies);

          return validate(companiesSchema)(responseData.data);
        });
    });

    it('should return an error if page_size has an invalid value', () => {
      return API.getCompanies(-2, 0, 'name')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if page has an invalid value', () => {
      return API.getCompanies(2, -1, 'name')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if order_by has an invalid value', () => {
      return API.getCompanies(2, 0, 'email')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the order_desc field isn\'t allowed', () => {
      const rfc = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(rfc, 'email', 'something')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.error.path.should.be.equal('order_desc');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an array of companies with companies ordered', () => {
      return API.getCompanies(2, 0, 'name', false)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.data.companies.length.should.be.equal(2);
          responseData.data.companies[0].name.should.be.equal(companiesList[0].name);
          responseData.data.companies[1].name.should.be.equal(companiesList[1].name);
          responseData.data.total_companies.should.be.equal(totalCompanies);

          return validate(companiesSchema)(responseData.data);
        });
    });

    it('should return an array of companies with companies ordered and desc', () => {
      return API.getCompanies(2, 0, 'name', true)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;
          const length = companiesList.length;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.data.companies.length.should.be.equal(2);
          responseData.data.companies[0].name.should.be.equal(companiesList[length - 1].name);
          responseData.data.companies[1].name.should.be.equal(companiesList[length - 2].name);
          responseData.data.total_companies.should.be.equal(totalCompanies);

          return validate(companiesSchema)(responseData.data);
        });
    });

    it('should return an array of companies', () => {
      return API.getCompanies()
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyList');
          responseData.data.companies.length.should.be.not.equal(0);

          return validate(companiesSchema)(responseData.data);
        });
    });
  });

  describe('getInvestorCompanies', () => {
    let totalCompanies = 0;
    let companiesList = {};

    before(() => {
      return API.login()
        .then(() => {
          return API.getInvestorCompanies(100);
        })
        .then(response => {
          const responseData = response.data;

          totalCompanies = responseData.data.total_companies;
          companiesList = _.sortBy(responseData.data.companies, [ 'name' ]);
        });
    });

    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getInvestorCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getInvestorCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getInvestorCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getInvestorCompanies())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an array of companies with page_size limits', () => {
      return API.getInvestorCompanies(2, 0)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.data.companies.length.should.be.equal(2);
          responseData.data.total_companies.should.be.equal(totalCompanies);

          return validate(companiesSchema)(responseData.data);
        });
    });

    it('should return an error if page_size has an invalid value', () => {
      return API.getInvestorCompanies(-2, 0, 'name')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if page has an invalid value', () => {
      return API.getInvestorCompanies(2, -1, 'name')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if order_by has an invalid value', () => {
      return API.getInvestorCompanies(2, 0, 'email')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the order_desc field isn\'t allowed', () => {
      return API.getInvestorCompanies(100, 0, 'user_count', 'something')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.error.path.should.be.equal('order_desc');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an array of companies with companies ordered', () => {
      return API.getInvestorCompanies(2, 0, 'name', false)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.data.companies.length.should.be.equal(2);
          responseData.data.companies[0].name.should.be.equal(companiesList[0].name);
          responseData.data.companies[1].name.should.be.equal(companiesList[1].name);
          responseData.data.total_companies.should.be.equal(totalCompanies);

          return validate(companiesSchema)(responseData.data);
        });
    });

    it('should return an array of companies with companies ordered and desc', () => {
      return API.getInvestorCompanies(2, 0, 'name', true)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;
          const length = companiesList.length;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.data.companies.length.should.be.equal(2);
          responseData.data.companies[0].name.should.be.equal(companiesList[length - 1].name);
          responseData.data.companies[1].name.should.be.equal(companiesList[length - 2].name);
          responseData.data.total_companies.should.be.equal(totalCompanies);

          return validate(companiesSchema)(responseData.data);
        });
    });

    it('should return an array of companies', () => {
      return API.getInvestorCompanies()
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvestorList');
          responseData.data.companies.length.should.be.not.equal(0);

          return validate(companiesSchema)(responseData.data);
        });
    });
  });

  describe('getBankInfo', () => {
    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getBankInfo())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getBankInfo())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getBankInfo())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getBankInfo())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the clabe is invalid', () => {
      const clabe = fixtures.invalidClabe();

      return API.getBankInfo(clabe)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('BankInfo');
          responseData.error.code.should.be.eql(errorCodes.BankInfo[responseData.error.message]);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return a bankInfo if the clabe is valid even though it belongs to a company', () => {
      const clabe = fixtures.notUniqueClabe();

      return API.getBankInfo(clabe)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('BankInfo');

          return validate(bankInfoSchema)(responseData.data);
        });
    });

    it('should return a bankInfo if the clabe is valid', () => {
      const clabe = fixtures.validClabe();

      return API.getBankInfo(clabe)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('BankInfo');

          return validate(bankInfoSchema)(responseData.data);
        });
    });
  });

  describe('clabeExists', () => {
    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getBankInfoByClabe())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getBankInfoByClabe())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getBankInfoByClabe())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getBankInfoByClabe())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the clabe is invalid', () => {
      const clabe = fixtures.invalidClabe();

      return API.getBankInfoByClabe(clabe)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('BankInfo');
          responseData.error.code.should.be.eql(errorCodes.BankInfo[responseData.error.message]);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the clabe is related to another company', () => {
      const clabe = fixtures.notUniqueClabe();

      return API.getBankInfoByClabe(clabe)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('BankInfo');
          responseData.error.code.should.be.eql(errorCodes.BankInfo[responseData.error.message]);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return a bankInfo if the clabe is valid', () => {
      const clabe = fixtures.validClabe();

      return API.getBankInfoByClabe(clabe)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('BankInfo');

          return validate(bankInfoSchema)(responseData.data);
        });
    });
  });

  describe('getCompanyUsers', () => {
    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getCompanyUsers())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getCompanyUsers())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getCompanyUsers())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getCompanyUsers())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();

      return API.getCompanyUsers(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return a company users with only invitations', () => {
      const id = fixtures.validCompanyWithInvitations();

      return API.getCompanyUsers(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.data.users.length.should.be.eql(1);

          return validate(companyUsersSchema)(responseData.data);
        });
    });

    it('should return a complete company users', () => {
      const id = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.data.users.length.should.be.eql(2);

          return validate(companyUsersSchema)(responseData.data);
        });
    });

    it('should return an error if the order_by field isn\'t allowed', () => {
      const id = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(id, 'id', false)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.error.path.should.be.equal('order_by');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the order_desc field isn\'t allowed', () => {
      const id = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(id, 'name', 'something')
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.error.path.should.be.equal('order_desc');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return a list ordered by name', () => {
      const id = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(id, 'name', true)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.data.users.length.should.be.eql(2);
          responseData.data.users[0].email.should.be.eql('invitation@fondeodirecto.com');

          return validate(companyUsersSchema)(responseData.data);
        });
    });

    it('should return a list ordered by email', () => {
      const id = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(id, 'email', false)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.data.users.length.should.be.eql(2);
          responseData.data.users[0].email.should.be.eql('company@fondeodirecto.com');

          return validate(companyUsersSchema)(responseData.data);
        });
    });

    it('should return a list ordered by status', () => {
      const id = fixtures.validCompanyWithInvitationsAndUsers();

      return API.getCompanyUsers(id, 'status', true)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('CompanyUsers');
          responseData.data.users.length.should.be.eql(2);
          responseData.data.users[0].email.should.be.eql('invitation@fondeodirecto.com');

          return validate(companyUsersSchema)(responseData.data);
        });
    });
  });

  describe('getCompanyOperationCost', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.getOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\‘t have the same company role as you requested', () => {
      const id = fixtures.validInvestorId();

      return API.getOperationCost(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();

      return API.getOperationCost(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return the operation costs', () => {
      const id = fixtures.validCompanyId();

      return API.getOperationCost(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');

          return validate(companyOperationCostSchema)(responseData.data);
        });
    });
  });

  describe('getInvestorOperationCost', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.getInvestorOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getInvestorOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getInvestorOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getInvestorOperationCost(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\‘t have the same company role as you requested', () => {
      const id = fixtures.validCompanyId();

      return API.getInvestorOperationCost(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();

      return API.getInvestorOperationCost(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');
          responseData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return the operation costs', () => {
      const id = fixtures.validInvestorId();

      return API.getInvestorOperationCost(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationCost');

          return validate(investorOperationCostSchema)(responseData.data);
        });
    });
  });

  describe('registerInvestor', function() {
    it('should return error if email does not equal to token email', () => {
      const data = fixtures.notValidInvestorInvitationEmail();

      return API.registerInvestor(data)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
          responseData.error.code.should.be.eql(501);

          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return success response on register physical investor', function() {
      const data = fixtures.physicalInvestorByInvitation();

      return API.registerInvestor(data)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');

          return validate(registerInvestorByInvitationSchema)(responseData.data).then(() => {
            return consumer.getNextEvent()
              .then(() => {
                response.status.should.be.equal(200);
              });
          }).catch(reThrow);
        });
    });

    it('should return success response on register moral investor', function() {
      const data = fixtures.moralInvestorByInvitation();

      return API.registerInvestor(data)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');

          return validate(registerInvestorByInvitationSchema)(responseData.data).then(() => {
            return consumer.getNextEvent()
              .then(() => {
                response.status.should.be.equal(200);
              });
          }).catch(reThrow);
        });
    });
  });

  describe('addUser', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.addUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.addUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.addUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.addUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();
      const user = fixtures.userUniqueFactory();

      return API.addUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('User');
          errorData.error.path.should.be.equal('company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(errorData.error);
        }).catch(reThrow);
    });

    it('should return an error if name is not included', () => {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory([ 'name' ]);

      return API.addUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('User');
          errorData.error.path.should.be.equal('name');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if email is not included', () => {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory([ 'email' ]);

      return API.addUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('User');
          errorData.error.path.should.be.equal('email');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if type is not included', () => {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory([ 'type' ]);

      return API.addUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('User');
          errorData.error.path.should.be.equal('type');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if the user already is a member of a company', () => {
      const id = fixtures.validCompanyToAddUser();
      const user = fixtures.existingUser();

      return API.addUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('User');
          errorData.error.path.should.be.equal('email');
          errorData.error.data.company_name.should.be.equal('Fondeo Directo');
          errorData.error.code.should.be.eql(errorCodes.User[errorData.error.message]);

          return validate(errorSchema)(errorData.error);
        });
    });

    /* it('should return a new user structure', () => {
      const rfc = fixtures.validCompanyToAddUser();
      const user = fixtures.userUniqueFactory();

      return API.addUser(rfc, user)
      .should.be.fulfilled
      .then(response => {

        const responseData = response.data;

        response.status.should.be.equal(200);
        responseData.type.should.be.equal('User');
        return consumer.getNextEvent()
        .then(validate(invitationCreatedSchema))
        .then(validate(newCompanyUserSchema)(responseData.data));
      });
    });*/
  });


  describe('getFactorajeAndCashSummaries', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.validInvestorId();

      return API.logout()
        .then(() => API.getFactorajeAndCashSummaries(id, { month: 1, year: 2018 }))
        .should.be.fulfilled
        .then( error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return succesful response on three months ago', function() {
      const id = 11003;
      const requestData = fixtures.subtractMonthsToCurrentMoment(3);

      return API.logout()
        .then(() => API.investorLogin2())
        .then(() => API.getFactorajeAndCashSummaries(id, requestData ))
        .should.be.fulfilled
        .then( response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.data.cash.initial_ammount.should.be.equal(0);
          responseData.data.factoraje.initial_ammount.should.be.equal(0);
          responseData.data.factoraje.acquisitions.should.be.above(0);
          responseData.type.should.be.equal('Investor');
        });
    });

    it('should return succesful response on two months ago', function() {
      const id = fixtures.validInvestorId();
      const requestData = fixtures.subtractMonthsToCurrentMoment(2);

      return API.logout()
        .then(() => API.investorLogin2())
        .then(() => API.getFactorajeAndCashSummaries(id, requestData ))
        .should.be.fulfilled
        .then( response => {
          const responseData = response.data;

          responseData.data.factoraje.initial_ammount.should.be.above(0);
          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
        });
    });

    it('should return succesful response on one month ago', function() {
      const id = fixtures.validInvestorId();
      const requestData = fixtures.subtractMonthsToCurrentMoment(1);

      return API.logout()
        .then(() => API.investorLogin2())
        .then(() => API.getFactorajeAndCashSummaries(id, requestData ))
        .should.be.fulfilled
        .then( response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.data.factoraje.initial_ammount.should.be.equal(0);
          responseData.type.should.be.equal('Investor');
        });
    });

    it('should return error if request data is current month and year', function() {
      const id = fixtures.validInvestorId();
      const requestData = fixtures.subtractMonthsToCurrentMoment(0);

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getFactorajeAndCashSummaries(id, requestData ))
        .should.be.fulfilled
        .then( error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });


    it('should return error if request data is to old from current month and year', function() {
      const id = fixtures.validInvestorId();
      const requestData = fixtures.subtractMonthsToCurrentMoment(4);

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getFactorajeAndCashSummaries(id, requestData ))
        .should.be.fulfilled
        .then( error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if request data is beyond current month and year', function() {
      const id = fixtures.validInvestorId();
      const requestData = fixtures.subtractMonthsToCurrentMoment(-1);

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getFactorajeAndCashSummaries(id, requestData ))
        .should.be.fulfilled
        .then( error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });
  });

  describe('createInvestor', () => {
    it('should return error if not authenticated', function() {
      const company = fixtures.investorCompanyFactory();

      return API.logout()
        .then(() => API.createInvestor(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const company = fixtures.investorCompanyFactory();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.createInvestor(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const company = fixtures.investorCompanyFactory();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.createInvestor(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const company = fixtures.investorCompanyFactory();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.createInvestor(company))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if rfc is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'rfc' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('rfc');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if rfc is not unique', () => {
      const company = fixtures.investorCompanyFactory([ 'rfc' ]);

      company.rfc = fixtures.validRfc();

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('rfc');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if name is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'name' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('name');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if business_name is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'business_name' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('business_name');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if clabe is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'clabe' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('clabe');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if clabe is not the correct format', () => {
      const company = fixtures.investorCompanyFactory();

      company.clabe = fixtures.invalidClabe();

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('clabe');
          errorData.error.code.should.be.eql(errorCodes.Investor[errorData.error.message]);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if fee is a negative value', () => {
      const company = fixtures.investorCompanyFactory([ 'fee' ]);

      company.fee = -1;

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('fee');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if fee is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'fee' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('fee');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if variable_fee_percentage is a negative value', () => {
      const company = fixtures.investorCompanyFactory([ 'variable_fee_percentage' ]);

      company.variable_fee_percentage = -1;

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('variable_fee_percentage');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if variable_fee_percentage is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'variable_fee_percentage' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('variable_fee_percentage');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if taxpayer_type is not included', () => {
      const company = fixtures.investorCompanyFactory([ 'taxpayer_type' ]);

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('taxpayer_type');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if taxpayer_type is not a valid valud', () => {
      const company = fixtures.investorCompanyFactory([ 'taxpayer_type' ]);

      company.taxpayer_type = 'Not valid';

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('taxpayer_type');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return a company object with duplicated clabe', () => {
      const company = fixtures.investorCompanyFactory();

      company.cable = fixtures.notUniqueClabe();

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
          return validate(companySchema)(responseData.data);
        });
    });

    it('should return a company object if there was no issue with the information', () => {
      const company = fixtures.investorCompanyFactory();

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return a fideicomiso company object if there was no issue with the information', () => {
      const company = fixtures.fideicomisoInvestorCompanyFactory();

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
          return validate(companySchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return a company object if there was no issue with the information even though are decimals', () => {
      const company = fixtures.investorCompanyFactory();

      company.fee = 5.3;

      return API.createInvestor(company)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
          return validate(companySchema)(responseData.data);
        });
    });
  });

  describe('addInvestorUser', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.addInvestorUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.addInvestorUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.addInvestorUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.userUniqueFactory();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.addInvestorUser(id, user))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an error if the company doesn\'t exists', () => {
      const id = fixtures.invalidCompanyId();
      const user = fixtures.userUniqueFactory();

      return API.addInvestorUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if name is not included', () => {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.userUniqueFactory([ 'name' ]);

      return API.addInvestorUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('name');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if email is not included', () => {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.userUniqueFactory([ 'email' ]);

      return API.addInvestorUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('email');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an error if the user already is a member of a company', () => {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.existingUser();

      return API.addInvestorUser(id, user)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Investor');
          errorData.error.path.should.be.equal('email');
          errorData.error.data.company_name.should.be.equal('Fondeo Directo');
          errorData.error.code.should.be.eql(errorCodes.Investor[errorData.error.message]);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return a new investor structure', () => {
      const id = fixtures.validCompanyToAddInvestor();
      const user = fixtures.investorUniqueFactory();

      return API.addInvestorUser(id, user)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');
          return consumer.getNextEvent()
            .then(validate(invitationCreatedSchema))
            .then(validate(newCompanyUserSchema)(responseData.data));
        });
    });
  });

  describe('getBalance', () => {
    it('should return error if not authenticated', function() {
      const id = fixtures.validCompanyId();

      return API.logout()
        .then(() => API.getCompanyBalance(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const id = fixtures.validCompanyId();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getCompanyBalance(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const id = fixtures.validCompanyId();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getCompanyBalance(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return not found error if company doesn\'t exist', () => {
      const id = fixtures.invalidCompanyId();

      return API.getCompanyBalance(id)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Balance');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return not found error if company doesn\'t belong to investor user', () => {
      const { id } = fixtures.validCompanyWithBalancePendingWithdraw();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getCompanyBalance(id))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Balance');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout())
        .catch(reThrow);
    });

    it('should return balance minus pending withdraw transactions', () => {
      const { id, balance, transactions } = fixtures.validCompanyWithBalancePendingWithdraw();
      const expectedBalance = transactions.reduce((total, { amount }) => total - amount, balance);

      return API.getCompanyBalance(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Balance');
          responseData.data.total.should.be.eql(expectedBalance.toFixed(2));

          return validate(balanceSchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return total balance if company belongs investor user', () => {
      const { id, balance } = fixtures.validCompanyWithBalanceNoPendingWithdraw();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getCompanyBalance(id))
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Balance');
          responseData.data.total.should.be.eql(balance.toFixed(2));

          return validate(balanceSchema)(responseData.data);
        })
        .then(() => API.logout())
        .catch(reThrow);
    });

    it('should return total balance as admin', () => {
      const { id, balance } = fixtures.validCompanyWithBalanceNoPendingWithdraw();

      return API.getCompanyBalance(id)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Balance');
          responseData.data.total.should.be.eql(balance.toFixed(2));

          return validate(balanceSchema)(responseData.data);
        });
    });
  });

  describe('updateCompanyRoleSuspension', () => {
    it('should return error if not authenticated', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.suspendCXCRole();

      return API.logout()
        .then(() => API.updateCompanyRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if cxc', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.suspendCXCRole();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.updateCompanyRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if cxp', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.suspendCXCRole();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.updateCompanyRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if investor', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.suspendCXCRole();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.updateCompanyRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is invalid', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.invalidSuspendRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role parameter is not provided', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.suspendCXCRole();

      delete request.role;

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if suspended parameter is not provided', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.suspendCXCRole();

      delete request.suspended;

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return not found if company doesn\'t exist', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.suspendCXCRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.path.should.be.equal('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if suspending suspended role', function() {
      const { id } = fixtures.suspendedCXCCompany();
      const request = fixtures.suspendCXCRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.Unassigned);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if unsuspending unsuspended role', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.unsuspendCXCRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.Unassigned);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should suspend cxc role', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.suspendCXCRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Role');

          return consumer.getNextEvent()
            .then(validate(companyRoleSuspensionUpdated))
            .then(event => {
              event.value.body.role.should.be.eql('CXC');
              event.value.body.suspended.should.be.true;
            })
            .then(validate(successSchema)(responseData));
        });
    });

    it('should unsuspend cxc role', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.unsuspendCXCRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Role');

          return consumer.getNextEvent()
            .then(validate(companyRoleSuspensionUpdated))
            .then(event => {
              event.value.body.role.should.be.eql('CXC');
              event.value.body.suspended.should.be.false;
            })
            .then(validate(successSchema)(responseData));
        });
    });

    it('should suspend cxp role', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.suspendCXPRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Role');

          return consumer.getNextEvent()
            .then(validate(companyRoleSuspensionUpdated))
            .then(event => {
              event.value.body.role.should.be.eql('CXP');
              event.value.body.suspended.should.be.true;
            })
            .then(validate(successSchema)(responseData));
        });
    });

    it('should unsuspend cxp role', function() {
      const { id } = fixtures.noSuspensionsCompany();
      const request = fixtures.unsuspendCXPRole();

      return API.updateCompanyRoleSuspension(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Role');

          return consumer.getNextEvent()
            .then(validate(companyRoleSuspensionUpdated))
            .then(event => {
              event.value.body.role.should.be.eql('CXP');
              event.value.body.suspended.should.be.false;
            })
            .then(validate(successSchema)(responseData));
        });
    });
  });

  describe('updateCompanyPrepayment', () => {
    it('should return error if not authenticated', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.grantPrepayment();

      return API.logout()
        .then(() => API.updateCompanyPrepayment(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error unauthorized error if user is cxc', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.grantPrepayment();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.updateCompanyPrepayment(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error unauthorized error if user is cxp', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.grantPrepayment();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.updateCompanyPrepayment(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error unauthorized error if user is investor', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.grantPrepayment();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.updateCompanyPrepayment(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if prepayment parameter is not included', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.grantPrepayment();

      delete request.prepayment;

      return API.updateCompanyPrepayment(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.equal('Company');
          errorData.error.path.should.be.equal('prepayment');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should grant prepayment to a company', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.grantPrepayment();

      return API.updateCompanyPrepayment(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(successSchema)(responseData);
        });
    });

    it('should revoke prepayment to a company', function() {
      const { id } = fixtures.noPrepaymentCompany();
      const request = fixtures.revokePrepayment();

      return API.updateCompanyPrepayment(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');
          return validate(successSchema)(responseData);
        });
    });
  });

  describe('updateInvestorRoleSuspension', () => {
    it('should return error if not authenticated', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.suspendInvestorRole();

      return API.logout()
        .then(() => API.updateInvestorRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error unauthorized error if user is cxc', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.suspendInvestorRole();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.updateInvestorRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error unauthorized error if user is cxp', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.suspendInvestorRole();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.updateInvestorRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error unauthorized error if user is investor', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.suspendInvestorRole();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.updateInvestorRoleSuspension(id, request))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role value is invalid', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.invalidSuspendRole();

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role parameter is not provided', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.suspendInvestorRole();

      delete request.role;

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if suspended parameter is not provided', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.suspendInvestorRole();

      delete request.suspended;

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return not found if investor doesn\'t exist', function() {
      const id = fixtures.invalidCompanyId();
      const request = fixtures.suspendInvestorRole();

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.path.should.be.equal('Investor');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if suspending suspended role', function() {
      const { id } = fixtures.suspendedInvestor();
      const request = fixtures.suspendInvestorRole();

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.Unassigned);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if unsuspending unsuspended role', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.unsuspendInvestorRole();

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Server.Unassigned);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should suspend investor', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.suspendInvestorRole();

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Role');

          return consumer.getNextEvent()
            .then(validate(investorRoleSuspensionUpdated))
            .then(event => {
              event.value.body.suspended.should.be.true;
            })
            .then(validate(successSchema)(responseData));
        });
    });

    it('should unsuspend investor role', function() {
      const { id } = fixtures.noSuspensionsInvestor();
      const request = fixtures.unsuspendInvestorRole();

      return API.updateInvestorRoleSuspension(id, request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Role');

          return consumer.getNextEvent()
            .then(validate(investorRoleSuspensionUpdated))
            .then(event => {
              event.value.body.suspended.should.be.false;
            })
            .then(validate(successSchema)(responseData));
        });
    });
  });

  describe('propose', () => {
    beforeEach(() => API.cxcLogin());

    after(() => API.logout());

    it('should return error if not authenticated', function() {
      const data = fixtures.proposeCompany();

      return API.logout()
        .then(() => API.proposeCompany(data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is admin', function() {
      const data = fixtures.proposeCompany();

      return API.logout()
        .then(() => API.login())
        .then(() => API.proposeCompany(data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const data = fixtures.proposeCompany();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.proposeCompany(data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if business_name is not present', function() {
      const data = fixtures.proposeCompany('business_name');

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if type is not present', function() {
      const data = fixtures.proposeCompany('type');

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if contact_name is not present', function() {
      const data = fixtures.proposeCompany('contact_name');

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if position is not present', function() {
      const data = fixtures.proposeCompany('position');

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if email is not present', function() {
      const data = fixtures.proposeCompany('email');

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if phone is not present', function() {
      const data = fixtures.proposeCompany('phone');

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Company');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return a successfully response', () => {
      const data = fixtures.proposeCompany();

      return API.proposeCompany(data)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');

          return consumer.getNextEvent()
            .then(validate(proposedCompany))
            .then(validate(successSchema)(responseData));
        });
    });
  });

  describe('createInvestorInvitation', () => {
    beforeEach(() => {
      if (!API.isLoggedIn()) {
        return API.login();
      }

      return Promise.resolve();
    });

    it('should return error if not authenticated', function() {
      const invitation  = fixtures.invitationSend();

      return API.logout()
        .then(() => API.createInvestorInvitation(invitation))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const invitation  = fixtures.invitationSend();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.createInvestorInvitation(invitation))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const invitation  = fixtures.invitationSend();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.createInvestorInvitation(invitation))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      const invitation  = fixtures.invitationSend();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.createInvestorInvitation(invitation))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an object if there was no issue with the information', () => {
      const invitation  = fixtures.invitationSend();

      return API.createInvestorInvitation(invitation)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Company');

          return consumer.getNextEvent()
            .then(validate(investorInvitationCreated))
            .then(validate(investorInvitationCreatedSchema)(responseData.data));
        }).catch(reThrow);
    });
  });

  describe('getInvestorTransactions', () => {
    beforeEach(() => {
      if (!API.isLoggedIn()) {
        return API.investorLogin();
      }

      return Promise.resolve();
    });

    it('should return error if not authenticated', function() {
      const data = fixtures.validStatementRequest();
      const investorCompanyId = fixtures.investorUser();

      return API.logout()
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      const data = fixtures.validStatementRequest();
      const investorCompanyId = fixtures.investorUser();

      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      const data = fixtures.validStatementRequest();
      const investorCompanyId = fixtures.investorUser();

      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);

          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if year param is not present', function() {
      const data = fixtures.invalidStatementRequest('year');
      const investorCompanyId = fixtures.investorUser();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Investor');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if month param is not present', function() {
      const data = fixtures.invalidStatementRequest('month');
      const investorCompanyId = fixtures.investorUser();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Investor');
          errorData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return a successful response for with a regular investor\'s transactions', () => {
      const data = fixtures.validStatementRequest();
      const investorCompanyId = fixtures.investorUser();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');

          return validate(investorTransactionsSchema)(responseData.data);
        }).catch(reThrow);
    });

    it('should return a successful response with a fideicomiso investor\'s transactions', () => {
      const data = fixtures.validStatementFideicomisoRequest();
      const investorCompanyId = fixtures.fideicomisoInvestor();

      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getInvestorTransactions(investorCompanyId, data))
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Investor');

          return validate(investorTransactionsSchema)(responseData.data);
        }).catch(reThrow);
    });
  });
});
