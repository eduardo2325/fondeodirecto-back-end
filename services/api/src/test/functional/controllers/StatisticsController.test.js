const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const API = require('../helpers/api');
const newStatisticSchema = require('../schemas/new-statistics');
const validate = require('../helpers/validate');
const errorSchema = require('../schemas/error');
const fixtures = require('../fixtures/statistics');

chai.should();
chai.use(chaiAsPromised);

describe('functional/Statistics tests', function() {
  beforeEach(() => {
    if (!API.isLoggedIn()) {
      return API.login();
    }

    return Promise.resolve();
  });

  describe('checkToken', () => {
    it('should return error if token does not exists', () => {
      const data = fixtures.nonExistentToken();

      return API.checkToken(data).should.be.fulfilled
        .then( error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Statistics');
          errorData.error.path.should.be.equal('token');

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if statistics_type is not valid', () => {
      const data = fixtures.nonExistentMetric();

      return API.checkToken(data).should.be.fulfilled
        .then( error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Statistics');
          errorData.error.path.should.be.equal('statistics');

          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return ok', () => {
      const data = fixtures.validInvestorInvitation();

      return API.checkToken(data).should.be.fulfilled
        .then( response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.eql('Statistics');

          return validate(newStatisticSchema)(responseData.data);
        });
    });

    it('should return success for a valid new invoice notification', () => {
      const data = fixtures.validNewInvoiceNotification();

      return API.checkToken(data).should.be.fulfilled
        .then( response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.eql('Statistics');

          return validate(newStatisticSchema)(responseData.data);
        });
    });
  });
});
