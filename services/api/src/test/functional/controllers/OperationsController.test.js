const API = require('../helpers/api');
const fixtures = require('../fixtures/invoice');
const operationFixtures = require('../fixtures/operation');
const errorSchema = require('../schemas/error');
const errorCodes = require('../../../config/error-codes');
const validate = require('../helpers/validate');
const _ = require('lodash');
const operationsSchema = require('../schemas/operations');
const adminOperationsSchema = require('../schemas/adminOperations');
const operationGeneralInfoWithClientSchema = require('../schemas/operationGeneralInfoWithClient');

describe('functional/Operations controller', () => {
  describe('getOperations by CXC user', () => {
    beforeEach(() => {
      if (!API.isLoggedIn()) {
        return API.cxcLogin();
      }

      return Promise.resolve();
    });

    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getOperations())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is investor', function() {
      return API.investorLogin()
        .then(() => API.getOperations())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an array of operations', () => {
      return API.getOperations()
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.not.equal(0);

          return validate(operationsSchema)(responseData.data);
        });
    });
  });


  describe('getOperations by CXP user', () => {
    beforeEach(() => {
      if (!API.isLoggedIn()) {
        return API.cxpLogin();
      }

      return Promise.resolve();
    });

    it('should return an array of operations', () => {
      return API.cxpLogin()
        .then( () => API.getOperations() )
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.not.equal(0);

          _.each( responseData.data.operations, element => {
            element.status.should.be.not.equal('payment_in_process');
            element.status.should.be.not.equal('completed');
          });

          return validate(operationsSchema)(responseData.data);
        });
    });


    it('should return an array of payed operations', () => {
      return API.cxpLogin()
        .then( () => API.getPayedOperations())
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.not.equal(0);

          const validStatus = [ 'completed', 'payment_in_process' ];

          _.each( responseData.data.operations, element => {
            _.includes(validStatus, element.status).should.be.eq(true);
          });

          return validate(operationsSchema)(responseData.data);
        });
    });


    it('should return an array of payed operations case 2', () => {
      return API.cxpLoginCase2()
        .then( () => API.getPayedOperations())
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.not.equal(0);

          const validStatus = [ 'completed', 'payment_in_process' ];

          _.each( responseData.data.operations, element => {
            _.includes(validStatus, element.status).should.be.eq(true);
          });

          return validate(operationsSchema)(responseData.data);
        });
    });

    it('should return an array of payed operations', () => {
      return API.getPayedOperations()
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');

          return validate(operationsSchema)(responseData.data);
        });
    });
  });

  describe('getOperation', () => {
    beforeEach(() => {
      if (!API.isLoggedIn()) {
        return API.cxcLogin();
      }

      return Promise.resolve();
    });

    it('should return error if not authenticated', function() {
      const operationId = operationFixtures.someOperationId();

      return API.logout()
        .then(() => API.getOperation(operationId))
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return not found if operation does not exists', () => {
      const operationId = operationFixtures.nonExistentOperationId();

      return API.getOperation(operationId)
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Operation');
          errorData.error.path.should.be.eql('Operation');
          errorData.error.code.should.be.eql(errorCodes.Server.NotFound);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return an operation', () => {
      const operationId = operationFixtures.someOperationId();

      return API.getOperation(operationId)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('Operation');

          return validate(operationGeneralInfoWithClientSchema)(responseData.data);
        });
    });
  });

  describe('getAdminOperations', () => {
    const pageSize = 500;
    let operationsList = {};
    let totalOperations = 0;
    let filterClientNameList = [];
    let filterCompanyNameList = [];
    let filterInvestorNameList = [];
    let filterStatusList = [];

    before(() => {
      return API.login()
        .then(() => API.getAdminOperations({
          page_size: pageSize
        }))
        .then(response => {
          const responseData = response.data;
          const { company_name } = fixtures.validCompanyName();
          const { client_name } = fixtures.validClientName();
          const { investor_name } = fixtures.validInvestorName();
          const { status } = operationFixtures.validStatusArray();

          totalOperations = responseData.data.total_operations;
          operationsList = _.sortBy(responseData.data.operations, function(e) {
            return e.invoice.client_name;
          });
          filterClientNameList = _.filter(responseData.data.operations, inv => {
            return inv.invoice.client_name.indexOf(client_name) !== -1;
          });
          filterCompanyNameList = _.filter(responseData.data.operations, inv => {
            return inv.invoice.company_name.indexOf(company_name) !== -1;
          });
          filterInvestorNameList = _.filter(responseData.data.operations, inv => {
            return inv.invoice.investor_name && inv.invoice.investor_name.indexOf(investor_name) !== -1;
          });
          filterStatusList = _.filter(responseData.data.operations, inv => {
            return inv.status.indexOf(status[0]) !== -1 || inv.status.indexOf(status[1]) !== -1;
          });
        });
    });

    beforeEach(() => {
      if (!API.isLoggedIn()) {
        return API.login();
      }

      return Promise.resolve();
    });

    it('should return error if not authenticated', function() {
      return API.logout()
        .then(() => API.getAdminOperations())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Session');
          errorData.error.code.should.be.eql(errorCodes.Session.Unauthorized);
          return validate(errorSchema)(errorData.error);
        });
    });

    it('should return error if role is cxc', function() {
      return API.logout()
        .then(() => API.cxcLogin())
        .then(() => API.getAdminOperations())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is cxp', function() {
      return API.logout()
        .then(() => API.cxpLogin())
        .then(() => API.getAdminOperations())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return error if role is investor', function() {
      return API.logout()
        .then(() => API.investorLogin())
        .then(() => API.getAdminOperations())
        .should.be.fulfilled
        .then(error => {
          const errorData = error.data;

          error.status.should.be.equal(200);
          errorData.type.should.be.eql('Role');
          errorData.error.code.should.be.eql(errorCodes.Role.Unauthorized);
          return validate(errorSchema)(errorData.error);
        })
        .then(() => API.logout());
    });

    it('should return an array of operations with page_size limits', () => {
      const request = fixtures.validPageSizeLimit();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(request.page_size);
          responseData.data.total_operations.should.be.equal(totalOperations);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an error if order_by has an invalid value', () => {
      const request = fixtures.invalidOrderBy();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.error.path.should.be.equal('order_by');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if page_size has an invalid value', () => {
      const request = fixtures.invalidPageSizeLimit();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.error.path.should.be.equal('page_size');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if the order_desc field isn\'t allowed', () => {
      const request = fixtures.invalidOrderDesc();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.error.path.should.be.equal('order_desc');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an array of operations ordered', () => {
      const request = fixtures.validOrderBy();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;
          const lastIndex = operationsList.length - 1;
          const firstOperation = operationsList[0];
          const lastOperation = operationsList[lastIndex];

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(operationsList.length);
          responseData.data.operations[0].invoice.client_name.should.be.equal(firstOperation.invoice.client_name);
          responseData.data.operations[lastIndex].invoice.client_name
            .should.be.equal(lastOperation.invoice.client_name);
          responseData.data.total_operations.should.be.equal(totalOperations);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an array of operations ordered desc', () => {
      const request = fixtures.validOrderDesc();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;
          const lastIndex = operationsList.length - 1;
          const firstOperation = operationsList[0];
          const lastOperation = operationsList[lastIndex];

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(operationsList.length);
          responseData.data.operations[0].invoice.client_name.should.be.equal(lastOperation.invoice.client_name);
          responseData.data.operations[lastIndex].invoice.client_name
            .should.be.equal(firstOperation.invoice.client_name);
          responseData.data.total_operations.should.be.equal(totalOperations);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an array of operations', () => {
      return API.getAdminOperations({ page_size: operationFixtures.totalOperationsCount() })
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an array of operations where client_name must to match', () => {
      const request = fixtures.validClientName();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(filterClientNameList.length);
          responseData.data.total_operations.should.be.equal(filterClientNameList.length);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an array of operations where company_name must to match', () => {
      const request = fixtures.validCompanyName();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(filterCompanyNameList.length);
          responseData.data.total_operations.should.be.equal(filterCompanyNameList.length);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an array of operations where investor_name must to match', () => {
      const request = fixtures.validInvestorName();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(filterInvestorNameList.length);
          responseData.data.total_operations.should.be.equal(filterInvestorNameList.length);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an array of operations filtered by status', () => {
      const request = operationFixtures.validStatusArray();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.data.operations.length.should.be.equal(filterStatusList.length);
          responseData.data.total_operations.should.be.equal(filterStatusList.length);

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an error if the status field isn\'t allowed', () => {
      const request = fixtures.invalidStatusArray();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');
          responseData.error.path.should.be.equal('status');
          responseData.error.code.should.be.eql(errorCodes.Server.InvalidRequest);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if start_fund_date has invalid value', () => {
      const request = fixtures.invalidFundStartDate();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvoiceList');
          responseData.error.path.should.be.equal('start_fund_date');
          responseData.error.code.should.be.eql(errorCodes.InvoiceList['Invalid start_fund_date']);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if end_fund_date has invalid value', () => {
      const request = fixtures.invalidFundEndDate();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvoiceList');
          responseData.error.path.should.be.equal('end_fund_date');
          responseData.error.code.should.be.eql(errorCodes.InvoiceList['Invalid end_fund_date']);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an array of operations where fund date is between start_fund_date and end_fund_date', () => {
      const { uuid } = fixtures.yesterdayFundRequestInvoice();
      const request = fixtures.validFundDateRange();

      request.page_size = pageSize;

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('OperationList');

          const olderOperation = _.find(responseData.data.operations, { uuid });

          if (olderOperation) {
            return Promise.reject('Operation out of range is present');
          }

          return validate(adminOperationsSchema)(responseData.data);
        });
    });

    it('should return an error if start_expiration_date has invalid value', () => {
      const request = fixtures.invalidExpirationStartDate();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvoiceList');
          responseData.error.path.should.be.equal('start_expiration_date');
          responseData.error.code.should.be.eql(errorCodes.InvoiceList['Invalid start_expiration_date']);
          return validate(errorSchema)(responseData.error);
        });
    });

    it('should return an error if end_expiration_date has invalid value', () => {
      const request = fixtures.invalidExpirationEndDate();

      return API.getAdminOperations(request)
        .should.be.fulfilled
        .then(response => {
          const responseData = response.data;

          response.status.should.be.equal(200);
          responseData.type.should.be.equal('InvoiceList');
          responseData.error.path.should.be.equal('end_expiration_date');
          responseData.error.code.should.be.eql(errorCodes.InvoiceList['Invalid end_expiration_date']);
          return validate(errorSchema)(responseData.error);
        });
    });
  });
});
