const invoiceSchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: false,
      type: 'integer'
    },
    company_id: {
      required: true,
      type: 'integer'
    },
    company_name: {
      type: 'string'
    },
    company_color: {
      type: 'string'
    },
    client_company_id: {
      required: true,
      type: 'integer'
    },
    client_created_at: {
      required: false,
      type: 'string'
    },
    client_name: {
      required: true,
      type: 'string',
      minLength: 1
    },
    client_color: {
      required: true,
      type: 'string',
      minLength: 1
    },
    client_description: {
      required: true,
      type: 'string'
    },
    business_name: {
      required: true,
      type: 'string'
    },
    number: {
      required: true,
      type: 'string'
    },
    emission_date: {
      required: true,
      type: 'string'
    },
    expiration: {
      required: true,
      type: 'string'
    },
    uuid: {
      required: true,
      type: 'string'
    },
    status: {
      required: true,
      type: 'string'
    },
    expiration_extra: {
      required: true,
      type: 'string'
    },
    subtotal: {
      required: true,
      type: 'string'
    },
    total: {
      required: true,
      type: 'string'
    },
    expiration_days: {
      required: true,
      type: 'integer'
    },
    client_rfc: {
      required: true,
      type: 'string'
    },
    published_date: {
      type: 'string'
    },
    upload_date: {
      type: 'string'
    },
    fund_date: {
      type: 'string'
    },
    approved_date: {
      type: 'string'
    },
    payment_date: {
      type: 'string'
    },
    items: {
      required: true,
      type: 'array',
      items: {
        required: true,
        type: 'object',
        properties: {
          count: {
            required: true,
            type: 'string'
          },
          description: {
            required: true,
            type: 'string'
          },
          price: {
            required: true,
            type: 'string'
          },
          single: {
            type: 'string'
          },
          total: {
            required: true,
            type: 'string'
          }
        },
        additionalProperties: false
      }
    },
    taxes: {
      type: 'string'
    },
    operation_id: {
      type: 'integer'
    }
  },
  additionalProperties: false
};

module.exports = invoiceSchema;
