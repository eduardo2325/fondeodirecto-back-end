const pendingTransactionSchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: true,
      type: 'integer'
    },
    amount: {
      required: true,
      type: 'string'
    },
    type: {
      required: true,
      enum: [ 'withdraw', 'deposit' ]
    },
    company_id: {
      required: true,
      type: 'integer'
    },
    status: {
      required: true,
      enum: [ 'pending', 'approved', 'rejected' ]
    },
    created_at: {
      required: true,
      type: 'string'
    }
  }
};

module.exports = pendingTransactionSchema;
