const investorOperationCostSchema = {
  required: true,
  type: 'object',
  properties: {
    fee: {
      required: true,
      type: 'number'
    },
    variable_fee_percentage: {
      required: true,
      type: 'number'
    }
  },
  additionalProperties: false
};

module.exports = investorOperationCostSchema;
