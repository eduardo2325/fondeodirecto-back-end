const newInvestorSchema = {
  type: 'object',
  required: true,
  properties: {
    name: {
      required: true,
      type: 'string'
    },
    email: {
      required: true,
      type: 'string'
    },
    status: {
      required: true,
      type: 'string'
    },
    role: {
      required: true,
      type: 'string'
    },
    color: {
      required: true,
      type: 'string'
    },
    company_id: {
      required: true,
      type: 'integer'
    }
  }
};

module.exports = newInvestorSchema;

