
const newStatisticsSchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: true,
      type: 'integer'
    }
  }
};

module.exports = newStatisticsSchema;
