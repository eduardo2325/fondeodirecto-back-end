const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  invoices: {
    required: true,
    type: 'array'
  },
  investor_name: {
    required: true,
    type: 'string'
  }
};

module.exports = eventSchema('user', 'InvoiceFundRequest', body);
