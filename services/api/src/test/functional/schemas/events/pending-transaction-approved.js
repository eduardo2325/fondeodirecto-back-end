const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  type: {
    required: true,
    type: 'string'
  },
  mailerFlagFideicomiso: {
    required: true,
    type: 'boolean'
  },
  amount: {
    require: true,
    type: 'string'
  }
};

module.exports = eventSchema('user', 'PendingTransactionApproved', body);
