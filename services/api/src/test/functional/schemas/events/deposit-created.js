const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  mailerFlagFideicomiso: {
    required: true,
    type: 'boolean'
  },
  company_id: {
    required: true,
    type: 'integer'
  },
  company_name: {
    required: true,
    type: 'string'
  }
};

module.exports = eventSchema('user', 'DepositCreated', body);
