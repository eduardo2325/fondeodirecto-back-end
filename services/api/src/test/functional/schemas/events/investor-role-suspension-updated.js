const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  company_id: {
    required: true,
    type: 'integer'
  },
  suspended: {
    required: true,
    type: 'boolean'
  }
};

module.exports = eventSchema('user', 'InvestorRoleSuspensionUpdated', body);
