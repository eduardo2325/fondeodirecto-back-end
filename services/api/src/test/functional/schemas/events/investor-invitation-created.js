const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  name: {
    required: true,
    type: 'string'
  },
  token: {
    required: true,
    type: 'string'
  }
};

module.exports = eventSchema('user', 'InvestorInvitationCreated', body);
