const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  company_id: {
    required: true,
    type: 'integer'
  },
  suspended: {
    required: true,
    type: 'boolean'
  },
  role: {
    required: true,
    type: 'string'
  }
};

module.exports = eventSchema('user', 'CompanyRoleSuspensionUpdated', body);
