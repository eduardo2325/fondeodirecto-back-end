const eventSchema = require('./event');

const body = {
  emails: {
    required: true,
    type: 'string'
  },
  mailerFlagFideicomiso: {
    required: true,
    type: 'boolean'
  },
  invoice_number: {
    required: true,
    type: 'string'
  },
  invoice_id: {
    required: true,
    type: 'integer'
  },
  client_name: {
    required: true,
    type: 'string'
  },
  amount: {
    required: true,
    type: 'string'
  }
};

module.exports = eventSchema('user', 'ClientInvoicePaymentCreated', body);
