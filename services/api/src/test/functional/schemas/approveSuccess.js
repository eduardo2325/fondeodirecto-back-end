const approveSuccessSchema = {
  required: true,
  type: 'object',
  properties: {
    success: {
      required: true,
      type: 'boolean'
    }
  },
  additionalProperties: false
};

module.exports = approveSuccessSchema;
