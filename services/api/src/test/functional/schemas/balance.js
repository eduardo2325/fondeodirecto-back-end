const balanceSchema = {
  required: true,
  type: 'object',
  properties: {
    total: {
      required: true,
      type: 'string'
    }
  }
};

module.exports = balanceSchema;
