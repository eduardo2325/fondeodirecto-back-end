const schema = {
  required: true,
  type: 'object',
  properties: {
    excel_files: {
      type: 'array',
      required: true,
      items: {
        required: true,
        type: 'object',
        properties: {
          bank_report_id: {
            type: 'number'
          },
          no_transactions: {
            type: 'string'
          },
          amount: {
            type: 'number'
          },
          emmission_date: {
            type: 'string'
          },
          generated_by: {
            type: 'string'
          },
          url: {
            type: 'string'
          }
        }
      }
    }
  }
};

module.exports = schema;
