const marketplaceSchema = {
  required: true,
  type: 'object',
  properties: {
    invoices: {
      required: true,
      type: 'array',
      minItems: 1,
      items: {
        required: true,
        type: 'object',
        properties: {
          id: {
            required: true,
            type: 'integer'
          },
          client_company_id: {
            required: true,
            type: 'integer'
          },
          client_name: {
            required: true,
            type: 'string'
          },
          client_color: {
            required: true,
            type: 'string'
          },
          published: {
            required: true,
            type: 'string'
          },
          total: {
            required: true,
            type: 'number'
          },
          expiration: {
            required: true,
            type: 'string'
          },
          status: {
            required: true,
            type: 'string'
          },
          uuid: {
            required: true,
            type: 'string'
          },
          gain: {
            required: true,
            type: 'number'
          },
          gain_percentage: {
            required: true,
            type: 'number'
          },
          term_days: {
            required: true,
            type: 'number'
          },
          number: {
            required: true,
            type: 'string'
          },
          emission_date: {
            required: true,
            type: 'string'
          },
          expiration_extra: {
            required: true,
            type: 'string'
          },
          operation_id: {
            required: true,
            type: 'integer'
          }
        },
        additionalProperties: false
      }
    },
    total_invoices: {
      required: true,
      type: 'integer'
    },
    max_total: {
      required: true,
      type: 'number'
    }
  },
  additionalProperties: false
};

module.exports = marketplaceSchema;
