const companySchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: true,
      type: 'integer'
    },
    client_company_id: {
      required: true,
      type: 'integer'
    },
    client_name: {
      required: true,
      type: 'string'
    },
    client_color: {
      required: true,
      type: 'string'
    },
    company_id: {
      required: true,
      type: 'integer'
    },
    company_name: {
      required: true,
      type: 'string'
    },
    company_color: {
      required: true,
      type: 'string'
    },
    expiration: {
      required: true,
      type: 'string'
    },
    number: {
      required: true,
      type: 'string'
    },
    status: {
      required: true,
      type: 'string'
    },
    created_at: {
      required: true,
      type: 'string'
    },
    total: {
      required: true,
      type: 'string'
    },
    uuid: {
      required: true,
      type: 'string'
    },
    fund_date: {
      required: true,
      type: 'string'
    },
    term_days: {
      required: true,
      type: 'number'
    },
    gain: {
      required: true,
      type: 'number'
    },
    gain_percentage: {
      required: true,
      type: 'number'
    },
    annual_gain: {
      required: true,
      type: 'number'
    }
  }
};


const invoicesSchema = {
  required: true,
  type: 'object',
  properties: {
    invoices: {
      required: true,
      type: 'array',
      minItems: 1,
      items: {
        required: true,
        type: 'object',
        properties: companySchema
      }
    },
    total_invoices: {
      required: true,
      type: 'integer'
    },
    total_pages: {
      required: true,
      type: 'integer'
    }
  },
  additionalProperties: false
};

module.exports = invoicesSchema;

