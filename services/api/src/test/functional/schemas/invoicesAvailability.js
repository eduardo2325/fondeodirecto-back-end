const invoicesAvailabilitySchema = {
  required: true,
  type: 'object',
  properties: {
    invoices: {
      required: true,
      type: 'array',
      minItems: 1,
      items: {
        required: true,
        type: 'object',
        properties: {
          id: {
            required: true,
            type: 'integer'
          },
          is_available: {
            required: true,
            type: 'boolean'
          }
        },
        additionalProperties: false
      }
    }
  },
  additionalProperties: false
};

module.exports = invoicesAvailabilitySchema;
