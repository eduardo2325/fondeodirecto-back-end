const schema = {
  required: true,
  type: 'object',
  properties: {
    total_amount: {
      type: 'number',
      required: true
    },
    generated_by: {
      type: 'string',
      required: true
    },
    emission_date: {
      type: 'string',
      required: true
    },
    transactions: {
      required: true,
      type: 'array',
      items: {
        required: true,
        type: 'object',
        properties: {
          reference: {
            type: 'number',
            required: true
          },
          transaction_id: {
            type: 'number',
            required: true
          },
          destinated_to: {
            type: 'string',
            required: true
          },
          bank_name: {
            type: 'string',
            required: true
          },
          clabe: {
            type: 'string',
            required: true
          },
          amount: {
            type: 'number',
            required: true
          },
          selected: {
            type: 'boolean',
            required: true
          },
          status: {
            type: 'string',
            required: false
          }
        }
      }
    }
  }
};

module.exports = schema;
