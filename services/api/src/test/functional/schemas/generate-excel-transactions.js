const schema = {
  required: true,
  type: 'object',
  properties: {
    excel_url: {
      type: 'string',
      required: true
    }
  }
};

module.exports = schema;
