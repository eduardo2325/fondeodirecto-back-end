const operationSchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: true,
      'type': 'integer'
    },
    invoice_id: {
      required: true,
      'type': 'integer'
    },
    annual_cost_percentage: {
      required: true,
      'type': 'number'
    },
    reserve_percentage: {
      required: true,
      'type': 'number'
    },
    fd_commission_percentage: {
      required: true,
      'type': 'number'
    },
    annual_cost: {
      required: true,
      'type': 'number'
    },
    reserve: {
      required: true,
      'type': 'number'
    },
    fd_commission: {
      required: true,
      'type': 'number'
    },
    factorable: {
      required: true,
      'type': 'number'
    },
    subtotal: {
      required: true,
      'type': 'number'
    },
    fund_payment: {
      required: true,
      'type': 'number'
    },
    commission: {
      required: true,
      'type': 'number'
    },
    operation_cost: {
      required: true,
      'type': 'number'
    },
    fund_total: {
      required: true,
      'type': 'number'
    },
    expiration_date: {
      required: true,
      'type': 'string'
    },
    published_date: {
      required: true,
      'type': 'string'
    },
    days_limit: {
      required: true,
      'type': 'integer'
    },
    status: {
      required: true,
      'type': 'string'
    },
    formula: {
      required: false,
      'type': 'string'
    },
    user_id: {
      required: true,
      'type': 'integer'
    },
    invoice: {
      type: 'object',
      required: true,
      properties: {
        client_company_id: {
          required: true,
          type: 'integer'
        },
        client_created_at: {
          required: false,
          type: 'string'
        },
        client_name: {
          required: true,
          type: 'string'
        },
        client_color: {
          required: true,
          type: 'string'
        },
        company_id: {
          required: true,
          type: 'integer'
        },
        company_name: {
          required: true,
          type: 'string'
        },
        company_color: {
          required: true,
          type: 'string'
        },
        expiration: {
          required: true,
          type: 'string'
        },
        number: {
          required: true,
          type: 'string'
        },
        status: {
          required: true,
          type: 'string'
        },
        uuid: {
          required: true,
          type: 'string'
        },
        upload_date: {
          type: 'string'
        },
        fund_date: {
          type: 'string'
        },
        approved_date: {
          type: 'string'
        },
        payment_date: {
          type: 'string'
        },
        taxes: {
          type: 'string'
        }
      }
    }
  }
};

module.exports = operationSchema;
