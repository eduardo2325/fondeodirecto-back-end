
const operationsSchema = {
  required: true,
  type: 'object',
  properties: {
    operations: {
      required: true,
      type: 'array',
      minItems: 1,
      items: {
        required: true,
        type: 'object',
        properties: {
          required: true,
          type: 'object',
          properties: {
            id: {
              required: true,
              'type': 'integer'
            },
            invoice_id: {
              required: true,
              'type': 'integer'
            },
            total: {
              required: true,
              'type': 'number'
            },
            expiration_date: {
              required: true,
              'type': 'string'
            },
            published_date: {
              required: true,
              'type': 'string'
            },
            days_limit: {
              required: true,
              'type': 'integer'
            },
            status: {
              required: true,
              'type': 'string'
            },
            number: {
              required: false,
              'type': 'string'
            }
          }
        }
      }
    },
    total_operations: {
      required: true,
      type: 'integer'
    },
    total_pages: {
      required: true,
      type: 'integer'
    }
  },
  additionalProperties: false
};

module.exports = operationsSchema;

