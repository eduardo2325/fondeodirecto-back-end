const userSchema = {
  type: 'object',
  required: true,
  properties: {
    exists: {
      required: true,
      type: 'boolean'
    }
  }
};

module.exports = userSchema;
