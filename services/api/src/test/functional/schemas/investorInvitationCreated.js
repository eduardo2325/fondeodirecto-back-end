const investorInvitationCreatedSchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: true,
      type: 'integer'
    },
    created_at: {
      required: true,
      type: 'string'
    }
  },
  additionalProperties: false
};

module.exports = investorInvitationCreatedSchema;

