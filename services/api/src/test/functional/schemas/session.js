const _ = require('lodash');
const userSchema = _.cloneDeep(require('./user'));
const companySchema = _.cloneDeep(userSchema.properties.company);

delete userSchema.properties.company;

const sessionSchema = {
  required: true,
  type: 'object',
  properties: {
    token: {
      type: 'object',
      properties: {
        token: {
          required: true,
          type: 'string'
        },
        expiration_date: {
          required: true,
          type: 'string'
        }
      },
      required: true
    },
    company_created_at: {
      type: 'string',
      required: false
    },
    user: _.merge({}, userSchema, {
      properties: {
        suspended: {
          required: true,
          type: 'boolean'
        }
      }
    }),
    company: {
      ...companySchema
    },
    agreement: {
      type: 'object',
      properties: {
        agreed_at: {
          required: true,
          type: 'string'
        },
        agreement_url: {
          required: true,
          type: 'string'
        }
      }
    }
  }
};

module.exports = sessionSchema;
