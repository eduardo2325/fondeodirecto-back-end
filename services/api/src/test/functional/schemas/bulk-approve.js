const typeIds = {
  required: true,
  type: 'array',
  items: {
    required: false,
    type: 'object',
    properties: {
      id: {
        type: 'number',
        required: true
      }
    }
  }
};

const schema = {
  required: true,
  type: 'object',
  properties: {
    success: typeIds,
    fail: typeIds
  }
};

module.exports = schema;
