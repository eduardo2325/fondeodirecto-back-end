const invoicesAvailabilitySchema = {
  required: true,
  type: 'object',
  properties: {
    invoices: {
      required: true,
      type: 'array',
      minItems: 1,
      items: {
        required: true,
        type: 'object',
        properties: {
          id: {
            required: true,
            type: 'integer'
          },
          is_available: {
            required: true,
            type: 'boolean'
          },
          client_name: {
            required: true,
            type: 'string'
          },
          total: {
            required: true,
            type: 'number'
          },
          term_days: {
            required: true,
            type: 'integer'
          },
          gain: {
            required: true,
            type: 'number'
          },
          isr: {
            required: true,
            type: 'number'
          },
          fee: {
            required: true,
            type: 'number'
          },
          interest: {
            required: true,
            type: 'number'
          },
          perception: {
            required: true,
            type: 'number'
          }
        },
        additionalProperties: false
      }
    },
    summary: {
      required: true,
      type: 'object',
      properties: {
        invoices_total: {
          required: true,
          type: 'number'
        },
        interests_total: {
          required: true,
          type: 'number'
        },
        isr_total: {
          required: true,
          type: 'number'
        },
        fee_total: {
          required: true,
          type: 'number'
        },
        earnings_total: {
          required: true,
          type: 'number'
        }
      },
      additionalProperties: false
    },
    total: {
      required: true,
      type: 'integer'
    },
    fund: {
      required: true,
      type: 'boolean'
    }
  },
  additionalProperties: false
};

module.exports = invoicesAvailabilitySchema;
