const investorTransactionsSchema = {
  required: true,
  type: 'object',
  properties: {
    investor_transactions: {
      required: true,
      type: 'array',
      items: {
        required: true,
        type: 'object',
        properties: {
          day: {
            required: true,
            type: 'string'
          },
          description: {
            required: true,
            type: 'string'
          },
          operation: {
            required: true,
            type: 'string'
          },
          factoraje: {
            required: true,
            type: 'string'
          },
          cash: {
            required: true,
            type: 'string'
          }
        },
        additionalProperties: false
      }
    }
  },
  additionalProperties: false
};

module.exports = investorTransactionsSchema;
