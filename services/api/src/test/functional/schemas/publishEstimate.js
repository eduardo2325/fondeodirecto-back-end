const publishEstimateSchema = {
  required: true,
  type: 'object',
  properties: {
    total_invoices: {
      required: true,
      type: 'number'
    },
    invoices: {
      required: true,
      type: 'array',
      minItems: 1,
      items: {
        required: true,
        type: 'object',
        properties: {
          is_available: {
            required: true,
            type: 'boolean'
          },
          client_name: {
            required: true,
            type: 'string'
          },
          number: {
            required: true,
            type: 'string'
          },
          total: {
            required: true,
            type: 'number'
          },
          operation_cost: {
            required: true,
            type: 'number'
          },
          reserve: {
            required: true,
            type: 'number'
          },
          fund_payment: {
            required: true,
            type: 'number'
          }
        },
        additionalProperties: false
      }
    },
    summary: {
      required: true,
      type: 'object',
      properties: {
        invoices_total: {
          required: true,
          type: 'number'
        },
        operation_cost_total: {
          required: true,
          type: 'number'
        },
        reserve_total: {
          required: true,
          type: 'number'
        },
        fund_payment_total: {
          required: true,
          type: 'number'
        },
        additionalProperties: false
      }
    }
  },
  additionalProperties: false
};

module.exports = publishEstimateSchema;
