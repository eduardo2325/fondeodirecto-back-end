const operationSchema = {
  required: true,
  type: 'object',
  properties: {
    id: {
      required: true,
      'type': 'integer'
    },
    invoice_id: {
      required: true,
      'type': 'integer'
    },
    annual_cost_percentage: {
      required: true,
      'type': 'number'
    },
    reserve_percentage: {
      required: true,
      'type': 'number'
    },
    fd_commission_percentage: {
      required: true,
      'type': 'number'
    },
    annual_cost: {
      required: true,
      'type': 'number'
    },
    reserve: {
      required: true,
      'type': 'number'
    },
    fd_commission: {
      required: true,
      'type': 'number'
    },
    factorable: {
      required: true,
      'type': 'number'
    },
    subtotal: {
      required: true,
      'type': 'number'
    },
    fund_payment: {
      required: true,
      'type': 'number'
    },
    commission: {
      required: true,
      'type': 'number'
    },
    operation_cost: {
      required: true,
      'type': 'number'
    },
    fund_total: {
      required: true,
      'type': 'number'
    },
    expiration_date: {
      required: true,
      'type': 'string'
    },
    published_date: {
      required: true,
      'type': 'string'
    },
    days_limit: {
      required: true,
      'type': 'integer'
    },
    status: {
      required: true,
      'type': 'string'
    },
    formula: {
      required: false,
      'type': 'string'
    },
    user_id: {
      required: true,
      'type': 'integer'
    }
  }
};

module.exports = operationSchema;
