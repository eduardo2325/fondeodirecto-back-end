const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/StatisticsController');
const helperFixtures = require('../fixtures/controllers/statistics');
const gateway = require('../../../api/helpers/gateway');
const errorHelper = require('../../../api/helpers/error');


describe('unit/Statistics controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('checkToken', () => {
    const fixtures = helperFixtures.checkToken;
    const { request, response, messageCall, responseType,
      responseData, responseError, guid, sendParams, errorHelperParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(response));
      sandbox.stub(response, 'send').callsFake(() => Promise.resolve());
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.resolve());
    });

    it('should return an error if send return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(responseError.error));

      return controller.checkToken(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a successful response', () => {
      return controller.checkToken(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(messageCall, responseData, responseType, guid).should.be.true;
          response.send.calledOnce.should.be.true;
          response.send.calledWithMatch(responseData).should.be.true;
          errorHelper.handleResponse.called.should.be.false;
        });
    });
  });
});

