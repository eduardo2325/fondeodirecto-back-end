const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/OperationsController');
const helperFixtures = require('../fixtures/controllers/operation');

const gateway = require('../../../api/helpers/gateway');
const errorHelper = require('../../../api/helpers/error');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Operation controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('getOperations', () => {
    const fixtures = helperFixtures.getOperations;
    const { request, gatewayResult, requestOrderBy, response, messageCall, responseType,
      responseData, responseError, guid, operationList, sendParams, sendParamsOrderBy, errorHelperParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(gatewayResult));
      sandbox.stub(response, 'send').callsFake(() => Promise.resolve());
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.resolve());
    });

    it('should return an error if send return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(responseError.error));

      return controller.getOperations(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a successful response when order by is applied', () => {
      return controller.getOperations(requestOrderBy, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParamsOrderBy);
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(messageCall, responseData, responseType, guid).should.be.true;
          response.send.calledOnce.should.be.true;
          response.send.calledWithMatch(responseData).should.be.true;
          errorHelper.handleResponse.called.should.be.false;
          operationList.should.be.deep.equal(responseData.data.operations);
        });
    });

    it('should return a successful response', () => {
      return controller.getOperations(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(messageCall, responseData, responseType, guid).should.be.true;
          response.send.calledOnce.should.be.true;
          response.send.calledWithMatch(responseData).should.be.true;
          errorHelper.handleResponse.called.should.be.false;
        });
    });
  });

  describe('getOperation', () => {
    const fixtures = helperFixtures.getOperation;
    const { request, response, messageCall, responseType,
      responseData, responseError, guid, operation, sendParams, errorHelperParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(operation));
      sandbox.stub(response, 'send').callsFake(() => Promise.resolve());
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.resolve());
    });

    it('should return an error if send return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(responseError.error));

      return controller.getOperation(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a successful response', () => {
      return controller.getOperation(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(messageCall, responseData, responseType, guid).should.be.true;
          response.send.calledOnce.should.be.true;
          response.send.calledWithMatch(responseData).should.be.true;
          errorHelper.handleResponse.called.should.be.false;
        });
    });
  });

  describe('getAdminOperations', () => {
    const fixtures = helperFixtures.getAdminOperations;
    const { request, response, messageCall, responseType,
      responseData, responseError, guid, operationList, sendParams, errorHelperParams,
      requestInvalidStartFundDate, requestInvalidEndFundDate, requestInvalidEndExpirationDate,
      requestInvalidStartExpirationDate, startFundDateError, endFundDateError,
      startExpirationDateError, endExpirationDateError } = fixtures;

    beforeEach(() => {
      sandbox.stub(_, 'map').callsFake(() => []);
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.resolve(operationList));
      sandbox.stub(response, 'send').callsFake(() => Promise.resolve());
      sandbox.stub(errorHelper, 'handleResponse').callsFake(() => Promise.resolve());
    });

    it('should return an error if send return a reject', () => {
      gateway.sendUser.restore();
      sandbox.stub(gateway, 'sendUser').callsFake(() => Promise.reject(responseError.error));

      return controller.getAdminOperations(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(errorHelperParams);
        });
    });

    it('should return a successful response', () => {
      return controller.getAdminOperations(request, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.calledOnce.should.be.true;
          gateway.sendUser.args[0].should.be.eql(sendParams);
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(messageCall, responseData, responseType, guid).should.be.true;
          response.send.calledOnce.should.be.true;
          response.send.calledWithMatch(responseData).should.be.true;
          errorHelper.handleResponse.called.should.be.false;
        });
    });

    it('should return an error if start_fund_date has invalid value', () => {
      return controller.getAdminOperations(requestInvalidStartFundDate, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.called.should.be.false;
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(startFundDateError);
        });
    });

    it('should return an error if end_fund_date has invalid value', () => {
      return controller.getAdminOperations(requestInvalidEndFundDate, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.called.should.be.false;
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(endFundDateError);
        });
    });

    it('should return an error if start_expiration_date has invalid value', () => {
      return controller.getAdminOperations(requestInvalidStartExpirationDate, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.called.should.be.false;
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(startExpirationDateError);
        });
    });

    it('should return an error if end_expiration_date has invalid value', () => {
      return controller.getAdminOperations(requestInvalidEndExpirationDate, response)
        .should.be.fulfilled
        .then(() => {
          gateway.sendUser.called.should.be.false;
          log.message.called.should.be.false;
          response.send.called.should.be.false;
          errorHelper.handleResponse.calledOnce.should.be.true;
          errorHelper.handleResponse.args[0].should.be.eql(endExpirationDateError);
        });
    });
  });
});
