const companyHasAgreed = {
  company_agreed: true
};

const companyHasNotAgreed = {
  company_agreed: null
};

module.exports = {
  operationIsAllowed: {
    reqRestrictedOperation: {
      swagger: {
        operation: {
          method: 'post',
          pathObject: {
            path: '/invoices'
          }
        }
      }
    },
    reqUnrestrictedOperation: {
      swagger: {
        operation: {
          method: 'get',
          pathObject: {
            path: '/invoices'
          }
        }
      }
    },
    scope: [
      'CXC',
      'CXP',
      'ADMIN'
    ],
    unauthorizedRoleUser: {
      suspended: false,
      user_role: 'INVESTOR',
      ...companyHasAgreed
    },
    suspendedUser: {
      suspended: true,
      user_role: 'CXC',
      ...companyHasAgreed
    },
    activeUser: {
      suspended: false,
      user_role: 'CXC',
      ...companyHasAgreed
    },
    activeUserCompanyHasNotAgreed: {
      suspended: false,
      user_role: 'CXC',
      ...companyHasNotAgreed
    }
  }
};
