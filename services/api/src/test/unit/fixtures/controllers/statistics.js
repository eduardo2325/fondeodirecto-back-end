const { apiCommonError } = require('../../../common/fixtures/error');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const request = {
  guid: guid,
  body: {
    token: '123456',
    statistics_type: 'register-investor',
    event_type: 'CLICK'
  }
};

const gatewayRequest = {
  guid: request.guid,
  token: request.body.token,
  statistics_type: request.body.statistics_type,
  event_type: request.body.event_type
};
const response = {
  send: () => {
    return;
  }
};
const responseData = (type, data) => {
  return {
    type,
    data
  };
};

module.exports = {
  checkToken: {
    guid,
    request,
    sendParams: [ 'statistics', 'checkToken', gatewayRequest ],
    errorHelperParams: [
      'Statistics',
      apiCommonError,
      guid,
      response
    ],
    responseData: responseData('Statistics', response),
    responseType: 'response',
    response,
    responseError: {
      type: 'Company',
      error: apiCommonError
    },
    messageCall: 'Store user event'
  }
};
