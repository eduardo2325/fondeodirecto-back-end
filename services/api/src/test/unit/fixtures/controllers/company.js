const _ = require('lodash');
const { apiCommonError } = require('../../../common/fixtures/error');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const companyId = 1;
const rfc = 'FODA900806965';
const clabe = '002090700355936431';
const fee = 230.00;
const variable_fee_percentage = 13.33;

const response = {
  send: () => {
    return;
  }
};
const invitation = {
  id: 0,
  created_at: new Date().toString()
};

const companyNotFoundError = {
  path: 'Company',
  message: 'Not found'
};
const company = {
  id: companyId,
  rfc,
  role: 'ADMIN',
  name: 'Fondeo Directo',
  business_name: 'Fondeo Directo',
  holder: 'Fondeo Directo',
  bank: 'BANAMEX',
  bank_account: '70035593622',
  clabe: '002090700355936431',
  description: 'description',
  fee: 6
};
const bankInfo = {
  name: 'Fondeo',
  account: '123456789'
};
const newUser = {
  name: 'John Doe',
  email: 'john@doe.com',
  type: 'cxc'
};
const operationCost = {
  fd_commission: 1,
  annual_cost: 2,
  reserve: 3
};
const stringOperationCost = {
  fd_commission: '1.00',
  annual_cost: '2.00',
  reserve: '3.00'
};
const operationInvestor_cost = {
  fee: 5,
  variable_fee_percentage: 10
};
const companyWithOperationCost = {
  ...company,
  ...operationCost,
  active_cxc: 'true',
  active_cxp: 'false',
  prepayment: true
};
const responseData = (type, data) => {
  return {
    type,
    data
  };
};
const newCompany = {
  guid,
  rfc,
  name: company.name,
  business_name: company.business_name,
  holder: company.holder,
  clabe,
  role: 'COMPANY',
  operation_cost: operationCost,
  description: company.description
};

const newInvestorInvitationCompanyCall = {
  guid: guid,
  token: '123456',
  taxpayer_type: 'physical',
  name: 'Investor Invitation',
  email: 'investor-invitation-fixture@fondeodirecto.com',
  password: '123456',
  phone: '123456',
  age: 26,
  address: 'address',
  rfc: rfc,
  company_business_name: company.business_name
};
const investorInvitationCompany = {
  name: 'Investor Invitation',
  email: 'investor-invitation-fixture@fondeodirecto.com',
  status: '',
  role: 'INVESTOR',
  color: '#FF7372'
};
const newCompanyCall = {
  guid,
  rfc,
  name: company.name,
  business_name: company.business_name,
  holder: company.holder,
  clabe,
  role: 'COMPANY',
  operation_cost: stringOperationCost,
  description: company.description,
  suspended_roles: [ 'CXP' ],
  prepayment: true
};

const statementSummaries = {
  factoraje: {
    initial_ammount: '0',
    final_ammount: '0',
    payments: '0',
    acquisitions: '0'
  },
  cash: {
    initial_ammount: '0',
    final_ammount: '0',
    increments: '0',
    decrements: '0'
  }
};

const statementSummariesData = {
  factoraje: {
    initial_ammount: 0,
    final_ammount: 0,
    payments: 0,
    acquisitions: 0
  },
  cash: {
    initial_ammount: 0,
    final_ammount: 0,
    increments: 0,
    decrements: 0
  }
};

delete newCompanyCall.operation_cost.fee;

const companies = [ company ];
const usersList = {
  users: [ {
    name: 'John Doe',
    email: 'john@doe.com',
    color: '#1234',
    status: 'active'
  }, {
    name: 'Jane Doe',
    email: 'jane@doe.com',
    color: '#4321',
    status: 'pending'
  } ]
};
const exists = {
  exists: true
};
const newInvestorCompany = _.cloneDeep(newCompany);
const newInvestor = _.cloneDeep(newUser);

newInvestorCompany.role = 'INVESTOR';
newInvestorCompany.taxpayer_type = 'moral';
newInvestorCompany.fee = 5;
newInvestorCompany.variable_fee_percentage = 10;
newInvestorCompany.isFideicomiso = false;
newInvestorCompany.fideicomiso_fee = 5;
newInvestorCompany.fideicomiso_variable_fee = 10;

delete newInvestorCompany.description;

const newInvestorCompanyCall = _.cloneDeep(newInvestorCompany);

newInvestor.type = 'INVESTOR';

newInvestorCompanyCall.operation_cost.fee = '5.00';
newInvestorCompanyCall.operation_cost.variable_fee_percentage = '10.00';
newInvestorCompanyCall.operation_cost.fideicomiso_fee = '5.00';
newInvestorCompanyCall.operation_cost.fideicomiso_variable_fee = '10.00';
delete newInvestorCompanyCall.description;
// delete newInvestorCompanyCall.user_id;
delete newInvestorCompanyCall.operation_cost.annual_cost;
delete newInvestorCompanyCall.operation_cost.fd_commission;
delete newInvestorCompanyCall.operation_cost.reserve;
delete newInvestorCompanyCall.fee;
delete newInvestorCompanyCall.variable_fee_percentage;
delete newInvestorCompanyCall.fideicomiso_fee;
delete newInvestorCompanyCall.fideicomiso_variable_fee;

function obj2swaggerParams(value) {
  const obj = {};

  Object.keys(value).forEach(k => {
    if (typeof value[k] === 'object') {
      obj[k] = obj2swaggerParams(value[k]);
    } else {
      obj[k] = { value: value[k] };
    }
  });
  return obj;
}

module.exports = {
  create: {
    guid,
    request: {
      guid,
      swagger: {
        params: obj2swaggerParams(companyWithOperationCost)
      }
    },
    response,
    messageCall: 'Create company',
    responseType: 'response',
    responseError: {
      type: 'Company',
      error: apiCommonError
    },
    errorHelperParams: [
      'Company',
      apiCommonError,
      guid,
      response
    ],
    company,
    responseData: responseData('Company', company),
    sendParams: [ 'company', 'create', newCompanyCall ]
  },
  exists: {
    request: {
      guid,
      swagger: {
        params: {
          rfc: {
            value: rfc
          }
        }
      }
    },
    response,
    messageCall: 'Company Exists',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'exists', {
      guid,
      rfc
    } ],
    responseError: {
      type: 'CompanyExists',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'CompanyExists',
      companyNotFoundError,
      guid,
      response
    ],
    exists,
    responseData: responseData('CompanyExists', exists)
  },
  updateCompany: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      body: {
        name: company.name,
        business_name: company.business_name,
        holder: company.holder,
        clabe,
        description: company.description
      }
    },
    response,
    messageCall: 'Update company',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'update', {
      guid,
      id: companyId,
      name: company.name,
      business_name: company.business_name,
      holder: company.holder,
      clabe,
      description: company.description
    } ],
    responseError: {
      type: 'Company',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'Company',
      companyNotFoundError,
      guid,
      response
    ],
    company,
    responseData: responseData('Company', company)
  },
  updateCompanyOperationCost: {
    request: {
      guid,
      swagger: {
        params: {
          companyOperationCost: {
            value: {
              operation_cost: stringOperationCost
            }
          },
          id: {
            value: companyId
          }
        }
      }
    },
    response,
    messageCall: 'Update company operation cost',
    responseType: 'response',
    guid,
    operationCost,
    sendParams: [ 'company', 'updateOperationCost', {
      guid,
      id: companyId,
      ...stringOperationCost,
      role: 'COMPANY'
    } ],
    responseError: {
      type: 'OperationCost',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'OperationCost',
      companyNotFoundError,
      guid,
      response
    ],
    responseData: responseData('OperationCost', operationCost)
  },
  updateInvestorOperationCost: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          },
          investorOperationCost: {
            value: {
              operation_cost: operationInvestor_cost
            }
          }
        }
      }
    },
    response,
    messageCall: 'Update investor operation cost',
    responseType: 'response',
    guid,
    operationCost: {
      fee: operationInvestor_cost.fee,
      variable_fee_percentage: operationInvestor_cost.variable_fee_percentage
    },
    sendParams: [ 'company', 'updateOperationCost', {
      guid,
      id: companyId,
      fee: operationInvestor_cost.fee.toFixed(2),
      variable_fee_percentage: operationInvestor_cost.variable_fee_percentage.toFixed(2),
      role: 'INVESTOR'
    } ],
    responseError: {
      type: 'OperationCost',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'OperationCost',
      companyNotFoundError,
      guid,
      response
    ],
    responseData: responseData('OperationCost', {
      fee: operationInvestor_cost.fee,
      variable_fee_percentage: operationInvestor_cost.variable_fee_percentage
    })
  },
  getCompany: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      }
    },
    response,
    messageCall: 'Get company',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'getFullInformation', {
      guid,
      id: companyId
    } ],
    responseError: {
      type: 'Company',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'Company',
      companyNotFoundError,
      guid,
      response
    ],
    company,
    responseData: responseData('Company', company)
  },
  getCompanies: {
    request: {
      guid,
      query: {
        page_size: 2,
        page: 2
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'getCompanies', {
      guid,
      limit: 2,
      offset: 2,
      role: 'COMPANY'
    } ],
    companies,
    messageCall: 'Get companies',
    responseType: 'response',
    responseData: responseData('CompanyList', companies),
    responseError: {
      type: 'CompanyList',
      error: apiCommonError
    },
    errorHelperParams: [
      'CompanyList',
      apiCommonError,
      guid,
      response
    ]
  },
  getInvestorCompanies: {
    request: {
      guid,
      query: {
        page_size: 2,
        page: 2
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'getCompanies', {
      guid,
      limit: 2,
      offset: 2,
      role: 'INVESTOR'
    } ],
    companies,
    messageCall: 'Get investor companies',
    responseType: 'response',
    responseData: responseData('InvestorList', companies),
    responseError: {
      type: 'InvestorList',
      error: apiCommonError
    },
    errorHelperParams: [
      'InvestorList',
      apiCommonError,
      guid,
      response
    ]
  },
  getBankInfo: {
    request: {
      guid,
      swagger: {
        params: {
          clabe: {
            value: clabe
          }
        }
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'analyzeClabe', {
      guid,
      clabe
    } ],
    bankInfo,
    messageCall: 'Get Bank Info',
    responseType: 'response',
    responseData: responseData('BankInfo', bankInfo),
    responseError: {
      type: 'BankInfo',
      error: apiCommonError
    },
    errorHelperParams: [
      'BankInfo',
      apiCommonError,
      guid,
      response
    ]
  },
  clabeExists: {
    request: {
      guid,
      swagger: {
        params: {
          clabe: {
            value: clabe
          }
        }
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'analyzeClabe', {
      guid,
      clabe,
      exists: true
    } ],
    bankInfo,
    messageCall: 'Get Bank Info By Clabe',
    responseType: 'response',
    responseData: responseData('BankInfo', bankInfo),
    responseError: {
      type: 'BankInfo',
      error: apiCommonError
    },
    errorHelperParams: [
      'BankInfo',
      apiCommonError,
      guid,
      response
    ]
  },
  getCompanyUsers: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      query: {
        order_by: 'name',
        order_desc: 'false'
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'getUsers', {
      guid,
      id: companyId
    } ],
    sendParamsWithQuery: [ 'company', 'getUsers', {
      guid,
      id: companyId,
      order_by: 'name',
      order_desc: false
    } ],
    usersList,
    messageCall: 'Get company users',
    responseType: 'response',
    responseData: responseData('CompanyUsers', usersList),
    responseError: {
      type: 'CompanyUsers',
      error: apiCommonError
    },
    errorHelperParams: [
      'CompanyUsers',
      apiCommonError,
      guid,
      response
    ]
  },
  getCompanyOperationCost: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      }
    },
    response,
    messageCall: 'Get Company Operation Costs',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'getOperationCost', {
      guid,
      id: companyId,
      role: 'COMPANY'
    } ],
    responseError: {
      type: 'OperationCost',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'OperationCost',
      companyNotFoundError,
      guid,
      response
    ],
    operationCost,
    responseData: responseData('OperationCost', operationCost)
  },
  getInvestorOperationCost: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      }
    },
    response,
    messageCall: 'Get Investor Operation Costs',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'getOperationCost', {
      guid,
      id: companyId,
      role: 'INVESTOR'
    } ],
    responseError: {
      type: 'OperationCost',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'OperationCost',
      companyNotFoundError,
      guid,
      response
    ],
    operationInvestorCost: operationInvestor_cost,
    responseData: responseData('OperationCost', operationInvestor_cost)
  },
  addUser: {
    request: {
      guid,
      body: newUser,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'addUser', {
      guid,
      company_id: companyId,
      user: newUser
    } ],
    userInfo: newUser,
    messageCall: 'Add user to company',
    responseType: 'response',
    responseData: responseData('User', newUser),
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  createInvestor: {
    guid,
    request: {
      guid,
      swagger: {
        params: obj2swaggerParams(newInvestorCompany)
      }
    },
    response,
    messageCall: 'Create investor company',
    responseType: 'response',
    responseError: {
      type: 'Investor',
      error: apiCommonError
    },
    errorHelperParams: [
      'Investor',
      apiCommonError,
      guid,
      response
    ],
    company,
    responseData: responseData('Investor', company),
    sendParams: [ 'company', 'create', newInvestorCompanyCall ]
  },
  registerInvestor: {
    guid,
    request: {
      guid,
      body: {
        token: '123456',
        taxpayer_type: 'physical',
        name: 'Investor Invitation',
        email: 'investor-invitation-fixture@fondeodirecto.com',
        password: '123456',
        phone: '123456',
        age: 26,
        address: 'address',
        rfc: rfc,
        company_business_name: company.business_name
      }
    },
    response,
    messageCall: 'Create investor company',
    responseType: 'response',
    responseError: {
      type: 'Investor',
      error: apiCommonError
    },
    errorHelperParams: [
      'Investor',
      apiCommonError,
      guid,
      response
    ],
    investorInvitationCompany,
    responseData: responseData('Investor', investorInvitationCompany),
    sendParams: [ 'company', 'registerInvestor', newInvestorInvitationCompanyCall ]
  },
  addInvestorUser: {
    request: {
      guid,
      body: newInvestor,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'addUser', {
      guid,
      company_id: companyId,
      user: newInvestor
    } ],
    userInfo: newInvestor,
    messageCall: 'Add user to company',
    responseType: 'response',
    responseData: responseData('Investor', newInvestor),
    responseError: {
      type: 'Investor',
      error: apiCommonError
    },
    errorHelperParams: [
      'Investor',
      apiCommonError,
      guid,
      response
    ]
  },
  getFactorajeAndCashSummaries: {
    request: {
      guid,
      swagger: {
        params: {
          company_id: {
            value: companyId
          }
        }
      },
      body: {
        year: 2018,
        month: 9
      },
      user: {
        user_id: 1,
        company_id: 1
      }
    },
    guid,
    response,
    messageCall: 'Get investor summaries',
    responseType: 'response',
    sendParams: [ 'company', 'getFactorajeAndCashSummaries', {
      guid,
      year: 2018,
      month: 9,
      company_id: 1
    } ],
    responseData: responseData('Investor', statementSummariesData),
    userInfo: statementSummaries,
    responseError: {
      type: 'Investor',
      error: apiCommonError
    },
    errorHelperParams: [
      'Investor',
      apiCommonError,
      guid,
      response
    ]
  },
  createInvestorInvitation: {
    request: {
      guid,
      user: {
        user_id: 1
      },
      body: {
        name: 'Investor',
        company_name: 'Investor inc',
        email: 'investor-fixture@fondeodirecto.com',
        fee: fee,
        issuer: 1,
        variable_fee_percentage: variable_fee_percentage
      }
    },
    response,
    guid,
    sendParams: [ 'company', 'createInvestorInvitation', {
      guid,
      name: 'Investor',
      company_name: 'Investor inc',
      email: 'investor-fixture@fondeodirecto.com',
      fee: fee.toFixed(2),
      issuer: 1,
      variable_fee_percentage: variable_fee_percentage.toFixed(2)
    } ],
    userInfo: invitation,
    messageCall: 'Create investor invitation',
    responseType: 'response',
    responseData: responseData('Company', invitation),
    responseError: {
      type: 'Company',
      error: apiCommonError
    },
    errorHelperParams: [
      'Company',
      apiCommonError,
      guid,
      response
    ]
  },
  getBalance: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      user: {
        user_id: 1
      }
    },
    response,
    messageCall: 'Get company balance',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'getBalance', {
      guid,
      id: companyId,
      user_id: 1
    } ],
    responseError: {
      type: 'Balance',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'Balance',
      companyNotFoundError,
      guid,
      response
    ],
    balance: {
      total: '100.00'
    },
    responseData: responseData('Balance', {
      total: '100.00'
    })
  },
  updateCompanyRoleSuspension: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      user: {
        user_id: 1
      },
      body: {
        role: 'cxc',
        suspended: true
      }
    },
    response,
    messageCall: 'Update company role suspension',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'updateCompanyRoleSuspension', {
      guid,
      id: companyId,
      role: 'CXC',
      suspended: true
    } ],
    responseError: {
      type: 'Role',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'Role',
      companyNotFoundError,
      guid,
      response
    ],
    responseData: responseData('Role', {
      success: true
    })
  },
  updateInvestorRoleSuspension: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      user: {
        user_id: 1
      },
      body: {
        role: 'investor',
        suspended: true
      }
    },
    response,
    messageCall: 'Update investor role suspension',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'updateInvestorRoleSuspension', {
      guid,
      id: companyId,
      role: 'INVESTOR',
      suspended: true
    } ],
    responseError: {
      type: 'Role',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'Role',
      companyNotFoundError,
      guid,
      response
    ],
    responseData: responseData('Role', {
      success: true
    })
  },
  propose: {
    request: {
      guid,
      user: {
        company_id: companyId
      },
      body: {
        business_name: 'business_name',
        type: 'Proveedor',
        contact_name: 'contact_name',
        position: 'position',
        email: 'email',
        phone: 'phone'
      }
    },
    response,
    messageCall: 'Propose a company',
    responseType: 'response',
    success: {
      success: true
    },
    guid,
    sendParams: [ 'company', 'propose', {
      guid,
      company_id: companyId,
      business_name: 'business_name',
      type: 'Proveedor',
      contact_name: 'contact_name',
      position: 'position',
      email: 'email',
      phone: 'phone'
    } ],
    responseError: {
      type: 'Company',
      error: apiCommonError
    },
    errorHelperParams: [
      'Company',
      apiCommonError,
      guid,
      response
    ],
    responseData: responseData('Company', {
      success: true
    })
  },
  agreed: {
    request: {
      guid,
      user: {
        user_id: 123,
        user_role: 'CXC'
      },
      swagger: {
        params: obj2swaggerParams({ id: companyId })
      }
    },
    response,
    sendParams: {
      role: 'CXC',
      user: 123,
      guid,
      id: companyId
    },
    responseData: responseData('Company', {
      success: true
    }),
    guid,
    apiCommonError,
    successfulResponse: {
      success: true
    }
  },
  getInvestorTransactions: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      body: {
        year: 2018,
        month: 8
      }
    },
    response,
    messageCall: 'Get an investor\'s transactions',
    responseType: 'response',
    success: {
      success: true
    },
    guid,
    sendParams: [ 'company', 'getInvestorTransactions', {
      guid,
      id: companyId,
      year: 2018,
      month: 8
    } ],
    responseError: {
      type: 'Investor',
      error: apiCommonError
    },
    errorHelperParams: [
      'Investor',
      apiCommonError,
      guid,
      response
    ],
    responseData: responseData('Investor', {
      success: true
    })
  },
  updateCompanyPrepayment: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: companyId
          }
        }
      },
      body: {
        prepayment: true
      }
    },
    prepaymentSuccess: {
      success: true
    },
    response,
    messageCall: 'Update company prepayment',
    responseType: 'response',
    guid,
    sendParams: [ 'company', 'updateCompanyPrepayment', {
      guid,
      id: companyId,
      prepayment: true
    } ],
    responseError: {
      type: 'Company',
      error: companyNotFoundError
    },
    errorHelperParams: [
      'Company',
      companyNotFoundError,
      guid,
      response
    ],
    responseData: responseData('Company', {
      success: true
    })
  }
};
