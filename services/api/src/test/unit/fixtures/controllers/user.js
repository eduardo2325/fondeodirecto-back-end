const { apiCommonError } = require('../../../common/fixtures/error');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const token = 'FsRCWfsmBEFxpsGy';
const response = {
  send: () => {
    return;
  }
};
const filenameTracker = 'tracker-' + new Date().toDateString().replace(/ /g, '-');
const wb = {};
const ws = {};
const responseWithSet = {
  send: () => {
    return;
  },
  set: () => {
    return;
  }
};
const trackerInfo = {
  trackerInfo: [ {
    id: '',
    published_date: '',
    invoice_id: '',
    invoice_status: '',
    total: '',
    reserve: '',
    cxc_name: '',
    cxp_name: '',
    payment_date: '',
    fund_payment: '',
    annual_cost: '',
    op_status: '',
    investor_name: '',
    isr: '',
    fd_commission: '',
    fideicomiso_fee: '',
    fund_request_date: '',
    transaction_status: '',
    cxp_payment_date: ''
  } ]
};

const user = {
  id: 1,
  name: 'Fondeo',
  email: 'test@fondeo.com',
  company: {
    rfc: 'FODA900806965'
  }
};
const simpleUser = {
  name: 'name',
  email: 'email',
  role: 'type'
};
const roles = [
  {
    name: 'Cuentas por cobrar',
    value: 'CXC'
  },
  {
    name: 'Cuentas por pagar',
    value: 'CXP'
  }
];
const passwordValues = {
  actual_password: 'current password',
  new_password: 'new super secret password',
  confirmation_password: 'new super secret password'
};
const passwordConfirmationError = {
  path: 'password_confirmation',
  message: 'password_confirmation is different'
};
const confirmationPasswordError = {
  path: 'confirmation_password',
  message: 'confirmation_password is different'
};

module.exports = {
  getInvitation: {
    request: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      }
    },
    response,
    user,
    messageCall: 'Get invitation',
    responseType: 'response',
    guid,
    sendParams: [ 'user', 'getInvitation', {
      guid,
      token
    } ],
    responseData: {
      type: 'User',
      data: user
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  registration: {
    request: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      },
      body: {
        name: 'name',
        password: '123456',
        password_confirmation: '123456'
      }
    },
    invalidRequest: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      },
      body: {
        name: 'name',
        password: '123456',
        password_confirmation: '123'
      }
    },
    response,
    sendParams: [ 'user', 'registration', {
      guid,
      token,
      name: 'name',
      password: '123456'
    } ],
    user: simpleUser,
    guid,
    messageCall: 'Registration',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: simpleUser
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ],
    passwordConfirmationErrorHelperParams: [
      'User',
      passwordConfirmationError,
      guid,
      response
    ]
  },
  getRoles: {
    request: {
      guid
    },
    response,
    roles: {
      roles: roles
    },
    messageCall: 'Get user roles',
    responseType: 'response',
    guid,
    sendParams: [ 'user', 'getRoles', {
      guid
    } ],
    responseData: {
      type: 'UserRoles',
      data: roles
    },
    responseError: {
      type: 'UserRoles',
      error: apiCommonError
    },
    errorHelperParams: [
      'UserRoles',
      apiCommonError,
      guid,
      response
    ]
  },
  delete: {
    request: {
      guid,
      swagger: {
        params: {
          email: {
            value: user.email
          }
        }
      }
    },
    response,
    sendParams: [ 'user', 'delete', {
      guid,
      email: user.email
    } ],
    user: simpleUser,
    guid,
    messageCall: 'Delete user',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: simpleUser
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  checkEmail: {
    request: {
      guid,
      swagger: {
        params: {
          email: {
            value: user.email
          }
        }
      }
    },
    response,
    sendParams: [ 'user', 'checkEmail', {
      guid,
      email: user.email
    } ],
    user: { exists: true },
    guid,
    messageCall: 'Check email user',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: { exists: true }
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  resendInvitation: {
    request: {
      guid,
      swagger: {
        params: {
          email: {
            value: user.email
          }
        }
      }
    },
    response,
    sendParams: [ 'user', 'resendInvitation', {
      guid,
      email: user.email
    } ],
    guid,
    messageCall: 'Resend invitation',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: {
        success: true
      }
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  changePassword: {
    request: {
      guid,
      user: {
        user_id: user.id
      },
      body: {
        actual_password: passwordValues.actual_password,
        new_password: passwordValues.new_password,
        confirmation_password: passwordValues.confirmation_password
      }
    },
    response,
    sendParams: [ 'user', 'changePassword', {
      guid,
      user_id: user.id,
      actual_password: passwordValues.actual_password,
      new_password: passwordValues.new_password
    } ],
    guid,
    user,
    messageCall: 'Change password',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: user
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ],
    passwordConfirmationErrorHelperParams: [
      'User',
      confirmationPasswordError,
      guid,
      response
    ]
  },
  recoverPassword: {
    request: {
      guid,
      body: {
        email: user.email
      }
    },
    response,
    sendParams: [ 'user', 'generatePasswordToken', {
      guid,
      email: user.email
    } ],
    guid,
    messageCall: 'Recover password',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: {
        success: true
      }
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  validateRecoverToken: {
    request: {
      guid,
      query: {
        token
      }
    },
    response,
    sendParams: [ 'user', 'validateRecoverToken', {
      guid,
      token
    } ],
    guid,
    messageCall: 'Validate recover token',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: {
        token,
        email: user.email
      }
    },
    validToken: {
      token,
      email: user.email
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  resetPassword: {
    request: {
      guid,
      body: {
        password: '123456',
        confirmation_password: '123456'
      },
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      }
    },
    invalidRequest: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      },
      body: {
        password: '123456',
        confirmation_password: '123'
      }
    },
    response,
    guid,
    user,
    sendParams: [ 'user', 'resetPassword', {
      guid,
      token,
      password: '123456'
    } ],
    messageCall: 'Reset password',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: user
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ],
    passwordConfirmationErrorHelperParams: [
      'User',
      confirmationPasswordError,
      guid,
      response
    ]
  },
  downloadTracker: {
    ws,
    wb,
    request: {
      guid
    },
    sendParams: [ 'user', 'downloadTracker', { guid } ],
    response: responseWithSet,
    guid,
    buffer: {},
    bookAppendSheetParams: [ wb, ws, 'SheetJS' ],
    sheetJs: 'SheetJS',
    writeSecondParam: { type: 'buffer', bookType: 'xlsx' },
    setParams: {
      'Content-Disposition': "inline; filename='" + filenameTracker + ".xlsx'"
    },
    messageCall: 'Download tracker',
    responseType: 'response',
    responseData: {
      type: 'User',
      data: trackerInfo
    },
    trackerInfo,
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      responseWithSet
    ]
  },
  findExcelTransactions: {
    request: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      }
    },
    response,
    user: {
      total_amount: '1.0',
      transactions: [ { amount: '1.0' } ]
    },
    messageCall: 'Find excel transactions',
    responseType: 'response',
    guid,
    sendParams: [ 'user', 'findExcelTransactions', {
      guid
    } ],
    responseData: {
      type: 'User',
      data: {
        total_amount: parseFloat('1.0'),
        transactions: [ { amount: parseFloat('1.0') } ]
      }
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  generateExcelTransactions: {
    request: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      },
      body: {
        pending_transactions: []
      },
      user: {
        user_id: 1
      }
    },
    response,
    user,
    messageCall: 'Generate excel transactions',
    responseType: 'response',
    guid,
    sendParams: [ 'user', 'generateExcelTransactions', {
      pending_transactions: [],
      user_id: 1,
      guid
    } ],
    responseData: {
      type: 'User',
      data: user
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  excelBankReports: {
    request: {
      guid,
      swagger: {
        params: {
          token: {
            value: token
          }
        }
      },
      body: {
        pending_transactions: []
      },
      user: {
        user_id: 1
      }
    },
    response,
    user: {
      excel_files: [
        { amount: '1.0' }
      ]
    },
    messageCall: 'Excel bank reports',
    responseType: 'response',
    guid,
    sendParams: [ 'user', 'excelBankReports', {
      guid
    } ],
    responseData: {
      type: 'User',
      data: {
        excel_files: [
          { amount: parseFloat('1.0') }
        ]
      }
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  },
  findExcelTransactionsFromBankReportId: {
    request: {
      guid,
      swagger: {
        params: {
          id: {
            value: 3
          }
        }
      }
    },
    response,
    user: {
      total_amount: '1.0',
      transactions: [ { amount: '1.0' } ]
    },
    messageCall: 'Find excel transactions from bank report',
    responseType: 'response',
    guid,
    sendParams: [ 'user', 'findExcelTransactionsFromBankReportId', {
      bank_report_id: 3,
      guid
    } ],
    responseData: {
      type: 'User',
      data: {
        total_amount: parseFloat('1.0'),
        transactions: [ { amount: parseFloat('1.0'), selected: false } ]
      }
    },
    responseError: {
      type: 'User',
      error: apiCommonError
    },
    errorHelperParams: [
      'User',
      apiCommonError,
      guid,
      response
    ]
  }
};
