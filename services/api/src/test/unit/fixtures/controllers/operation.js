const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const { apiCommonError } = require('../../../common/fixtures/error');
const response = {
  send: () => {
    return;
  },
  set: () => {
    return;
  }
};
const responseData = (type, data) => {
  return {
    type,
    data
  };
};
const newOperation = {
  id: 1,
  invoice_id: 10049,
  user_id: 1001
};

const operationList = [ newOperation ];
const gatewayResult = {
  operations: operationList
};

module.exports = {

  getOperations: {
    request: {
      guid,
      query: {
        page_size: 2,
        page: 2
      },
      user: {
        user_id: 1
      }
    },
    requestOrderBy: {
      guid,
      query: {
        page_size: 2,
        page: 2,
        order_by: 'expiration_date',
        order_desc: 'true'
      },
      user: {
        user_id: 1
      }
    },
    sendParamsOrderBy: [ 'operation', 'getOperations', {
      guid,
      limit: 2,
      offset: 2,
      user_id: 1,
      order_by: 'expiration_date',
      order_desc: true
    } ],
    response,
    gatewayResult,
    guid,
    sendParams: [ 'operation', 'getOperations', {
      guid,
      limit: 2,
      offset: 2,
      user_id: 1
    } ],
    operationList,
    messageCall: 'Get operations',
    responseType: 'response',
    responseData: responseData('OperationList', gatewayResult),
    responseError: {
      type: 'OperationList',
      error: apiCommonError
    },
    errorHelperParams: [
      'OperationList',
      apiCommonError,
      guid,
      response
    ]
  },
  getOperation: {
    request: {
      guid,
      user: {
        user_id: 1
      },
      swagger: {
        params: {
          id: {
            value: 1
          }
        }
      }
    },
    response,
    guid,
    sendParams: [ 'operation', 'getOperation', {
      guid,
      operation_id: 1,
      user_id: 1
    } ],
    operation: newOperation,
    messageCall: 'Get operation',
    responseType: 'response',
    responseData: responseData('Operation', newOperation),
    responseError: {
      type: 'Operation',
      error: apiCommonError
    },
    errorHelperParams: [
      'Operation',
      apiCommonError,
      guid,
      response
    ]
  },
  getAdminOperations: {
    request: {
      guid,
      user: {
        user_id: 1
      },
      query: {
        page_size: 25,
        page: 1,
        order_by: 'Something',
        order_desc: 'true',
        client_name: 'client',
        company_name: 'company',
        investor_name: 'investor',
        status: 'approved',
        start_fund_date: 'Tue Aug 01 2017 19:49:29 GMT+0000 (UTC)',
        end_fund_date: 'Tue Aug 03 2017 19:49:29 GMT+0000 (UTC)',
        start_expiration_date: 'Tue Aug 05 2017 19:49:29 GMT+0000 (UTC)',
        end_expiration_date: 'Tue Aug 10 2017 19:49:29 GMT+0000 (UTC)'
      }
    },
    requestInvalidStartFundDate: {
      guid,
      query: {
        start_fund_date: 'something'
      },
      user: {
        user_id: 1
      }
    },
    requestInvalidEndFundDate: {
      guid,
      query: {
        end_fund_date: 'something'
      },
      user: {
        user_id: 1
      }
    },
    requestInvalidStartExpirationDate: {
      guid,
      query: {
        start_expiration_date: 'something'
      },
      user: {
        user_id: 1
      }
    },
    requestInvalidEndExpirationDate: {
      guid,
      query: {
        end_expiration_date: 'something'
      },
      user: {
        user_id: 1
      }
    },
    response,
    guid,
    sendParams: [ 'operation', 'getAdminOperations', {
      guid,
      user_id: 1,
      limit: 25,
      offset: 1,
      order_by: 'Something',
      order_desc: true,
      client_name: 'client',
      company_name: 'company',
      investor_name: 'investor',
      status: 'approved',
      start_fund_date: 'Tue Aug 01 2017 19:49:29 GMT+0000 (UTC)',
      end_fund_date: 'Tue Aug 03 2017 19:49:29 GMT+0000 (UTC)',
      start_expiration_date: 'Tue Aug 05 2017 19:49:29 GMT+0000 (UTC)',
      end_expiration_date: 'Tue Aug 10 2017 19:49:29 GMT+0000 (UTC)'
    } ],
    operationList,
    messageCall: 'Get admin operations',
    responseType: 'response',
    responseData: responseData('OperationList', operationList),
    responseError: {
      type: 'OperationList',
      error: apiCommonError
    },
    errorHelperParams: [
      'InvoiceList',
      apiCommonError,
      guid,
      response
    ],
    startFundDateError: [
      'InvoiceList',
      {
        path: 'start_fund_date',
        message: 'Invalid start_fund_date'
      },
      guid,
      response
    ],
    endFundDateError: [
      'InvoiceList',
      {
        path: 'end_fund_date',
        message: 'Invalid end_fund_date'
      },
      guid,
      response
    ],
    startExpirationDateError: [
      'InvoiceList',
      {
        path: 'start_expiration_date',
        message: 'Invalid start_expiration_date'
      },
      guid,
      response
    ],
    endExpirationDateError: [
      'InvoiceList',
      {
        path: 'end_expiration_date',
        message: 'Invalid end_expiration_date'
      },
      guid,
      response
    ]
  }
};
