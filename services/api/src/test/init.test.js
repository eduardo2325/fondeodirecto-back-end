const consumer = require('./functional/helpers/consumer');
const app = require('../app');

before(() => Promise.all([ app.initPromise, consumer.setup() ]));

// after is not working after failures...
process.on('exit', () => {
  app.shutdown();
  consumer.teardown();
});
