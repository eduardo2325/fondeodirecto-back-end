/* eslint no-process-exit: 0 */

const express = require('express');
const SwaggerRunner = require('swagger-node-runner');
const swaggerUi = require('./api/swagger/swagger-ui');
const swaggerParser = require('./api/swagger/parser');
const app = express();
const port = process.env.PORT || 3000;
const useragent = require('express-useragent');
const log = new (require('/var/lib/core/js/log'))(module);
const scheduler = require('./vendor/scheduler');

const SwaggerExpress = {
  create(config, cb) {
    SwaggerRunner.create(config, (err, runner) => {
      if (err) {
        cb(err);
        return;
      }
      cb(undefined, runner.expressMiddleware());
    });
  }
};

const swaggerConfig = {
  appRoot: __dirname,
  swaggerSecurityHandlers: {
    Admin: require('./middleware/authentication'),
    Investor: require('./middleware/authentication'),
    Cxc: require('./middleware/authentication'),
    Cxp: require('./middleware/authentication')
  }
};

let server;

app.enable('trust proxy');

app.use('/public', express.static('public'));


/* istanbul ignore next */
function initScheduler() {
  return scheduler.init()
    .then(() => log.message('Scheduled jobs loaded', {}, 'App'));
}

/* istanbul ignore next */
function initServer() {
  return swaggerParser.getJson('./api/swagger/swagger.yaml')
    .then(schema => {
      swaggerConfig.swagger = schema;

      return new Promise((resolve, reject) => {
        SwaggerExpress.create(swaggerConfig, (err, swaggerExpress) => {
          if (err) {
            if (err.validationErrors) {
              err.validationErrors.forEach(e => {
                /* eslint-disable-next-line */
                console.log(e);
              });
            } else {
              /* eslint-disable-next-line */
              console.log(err);
            }

            return reject(err);
          }

          app.use(useragent.express());
          app.use(require('./middleware/log'));

          const dispatch = swaggerExpress.middleware();

          app.use((req, res, next) => {
            // patch for "Error: req.files must be provided for 'formData' parameters of type 'file'" issues
            if (req.method === 'POST' || req.method === 'PUT') {
              req.files = req.files || [];
            }

            dispatch(req, res, next);
          });

          swaggerExpress.runner.on('responseValidationError', function(validationResponse, request) {
            log.error('Response validation error', request.guid, validationResponse.errors);
          });


          server = app.listen(port);

          server.forceShutdown = () => {
            return new Promise(_resolve => {
              server.close(() => {
                return _resolve();
              });
            });
          };

          log.message(`Server listening on port ${port}`, {}, 'App');

          if (process.env.NODE_ENV !== 'production') {
            swaggerUi(app, schema);
          }

          return resolve();
        });
      });
    });
}

/* istanbul ignore next */
function shutdown() {
  return Promise.resolve()
    .then(() => server.forceShutdown())
    .then(() => {
      log.message('Shutdown completed', {}, 'App');
      process.exit(0);
    })
    .catch(err => {
      log.error(err, '', { message: 'Shutdown error' });
      process.exit(1);
    });
}

/* istanbul ignore next */
const initPromise = Promise.resolve()
  .then(() => initServer())
  .then(() => initScheduler())
  .catch(err => {
    log.error(err, '', { message: 'Unable to initialize server' });
    return shutdown();
  });

module.exports = {
  initPromise,
  shutdown,
  server
};
