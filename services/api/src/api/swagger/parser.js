const JsonRefs = require('json-refs');
const YAML = require('js-yaml');

/* istanbul ignore next */
function getJson(path) {
  return JsonRefs.resolveRefsAt(path, {
    filter: [ 'relative', 'remote' ],
    loaderOptions: {
      processContent: (res, cb) => {
        try {
          cb(undefined, YAML.safeLoad(res.text));
        } catch (e) {
          /* eslint-disable */
          console.log(e.message);
          process.exit(1);
          /* eslint-enable */
        }
      }
    }
  })
    .then(results => results.resolved);
}

module.exports.getJson = getJson;
