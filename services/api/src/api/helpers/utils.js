const fs = require('fs');
const s3 = require('/var/lib/core/js/s3');

module.exports = {
  readFile: function(path) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  },
  unlink: function(path) {
    return new Promise((resolve, reject) => {
      fs.unlink(path, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  },
  readDir: function(path) {
    return new Promise((resolve, reject) => {
      fs.readdir(path, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  },

  validateMime(file, mimetype) {
    if (file.mimetype === mimetype) {
      return true;
    }
    return false;
  },

  uploadToS3({ file, company_rfc, guid }, baseDir, groupDir) {
    const originalname = file.originalname;
    const timestamp = new Date().getTime();
    const filename = `${timestamp}.${originalname.split('.').pop()}`;
    const key = `${baseDir}/${company_rfc}/${groupDir}/${filename}`;

    return Promise.resolve()
      .then(() => {
        return file.buffer || this.readFile(file.path);
      })
      .then(fileInfo => {
        return s3.upload(key, fileInfo, guid);
      });
  }
};
