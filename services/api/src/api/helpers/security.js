const { suspendedOperationsByRole } = require('../../config/security');

function operationIsAllowed(req, scope, user) {
  const { user_role, suspended } = user;
  const { method, pathObject } = req.swagger.operation;
  const { path } = pathObject;

  if (scope.indexOf(user_role) === -1) {
    return 'Unauthorized role';
  }

  if (suspended && suspendedOperationsByRole[user_role].indexOf(method + ' ' + path) !== -1) {
    if (user_role === 'INVESTOR') {
      return 'Suspended investor role';
    }

    return 'Suspended company role';
  }

  if (user_role !== 'ADMIN' && !user.company_agreed && !/(?:\/login|logout|balance)/.test(req.url)) {
    // allow only the /companies/{rfc}/agreement endpoint, otherwise the update will never happen!
    if (!(pathObject['x-swagger-router-controller'] === 'CompaniesController'
      && pathObject.post.operationId === 'agreed')) {
      return 'Company has not accepted agreement';
    }
  }

  return null;
}

module.exports = {
  operationIsAllowed
};
