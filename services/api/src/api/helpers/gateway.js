const fs = require('fs');
const path = require('path');
const protosPath = '/var/lib/core/protos';
const grpc = require('grpc');
const timeout = process.env.GRPC_TIMEOUT || 10;
const { services } = require('../../config/config');

class Gateway {
  constructor() {
    const credentials = grpc.credentials.createInsecure();
    const directories = this.getDirectories(protosPath);

    directories.forEach(directory => {
      if (directory === 'api') {
        return;
      }

      this[directory] = {};

      const files = this.getFiles(`${protosPath}/${directory}/`);

      files.forEach(file => {
        const load = grpc.load(`${protosPath}/${directory}/${file}.proto`)[file];
        const uppercase = `${file.charAt(0).toUpperCase()}${file.slice(1)}`;
        const address = services[directory] || `${directory}:80`;

        this[directory][file] = new load[uppercase](address, credentials);
      });
    });
  }

  request(client, method, data) {
    let givenResult;

    /* istanbul ignore next */
    return new Promise((resolve, reject) => {
      const deadline = new Date();

      deadline.setSeconds(deadline.getSeconds() + timeout);

      client[method](data, { deadline }, (error, result) => {
        givenResult = result;

        if (error) {
          let parsedError;

          try {
            parsedError = JSON.parse(error.details || error.message);
          } catch (e) {
            parsedError = {
              path: 'Internal server error',
              message: error.details || error.message
            };
          }

          return reject(parsedError);
        }

        return resolve(result);
      });
    }).catch(error => {
      if (givenResult && process.env.NODE_ENV !== 'production') {
        /* eslint-disable */
        console.log('---');
        console.log('E_REQ', `#${method}`, data);
        console.log('E_RESP', givenResult);
        console.log('E_FAILED', error);
        /* eslint-enable */
      }

      throw error;
    });
  }

  getDirectories(srcpath) {
    return fs.readdirSync(srcpath)
      .filter(file => fs.statSync(path.join(srcpath, file)).isDirectory());
  }

  getFiles(srcpath) {
    return fs.readdirSync(srcpath)
      .filter(file => fs.statSync(path.join(srcpath, file)).isFile())
      .map(file => {
        return file.slice(0, -6);
      });
  }

  sendUser(service, method, data) {
    return this.request(this.user[service], method, data);
  }

  sendCfdi(service, method, data) {
    return this.request(this.cfdi[service], method, data);
  }
}

module.exports = new Gateway();
