const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  getInvitation(req, res) {
    const request = {
      guid: req.guid,
      token: req.swagger.params.token.value
    };

    return gateway.sendUser('user', 'getInvitation', request)
      .then(invitation => {
        const response = {
          type: 'User',
          data: invitation
        };

        log.message('Get invitation', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  registration(req, res) {
    const userData = {
      guid: req.guid,
      token: req.swagger.params.token.value,
      name: req.body.name,
      password: req.body.password
    };

    return Promise.resolve()
      .then(() => {
        if (req.body.password !== req.body.password_confirmation) {
          return Promise.reject({
            path: 'password_confirmation',
            message: 'password_confirmation is different'
          });
        }

        return gateway.sendUser('user', 'registration', userData);
      })
      .then(user => {
        const response = {
          type: 'User',
          data: user
        };

        log.message('Registration', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  getRoles(req, res) {
    const rolesRequest = {
      guid: req.guid
    };

    return gateway.sendUser('user', 'getRoles', rolesRequest)
      .then(data => {
        const response = {
          type: 'UserRoles',
          data: data.roles
        };

        log.message('Get user roles', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('UserRoles', error, req.guid, res));
  },

  delete(req, res) {
    const userData = {
      guid: req.guid,
      email: req.swagger.params.email.value
    };

    return Promise.resolve()
      .then(() => gateway.sendUser('user', 'delete', userData))
      .then(user => {
        const response = {
          type: 'User',
          data: user
        };

        log.message('Delete user', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  checkEmail(req, res) {
    const userData = {
      guid: req.guid,
      email: req.swagger.params.email.value
    };

    return gateway.sendUser('user', 'checkEmail', userData)
      .then(user => {
        const response = {
          type: 'User',
          data: user
        };

        log.message('Check email user', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  resendInvitation(req, res) {
    const userData = {
      guid: req.guid,
      email: req.swagger.params.email.value
    };

    return gateway.sendUser('user', 'resendInvitation', userData)
      .then(() => {
        const response = {
          type: 'User',
          data: {
            success: true
          }
        };

        log.message('Resend invitation', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  changePassword(req, res) {
    const passwordData = {
      guid: req.guid,
      user_id: req.user.user_id,
      actual_password: req.body.actual_password,
      new_password: req.body.new_password
    };

    return Promise.resolve().then(() => {
      const { new_password, confirmation_password } = req.body;

      if (new_password !== confirmation_password) {
        return Promise.reject({
          path: 'confirmation_password',
          message: 'confirmation_password is different'
        });
      }

      return gateway.sendUser('user', 'changePassword', passwordData);
    }).then(user => {
      const response = {
        type: 'User',
        data: user
      };

      log.message('Change password', response, 'response', req.guid);

      return res.send(response);
    })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  recoverPassword(req, res) {
    const data = {
      guid: req.guid,
      email: req.body.email
    };

    return gateway.sendUser('user', 'generatePasswordToken', data)
      .then(() => {
        const response = {
          type: 'User',
          data: {
            success: true
          }
        };

        log.message('Recover password', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  validateRecoverToken(req, res) {
    const data = {
      guid: req.guid,
      token: req.query.token
    };

    return gateway.sendUser('user', 'validateRecoverToken', data)
      .then(token => {
        const response = {
          type: 'User',
          data: token
        };

        log.message('Validate recover token', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  resetPassword(req, res) {
    const passwordData = {
      guid: req.guid,
      token: req.swagger.params.token.value,
      password: req.body.password
    };

    return Promise.resolve().then(() => {
      const { password, confirmation_password } = req.body;

      if (password !== confirmation_password) {
        return Promise.reject({
          path: 'confirmation_password',
          message: 'confirmation_password is different'
        });
      }

      return gateway.sendUser('user', 'resetPassword', passwordData);
    }).then(user => {
      const response = {
        type: 'User',
        data: user
      };

      log.message('Reset password', response, 'response', req.guid);

      return res.send(response);
    })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  downloadTracker(req, res) {
    const data = [
      [
        'ID OP',
        'OP Date (published)',
        'Invoice',
        'Invoice Status',
        'Invoice Total',
        'Reserve',
        'Seller (CxC)',
        'Buyer (CxP)',
        'Payment Date',
        'Days',
        'Payment',
        'Anual rate (%)',
        'Intereses',
        'Operation Fee $',
        'Suppliers cost $',
        'OP_Status',
        'Investor',
        'Intereses',
        'ISR 20%',
        'Fee FD',
        'Fee Fide',
        'Started Funding',
        'Trans_Status',
        'Payment_date'
      ]
    ];

    const request = {
      guid: req.guid
    };

    return gateway.sendUser('user', 'downloadTracker', request)
      .then( (infos) => {
        log.message('Download tracker', infos, 'response', req.guid);

        infos.trackerInfo.forEach( info => {
          const detailData = [
            info.id,
            info.published_date,
            info.invoice_id,
            info.invoice_status,
            info.total,
            info.reserve,
            info.cxc_name,
            info.cxp_name,
            info.payment_date,
            '',
            info.fund_payment,
            info.annual_cost,
            '',
            '',
            '',
            info.op_status,
            info.investor_name,
            '',
            info.isr,
            info.fd_commission,
            info.fideicomiso_fee,
            info.fund_request_date,
            info.transaction_status,
            info.cxp_payment_date
          ];

          data.push(detailData);
        });

        const response = {
          excel_data: data
        };

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res) );
  },

  findExcelTransactions(req, res) {
    const request = {
      guid: req.guid
    };

    return gateway.sendUser('user', 'findExcelTransactions', request)
      .then( (userData) => {
        userData.total_amount = parseFloat(userData.total_amount);

        userData.transactions.forEach( transaction => {
          transaction.amount = parseFloat(transaction.amount);
        });

        const response = {
          type: 'User',
          data: userData
        };

        log.message('Find excel transactions', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res) );
  },

  generateExcelTransactions(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      pending_transactions: req.body.pending_transactions
    };

    return gateway.sendUser('user', 'generateExcelTransactions', request)
      .then( (userData) => {
        const response = {
          type: 'User',
          data: userData
        };

        log.message('Generate excel transactions', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res) );
  },

  excelBankReports(req, res) {
    const request = {
      guid: req.guid
    };

    return gateway.sendUser('user', 'excelBankReports', request)
      .then( (userData) => {
        userData.excel_files.forEach( element => {
          element.amount = parseFloat(element.amount);
        });

        const response = {
          type: 'User',
          data: userData
        };

        log.message('Excel bank reports', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res) );
  },

  findExcelTransactionsFromBankReportId(req, res) {
    const request = {
      guid: req.guid,
      bank_report_id: req.swagger.params.id.value
    };

    return gateway.sendUser('user', 'findExcelTransactionsFromBankReportId', request)
      .then( (userData) => {
        userData.total_amount = parseFloat(userData.total_amount);

        userData.transactions.forEach( transaction => {
          transaction.amount = parseFloat(transaction.amount);
          transaction.selected = false;
        });

        const response = {
          type: 'User',
          data: userData
        };

        log.message('Find excel transactions from bank report', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res) );
  }
};
