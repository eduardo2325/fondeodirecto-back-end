const _ = require('lodash');

const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  create(req, res) {
    const validateData = {
      guid: req.guid
    };

    let stringCfdi;

    return Promise.resolve()
      .then(() => req.swagger.params.invoice.value.buffer)
      .then(xml => {
        stringCfdi = xml.toString();
        validateData.xml = stringCfdi;

        return gateway.sendCfdi('invoice', 'validateAndExtract', validateData);
      }).then(invoice => {
        const parsedDigest = JSON.parse(invoice.digest);
        const items = _.map(parsedDigest.items, function(item) {
          return {
            count: item.count.toFixed(2),
            description: item.description,
            price: item.price.toFixed(2),
            single: item.single,
            total: item.total.toFixed(2)
          };
        });
        const createData = {
          guid: req.guid,
          user_id: req.user.user_id,
          client_rfc: parsedDigest.client_rfc,
          company_rfc: parsedDigest.company_rfc,
          subtotal: parsedDigest.subtotal.toFixed(2),
          total: parsedDigest.total.toFixed(2),
          folio: parsedDigest.folio,
          uuid: parsedDigest.uuid,
          company_regime: parsedDigest.company_regime,
          items,
          cadena_original: parsedDigest.cadenaOriginal,
          sat_digital_stamp: parsedDigest.sat_digital_stamp,
          cfdi_digital_stamp: parsedDigest.cfdi_digital_stamp,
          company_address: parsedDigest.company_address,
          emission_date: parsedDigest.emission_date.toString(),
          tax_total: parsedDigest.tax_total.toFixed(2),
          tax_percentage: parsedDigest.tax_percentage.toFixed(2),
          cfdi: stringCfdi,
          serie: parsedDigest.serie,
          company_postal_code: parsedDigest.company_postal_code,
          written_amount: parsedDigest.written_amount
        };

        return gateway.sendUser('invoice', 'create', createData);
      }).then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Upload invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getInvoices(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('invoice', 'getInvoices', request)
      .then(result => {
        const inv = _.map(result.invoices, value => {
          value.subtotal = Number(value.subtotal);
          value.total = Number(value.total);

          return value;
        });

        result.invoices = inv;

        const response = {
          type: 'InvoiceList',
          data: result
        };

        log.message('Get invoices', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('InvoiceList', error, req.guid, res));
  },

  getInvoice(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value
    };

    return gateway.sendUser('invoice', 'getInvoice', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Get invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  approve(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoices: req.body.invoices,
      expiration: req.body.expiration
    };

    return gateway.sendUser('invoice', 'approve', request)
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Approve invoices', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  reject(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value,
      reason: req.body.reason
    };

    return gateway.sendUser('invoice', 'reject', request)
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Reject invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getPublishSummary(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoices: req.body.invoices
    };

    return gateway.sendUser('invoice', 'getPublishSummary', request)
      .then(result => {
        const publishSummary = [ 'total', 'operation_cost', 'reserve', 'fund_payment' ];

        _.each(result.invoices, function(invoice, i) {
          _.each(invoice, function(value, j) {
            result.invoices[i][j] = _.includes(publishSummary, j) ? Number(value) : value;
          });
        });

        _.each(result.summary, function(value, index) {
          result.summary[index] = Number(value);
        });

        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Get invoices publish summary', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  publish(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoices: req.body.invoices
    };

    return gateway.sendUser('invoice', 'publish', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Publish invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  fund(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      investor_company_id: req.user.company_id,
      invoices: req.body.invoices,
      fund: req.body.fund
    };

    return gateway.sendUser('invoice', 'fund', request)
      .then(result => {
        _.each(result.summary, function(value, index) {
          result.summary[index] = Number(value);
        });

        const invoicesSummary = [ 'total', 'gain', 'isr', 'fee', 'interest', 'perception' ];

        _.each(result.invoices, function(invoice, i) {
          _.each(invoice, function(value, j) {
            result.invoices[i][j] = _.includes(invoicesSummary, j) ? Number(value) : value;
          });
        });

        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Fund invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  completed(req, res) {
    const request = {
      guid: req.guid,
      company_id: req.user.company_id,
      invoice_id: req.swagger.params.id.value,
      cxp_payment: req.body.cxp_payment.toFixed(2),
      cxp_payment_date: req.body.cxp_payment_date,
      cxc_payment: req.body.cxc_payment.toFixed(2),
      investor_payment: req.body.investor_payment.toFixed(2),
      fondeo_payment_date: req.body.fondeo_payment_date
    };

    return Promise.resolve()
      .then(() => {
        if (!Date.parse(request.cxp_payment_date)) {
          return Promise.reject({
            path: 'cxp_payment_date',
            message: 'Invalid cxp_payment_date'
          });
        }
        if (!Date.parse(request.fondeo_payment_date)) {
          return Promise.reject({
            path: 'fondeo_payment_date',
            message: 'Invalid fondeo_payment_date'
          });
        }

        return gateway.sendUser('invoice', 'completed', request);
      })
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Completed invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  lost(req, res) {
    const request = {
      guid: req.guid,
      company_id: req.user.company_id,
      invoice_id: req.swagger.params.id.value,
      cxc_payment: req.body.cxc_payment.toFixed(2),
      investor_payment: req.body.investor_payment.toFixed(2),
      payment_date: req.body.payment_date
    };

    return Promise.resolve()
      .then(() => {
        if (!Date.parse(request.payment_date)) {
          return Promise.reject({
            path: 'payment_date',
            message: 'Invalid payment_date'
          });
        }

        return gateway.sendUser('invoice', 'lost', request);
      })
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Lost invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  latePayment(req, res) {
    const request = {
      guid: req.guid,
      company_id: req.user.company_id,
      invoice_id: req.swagger.params.id.value,
      cxc_payment: req.body.cxc_payment.toFixed(2),
      investor_payment: req.body.investor_payment.toFixed(2),
      cxp_payment: req.body.cxp_payment.toFixed(2),
      fondeo_payment_date: req.body.fondeo_payment_date,
      cxp_payment_date: req.body.cxp_payment_date
    };

    return Promise.resolve()
      .then(() => {
        if (!Date.parse(request.fondeo_payment_date)) {
          return Promise.reject({
            path: 'fondeo_payment_date',
            message: 'Invalid fondeo_payment_date'
          });
        }

        if (!Date.parse(request.cxp_payment_date)) {
          return Promise.reject({
            path: 'cxp_payment_date',
            message: 'Invalid cxp_payment_date'
          });
        }

        return gateway.sendUser('invoice', 'latePayment', request);
      })
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Late payment invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  rejectPublished(req, res) {
    const request = {
      guid: req.guid,
      company_id: req.user.company_id,
      user_role: req.user.user_role,
      invoice_id: req.swagger.params.id.value,
      reason: req.body.reason
    };

    if (req.user.user_role === 'CXC') {
      request.reason = 'Por solicitud del emisor de la factura';
    }

    return gateway.sendUser('invoice', 'rejectPublished', request)
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Reject published invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  rejectFunded(req, res) {
    const request = {
      guid: req.guid,
      invoice_id: req.swagger.params.id.value,
      reason: req.body.reason
    };

    return gateway.sendUser('invoice', 'rejectFunded', request)
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Reject fund requested invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  approveFund(req, res) {
    const request = {
      guid: req.guid,
      invoice_id: req.swagger.params.id.value,
      company_id: req.user.company_id
    };

    return gateway.sendUser('invoice', 'approveFund', request)
      .then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Approve fund request', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getEstimate(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value
    };

    return gateway.sendUser('invoice', 'getEstimate', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: {
            total: Number(result.total),
            operation_cost: Number(result.operation_cost),
            fund_total: Number(result.fund_total)
          }
        };

        log.message('Get estimate', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getDetail(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value
    };

    return gateway.sendUser('invoice', 'getDetail', request)
      .then(result => {
        result.subtotal = Number(result.subtotal);
        result.taxes = Number(result.taxes);
        result.total = Number(result.total);

        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Get invoice detail', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getFundEstimate(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value
    };

    return gateway.sendUser('invoice', 'getFundEstimate', request)
      .then(result => {
        const data = {};

        _.each(result, function(value, index) {
          data[index] = index === 'expiration' ? value : Number(value);
        });

        const response = {
          type: 'Invoice',
          data
        };

        log.message('Get fund estimate', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getXml(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value
    };

    return gateway.sendUser('invoice', 'getXml', request)
      .then(result => {
        log.message('Get invoice xml', result, 'response', req.guid);

        res.set({ 'Content-Disposition': `attachment; filename=\"invoice${request.invoice_id}.xml\"` });
        return res.send(result.xml);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getMarketplace(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      client_name: req.query.client_name,
      start_date: req.query.start_date,
      end_date: req.query.end_date
    };

    if (req.query.min_total) {
      request.min_total = req.query.min_total.toString();
    }
    if (req.query.max_total) {
      request.max_total = req.query.max_total.toString();
    }

    return Promise.resolve()
      .then(() => {
        if (request.start_date && !Date.parse(request.start_date)) {
          return Promise.reject({
            path: 'start_date',
            message: 'Invalid start_date'
          });
        }
        if (request.end_date && !Date.parse(request.end_date)) {
          return Promise.reject({
            path: 'end_date',
            message: 'Invalid end_date'
          });
        }

        return gateway.sendUser('invoice', 'getMarketplace', request);
      })
      .then(result => {
        const inv = _.map(result.invoices, value => {
          value.total = Number(value.total);
          value.gain = Number(value.gain);
          value.gain_percentage = Number(value.gain_percentage);

          return value;
        });

        result.invoices = inv;
        result.max_total = Number(result.max_total);

        const response = {
          type: 'Marketplace',
          data: result
        };

        log.message('Get marketplace', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Marketplace', error, req.guid, res));
  },

  getInvestorFundEstimate(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value,
      company_id: req.user.company_id
    };

    return gateway.sendUser('invoice', 'getInvestorFundEstimate', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: {
            total: Number(result.total),
            earnings: Number(result.earnings),
            commission: Number(result.commission),
            perception: Number(result.perception),
            isr: Number(result.isr),
            include_isr: result.include_isr
          }
        };

        log.message('Get investor fund estimate', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getInvestorProfitEstimate(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value,
      company_id: req.user.company_id
    };

    return gateway.sendUser('invoice', 'getInvestorProfitEstimate', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: {
            gain: Number(result.gain),
            gain_percentage: Number(result.gain_percentage),
            annual_gain: Number(result.annual_gain)
          }
        };

        log.message('Get investor profit estimate', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getInvestorInvoices(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('invoice', 'getInvestorInvoices', request)
      .then(result => {
        const inv = _.map(result.invoices, value => {
          value.total = Number(value.total);
          value.gain = Number(value.gain);
          value.gain_percentage = Number(value.gain_percentage);
          value.annual_gain = Number(value.annual_gain);

          return value;
        });

        result.invoices = inv;

        const response = {
          type: 'InvoiceList',
          data: result
        };

        log.message('Get investor invoices', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('InvoiceList', error, req.guid, res));
  },

  getInvestorFundDetail(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value,
      company_id: req.user.company_id
    };

    return gateway.sendUser('invoice', 'getInvestorFundDetail', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Get investor fund detail', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getInvoicePaymentSummary(req, res) {
    const request = {
      guid: req.guid,
      invoice_id: req.swagger.params.id.value,
      company_id: req.user.company_id
    };

    return gateway.sendUser('invoice', 'getInvoicePaymentSummary', request)
      .then(result => {
        const data = {
          payment_summary: {},
          financial_summary: {}
        };

        _.each(result.payment_summary, function(value, index) {
          data.payment_summary[index] = Number(value);
        });

        _.each(result.financial_summary, function(value, index) {
          data.financial_summary[index] = index === 'expiration' || index === 'fund_date' ? value : Number(value);
        });

        const response = {
          type: 'Invoice',
          data
        };

        log.message('Get invoice payment summary', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getAdminInvoices(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0,
      status: req.query.status,
      client_name: req.query.client_name,
      company_name: req.query.company_name,
      investor_name: req.query.investor_name,
      start_fund_date: req.query.start_fund_date,
      end_fund_date: req.query.end_fund_date,
      start_expiration_date: req.query.start_expiration_date,
      end_expiration_date: req.query.end_expiration_date
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return Promise.resolve()
      .then(() => {
        if (request.start_fund_date && !Date.parse(request.start_fund_date)) {
          return Promise.reject({
            path: 'start_fund_date',
            message: 'Invalid start_fund_date'
          });
        }
        if (request.end_fund_date && !Date.parse(request.end_fund_date)) {
          return Promise.reject({
            path: 'end_fund_date',
            message: 'Invalid end_fund_date'
          });
        }
        if (request.start_expiration_date && !Date.parse(request.start_expiration_date)) {
          return Promise.reject({
            path: 'start_expiration_date',
            message: 'Invalid start_expiration_date'
          });
        }
        if (request.end_expiration_date && !Date.parse(request.end_expiration_date)) {
          return Promise.reject({
            path: 'end_expiration_date',
            message: 'Invalid end_expiration_date'
          });
        }

        return gateway.sendUser('invoice', 'getAdminInvoices', request);
      })
      .then(result => {
        const inv = _.map(result.invoices, value => {
          value.total = Number(value.total);

          return value;
        });

        result.invoices = inv;

        const response = {
          type: 'InvoiceList',
          data: result
        };

        log.message('Get admin invoices', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('InvoiceList', error, req.guid, res));
  },

  getInvoiceDetailAsAdmin(req, res) {
    const request = {
      guid: req.guid,
      invoice_id: req.swagger.params.id.value
    };

    return gateway.sendUser('invoice', 'getInvoiceDetailAsAdmin', request)
      .then(result => {
        const data = {
          invoice_detail: {},
          operation_summary: {},
          cxc_payment: {},
          investor_payment: {}
        };

        _.each(result.invoice_detail, function(value, index) {
          data.invoice_detail[index] = index === 'total' ? Number(value) : value;
        });

        const operationSummary = [ 'fund_date', 'expiration' ];

        _.each(result.operation_summary, function(value, index) {
          data.operation_summary[index] = _.includes(operationSummary, index) ? value : Number(value);
        });

        _.each(result.cxc_payment, function(value, index) {
          data.cxc_payment[index] = Number(value);
        });

        const investorPayment = [ 'investor_name', 'fund_date', 'include_isr', 'fideicomiso_fee',
          'fideicomiso_fee_percentage' ];

        _.each(result.investor_payment, function(value, index) {
          data.investor_payment[index] = _.includes(investorPayment, index) ? value : Number(value);
        });

        const response = {
          type: 'Invoice',
          data
        };

        log.message('Get invoice detail as admin', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  bulkDelete(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      company_id: req.user.company_id,
      invoices: req.body.invoices
    };

    return gateway.sendUser('invoice', 'bulkDelete', request)
      .then(userData => {
        const response = {
          type: 'Invoice',
          data: userData
        };

        log.message('Bulk delete invoice', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getInvoicesAvailability(req, res) {
    const request = {
      guid: req.guid,
      invoices: req.body.invoices
    };

    return gateway.sendUser('invoice', 'getInvoicesAvailability', request)
      .then(result => {
        const response = {
          type: 'Invoice',
          data: result
        };

        log.message('Get invoices availability', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  bulkApprove(req, res) {
    const request = {
      guid: req.guid,
      pending_transactions: req.body.pending_transactions,
      bank_report_id: req.body.bank_report_id
    };

    return gateway.sendUser('invoice', 'bulkApprove', request)
      .then( (userData) => {
        userData.success = userData.success ? userData.success : [];
        userData.fail = userData.fail ? userData.fail : [];

        const response = {
          type: 'Invoice',
          data: userData
        };

        log.message('Bulk approve', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res) );
  }
};
