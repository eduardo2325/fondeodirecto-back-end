const _ = require('lodash');
const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  getOperations(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('operation', 'getOperations', request)
      .then(result => {
        result.operations.forEach( value => {
          value.total = Number(value.total);
          value.saving = Number(value.saving);
          value.prepaid = Number(value.prepaid);
        });

        const response = {
          type: 'OperationList',
          data: result
        };

        log.message('Get operations', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('OperationList', error, req.guid, res));
  },

  getPayedOperations(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('operation', 'getPayedOperations', request)
      .then(result => {
        result.operations.forEach( value => {
          value.total = Number(value.total);
          value.saving = Number(value.saving);
          value.prepaid = Number(value.prepaid);
        });

        const response = {
          type: 'OperationList',
          data: result
        };

        log.message('Get payed operations', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('OperationList', error, req.guid, res));
  },

  getOperation(req, res) {
    const request = {
      guid: req.guid,
      operation_id: req.swagger.params.id.value,
      user_id: req.user.user_id
    };

    return gateway.sendUser('operation', 'getOperation', request)
      .then(result => {
        result.annual_cost_percentage = Number(result.annual_cost_percentage);
        result.reserve_percentage = Number(result.reserve_percentage);
        result.fd_commission_percentage = Number(result.fd_commission_percentage);
        result.annual_cost = Number(result.annual_cost);
        result.reserve = Number(result.reserve);
        result.fd_commission = Number(result.fd_commission);
        result.factorable = Number(result.factorable);
        result.subtotal = Number(result.subtotal);
        result.fund_payment = Number(result.fund_payment);
        result.commission = Number(result.commission);
        result.operation_cost = Number(result.operation_cost);
        result.fund_total = Number(result.fund_total);
        result.total = Number(result.total);

        const response = {
          type: 'Operation',
          data: result
        };

        log.message('Get operation', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Operation', error, req.guid, res));
  },

  getAdminOperations(req, res) {
    const request = {
      guid: req.guid,
      user_id: req.user.user_id,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0,
      status: req.query.status,
      client_name: req.query.client_name,
      company_name: req.query.company_name,
      investor_name: req.query.investor_name,
      start_fund_date: req.query.start_fund_date,
      end_fund_date: req.query.end_fund_date,
      start_expiration_date: req.query.start_expiration_date,
      end_expiration_date: req.query.end_expiration_date
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return Promise.resolve()
      .then(() => {
        if (request.start_fund_date && !Date.parse(request.start_fund_date)) {
          return Promise.reject({
            path: 'start_fund_date',
            message: 'Invalid start_fund_date'
          });
        }
        if (request.end_fund_date && !Date.parse(request.end_fund_date)) {
          return Promise.reject({
            path: 'end_fund_date',
            message: 'Invalid end_fund_date'
          });
        }
        if (request.start_expiration_date && !Date.parse(request.start_expiration_date)) {
          return Promise.reject({
            path: 'start_expiration_date',
            message: 'Invalid start_expiration_date'
          });
        }
        if (request.end_expiration_date && !Date.parse(request.end_expiration_date)) {
          return Promise.reject({
            path: 'end_expiration_date',
            message: 'Invalid end_expiration_date'
          });
        }

        return gateway.sendUser('operation', 'getAdminOperations', request);
      })
      .then(result => {
        const inv = _.map(result.operations, value => {
          value.total = Number(value.total);
          value.invoice.total = Number(value.invoice.total);

          return value;
        });

        result.operations = inv;

        const response = {
          type: 'OperationList',
          data: result
        };

        log.message('Get admin operations', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('InvoiceList', error, req.guid, res));
  }
};
