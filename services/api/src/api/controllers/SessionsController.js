const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  login(req, res) {
    const credentials = {
      guid: req.guid,
      email: req.body.email,
      password: req.body.password
    };

    return Promise.resolve()
      .then(() => {
        return gateway.sendUser('session', 'verifyAndCreate', credentials);
      })
      .then(session => {
        const response = {
          type: 'Session',
          data: session
        };

        log.message('Login', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Session', error, req.guid, res));
  },

  logout(req, res) {
    const session = {
      guid: req.guid,
      token: req.user.token
    };

    return gateway.sendUser('session', 'deleteToken', session)
      .then(() => {
        const response = {
          type: 'Session',
          data: {
            success: true
          }
        };

        log.message('Logout', response, 'response', req.guid);

        res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Session', error, req.guid, res));
  },

  me(req, res) {
    const token = {
      guid: req.guid,
      token: req.user.token
    };

    return gateway.sendUser('session', 'me', token).then(session => {
      const response = {
        type: 'Session',
        data: session
      };

      log.message('User credentials', response, 'response', req.guid);

      return res.send(response);
    })
      .catch(error => errorHelper.handleResponse('Session', error, req.guid, res));
  }
};
