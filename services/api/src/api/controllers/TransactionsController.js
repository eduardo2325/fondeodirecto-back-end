const _ = require('lodash');
const s3 = require('/var/lib/core/js/s3');
const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  withdraw(req, res) {
    const data = {
      guid: req.guid,
      user_id: req.user.user_id,
      amount: req.body.amount.toFixed(2)
    };

    return gateway.sendUser('transaction', 'withdraw', data)
      .then(transaction => {
        const response = {
          type: 'Transaction',
          data: transaction
        };

        transaction.data = JSON.parse(transaction.data);
        transaction.amount = String(transaction.amount);

        log.message('Create withdraw', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Transaction', error, req.guid, res));
  },

  getPendingTransactions(req, res) {
    const data = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      status: 'PENDING'
    };
    const response = {
      type: 'TransactionList',
      data: {}
    };

    return gateway.sendUser('transaction', 'getTransactions', data)
      .then(result => {
        response.data.balance = Number(result.balance);

        const tasks = result.transactions.map(t => {
          return Promise.resolve()
            .then(() => {
              const key = JSON.parse(t.data).key;

              return key ? s3.getUrl(key, req.guid) : Promise.resolve();
            }).then(url => {
              t.data = JSON.parse(t.data);
              t.amount = String(t.amount);

              if (url) {
                t.data.url = url;
                delete t.data.key;
              }

              return t;
            });
        });

        return Promise.all(tasks);
      }).then(transactions => {
        response.data.transactions = transactions;

        log.message('Get company pending transactions', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('TransactionList', error, req.guid, res));
  },

  deposit(req, res) {
    const fileUpload = req.swagger.params.receipt.value;

    if (!_.includes([ 'image/jpeg', 'image/png', 'application/pdf' ], fileUpload.mimetype)) {
      const error = {
        path: 'receipt',
        message: 'Incorrect file type'
      };

      return errorHelper.handleResponse('Transaction', error, req.guid, res);
    }

    const user = req.user;
    const originalname = fileUpload.originalname;
    const timestamp = new Date().getTime();
    const filename = `${user.user_id}-${timestamp}.${originalname.split('.').pop()}`;
    const key = `investors/${user.company_id}/deposits/${filename}`;
    const validateData = {
      guid: req.guid,
      user_id: req.user.user_id,
      amount: String(req.swagger.params.amount.value),
      deposit_date: req.swagger.params.deposit_date.value,
      receipt: key
    };
    let response = {};

    return Promise.resolve()
      .then(() => fileUpload.buffer)
      .then(file => {
        return s3.upload(key, file, req.guid);
      }).then(() => {
        return gateway.sendUser('transaction', 'deposit', validateData);
      }).then(transaction => {
        transaction.data = JSON.parse(transaction.data);
        transaction.amount = String(transaction.amount);

        response = {
          type: 'Transaction',
          data: transaction
        };

        return s3.getUrl(transaction.data.key, req.guid);
      }).then(url => {
        response.data.data.url = url;
        delete response.data.data.key;

        log.message('Create deposit', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Transaction', error, req.guid, res));
  },

  approve(req, res) {
    const data = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      company_id: req.user.company_id
    };
    let result;

    return gateway.sendUser('transaction', 'approve', data)
      .then(transaction => {
        transaction.data = JSON.parse(transaction.data);
        transaction.amount = String(transaction.amount);
        result = transaction;
        const key = transaction.data.key;

        return key ? s3.getUrl(key, req.guid) : Promise.resolve();
      })
      .then(url => {
        if (url) {
          result.data.url = url;
          delete result.data.key;
        }

        const response = {
          type: 'Transaction',
          data: result
        };

        log.message('Approve transaction', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Transaction', error, req.guid, res));
  },

  reject(req, res) {
    const data = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      company_id: req.user.company_id,
      reason: req.body.reason
    };
    let result;

    return gateway.sendUser('transaction', 'reject', data)
      .then(transaction => {
        transaction.data = JSON.parse(transaction.data);
        transaction.amount = String(transaction.amount);
        result = transaction;
        const key = transaction.data.key;

        return key ? s3.getUrl(key, req.guid) : Promise.resolve();
      })
      .then(url => {
        if (url) {
          result.data.url = url;
          delete result.data.key;
        }

        const response = {
          type: 'Transaction',
          data: result
        };

        log.message('Reject transaction', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Transaction', error, req.guid, res));
  },

  createClientInvoicePayment(req, res) {
    const user = req.user;
    const data = {
      guid: req.guid,
      user_id: req.user.user_id,
      user_role: req.user.user_role,
      company_id: req.user.company_id,
      invoice_id: req.swagger.params.id.value,
      amount: req.swagger.params.amount.value.toString(),
      payment_date: req.swagger.params.payment_date.value
    };

    const fileUpload = req.swagger.params.receipt.value;

    return Promise.resolve()
      .then(() => {
        if (fileUpload && !_.includes([ 'image/jpeg', 'image/png', 'application/pdf' ], fileUpload.mimetype)) {
          return Promise.reject({
            path: 'receipt',
            message: 'Incorrect file type'
          });
        }

        if (!Date.parse(data.payment_date)) {
          return Promise.reject({
            path: 'payment_date',
            message: 'Invalid payment_date'
          });
        }

        if (fileUpload) {
          const originalname = fileUpload.originalname;
          const timestamp = new Date().getTime();
          const filename = `${user.user_id}-${timestamp}.${originalname.split('.').pop()}`;
          const key = `cxp/${user.company_id}/payments/${filename}`;

          data.receipt = key;

          return Promise.resolve(fileUpload.buffer).then(file => s3.upload(key, file, req.guid));
        }

        return Promise.resolve();
      }).then(() => {
        return gateway.sendUser('transaction', 'createClientInvoicePayment', data);
      }).then(invoice => {
        const response = {
          type: 'Invoice',
          data: invoice
        };

        log.message('Create client invoice payment', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Invoice', error, req.guid, res));
  },

  getClientInvoicePayment(req, res) {
    const data = {
      guid: req.guid,
      user_id: req.user.user_id,
      invoice_id: req.swagger.params.id.value
    };
    let payment;

    return gateway.sendUser('transaction', 'getClientInvoicePayment', data)
      .then(result => {
        payment = result;
        payment.data = JSON.parse(payment.data);
        payment.amount = String(payment.amount);

        return payment.data.key ? s3.getUrl(payment.data.key, req.guid) : null;
      }).then(url => {
        const response = {
          type: 'Transaction',
          data: payment
        };

        if (url) {
          payment.data.url = url;
          delete payment.data.key;
        }

        log.message('Get client invoice payment', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Transaction', error, req.guid, res));
  }
};
