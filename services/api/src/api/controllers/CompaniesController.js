const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');

const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  create(req, res) {
    const companyData = {
      guid: req.guid,
      rfc: req.swagger.params.rfc.value,
      name: req.swagger.params.name.value,
      business_name: req.swagger.params.business_name.value,
      holder: req.swagger.params.holder.value,
      clabe: req.swagger.params.clabe.value,
      role: 'COMPANY',
      description: req.swagger.params.description.value,
      operation_cost: {
        fd_commission: req.swagger.params.fd_commission.value.toFixed(2),
        reserve: req.swagger.params.reserve.value.toFixed(2),
        annual_cost: req.swagger.params.annual_cost.value.toFixed(2)
      },
      suspended_roles: [],
      prepayment: req.swagger.params.prepayment.value
    };

    if (req.swagger.params.active_cxc.value !== 'true') {
      companyData.suspended_roles.push('CXC');
    }

    if (req.swagger.params.active_cxp.value !== 'true') {
      companyData.suspended_roles.push('CXP');
    }

    return gateway.sendUser('company', 'create', companyData)
      .then(company => {
        const response = {
          type: 'Company',
          data: company
        };

        log.message('Create company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Company', error, req.guid, res));
  },

  exists(req, res) {
    const companyData = {
      guid: req.guid,
      rfc: req.swagger.params.rfc.value
    };

    return gateway.sendUser('company', 'exists', companyData)
      .then(exists => {
        const response = {
          type: 'CompanyExists',
          data: exists
        };

        log.message('Company Exists', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('CompanyExists', error, req.guid, res));
  },

  updateCompany(req, res) {
    const companyData = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      name: req.body.name,
      business_name: req.body.business_name,
      holder: req.body.holder,
      clabe: req.body.clabe,
      description: req.body.description
    };

    return gateway.sendUser('company', 'update', companyData)
      .then(company => {
        const response = {
          type: 'Company',
          data: company
        };

        log.message('Update company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Company', error, req.guid, res));
  },

  updateCompanyOperationCost(req, res) {
    const { operation_cost } = req.swagger.params.companyOperationCost.value;

    const companyData = {
      guid: req.guid,
      role: 'COMPANY',
      id: req.swagger.params.id.value,
      reserve: String(operation_cost.reserve),
      annual_cost: String(operation_cost.annual_cost),
      fd_commission: String(operation_cost.fd_commission)
    };

    return gateway.sendUser('company', 'updateOperationCost', companyData)
      .then(operationCost => {
        const response = {
          type: 'OperationCost',
          data: {
            reserve: parseFloat(operationCost.reserve),
            annual_cost: parseFloat(operationCost.annual_cost),
            fd_commission: parseFloat(operationCost.fd_commission)
          }
        };

        log.message('Update company operation cost', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('OperationCost', error, req.guid, res));
  },

  updateInvestorOperationCost(req, res) {
    const { operation_cost } = req.swagger.params.investorOperationCost.value;

    const companyData = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      fee: operation_cost.fee.toFixed(2),
      variable_fee_percentage: operation_cost.variable_fee_percentage.toFixed(2),
      role: 'INVESTOR'
    };

    return gateway.sendUser('company', 'updateOperationCost', companyData)
      .then(operationCost => {
        const response = {
          type: 'OperationCost',
          data: {
            fee: Number(operationCost.fee),
            variable_fee_percentage: Number(operationCost.variable_fee_percentage)
          }
        };

        log.message('Update investor operation cost', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('OperationCost', error, req.guid, res));
  },

  getCompany(req, res) {
    const companyData = {
      guid: req.guid,
      id: req.swagger.params.id.value
    };

    return gateway.sendUser('company', 'getFullInformation', companyData)
      .then(company => {
        const response = {
          type: 'Company',
          data: company
        };

        log.message('Get company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Company', error, req.guid, res));
  },

  getCompanies(req, res) {
    const request = {
      guid: req.guid,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0,
      role: 'COMPANY'
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('company', 'getCompanies', request)
      .then(companies => {
        const response = {
          type: 'CompanyList',
          data: companies
        };

        log.message('Get companies', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('CompanyList', error, req.guid, res));
  },

  getInvestorCompanies(req, res) {
    const request = {
      guid: req.guid,
      limit: parseInt(req.query.page_size, 10) || 0,
      offset: parseInt(req.query.page, 10) || 0,
      role: 'INVESTOR'
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('company', 'getCompanies', request)
      .then(companies => {
        const response = {
          type: 'InvestorList',
          data: companies
        };

        log.message('Get investor companies', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('InvestorList', error, req.guid, res));
  },

  getBankInfo(req, res) {
    const clabe = {
      guid: req.guid,
      clabe: req.swagger.params.clabe.value
    };

    return gateway.sendUser('company', 'analyzeClabe', clabe)
      .then(bankInfo => {
        const response = {
          type: 'BankInfo',
          data: bankInfo
        };

        log.message('Get Bank Info', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('BankInfo', error, req.guid, res));
  },

  clabeExists(req, res) {
    const clabe = {
      guid: req.guid,
      clabe: req.swagger.params.clabe.value,
      exists: true
    };

    return gateway.sendUser('company', 'analyzeClabe', clabe)
      .then(bankInfo => {
        const response = {
          type: 'BankInfo',
          data: bankInfo
        };

        log.message('Get Bank Info By Clabe', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('BankInfo', error, req.guid, res));
  },

  getCompanyUsers(req, res) {
    const request = {
      guid: req.guid,
      id: req.swagger.params.id.value
    };

    if (req.query.order_by) {
      request.order_by = req.query.order_by;
    }
    if (req.query.order_desc) {
      request.order_desc = req.query.order_desc === 'true';
    }

    return gateway.sendUser('company', 'getUsers', request)
      .then(companyUsers => {
        const response = {
          type: 'CompanyUsers',
          data: companyUsers
        };

        log.message('Get company users', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('CompanyUsers', error, req.guid, res));
  },

  getCompanyOperationCost(req, res) {
    const request = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      role: 'COMPANY'
    };

    return gateway.sendUser('company', 'getOperationCost', request)
      .then(operationCost => {
        const response = {
          type: 'OperationCost',
          data: {
            annual_cost: Number(operationCost.annual_cost),
            reserve: Number(operationCost.reserve),
            fd_commission: Number(operationCost.fd_commission)
          }
        };

        log.message('Get Company Operation Costs', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('OperationCost', error, req.guid, res));
  },

  getInvestorOperationCost(req, res) {
    const request = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      role: 'INVESTOR'
    };

    return gateway.sendUser('company', 'getOperationCost', request)
      .then(operationCost => {
        const response = {
          type: 'OperationCost',
          data: {
            fee: Number(operationCost.fee),
            variable_fee_percentage: Number(operationCost.variable_fee_percentage)
          }
        };

        log.message('Get Investor Operation Costs', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('OperationCost', error, req.guid, res));
  },

  addUser(req, res) {
    const userData = {
      guid: req.guid,
      company_id: req.swagger.params.id.value,
      user: {
        name: req.body.name,
        email: req.body.email,
        type: req.body.type
      }
    };

    return gateway.sendUser('company', 'addUser', userData)
      .then(user => {
        const response = {
          type: 'User',
          data: user
        };

        log.message('Add user to company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('User', error, req.guid, res));
  },

  createInvestor(req, res) {
    const companyData = {
      guid: req.guid,
      rfc: req.swagger.params.rfc.value,
      name: req.swagger.params.name.value,
      business_name: req.swagger.params.business_name.value,
      holder: req.swagger.params.holder.value,
      clabe: req.swagger.params.clabe.value,
      role: 'INVESTOR',
      operation_cost: {
        fee: req.swagger.params.fee.value.toFixed(2),
        variable_fee_percentage: req.swagger.params.variable_fee_percentage.value.toFixed(2),
        fideicomiso_fee: req.swagger.params.fideicomiso_fee.value ?
          req.swagger.params.fideicomiso_fee.value.toFixed(2) : undefined,
        fideicomiso_variable_fee: req.swagger.params.fideicomiso_variable_fee.value ?
          req.swagger.params.fideicomiso_variable_fee.value.toFixed(2) : undefined
      },
      taxpayer_type: req.swagger.params.taxpayer_type.value,
      isFideicomiso: req.swagger.params.isFideicomiso.value
    };

    return gateway.sendUser('company', 'create', companyData)
      .then(company => {
        const response = {
          type: 'Investor',
          data: company
        };

        log.message('Create investor company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Investor', error, req.guid, res));
  },

  registerInvestor(req, res) {
    const investorData = {
      guid: req.guid,
      token: req.body.token,
      taxpayer_type: req.body.taxpayer_type,
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      phone: req.body.phone,
      age: req.body.age,
      address: req.body.address,
      rfc: req.body.rfc,
      company_business_name: req.body.company_business_name
    };

    return gateway.sendUser('company', 'registerInvestor', investorData)
      .then(company => {
        const response = {
          type: 'Investor',
          data: company
        };

        log.message('Create investor company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Investor', error, req.guid, res));
  },

  addInvestorUser(req, res) {
    const userData = {
      guid: req.guid,
      company_id: req.swagger.params.id.value,
      user: {
        name: req.body.name,
        email: req.body.email,
        type: 'INVESTOR'
      }
    };

    return gateway.sendUser('company', 'addUser', userData)
      .then(user => {
        const response = {
          type: 'Investor',
          data: user
        };

        log.message('Add user to company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Investor', error, req.guid, res));
  },


  getFactorajeAndCashSummaries(req, res) {
    const userData = {
      guid: req.guid,
      company_id: req.user.company_id,
      month: req.body.month,
      year: req.body.year
    };

    return gateway.sendUser('company', 'getFactorajeAndCashSummaries', userData)
      .then(user => {
        const response = {
          type: 'Investor',
          data: user
        };

        user.factoraje.initial_ammount = parseFloat(user.factoraje.initial_ammount);
        user.factoraje.final_ammount = parseFloat(user.factoraje.final_ammount);
        user.factoraje.payments = parseFloat(user.factoraje.payments);
        user.factoraje.acquisitions = parseFloat(user.factoraje.acquisitions);

        user.cash.initial_ammount = parseFloat(user.cash.initial_ammount);
        user.cash.final_ammount = parseFloat(user.cash.final_ammount);
        user.cash.increments = parseFloat(user.cash.increments);
        user.cash.decrements = parseFloat(user.cash.decrements);

        log.message('Get investor summaries', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Investor', error, req.guid, res));
  },

  createInvestorInvitation(req, res) {
    const request = {
      guid: req.guid,
      issuer: req.user.user_id,
      name: req.body.name,
      company_name: req.body.company_name,
      email: req.body.email,
      fee: req.body.fee.toFixed(2),
      variable_fee_percentage: req.body.variable_fee_percentage.toFixed(2)
    };

    return gateway.sendUser('company', 'createInvestorInvitation', request)
      .then( invitation => {
        const response = {
          type: 'Company',
          data: invitation
        };

        log.message('Create investor invitation', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Company', error, req.guid, res));
  },

  getBalance(req, res) {
    const userData = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      user_id: req.user.user_id
    };

    return gateway.sendUser('company', 'getBalance', userData)
      .then(balance => {
        const response = {
          type: 'Balance',
          data: balance
        };

        log.message('Get company balance', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Balance', error, req.guid, res));
  },

  updateCompanyRoleSuspension(req, res) {
    const userData = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      role: req.body.role.toUpperCase(),
      suspended: req.body.suspended
    };

    return gateway.sendUser('company', 'updateCompanyRoleSuspension', userData)
      .then(() => {
        const response = {
          type: 'Role',
          data: {
            success: true
          }
        };

        log.message('Update company role suspension', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Role', error, req.guid, res));
  },

  updateInvestorRoleSuspension(req, res) {
    const userData = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      role: req.body.role.toUpperCase(),
      suspended: req.body.suspended
    };

    return gateway.sendUser('company', 'updateInvestorRoleSuspension', userData)
      .then(() => {
        const response = {
          type: 'Role',
          data: {
            success: true
          }
        };

        log.message('Update investor role suspension', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Role', error, req.guid, res));
  },

  propose(req, res) {
    const companyData = {
      guid: req.guid,
      company_id: req.user.company_id,
      business_name: req.body.business_name,
      type: req.body.type,
      contact_name: req.body.contact_name,
      position: req.body.position,
      email: req.body.email,
      phone: req.body.phone
    };

    return gateway.sendUser('company', 'propose', companyData)
      .then(success => {
        const response = {
          type: 'Company',
          data: success
        };

        log.message('Propose a company', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Company', error, req.guid, res));
  },

  agreed(req, res) {
    const params = {
      guid: req.guid,
      user: req.user.user_id,
      role: req.user.user_role,
      id: req.swagger.params.id.value
    };

    return gateway.sendUser('company', 'agreed', params)
      .then(result => {
        const response = {
          type: 'Company',
          data: result
        };

        log.message('Company agreement', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => {
        return errorHelper.handleResponse('Company', error, req.guid, res);
      });
  },

  getInvestorTransactions(req, res) {
    const params = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      month: req.body.month,
      year: req.body.year
    };

    return gateway.sendUser('company', 'getInvestorTransactions', params)
      .then(result => {
        const response = {
          type: 'Investor',
          data: result
        };

        log.message('Get an investor\'s transactions', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Investor', error, req.guid, res));
  },

  updateCompanyPrepayment(req, res) {
    const userData = {
      guid: req.guid,
      id: req.swagger.params.id.value,
      prepayment: req.body.prepayment
    };

    return gateway.sendUser('company', 'updateCompanyPrepayment', userData)
      .then(result => {
        const response = {
          type: 'Company',
          data: result
        };

        log.message('Update company prepayment', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Company', error, req.guid, res));
  }
};
