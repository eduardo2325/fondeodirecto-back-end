const gateway = require('../helpers/gateway');
const errorHelper = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);

module.exports = {
  checkToken(req, res) {
    const request = {
      guid: req.guid,
      token: req.body.token,
      statistics_type: req.body.statistics_type,
      event_type: req.body.event_type
    };

    return gateway.sendUser('statistics', 'checkToken', request)
      .then( data => {
        const response = {
          type: 'Statistics',
          data: data
        };

        log.message('Store user event', response, 'response', req.guid);

        return res.send(response);
      })
      .catch(error => errorHelper.handleResponse('Statistics', error, req.guid, res));
  }
};
