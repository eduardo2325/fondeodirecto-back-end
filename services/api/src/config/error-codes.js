const codes = {
  Session: {
    'email or password doesn\'t match': 100,
    Unaccepted: 502,
    Unauthorized: 101
  },
  Role: {
    Unauthorized: 102,
    'Suspended company role': 103,
    'Suspended investor role': 104
  },
  Company: {
    'invalid clabe': 200,
    'email must be unique': 202
  },
  Investor: {
    'invalid clabe': 200,
    'rfc must be unique': 502,
    'email must be unique': 202,
    'taxpayer_type cannot be null': 502
  },
  BankInfo: {
    'invalid clabe': 200,
    'clabe must be unique': 201
  },
  Invoice: {
    'Invalid format': 300,
    'Issuer not found': 301,
    'Receptor not found': 302,
    'uuid must be unique': 303,
    'Company and Client RFC must be different': 304,
    'Can not afford': 305,
    'Receptor is suspended': 306,
    'Invalid payment_date': 307,
    'Invalid cxp_payment_date': 308,
    'Invalid fondeo_payment_date': 309,
    'Invoice has expired': 310,
    'Invoice already funded': 311,
    'Incorrect file type': 502,
    'Array is too short (0), minimum 1': 502
  },
  Transaction: {
    'Value is required but was not provided': 502,
    'Incorrect file type': 502,
    'Invalid payment_date': 502
  },
  Server: {
    'Internal server error': 500,
    Unassigned: 501,
    InvalidRequest: 502,
    NotFound: 503
  },
  Marketplace: {
    'Invalid start_date': 400,
    'Invalid end_date': 401
  },
  User: {
    'Unable to delete the last user': 601,
    'email must be unique': 202
  },
  InvoiceList: {
    'Invalid start_fund_date': 600,
    'Invalid end_fund_date': 601,
    'Invalid start_expiration_date': 602,
    'Invalid end_expiration_date': 603
  }
};

module.exports = codes;
