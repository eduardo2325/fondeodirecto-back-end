const env = process.env.NODE_ENV || 'development';
let database = process.env.DB_NAME;
/* eslint-disable-next-line */
const logging = env === 'test' ? false : console.log;
let kafkaConsumerGroup = 'api-consumer-service';
let kafkaOffsetReset = 'earliest';

if (env === 'test' || env === 'development') {
  database = env === 'test' ? database + '_test' : database + '_dev';
  // Avoid group restabilization to improve consumer initialization time
  kafkaConsumerGroup = kafkaConsumerGroup + Math.random();
  // Avoid reprocessing messages when running tests
  kafkaOffsetReset = env === 'test' ? 'latest' : kafkaOffsetReset;
}

const config = {
  services: {
    cfdi: process.env.SERVICE_CFDI_ADDRESS,
    user: process.env.SERVICE_USER_ADDRESS
  },
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: database,
  host: process.env.DB_HOST,
  dialect: 'postgres',
  logging: logging,
  seederStorage: 'sequelize',
  kafkaConsumer: {
    'group.id': kafkaConsumerGroup,
    'metadata.broker.list': `${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT}`,
    'enable.auto.commit': false,
    'event_cb': true,
    'offsetReset': kafkaOffsetReset,
    'maxMessages': 10,
    topics: [ 'user' ]
  }
};

module.exports = config;
