// Add swagger operationIds here for endpoints restricted upon suspension
const suspendedOperationsByRole = {
  CXC: [
    'post /invoices/publish',
    'post /invoices'
  ],
  CXP: [
    'post /invoices/approve',
    'put /invoices/{id}/reject'
  ],
  INVESTOR: [
    'post /invoices/fund/checkout',
    'post /investor/deposit'
  ],
  ADMIN: [

  ]
};

module.exports = {
  suspendedOperationsByRole
};
