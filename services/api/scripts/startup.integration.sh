#!/bin/bash

set -e

wait-for-it -t 0 user:80

if [ ! -z "${INTERACTIVE:-}" ]; then
  echo ""
  echo "Welcome to the interactive mode, please run \`make dev\`"
  echo "or whatever you want to debug this service. Enjoy your time!"
  echo ""

  tail -f /dev/null
else
  make dev
fi
