#!/bin/bash

set -e

npm install

wait-for-it -t 0 user:80
wait-for-it -t 0 cfdi:80

tail -f /dev/null
