#!/bin/bash

set -e

wait-for-it -t 0 user:80

[ -d unit_coverage ] || mkdir -p unit_coverage

npm run test:coverage:unit
npm run test:report -- -r text-lcov > unit_coverage/lcov.info

[ -d functional_coverage ] || mkdir -p functional_coverage

npm run test:coverage:functional
npm run test:report -- -r text-lcov > functional_coverage/lcov.info
