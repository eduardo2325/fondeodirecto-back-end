#!/bin/bash

set -x -u -e

aws-secrets-manager.sh backend/${AWS_SM_ENV}/${AWS_SM_SERVICE} -- node app.js
