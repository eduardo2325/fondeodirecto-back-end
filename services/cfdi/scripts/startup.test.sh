#!/bin/bash

set -e

# Uncomment this to run dev container without nodemon
make unit-test
make functional-test
