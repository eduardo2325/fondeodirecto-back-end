#!/bin/bash

set -x -u

./build/install/cfdi/bin/cfdi

# Uncomment this to run dev container without nodemon
# tail -f /dev/null
