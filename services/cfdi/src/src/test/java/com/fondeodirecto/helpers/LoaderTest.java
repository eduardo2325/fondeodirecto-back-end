package com.fondeodirecto.helpers;

import com.fondeodirecto.models.Invoice;
import mx.bigdata.sat.cfdi.CFDv33;
import org.junit.Test;
import org.xml.sax.SAXParseException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by oscargerardo on 6/2/17.
 */
public class LoaderTest {

    @Test
    public void testValidInvoice() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("validInvoice.xml");
        String validInvoice = new Scanner(new File(resource.getPath())).useDelimiter("\\Z").next();
        URL result = ClassLoader.getSystemClassLoader().getResource("response.json");
        String expected = new Scanner(new File(result.getPath())).useDelimiter("\\Z").next();
        String response = loader.loadCFD(validInvoice);

        assertEquals("failure - strings are not equal", expected, response);
    }

    @Test
    public void testValidInvoiceV33() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("validInvoiceV33.xml");
        String validInvoice = new Scanner(new File(resource.getPath())).useDelimiter("\\Z").next();
        URL result = ClassLoader.getSystemClassLoader().getResource("responseV33.json");
        String expected = new Scanner(new File(result.getPath())).useDelimiter("\\Z").next();
        String response = loader.loadCFD(validInvoice);

        assertEquals("failure - strings are not equal", expected, response);
    }

    @Test
    public void testInvalidNombreV33() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("invalidNombreV33.xml");
        String validInvoice = new Scanner(new File(resource.getPath())).useDelimiter("\\Z").next();
        URL result = ClassLoader.getSystemClassLoader().getResource("invalidNombreV33.json");
        String expected = new Scanner(new File(result.getPath())).useDelimiter("\\Z").next();
        String response = loader.loadCFD(validInvoice);

        assertEquals("failure - strings are not equal", expected, response);
    }


    public void testInvalidSignatureV33() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("invalidSignatureV33.xml");
        String validInvoice = new Scanner(new File(resource.getPath())).useDelimiter("\\Z").next();
        String response = loader.loadCFD(validInvoice);
        assertNotNull(response);
    }

    @Test // TODO Check test when we got an invoice V33
    public void testValidStructureV33() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("invalidSignatureV33.xml");
        Invoice response = loader.loadCFD33(new CFDv33(new BufferedInputStream(new FileInputStream(resource.getPath()))));
    }

    @Test(expected = SAXParseException.class)
    public void testInvalidInvoice() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("invalidInvoice.xml");
        String invalidInvoice = new Scanner(new File(resource.getPath())).useDelimiter("\\Z").next();

        loader.loadCFD(invalidInvoice);
    }

    @Test(expected = Exception.class)
    public void testNotXMLFormat() throws Exception {
        Loader loader = new Loader();

        loader.loadCFD("Not a XML");
    }

    @Test
    public void testValidInvoiceWithAddenda() throws Exception {
        Loader loader = new Loader();
        URL resource = ClassLoader.getSystemClassLoader().getResource("validInvoiceWithAddenda.xml");
        String validInvoice = new Scanner(new File(resource.getPath())).useDelimiter("\\Z").next();
        URL result = ClassLoader.getSystemClassLoader().getResource("responseInvoiceWithAddenda.json");
        String expected = new Scanner(new File(result.getPath())).useDelimiter("\\Z").next();
        String response = loader.loadCFD(validInvoice);

        assertEquals("failure - strings are not equal", expected, response);
    }
}
