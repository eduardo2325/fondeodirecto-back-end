package com.fondeodirecto.utils;

import com.fondeodirecto.cfdi.grpc.XmlInvoice;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

/**
 * Created by zhntr on 8/16/17.
 */
public class FondeoExclusionStrategyTest {
    static class TestObject {
        String name;
        String password;
        String password_confirmation;
        String actual_password;
        String new_password;
        String confirmation_password;
        String xml;
        String cfdi;
    }

    static class NestedTestObject {
        TestObject data;
        String type;
    }

    @Test
    public void testExcludedFields() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new FondeoExclusionStrategy()).create();

        TestObject object = new TestObject();
        object.name = "name";
        object.password = "password";
        object.password_confirmation = "password_confirmation";
        object.actual_password = "actual_password";
        object.new_password = "new_password";
        object.confirmation_password = "confirmation_password";
        object.xml = "xml";
        object.cfdi = "cfdi";

        String json = gson.toJson(object);
        assertThat(json, containsString("name"));
        assertThat(json, not(containsString("password")));
        assertThat(json, not(containsString("password_confirmation")));
        assertThat(json, not(containsString("actual_password")));
        assertThat(json, not(containsString("new_password")));
        assertThat(json, not(containsString("confirmation_password")));
        assertThat(json, not(containsString("xml")));
        assertThat(json, not(containsString("cfdi")));
    }

    @Test
    public void testFieldsAreExcludedByName() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new FondeoExclusionStrategy()).create();

        TestObject object = new TestObject();
        object.name = "password";

        String json = gson.toJson(object);
        assertThat(json, containsString("name"));
        assertThat(json, containsString("password"));

    }

    @Test
    public void testExcludedFieldsWithProtoClass() {
        Gson gson = new GsonBuilder()
                .setFieldNamingStrategy(new FondeoFieldNamingStrategy())
                .setExclusionStrategies(new FondeoExclusionStrategy())
                .create();

        XmlInvoice xmlInvoice = XmlInvoice.newBuilder().setGuid("guid-id").setXml("forbidden-value").build();

        String json = gson.toJson(xmlInvoice);
        assertThat(json, containsString("guid"));
        assertThat(json, containsString("guid-id"));
        assertThat(json, not(containsString("xml")));
        assertThat(json, not(containsString("forbidden-value")));

    }

    @Test
    public void testExcludedFieldsNestedObject() {
        Gson gson = new GsonBuilder().setExclusionStrategies(new FondeoExclusionStrategy()).create();

        TestObject testObject = new TestObject();
        testObject.name = "object-name";
        testObject.password = "12345678";

        NestedTestObject nestedTestObject = new NestedTestObject();
        nestedTestObject.data  = testObject;
        nestedTestObject.type = "nested-type";

        String json = gson.toJson(nestedTestObject);
        assertThat(json, containsString("name"));
        assertThat(json, containsString("object-name"));
        assertThat(json, containsString("nested-type"));
        assertThat(json, not(containsString("password")));
        assertThat(json, not(containsString("12345678")));
    }
}