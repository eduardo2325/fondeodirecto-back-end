package com.fondeodirecto.cfdi;

import com.fondeodirecto.cfdi.grpc.InvoiceGrpc;
import com.fondeodirecto.cfdi.grpc.InvoiceInfo;
import com.fondeodirecto.cfdi.grpc.XmlInvoice;
import com.fondeodirecto.helpers.Loader;
import com.google.gson.Gson;
import io.grpc.StatusRuntimeException;
import io.grpc.testing.GrpcServerRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by zhntr on 7/6/17.
 */
public class InvoiceImplTest {
    @Rule
    public final GrpcServerRule grpcServerRule = new GrpcServerRule().directExecutor();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testInvoiceService() throws Exception {
        Loader loader = mock(Loader.class);

        when(loader.loadCFD(anyString())).thenReturn("json");

        grpcServerRule.getServiceRegistry().addService(new InvoiceImpl(loader, new Gson()));
        InvoiceGrpc.InvoiceBlockingStub blockingStub = InvoiceGrpc.newBlockingStub(grpcServerRule.getChannel());

        InvoiceInfo invoiceInfo = blockingStub.validateAndExtract(
                XmlInvoice.newBuilder().setGuid("guid").setXml("xml").build());

        verify(loader).loadCFD("xml");
        assertThat(invoiceInfo.getDigest(), is("json"));
    }

    @Test
    public void testInvoiceServiceError() throws Exception {
        Loader loader = mock(Loader.class);
        Gson gson = new Gson();

        when(loader.loadCFD(anyString())).thenThrow(new Exception("Loader error"));

        grpcServerRule.getServiceRegistry().addService(new InvoiceImpl(loader, gson));
        InvoiceGrpc.InvoiceBlockingStub blockingStub = InvoiceGrpc.newBlockingStub(grpcServerRule.getChannel());

        thrown.expect(StatusRuntimeException.class);
        thrown.expectMessage(containsString("\"path\":\"XML\""));
        thrown.expectMessage(containsString("\"message\":\"Invalid format\""));

        blockingStub.validateAndExtract(
                XmlInvoice.newBuilder().setGuid("guid").setXml("bad-invoice").build());

    }
}