package com.fondeodirecto.cfdi;

import com.fondeodirecto.cfdi.grpc.InvoiceGrpc;
import com.fondeodirecto.cfdi.grpc.InvoiceInfo;
import com.fondeodirecto.cfdi.grpc.XmlInvoice;
import com.fondeodirecto.helpers.Loader;
import com.google.gson.Gson;
import io.grpc.Server;
import io.grpc.StatusRuntimeException;
import io.grpc.testing.GrpcServerRule;
import mx.bigdata.sat.common.CFD;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created by zhntr on 7/3/17.
 */
public class CfdiValidatorTest {

    private Server server;
    private CfdiValidator validator;

    @Before
    public void setup() {
        server = mock(Server.class);
        validator = new CfdiValidator(server);
    }

    @Test
    public void testStartServer() throws IOException {
        validator.start();
        verify(server).start();
    }

    @Test
    public void testStopServer() {
        validator.stop();
        verify(server).shutdown();
    }

    @Test
    public void testBlockingServer() throws InterruptedException {
        validator.blockUntilShutdown();
        verify(server).awaitTermination();
    }
}