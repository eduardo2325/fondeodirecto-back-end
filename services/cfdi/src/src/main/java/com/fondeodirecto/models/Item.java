package com.fondeodirecto.models;

import java.math.BigDecimal;

public class Item {
    private BigDecimal count;
    private String description;
    private BigDecimal price;
    private String single;
    private BigDecimal total;

    public Item(BigDecimal count, String description, BigDecimal price, String single, BigDecimal total) {
        this.count = count;
        this.description = description;
        this.price = price;
        this.single = single;
        this.total = total;
    }
}