package com.fondeodirecto.helpers;

import com.fondeodirecto.models.Invoice;
import com.fondeodirecto.models.Item;
import com.google.gson.Gson;
import mx.bigdata.sat.cfdi.CFDI;
import mx.bigdata.sat.cfdi.CFDv32;
import mx.bigdata.sat.cfdi.CFDv33;
import mx.bigdata.sat.cfdi.v32.schema.TUbicacionFiscal;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Loader {
    static private Pattern ADDENDA_PATTERN = Pattern.compile("<cfdi:Addenda>(.+?)</cfdi:Addenda>", Pattern.DOTALL);

    public String loadCFD(String xml) throws Exception {

        xml = ADDENDA_PATTERN.matcher(xml).replaceAll("");

        InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        Gson gson = new Gson();
        CFDI cfd;
        boolean version32 = false;
        if(xml.contains("version=\"3.2\"")) {
            version32 = true;
            cfd = new CFDv32(stream);
        } else if (xml.contains("Version=\"3.3\"")) {
            cfd = new CFDv33(stream);
        } else {
            throw new Exception("Invalid XML File");
        }

        cfd.validar();
        if (version32) {
            cfd.verificar();
        }

        Invoice invoice = version32 ? loadCFD32(cfd) : loadCFD33(cfd);

        return gson.toJson(invoice);
    }

    private Invoice loadCFD32(CFDI cfd) throws Exception {
        Invoice invoice = new Invoice();
        List<Item> items = new ArrayList<>();

        mx.bigdata.sat.cfdi.v32.schema.Comprobante comprobante = ((mx.bigdata.sat.cfdi.v32.schema.Comprobante)cfd.getComprobante().getComprobante());
        mx.bigdata.sat.cfdi.v32.schema.TimbreFiscalDigital timbre = ((mx.bigdata.sat.cfdi.v32.schema.TimbreFiscalDigital)comprobante.getComplemento().getAny().get(0));

        invoice.setUuid(timbre.getUUID());
        invoice.setClient_rfc(comprobante.getReceptor().getRfc());
        invoice.setCompany_rfc(comprobante.getEmisor().getRfc());
        invoice.setSubtotal(comprobante.getSubTotal());
        invoice.setTotal(comprobante.getTotal());

        BigDecimal tax = comprobante.getImpuestos().getTotalImpuestosTrasladados();
        tax.add(comprobante.getImpuestos().getTotalImpuestosRetenidos() != null ? comprobante.getImpuestos().getTotalImpuestosRetenidos() : new BigDecimal(0));

        invoice.setTax_total(tax);
        invoice.setTax_percentage(comprobante.getImpuestos().getTraslados().getTraslado().get(0).getTasa());
        invoice.setSerie(comprobante.getSerie());
        invoice.setFolio(comprobante.getFolio());
        invoice.setCompany_regime(comprobante.getEmisor().getRegimenFiscal().get(0).getRegimen());

        comprobante.getConceptos().getConcepto().forEach(concepto -> {
            items.add(new Item(concepto.getCantidad(), concepto.getDescripcion(), concepto.getValorUnitario(), concepto.getUnidad(), concepto.getImporte()));
        });

        invoice.setItems(items);
        invoice.setCadenaOriginal(cfd.getCadenaOriginal());

        invoice.setSat_digital_stamp(timbre.getSelloSAT());
        invoice.setCfdi_digital_stamp(timbre.getSelloCFD());
        invoice.setCompany_address(getAddress(comprobante.getEmisor().getDomicilioFiscal()));
        invoice.setCompany_postal_code(comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal());

        DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        invoice.setEmission_date(date.format(timbre.getFechaTimbrado().toGregorianCalendar().getTime()));

        return invoice;
    }

    Invoice loadCFD33(CFDI cfd) throws Exception {
        Invoice invoice = new Invoice();
        List<Item> items = new ArrayList<>();

        mx.bigdata.sat.cfdi.v33.schema.Comprobante comprobante = ((mx.bigdata.sat.cfdi.v33.schema.Comprobante)cfd.getComprobante().getComprobante());

        mx.bigdata.sat.cfdi.v33.schema.TimbreFiscalDigital timbre = null;
        for (mx.bigdata.sat.cfdi.v33.schema.Comprobante.Complemento o : comprobante.getComplemento()) {
            for (Object c : o.getAny()) {
                if (c instanceof mx.bigdata.sat.cfdi.v33.schema.TimbreFiscalDigital) {
                    timbre = (mx.bigdata.sat.cfdi.v33.schema.TimbreFiscalDigital) c;
                }
            }
        }

        if (timbre == null) {
            throw new NullPointerException("Factura no tiene comprobante fiscal en cfdi:Complemento");
        }

        invoice.setUuid(timbre.getUUID());
        invoice.setClient_rfc(comprobante.getReceptor().getRfc());
        invoice.setCompany_rfc(comprobante.getEmisor().getRfc());
        invoice.setSubtotal(comprobante.getSubTotal());
        invoice.setTotal(comprobante.getTotal());

        BigDecimal tax = comprobante.getImpuestos().getTotalImpuestosTrasladados();
        tax.add(comprobante.getImpuestos().getTotalImpuestosRetenidos() != null ? comprobante.getImpuestos().getTotalImpuestosRetenidos() : new BigDecimal(0));

        invoice.setTax_total(tax);
        invoice.setTax_percentage(comprobante.getImpuestos().getTraslados().getTraslado().get(0).getTasaOCuota());
        invoice.setSerie(comprobante.getSerie());
        invoice.setFolio(comprobante.getFolio());
        invoice.setCompany_regime(comprobante.getEmisor().getRegimenFiscal());

        comprobante.getConceptos().getConcepto().forEach(concepto -> {
            items.add(new Item(concepto.getCantidad(), concepto.getDescripcion(), concepto.getValorUnitario(), concepto.getUnidad(), concepto.getImporte()));
        });

        invoice.setItems(items);
        invoice.setCadenaOriginal(cfd.getCadenaOriginal());

        invoice.setSat_digital_stamp(timbre.getSelloSAT());
        invoice.setCfdi_digital_stamp(timbre.getSelloCFD());
        invoice.setEmission_date(timbre.getFechaTimbrado().toString());

        return invoice;
    }

    private String getAddress(TUbicacionFiscal address) {
        return String.format("%1$S %2$S, %3$S, %4$S, %5$S, %6$S", address.getCalle(), address.getNoExterior(), address.getColonia(), address.getLocalidad(), address.getMunicipio(), address.getEstado());
    }
}