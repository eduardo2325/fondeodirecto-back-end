package com.fondeodirecto.models;

import java.math.BigDecimal;
import java.util.List;

public class Invoice {
    private String client_rfc;
    private String company_rfc;
    private BigDecimal subtotal;
    private String serie;
    private String folio;
    private String uuid;
    private String company_regime;
    private List<Item> items;
    private String cadenaOriginal;
    private String sat_digital_stamp;
    private String cfdi_digital_stamp;
    private String company_address;
    private String company_postal_code;
    private String emission_date;
    private String written_amount;
    private BigDecimal tax_total;
    private BigDecimal tax_percentage;
    private BigDecimal total;

    public void setClient_rfc(String client_rfc) {
        this.client_rfc = client_rfc;
    }

    public void setCompany_rfc(String company_rfc) {
        this.company_rfc = company_rfc;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setCompany_regime(String company_regime) {
        this.company_regime = company_regime;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setCadenaOriginal(String cadenaOriginal) {
        this.cadenaOriginal = cadenaOriginal;
    }

    public void setSat_digital_stamp(String sat_digital_stamp) {
        this.sat_digital_stamp = sat_digital_stamp;
    }

    public void setCfdi_digital_stamp(String cfdi_digital_stamp) {
        this.cfdi_digital_stamp = cfdi_digital_stamp;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public void setCompany_postal_code(String company_postal_code) {
        this.company_postal_code = company_postal_code;
    }

    public void setEmission_date(String emission_date) {
        this.emission_date = emission_date;
    }

    public void setTax_total(BigDecimal tax_total) {
        this.tax_total = tax_total;
    }

    public void setTax_percentage(BigDecimal tax_percentage) {
        this.tax_percentage = tax_percentage;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
