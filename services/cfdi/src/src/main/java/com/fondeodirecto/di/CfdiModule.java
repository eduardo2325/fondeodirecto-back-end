package com.fondeodirecto.di;

import com.fondeodirecto.cfdi.CfdiValidator;
import com.fondeodirecto.cfdi.InvoiceImpl;
import com.fondeodirecto.helpers.Loader;
import com.fondeodirecto.models.Invoice;
import com.google.gson.Gson;
import dagger.Module;
import dagger.Provides;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import javax.inject.Singleton;

/**
 * Created by zhntr on 7/6/17.
 */
@Module
public class CfdiModule {

    @Singleton
    @Provides
    public CfdiValidator providesCfdiValidator(Server server) {
        return new CfdiValidator(server);
    }

    @Singleton
    @Provides
    public Server providesServer(InvoiceImpl invoice) {
        int port = Integer.parseInt(System.getenv("PORT"));
        return ServerBuilder.forPort(port)
                .addService(invoice)
                .build();
    }

    @Singleton
    @Provides
    public InvoiceImpl providesInvoiceService(Loader loader) {
        return new InvoiceImpl(loader, new Gson());
    }

    @Singleton
    @Provides
    public Loader providesLoader() {
        return new Loader();
    }
}
