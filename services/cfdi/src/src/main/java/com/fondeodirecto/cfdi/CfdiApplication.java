package com.fondeodirecto.cfdi;

import com.fondeodirecto.di.CfdiAppComponent;
import com.fondeodirecto.di.DaggerCfdiAppComponent;

import java.io.IOException;

/**
 * Created by zhntr on 7/6/17.
 */
public class CfdiApplication {
    void run() throws IOException, InterruptedException {
        CfdiAppComponent component = DaggerCfdiAppComponent.create();
        CfdiValidator validator = component.validator();
        validator.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may have been reset by its JVM shutdown hook.
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            validator.stop();
            System.err.println("*** server shut down");
        }));

        validator.blockUntilShutdown();
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        new CfdiApplication().run();
    }
}
