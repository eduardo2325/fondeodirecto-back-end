package com.fondeodirecto.cfdi;

import com.fondeodirecto.helpers.Loader;
import com.google.gson.Gson;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import mx.bigdata.sat.common.CFD;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by alberto on 5/30/17.
 */
public class CfdiValidator {

    private Server server;

    public CfdiValidator(Server server) {
        this.server = server;
    }

    public void start() throws IOException {
        if (server != null) {
            server.start();
        }
   }

    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    public void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

}
