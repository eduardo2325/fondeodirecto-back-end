package com.fondeodirecto.cfdi;

/**
 * Created by alberto on 5/31/17.
 */
public class LogMessageFactory {

    public static LogMessage info(String msg, Object data, String type, String guid) {
        return new LogMessage(msg, data, type, guid);
    }

    public static LogMessage error(Throwable error, String guid) {
        return new LogMessage(error, guid);
    }
}
