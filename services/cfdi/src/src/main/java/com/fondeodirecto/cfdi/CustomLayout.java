package com.fondeodirecto.cfdi;

import com.fondeodirecto.utils.FondeoExclusionStrategy;
import com.fondeodirecto.utils.FondeoFieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import uk.me.mjt.log4jjson.SimpleJsonLayout;

import java.util.*;

/**
 * Created by alberto on 5/31/17.
 */
public class CustomLayout extends SimpleJsonLayout {

    private final Gson gson = new GsonBuilder()
            .setExclusionStrategies(new FondeoExclusionStrategy())
            .setFieldNamingStrategy(new FondeoFieldNamingStrategy())
            .create();

    private Level minimumLevelForSlowLogging = Level.ALL;
    private List<String> mdcFieldsToLog = Collections.EMPTY_LIST;

    @Override
    public String format(LoggingEvent le) {
        Map<String,Object> r = new LinkedHashMap();
        r.put("level", le.getLevel().toString());
        r.put("data", safeToString(gson.toJson(le.getMessage())));

        after(le,r);
        return gson.toJson(r)+"\n";
    }

    private static String safeToString(Object obj) {
        if (obj==null) {
            return null;
        }
        try {
            return obj.toString();
        } catch (Throwable t) {
            return "Error getting message: "+ t.getMessage();
        }
    }
}
