package com.fondeodirecto.cfdi;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by alberto on 5/31/17.
 */
public class LogMessage {
    Object data;
    String error;
    String msg;
    String guid;
    String type;
    String stack;
    long timestamp;

    public LogMessage(String msg, Object data, String type, String guid) {
        this.msg = msg;
        this.data = data;
        this.type = type;
        this.guid = guid;
        this.timestamp = System.currentTimeMillis();
    }

    public LogMessage(Throwable error, String guid) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        error.printStackTrace(pw);

        this.msg = error.getMessage();
        this.stack = sw.toString();
        this.guid = guid;
        this.timestamp = System.currentTimeMillis();
    }
}
