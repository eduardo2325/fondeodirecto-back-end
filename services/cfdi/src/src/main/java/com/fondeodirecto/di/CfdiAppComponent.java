package com.fondeodirecto.di;

import com.fondeodirecto.cfdi.CfdiValidator;
import dagger.Component;

import javax.inject.Singleton;

/**
 * Created by zhntr on 7/6/17.
 */

@Singleton
@Component(modules = CfdiModule.class)
public interface CfdiAppComponent {
    CfdiValidator validator();
}
