package com.fondeodirecto.cfdi;

import com.fondeodirecto.cfdi.grpc.InvoiceGrpc;
import com.fondeodirecto.cfdi.grpc.InvoiceInfo;
import com.fondeodirecto.cfdi.grpc.XmlInvoice;
import com.fondeodirecto.helpers.Loader;
import com.google.gson.Gson;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.apache.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by zhntr on 7/6/17.
 */
public class InvoiceImpl extends InvoiceGrpc.InvoiceImplBase {
    private static final Logger logger = Logger.getLogger(InvoiceImpl.class.getName());
    private static final String PATH_KEY = "path";
    private static final String XML = "XML";
    private static final String MESSAGE_KEY = "message";


    private Loader loader;
    private Gson gson;

    public InvoiceImpl(Loader loader, Gson gson) {
        this.loader = loader;
        this.gson = gson;
    }

    @Override
    public void validateAndExtract(XmlInvoice req, StreamObserver<InvoiceInfo> responseObserver) {

        logger.info(LogMessageFactory.info("Validating xml format", req, "request", req.getGuid()));

        try {
            String result = loader.loadCFD(req.getXml());

            InvoiceInfo reply = InvoiceInfo.newBuilder().setDigest(result).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(LogMessageFactory.error(e, req.getGuid()));

            Map<String, Object> r = new LinkedHashMap<>();
            r.put(PATH_KEY, XML);
            r.put(MESSAGE_KEY, "Invalid format");

            responseObserver.onError(Status.INTERNAL.withDescription(gson.toJson(r))
                    .asRuntimeException());
        }
    }
}
