package com.fondeodirecto.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by zhntr on 8/16/17.
 */
public class FondeoExclusionStrategy implements ExclusionStrategy {
    private List<String> forbiddenFields = Collections.emptyList();

    public FondeoExclusionStrategy() {
        ClassLoader loader = this.getClass().getClassLoader();
        InputStream stream = loader.getResourceAsStream("rules/forbidden.json");
        Gson gson = new Gson();
        String[] forbiddenFields = gson.fromJson(new JsonReader(new InputStreamReader(stream)), String[].class);
        this.forbiddenFields = Arrays.asList(forbiddenFields);
    }

    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        return forbiddenFields.contains(getNormalizedName(f.getName()));
    }

    private String getNormalizedName(String name) {
       if (name != null && name.charAt(name.length() - 1) == '_') {
           return name.substring(0, name.length() - 1);
       }
       return name;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
