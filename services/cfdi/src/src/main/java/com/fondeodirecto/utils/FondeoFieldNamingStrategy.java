package com.fondeodirecto.utils;

import com.google.gson.FieldNamingStrategy;

import java.lang.reflect.Field;

/**
 * Created by zhntr on 8/16/17.
 */
public class FondeoFieldNamingStrategy implements FieldNamingStrategy {
    @Override
    public String translateName(Field f) {
        if (f.getName().endsWith("_")) {
            return f.getName().substring(0, f.getName().length() - 1);
        }
        return f.getName();
    }
}
