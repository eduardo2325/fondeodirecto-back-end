#!/bin/bash

set -e

npm install

source /var/lib/core/database/postgres-create.sh
source /var/lib/core/database/sequelize-migrations.sh
