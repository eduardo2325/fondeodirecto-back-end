#!/bin/bash

set -e

source "$(dirname "$0")/deps.sh"

wait-for-it -t 0 kafka:9092

[ -d unit_coverage ] || mkdir -p unit_coverage

npm run test:coverage:unit
npm run test:report -- -r text-lcov > unit_coverage/lcov.info
