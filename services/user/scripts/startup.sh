#!/bin/bash

set -x -u -e

aws-secrets-manager.sh backend/${AWS_SM_ENV}/${AWS_SM_SERVICE} -- ./node_modules/.bin/sequelize db:migrate --config config/config.js
aws-secrets-manager.sh backend/${AWS_SM_ENV}/${AWS_SM_SERVICE} -- ./node_modules/.bin/sequelize db:seed:all --config config/config.js
aws-secrets-manager.sh backend/${AWS_SM_ENV}/${AWS_SM_SERVICE} -- node app.js
