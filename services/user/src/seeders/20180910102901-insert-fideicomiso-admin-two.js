'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users', [ {
      name: 'Fideicomiso 2',
      password: 'not used',
      email: 'bernardo@eliteempresarial.com.mx',
      company_id: 10002,
      role: 'ADMIN-FIDE',
      created_at: new Date(),
      updated_at: new Date()
    } ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', {
      email: 'bernardo@eliteempresarial.com.mx'
    });
  }
};

