'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users', [ {
      name: 'Sonja Kosunen',
      password: '$2a$10$KCnbhEipoxLaeok3qkEqN.efuEl4NHKDQTbLLFiSwrxL1f93hbmLm',
      email: 'skosunen@fondeodirecto.com',
      company_id: 10001,
      role: 'ADMIN',
      created_at: new Date(),
      updated_at: new Date()
    } ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', {
      email: 'skosunen@fondeodirecto.com'
    });
  }
};

