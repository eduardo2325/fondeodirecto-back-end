'use strict';

const notificationTypeList = require('../vendor/notification-types-list');

module.exports = {
  up: queryInterface => {
    notificationTypeList.forEach(notificationType => {
      notificationType.created_at = new Date();
      notificationType.updated_at = new Date();
    });
    return queryInterface.bulkInsert('notification_types', notificationTypeList);
  },

  down: queryInterface => {
    const ids = notificationTypeList.map(notificationType => {
      return notificationType.id;
    });

    return queryInterface.bulkDelete('notification_types', {
      id: {
        $in: ids
      }
    });
  }
};
