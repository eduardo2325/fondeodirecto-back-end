'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users', [ {
      name: 'Galo Admin',
      password: '$2a$10$KCnbhEipoxLaeok3qkEqN.efuEl4NHKDQTbLLFiSwrxL1f93hbmLm',
      email: 'galo@fondeodirecto.com',
      company_id: 10001,
      role: 'ADMIN',
      created_at: new Date(),
      updated_at: new Date()
    } ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', {
      email: 'galo@fondeodirecto.com'
    });
  }
};
