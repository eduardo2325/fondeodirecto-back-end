'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('companies', [ {
      id: 10001,
      rfc: 'FODA900806965',
      name: 'Fondeo Directo',
      business_name: 'Plataforma FD, S.A.P.I. de C.V.',
      holder: 'Plataforma FD, S.A.P.I. de C.V.',
      clabe: '113180000001316759',
      bank: 'Ve por más',
      bank_account: '131675',
      role: 'ADMIN',
      created_at: new Date(),
      updated_at: new Date()
    } ])
      .then(() => {
        return queryInterface.bulkInsert('users', [ {
          name: 'Admin',
          password: '$2a$10$4oyzo8iMYMHBKs5l0z87m.HCjuTlTiy9tajEXE2VaeTvwMhO6FLxK',
          email: 'dev@fondeodirecto.com',
          company_id: 10001,
          role: 'ADMIN',
          created_at: new Date(),
          updated_at: new Date()
        } ]);
      });
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', {
      email: 'dev@fondeodirecto.com'
    })
      .then(() => {
        return queryInterface.bulkDelete('companies', {
          rfc: 'FODA900806965'
        });
      });
  }
};
