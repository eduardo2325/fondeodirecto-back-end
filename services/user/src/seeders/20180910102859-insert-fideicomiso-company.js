'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('companies', [ {
      id: 10002,
      rfc: '0000000000001',
      name: 'Fideicomiso FD',
      business_name: 'FIDEICOMISO PLATAFORMA FD 01/2018',
      holder: 'FIDEICOMISO PLATAFORMA FD 01/2018',
      clabe: '060320000989711907',
      bank: 'BANSI',
      bank_account: '00098971190',
      role: 'ADMIN',
      created_at: new Date(),
      updated_at: new Date()
    } ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('companies', {
      rfc: '0000000000001'
    });
  }
};
