'use strict';

const bankList = require('../vendor/bank-list');

module.exports = {
  up: queryInterface => {
    bankList.forEach(bank => {
      bank.created_at = new Date();
      bank.updated_at = new Date();
    });
    return queryInterface.bulkInsert('bank_codes', bankList);
  },

  down: queryInterface => {
    const ids = bankList.map(bank => {
      return bank.id;
    });

    return queryInterface.bulkDelete('bank_codes', {
      id: {
        $in: ids
      }
    });
  }
};
