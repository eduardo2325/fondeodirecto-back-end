'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users', [ {
      name: 'Admin Admin',
      password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
      email: 'francisco+administrador@agavelab.com',
      company_id: 10001,
      role: 'ADMIN',
      created_at: new Date(),
      updated_at: new Date()
    } ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', {
      email: 'francisco+administrador@agavelab.com'
    });
  }
};
