'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users', [ {
      name: 'Fideicomiso 1',
      password: 'not used',
      email: 'factoraje@eliteempresarial.com.mx',
      company_id: 10002,
      role: 'ADMIN-FIDE',
      created_at: new Date(),
      updated_at: new Date()
    } ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', {
      email: 'factoraje@eliteempresarial.com.mx'
    });
  }
};

