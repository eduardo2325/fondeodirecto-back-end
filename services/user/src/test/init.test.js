const app = require('../app');

before(() => app.initPromise);

// after is not working after failures...
process.on('exit', () => {
  app.shutdown();
});
