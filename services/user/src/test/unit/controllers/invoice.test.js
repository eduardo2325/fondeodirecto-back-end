const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/invoice');
const errorHandler = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/controllers/invoice');

const { Invoice, User, Operation, PendingTransaction, Order, Transaction,
  UserActivity, sequelize } = require('../../../models');
const stateMachine = require('../../../api/helpers/state-machine');
const shoppingCart = require('../../../api/helpers/shopping-cart');
const userProducer = require('../../../api/producers/user');
const expect = chai.expect;
const SendengoStrategy = require('../../../api/helpers/sendengo-strategy');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Invoice controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('create', () => {
    const fixtures = helperFixtures.create;
    const { request, invoice, commonError, validateParams, instances,
      createParams, newInvoice, generalInfoParams, guid, companyCantApproveInvoiceError } = fixtures;

    beforeEach(() => {
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'create').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(Invoice, 'validateCompanies').callsFake(() => Promise.resolve(instances));
      sandbox.stub(invoice, 'getGeneralInfo').callsFake(() => Promise.resolve(newInvoice));
      sandbox.stub(instances[1], 'allowedCXP').callsFake(() => Promise.resolve(true));
      sandbox.stub(userProducer, 'invoiceCreated').callsFake(() => Promise.resolve());
    });

    it('should return an error if Invoice.validateCompanies return a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateCompanies.restore();
      sandbox.stub(Invoice, 'validateCompanies').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          Invoice.validateCompanies.calledOnce.should.be.true;
          Invoice.validateCompanies.args[0].should.be.eql(validateParams);
          instances[1].allowedCXP.called.should.be.false;
          Invoice.create.called.should.be.false;
          userProducer.invoiceCreated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.allowedCXP returns false', () => {
      const callback = sandbox.spy();

      instances[1].allowedCXP.restore();
      sandbox.stub(instances[1], 'allowedCXP').callsFake(() => Promise.resolve(false));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          Invoice.validateCompanies.calledOnce.should.be.true;
          Invoice.validateCompanies.args[0].should.be.eql(validateParams);
          instances[1].allowedCXP.calledOnce.should.be.true;
          instances[1].allowedCXP.calledWith().should.be.true;
          Invoice.create.called.should.be.false;
          userProducer.invoiceCreated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(companyCantApproveInvoiceError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Invoice.create return a reject', () => {
      const callback = sandbox.spy();

      Invoice.create.restore();
      sandbox.stub(Invoice, 'create').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          Invoice.validateCompanies.calledOnce.should.be.true;
          Invoice.validateCompanies.args[0].should.be.eql(validateParams);
          instances[1].allowedCXP.calledOnce.should.be.true;
          instances[1].allowedCXP.calledWith().should.be.true;
          Invoice.create.calledOnce.should.be.true;
          Invoice.create.args[0].should.be.eql(createParams);
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.invoiceCreated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getGeneralInfo.restore();
      sandbox.stub(invoice, 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          Invoice.validateCompanies.calledOnce.should.be.true;
          Invoice.validateCompanies.args[0].should.be.eql(validateParams);
          instances[1].allowedCXP.calledOnce.should.be.true;
          instances[1].allowedCXP.calledWith().should.be.true;
          Invoice.create.calledOnce.should.be.true;
          Invoice.create.args[0].should.be.eql(createParams);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.args[0].should.be.eql(generalInfoParams);
          userProducer.invoiceCreated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceCreated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceCreated.restore();
      sandbox.stub(userProducer, 'invoiceCreated').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          Invoice.validateCompanies.calledOnce.should.be.true;
          Invoice.validateCompanies.args[0].should.be.eql(validateParams);
          instances[1].allowedCXP.calledOnce.should.be.true;
          instances[1].allowedCXP.calledWith().should.be.true;
          Invoice.create.calledOnce.should.be.true;
          Invoice.create.args[0].should.be.eql(createParams);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.args[0].should.be.eql(generalInfoParams);
          userProducer.invoiceCreated.calledOnce.should.be.true;
          userProducer.invoiceCreated.args[0].should.be.eql([ newInvoice, guid ]);
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          Invoice.validateCompanies.calledOnce.should.be.true;
          Invoice.validateCompanies.args[0].should.be.eql(validateParams);
          instances[1].allowedCXP.calledOnce.should.be.true;
          instances[1].allowedCXP.calledWith().should.be.true;
          Invoice.create.calledOnce.should.be.true;
          Invoice.create.args[0].should.be.eql(createParams);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.args[0].should.be.eql(generalInfoParams);
          userProducer.invoiceCreated.calledOnce.should.be.true;
          userProducer.invoiceCreated.args[0].should.be.eql([ newInvoice, guid ]);
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, newInvoice).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('bulkDelete', () => {
    const fixtures = helperFixtures.bulkDelete;
    const { request, response, message, transaction, guid, requestType, responseType, commonError, invoices,
      findParams, destroyParams, notFound, activityData, options } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(Invoice, 'destroy').callsFake(() => Promise.resolve(true));
      _.forEach(invoices, i => {
        sandbox.stub(i, 'update').resolves(true);
      });
      sandbox.stub(UserActivity, 'registerDeleteInvoices').resolves(true);
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
    });

    it('should return an error if Invoice.findAll return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.bulkDelete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findParams).should.be.true;
          invoices[0].update.calledOnce.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });


    it('should return an error if Invoice.findAll return empty array', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').resolves([]);

      return controller.bulkDelete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findParams).should.be.true;
          invoices[0].update.calledOnce.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if any of the invoices returned rejects', () => {
      const callback = sandbox.spy();

      invoices[1].update.restore();
      sandbox.stub(invoices[1], 'update').callsFake(() => Promise.reject(commonError));

      return controller.bulkDelete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findParams).should.be.true;
          invoices[0].update.calledOnce.should.be.true;
          invoices[1].update.calledOnce.should.be.true;
          invoices[2].update.calledOnce.should.be.true;
          Invoice.destroy.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.destroy rejects', () => {
      const callback = sandbox.spy();

      Invoice.destroy.restore();
      sandbox.stub(Invoice, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.bulkDelete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.destroy.calledOnce.should.be.true;
          Invoice.destroy.calledWithMatch(destroyParams).should.be.true;
          UserActivity.registerDeleteInvoices.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });


    it('should return an error if UserActivity.registerDeleteInvoices rejects', () => {
      const callback = sandbox.spy();

      UserActivity.registerDeleteInvoices.restore();
      sandbox.stub(UserActivity, 'registerDeleteInvoices').callsFake(() => Promise.reject(commonError));

      return controller.bulkDelete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.destroy.calledOnce.should.be.true;
          Invoice.destroy.calledWithMatch(destroyParams).should.be.true;
          UserActivity.registerDeleteInvoices.called.should.be.true;
          UserActivity.registerDeleteInvoices.calledWith(activityData, invoices, options).should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.bulkDelete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvoices', () => {
    const fixtures = helperFixtures.getInvoices;
    const { request, response, message, guid, requestType, responseType, invoices,
      commonError, cxcUser, cxpUser, whereUser, whereCxc, whereCxp } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'getInvoices').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.resolve(4));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxcUser));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.called.should.be.false;
          Invoice.count.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.getInvoices return a reject', () => {
      const callback = sandbox.spy();

      Invoice.getInvoices.restore();
      sandbox.stub(Invoice, 'getInvoices').callsFake(() => Promise.reject(commonError));

      return controller.getInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(cxcUser, whereCxc, request.request).should.be.true;
          Invoice.count.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.count return a reject', () => {
      const callback = sandbox.spy();

      Invoice.count.restore();
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.reject(commonError));

      return controller.getInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(cxcUser, whereCxc, request.request).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response with cxcUser', () => {
      const callback = sandbox.spy();

      return controller.getInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(cxcUser, whereCxc, request.request).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response with cxpUser', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxpUser));

      return controller.getInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(cxpUser, whereCxp, request.request).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvoice', () => {
    const fixtures = helperFixtures.getInvoice;
    const { request, response, message, guid, requestType, responseType, notFound,
      commonError, userInformation, invoice, invoiceGeneralInfo, whereUser, whereInvoice,
      invoiceWrongCompanyId, userInvestor, publishedInvoice, fundedInvoice, userCxc } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(invoice, 'getBasicInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(invoice, 'getBasicInfoWithClient').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(publishedInvoice, 'getBasicInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(publishedInvoice, 'getBasicInfoWithClient').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(fundedInvoice, 'getBasicInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(fundedInvoice, 'getBasicInfoWithClient').callsFake(() => Promise.resolve(invoiceGeneralInfo));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if user does not exists', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if invoice does not exists', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if invoice does not belong to the current user', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoiceWrongCompanyId));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if investor try to get a not published invoice', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInvestor));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if investor try to get an invoice that is not funded by him', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInvestor));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getBasicInfo.restore();
      sandbox.stub(invoice, 'getBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.calledOnce.should.be.true;
          invoice.getBasicInfoWithClient.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.calledOnce.should.be.true;
          invoice.getBasicInfoWithClient.called.should.be.false;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response if cxc get a funded invoice', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userCxc));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getBasicInfo.calledOnce.should.be.false;
          invoice.getBasicInfoWithClient.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response if investor get a funded invoice', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInvestor));
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(fundedInvoice));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          fundedInvoice.getBasicInfo.calledOnce.should.be.false;
          fundedInvoice.getBasicInfoWithClient.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response if investor get a published invoice', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInvestor));
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(publishedInvoice));

      return controller.getInvoice(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          publishedInvoice.getBasicInfo.calledOnce.should.be.false;
          publishedInvoice.getBasicInfoWithClient.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('approve', () => {
    const fixtures = helperFixtures.approve;
    const { request, response, message, guid, requestType, responseType, commonError,
      invoice, expiration, user_id, invoice_id } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'validateExpirationDate').callsFake(() => Promise.resolve(new Date()));
      sandbox.stub(Invoice, 'validateCxp').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invoiceApproved').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'notifyInvoiceApproved').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection returns a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.validateExpirationDate returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateExpirationDate.restore();
      sandbox.stub(Invoice, 'validateExpirationDate').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.calledOnce.should.be.true;
          Invoice.validateExpirationDate.args[0].should.be.eql([ expiration ]);
          Invoice.validateCxp.calledOnce.should.be.false;
          stateMachine.next.calledOnce.should.be.false;
          userProducer.invoiceApproved.calledOnce.should.be.false;
          userProducer.notifyInvoiceApproved.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.validateCxp returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateCxp.restore();
      sandbox.stub(Invoice, 'validateCxp').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.calledOnce.should.be.true;
          Invoice.validateExpirationDate.args[0].should.be.eql([ expiration ]);
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.next.calledOnce.should.be.false;
          userProducer.invoiceApproved.calledOnce.should.be.false;
          userProducer.notifyInvoiceApproved.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.next returns a reject', () => {
      const callback = sandbox.spy();

      stateMachine.next.restore();
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.calledOnce.should.be.true;
          Invoice.validateExpirationDate.args[0].should.be.eql([ expiration ]);
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ invoice ]);
          userProducer.invoiceApproved.calledOnce.should.be.false;
          userProducer.notifyInvoiceApproved.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceApproved returns a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceApproved.restore();
      sandbox.stub(userProducer, 'invoiceApproved').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.calledOnce.should.be.true;
          Invoice.validateExpirationDate.args[0].should.be.eql([ expiration ]);
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ invoice ]);
          userProducer.invoiceApproved.calledOnce.should.be.true;
          userProducer.invoiceApproved.args[0].should.be.eql([ invoice, guid ]);
          userProducer.notifyInvoiceApproved.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.notifyInvoiceApproved returns a reject', () => {
      const callback = sandbox.spy();

      userProducer.notifyInvoiceApproved.restore();
      sandbox.stub(userProducer, 'notifyInvoiceApproved').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.calledOnce.should.be.true;
          Invoice.validateExpirationDate.args[0].should.be.eql([ expiration ]);
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ invoice ]);
          userProducer.invoiceApproved.calledOnce.should.be.true;
          userProducer.invoiceApproved.args[0].should.be.eql([ invoice, guid ]);
          userProducer.notifyInvoiceApproved.calledOnce.should.be.true;
          userProducer.notifyInvoiceApproved.args[0].should.be.eql([ [ invoice ], guid ]);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateExpirationDate.calledOnce.should.be.true;
          Invoice.validateExpirationDate.args[0].should.be.eql([ expiration ]);
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ invoice ]);
          userProducer.invoiceApproved.calledOnce.should.be.true;
          userProducer.invoiceApproved.args[0].should.be.eql([ invoice, guid ]);
          userProducer.notifyInvoiceApproved.calledOnce.should.be.true;
          userProducer.notifyInvoiceApproved.args[0].should.be.eql([ [ invoice ], guid ]);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getEstimate', () => {
    const fixtures = helperFixtures.getEstimate;
    const { request, response, message, guid, requestType, responseType,
      commonError, userInformation, invoice, invoiceGeneralInfo, whereUser, whereInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(invoice, 'getEstimate').callsFake(() => Promise.resolve(invoiceGeneralInfo));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getEstimate.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getEstimate.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getEstimate.restore();
      sandbox.stub(invoice, 'getEstimate').callsFake(() => Promise.reject(commonError));

      return controller.getEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getEstimate.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getEstimate.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getDetail', () => {
    const fixtures = helperFixtures.getDetail;
    const { request, response, message, guid, requestType, responseType,
      commonError, userInformation, invoice, invoiceGeneralInfo, whereUser, whereInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(invoice, 'getDetail').callsFake(() => Promise.resolve(invoiceGeneralInfo));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getDetail.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getDetail.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getDetail.restore();
      sandbox.stub(invoice, 'getDetail').callsFake(() => Promise.reject(commonError));

      return controller.getDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getDetail.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getDetail.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('reject', () => {
    const fixtures = helperFixtures.reject;
    const { request, response, message, guid, requestType, responseType, commonError, instances,
      invoice, invoiceGeneralInfo, company, reason, user_id, invoice_id, reasonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'validateCxp').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.resolve(instances));
      sandbox.stub(instances[0], 'getCompany').callsFake(() => Promise.resolve(company));
      sandbox.stub(instances[0], 'getGeneralInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invoiceRejected').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if a reason is not given', () => {
      const callback = sandbox.spy();
      const cloneRequest = _.cloneDeep(request);

      delete cloneRequest.request.reason;

      return controller.reject(cloneRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, cloneRequest.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(reasonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.validateCxp return a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateCxp.restore();
      sandbox.stub(Invoice, 'validateCxp').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.back.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.back return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.back.restore();
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          instances[0].getCompany.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getCompany return a reject', () => {
      const callback = sandbox.spy();

      instances[0].getCompany.restore();
      sandbox.stub(instances[0], 'getCompany').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfo return a reject', () => {
      const callback = sandbox.spy();

      instances[0].getGeneralInfo.restore();
      sandbox.stub(instances[0], 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.calledOnce.should.be.true;
          instances[0].getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.invoiceRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceRejected return a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceRejected.restore();
      sandbox.stub(userProducer, 'invoiceRejected').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.calledOnce.should.be.true;
          instances[0].getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.invoiceRejected.calledOnce.should.be.true;
          userProducer.invoiceRejected.args[0].should.be.eql([ instances[0], reason, guid ]);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.validateCxp.calledOnce.should.be.true;
          Invoice.validateCxp.args[0].should.be.eql([ user_id, invoice_id ]);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.calledOnce.should.be.true;
          instances[0].getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.invoiceRejected.calledOnce.should.be.true;
          userProducer.invoiceRejected.args[0].should.be.eql([ instances[0], reason, guid ]);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getPublishSummary', () => {
    const fixtures = helperFixtures.getPublishSummary;
    const { request, message, guid, requestType, responseType, commonError,
      invoices, invoiceQuery, publishEstimate, publishSummaryTotals, publishSummaryResponse } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      invoices.forEach(invoice => {
        sandbox.stub(invoice, 'getPublishEstimate').callsFake(() => Promise.resolve(publishEstimate));
      });
      sandbox.stub(shoppingCart, 'getPublishSummaryTotals').callsFake(() => Promise.resolve(publishSummaryTotals));
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.getPublishSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            invoice.getPublishEstimate.calledOnce.should.be.false;
          });
          shoppingCart.getPublishSummaryTotals.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getPublishEstimate returns a reject', () => {
      const callback = sandbox.spy();

      invoices.forEach(invoice => {
        invoice.getPublishEstimate.restore();
        sandbox.stub(invoice, 'getPublishEstimate').callsFake(() => Promise.reject(commonError));
      });

      return controller.getPublishSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            invoice.getPublishEstimate.calledOnce.should.be.true;
          });
          shoppingCart.getPublishSummaryTotals.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if shoppingCart.getPublishSummaryTotals returns a reject', () => {
      const callback = sandbox.spy();

      shoppingCart.getPublishSummaryTotals.restore();
      sandbox.stub(shoppingCart, 'getPublishSummaryTotals').callsFake(() => Promise.reject(commonError));

      return controller.getPublishSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            invoice.getPublishEstimate.calledOnce.should.be.true;
          });
          shoppingCart.getPublishSummaryTotals.calledOnce.should.be.true;
          shoppingCart.getPublishSummaryTotals.calledWithMatch(publishEstimate);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getPublishSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            invoice.getPublishEstimate.calledOnce.should.be.true;
          });
          shoppingCart.getPublishSummaryTotals.calledOnce.should.be.true;
          shoppingCart.getPublishSummaryTotals.calledWithMatch(publishEstimate);
          log.message.secondCall.calledWithMatch(message, publishSummaryResponse, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, publishSummaryResponse).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('publish', () => {
    const fixtures = helperFixtures.publish;
    const { request, response, message, guid, requestType, responseType, commonError, operation,
      invoices, invoiceQuery, company, companyCantApproveInvoiceError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      invoices.forEach(invoice => {
        sandbox.stub(Invoice, 'validateCxc').callsFake(() => Promise.resolve());
        sandbox.stub(invoice, 'getClient').callsFake(() => Promise.resolve(company));
        sandbox.stub(stateMachine, 'next').callsFake(() => Promise.resolve());
        sandbox.stub(Invoice, 'createOperation').callsFake(() => Promise.resolve(operation));
        sandbox.stub(company, 'allowedCXP').callsFake(() => Promise.resolve(true));
      });
      sandbox.stub(userProducer, 'notifyInvoicePublished').callsFake(() => Promise.resolve());
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.false;
            invoice.getClient.calledOnce.should.be.false;
            company.allowedCXP.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.validateCxc returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateCxc.restore();
      sandbox.stub(Invoice, 'validateCxc').callsFake(() => Promise.reject(commonError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.false;
            company.allowedCXP.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.validateCxc returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateCxc.restore();
      sandbox.stub(Invoice, 'validateCxc').callsFake(() => Promise.reject(commonError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.false;
            company.allowedCXP.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getClient returns a reject', () => {
      const callback = sandbox.spy();

      invoices.forEach(invoice => {
        invoice.getClient.restore();
        sandbox.stub(invoice, 'getClient').callsFake(() => Promise.reject(commonError));
      });

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.true;
            company.allowedCXP.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if company.allowedCXP returns a reject', () => {
      const callback = sandbox.spy();

      company.allowedCXP.restore();
      sandbox.stub(company, 'allowedCXP').callsFake(() => Promise.reject(commonError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.true;
            company.allowedCXP.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if company.allowedCXP returns false', () => {
      const callback = sandbox.spy();

      company.allowedCXP.restore();
      sandbox.stub(company, 'allowedCXP').callsFake(() => Promise.reject(companyCantApproveInvoiceError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.true;
            company.allowedCXP.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(companyCantApproveInvoiceError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.next return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.next.restore();
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.reject(commonError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.true;
            company.allowedCXP.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.args[0].should.be.eql([ invoice ]);
            invoice.published.should.not.be.null;
            Invoice.createOperation.calledOnce.should.be.false;
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.createOperation return a reject', () => {
      const callback = sandbox.spy();

      Invoice.createOperation.restore();
      sandbox.stub(Invoice, 'createOperation').callsFake(() => Promise.reject(commonError));

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.true;
            company.allowedCXP.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.args[0].should.be.eql([ invoice ]);
            invoice.published.should.not.be.null;
            Invoice.createOperation.calledOnce.should.be.true;
            Invoice.createOperation.args[0].should.be.eql([ invoice, request.request.user_id ]);
          });
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.publish(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          invoices.forEach(invoice => {
            Invoice.validateCxc.calledOnce.should.be.true;
            Invoice.validateCxc.args[0].should.be.eql([ request.request.user_id, invoice.id ]);
            invoice.getClient.calledOnce.should.be.true;
            company.allowedCXP.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.args[0].should.be.eql([ invoice ]);
            invoice.published.should.not.be.null;
            Invoice.createOperation.calledOnce.should.be.true;
            Invoice.createOperation.args[0].should.be.eql([ invoice, request.request.user_id ]);
          });
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('fund', () => {
    const fixtures = helperFixtures.fund;
    const { request, shoppingCartResponse, message, guid, companyId, requestType, responseType, commonError,
      invoices, availableInvoice, user, balance, successfulResponse, userQuery, invoiceQuery,
      balanceNotEnough, shoppingCartNegativeGain, notEnoughBalanceError, negativeGainError, basicOperationInfo,
      transaction, failResponse, shoppingCartUnavailableInvoices } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(Invoice, 'createInvestorTransaction').callsFake(() => Promise.resolve());
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      invoices.forEach(invoice => {
        sandbox.stub(invoice, 'getInvoiceAmounts').callsFake(() => Promise.resolve(availableInvoice));
        sandbox.stub(invoice, 'save').callsFake(() => Promise.resolve());
        sandbox.stub(invoice, 'getBasicOperationInfo').callsFake(() => Promise.resolve(basicOperationInfo));
      });
      sandbox.stub(shoppingCart, 'getCartResponse').callsFake(() => Promise.resolve(shoppingCartResponse));
      sandbox.stub(Order, 'newFundOrder').callsFake(() => Promise.resolve());
      sandbox.stub(user.Company, 'getBalance').callsFake(() => Promise.resolve(balance));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invoiceFundRequest').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'notifyFundRequest').callsFake(() => Promise.resolve());
      sandbox.stub(PendingTransaction, 'invoiceOperation').callsFake(() => Promise.resolve());
      sandbox.stub(transaction, 'rollback').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection returns a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.false;
          Invoice.findAll.calledOnce.should.be.false;
          shoppingCart.getCartResponse.calledOnce.should.be.false;
          user.Company.getBalance.calledOnce.should.be.false;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if User.findOne returns a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.false;
          user.Company.getBalance.calledOnce.should.be.false;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.false;
          user.Company.getBalance.calledOnce.should.be.false;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a fund false on response if there is at least one unavailable invoice', () => {
      const callback = sandbox.spy();

      shoppingCart.getCartResponse.restore();
      sandbox.stub(shoppingCart, 'getCartResponse').callsFake(() => Promise.resolve(shoppingCartUnavailableInvoices));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          user.Company.getBalance.calledOnce.should.be.false;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.called.should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, failResponse).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an error if shoppingCart.getCartResponse returns a reject', () => {
      const callback = sandbox.spy();

      shoppingCart.getCartResponse.restore();
      sandbox.stub(shoppingCart, 'getCartResponse').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.false;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if user.Company.getBalance returns a reject', () => {
      const callback = sandbox.spy();

      user.Company.getBalance.restore();
      sandbox.stub(user.Company, 'getBalance').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if investor\'s balance is not enough', () => {
      const callback = sandbox.spy();

      user.Company.getBalance.restore();
      sandbox.stub(user.Company, 'getBalance').callsFake(() => Promise.resolve(balanceNotEnough));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notEnoughBalanceError, guid).should.be.true;
        });
    });

    it('should return an error if gain is negative', () => {
      const callback = sandbox.spy();

      shoppingCart.getCartResponse.restore();
      sandbox.stub(shoppingCart, 'getCartResponse').callsFake(() => Promise.resolve(shoppingCartNegativeGain));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.save.calledOnce.should.be.false;
            stateMachine.next.calledOnce.should.be.false;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(negativeGainError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.save returns a reject', () => {
      const callback = sandbox.spy();

      invoices.forEach(invoice => {
        invoice.save.restore();
        sandbox.stub(invoice, 'save').callsFake(() => Promise.reject(commonError));
      });

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.next returns a reject', () => {
      const callback = sandbox.spy();

      stateMachine.next.restore();
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.false;
            PendingTransaction.invoiceOperation.calledOnce.should.false;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.createInvestorTransaction returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.createInvestorTransaction.restore();
      sandbox.stub(Invoice, 'createInvestorTransaction').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.true;
            Invoice.createInvestorTransaction.calledWithMatch(invoice, user.Company).should.be.true;
            PendingTransaction.invoiceOperation.calledOnce.should.true;
            PendingTransaction.invoiceOperation.calledWithMatch(companyId, invoice, transaction).should.true;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.invoiceOperation returns a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.invoiceOperation.restore();
      sandbox.stub(PendingTransaction, 'invoiceOperation').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.true;
            Invoice.createInvestorTransaction.calledWithMatch(invoice, user.Company).should.be.true;
            PendingTransaction.invoiceOperation.calledOnce.should.true;
            PendingTransaction.invoiceOperation.calledWithMatch(companyId, invoice, transaction).should.true;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.false;
          userProducer.invoiceFundRequest.called.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Order.newFundOrder returns a reject', () => {
      const callback = sandbox.spy();

      Order.newFundOrder.restore();
      sandbox.stub(Order, 'newFundOrder').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.true;
            Invoice.createInvestorTransaction.calledWithMatch(invoice, user.Company).should.be.true;
            PendingTransaction.invoiceOperation.calledOnce.should.true;
            PendingTransaction.invoiceOperation.calledWithMatch(companyId, invoice, transaction).should.true;
            invoice.getBasicOperationInfo.calledOnce.should.be.false;
          });
          Order.newFundOrder.calledOnce.should.be.true;
          Order.newFundOrder.calledWithMatch(shoppingCartResponse.invoices, user.Company).should.be.true;
          userProducer.invoiceFundRequest.calledOnce.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicOperationInfo returns a reject', () => {
      const callback = sandbox.spy();

      invoices.forEach(invoice => {
        invoice.getBasicOperationInfo.restore();
        sandbox.stub(invoice, 'getBasicOperationInfo').callsFake(() => Promise.reject(commonError));
      });

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.true;
            Invoice.createInvestorTransaction.calledWithMatch(invoice, user.Company).should.be.true;
            PendingTransaction.invoiceOperation.calledOnce.should.true;
            PendingTransaction.invoiceOperation.calledWithMatch(companyId, invoice, transaction).should.true;
            invoice.getBasicOperationInfo.calledOnce.should.be.true;
          });
          Order.newFundOrder.calledOnce.should.be.true;
          Order.newFundOrder.calledWithMatch(shoppingCartResponse.invoices, user.Company).should.be.true;
          userProducer.invoiceFundRequest.calledOnce.should.be.false;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceFundRequest returns a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceFundRequest.restore();
      sandbox.stub(userProducer, 'invoiceFundRequest').callsFake(() => Promise.reject(commonError));

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.true;
            Invoice.createInvestorTransaction.calledWithMatch(invoice, user.Company).should.be.true;
            PendingTransaction.invoiceOperation.calledOnce.should.true;
            PendingTransaction.invoiceOperation.calledWithMatch(companyId, invoice, transaction).should.true;
            invoice.getBasicOperationInfo.calledOnce.should.be.true;
          });
          Order.newFundOrder.calledOnce.should.be.true;
          Order.newFundOrder.calledWithMatch(shoppingCartResponse.invoices, user.Company).should.be.true;
          userProducer.invoiceFundRequest.calledOnce.should.be.true;
          userProducer.invoiceFundRequest.calledWithMatch([ basicOperationInfo ], user.Company, guid).should.be.true;

          transaction.rollback.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.fund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceQuery).should.be.true;
          shoppingCart.getCartResponse.calledOnce.should.be.true;
          shoppingCart.getCartResponse.calledWithMatch([ availableInvoice ]).should.be.true;
          user.Company.getBalance.calledOnce.should.be.true;
          invoices.forEach(invoice => {
            invoice.fund_date.should.not.be.null;
            invoice.fund_date.should.not.be.null;
            invoice.save.calledOnce.should.be.true;
            stateMachine.next.calledOnce.should.be.true;
            stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
            Invoice.createInvestorTransaction.calledOnce.should.be.true;
            Invoice.createInvestorTransaction.calledWithMatch(invoice, user.Company).should.be.true;
            PendingTransaction.invoiceOperation.calledOnce.should.true;
            PendingTransaction.invoiceOperation.calledWithMatch(companyId, invoice, transaction).should.true;
            invoice.getBasicOperationInfo.calledOnce.should.be.true;
          });
          Order.newFundOrder.calledOnce.should.be.true;
          Order.newFundOrder.calledWithMatch(shoppingCartResponse.invoices, user.Company).should.be.true;
          userProducer.invoiceFundRequest.calledOnce.should.be.true;
          userProducer.invoiceFundRequest.calledWithMatch([ basicOperationInfo ], user.Company, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, successfulResponse, responseType, guid).should.be.true;
          transaction.rollback.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, successfulResponse).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('completed', () => {
    const fixtures = helperFixtures.completed;
    const {
      request, response, message, guid, requestType, responseType, commonError,
      invoice, invoiceGeneralInfo, transaction, producerParams, createInvoiceCompletedParams,
      sendengoStrategy
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(PendingTransaction, 'createInvoiceCompleted').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.resolve());
      sandbox.stub(invoice, 'getGeneralInfoAsAdmin').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(userProducer, 'invoiceCompleted').callsFake(() => Promise.resolve());
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'execCallback' ).callsFake(() => true );
    });

    it('should return an error if sequelize.transaction return a reject', () => {
      const callback = sandbox.spy();

      sequelize.transaction.restore();
      sandbox.stub(sequelize, 'transaction').callsFake(() => Promise.reject(commonError));

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.called.should.be.false;
          PendingTransaction.createInvoiceCompleted.called.should.be.false;
          stateMachine.next.called.should.be.false;
          invoice.getGeneralInfoAsAdmin.called.should.be.false;
          userProducer.invoiceCompleted.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledWith().should.be.true;
          PendingTransaction.createInvoiceCompleted.called.should.be.false;
          stateMachine.next.called.should.be.false;
          invoice.getGeneralInfoAsAdmin.called.should.be.false;
          userProducer.invoiceCompleted.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.createInvoiceCompleted return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.createInvoiceCompleted.restore();
      sandbox.stub(PendingTransaction, 'createInvoiceCompleted').callsFake(() => Promise.reject(commonError));

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledWith().should.be.true;
          PendingTransaction.createInvoiceCompleted.calledOnce.should.be.true;
          PendingTransaction.createInvoiceCompleted.args[0].should.be.eql(createInvoiceCompletedParams);
          stateMachine.next.called.should.be.false;
          invoice.getGeneralInfoAsAdmin.called.should.be.false;
          userProducer.invoiceCompleted.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.next return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.next.restore();
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.reject(commonError));

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledWith().should.be.true;
          PendingTransaction.createInvoiceCompleted.calledOnce.should.be.true;
          PendingTransaction.createInvoiceCompleted.args[0].should.be.eql(createInvoiceCompletedParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getGeneralInfoAsAdmin.called.should.be.false;
          userProducer.invoiceCompleted.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfoAsAdmin return a reject', () => {
      const callback = sandbox.spy();

      invoice.getGeneralInfoAsAdmin.restore();
      sandbox.stub(invoice, 'getGeneralInfoAsAdmin').callsFake(() => Promise.reject(commonError));

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledWith().should.be.true;
          PendingTransaction.createInvoiceCompleted.calledOnce.should.be.true;
          PendingTransaction.createInvoiceCompleted.args[0].should.be.eql(createInvoiceCompletedParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getGeneralInfoAsAdmin.calledOnce.should.be.true;
          invoice.getGeneralInfoAsAdmin.calledWith().should.be.true;
          userProducer.invoiceCompleted.calledOnce.should.be.true;
          userProducer.invoiceCompleted.args[0].should.be.eql(producerParams);

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceCompleted return a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceCompleted.restore();
      sandbox.stub(userProducer, 'invoiceCompleted').callsFake(() => Promise.reject(commonError));

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledWith().should.be.true;
          PendingTransaction.createInvoiceCompleted.calledOnce.should.be.true;
          PendingTransaction.createInvoiceCompleted.args[0].should.be.eql(createInvoiceCompletedParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getGeneralInfoAsAdmin.calledOnce.should.be.true;
          invoice.getGeneralInfoAsAdmin.calledWith().should.be.true;
          userProducer.invoiceCompleted.calledOnce.should.be.true;
          userProducer.invoiceCompleted.args[0].should.be.eql(producerParams);

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.completed(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledWith().should.be.true;
          PendingTransaction.createInvoiceCompleted.calledOnce.should.be.true;
          PendingTransaction.createInvoiceCompleted.args[0].should.be.eql(createInvoiceCompletedParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getGeneralInfoAsAdmin.calledOnce.should.be.true;
          invoice.getGeneralInfoAsAdmin.calledWith().should.be.true;
          userProducer.invoiceCompleted.calledOnce.should.be.true;
          userProducer.invoiceCompleted.args[0].should.be.eql(producerParams);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('lost', () => {
    const fixtures = helperFixtures.lost;
    const { request, response, message, guid, requestType, responseType, commonError, notFoundError,
      invoice, invoiceGeneralInfo, transaction, paymentTransaction, investorTransaction, findOneCall } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invoiceLost').callsFake(() => Promise.resolve());
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getAdminBasicInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(Transaction, 'createDeposit').callsFake(() => Promise.resolve(paymentTransaction));
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.called.should.be.false;
          stateMachine.back.called.should.be.false;
          invoice.getAdminBasicInfo.calledOnce.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLost.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.called.should.be.false;
          invoice.getAdminBasicInfo.calledOnce.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLost.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return an empty object', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.called.should.be.false;
          invoice.getAdminBasicInfo.calledOnce.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLost.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.back return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.back.restore();
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.reject(commonError));

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLost.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getAdminBasicInfo.restore();
      sandbox.stub(invoice, 'getAdminBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledTwice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(investorTransaction);
          userProducer.invoiceLost.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Transaction.create return a reject', () => {
      const callback = sandbox.spy();

      Transaction.createDeposit.restore();
      sandbox.stub(Transaction, 'createDeposit').callsFake(() => Promise.reject(commonError));

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledTwice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(investorTransaction);
          userProducer.invoiceLost.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceLost return a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceLost.restore();
      sandbox.stub(userProducer, 'invoiceLost').callsFake(() => Promise.reject(commonError));

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledTwice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(investorTransaction);
          userProducer.invoiceLost.calledOnce.should.be.true;
          userProducer.invoiceLost.calledWithMatch(invoice,
            paymentTransaction, paymentTransaction, guid).should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.lost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.calledWithMatch(null, invoice.Operation, null, transaction).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledTwice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(investorTransaction);
          userProducer.invoiceLost.calledOnce.should.be.true;
          userProducer.invoiceLost.calledWithMatch(invoice,
            paymentTransaction, paymentTransaction, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('latePayment', () => {
    const fixtures = helperFixtures.latePayment;
    const { request, response, message, guid, requestType, responseType, commonError, notFoundError,
      invoice, invoiceGeneralInfo, transaction, findOneCall, paymentTransaction, investorTransaction,
      operation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invoiceLatePayment').callsFake(() => Promise.resolve());
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice.Operation, 'save').callsFake(() => Promise.resolve(operation));
      sandbox.stub(invoice, 'getAdminBasicInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(Transaction, 'createDeposit').callsFake(() => Promise.resolve(paymentTransaction));
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.called.should.be.false;
          invoice.Operation.save.called.should.be.false;
          invoice.getAdminBasicInfo.called.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLatePayment.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          invoice.Operation.save.called.should.be.false;
          invoice.getAdminBasicInfo.called.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLatePayment.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return an empty object', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          invoice.Operation.save.called.should.be.false;
          invoice.getAdminBasicInfo.called.should.be.false;
          Transaction.createDeposit.called.should.be.false;
          userProducer.invoiceLatePayment.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getAdminBasicInfo.restore();
      sandbox.stub(invoice, 'getAdminBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          invoice.Operation.save.calledOnce.should.be.true;
          invoice.Operation.save.calledWithMatch({ transaction }).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledThrice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[2].should.be.eql(investorTransaction);
          userProducer.invoiceLatePayment.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Transaction.create return a reject', () => {
      const callback = sandbox.spy();

      Transaction.createDeposit.restore();
      sandbox.stub(Transaction, 'createDeposit').callsFake(() => Promise.reject(commonError));

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          invoice.Operation.save.calledOnce.should.be.true;
          invoice.Operation.save.calledWithMatch({ transaction }).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledThrice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[2].should.be.eql(investorTransaction);
          userProducer.invoiceLatePayment.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.invoiceLost return a reject', () => {
      const callback = sandbox.spy();

      userProducer.invoiceLatePayment.restore();
      sandbox.stub(userProducer, 'invoiceLatePayment').callsFake(() => Promise.reject(commonError));

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.called.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          invoice.Operation.save.calledOnce.should.be.true;
          invoice.Operation.save.calledWithMatch({ transaction }).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledThrice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[2].should.be.eql(investorTransaction);
          userProducer.invoiceLatePayment.calledOnce.should.be.true;
          userProducer
            .invoiceLatePayment.calledWithMatch(invoice, paymentTransaction, paymentTransaction, guid).should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.latePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(findOneCall).should.be.true;
          invoice.Operation.save.calledOnce.should.be.true;
          invoice.Operation.save.calledWithMatch({ transaction }).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          Transaction.createDeposit.calledThrice.should.be.true;
          Transaction.createDeposit.args[0].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[1].should.be.eql(paymentTransaction);
          Transaction.createDeposit.args[2].should.be.eql(investorTransaction);
          userProducer.invoiceLatePayment.calledOnce.should.be.true;
          userProducer.invoiceLatePayment
            .calledWithMatch(invoice, paymentTransaction, paymentTransaction, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('rejectPublished', () => {
    const fixtures = helperFixtures.rejectPublished;
    const { request, response, message, guid, requestType, responseType, commonError, cxcRequest,
      invoice, invoiceGeneralInfo, company, reason, invoiceCxcQuery, invoiceQuery, reasonError,
      notFoundError, instances, destroyQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.resolve(instances));
      sandbox.stub(instances[0], 'getCompany').callsFake(() => Promise.resolve(company));
      sandbox.stub(instances[0], 'getGeneralInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(Operation, 'destroy').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'publishedInvoiceRejected').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if a reason is not given', () => {
      const callback = sandbox.spy();
      const cloneRequest = _.cloneDeep(request);

      delete cloneRequest.request.reason;

      return controller.rejectPublished(cloneRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, cloneRequest.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(reasonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject(Cxc)', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(cxcRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceCxcQuery);
          stateMachine.back.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject(Admin)', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return an undefined value', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.back return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.back.restore();
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          expect(invoice.published).to.be.null;
          Operation.destroy.calledOnce.should.be.false;
          instances[0].getCompany.called.should.be.false;
          instances[0].getGeneralInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Operation.destroy fails', () => {
      const callback = sandbox.spy();

      Operation.destroy.restore();
      sandbox.stub(Operation, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          expect(invoice.published).to.be.null;
          Operation.destroy.calledOnce.should.be.true;
          Operation.destroy.args[0].should.be.eql(destroyQuery);
          instances[0].getCompany.called.should.be.false;
          instances[0].getGeneralInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getCompany return a reject', () => {
      const callback = sandbox.spy();

      instances[0].getCompany.restore();
      sandbox.stub(instances[0], 'getCompany').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          expect(invoice.published).to.be.null;
          Operation.destroy.calledOnce.should.be.true;
          Operation.destroy.args[0].should.be.eql(destroyQuery);
          instances[0].getCompany.called.should.be.true;
          instances[0].getGeneralInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfo return a reject', () => {
      const callback = sandbox.spy();

      instances[0].getGeneralInfo.restore();
      sandbox.stub(instances[0], 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          expect(invoice.published).to.be.null;
          Operation.destroy.calledOnce.should.be.true;
          Operation.destroy.args[0].should.be.eql(destroyQuery);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.calledOnce.should.be.true;
          instances[0].getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.publishedInvoiceRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.publishedInvoiceRejected return a reject', () => {
      const callback = sandbox.spy();

      userProducer.publishedInvoiceRejected.restore();
      sandbox.stub(userProducer, 'publishedInvoiceRejected').callsFake(() => Promise.reject(commonError));

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          expect(invoice.published).to.be.null;
          Operation.destroy.calledOnce.should.be.true;
          Operation.destroy.args[0].should.be.eql(destroyQuery);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.calledOnce.should.be.true;
          instances[0].getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.publishedInvoiceRejected.calledOnce.should.be.true;
          userProducer.publishedInvoiceRejected.args[0].should.be.eql([ instances[0], reason, guid ]);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.rejectPublished(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ invoice ]);
          expect(invoice.published).to.be.null;
          Operation.destroy.calledOnce.should.be.true;
          Operation.destroy.args[0].should.be.eql(destroyQuery);
          instances[0].getCompany.calledOnce.should.be.true;
          instances[0].getGeneralInfo.calledOnce.should.be.true;
          instances[0].getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.publishedInvoiceRejected.calledOnce.should.be.true;
          userProducer.publishedInvoiceRejected.args[0].should.be.eql([ instances[0], reason, guid ]);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('rejectFunded', () => {
    const fixtures = helperFixtures.rejectFunded;
    const { request, response, message, guid, requestType, responseType, commonError,
      invoiceToClone, invoiceGeneralInfo, company, reason, pendingTransactionParams, reasonError,
      transaction, investorCompanyId } = fixtures;
    let invoice;

    beforeEach(() => {
      invoice = _.cloneDeep(invoiceToClone);

      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(PendingTransaction, 'rejectInvoiceFund').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(Invoice, 'removeFundRequest').callsFake(() => Promise.resolve());
      sandbox.stub(invoice, 'getCompany').callsFake(() => Promise.resolve(company));
      sandbox.stub(invoice, 'getGeneralInfo').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'fundRequestedInvoiceRejected').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if a reason is not given', () => {
      const callback = sandbox.spy();
      const cloneRequest = _.cloneDeep(request);

      delete cloneRequest.request.reason;

      return controller.rejectFunded(cloneRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, cloneRequest.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(reasonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.rejectInvoiceFund return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.rejectInvoiceFund.restore();
      sandbox.stub(PendingTransaction, 'rejectInvoiceFund').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.back return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.back.restore();
      sandbox.stub(stateMachine, 'back').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ null, invoice.Operation, invoice.Operation.InvestorTransaction,
            transaction ]);
          Invoice.removeFundRequest.calledOnce.should.be.false;
          invoice.getCompany.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.removeFundRequest returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.removeFundRequest.restore();
      sandbox.stub(Invoice, 'removeFundRequest').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ null, invoice.Operation, invoice.Operation.InvestorTransaction,
            transaction ]);
          Invoice.removeFundRequest.calledOnce.should.be.true;
          Invoice.removeFundRequest.calledWithMatch(invoice, transaction);
          invoice.getGeneralInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getCompany return a reject', () => {
      const callback = sandbox.spy();

      invoice.getCompany.restore();
      sandbox.stub(invoice, 'getCompany').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ null, invoice.Operation, invoice.Operation.InvestorTransaction,
            transaction ]);
          Invoice.removeFundRequest.calledOnce.should.be.true;
          Invoice.removeFundRequest.calledWithMatch(invoice, transaction);
          invoice.getCompany.calledOnce.should.be.true;
          invoice.getGeneralInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getGeneralInfo.restore();
      sandbox.stub(invoice, 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ null, invoice.Operation, invoice.Operation.InvestorTransaction,
            transaction ]);
          Invoice.removeFundRequest.calledOnce.should.be.true;
          Invoice.removeFundRequest.calledWithMatch(invoice, transaction);
          invoice.getCompany.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.fundRequestedInvoiceRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.fundRequestedInvoiceRejected return a reject', () => {
      const callback = sandbox.spy();

      userProducer.fundRequestedInvoiceRejected.restore();
      sandbox.stub(userProducer, 'fundRequestedInvoiceRejected').callsFake(() => Promise.reject(commonError));

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ null, invoice.Operation, invoice.Operation.InvestorTransaction,
            transaction ]);
          Invoice.removeFundRequest.calledOnce.should.be.true;
          Invoice.removeFundRequest.calledWithMatch(invoice, transaction);
          invoice.getCompany.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.fundRequestedInvoiceRejected.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceRejected.args[0].should.be.eql([ invoice, reason, investorCompanyId, guid ]);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.rejectFunded(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.rejectInvoiceFund.args[0].should.be.eql(pendingTransactionParams);
          stateMachine.back.calledOnce.should.be.true;
          stateMachine.back.args[0].should.be.eql([ null, invoice.Operation, invoice.Operation.InvestorTransaction,
            transaction ]);
          Invoice.removeFundRequest.calledOnce.should.be.true;
          Invoice.removeFundRequest.calledWithMatch(invoice, transaction);
          invoice.getCompany.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.args[0].should.be.eql([ company, 'company' ]);
          userProducer.fundRequestedInvoiceRejected.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceRejected.args[0].should.be.eql([ invoice, reason, investorCompanyId, guid ]);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('approveFund', () => {
    const fixtures = helperFixtures.approveFund;
    const { request, response, message, guid, requestType, responseType, commonError,
      invoice, invoiceGeneralInfo, transaction, transactionArguments, payment } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(PendingTransaction, 'createInvoiceFund').callsFake(() => Promise.resolve([ invoice, payment ]));
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.resolve());
      sandbox.stub(invoice, 'getGeneralInfoAsAdmin').callsFake(() => Promise.resolve(invoiceGeneralInfo));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'fundRequestedInvoiceApproved').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'notifyFundApproved').callsFake(() => Promise.resolve());
    });

    it('should return an error if sequelize.transaction return a reject', () => {
      const callback = sandbox.spy();

      sequelize.transaction.restore();
      sandbox.stub(sequelize, 'transaction').callsFake(() => Promise.reject(commonError));

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.createInvoiceFund return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.createInvoiceFund.restore();
      sandbox.stub(PendingTransaction, 'createInvoiceFund').callsFake(() => Promise.reject(commonError));

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.args[0].should.be.eql(transactionArguments);
          stateMachine.next.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.next return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.next.restore();
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.reject(commonError));

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.args[0].should.be.eql(transactionArguments);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation,
            invoice.Operation.InvestorTransaction, transaction ]);
          invoice.getGeneralInfoAsAdmin.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfoAsAdmin return a reject', () => {
      const callback = sandbox.spy();

      invoice.getGeneralInfoAsAdmin.restore();
      sandbox.stub(invoice, 'getGeneralInfoAsAdmin').callsFake(() => Promise.reject(commonError));

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.args[0].should.be.eql(transactionArguments);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation,
            invoice.Operation.InvestorTransaction, transaction ]);
          invoice.getGeneralInfoAsAdmin.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.fundRequestedInvoiceApproved return a reject', () => {
      const callback = sandbox.spy();

      userProducer.fundRequestedInvoiceApproved.restore();
      sandbox.stub(userProducer, 'fundRequestedInvoiceApproved').callsFake(() => Promise.reject(commonError));

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.args[0].should.be.eql(transactionArguments);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation,
            invoice.Operation.InvestorTransaction, transaction ]);
          invoice.getGeneralInfoAsAdmin.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceApproved.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceApproved.args[0].should.be.eql([ invoice, payment, guid ]);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a success if there was no issues creating the transactions', () => {
      const callback = sandbox.spy();

      return controller.approveFund(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.createInvoiceFund.args[0].should.be.eql(transactionArguments);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation,
            invoice.Operation.InvestorTransaction, transaction ]);
          invoice.getGeneralInfoAsAdmin.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceApproved.calledOnce.should.be.true;
          userProducer.fundRequestedInvoiceApproved.args[0].should.be.eql([ invoice, payment, guid ]);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getFundEstimate', () => {
    const fixtures = helperFixtures.getFundEstimate;
    const { request, response, message, guid, requestType, responseType, commonError,
      invoice, estimate, user_id, invoice_id, sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'validateCxc').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getFundEstimate').callsFake(() => Promise.resolve(estimate));
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should return an error if Invoice.validateCxc return a reject', () => {
      const callback = sandbox.spy();

      Invoice.validateCxc.restore();
      sandbox.stub(Invoice, 'validateCxc').callsFake(() => Promise.reject(commonError));

      return controller.getFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.validateCxc.calledOnce.should.be.true;
          Invoice.validateCxc.args[0].should.be.eql([ user_id, invoice_id ]);
          invoice.getFundEstimate.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoi.getFundEstimate return a reject', () => {
      const callback = sandbox.spy();

      invoice.getFundEstimate.restore();
      sandbox.stub(invoice, 'getFundEstimate').callsFake(() => Promise.reject(commonError));

      return controller.getFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.validateCxc.calledOnce.should.be.true;
          Invoice.validateCxc.args[0].should.be.eql([ user_id, invoice_id ]);
          invoice.getFundEstimate.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.validateCxc.calledOnce.should.be.true;
          Invoice.validateCxc.args[0].should.be.eql([ user_id, invoice_id ]);
          invoice.getFundEstimate.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getXml', () => {
    const fixtures = helperFixtures.getXml;
    const { request, response, message, guid, requestType, responseType,
      commonError, userInformation, invoice, whereUser, whereInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInformation));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getXml(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getXml(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getXml(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getMarketplace', () => {
    const fixtures = helperFixtures.getMarketplace;
    const { request, response, message, guid, requestType, responseType, invoices,
      commonError, where, requestTotal, whereTotal, requestStartEndDate, whereStartEndDate,
      whereMinTotal, requestMinTotal, requestStartDate, whereStartDate, maxTotal, count,
      countQuery, maxQuery, responseWithZero } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'getMarketplace').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.resolve(count));
      sandbox.stub(Invoice, 'max').callsFake(() => Promise.resolve(maxTotal));
    });

    it('should return an error if Invoice.getMarketplace return a reject', () => {
      const callback = sandbox.spy();

      Invoice.getMarketplace.restore();
      sandbox.stub(Invoice, 'getMarketplace').callsFake(() => Promise.reject(commonError));

      return controller.getMarketplace(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(where).should.be.true;
          Invoice.count.called.should.be.false;
          Invoice.max.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.count return a reject', () => {
      const callback = sandbox.spy();

      Invoice.count.restore();
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.reject(commonError));

      return controller.getMarketplace(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(where).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', maxQuery).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.max return a reject', () => {
      const callback = sandbox.spy();

      Invoice.max.restore();
      sandbox.stub(Invoice, 'max').callsFake(() => Promise.reject(commonError));

      return controller.getMarketplace(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(where).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', maxQuery).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an 0 if Invoice.max return a null value', () => {
      const callback = sandbox.spy();

      Invoice.max.restore();
      sandbox.stub(Invoice, 'max').callsFake(() => Promise.resolve(null));

      return controller.getMarketplace(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(where).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', countQuery).should.be.true;
          callback.firstCall.args[1].max_total.should.be.eql('0');
          log.message.secondCall.calledWithMatch(message, responseWithZero, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, responseWithZero).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should add min_total and max_total to the where', () => {
      const callback = sandbox.spy();

      return controller.getMarketplace(requestTotal, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(whereTotal).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', maxQuery).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should not to add min_total to the where', () => {
      const callback = sandbox.spy();

      return controller.getMarketplace(requestMinTotal, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(whereMinTotal).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', maxQuery).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should add start_date and end_date to the where', () => {
      const callback = sandbox.spy();

      return controller.getMarketplace(requestStartEndDate, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(whereStartEndDate).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', maxQuery).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should add start_date to the query', () => {
      const callback = sandbox.spy();

      return controller.getMarketplace(requestStartDate, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(whereStartDate).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', maxQuery).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getMarketplace(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.getMarketplace.calledOnce.should.be.true;
          Invoice.getMarketplace.calledWithMatch(where).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(countQuery).should.be.true;
          Invoice.max.calledOnce.should.be.true;
          Invoice.max.calledWithMatch('total', countQuery).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvestorFundEstimate', () => {
    const fixtures = helperFixtures.getInvestorFundEstimate;
    const { request, response, message, guid, requestType, responseType,
      commonError, invoice, investorFundEstimate, whereInvoice, user, userWhere, fundRequestedInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(invoice, 'getInvestorFundEstimate').callsFake(() => Promise.resolve(investorFundEstimate));
      sandbox.stub(fundRequestedInvoice, 'getInvestorFundEstimateFromTransaction').callsFake(() =>
        Promise.resolve(investorFundEstimate));
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userWhere).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundEstimate.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userWhere).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundEstimate.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getInvestorFundEstimate.restore();
      sandbox.stub(invoice, 'getInvestorFundEstimate').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userWhere).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundEstimate.calledOnce.should.be.true;
          invoice.getInvestorFundEstimate.calledWithMatch(user.Company).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response when its a published invoice', () => {
      const callback = sandbox.spy();

      return controller.getInvestorFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userWhere).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundEstimate.calledOnce.should.be.true;
          invoice.getInvestorFundEstimate.calledWithMatch(user.Company).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response if an invoice has an investor transaction', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(fundRequestedInvoice));

      return controller.getInvestorFundEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userWhere).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          fundRequestedInvoice.getInvestorFundEstimateFromTransaction.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvestorProfitEstimate', () => {
    const fixtures = helperFixtures.getInvestorProfitEstimate;
    const { request, response, message, guid, requestType, responseType,
      commonError, invoice, investorFundEstimate, whereInvoice, fundRequestedInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getInvestorProfitEstimate').callsFake(() => Promise.resolve(investorFundEstimate));
      sandbox.stub(fundRequestedInvoice, 'getInvestorProfitEstimateFromTransaction').callsFake(() =>
        Promise.resolve(investorFundEstimate));
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorProfitEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorProfitEstimate.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getInvestorProfitEstimate.restore();
      sandbox.stub(invoice, 'getInvestorProfitEstimate').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorProfitEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorProfitEstimate.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response when its a published invoice', () => {
      const callback = sandbox.spy();

      return controller.getInvestorProfitEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorProfitEstimate.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response if the invoice has an investor transaction', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(fundRequestedInvoice));

      return controller.getInvestorProfitEstimate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          fundRequestedInvoice.getInvestorProfitEstimateFromTransaction.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvestorInvoices', () => {
    const fixtures = helperFixtures.getInvestorInvoices;
    const { request, response, message, guid, requestType, responseType, invoices,
      commonError, investor, whereUser, whereInvestor } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'getInvoices').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.resolve(4));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(investor));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.called.should.be.false;
          Invoice.count.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.getInvoices return a reject', () => {
      const callback = sandbox.spy();

      Invoice.getInvoices.restore();
      sandbox.stub(Invoice, 'getInvoices').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(investor, whereInvestor, request.request).should.be.true;
          Invoice.count.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.count return a reject', () => {
      const callback = sandbox.spy();

      Invoice.count.restore();
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(investor, whereInvestor, request.request).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvestorInvoices(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(whereUser).should.be.true;
          Invoice.getInvoices.calledOnce.should.be.true;
          Invoice.getInvoices.calledWithMatch(investor, whereInvestor, request.request).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvestorFundDetail', () => {
    const fixtures = helperFixtures.getInvestorFundDetail;
    const { request, response, message, guid, requestType, responseType,
      commonError, invoice, operationTerm, whereInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getInvestorFundDetail').callsFake(() => Promise.resolve(operationTerm));
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorFundDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundDetail.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getInvestorFundDetail.restore();
      sandbox.stub(invoice, 'getInvestorFundDetail').callsFake(() => Promise.reject(commonError));

      return controller.getInvestorFundDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundDetail.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvestorFundDetail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundDetail.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvoicePaymentSummary', () => {
    const fixtures = helperFixtures.getInvoicePaymentSummary;
    const { request, response, message, guid, requestType, responseType,
      commonError, invoice, operationTerm, investorFundEstimate, paymentSummary, whereInvoice,
      sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getInvoicePaymentSummary').callsFake(() => Promise.resolve(paymentSummary));
      sandbox.stub(invoice, 'getInvestorFundDetail').callsFake(() => Promise.resolve(operationTerm));
      sandbox.stub(invoice, 'getInvestorProfitEstimateFromTransaction')
        .callsFake(() => Promise.resolve(investorFundEstimate));
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvoicePaymentSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvestorFundDetail.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getInvoicePaymentSummary return a reject', () => {
      const callback = sandbox.spy();

      invoice.getInvoicePaymentSummary.restore();
      sandbox.stub(invoice, 'getInvoicePaymentSummary').callsFake(() => Promise.reject(commonError));

      return controller.getInvoicePaymentSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvoicePaymentSummary.calledOnce.should.be.true;
          invoice.getInvestorFundDetail.calledOnce.should.be.true;
          invoice.getInvestorProfitEstimateFromTransaction.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getInvestorFundDetail return a reject', () => {
      const callback = sandbox.spy();

      invoice.getInvestorFundDetail.restore();
      sandbox.stub(invoice, 'getInvestorFundDetail').callsFake(() => Promise.reject(commonError));

      return controller.getInvoicePaymentSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvoicePaymentSummary.calledOnce.should.be.true;
          invoice.getInvestorFundDetail.calledOnce.should.be.true;
          invoice.getInvestorProfitEstimateFromTransaction.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getInvestorProfitEstimateFromTransaction return a reject', () => {
      const callback = sandbox.spy();

      invoice.getInvestorProfitEstimateFromTransaction.restore();
      sandbox.stub(invoice, 'getInvestorProfitEstimateFromTransaction').callsFake(() => Promise.reject(commonError));

      return controller.getInvoicePaymentSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvoicePaymentSummary.calledOnce.should.be.true;
          invoice.getInvestorFundDetail.calledOnce.should.be.true;
          invoice.getInvestorProfitEstimateFromTransaction.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvoicePaymentSummary(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getInvoicePaymentSummary.calledOnce.should.be.true;
          invoice.getInvestorFundDetail.calledOnce.should.be.true;
          invoice.getInvestorProfitEstimateFromTransaction.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getAdminInvoices', () => {
    const fixtures = helperFixtures.getAdminInvoices;
    const {
      allParamsRequest, noPaginationParamsRequest, emptyStatusRequest, response,
      responseDefaultPagination, message, guid, requestType, responseType, adminInvoices,
      commonError, allParams, noPaginationParams, nullStatusParams
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'getAdminInvoices').callsFake(() => Promise.resolve(adminInvoices));
    });

    it('should return an error if Invoice.getAdminInvoices return a reject', () => {
      const callback = sandbox.spy();

      Invoice.getAdminInvoices.restore();
      sandbox.stub(Invoice, 'getAdminInvoices').callsFake(() => Promise.reject(commonError));

      return controller.getAdminInvoices(allParamsRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, allParamsRequest.request, requestType, guid).should.be.true;
          Invoice.getAdminInvoices.calledOnce.should.be.true;
          Invoice.getAdminInvoices.calledWithMatch(allParams).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return add default pagination parameters', () => {
      const callback = sandbox.spy();

      return controller.getAdminInvoices(noPaginationParamsRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall
            .calledWithMatch(message, noPaginationParamsRequest.request, requestType, guid).should.be.true;
          Invoice.getAdminInvoices.calledOnce.should.be.true;
          Invoice.getAdminInvoices.calledWithMatch(noPaginationParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, responseDefaultPagination, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, responseDefaultPagination).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should set status to null if provided an empty status array', () => {
      const callback = sandbox.spy();

      return controller.getAdminInvoices(emptyStatusRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall
            .calledWithMatch(message, emptyStatusRequest.request, requestType, guid).should.be.true;
          Invoice.getAdminInvoices.calledOnce.should.be.true;
          Invoice.getAdminInvoices.calledWithMatch(nullStatusParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getAdminInvoices(allParamsRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, allParamsRequest.request, requestType, guid).should.be.true;
          Invoice.getAdminInvoices.calledOnce.should.be.true;
          Invoice.getAdminInvoices.calledWithMatch(allParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvoiceDetailAsAdmin', () => {
    const fixtures = helperFixtures.getInvoiceDetailAsAdmin;
    const { request, response, incompleteResponse, message, guid, requestType, responseType, commonError, invoice,
      adminInvoiceDetail, whereInvoice } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getAdminBasicInfo').callsFake(() => Promise.resolve(adminInvoiceDetail.invoiceDetail));
      sandbox.stub(invoice, 'getAdminOperationSummary').callsFake(
        () => Promise.resolve(adminInvoiceDetail.operationSummary)
      );
      sandbox.stub(invoice, 'getAdminCxcPayment').callsFake(() => Promise.resolve(adminInvoiceDetail.cxcPayment));
      sandbox.stub(invoice, 'getAdminInvestorPayment').callsFake(
        () => Promise.resolve(adminInvoiceDetail.investorPayment)
      );
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getAdminBasicInfo.called.should.be.false;
          invoice.getAdminOperationSummary.called.should.be.false;
          invoice.getAdminCxcPayment.called.should.be.false;
          invoice.getAdminInvestorPayment.called.should.be.false;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should only call getAdminBasicInfo if expiration parameter is not present', () => {
      const callback = sandbox.spy();
      const invoiceWithoutExpiration = _.cloneDeep(invoice);

      delete invoiceWithoutExpiration.expiration;

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoiceWithoutExpiration));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoiceWithoutExpiration.getAdminBasicInfo.calledOnce.should.be.true;
          invoiceWithoutExpiration.getAdminOperationSummary.called.should.be.false;
          invoiceWithoutExpiration.getAdminCxcPayment.called.should.be.false;
          invoiceWithoutExpiration.getAdminInvestorPayment.called.should.be.false;

          log.message.secondCall.calledWithMatch(message, incompleteResponse, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, incompleteResponse).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should only call getAdminBasicInfo if fund_date parameters is not present', () => {
      const callback = sandbox.spy();
      const invoiceWithoutFundDate = _.cloneDeep(invoice);

      delete invoiceWithoutFundDate.fund_date;

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoiceWithoutFundDate));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoiceWithoutFundDate.getAdminBasicInfo.calledOnce.should.be.true;
          invoiceWithoutFundDate.getAdminOperationSummary.called.should.be.false;
          invoiceWithoutFundDate.getAdminCxcPayment.called.should.be.false;
          invoiceWithoutFundDate.getAdminInvestorPayment.called.should.be.false;

          log.message.secondCall.calledWithMatch(message, incompleteResponse, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, incompleteResponse).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an error if invoice.getAdminBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getAdminBasicInfo.restore();
      sandbox.stub(invoice, 'getAdminBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          invoice.getAdminOperationSummary.calledOnce.should.be.true;
          invoice.getAdminCxcPayment.calledOnce.should.be.true;
          invoice.getAdminInvestorPayment.calledOnce.should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminOperationSummary return a reject', () => {
      const callback = sandbox.spy();

      invoice.getAdminOperationSummary.restore();
      sandbox.stub(invoice, 'getAdminOperationSummary').callsFake(() => Promise.reject(commonError));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          invoice.getAdminOperationSummary.calledOnce.should.be.true;
          invoice.getAdminCxcPayment.calledOnce.should.be.true;
          invoice.getAdminInvestorPayment.calledOnce.should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminCxcPayment return a reject', () => {
      const callback = sandbox.spy();

      invoice.getAdminCxcPayment.restore();
      sandbox.stub(invoice, 'getAdminCxcPayment').callsFake(() => Promise.reject(commonError));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          invoice.getAdminOperationSummary.calledOnce.should.be.true;
          invoice.getAdminCxcPayment.calledOnce.should.be.true;
          invoice.getAdminInvestorPayment.calledOnce.should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminInvestorPayment return a reject', () => {
      const callback = sandbox.spy();

      invoice.getAdminInvestorPayment.restore();
      sandbox.stub(invoice, 'getAdminInvestorPayment').callsFake(() => Promise.reject(commonError));

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          invoice.getAdminOperationSummary.calledOnce.should.be.true;
          invoice.getAdminCxcPayment.calledOnce.should.be.true;
          invoice.getAdminInvestorPayment.calledOnce.should.be.true;

          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvoiceDetailAsAdmin(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(whereInvoice).should.be.true;
          invoice.getAdminBasicInfo.calledOnce.should.be.true;
          invoice.getAdminOperationSummary.calledOnce.should.be.true;
          invoice.getAdminCxcPayment.calledOnce.should.be.true;
          invoice.getAdminInvestorPayment.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvoicesAvailability', () => {
    const fixtures = helperFixtures.getInvoicesAvailability;
    const { request, response, message, guid, requestType, responseType, invoices,
      commonError, query } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.getInvoicesAvailability(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(query).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvoicesAvailability(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(query).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('bulkApprove', () => {
    const fixtures = helperFixtures.bulkApprove;
    const { request, response, message, guid, requestType, responseType,
      commonError, queryParams, pendingTransactions, buildQueryParams,
      invoice, invoiceFindOneQuery, investorPayment,
      cxcPayment, callbackDataOne, callbackDataTwo, arrayCallbacks
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(PendingTransaction, 'buildBankReportQuery').callsFake(() => queryParams);
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve(pendingTransactions));

      let _pendingTransaction = pendingTransactions[0];

      sandbox.stub(_pendingTransaction, 'isForInvestorInvoiceFund').callsFake(() => true);
      _pendingTransaction = pendingTransactions[1];
      sandbox.stub(_pendingTransaction, 'isForInvestorInvoiceFund').callsFake(() => false);

      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(invoice, 'getAdminCxcPayment').resolves(cxcPayment);
      sandbox.stub(invoice, 'getAdminInvestorPayment').resolves(investorPayment);

      const stub = sandbox.stub(controller, '_buildCallbackData');

      stub.onCall(0).callsFake(() => callbackDataOne);
      stub.onCall(1).callsFake(() => callbackDataTwo);

      sandbox.stub(callbackDataOne, 'callback').resolves(true);
      sandbox.stub(callbackDataTwo, 'callback').resolves(true);
      sandbox.stub(controller, '_processCallbackResults').callsFake(() => response);
    });

    it('should return an error if PendingTransaction.findAll returns a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.buildBankReportQuery.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledWithMatch(buildQueryParams).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWithMatch(queryParams).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.findAll returns a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.buildBankReportQuery.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledWithMatch(buildQueryParams).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWithMatch(queryParams).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne rejects', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').rejects(commonError);

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          pendingTransactions[1].isForInvestorInvoiceFund.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneQuery).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminCxcPayment rejects', () => {
      const callback = sandbox.spy();

      invoice.getAdminCxcPayment.restore();
      sandbox.stub(invoice, 'getAdminCxcPayment').rejects(commonError);

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          pendingTransactions[1].isForInvestorInvoiceFund.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneQuery).should.be.true;
          invoice.getAdminCxcPayment.called.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getAdminInvestorPayment rejects', () => {
      const callback = sandbox.spy();

      invoice.getAdminInvestorPayment.restore();
      sandbox.stub(invoice, 'getAdminInvestorPayment').rejects(commonError);

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          pendingTransactions[1].isForInvestorInvoiceFund.called.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneQuery).should.be.true;
          invoice.getAdminInvestorPayment.called.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          controller._buildCallbackData.calledTwice.should.be.true;
          controller._processCallbackResults.called.should.be.true;
          controller._processCallbackResults.calledWith(arrayCallbacks).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });


    it('should return an successful response if any callback from the callbacks rejects', () => {
      const callback = sandbox.spy();

      callbackDataTwo.callback.restore();
      sandbox.stub(callbackDataTwo, 'callback').rejects(false);

      return controller.bulkApprove(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          controller._buildCallbackData.calledTwice.should.be.true;
          controller._processCallbackResults.called.should.be.true;
          controller._processCallbackResults.calledWith(arrayCallbacks).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('_buildCallbackData', () => {
    const fixtures = helperFixtures._buildCallbackData;
    const { request, pendingTransaction, pendingTransactionOut } = fixtures;

    it('should work when controller parameter resolves', () => {
      const method  = sinon.stub();

      method.resolves(true);

      const output = controller._buildCallbackData(method, request, pendingTransaction);

      output.callback()
        .should.be.fulfilled
        .then( () => {
          method.called.should.be.true;
          output.processed.should.be.true;
          expect(output.pending_transaction).to.deep.equal(pendingTransactionOut);
        });
    });

    it('should work when controller parameter rejects', () => {
      const method  = sinon.stub();

      method.rejects(false);

      const output = controller._buildCallbackData(method, request, pendingTransaction);

      return output.callback()
        .should.be.fulfilled
        .then( () => {
          method.called.should.be.true;
          output.processed.should.be.false;
          expect(output.pending_transaction).to.deep.equal(pendingTransactionOut);
        });
    });
  });


  describe('_processCallbackResults', () => {
    const fixtures = helperFixtures._processCallbackResults;
    const { callbacks, response } = fixtures;

    it('should work correctly', () => {
      const output = controller._processCallbackResults(callbacks);

      return Promise.resolve()
        .should.be.fulfilled
        .then(() => {
          output.should.be.deep.equal(response);
        });
    });
  });
});
