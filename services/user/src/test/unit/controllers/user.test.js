const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/user');
const errorHandler = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/controllers/user');
const _ = require('lodash');

const { Token, User, Company, Invoice, BankReport, PendingTransaction } = require('../../../models');
const userProducer = require('../../../api/producers/user');

chai.should();
const expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('unit/User controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('getInvitation', () => {
    const fixtures = helperFixtures.getInvitation;
    const { request, response, token, findOneQuery, logRequestMessageParams,
      logResponseMessageParams, commonError, notFoundError, invitationInfo } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(token));
      sandbox.stub(token, 'getInvitationInfo').callsFake(() => Promise.resolve(invitationInfo));
    });

    it('should return an error if Token.findOne fails', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneQuery).should.be.true;
          token.getInvitationInfo.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if the token doesn\'t exists', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());

      return controller.getInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneQuery).should.be.true;
          token.getInvitationInfo.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if token.getInvitationInfo fails', () => {
      const callback = sandbox.spy();

      token.getInvitationInfo.restore();
      sandbox.stub(token, 'getInvitationInfo').callsFake(() => Promise.reject(commonError));

      return controller.getInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneQuery).should.be.true;
          token.getInvitationInfo.calledOnce.should.be.true;
          token.getInvitationInfo.calledWith().should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.getInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneQuery).should.be.true;
          token.getInvitationInfo.calledOnce.should.be.true;
          token.getInvitationInfo.calledWith().should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('registration', () => {
    const fixtures = helperFixtures.registration;
    const { request, response, token, user, logRequestMessageParams, logResponseMessageParams,
      findOneCall, createCall, commonError, notFoundError, guid, userInformation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(token));
      sandbox.stub(User, 'create').callsFake(() => Promise.resolve(user));
      sandbox.stub(Token, 'destroy').callsFake(() => Promise.resolve());
      sandbox.stub(user, 'getInformation').callsFake(() => Promise.resolve(userInformation));
    });

    it('should return an error if Token.findOne fails', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(notFoundError));

      return controller.registration(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneCall).should.be.true;
          User.create.called.should.be.false;
          Token.destroy.called.should.be.false;
          user.getInformation.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if token does not exists', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());

      return controller.registration(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneCall).should.be.true;
          User.create.called.should.be.false;
          Token.destroy.called.should.be.false;
          user.getInformation.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if User.create fails', () => {
      const callback = sandbox.spy();

      User.create.restore();
      sandbox.stub(User, 'create').callsFake(() => Promise.reject(commonError));

      return controller.registration(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneCall).should.be.true;
          User.create.calledOnce.should.be.true;
          User.create.calledWithMatch(createCall).should.be.true;
          Token.destroy.called.should.be.false;
          user.getInformation.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Token.destroy fails', () => {
      const callback = sandbox.spy();

      Token.destroy.restore();
      sandbox.stub(Token, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.registration(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneCall).should.be.true;
          User.create.calledOnce.should.be.true;
          User.create.calledWithMatch(createCall).should.be.true;
          Token.destroy.calledOnce.should.be.true;
          Token.destroy.calledWithMatch(findOneCall).should.be.true;
          user.getInformation.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if user.getInformation fails', () => {
      const callback = sandbox.spy();

      user.getInformation.restore();
      sandbox.stub(user, 'getInformation').callsFake(() => Promise.reject(commonError));

      return controller.registration(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneCall).should.be.true;
          User.create.calledOnce.should.be.true;
          User.create.calledWithMatch(createCall).should.be.true;
          Token.destroy.calledOnce.should.be.true;
          Token.destroy.calledWithMatch(findOneCall).should.be.true;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.registration(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findOneCall).should.be.true;
          User.create.calledOnce.should.be.true;
          User.create.calledWithMatch(createCall).should.be.true;
          Token.destroy.calledOnce.should.be.true;
          Token.destroy.calledWithMatch(findOneCall).should.be.true;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getRoles', () => {
    const fixtures = helperFixtures.getRoles;
    const { request, response, logRequestMessageParams, logResponseMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.getRoles(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('delete', () => {
    const fixtures = helperFixtures.delete;
    const { request, token, user, logRequestMessageParams, userFindOneCall,
      tokenFindOneCall, commonError, notFoundError, guid, userInformation,
      invitationInfo, logUserResponseParams, logTokenResponseParams, getUsersCall, unableError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(token));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(Company, 'getUsers').callsFake(() => Promise.resolve([ {}, {} ]));
      sandbox.stub(user, 'getInformation').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(user, 'destroy').callsFake(() => Promise.resolve());
      sandbox.stub(token, 'getInvitationInfo').callsFake(() => Promise.resolve(invitationInfo));
      sandbox.stub(token, 'destroy').callsFake(() => Promise.resolve());
    });

    it('should return an error if User.findOne fails', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.called.should.be.false;
          user.getInformation.called.should.be.false;
          token.getInvitationInfo.called.should.be.false;
          Company.getUsers.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Token.findOne fails', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      User.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(commonError));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindOneCall).should.be.true;
          user.getInformation.called.should.be.false;
          token.getInvitationInfo.called.should.be.false;
          Company.getUsers.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if user.getInformation fails', () => {
      const callback = sandbox.spy();

      user.getInformation.restore();
      sandbox.stub(user, 'getInformation').callsFake(() => Promise.reject(commonError));

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.called.should.be.false;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          token.getInvitationInfo.called.should.be.false;
          Company.getUsers.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if token.getInvitationInfo fails', () => {
      const callback = sandbox.spy();

      token.getInvitationInfo.restore();
      User.findOne.restore();
      sandbox.stub(token, 'getInvitationInfo').callsFake(() => Promise.reject(commonError));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWith(tokenFindOneCall).should.be.true;
          user.getInformation.called.should.be.false;
          token.getInvitationInfo.calledOnce.should.be.true;
          token.getInvitationInfo.calledWith().should.be.true;
          Company.getUsers.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Company.getUsers fails', () => {
      const callback = sandbox.spy();

      Company.getUsers.restore();
      sandbox.stub(Company, 'getUsers').callsFake(() => Promise.reject(commonError));

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.called.should.be.false;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          token.getInvitationInfo.called.should.be.false;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.args[0].should.be.eql(getUsersCall);
          token.destroy.called.should.be.false;
          user.destroy.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Company.getUsers returns just one user', () => {
      const callback = sandbox.spy();

      Company.getUsers.restore();
      sandbox.stub(Company, 'getUsers').callsFake(() => Promise.resolve([ {} ]));

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.called.should.be.false;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          token.getInvitationInfo.called.should.be.false;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.args[0].should.be.eql(getUsersCall);
          token.destroy.called.should.be.false;
          user.destroy.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(unableError, guid).should.be.true;
        });
    });

    it('should return an error if no user or token exist', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      User.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindOneCall).should.be.true;
          user.getInformation.called.should.be.false;
          token.getInvitationInfo.called.should.be.false;
          Company.getUsers.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if user.destroy fails', () => {
      const callback = sandbox.spy();

      user.destroy.restore();
      sandbox.stub(user, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.called.should.be.false;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          token.getInvitationInfo.called.should.be.false;
          token.destroy.called.should.be.false;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.args[0].should.be.eql(getUsersCall);
          user.destroy.calledOnce.should.be.true;
          user.destroy.calledWith().should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if token.destroy fails', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      token.destroy.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());
      sandbox.stub(token, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWith(tokenFindOneCall).should.be.true;
          user.getInformation.called.should.be.false;
          token.getInvitationInfo.calledOnce.should.be.true;
          token.getInvitationInfo.calledWith().should.be.true;
          token.destroy.calledOnce.should.be.true;
          token.destroy.calledWith().should.be.true;
          user.destroy.called.should.be.false;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.args[0].should.be.eql(getUsersCall);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a user', () => {
      const callback = sandbox.spy();

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.called.should.be.false;
          user.getInformation.calledOnce.should.be.true;
          user.getInformation.calledWith().should.be.true;
          token.getInvitationInfo.called.should.be.false;
          token.destroy.called.should.be.false;
          user.destroy.calledOnce.should.be.true;
          user.destroy.calledWith().should.be.true;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.args[0].should.be.eql(getUsersCall);
          log.message.args[1].should.be.eql(logUserResponseParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, userInformation).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return invitation information if user does not exist', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return controller.delete(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneCall).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWith(tokenFindOneCall).should.be.true;
          user.getInformation.called.should.be.false;
          token.getInvitationInfo.calledOnce.should.be.true;
          token.getInvitationInfo.calledWith().should.be.true;
          token.destroy.calledOnce.should.be.true;
          token.destroy.calledWith().should.be.true;
          user.destroy.called.should.be.false;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.args[0].should.be.eql(getUsersCall);
          log.message.args[1].should.be.eql(logTokenResponseParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, invitationInfo).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });


  describe('checkEmail', () => {
    const fixtures = helperFixtures.checkEmail;
    const { logResponseParamsDoesNotExists, request,
      logRequestMessageParams, whereParam, logResponseParams, guid, commonError,
      response, responseDoesNotExists } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(User, 'findOne').resolves(true);
    });


    it('should return an error if User.findOne rejects', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.checkEmail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.called.should.be.true;
          User.findOne.calledWithMatch(whereParam).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return exists true', () => {
      const callback = sandbox.spy();

      return controller.checkEmail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.called.should.be.true;
          User.findOne.calledWithMatch(whereParam).should.be.true;
          errorHandler.format.called.should.be.false;
          log.message.args[1].should.be.eql(logResponseParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
        });
    });

    it('should return exists false', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').resolves(false);

      return controller.checkEmail(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          User.findOne.called.should.be.true;
          User.findOne.calledWithMatch(whereParam).should.be.true;
          errorHandler.format.called.should.be.false;
          log.message.args[1].should.be.eql(logResponseParamsDoesNotExists);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, responseDoesNotExists).should.be.true;
        });
    });
  });

  describe('resendInvitation', () => {
    const fixtures = helperFixtures.resendInvitation;
    const { request, token, query, logRequestMessageParams, guid,
      logResponseMessageParams, commonError, notFoundError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(token));
      sandbox.stub(token, 'get').callsFake(() => token);
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'resendInvitation').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection fails', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.resendInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.findOne.called.should.be.false;
          token.get.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.findOne fails', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.resendInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(query).should.be.true;
          token.get.called.should.be.false;
          userProducer.resendInvitation.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if token instance is not found', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());

      return controller.resendInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(query).should.be.true;
          token.get.called.should.be.false;
          userProducer.resendInvitation.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.resendInvitation return a reject', () => {
      const callback = sandbox.spy();

      userProducer.resendInvitation.restore();
      sandbox.stub(userProducer, 'resendInvitation').callsFake(() => Promise.reject(commonError));

      return controller.resendInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(query).should.be.true;
          token.get.calledOnce.should.be.true;
          token.get.calledWith({ plain: true }).should.be.true;
          userProducer.resendInvitation.calledOnce.should.be.true;
          userProducer.resendInvitation.calledWithMatch(token, token.Company, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a success if there was no issue sending the event', () => {
      const callback = sandbox.spy();

      return controller.resendInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(query).should.be.true;
          token.get.calledOnce.should.be.true;
          token.get.calledWith({ plain: true }).should.be.true;
          userProducer.resendInvitation.calledOnce.should.be.true;
          userProducer.resendInvitation.calledWithMatch(token, token.Company, guid).should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, request.request).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('changePassword', () => {
    const fixtures = helperFixtures.changePassword;
    const { request, userInformation, logRequestMessageParams, guid, methodValues,
      logResponseMessageParams, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(User, 'updatePassword').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'passwordChanged').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection fails', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.changePassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.updatePassword.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if User.updatePassword fails', () => {
      const callback = sandbox.spy();

      User.updatePassword.restore();
      sandbox.stub(User, 'updatePassword').callsFake(() => Promise.reject(commonError));

      return controller.changePassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.updatePassword.calledOnce.should.be.true;
          User.updatePassword.args[0].should.be.eql(methodValues);
          userProducer.passwordChanged.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.passwordChanged return a reject', () => {
      const callback = sandbox.spy();

      userProducer.passwordChanged.restore();
      sandbox.stub(userProducer, 'passwordChanged').callsFake(() => Promise.reject(commonError));

      return controller.changePassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.updatePassword.calledOnce.should.be.true;
          User.updatePassword.args[0].should.be.eql(methodValues);
          userProducer.passwordChanged.calledOnce.should.be.true;
          userProducer.passwordChanged.calledWithMatch(userInformation, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a success if there was no issue updating the password', () => {
      const callback = sandbox.spy();

      return controller.changePassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.updatePassword.calledOnce.should.be.true;
          User.updatePassword.args[0].should.be.eql(methodValues);
          userProducer.passwordChanged.calledOnce.should.be.true;
          userProducer.passwordChanged.calledWithMatch(userInformation, guid).should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, userInformation).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('generatePasswordToken', () => {
    const fixtures = helperFixtures.generatePasswordToken;
    const { request, userInformation, logRequestMessageParams, guid, userQuery,
      errorLogResponseMessageParams, logResponseMessageParams, commonError, tokenInformation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(Token, 'recoverPassword').callsFake(() => Promise.resolve(tokenInformation));
      sandbox.stub(tokenInformation, 'get').callsFake(() => tokenInformation);
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'recoverPassword').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection fails', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.generatePasswordToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.called.should.be.false;
          Token.recoverPassword.called.should.be.false;
          tokenInformation.get.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if User.findOne fails', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.generatePasswordToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.args[0].should.be.eql(userQuery);
          Token.recoverPassword.called.should.be.false;
          tokenInformation.get.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should a success if User.findOne returns empty', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return controller.generatePasswordToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.args[0].should.be.eql(userQuery);
          Token.recoverPassword.called.should.be.false;
          tokenInformation.get.called.should.be.false;
          log.message.args[1].should.be.eql(errorLogResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, request.request).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an error if Token.recoverPassword fails', () => {
      const callback = sandbox.spy();

      Token.recoverPassword.restore();
      sandbox.stub(Token, 'recoverPassword').callsFake(() => Promise.reject(commonError));

      return controller.generatePasswordToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.args[0].should.be.eql(userQuery);
          Token.recoverPassword.calledOnce.should.be.true;
          Token.recoverPassword.args[0].should.be.eql([ userInformation ]);
          tokenInformation.get.called.should.be.false;
          userProducer.recoverPassword.called.should.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.recoverPassword fails', () => {
      const callback = sandbox.spy();

      userProducer.recoverPassword.restore();
      sandbox.stub(userProducer, 'recoverPassword').callsFake(() => Promise.reject(commonError));

      return controller.generatePasswordToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.args[0].should.be.eql(userQuery);
          Token.recoverPassword.calledOnce.should.be.true;
          Token.recoverPassword.args[0].should.be.eql([ userInformation ]);
          tokenInformation.get.calledOnce.should.be.true;
          tokenInformation.get.calledWith({ plain: true }).should.be.true;
          userProducer.recoverPassword.calledOnce.should.be.true;
          userProducer.recoverPassword.args[0].should.be.eql([ tokenInformation, guid ]);
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a succes if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.generatePasswordToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.args[0].should.be.eql(userQuery);
          Token.recoverPassword.calledOnce.should.be.true;
          Token.recoverPassword.args[0].should.be.eql([ userInformation ]);
          tokenInformation.get.calledOnce.should.be.true;
          tokenInformation.get.calledWith({ plain: true }).should.be.true;
          userProducer.recoverPassword.calledOnce.should.be.true;
          userProducer.recoverPassword.args[0].should.be.eql([ tokenInformation, guid ]);
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, request.request).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('validateRecoverToken', () => {
    const fixtures = helperFixtures.validateRecoverToken;
    const { request, logRequestMessageParams, guid, logResponseMessageParams,
      commonError, tokenInformation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'validateRecoverToken').callsFake(() => Promise.resolve(tokenInformation));
      sandbox.stub(tokenInformation, 'get').callsFake(() => tokenInformation);
    });

    it('should return an error if Token.validateRecoverToken fails', () => {
      const callback = sandbox.spy();

      Token.validateRecoverToken.restore();
      sandbox.stub(Token, 'validateRecoverToken').callsFake(() => Promise.reject(commonError));

      return controller.validateRecoverToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          tokenInformation.get.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a success if there was no issue', () => {
      const callback = sandbox.spy();

      return controller.validateRecoverToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          tokenInformation.get.calledOnce.should.be.true;
          tokenInformation.get.calledWith({ plain: true }).should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, tokenInformation).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('resetPassword', () => {
    const fixtures = helperFixtures.resetPassword;
    const { request, logRequestMessageParams, guid, logResponseMessageParams,
      commonError, tokenInformation, userInformation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'validateRecoverToken').callsFake(() => Promise.resolve(tokenInformation));
      sandbox.stub(User, 'resetPassword').callsFake(() => Promise.resolve(userInformation));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'passwordChanged').callsFake(() => Promise.resolve());
      sandbox.stub(tokenInformation, 'destroy').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection fails', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.resetPassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.validateRecoverToken.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.validateRecoverToken fails', () => {
      const callback = sandbox.spy();

      Token.validateRecoverToken.restore();
      sandbox.stub(Token, 'validateRecoverToken').callsFake(() => Promise.reject(commonError));

      return controller.resetPassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          User.resetPassword.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if User.resetPassword fails', () => {
      const callback = sandbox.spy();

      User.resetPassword.restore();
      sandbox.stub(User, 'resetPassword').callsFake(() => Promise.reject(commonError));

      return controller.resetPassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          User.resetPassword.calledOnce.should.be.true;
          User.resetPassword.args[0].should.be.eql([ tokenInformation.email, request.request.password ]);
          tokenInformation.destroy.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if token.destroy fails', () => {
      const callback = sandbox.spy();

      tokenInformation.destroy.restore();
      sandbox.stub(tokenInformation, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.resetPassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          User.resetPassword.calledOnce.should.be.true;
          User.resetPassword.args[0].should.be.eql([ tokenInformation.email, request.request.password ]);
          tokenInformation.destroy.calledOnce.should.be.true;
          userProducer.passwordChanged.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.passwordChanged fails', () => {
      const callback = sandbox.spy();

      userProducer.passwordChanged.restore();
      sandbox.stub(userProducer, 'passwordChanged').callsFake(() => Promise.reject(commonError));

      return controller.resetPassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          User.resetPassword.calledOnce.should.be.true;
          User.resetPassword.args[0].should.be.eql([ tokenInformation.email, request.request.password ]);
          tokenInformation.destroy.calledOnce.should.be.true;
          userProducer.passwordChanged.calledOnce.should.be.true;
          userProducer.passwordChanged.args[0].should.be.eql([ userInformation, guid ]);
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.resetPassword(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.validateRecoverToken.calledOnce.should.be.true;
          Token.validateRecoverToken.args[0].should.be.eql([ tokenInformation.token ]);
          User.resetPassword.calledOnce.should.be.true;
          User.resetPassword.args[0].should.be.eql([ tokenInformation.email, request.request.password ]);
          tokenInformation.destroy.calledOnce.should.be.true;
          userProducer.passwordChanged.calledOnce.should.be.true;
          userProducer.passwordChanged.args[0].should.be.eql([ userInformation, guid ]);
          log.message.args[1].should.be.eql(logResponseMessageParams);
          callback.calledOnce.should.be.true;
          callback.calledWith(null, userInformation).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('downloadTracker', () => {
    const fixtures = helperFixtures.downloadTracker;
    const { request, trackerInfo, logRequestMessageParams, logResponseMessageParams, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'getTrackerInfo').callsFake(() => Promise.resolve(trackerInfo));
    });

    it('should return error if Invoice.getTrackerInfo rejects', () => {
      const callback = sandbox.spy();

      Invoice.getTrackerInfo.restore();
      sandbox.stub(Invoice, 'getTrackerInfo').rejects(commonError);

      return controller.downloadTracker(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          Invoice.getTrackerInfo.calledOnce.should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.downloadTracker(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          Invoice.getTrackerInfo.calledOnce.should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });

  describe('findExcelTransactions', () => {
    const fixtures = helperFixtures.findExcelTransactions;
    const { request, findOneQuery, logRequestMessageParams, queryParamsPendingTransactions,
      logResponseMessageParams, commonError, bankRawData, bankReport, pendingTransactions
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(BankReport, 'findOne').callsFake(() => Promise.resolve(bankReport));
      sandbox.stub(PendingTransaction, 'bankRawData').callsFake(() => Promise.resolve(bankRawData));
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve(pendingTransactions));
      sandbox.stub(PendingTransaction, 'buildBankReportQuery').callsFake(() => queryParamsPendingTransactions);
    });

    it('should return error if BankReport.findOne rejects', () => {
      const callback = sandbox.spy();

      BankReport.findOne.restore();
      sandbox.stub(BankReport, 'findOne').rejects(commonError);

      return controller.findExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.findOne.calledOnce.should.be.true;
          BankReport.findOne.calledWith(findOneQuery).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if PendingTransaction.findAll rejects', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').rejects(commonError);

      return controller.findExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(queryParamsPendingTransactions).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if PendingTransaction.bankRawData rejects', () => {
      const callback = sandbox.spy();

      PendingTransaction.bankRawData.restore();
      sandbox.stub(PendingTransaction, 'bankRawData').rejects(commonError);

      return controller.findExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.bankRawData.calledOnce.should.be.true;
          PendingTransaction.bankRawData.calledWith(pendingTransactions).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.findExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          BankReport.findOne.calledOnce.should.be.true;
          BankReport.findOne.calledWith(findOneQuery).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(queryParamsPendingTransactions).should.be.true;
          PendingTransaction.bankRawData.calledOnce.should.be.true;
          PendingTransaction.bankRawData.calledWith(pendingTransactions).should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });

  describe('generateExcelTransactions', () => {
    const fixtures = helperFixtures.generateExcelTransactions;
    const { request, logRequestMessageParams, queryParamsPendingTransactions,
      logResponseMessageParams, commonError, bankRawData, bankReport, pendingTransactions,
      buildBankReportQuery, emptyPendingTransactions, s3Url, excelData, ids, newData
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(PendingTransaction, 'buildBankReportQuery').callsFake(() => queryParamsPendingTransactions);
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve(pendingTransactions));
      sandbox.stub(PendingTransaction, 'bankRawData').callsFake(() => Promise.resolve(bankRawData));
      sandbox.stub(BankReport, 'uploadCxcExcelReportDataToS3').callsFake(() => Promise.resolve(s3Url));
      sandbox.stub(BankReport, 'savePendingTransactionsToBankReport').callsFake(() => Promise.resolve(bankReport));
    });

    it('should return error if PendingTransaction.findAll rejects', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').rejects(commonError);

      return controller.generateExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledWith(buildBankReportQuery).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(queryParamsPendingTransactions).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return error if PendingTransaction.findAll resolves empty array', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').resolves(emptyPendingTransactions);

      return controller.generateExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledWith(buildBankReportQuery).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(queryParamsPendingTransactions).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return error if PendingTransaction.bankRawData rejects', () => {
      const callback = sandbox.spy();

      PendingTransaction.bankRawData.restore();
      sandbox.stub(PendingTransaction, 'bankRawData').rejects(commonError);

      return controller.generateExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledOnce.should.be.true;
          PendingTransaction.buildBankReportQuery.calledWith(buildBankReportQuery).should.be.true;
          PendingTransaction.bankRawData.calledOnce.should.be.true;
          PendingTransaction.bankRawData.calledWith(pendingTransactions).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return error if BankReport.uploadCxcExcelDataToS3 rejects', () => {
      const callback = sandbox.spy();

      BankReport.uploadCxcExcelReportDataToS3.restore();
      sandbox.stub(BankReport, 'uploadCxcExcelReportDataToS3').rejects(commonError);

      return controller.generateExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.uploadCxcExcelReportDataToS3.calledOnce.should.be.true;
          BankReport.uploadCxcExcelReportDataToS3.calledWith(excelData).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if BankReport.savePendingTransactionsToBankReport rejects', () => {
      const callback = sandbox.spy();

      BankReport.savePendingTransactionsToBankReport.restore();
      sandbox.stub(BankReport, 'savePendingTransactionsToBankReport').rejects(commonError);

      return controller.generateExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.savePendingTransactionsToBankReport.calledOnce.should.be.true;
          const args = BankReport.savePendingTransactionsToBankReport.getCalls()[0].args;

          expect(args[0]).to.deep.equal(ids);
          expect(_.omit(args[1], 'generated_at')).to.deep.equal(_.omit(newData, 'generated_at'));
          expect(args[2]).to.deep.equal(null);

          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.generateExcelTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          log.message.args[1].should.be.eql(logResponseMessageParams);

          const args = BankReport.savePendingTransactionsToBankReport.getCalls()[0].args;

          expect(args[0]).to.deep.equal(ids);
          expect(_.omit(args[1], 'generated_at')).to.deep.equal(_.omit(newData, 'generated_at'));
          expect(args[2]).to.deep.equal(null);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });

  describe('excelBankReports', () => {
    const fixtures = helperFixtures.excelBankReports;
    const { request, findAllQuery, logRequestMessageParams,
      logResponseMessageParams, commonError, reports, emptyReports
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(BankReport, 'findAll').callsFake(() => Promise.resolve(reports));
      sandbox.stub(BankReport, 'findNumberOfApprovedTransactions').callsFake(() => Promise.resolve(reports));
    });


    it('should return error if BankReport.findAll rejects', () => {
      const callback = sandbox.spy();

      BankReport.findAll.restore();
      sandbox.stub(BankReport, 'findAll').rejects(commonError);

      return controller.excelBankReports(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.findAll.calledOnce.should.be.true;
          BankReport.findAll.calledWith(findAllQuery).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return error if BankReport.findAll returns empty object', () => {
      const callback = sandbox.spy();

      BankReport.findAll.restore();
      sandbox.stub(BankReport, 'findAll').resolves(emptyReports);

      return controller.excelBankReports(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.findAll.calledOnce.should.be.true;
          BankReport.findAll.calledWith(findAllQuery).should.be.true;
          BankReport.findNumberOfApprovedTransactions.calledOnce.should.be.false;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });


    it('should return error if BankReport.findNumberOfApprovedTransactions rejects', () => {
      const callback = sandbox.spy();


      BankReport.findNumberOfApprovedTransactions.restore();
      sandbox.stub(BankReport, 'findNumberOfApprovedTransactions').rejects(commonError);

      return controller.excelBankReports(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.findNumberOfApprovedTransactions.calledOnce.should.be.true;
          BankReport.findNumberOfApprovedTransactions.calledWith(reports).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.excelBankReports(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);

          BankReport.findAll.calledOnce.should.be.true;
          BankReport.findAll.calledWith(findAllQuery).should.be.true;
          BankReport.findNumberOfApprovedTransactions.calledOnce.should.be.true;
          BankReport.findNumberOfApprovedTransactions.calledWith(reports).should.be.true;
          log.message.args[1].should.be.eql(logResponseMessageParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });

  describe('findExcelTransactionsFromBankReportId', () => {
    const fixtures = helperFixtures.findExcelTransactionsFromBankReportId;
    const { request, findOneQuery, logRequestMessageParams, queryParamsPendingTransactions,
      logResponseMessageParams, commonError, bankRawData, bankReport, pendingTransactions
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(BankReport, 'findOne').callsFake(() => Promise.resolve(bankReport));
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve(pendingTransactions));
      sandbox.stub(PendingTransaction, 'bankRawData').callsFake(() => Promise.resolve(bankRawData));
    });

    it('should return error if BankReport.findOne rejects', () => {
      const callback = sandbox.spy();

      BankReport.findOne.restore();
      sandbox.stub(BankReport, 'findOne').rejects(commonError);

      return controller.findExcelTransactionsFromBankReportId(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.findOne.calledOnce.should.be.true;
          BankReport.findOne.calledWith(findOneQuery).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if BankReport.findOne returns null', () => {
      const callback = sandbox.spy();

      BankReport.findOne.restore();
      sandbox.stub(BankReport, 'findOne').resolves(null);

      return controller.findExcelTransactionsFromBankReportId(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          BankReport.findOne.calledOnce.should.be.true;
          BankReport.findOne.calledWith(findOneQuery).should.be.true;
          PendingTransaction.findAll.called.should.be.false;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if PendingTransaction.findAll rejects', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').rejects(commonError);

      return controller.findExcelTransactionsFromBankReportId(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.findAll.called.should.be.true;
          PendingTransaction.findAll.calledWith(queryParamsPendingTransactions).should.be.true;
          PendingTransaction.bankRawData.called.should.be.false;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if PendingTransaction.findAll returns empty array', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').resolves([]);

      return controller.findExcelTransactionsFromBankReportId(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.findAll.called.should.be.true;
          PendingTransaction.findAll.calledWith(queryParamsPendingTransactions).should.be.true;
          PendingTransaction.bankRawData.called.should.be.false;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if PendingTransaction.bankRawData rejects', () => {
      const callback = sandbox.spy();

      PendingTransaction.bankRawData.restore();
      sandbox.stub(PendingTransaction, 'bankRawData').rejects(commonError);

      return controller.findExcelTransactionsFromBankReportId(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          PendingTransaction.bankRawData.called.should.be.true;
          PendingTransaction.bankRawData.calledWith(pendingTransactions).should.be.true;
          errorHandler.format.called.should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.findExcelTransactionsFromBankReportId(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.args[0].should.be.eql(logRequestMessageParams);
          log.message.args[1].should.be.eql(logResponseMessageParams);
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });
});

