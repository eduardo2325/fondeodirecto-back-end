const { agreed: fixtures } = require('../../fixtures/controllers/company');

const controller = require('../../../../api/controllers/company');
const errorHandler = require('../../../../api/helpers/error');
const userProducer = require('../../../../api/producers/user');

const { Agreement, Session, sequelize } = require('../../../../models');

const log = require('/var/lib/core/js/log').prototype;

const td = require('testdouble');

const {
  request,
  response,
  guid,
  commonError,
  role,
  id,
  data,
  where,
  foundAgreement
} = fixtures;

const IF_OK = Symbol('IT_SHALL_NOT_PASS');

describe('user/unit/Company controller', () => {
  let errorFormatCallback;
  let rollbackCallback;
  let updateCallback;
  let findCallback;
  let saveCallback;
  let agreeCallback;
  let sendCallback;

  function shouldFail(expected) {
    return actual => {
      actual.should.be.equal(expected);
      td.explain(rollbackCallback).callCount.should.be.equal(1);
      td.explain(errorFormatCallback).callCount.should.be.equal(1);
    };
  }

  function shouldSucceed(expected) {
    return actual => {
      actual.should.be.equal(expected);
      td.explain(sendCallback).callCount.should.be.equal(1);
      td.explain(errorFormatCallback).callCount.should.be.equal(0);
    };
  }

  beforeEach(() => {
    errorFormatCallback = td.func('errorHandler.errorFormat');
    rollbackCallback = td.func('transaction.rollback');
    updateCallback = td.func('Session.update');
    findCallback = td.func('Agreement.findContract');
    saveCallback = td.func('foundAgreement.save');
    agreeCallback = td.func('Agreement.finalizeContract');
    sendCallback = td.func('grpc.callback');

    td.replace(userProducer, 'ensureConnection', td.func('userProducer.ensureConnection'));
    td.replace(userProducer, 'agreementAccepted', td.func('userProducer.agreementAccepted'));

    td.replace(sequelize, 'transaction', cb => cb({ rollback: rollbackCallback }));
    td.replace(foundAgreement, 'save', saveCallback);
    td.replace(Session, 'update', updateCallback);
    td.replace(Agreement, 'finalizeContract', agreeCallback);
    td.replace(Agreement, 'findContract', findCallback);

    td.replace(errorHandler, 'format', errorFormatCallback);
    td.replace(log, 'message');
  });

  afterEach(() => {
    td.reset();
  });

  describe('agreed', () => {
    it('should return an error if userProducer.ensureConnection() rejects anything', () => {
      td.when(userProducer.ensureConnection()).thenReject(commonError);
      td.when(errorHandler.format(commonError, guid, sendCallback)).thenResolve(IF_OK);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldFail(IF_OK));
    });

    it('should return an error if Agreement.findContract() rejects anything', () => {
      td.when(userProducer.ensureConnection()).thenResolve();
      td.when(errorHandler.format(commonError, guid, sendCallback)).thenResolve(IF_OK);

      td.when(Agreement.findContract(id, role, {
        transaction: td.matchers.isA(Object)
      })).thenReject(commonError);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldFail(IF_OK));
    });

    it('should return an error if Agreement.finalizeContract() rejects anything', () => {
      td.when(userProducer.ensureConnection()).thenResolve();
      td.when(errorHandler.format(commonError, guid, sendCallback)).thenResolve(IF_OK);

      td.when(Agreement.findContract(id, role, {
        transaction: td.matchers.isA(Object)
      })).thenReject(commonError);

      td.when(Agreement.finalizeContract(foundAgreement, guid))
        .thenReject(commonError);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldFail(IF_OK));
    });

    it('should return an error if userProducer.agreementAccepted() rejects anything', () => {
      td.when(userProducer.ensureConnection()).thenResolve();
      td.when(errorHandler.format(commonError, guid, sendCallback)).thenResolve(IF_OK);

      td.when(Agreement.findContract(id, role, {
        transaction: td.matchers.isA(Object)
      })).thenResolve(foundAgreement);

      td.when(Agreement.finalizeContract(foundAgreement, guid))
        .thenResolve('SOME_S3_URL');

      td.when(userProducer.agreementAccepted(td.matchers.isA(Object), guid))
        .thenReject(commonError);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldFail(IF_OK));
    });

    it('should return an error if foundAgreement.save() rejects anything', () => {
      td.when(userProducer.ensureConnection()).thenResolve();
      td.when(errorHandler.format(commonError, guid, sendCallback)).thenResolve(IF_OK);

      td.when(Agreement.findContract(id, role, {
        transaction: td.matchers.isA(Object)
      })).thenResolve(foundAgreement);

      td.when(Agreement.finalizeContract(foundAgreement, guid))
        .thenResolve('SOME_S3_URL');

      td.when(userProducer.agreementAccepted(td.matchers.isA(Object), guid))
        .thenResolve();

      td.when(foundAgreement.save({
        transaction: td.matchers.isA(Object)
      })).thenReject(commonError);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldFail(IF_OK));
    });

    it('should return an error if Session.update() rejects anything', () => {
      td.when(userProducer.ensureConnection()).thenResolve();
      td.when(errorHandler.format(commonError, guid, sendCallback)).thenResolve(IF_OK);

      td.when(Agreement.findContract(id, role, {
        transaction: td.matchers.isA(Object)
      })).thenResolve(foundAgreement);

      td.when(Agreement.finalizeContract(foundAgreement, guid))
        .thenResolve('SOME_S3_URL');

      td.when(userProducer.agreementAccepted(td.matchers.isA(Object), guid))
        .thenResolve();

      td.when(Session.update(data, { where, transaction: td.matchers.isA(Object) }))
        .thenReject(commonError);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldFail(IF_OK));
    });

    it('should return a succesful response otherwise', () => {
      td.when(userProducer.ensureConnection()).thenResolve();
      td.when(sendCallback(null, response)).thenResolve(IF_OK);
      td.when(Agreement.finalizeContract(foundAgreement, guid))
        .thenResolve('SOME_S3_URL');

      td.when(Agreement.findContract(id, role, {
        transaction: td.matchers.isA(Object)
      })).thenResolve(foundAgreement);

      td.when(userProducer.agreementAccepted(td.matchers.isA(Object), guid))
        .thenResolve();

      td.when(foundAgreement.save({
        transaction: td.matchers.isA(Object)
      })).thenResolve();

      td.when(Session.update(data, { where, transaction: td.matchers.isA(Object) }))
        .thenResolve(foundAgreement);

      return controller.agreed(request, sendCallback)
        .should.be.fulfilled
        .then(shouldSucceed(IF_OK));
    });
  });
});
