const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/company');
const errorHandler = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/controllers/company');
const statementQueries = require('../../../api/helpers/statement-queries');

const { Company, Token, OperationCost, User, Session, Agreement, Invoice, sequelize } = require('../../../models');
const userProducer = require('../../../api/producers/user');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Company controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('create', () => {
    const fixtures = helperFixtures.create;
    const { request, fullInformation, company, message, guid, requestType, responseType,
      commonError, transaction, token, operationCost, investorRequest, notNullError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'create').callsFake(() => Promise.resolve(company));
      sandbox.stub(Token, 'userRegistration').callsFake(() => Promise.resolve(token));
      sandbox.stub(OperationCost, 'create').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(company, 'getInformation').callsFake(() => Promise.resolve(fullInformation));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invitationCreated').callsFake(() => Promise.resolve());
    });

    it('should return an error if company is an investor and taxpayer type is not included', () => {
      const callback = sandbox.spy();
      const investor = _.cloneDeep(investorRequest);

      delete investor.request.taxpayer_type;

      return controller.create(investor, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, investor.request, requestType, guid).should.be.true;
          Company.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notNullError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company is an investor and taxpayer type is not valid', () => {
      const callback = sandbox.spy();
      const investor = _.cloneDeep(investorRequest);

      investor.request.taxpayer_type = 'not valid';

      return controller.create(investor, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, investor.request, requestType, guid).should.be.true;
          Company.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notNullError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.create return a reject', () => {
      const callback = sandbox.spy();

      Company.create.restore();
      sandbox.stub(Company, 'create').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.create.calledOnce.should.be.true;
          Company.create.calledWithMatch(request.request, { transaction: transaction }).should.be.true;
          userProducer.ensureConnection.called.should.be.false;
          OperationCost.create.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.create.calledOnce.should.be.true;
          Company.create.calledWithMatch(request.request, { transaction: transaction }).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          OperationCost.create.calledOnce.should.be.false;
          Token.userRegistration.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if OperationCost.create return a reject', () => {
      const callback = sandbox.spy();

      OperationCost.create.restore();
      sandbox.stub(OperationCost, 'create').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.create.calledOnce.should.be.true;
          Company.create.calledWithMatch(request.request, { transaction: transaction }).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCost, { transaction }).should.be.true;
          Token.userRegistration.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.getInformation return a reject', () => {
      const callback = sandbox.spy();

      company.getInformation.restore();
      sandbox.stub(company, 'getInformation').callsFake(() => Promise.reject(commonError));

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.create.calledOnce.should.be.true;
          Company.create.calledWithMatch(request.request, { transaction: transaction }).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCost, { transaction }).should.be.true;
          Token.userRegistration.called.should.be.false;
          userProducer.invitationCreated.calledOnce.should.be.false;
          company.getInformation.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.create(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.create.calledOnce.should.be.true;
          Company.create.calledWithMatch(request.request, { transaction: transaction }).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCost, { transaction }).should.be.true;
          Token.userRegistration.called.should.be.false;
          userProducer.invitationCreated.calledOnce.should.be.false;
          company.getInformation.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, fullInformation, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, fullInformation).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('exists', () => {
    const fixtures = helperFixtures.exists;
    const { request, exists, company, message, guid, query, requestType, responseType,
      commonError, notExists } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.exists(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a false response if the copmany doesn\'t exists', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.exists(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          log.message.secondCall.calledWithMatch(message, notExists, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, notExists).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.exists(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          log.message.secondCall.calledWithMatch(message, exists, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, exists).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('update', () => {
    const fixtures = helperFixtures.update;
    const { request, company, companyObj, companyValues, returning, message, guid, query, requestType, responseType,
      commonError, notFound, requestWithoutName, requestWithoutBussinessName, requestWithoutHolder,
      requestWithoutClabe, requestWithoutDescription, companyWithoutName, companyWithoutBussinessName,
      companyWithoutHolder, companyWithoutClabe, companyWithoutDescription } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(companyObj));
      sandbox.stub(companyObj, 'update').callsFake(() => Promise.resolve(companyObj));
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.update(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return an empty value', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.update(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if companyObj.update return a reject', () => {
      const callback = sandbox.spy();

      companyObj.update.restore();
      sandbox.stub(companyObj, 'update').callsFake(() => Promise.reject(commonError));

      return controller.update(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyValues, returning).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response ignoring name parameter', () => {
      const callback = sandbox.spy();

      return controller.update(requestWithoutName, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestWithoutName.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyWithoutName, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, company, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, company).should.be.true;
        });
    });

    it('should return a successful response ignoring Business Name parameter', () => {
      const callback = sandbox.spy();

      return controller.update(requestWithoutBussinessName, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestWithoutBussinessName.request,
            requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyWithoutBussinessName, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, company, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, company).should.be.true;
        });
    });

    it('should return a successful response ignoring Holder parameter', () => {
      const callback = sandbox.spy();

      return controller.update(requestWithoutHolder, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestWithoutHolder.request,
            requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyWithoutHolder, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, company, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, company).should.be.true;
        });
    });

    it('should return a successful response ignoring Clabe parameter', () => {
      const callback = sandbox.spy();

      return controller.update(requestWithoutClabe, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestWithoutClabe.request,
            requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyWithoutClabe, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, company, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, company).should.be.true;
        });
    });

    it('should return a successful response ignoring description parameter', () => {
      const callback = sandbox.spy();

      return controller.update(requestWithoutDescription, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestWithoutDescription.request,
            requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyWithoutDescription, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, company, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, company).should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.update(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          companyObj.update.calledOnce.should.be.true;
          companyObj.update.calledWithMatch(companyValues, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, company, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, company).should.be.true;
        });
    });
  });

  describe('updateOperationCost', () => {
    const fixtures = helperFixtures.updateOperationCost;
    const { request, operationCost, operationCostValues, returning, message, guid, query, requestType, responseType,
      commonError, notFound } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(operationCost, 'update').callsFake(() => Promise.resolve(operationCost));
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve(operationCost));
    });

    it('should return an error if OperationCost.findOne return a reject', () => {
      const callback = sandbox.spy();

      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.updateOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          operationCost.update.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if OperationCost.findOne return an empty value', () => {
      const callback = sandbox.spy();

      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve());

      return controller.updateOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          operationCost.update.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if operationCost.update return a reject', () => {
      const callback = sandbox.spy();

      operationCost.update.restore();
      sandbox.stub(operationCost, 'update').callsFake(() => Promise.reject(commonError));

      return controller.updateOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          operationCost.update.calledOnce.should.be.true;
          operationCost.update.calledWithMatch(operationCostValues, returning).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.updateOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          operationCost.update.calledOnce.should.be.true;
          operationCost.update.calledWithMatch(operationCostValues, returning).should.be.true;
          log.message.secondCall.calledWithMatch(message, operationCostValues, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, {}).should.be.true;
        });
    });
  });

  describe('getFullInformation', () => {
    const fixtures = helperFixtures.getFullInformation;
    const { request, fullInformation, company, message, guid, query, requestType, responseType,
      commonError, notFound } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(company, 'getInformation').callsFake(() => Promise.resolve(fullInformation));
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getFullInformation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.getInformation.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if the copmany doesn\'t exists', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.getFullInformation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.getInformation.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.getInformation return a reject', () => {
      const callback = sandbox.spy();

      company.getInformation.restore();
      sandbox.stub(company, 'getInformation').callsFake(() => Promise.reject(commonError));

      return controller.getFullInformation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.getInformation.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.getFullInformation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.getInformation.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, fullInformation, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, fullInformation).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('analyzeClabe', () => {
    const fixtures = helperFixtures.analyzeClabe;
    const { request, response, message, guid, requestType, responseType, clabe,
      commonError, bankInfo, company, companyQuery, clabeNotUniqueError, requestFalse } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.resolve(bankInfo));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.analyzeClabe(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Company.analyzeClabe.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if the clabe belongs to a company', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));

      return controller.analyzeClabe(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Company.analyzeClabe.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(clabeNotUniqueError, guid).should.be.true;
        });
    });

    it('should return an error if Company.analyzeClabe return a reject', () => {
      const callback = sandbox.spy();

      Company.analyzeClabe.restore();
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.reject(commonError));

      return controller.analyzeClabe(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;

          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a successful response without calling findOne method', () => {
      const callback = sandbox.spy();

      return controller.analyzeClabe(requestFalse, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestFalse.request, requestType, guid).should.be.true;
          Company.findOne.called.should.be.false;
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.analyzeClabe(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getCompanies', () => {
    const fixtures = helperFixtures.getCompanies;
    const { request, response, message, guid, requestType, responseType, companies,
      limit, offset, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'getCompanies').callsFake(() => Promise.resolve(companies));
      sandbox.stub(Company, 'count').callsFake(() => Promise.resolve(4));
    });

    it('should return an error if Company.getCompanies return a reject', () => {
      const callback = sandbox.spy();

      Company.getCompanies.restore();
      sandbox.stub(Company, 'getCompanies').callsFake(() => Promise.reject(commonError));

      return controller.getCompanies(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.getCompanies.calledOnce.should.be.true;
          Company.getCompanies.calledWithMatch(limit, offset).should.be.true;
          Company.count.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Company.count return a reject', () => {
      const callback = sandbox.spy();

      Company.count.restore();
      sandbox.stub(Company, 'count').callsFake(() => Promise.reject(commonError));

      return controller.getCompanies(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.getCompanies.calledOnce.should.be.true;
          Company.getCompanies.calledWithMatch(limit, offset).should.be.true;
          Company.count.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getCompanies(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.getCompanies.calledOnce.should.be.true;
          Company.getCompanies.calledWithMatch(limit, offset).should.be.true;
          Company.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getUsers', () => {
    const fixtures = helperFixtures.getUsers;
    const { request, response, message, guid, requestType, responseType, usersList,
      commonError, orderBy, orderDesc, id } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'getUsers').callsFake(() => Promise.resolve(usersList));
    });

    it('should return an error if Company.getUsers return a reject', () => {
      const callback = sandbox.spy();

      Company.getUsers.restore();
      sandbox.stub(Company, 'getUsers').callsFake(() => Promise.reject(commonError));

      return controller.getUsers(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.calledWithMatch(id, orderBy, orderDesc).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.getUsers(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.calledWithMatch(id, orderBy, orderDesc).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return a successful response with default values', () => {
      const callback = sandbox.spy();

      delete request.request.order_by;
      delete request.request.order_desc;

      return controller.getUsers(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.getUsers.calledOnce.should.be.true;
          Company.getUsers.calledWithMatch(id, 'name', 'ASC').should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getOperationCost', () => {
    const fixtures = helperFixtures.getOperationCost;
    const { request, operationCost, message, guid, query, requestType, responseType,
      commonError, notFoundError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve(operationCost));
    });

    it('should return an error if OperationCost.findOne return a reject', () => {
      const callback = sandbox.spy();

      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if OperationCost.findOne return a null value', () => {
      const callback = sandbox.spy();

      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve());

      return controller.getOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.getOperationCost(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWithMatch(query).should.be.true;
          log.message.secondCall.calledWithMatch(message, operationCost, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, operationCost).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('registerInvestor', () => {
    const fixtures = helperFixtures.registerInvestor;
    const { request, newCompany, tokenQuery, tokenInstance, message, secondMessage, responseUser, guid, newUser,
      requestType, userData, responseType, newCompanyDataPhysical, agreementObj,
      previewContractParams, emailNotAllowedError, anotherEmailtokenInstance, newCompanyPlain,
      operationCostValues, commonError, newCompanyData, companyInstanceGetParams, requestPhysicalType,
      tokenNotFoundError, transaction, taxpayerTypes, taxpayerError, s3Url, userProducerParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(_, 'includes').callsFake(() => true);
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(tokenInstance));
      sandbox.stub(Company, 'create').callsFake(() => Promise.resolve(newCompany));
      sandbox.stub(newCompany, 'get').callsFake(() => Promise.resolve(newCompanyPlain));
      sandbox.stub(OperationCost, 'create').callsFake(() => Promise.resolve(true));
      sandbox.stub(User, 'create').callsFake(() => Promise.resolve(newUser));
      sandbox.stub(tokenInstance, 'destroy').callsFake(() => Promise.resolve(true));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'agreementAccepted').callsFake(() => Promise.resolve());
      sandbox.stub(Agreement, 'previewContract').callsFake(() => Promise.resolve(s3Url));
      sandbox.stub(Agreement, 'finalizeContract').callsFake((agreement) => {
        agreement.agreed_at = 'SomeDate';
        return Promise.resolve(s3Url);
      });
      sandbox.stub(Agreement, 'create').callsFake(() => Promise.resolve());
    });

    it('should return an error if includes taxpayer_type returns false', () => {
      const callback = sandbox.spy();

      _.includes.restore();
      sandbox.stub(_, 'includes').callsFake(() => false);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(taxpayerError, guid).should.be.true;
          callback.calledOnce.should.be.true;
          sequelize.transaction.called.should.be.false;
        });
    });

    it('should return an error if userProducer.ensureConnection rejects', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.findOne rejects', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.findOne does not found error', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').resolves(false);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(tokenNotFoundError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.create rejects', () => {
      const callback = sandbox.spy();

      Company.create.restore();
      sandbox.stub(Company, 'create').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.get rejects', () => {
      const callback = sandbox.spy();

      newCompany.get.restore();
      sandbox.stub(newCompany, 'get').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if OperationCost.create returns rejects', () => {
      const callback = sandbox.spy();

      OperationCost.create.restore();
      sandbox.stub(OperationCost, 'create').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if token.email does not match request.email', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').resolves(anotherEmailtokenInstance);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(emailNotAllowedError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if User.create rejects', () => {
      const callback = sandbox.spy();

      User.create.restore();
      sandbox.stub(User, 'create').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if tokenInstance rejects', () => {
      const callback = sandbox.spy();

      tokenInstance.destroy.restore();
      sandbox.stub(tokenInstance, 'destroy').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Agreement.previewContract rejects', () => {
      const callback = sandbox.spy();

      Agreement.previewContract.restore();
      sandbox.stub(Agreement, 'previewContract').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch( taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.calledWithMatch(
            agreementObj, previewContractParams, request.request.guid).should.be.true;
          userProducer.agreementAccepted.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Agreement.finalizeContract rejects', () => {
      const callback = sandbox.spy();

      Agreement.finalizeContract.restore();
      sandbox.stub(Agreement, 'finalizeContract').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch( taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.calledWithMatch(
            agreementObj, previewContractParams, request.request.guid).should.be.true;
          Agreement.finalizeContract.calledOnce.should.be.true;
          Agreement.finalizeContract.calledWithMatch(agreementObj, request.request.guid).should.be.true;
          userProducer.agreementAccepted.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.agreementAccepted rejects', () => {
      const callback = sandbox.spy();

      userProducer.agreementAccepted.restore();
      sandbox.stub(userProducer, 'agreementAccepted').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch( taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.calledWithMatch(
            agreementObj, previewContractParams, request.request.guid).should.be.true;
          Agreement.finalizeContract.calledOnce.should.be.true;
          Agreement.finalizeContract.calledWithMatch(agreementObj, request.request.guid).should.be.true;
          userProducer.agreementAccepted.calledOnce.should.be.true;
          userProducer.agreementAccepted.calledWithMatch(userProducerParams, guid).should.be.true;
          Agreement.create.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Agreement.create rejects', () => {
      const callback = sandbox.spy();

      Agreement.create.restore();
      sandbox.stub(Agreement, 'create').rejects(commonError);

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch( taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.calledWithMatch(
            agreementObj, previewContractParams, request.request.guid).should.be.true;
          Agreement.finalizeContract.calledOnce.should.be.true;
          Agreement.finalizeContract.calledWithMatch(agreementObj, request.request.guid).should.be.true;
          userProducer.agreementAccepted.calledOnce.should.be.true;
          userProducer.agreementAccepted.calledWithMatch(userProducerParams, guid).should.be.true;
          Agreement.create.calledOnce.should.be.true;
          Agreement.create.calledWithMatch(agreementObj, { transaction }).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an success', () => {
      const callback = sandbox.spy();

      return controller.registerInvestor(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch( taxpayerTypes, request.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyData, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.calledWithMatch(
            agreementObj, previewContractParams, request.request.guid).should.be.true;
          Agreement.finalizeContract.calledOnce.should.be.true;
          Agreement.finalizeContract.calledWithMatch(agreementObj, request.request.guid).should.be.true;
          userProducer.agreementAccepted.calledOnce.should.be.true;
          userProducer.agreementAccepted.calledWithMatch(userProducerParams, guid).should.be.true;
          Agreement.create.calledOnce.should.be.true;
          Agreement.create.calledWithMatch(agreementObj, { transaction }).should.be.true;
          log.message.secondCall.calledWithMatch(secondMessage, responseUser, responseType, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return valid response on physical taxpayer_type', () => {
      const callback = sandbox.spy();

      return controller.registerInvestor(requestPhysicalType, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, requestPhysicalType.request, requestType, guid).should.be.true;
          _.includes.calledOnce.should.be.true;
          _.includes.calledWithMatch(taxpayerTypes, requestPhysicalType.request.taxpayer_type).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenQuery).should.be.true;
          Company.create.called.should.be.true;
          Company.create.calledWithMatch( newCompanyDataPhysical, { transaction } ).should.be.true;
          newCompany.get.called.should.be.true;
          newCompany.get.calledWithMatch( companyInstanceGetParams ).should.be.true;
          OperationCost.create.calledOnce.should.be.true;
          OperationCost.create.calledWithMatch(operationCostValues, { transaction }).should.be.false;
          User.create.called.should.be.true;
          User.create.calledWithMatch(userData, { transaction }).should.be.true;
          tokenInstance.destroy.calledOnce.should.be.true;
          tokenInstance.destroy.calledWithMatch( { transaction } ).should.be.true;
          log.message.secondCall.calledWithMatch(secondMessage, responseUser, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, responseUser).should.be.true;
          errorHandler.format.calledOnce.should.be.false;
        });
    });
  });

  describe('addUser', () => {
    const fixtures = helperFixtures.addUser;
    const { request, company, query, message, guid, newUser, requestType, responseType,
      color, commonError, notFoundError, token, responseUser } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(Token, 'userRegistration').callsFake(() => Promise.resolve(token));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'invitationCreated').callsFake(() => Promise.resolve());
      sandbox.stub(_, 'sample').callsFake(() => color);
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.addUser(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return an empty response', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.addUser(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.addUser(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.userRegistration.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.userRegistration return a reject', () => {
      const callback = sandbox.spy();

      Token.userRegistration.restore();
      sandbox.stub(Token, 'userRegistration').callsFake(() => Promise.reject(commonError));

      return controller.addUser(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.userRegistration.calledOnce.should.be.true;
          Token.userRegistration.calledWithMatch(newUser, company.role).should.be.true;
          userProducer.invitationCreated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.invitationCreated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.invitationCreated.restore();
      sandbox.stub(userProducer, 'invitationCreated').callsFake(() => Promise.reject(commonError));

      return controller.addUser(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Token.userRegistration.calledOnce.should.be.true;
          Token.userRegistration.calledWithMatch(newUser, company.role).should.be.true;
          userProducer.invitationCreated.calledOnce.should.be.true;
          userProducer.invitationCreated.calledWithMatch(token, company, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.addUser(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          Token.userRegistration.calledOnce.should.be.true;
          Token.userRegistration.calledWithMatch(newUser, company.role).should.be.true;
          userProducer.invitationCreated.calledOnce.should.be.true;
          userProducer.invitationCreated.calledWithMatch(token, company, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, responseUser, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, responseUser).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });


  describe('getFactorajeAndCashSummaries', () => {
    const fixtures = helperFixtures.getFactorajeAndCashSummaries;
    const { request, belowOfRangeRequest, aboveOfRangeRequest, requestType, message, notFound,
      commonError, investor, companyQuery, invoices, cashflowDataOne, cashflowDataTwo,
      purchasedInvoices, payedInvoices, invoiceFindParam, total, totalPurchased, totalPayed,
      response, responseMessage, responseType, guid } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(Company, 'findOne').resolves(investor);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findAll').resolves(invoices);
      sandbox.stub(statementQueries, 'purchasedInvoices').resolves(purchasedInvoices);
      sandbox.stub(statementQueries, 'payedInvoices').resolves(payedInvoices);

      const investorStub = sandbox.stub(investor, 'getCashFlowFromRange');

      investorStub.onCall(0).resolves(cashflowDataOne);
      investorStub.onCall(1).resolves(cashflowDataTwo);

      const reduceStub = sandbox.stub(_, 'reduce');

      reduceStub.onCall(0).callsFake( () => total);
      reduceStub.onCall(1).callsFake( () => totalPurchased );
      reduceStub.onCall(2).callsFake( () => totalPayed);
    });

    it('should return error if requested date below allowed', () => {
      const callback = sandbox.spy();

      return controller.getFactorajeAndCashSummaries(belowOfRangeRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, belowOfRangeRequest.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return error if requested date above allowed', () => {
      const callback = sandbox.spy();

      return controller.getFactorajeAndCashSummaries(aboveOfRangeRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, aboveOfRangeRequest.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return error if Company.findOne rejects', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').rejects(commonError);

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if Invoice.findAll rejects', () => {
      const callback = sandbox.spy();

      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').rejects(commonError);

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceFindParam).should.be.true;
          _.reduce.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if statementQueries.purchasedInvoices rejects', () => {
      const callback = sandbox.spy();

      statementQueries.purchasedInvoices.restore();
      sandbox.stub(statementQueries, 'purchasedInvoices').rejects(commonError);

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceFindParam).should.be.true;
          statementQueries.purchasedInvoices.calledOnce.should.be.true;
          statementQueries.purchasedInvoices.calledWithMatch(request.request.company_id, request.request.year,
            request.request.month).should.be.true;
          _.reduce.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if statementQueries.payedInvoices rejects', () => {
      const callback = sandbox.spy();

      statementQueries.payedInvoices.restore();
      sandbox.stub(statementQueries, 'payedInvoices').rejects(commonError);

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceFindParam).should.be.true;
          statementQueries.payedInvoices.calledOnce.should.be.true;
          statementQueries.payedInvoices.calledWithMatch(request.request.company_id, request.request.year,
            request.request.month).should.be.true;
          _.reduce.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if companyInstance.getCashFlowFromRange 1st call rejects', () => {
      const callback = sandbox.spy();

      investor.getCashFlowFromRange.restore();
      sandbox.stub(investor, 'getCashFlowFromRange').onCall(0).rejects(commonError);

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceFindParam).should.be.true;
          statementQueries.payedInvoices.calledOnce.should.be.true;
          statementQueries.payedInvoices.calledWithMatch(request.request.company_id, request.request.year,
            request.request.month).should.be.true;
          investor.getCashFlowFromRange.calledTwice.should.be.true;
          _.reduce.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if companyInstance.getCashFlowFromRange 2nd call rejects', () => {
      const callback = sandbox.spy();

      investor.getCashFlowFromRange.restore();
      sandbox.stub(investor, 'getCashFlowFromRange').onCall(1).rejects(commonError);

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceFindParam).should.be.true;
          statementQueries.payedInvoices.calledOnce.should.be.true;
          statementQueries.payedInvoices.calledWithMatch(request.request.company_id, request.request.year,
            request.request.month).should.be.true;
          investor.getCashFlowFromRange.calledTwice.should.be.true;
          _.reduce.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a succesful response', () => {
      const callback = sandbox.spy();

      return controller.getFactorajeAndCashSummaries(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(invoiceFindParam).should.be.true;
          statementQueries.payedInvoices.calledOnce.should.be.true;
          statementQueries.payedInvoices.calledWithMatch(request.request.company_id, request.request.year,
            request.request.month).should.be.true;
          investor.getCashFlowFromRange.calledTwice.should.be.true;
          _.reduce.callCount.should.be.equal(3);
          log.message.secondCall.calledWithMatch(responseMessage, response, responseType, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });

  describe('createInvestorInvitation', () => {
    const fixtures = helperFixtures.createInvestorInvitation;
    const { request, message, guid, newUser, requestType, responseType,
      commonError, foundedToken, newToken, tokenData, tokenFindQuery,
      userFindQuery, existentUserError, response, tokenGet, invitation } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'investorInvitationCreated').callsFake(() => Promise.resolve());
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));

      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(false));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(foundedToken));
      sandbox.stub(foundedToken, 'destroy').callsFake(() => Promise.resolve());
      sandbox.stub(Token, 'create').callsFake(() => Promise.resolve(newToken));
      sandbox.stub(newToken, 'get').callsFake(() => Promise.resolve(tokenGet));
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();

      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();

      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if User.findOne return a valid user', () => {
      const callback = sandbox.spy();

      User.findOne.restore();

      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(newUser));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(existentUserError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.findOne return a reject', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();

      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          Token.create.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if token.destroy returns a reject', () => {
      const callback = sandbox.spy();

      foundedToken.destroy.restore();

      sandbox.stub(foundedToken, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          foundedToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Token.create returns a reject', () => {
      const callback = sandbox.spy();

      Token.create.restore();

      sandbox.stub(Token, 'create').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          foundedToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(tokenData).should.be.true;
          newToken.get.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if newToken.get returns a reject', () => {
      const callback = sandbox.spy();

      newToken.get.restore();

      sandbox.stub(newToken, 'get').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          foundedToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(tokenData).should.be.true;
          newToken.get.calledOnce.should.be.true;
          userProducer.investorInvitationCreated.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.investorInvitationCreated returns a reject', () => {
      const callback = sandbox.spy();

      userProducer.investorInvitationCreated.restore();

      sandbox.stub(userProducer, 'investorInvitationCreated').callsFake(() => Promise.reject(commonError));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          foundedToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(tokenData).should.be.true;
          newToken.get.calledOnce.should.be.true;
          userProducer.investorInvitationCreated.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response when there is new token', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(false));

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;

          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          foundedToken.destroy.calledOnce.should.be.false;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(tokenData).should.be.true;
          newToken.get.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.investorInvitationCreated.calledOnce.should.be.true;
          userProducer.investorInvitationCreated.calledWithMatch(invitation, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.createInvestorInvitation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;

          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindQuery).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(tokenFindQuery).should.be.true;
          foundedToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(tokenData).should.be.true;
          newToken.get.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.investorInvitationCreated.calledOnce.should.be.true;
          userProducer.investorInvitationCreated.calledWithMatch(invitation, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getBalance', () => {
    const fixtures = helperFixtures.getBalance;
    const { request, response, balance, company, adminUser, investorUser, message, guid, query,
      requestType, responseType, commonError, notFoundError, validInvestorRequest,
      validInvestorRequestQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(adminUser));
      sandbox.stub(company, 'getBalance').callsFake(() => Promise.resolve(balance));
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getBalance(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(query).should.be.true;
          company.getBalance.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if the company doesn\'t exists', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(_.omit(adminUser, 'Company')));

      return controller.getBalance(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(query).should.be.true;
          company.getBalance.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return error if the company doesn\'t belong to investor user', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(investorUser));

      return controller.getBalance(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(query).should.be.true;
          company.getBalance.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.getBalance fails', () => {
      const callback = sandbox.spy();

      company.getBalance.restore();
      sandbox.stub(company, 'getBalance').callsFake(() => Promise.reject(commonError));

      return controller.getBalance(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(query).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response if company belongs to investor', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(investorUser));

      return controller.getBalance(validInvestorRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message,
            validInvestorRequest.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(validInvestorRequestQuery).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.getBalance(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(query).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('updateCompanyRoleSuspension', () => {
    const fixtures = helperFixtures.updateCompanyRoleSuspension;
    const { request, company, query, message, guid, requestType, responseType,
      commonError, notFoundError, transaction, role, id, suspended } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(company, 'updateRoleSuspension').callsFake(() => Promise.resolve(company));
      sandbox.stub(Session, 'updateSuspensions').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'companyRoleSuspensionUpdated').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.called.should.be.false;
          company.updateRoleSuspension.called.should.be.false;
          Session.updateSuspensions.called.should.be.false;
          userProducer.companyRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.updateRoleSuspension.called.should.be.false;
          Session.updateSuspensions.called.should.be.false;
          userProducer.companyRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return an empty response', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.updateRoleSuspension.called.should.be.false;
          Session.updateSuspensions.called.should.be.false;
          userProducer.companyRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.updateRoleSuspension return a reject', () => {
      const callback = sandbox.spy();

      company.updateRoleSuspension.restore();
      sandbox.stub(company, 'updateRoleSuspension').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.companyRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Session.updateSuspensions return a reject', () => {
      const callback = sandbox.spy();

      Session.updateSuspensions.restore();
      sandbox.stub(Session, 'updateSuspensions').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.companyRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.companyRoleSuspensionUpdated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.companyRoleSuspensionUpdated.restore();
      sandbox.stub(userProducer, 'companyRoleSuspensionUpdated').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.companyRoleSuspensionUpdated.calledOnce.should.be.true;
          userProducer.companyRoleSuspensionUpdated.calledWith(id, role, suspended, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.updateCompanyRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.companyRoleSuspensionUpdated.calledOnce.should.be.true;
          userProducer.companyRoleSuspensionUpdated.calledWith(id, role, suspended, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, request.request, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, request.request).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('updateInvestorRoleSuspension', () => {
    const fixtures = helperFixtures.updateInvestorRoleSuspension;
    const { request, company, query, message, guid, requestType, responseType,
      commonError, notFoundError, transaction, role, id, suspended } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(company, 'updateRoleSuspension').callsFake(() => Promise.resolve(company));
      sandbox.stub(Session, 'updateSuspensions').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'investorRoleSuspensionUpdated').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.called.should.be.false;
          company.updateRoleSuspension.called.should.be.false;
          Session.updateSuspensions.called.should.be.false;
          userProducer.investorRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.updateRoleSuspension.called.should.be.false;
          Session.updateSuspensions.called.should.be.false;
          userProducer.investorRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return an empty response', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.updateRoleSuspension.called.should.be.false;
          Session.updateSuspensions.called.should.be.false;
          userProducer.investorRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.updateRoleSuspension return a reject', () => {
      const callback = sandbox.spy();

      company.updateRoleSuspension.restore();
      sandbox.stub(company, 'updateRoleSuspension').callsFake(() => Promise.reject(commonError));

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.investorRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Session.updateSuspensions return a reject', () => {
      const callback = sandbox.spy();

      Session.updateSuspensions.restore();
      sandbox.stub(Session, 'updateSuspensions').callsFake(() => Promise.reject(commonError));

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.investorRoleSuspensionUpdated.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.investorRoleSuspensionUpdated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.investorRoleSuspensionUpdated.restore();
      sandbox.stub(userProducer, 'investorRoleSuspensionUpdated').callsFake(() => Promise.reject(commonError));

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.investorRoleSuspensionUpdated.calledOnce.should.be.true;
          userProducer.investorRoleSuspensionUpdated.calledWith(id, suspended, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.updateInvestorRoleSuspension(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          company.updateRoleSuspension.calledOnce.should.be.true;
          company.updateRoleSuspension.calledWith(role, suspended, transaction).should.be.true;
          Session.updateSuspensions.calledOnce.should.be.true;
          Session.updateSuspensions.calledWith(id, role, suspended, transaction).should.be.true;
          userProducer.investorRoleSuspensionUpdated.calledOnce.should.be.true;
          userProducer.investorRoleSuspensionUpdated.calledWith(id, suspended, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, request.request, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, request.request).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('propose', () => {
    const fixtures = helperFixtures.propose;
    const { request, response, message, guid, requestType, responseType, commonError, company, findOneCall } = fixtures;
    const requestCall = request.request;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(userProducer, 'companyProposed').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.propose(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.called.should.be.false;
          userProducer.companyProposed.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne return a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.propose(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneCall).should.be.true;
          userProducer.companyProposed.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if userProducer.companyProposed return a reject', () => {
      const callback = sandbox.spy();

      userProducer.companyProposed.restore();
      sandbox.stub(userProducer, 'companyProposed').callsFake(() => Promise.reject(commonError));

      return controller.propose(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneCall).should.be.true;
          userProducer.companyProposed.calledOnce.should.be.true;
          userProducer.companyProposed.calledWithMatch(requestCall, company.name, requestCall.guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.propose(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneCall).should.be.true;
          userProducer.companyProposed.calledOnce.should.be.true;
          userProducer.companyProposed.calledWithMatch(requestCall, company.name, requestCall.guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getInvestorTransactions', () => {
    const fixtures = helperFixtures.getInvestorTransactions;
    const { request, requestType, message, commonError, investorCashWithDrawals, investorCashDeposits,
      purchasedInvoices, payedInvoices, response, responseMessage, responseType, guid } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(statementQueries, 'purchasedInvoices').resolves(purchasedInvoices);
      sandbox.stub(statementQueries, 'payedInvoices').resolves(payedInvoices);
      sandbox.stub(statementQueries, 'investorCashWithDrawals').resolves(investorCashWithDrawals);
      sandbox.stub(statementQueries, 'investorCashDeposits').resolves(investorCashDeposits);
    });

    it('should return error if statementQueries.purchasedInvoices rejects', () => {
      const callback = sandbox.spy();

      statementQueries.purchasedInvoices.restore();
      sandbox.stub(statementQueries, 'purchasedInvoices').rejects(commonError);

      return controller.getInvestorTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          statementQueries.purchasedInvoices.calledOnce.should.be.true;
          statementQueries.purchasedInvoices.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if statementQueries.payedInvoices rejects', () => {
      const callback = sandbox.spy();

      statementQueries.payedInvoices.restore();
      sandbox.stub(statementQueries, 'payedInvoices').rejects(commonError);

      return controller.getInvestorTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          statementQueries.payedInvoices.calledOnce.should.be.true;
          statementQueries.payedInvoices.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if statementQueries.investorCashWithDrawals rejects', () => {
      const callback = sandbox.spy();

      statementQueries.investorCashWithDrawals.restore();
      sandbox.stub(statementQueries, 'investorCashWithDrawals').rejects(commonError);

      return controller.getInvestorTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          statementQueries.investorCashWithDrawals.calledOnce.should.be.true;
          statementQueries.investorCashWithDrawals.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return error if statementQueries.investorCashDeposits rejects', () => {
      const callback = sandbox.spy();

      statementQueries.investorCashDeposits.restore();
      sandbox.stub(statementQueries, 'investorCashDeposits').rejects(commonError);

      return controller.getInvestorTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          statementQueries.investorCashDeposits.calledOnce.should.be.true;
          statementQueries.investorCashDeposits.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a succesful response', () => {
      const callback = sandbox.spy();

      return controller.getInvestorTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          statementQueries.payedInvoices.calledOnce.should.be.true;
          statementQueries.payedInvoices.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          statementQueries.purchasedInvoices.calledOnce.should.be.true;
          statementQueries.purchasedInvoices.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          statementQueries.investorCashWithDrawals.calledOnce.should.be.true;
          statementQueries.investorCashWithDrawals.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          statementQueries.investorCashDeposits.calledOnce.should.be.true;
          statementQueries.investorCashDeposits.calledWithMatch(request.request.id, request.request.year,
            request.request.month).should.be.true;
          log.message.secondCall.calledWithMatch(responseMessage, response, responseType, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.false;
          callback.calledOnce.should.be.true;
        });
    });
  });

  describe('updateCompanyPrepayment', () => {
    const fixtures = helperFixtures.updateCompanyPrepayment;
    const { request, response, company, query, message, guid, requestType, responseType, prepaymentCompany,
      commonError, notFoundError, updateParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(company, 'update').callsFake(() => Promise.resolve(prepaymentCompany));
    });

    it('should return an error if Company.findOne returns an empty response', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return controller.updateCompanyPrepayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.update.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if Company.findOne returns a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyPrepayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.update.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if company.update returns a reject', () => {
      const callback = sandbox.spy();

      company.update.restore();
      sandbox.stub(company, 'update').callsFake(() => Promise.reject(commonError));

      return controller.updateCompanyPrepayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.update.calledOnce.should.be.true;
          company.update.calledWithMatch(updateParams).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.updateCompanyPrepayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(query).should.be.true;
          company.update.calledOnce.should.be.true;
          company.update.calledWithMatch(updateParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });
});
