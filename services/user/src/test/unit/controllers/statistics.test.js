const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/statistics');
const errorHandler = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/controllers/statistics');
const { Token, InvitationNotification, NewInvoiceNotification } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/StatisticsController', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('checkToken', () => {
    const fixtures = helperFixtures.checkToken;
    const { request, tokenInstance, metricInstance, requestInvalid, metricNoValidError,
      invitationNotificationCreateParams, tokenNotFoundError, findTokenQuery, metricPlain, message,
      guid, requestType, responseType, commonError, plainParams, response, newInvoiceRequest,
      newInvoiceCreateParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(tokenInstance));
      sandbox.stub(InvitationNotification, 'create').resolves(metricInstance);
      sandbox.stub(NewInvoiceNotification, 'create').resolves(metricInstance);
      sandbox.stub(metricInstance, 'get').resolves(metricPlain);
    });


    it('should return error if Token.findOne rejects', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').rejects(commonError);

      return controller.checkToken( { request }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          metricInstance.get.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.guid).should.be.true;
        });
    });

    it('should return error if Token.findOne does not find token', () => {
      const callback = sandbox.spy();

      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').resolves(false);

      return controller.checkToken( { request }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          metricInstance.get.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(tokenNotFoundError, request.guid).should.be.true;
        });
    });

    it('should return error if request has invalid statistics_type', () => {
      const callback = sandbox.spy();

      return controller.checkToken( { request: requestInvalid }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestInvalid, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          metricInstance.get.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(metricNoValidError, requestInvalid.guid).should.be.true;
        });
    });

    it('should return error if InvitationNotification rejects', () => {
      const callback = sandbox.spy();

      InvitationNotification.create.restore();
      sandbox.stub(InvitationNotification, 'create').rejects(commonError);

      return controller.checkToken( { request }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          InvitationNotification.create.calledOnce.should.be.true;
          InvitationNotification.create.calledWithMatch(invitationNotificationCreateParams).should.be.true;
          metricInstance.get.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.guid).should.be.true;
        });
    });

    it('should return error if InvitationNotification newInstance.get() rejects', () => {
      const callback = sandbox.spy();

      metricInstance.get.restore();
      sandbox.stub(metricInstance, 'get').rejects(commonError);

      return controller.checkToken( { request }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          InvitationNotification.create.calledOnce.should.be.true;
          InvitationNotification.create.calledWithMatch(invitationNotificationCreateParams).should.be.true;
          metricInstance.get.calledOnce.should.be.true;
          metricInstance.get.calledWithMatch(plainParams).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.guid).should.be.true;
        });
    });

    it('should return success when statistics type is new invoice notification', () => {
      const callback = sandbox.spy();

      return controller.checkToken( { request: newInvoiceRequest }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, newInvoiceRequest, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          NewInvoiceNotification.create.calledOnce.should.be.true;
          NewInvoiceNotification.create.calledWithMatch(newInvoiceCreateParams).should.be.true;
          metricInstance.get.calledOnce.should.be.true;
          metricInstance.get.calledWithMatch(plainParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.false;
        });
    });

    it('should return success when statistics type is investor invitation', () => {
      const callback = sandbox.spy();

      return controller.checkToken( { request }, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request, requestType, guid).should.be.true;
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(findTokenQuery).should.be.true;
          InvitationNotification.create.calledOnce.should.be.true;
          InvitationNotification.create.calledWithMatch(invitationNotificationCreateParams).should.be.true;
          metricInstance.get.calledOnce.should.be.true;
          metricInstance.get.calledWithMatch(plainParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.false;
        });
    });
  });
});
