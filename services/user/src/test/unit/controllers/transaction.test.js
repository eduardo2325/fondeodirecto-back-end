const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/transaction');
const errorHandler = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/controllers/transaction');

const { sequelize, User, PendingTransaction, Company, Invoice, Transaction } = require('../../../models');
const userProducer = require('../../../api/producers/user');
const stateMachine = require('../../../api/helpers/state-machine');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Transaction controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('withdraw', () => {
    const fixtures = helperFixtures.withdraw;
    const { request, response, message, guid, requestType, responseType, commonError,
      user, company, withdraw, withdrawBasicInfo, userFindOneParams, balance,
      insufficientBalance, insufficientFundsError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(company, 'getBalance').callsFake(() => Promise.resolve(balance));
      sandbox.stub(PendingTransaction, 'withdraw').callsFake(() => Promise.resolve(withdraw));
      sandbox.stub(withdraw, 'getBasicInfo').callsFake(() => withdrawBasicInfo);
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'withdrawCreated').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.called.should.be.false;
          company.getBalance.called.should.be.false;
          PendingTransaction.withdraw.called.should.be.false;
          withdraw.getBasicInfo.called.should.be.false;
          userProducer.withdrawCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          company.getBalance.called.should.be.false;
          PendingTransaction.withdraw.called.should.be.false;
          withdraw.getBasicInfo.called.should.be.false;
          userProducer.withdrawCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if company.getBalance return a reject', () => {
      const callback = sandbox.spy();

      company.getBalance.restore();
      sandbox.stub(company, 'getBalance').callsFake(() => Promise.reject(commonError));

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          PendingTransaction.withdraw.called.should.be.false;
          withdraw.getBasicInfo.called.should.be.false;
          userProducer.withdrawCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if company doesn\'t have enough balance', () => {
      const callback = sandbox.spy();

      company.getBalance.restore();
      sandbox.stub(company, 'getBalance').callsFake(() => Promise.resolve(insufficientBalance));

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          PendingTransaction.withdraw.called.should.be.false;
          withdraw.getBasicInfo.called.should.be.false;
          userProducer.withdrawCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(insufficientFundsError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.withdraw return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.withdraw.restore();
      sandbox.stub(PendingTransaction, 'withdraw').callsFake(() => Promise.reject(commonError));

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          PendingTransaction.withdraw.calledOnce.should.be.true;
          PendingTransaction.withdraw.calledWithMatch(request.request.amount, user.company_id).should.be.true;
          withdraw.getBasicInfo.called.should.be.false;
          userProducer.withdrawCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.withdrawCreated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.withdrawCreated.restore();
      sandbox.stub(userProducer, 'withdrawCreated').callsFake(() => Promise.reject(commonError));

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          PendingTransaction.withdraw.calledOnce.should.be.true;
          PendingTransaction.withdraw.calledWithMatch(request.request.amount, user.company_id).should.be.true;
          withdraw.getBasicInfo.calledOnce.should.be.true;
          userProducer.withdrawCreated.calledOnce.should.be.true;
          userProducer.withdrawCreated.calledWithMatch(withdrawBasicInfo, company, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.withdraw(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          company.getBalance.calledOnce.should.be.true;
          company.getBalance.calledWith().should.be.true;
          PendingTransaction.withdraw.calledOnce.should.be.true;
          PendingTransaction.withdraw.calledWithMatch(request.request.amount, user.company_id).should.be.true;
          withdraw.getBasicInfo.calledOnce.should.be.true;
          userProducer.withdrawCreated.calledOnce.should.be.true;
          userProducer.withdrawCreated.calledWithMatch(withdrawBasicInfo, company, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getTransactions', () => {
    const fixtures = helperFixtures.getTransactions;
    const { request, response, message, guid, requestType, responseType, commonError,
      transactions, basicInfoTransactions, findAllQuery, company, investorCompany, balance,
      findOneQuery, invesorResponse } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve(transactions));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(company, 'getBalance').callsFake(() => Promise.resolve(0));
      sandbox.stub(investorCompany, 'getBalance').callsFake(() => Promise.resolve(balance));
      transactions.forEach(t => {
        const basicInfo = _.find(basicInfoTransactions, { id: t.id });

        sandbox.stub(t, 'getBasicInfo').callsFake(() => Promise.resolve(basicInfo));
      });
    });

    it('should return an error if PendingTransaction.findAll returns a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.findAll.restore();
      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.reject(commonError));

      return controller.getTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(findAllQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneQuery).should.be.true;
          transactions.forEach(t => t.getBasicInfo.called.should.be.false);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Company.findOne returns a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(findAllQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneQuery).should.be.true;
          transactions.forEach(t => t.getBasicInfo.called.should.be.false);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if transaction.getBasicInfo returns a reject', () => {
      const callback = sandbox.spy();

      transactions[0].getBasicInfo.restore();
      sandbox.stub(transactions[0], 'getBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.getTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(findAllQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneQuery).should.be.true;
          transactions.forEach(t => t.getBasicInfo.calledOnce.should.be.true);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(findAllQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneQuery).should.be.true;
          transactions.forEach(t => t.getBasicInfo.calledOnce.should.be.true);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response on investor company', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(investorCompany));

      return controller.getTransactions(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWith(findAllQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(findOneQuery).should.be.true;
          transactions.forEach(t => t.getBasicInfo.calledOnce.should.be.true);
          log.message.secondCall.calledWithMatch(message, invesorResponse, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, invesorResponse).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('deposit', () => {
    const fixtures = helperFixtures.deposit;
    const { request, response, message, guid, requestType, responseType, commonError,
      user, deposit, depositBasicInfo, userFindOneParams, depositParams, eventParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(PendingTransaction, 'deposit').callsFake(() => Promise.resolve(deposit));
      sandbox.stub(deposit, 'getBasicInfo').callsFake(() => depositBasicInfo);
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'depositCreated').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.deposit(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if User.findOne return a reject', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.deposit(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          PendingTransaction.deposit.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.deposit return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.deposit.restore();
      sandbox.stub(PendingTransaction, 'deposit').callsFake(() => Promise.reject(commonError));

      return controller.deposit(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          PendingTransaction.deposit.calledOnce.should.be.true;
          PendingTransaction.deposit.args[0].should.be.eql(depositParams);
          deposit.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.depositCreated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.depositCreated.restore();
      sandbox.stub(userProducer, 'depositCreated').callsFake(() => Promise.reject(commonError));

      return controller.deposit(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          PendingTransaction.deposit.calledOnce.should.be.true;
          PendingTransaction.deposit.args[0].should.be.eql(depositParams);
          deposit.getBasicInfo.calledOnce.should.be.true;
          userProducer.depositCreated.calledOnce.should.be.true;
          userProducer.depositCreated.args[0].should.be.eql(eventParams);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.deposit(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(userFindOneParams).should.be.true;
          PendingTransaction.deposit.calledOnce.should.be.true;
          PendingTransaction.deposit.args[0].should.be.eql(depositParams);
          deposit.getBasicInfo.calledOnce.should.be.true;
          userProducer.depositCreated.calledOnce.should.be.true;
          userProducer.depositCreated.args[0].should.be.eql(eventParams);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('approve', () => {
    const fixtures = helperFixtures.approve;
    const { request, transaction, company, pendingDepositTransaction, notFound,
      message, requestType, responseType, guid, query, companyCall, save, commonError,
      pendingWithdrawTransaction, companyId, pendingHighWithdrawTransaction, insufficientFundsError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(pendingDepositTransaction));
      sandbox.stub(transaction, 'rollback').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(pendingDepositTransaction, 'getBasicInfo').callsFake(
        () => Promise.resolve(pendingDepositTransaction)
      );
      sandbox.stub(pendingDepositTransaction, 'save').callsFake(() => Promise.resolve(pendingDepositTransaction));
      sandbox.stub(pendingWithdrawTransaction, 'getBasicInfo').callsFake(
        () => Promise.resolve(pendingWithdrawTransaction)
      );
      sandbox.stub(pendingWithdrawTransaction, 'save').callsFake(() => Promise.resolve(pendingWithdrawTransaction));
      sandbox.stub(company, 'save').callsFake(() => Promise.resolve(company));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'pendingTransactionApproved').callsFake(() => Promise.resolve());
      sandbox.stub(Transaction, 'formatAndCreate').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection throw a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.called.should.be.false;
          Company.findOne.called.should.be.false;
          company.save.called.should.be.false;
          pendingDepositTransaction.save.called.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.findOne throw a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.called.should.be.false;
          company.save.called.should.be.false;
          pendingDepositTransaction.save.called.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction do not exists', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.called.should.be.false;
          company.save.called.should.be.false;
          pendingDepositTransaction.save.called.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if Company.findOne throw a reject', () => {
      const callback = sandbox.spy();

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.called.should.be.false;
          pendingDepositTransaction.save.called.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if company doesn\'t have enough balance', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(pendingHighWithdrawTransaction));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.called.should.be.false;
          pendingDepositTransaction.save.called.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(insufficientFundsError, guid).should.be.true;
        });
    });

    it('should return an error if company.save throw a reject', () => {
      const callback = sandbox.spy();

      company.save.restore();
      sandbox.stub(company, 'save').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.calledOnce.should.be.true;
          company.save.calledWithMatch({ transaction }).should.be.true;
          pendingDepositTransaction.save.called.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if pendingDepositTransaction.save throw a reject', () => {
      const callback = sandbox.spy();

      pendingDepositTransaction.save.restore();
      sandbox.stub(pendingDepositTransaction, 'save').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.calledOnce.should.be.true;
          company.save.calledWithMatch({ transaction }).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          Transaction.formatAndCreate.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if pendingTransaction.getBasicInfo throw a reject', () => {
      const callback = sandbox.spy();

      pendingDepositTransaction.getBasicInfo.restore();
      sandbox.stub(pendingDepositTransaction, 'getBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.calledOnce.should.be.true;
          company.save.calledWithMatch({ transaction }).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          Transaction.formatAndCreate.calledOnce.should.be.true;
          Transaction.formatAndCreate
            .calledWithMatch(pendingDepositTransaction, company.id, companyId, null, true).should.be.true;
          pendingDepositTransaction.getBasicInfo.calledOnce.should.be.true;
          userProducer.pendingTransactionApproved.called.should.be.false;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.pendingTransactionApproved throw a reject', () => {
      const callback = sandbox.spy();

      userProducer.pendingTransactionApproved.restore();
      sandbox.stub(userProducer, 'pendingTransactionApproved').callsFake(() => Promise.reject(commonError));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.calledOnce.should.be.true;
          company.save.calledWithMatch({ transaction }).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          pendingDepositTransaction.getBasicInfo.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledOnce.should.be.true;
          Transaction.formatAndCreate
            .calledWithMatch(pendingDepositTransaction, company.id, companyId, null, true).should.be.true;
          userProducer.pendingTransactionApproved.calledOnce.should.be.true;
          userProducer.pendingTransactionApproved
            .calledWithMatch(pendingDepositTransaction, company, guid).should.be.true;
          callback.calledOnce.should.be.true;
          transaction.rollback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response and increase the company balance', () => {
      const callback = sandbox.spy();
      const balanceBefore = company.balance;

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.calledOnce.should.be.true;
          company.save.calledWithMatch({ transaction }).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          pendingDepositTransaction.getBasicInfo.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledOnce.should.be.true;
          Transaction.formatAndCreate
            .calledWithMatch(pendingDepositTransaction, company.id, companyId, null, true).should.be.true;
          userProducer.pendingTransactionApproved.calledOnce.should.be.true;
          userProducer.pendingTransactionApproved
            .calledWithMatch(pendingDepositTransaction, company, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, pendingDepositTransaction, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, pendingDepositTransaction).should.be.true;
          company.balance.should.be.eql(balanceBefore + pendingDepositTransaction.amount);
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response and decrease the company balance', () => {
      const callback = sandbox.spy();
      const balanceBefore = company.balance;

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(pendingWithdrawTransaction));

      return controller.approve(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyCall).should.be.true;
          company.save.calledOnce.should.be.true;
          company.save.calledWithMatch({ transaction }).should.be.true;
          pendingWithdrawTransaction.save.calledOnce.should.be.true;
          pendingWithdrawTransaction.save.calledWithMatch(save).should.be.true;
          pendingWithdrawTransaction.getBasicInfo.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledOnce.should.be.true;
          Transaction.formatAndCreate
            .calledWithMatch(pendingWithdrawTransaction, companyId, company.id, null, true).should.be.true;
          userProducer.pendingTransactionApproved.calledOnce.should.be.true;
          userProducer.pendingTransactionApproved
            .calledWithMatch(pendingWithdrawTransaction, company, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message,
            pendingWithdrawTransaction, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, pendingWithdrawTransaction).should.be.true;
          company.balance.should.be.eql(balanceBefore - pendingWithdrawTransaction.amount);
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('reject', () => {
    const fixtures = helperFixtures.reject;
    const { request, pendingDepositTransaction, notFound, reason,
      message, requestType, responseType, guid, query, save, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(pendingDepositTransaction));
      sandbox.stub(pendingDepositTransaction, 'getBasicInfo').callsFake(
        () => Promise.resolve(pendingDepositTransaction)
      );
      sandbox.stub(pendingDepositTransaction, 'save').callsFake(() => Promise.resolve(pendingDepositTransaction));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'pendingTransactionRejected').callsFake(() => Promise.resolve());
    });

    it('should return an error if userProducer.ensureConnection throw a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.called.should.be.false;
          pendingDepositTransaction.save.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.findOne throw a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          pendingDepositTransaction.save.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.findOne return an empty object', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          pendingDepositTransaction.save.called.should.be.false;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, guid).should.be.true;
        });
    });

    it('should return an error if pendingDepositTransaction.save throw a reject', () => {
      const callback = sandbox.spy();

      pendingDepositTransaction.save.restore();
      sandbox.stub(pendingDepositTransaction, 'save').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          pendingDepositTransaction.getBasicInfo.called.should.be.false;
          userProducer.pendingTransactionRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if pendingDepositTransaction.getBasicInfo throw a reject', () => {
      const callback = sandbox.spy();

      pendingDepositTransaction.getBasicInfo.restore();
      sandbox.stub(pendingDepositTransaction, 'getBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          pendingDepositTransaction.getBasicInfo.calledOnce.should.be.true;
          userProducer.pendingTransactionRejected.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.pendingTransactionRejected throw a reject', () => {
      const callback = sandbox.spy();

      userProducer.pendingTransactionRejected.restore();
      sandbox.stub(userProducer, 'pendingTransactionRejected').callsFake(() => Promise.reject(commonError));

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          pendingDepositTransaction.getBasicInfo.calledOnce.should.be.true;
          userProducer.pendingTransactionRejected.calledOnce.should.be.true;
          userProducer.pendingTransactionRejected
            .calledWithMatch(pendingDepositTransaction, reason, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.reject(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(query).should.be.true;
          pendingDepositTransaction.save.calledOnce.should.be.true;
          pendingDepositTransaction.save.calledWithMatch(save).should.be.true;
          pendingDepositTransaction.getBasicInfo.calledOnce.should.be.true;
          userProducer.pendingTransactionRejected.calledOnce.should.be.true;
          userProducer.pendingTransactionRejected
            .calledWithMatch(pendingDepositTransaction, reason, guid).should.be.true;
          log.message.secondCall.calledWithMatch(message, pendingDepositTransaction, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, pendingDepositTransaction).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('createClientInvoicePayment', () => {
    const fixtures = helperFixtures.createClientInvoicePayment;
    const {
      requestAdmin, requestCXP, requestInvalidCXP, response, message, guid,
      requestType, responseType, commonError, invoice, invoiceBasicInfo,
      invoiceFindOneParams, paymentParams, eventParams, notFoundError, transaction
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(PendingTransaction, 'createClientInvoicePayment').callsFake(() => Promise.resolve());
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.resolve());
      sandbox.stub(invoice, 'getGeneralInfo').callsFake(() => Promise.resolve(invoiceBasicInfo));
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.resolve());
      sandbox.stub(userProducer, 'clientInvoicePaymentCreated').callsFake(() => Promise.resolve());
    });

    it('should return an error if sequelize.transaction return a reject', () => {
      const callback = sandbox.spy();

      sequelize.transaction.restore();
      sandbox.stub(sequelize, 'transaction').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.called.should.be.false;
          Invoice.findOne.called.should.be.false;
          stateMachine.next.called.should.be.false;
          PendingTransaction.createClientInvoicePayment.called.should.be.false;
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.ensureConnection return a reject', () => {
      const callback = sandbox.spy();

      userProducer.ensureConnection.restore();
      sandbox.stub(userProducer, 'ensureConnection').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.called.should.be.false;
          stateMachine.next.called.should.be.false;
          PendingTransaction.createClientInvoicePayment.called.should.be.false;
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.called.should.be.false;
          stateMachine.next.called.should.be.false;
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice does not exist', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.called.should.be.false;
          stateMachine.next.called.should.be.false;
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if user is not admin and invoice does not belong to cxp user', () => {
      const callback = sandbox.spy();

      return controller.createClientInvoicePayment(requestInvalidCXP, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestInvalidCXP.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.called.should.be.false;
          stateMachine.next.called.should.be.false;
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFoundError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.createClientInvoicePayment return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.createClientInvoicePayment.restore();
      sandbox.stub(PendingTransaction, 'createClientInvoicePayment').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.args[0].should.be.eql(paymentParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation, null, transaction ]);
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if stateMachine.next return a reject', () => {
      const callback = sandbox.spy();

      stateMachine.next.restore();
      sandbox.stub(stateMachine, 'next').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.args[0].should.be.eql(paymentParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation, null, transaction ]);
          invoice.getGeneralInfo.called.should.be.false;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice.getGeneralInfo return a reject', () => {
      const callback = sandbox.spy();

      invoice.getGeneralInfo.restore();
      sandbox.stub(invoice, 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.args[0].should.be.eql(paymentParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation, null, transaction ]);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledWith(invoice.Client, 'client').should.be.true;
          userProducer.clientInvoicePaymentCreated.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if userProducer.clientInvoicePaymentCreated return a reject', () => {
      const callback = sandbox.spy();

      userProducer.clientInvoicePaymentCreated.restore();
      sandbox.stub(userProducer, 'clientInvoicePaymentCreated').callsFake(() => Promise.reject(commonError));

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.createClientInvoicePayment.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.args[0].should.be.eql(paymentParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation, null, transaction ]);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledWith(invoice.Client, 'client').should.be.true;
          userProducer.clientInvoicePaymentCreated.calledOnce.should.be.true;
          userProducer.clientInvoicePaymentCreated.args[0].should.be.eql(eventParams);
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response as cxp', () => {
      const callback = sandbox.spy();

      return controller.createClientInvoicePayment(requestCXP, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestCXP.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.args[0].should.be.eql(paymentParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation, null, transaction ]);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledWith(invoice.Client, 'client').should.be.true;
          userProducer.clientInvoicePaymentCreated.calledOnce.should.be.true;
          userProducer.clientInvoicePaymentCreated.args[0].should.be.eql(eventParams);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response as admin', () => {
      const callback = sandbox.spy();

      return controller.createClientInvoicePayment(requestAdmin, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, requestAdmin.request, requestType, guid).should.be.true;
          sequelize.transaction.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          userProducer.ensureConnection.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.calledOnce.should.be.true;
          PendingTransaction.createClientInvoicePayment.args[0].should.be.eql(paymentParams);
          stateMachine.next.calledOnce.should.be.true;
          stateMachine.next.args[0].should.be.eql([ null, invoice.Operation, null, transaction ]);
          invoice.getGeneralInfo.calledOnce.should.be.true;
          invoice.getGeneralInfo.calledWith(invoice.Client, 'client').should.be.true;
          userProducer.clientInvoicePaymentCreated.calledOnce.should.be.true;
          userProducer.clientInvoicePaymentCreated.args[0].should.be.eql(eventParams);
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getClientInvoicePayment', () => {
    const fixtures = helperFixtures.getClientInvoicePayment;
    const {
      request, response, message, guid, requestType, responseType, commonError,
      invoice, payment, paymentBasicInfo, pendingTransactionFindOneParams, invoiceFindOneParams,
      invoiceNotFoundError, paymentNotFoundError
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(payment));
      sandbox.stub(payment, 'getBasicInfo').callsFake(() => paymentBasicInfo);
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getClientInvoicePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.findOne.called.should.be.false;
          payment.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if invoice does not exist', () => {
      const callback = sandbox.spy();

      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return controller.getClientInvoicePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.findOne.called.should.be.false;
          payment.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(invoiceNotFoundError, guid).should.be.true;
        });
    });

    it('should return an error if PendingTransaction.findOne return a reject', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getClientInvoicePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(pendingTransactionFindOneParams).should.be.true;
          payment.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if payment does not exist', () => {
      const callback = sandbox.spy();

      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return controller.getClientInvoicePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(pendingTransactionFindOneParams).should.be.true;
          payment.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(paymentNotFoundError, guid).should.be.true;
        });
    });

    it('should return an error if payment.getBasicInfo return a reject', () => {
      const callback = sandbox.spy();

      payment.getBasicInfo.restore();
      sandbox.stub(payment, 'getBasicInfo').callsFake(() => Promise.reject(commonError));

      return controller.getClientInvoicePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(pendingTransactionFindOneParams).should.be.true;
          payment.getBasicInfo.calledOnce.should.be.true;
          payment.getBasicInfo.calledWith().should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response as admin', () => {
      const callback = sandbox.spy();

      return controller.getClientInvoicePayment(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWithMatch(invoiceFindOneParams).should.be.true;
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.calledWithMatch(pendingTransactionFindOneParams).should.be.true;
          payment.getBasicInfo.calledOnce.should.be.true;
          payment.getBasicInfo.calledWith().should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });
});
