const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/operation');
const errorHandler = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/controllers/operation');

const { Operation, User, Invoice } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Operation controller', () => {
  afterEach(() => {
    sandbox.restore();
  });


  describe('getOperations', () => {
    const fixtures = helperFixtures.getOperations;
    const { request, response, message, guid, requestType, responseType, operations, requestPayedOperations,
      commonError, cxcUser, cxpUser, includeCxc, includeCxp, getOperationsOptions,
      getPayedOperationsOptions, includeCxpPayed } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Operation, 'getOperations').callsFake(() => Promise.resolve(operations));
      sandbox.stub(Operation, 'count').callsFake(() => Promise.resolve(4));
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.resolve(4));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxcUser));
    });


    it('should return an error if Operation.getOperations returns a reject', () => {
      const callback = sandbox.spy();

      Operation.getOperations.restore();
      sandbox.stub(Operation, 'getOperations').callsFake(() => Promise.reject(commonError));

      return controller.getOperations(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.getOperations.calledOnce.should.be.true;
          Operation.getOperations.calledWithMatch(includeCxc, request.request, cxcUser).should.be.true;
          Operation.count.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if Operation.count returns a reject', () => {
      const callback = sandbox.spy();

      Operation.count.restore();
      sandbox.stub(Operation, 'count').callsFake(() => Promise.reject(commonError));

      return controller.getOperations(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.getOperations.calledOnce.should.be.true;
          Operation.getOperations.calledWithMatch(includeCxc, request.request, cxcUser).should.be.true;
          Operation.count.calledOnce.should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an successful response with cxcUser', () => {
      const callback = sandbox.spy();

      return controller.getOperations(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.getOperations.calledOnce.should.be.true;
          Operation.getOperations.calledWithMatch(includeCxc, request.request, cxcUser).should.be.true;
          Operation.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response with payed operations as cxp user', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxpUser));

      return controller.getOperations(requestPayedOperations, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.getOperations.calledOnce.should.be.true;
          Operation.getOperations.calledWithMatch(includeCxpPayed, requestPayedOperations.request, cxpUser,
            getPayedOperationsOptions).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response with cxpUser', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxpUser));

      return controller.getOperations(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.getOperations.calledOnce.should.be.true;
          Operation.getOperations.calledWithMatch(includeCxp, request.request, cxpUser,
            getOperationsOptions).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });

  describe('getOperation', () => {
    const fixtures = helperFixtures.getOperation;
    const { request, response, message, guid, requestType, responseType, notFound, commonError,
      operation, operationBasicInfo, whereOperation, invoiceElement, cxcUser, cxpUser, invoice
      , anotherUser } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Operation, 'findOne').callsFake(() => Promise.resolve(operation));
      sandbox.stub(operation, 'getInvoice').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(operation, 'getBasicInfo').callsFake(() => Promise.resolve(operationBasicInfo));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxcUser));
      sandbox.stub(invoice, 'getBasicInfo').callsFake(() => Promise.resolve(invoiceElement));
      sandbox.stub(invoice, 'getBasicInfoWithClient').callsFake(() => Promise.resolve(invoiceElement));
      sandbox.stub(invoice, 'getClient').callsFake( () => Promise.resolve() );
      sandbox.stub(invoice, 'getCompany').callsFake( () => Promise.resolve() );
    });

    it('should return an error if Operation.findOne return a reject', () => {
      const callback = sandbox.spy();

      Operation.findOne.restore();
      sandbox.stub(Operation, 'findOne').callsFake(() => Promise.reject(commonError));

      return controller.getOperation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.findOne.calledOnce.should.be.true;
          Operation.findOne.calledWithMatch(whereOperation).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return an error if operation does not exists', () => {
      const callback = sandbox.spy();

      Operation.findOne.restore();
      sandbox.stub(Operation, 'findOne').callsFake(() => Promise.resolve(false));

      return controller.getOperation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.findOne.calledOnce.should.be.true;
          Operation.findOne.calledWithMatch(whereOperation).should.be.true;
          invoice.getBasicInfo.called.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if invoice.getBasicInfo returns false', () => {
      const callback = sandbox.spy();

      invoice.getBasicInfo.restore();
      sandbox.stub(invoice, 'getBasicInfo').callsFake(() => Promise.resolve(false));
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(cxpUser));

      return controller.getOperation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.findOne.calledOnce.should.be.true;
          Operation.findOne.calledWithMatch(whereOperation).should.be.true;
          invoice.getBasicInfo.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an error if invoiceDoesBelongToUser rejects', () => {
      const callback = sandbox.spy();

      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(anotherUser));

      return controller.getOperation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.findOne.calledOnce.should.be.true;
          Operation.findOne.calledWithMatch(whereOperation).should.be.true;
          invoice.getBasicInfoWithClient.calledOnce.should.be.false;
          invoice.getBasicInfo.calledOnce.should.be.false;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(notFound, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getOperation(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWithMatch(message, request.request, requestType, guid).should.be.true;
          Operation.findOne.calledOnce.should.be.true;
          Operation.findOne.calledWithMatch(whereOperation).should.be.true;
          invoice.getBasicInfoWithClient.calledOnce.should.be.true;
          invoice.getBasicInfo.calledOnce.should.be.false;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });
  describe('getAdminOperations', () => {
    const fixtures = helperFixtures.getAdminOperations;
    const {
      allParamsRequest, noPaginationParamsRequest, emptyStatusRequest, response,
      responseDefaultPagination, message, guid, requestType, responseType, adminOperations,
      commonError, allParams, noPaginationParams, nullStatusParams
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Operation, 'getAdminOperations').callsFake(() => Promise.resolve(adminOperations));
    });

    it('should return an error if Operation.getAdminOperations return a reject', () => {
      const callback = sandbox.spy();

      Operation.getAdminOperations.restore();
      sandbox.stub(Operation, 'getAdminOperations').callsFake(() => Promise.reject(commonError));

      return controller.getAdminOperations(allParamsRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWithMatch(message, allParamsRequest.request, requestType, guid).should.be.true;
          Operation.getAdminOperations.calledOnce.should.be.true;
          Operation.getAdminOperations.calledWithMatch(allParams).should.be.true;
          callback.calledOnce.should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
        });
    });

    it('should return add default pagination parameters', () => {
      const callback = sandbox.spy();

      return controller.getAdminOperations(noPaginationParamsRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall
            .calledWithMatch(message, noPaginationParamsRequest.request, requestType, guid).should.be.true;
          Operation.getAdminOperations.calledOnce.should.be.true;
          Operation.getAdminOperations.calledWithMatch(noPaginationParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, responseDefaultPagination, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, responseDefaultPagination).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should set status to null if provided an empty status array', () => {
      const callback = sandbox.spy();

      return controller.getAdminOperations(emptyStatusRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall
            .calledWithMatch(message, emptyStatusRequest.request, requestType, guid).should.be.true;
          Operation.getAdminOperations.calledOnce.should.be.true;
          Operation.getAdminOperations.calledWithMatch(nullStatusParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });

    it('should return an successful response', () => {
      const callback = sandbox.spy();

      return controller.getAdminOperations(allParamsRequest, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;

          log.message.firstCall.calledWithMatch(message, allParamsRequest.request, requestType, guid).should.be.true;
          Operation.getAdminOperations.calledOnce.should.be.true;
          Operation.getAdminOperations.calledWithMatch(allParams).should.be.true;
          log.message.secondCall.calledWithMatch(message, response, responseType, guid).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWithMatch(null, response).should.be.true;
          errorHandler.format.called.should.be.false;
        });
    });
  });
});
