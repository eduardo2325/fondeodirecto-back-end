const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const controller = require('../../../api/controllers/session');
const errorHandler = require('../../../api/helpers/error');
const { Agreement, Session } = require('../../../models');
const helperFixtures = require('../fixtures/controllers/session');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Session controller', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('checkToken', () => {
    const fixtures = helperFixtures.checkToken;
    const { request, response, session, message, requestType, responseType, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Session, 'checkToken').callsFake(() => Promise.resolve(session));
    });

    it('should return an error if Session.checkToken return a reject', () => {
      const callback = sandbox.spy();

      Session.checkToken.restore();
      sandbox.stub(Session, 'checkToken').callsFake(() => Promise.reject(commonError));

      return controller.checkToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.checkToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          log.message.secondCall.calledWith(message, response, responseType, request.request.guid)
            .should.be.true;
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
        });
    });
  });

  describe('deleteToken', () => {
    const fixtures = helperFixtures.deleteToken;
    const { request, response, sessionCall, guid, message, requestType, responseType, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Session, 'destroy').callsFake(() => Promise.resolve());
    });

    it('should return error if Session.destroy return a rejected', () => {
      const callback = sandbox.spy();

      Session.destroy.restore();
      sandbox.stub(Session, 'destroy').callsFake(() => Promise.reject(commonError));

      return controller.deleteToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith(message, request.request, requestType, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          Session.destroy.calledOnce.should.be.true;
          Session.destroy.calledWith(sessionCall).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.deleteToken(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          log.message.secondCall.calledWith(message, response, responseType, request.request.guid)
            .should.be.true;
          errorHandler.format.called.should.be.false;
          Session.destroy.calledOnce.should.be.true;
          Session.destroy.calledWith(sessionCall).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
        });
    });
  });

  describe('verifyAndCreate', () => {
    const fixtures = helperFixtures.verifyAndCreate;
    const { request, response, sessionResponse, message, requestType, responseType, error } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Session, 'verifyAndCreate').callsFake(() => Promise.resolve(sessionResponse));
      sandbox.stub(Agreement, 'previewContract').callsFake(() => Promise.resolve('SOME_S3_URL'));
    });

    it('should return error if Session.verifyAndCreate return a rejected', () => {
      const callback = sandbox.spy();

      Session.verifyAndCreate.restore();
      sandbox.stub(Session, 'verifyAndCreate').callsFake(() => Promise.reject(error));

      return controller.verifyAndCreate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(error, request.request.guid).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.verifyAndCreate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          log.message.secondCall.calledWith(message, response, responseType, request.request.guid).should.be.true;
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
        });
    });

    it('should call Agreement.previewContract if role is INVESTOR or CXC', () => {
      const { fixedResponse, fixedSession, fixedParams } = fixtures.makePreview('INVESTOR');
      const callback = sandbox.spy();

      Session.verifyAndCreate.restore();
      sandbox.stub(Session, 'verifyAndCreate').callsFake(() => Promise.resolve(fixedSession));

      return controller.verifyAndCreate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          log.message.secondCall.calledWith(message, fixedResponse, responseType, request.request.guid).should.be.true;
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.calledWith(null, fixedResponse).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.firstCall.args.should.be.deep.equal([
            fixedSession.agreement,
            fixedParams,
            request.request
          ]);
        });
    });

    it('should not concatenate the Company.taxpayer_type to the template if role is CXC/CXP', () => {
      const { fixedResponse, fixedSession, fixedParams } = fixtures.makePreview('CXP', { template: 'cxp' });
      const callback = sandbox.spy();

      Session.verifyAndCreate.restore();
      sandbox.stub(Session, 'verifyAndCreate').callsFake(() => Promise.resolve(fixedSession));

      return controller.verifyAndCreate(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          log.message.secondCall.calledWith(message, fixedResponse, responseType, request.request.guid).should.be.true;
          errorHandler.format.called.should.be.false;
          callback.calledOnce.should.be.true;
          callback.calledWith(null, fixedResponse).should.be.true;
          Agreement.previewContract.calledOnce.should.be.true;
          Agreement.previewContract.firstCall.args.should.be.deep.equal([
            fixedSession.agreement,
            fixedParams,
            request.request
          ]);
        });
    });
  });

  describe('me', () => {
    const fixtures = helperFixtures.me;
    const { request, response, token, guid, message, requestType,
      responseType, commonError, sessionData } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(errorHandler, 'format').callsFake((e, g, callback) => Promise.resolve(callback()));
      sandbox.stub(Session, 'getInformation').callsFake(() => Promise.resolve(sessionData));
    });

    it('should return error if Session.getInformation return a rejected', () => {
      const callback = sandbox.spy();

      Session.getInformation.restore();
      sandbox.stub(Session, 'getInformation').callsFake(() => Promise.reject(commonError));

      return controller.me(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.calledWith(message, request.request, requestType, guid).should.be.true;
          errorHandler.format.calledOnce.should.be.true;
          errorHandler.format.calledWithMatch(commonError, guid).should.be.true;
          Session.getInformation.calledOnce.should.be.true;
          Session.getInformation.calledWith(token).should.be.true;
          callback.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return controller.me(request, callback)
        .should.be.fulfilled
        .then(() => {
          log.message.calledTwice.should.be.true;
          log.message.firstCall.calledWith(message, request.request, requestType, request.request.guid).should.be.true;
          log.message.secondCall.calledWith(message, response, responseType, request.request.guid)
            .should.be.true;
          errorHandler.format.called.should.be.false;
          Session.getInformation.calledOnce.should.be.true;
          Session.getInformation.calledWith(token).should.be.true;
          callback.calledOnce.should.be.true;
          callback.calledWith(null, response).should.be.true;
        });
    });
  });
});
