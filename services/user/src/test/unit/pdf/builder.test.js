const pdf = require('../../../pdf/builder');

const htmlPdf = require('html-pdf');
const td = require('testdouble');

const IF_OK = Symbol('IT_SHALL_NOT_PASS');

function toBufferCallback(error, returnValue) {
  return cb => cb(error, returnValue);
}

function shouldFail(result) {
  result.message.should.be.equal('Invalid input');
}

describe('PDF builder', () => {
  let createCallback;

  beforeEach(() => {
    createCallback = td.func('html-pdf.create');
    td.replace(htmlPdf, 'create', createCallback);
  });

  afterEach(() => {
    td.reset();
  });

  it('should fail if html is not a string or buffer', () => {
    return pdf.build().should.be.rejected.then(shouldFail);
  });

  it('should fail if options are invalid', () => {
    return pdf.build('', NaN).should.be.rejected.then(shouldFail);
  });

  it('should fail if toBuffer fails too', () => {
    td.when(htmlPdf.create(td.matchers.isA(String), td.matchers.isA(Object)))
      .thenReturn({ toBuffer: toBufferCallback(new Error('Oops!')) });

    return pdf.build('', {})
      .should.be.rejected
      .then(result => {
        result.message.should.be.equal('Oops!');
      });
  });

  it('should handle simple if-blocks', () => {
    const template = '{#foo}[baz]{/foo}{#bar}NOTHING{/bar}';
    const data = {
      foo: true,
      bar: false,
      baz: 'VALUE'
    };

    pdf.preview(template, data).should.be.equal('VALUE');
  });

  it('should generate a pdf otherwise', () => {
    td.when(htmlPdf.create(td.matchers.isA(String), td.matchers.isA(Object)))
      .thenReturn({ toBuffer: toBufferCallback(null, IF_OK) });

    const data = {
      'Your name': 'FondeoDirecto'
    };

    const html = pdf.preview(pdf.getTemplate('dummy'), data);
    const opts = pdf.getOptions();

    return pdf.build(html, opts)
      .should.be.fulfilled
      .then(result => {
        result.should.be.equal(IF_OK);
        createCallback = td.explain(createCallback);
        createCallback.calls[0].args.should.be.deep.equal([
          '<p>Hi, <em style="color:red">FONDEODIRECTO</em>!</p>\n',
          opts
        ]);
      });
  });
});
