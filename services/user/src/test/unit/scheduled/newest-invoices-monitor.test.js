const chai = require('chai');
const assert = chai.assert;
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const scheduled = require('../../../scheduled/newest-invoices-monitor');
const userProducer = require('../../../api/producers/user');
const helperFixtures = require('../fixtures/scheduled/newest-invoices-monitor');

const uuid = require('uuid');
const log = require('/var/lib/core/js/log').prototype;
const { Invoice } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/New invoices monitor', () => {
  const fixtures = helperFixtures.randomizeBehavior;
  const {
    invoices, guid, startedLogParams, finishedLogParams, invalidHour, validHour
  } = fixtures;
  let stub;

  beforeEach(() => {
    stub = sandbox.stub(Date.prototype, 'toLocaleTimeString').returns(validHour);
    sandbox.stub(log, 'message').callsFake(() => true);
    sandbox.stub(log, 'error').callsFake(() => true);
    sandbox.stub(uuid, 'v4').callsFake(() => guid);
    sandbox.stub(Invoice, 'getNewestInvoices').callsFake(() => Promise.resolve(invoices));
    sandbox.stub(userProducer, 'notifyNewInvoices').callsFake(() => Promise.resolve());
  });

  afterEach(() => {
    sandbox.restore();
    stub.restore();
  });

  it('should fail at producing the event', () => {
    stub.restore();
    stub = sandbox.stub(Date.prototype, 'toLocaleTimeString').returns(invalidHour);

    return scheduled()
      .should.be.fulfilled
      .then((response) => {
        log.message.calledOnce.should.be.false;
        uuid.v4.calledOnce.should.be.false;
        Invoice.getNewestInvoices.calledOnce.should.be.false;
        userProducer.notifyNewInvoices.calledOnce.should.be.false;
        assert.deepEqual(response, false);
      });
  });

  it('should produce event correctly', () => {
    return scheduled()
      .should.be.fulfilled
      .then((response) => {
        uuid.v4.calledOnce.should.be.true;
        uuid.v4.calledWith().should.be.true;
        Invoice.getNewestInvoices.calledOnce.should.be.true;
        userProducer.notifyNewInvoices.calledOnce.should.be.true;
        userProducer.notifyNewInvoices.args[0].should.be.eql([ invoices ]);
        log.message.calledTwice.should.be.true;
        log.message.args[0].should.be.eql(startedLogParams);
        log.message.args[1].should.be.eql(finishedLogParams);
        log.error.called.should.be.false;
        assert.deepEqual(response, true);
      });
  });
});
