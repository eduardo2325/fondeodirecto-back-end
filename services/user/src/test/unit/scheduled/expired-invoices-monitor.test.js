const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const scheduled = require('../../../scheduled/expired-invoices-monitor');
const userProducer = require('../../../api/producers/user');
const helperFixtures = require('../fixtures/scheduled/expired-invoices-monitor');

const uuid = require('uuid');
const log = require('/var/lib/core/js/log').prototype;
const { Invoice, sequelize } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Expired invoice monitor', () => {
  const fixtures = helperFixtures.pendingTransactionRejected;
  const {
    invoices, commonError, guid, operationStatus, transaction,
    startedLogParams, finishedLogParams, failedLogParams, invoiceStatus
  } = fixtures;
  let invoicesClone = [];

  beforeEach(() => {
    invoicesClone = _.cloneDeep(invoices);

    sandbox.stub(log, 'message').callsFake(() => true);
    sandbox.stub(log, 'error').callsFake(() => true);
    sandbox.stub(uuid, 'v4').callsFake(() => guid);
    sandbox.stub(sequelize, 'transaction').callsFake(cb => cb(transaction));
    sandbox.stub(Invoice, 'getExpirableInvoices').callsFake(() => Promise.resolve(invoicesClone));
    sandbox.stub(userProducer, 'invoiceExpired').callsFake(() => Promise.resolve());
    invoicesClone.forEach(invoice => sandbox.stub(invoice, 'save').callsFake(() => Promise.resolve(invoice)));
    invoicesClone.forEach(invoice => sandbox.stub(invoice.Operation, 'save').callsFake(() =>
      Promise.resolve(invoice.Operation)));
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should log error if sequelize.transaction fails', () => {
    sequelize.transaction.restore();
    sandbox.stub(sequelize, 'transaction').callsFake(() => Promise.reject(commonError));

    return scheduled()
      .should.be.fulfilled
      .then(() => {
        uuid.v4.calledOnce.should.be.true;
        uuid.v4.calledWith().should.be.true;
        sequelize.transaction.calledOnce.should.be.true;
        Invoice.getExpirableInvoices.called.should.be.false;
        invoicesClone.forEach(invoice => {
          invoice.Operation.status.should.not.be.equal(operationStatus);
          invoice.status.should.not.be.equal(invoiceStatus);
          invoice.save.called.should.be.false;
        });
        userProducer.invoiceExpired.called.should.be.false;
        log.message.calledOnce.should.be.true;
        log.message.args[0].should.be.eql(startedLogParams);
        log.error.calledOnce.should.be.true;
        log.error.args[0].should.be.eql(failedLogParams);
      });
  });

  it('should log error if Invoice.getExpirableInvoices fails', () => {
    Invoice.getExpirableInvoices.restore();
    sandbox.stub(Invoice, 'getExpirableInvoices').callsFake(() => Promise.reject(commonError));

    return scheduled()
      .should.be.fulfilled
      .then(() => {
        uuid.v4.calledOnce.should.be.true;
        uuid.v4.calledWith().should.be.true;
        sequelize.transaction.calledOnce.should.be.true;
        Invoice.getExpirableInvoices.calledOnce.should.be.true;
        invoicesClone.forEach(invoice => {
          invoice.Operation.status.should.not.be.equal(operationStatus);
          invoice.status.should.not.be.equal(invoiceStatus);
          invoice.save.called.should.be.false;
        });
        userProducer.invoiceExpired.called.should.be.false;
        log.message.calledOnce.should.be.true;
        log.message.args[0].should.be.eql(startedLogParams);
        log.error.calledOnce.should.be.true;
        log.error.args[0].should.be.eql(failedLogParams);
      });
  });

  it('should log error if invoice.Operation.save fails', () => {
    invoicesClone[0].Operation.save.restore();
    invoicesClone[1].Operation.save.restore();
    sandbox.stub(invoicesClone[0].Operation, 'save').callsFake(() => Promise.reject(commonError));
    sandbox.stub(invoicesClone[1].Operation, 'save').callsFake(() => Promise.reject(commonError));

    return scheduled()
      .should.be.fulfilled
      .then(() => {
        uuid.v4.calledOnce.should.be.true;
        uuid.v4.calledWith().should.be.true;
        sequelize.transaction.calledOnce.should.be.true;
        Invoice.getExpirableInvoices.calledOnce.should.be.true;
        invoicesClone.forEach(invoice => {
          invoice.Operation.save.calledOnce.should.be.true;
        });
        userProducer.invoiceExpired.called.should.be.false;
        log.message.calledOnce.should.be.true;
        log.message.args[0].should.be.eql(startedLogParams);
        log.error.calledOnce.should.be.true;
        log.error.args[0].should.be.eql(failedLogParams);
      });
  });

  it('should log error if userProducer.invoiceExpired fails', () => {
    userProducer.invoiceExpired.restore();
    sandbox.stub(userProducer, 'invoiceExpired').callsFake(() => Promise.reject(commonError));

    return scheduled()
      .should.be.fulfilled
      .then(() => {
        uuid.v4.calledOnce.should.be.true;
        uuid.v4.calledWith().should.be.true;
        sequelize.transaction.calledOnce.should.be.true;
        Invoice.getExpirableInvoices.calledOnce.should.be.true;
        invoicesClone.forEach((invoice) => {
          invoice.Operation.status.should.be.equal(operationStatus);
          invoice.Operation.save.calledOnce.should.be.true;
          invoice.Operation.save.calledWith({ transaction }).should.be.true;
          userProducer.invoiceExpired.calledWith([ invoice, invoice.Operation ]).should.be.true;
        });
        userProducer.invoiceExpired.args.length.should.be.equal(invoicesClone.length);
        log.message.calledOnce.should.be.true;
        log.message.args[0].should.be.eql(startedLogParams);
        log.error.calledOnce.should.be.true;
        log.error.args[0].should.be.eql(failedLogParams);
      });
  });

  it('should produce event correctly', () => {
    return scheduled()
      .should.be.fulfilled
      .then(() => {
        uuid.v4.calledOnce.should.be.true;
        uuid.v4.calledWith().should.be.true;
        sequelize.transaction.calledOnce.should.be.true;
        Invoice.getExpirableInvoices.calledOnce.should.be.true;
        invoicesClone.forEach(invoice => {
          invoice.Operation.status.should.be.equal(operationStatus);
          invoice.Operation.save.calledOnce.should.be.true;
          invoice.Operation.save.calledWith({ transaction }).should.be.true;
          userProducer.invoiceExpired.calledWith([ invoice, invoice.Operation ]).should.be.true;
        });
        userProducer.invoiceExpired.args.length.should.be.equal(invoicesClone.length);
        log.message.calledTwice.should.be.true;
        log.message.args[0].should.be.eql(startedLogParams);
        log.message.args[1].should.be.eql(finishedLogParams);
        log.error.called.should.be.false;
      });
  });
});
