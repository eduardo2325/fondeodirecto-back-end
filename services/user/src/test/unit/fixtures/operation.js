const { commonError } = require('../../common/fixtures/errors');
const { Company, Operation, sequelize, Invoice } = require('../../../models');

const rfc = 'FODA900806965';
const recipientRfc = rfc + 'diff';
const issuerRfc = rfc;
const companyId = 1;
const clientCompanyId = 2;
const clientCompany = {
  rfc: issuerRfc,
  name: 'Client Company',
  color: '#FFFFF'
};
const company = {
  rfc: recipientRfc,
  name: 'Company',
  color: '#000000'
};
const expiration = new Date();
const today = new Date();
const todayEnd = new Date();
const expirationEnd = new Date();
const sixtyDays = new Date();
const date = new Date();

date.setHours(0, 0, 0, 0);
today.setHours(0, 0, 0, 0);
todayEnd.setHours(23, 59, 59, 59);
const total = 200.99;

const cxcUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXC',
  company_rfc: rfc
};
const cxpUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXP',
  company_rfc: rfc
};
const invoice = {
  id: 1,
  getGeneralInfo() {
    return this;
  },
  getGeneralInfoAsAdmin() {
    return this;
  },
  getInvestorFundEstimate() {
    return 0;
  },
  getGeneralInfoWithOperation() {
    return this;
  },
  created_at: date,
  expiration,
  company_id: companyId,
  client_company_id: companyId,
  Client: clientCompany,
  Company: company,
  total: 330,
  status: 'PENDING'
};
const invoiceElement = {
  id: 1,
  status: 'PUBLISHED'
};
const operation = {
  invoice_id: 1,
  expiration_date: sixtyDays,
  published_date: today,
  annual_cost_percentage: '15.00',
  reserve_percentage: '10.00',
  fd_commission_percentage: '0.50',
  annual_cost: '1256.10',
  reserve: '837.40',
  fd_commission: '41.87',
  factorable: '7536.60',
  subtotal: '7330.74',
  fund_payment: '7323.88',
  commission: '6.86',
  status: 'FUNDED',
  fund_total: '8161.28',
  operation_cost: '212.72',
  formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
              ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
  user_id: 2,
  days_limit: 59,
  getGeneralInfoAsAdmin() {
    return this;
  },
  getGeneralInfo() {
    return this;
  },
  invoice
};
const operationElement = {
  invoice_id: 1,
  expiration_date: sixtyDays,
  published_date: today,
  annual_cost_percentage: 15.00,
  reserve_percentage: 10.00,
  fd_commission_percentage: 0.50,
  annual_cost: 1256.10,
  reserve: 837.40,
  fd_commission: 41.87,
  factorable: 7536.60,
  subtotal: 7330.74,
  fund_payment: 7323.88,
  commission: 6.86,
  fund_total: 8161.28,
  operation_cost: 212.72,
  formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
              ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
  user_id: 2,
  days_limit: 59,
  invoice
};
const invoiceInclude = [
  {
    model: Company,
    as: 'Client',
    where: { name: { $iLike: '%client_name%' } },
    required: true
  },
  {
    model: Company,
    as: 'Company',
    where: { name: { $iLike: '%company_name%' } },
    required: true
  },
  {
    model: Company,
    as: 'Investor',
    where: { name: { $iLike: '%investor_name%' } },
    required: true
  }
];

const include = [ {
  model: Invoice,
  include: invoiceInclude,
  required: true,
  attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
  where: {
    expiration: { $lte: expirationEnd, $gte: expiration },
    fund_date: { $lte: todayEnd, $gte: today }
  }
} ];

const allParams = {
  limit: 25,
  offset: 1,
  order_by: 'client_name',
  order_desc: true,
  client_name: 'client_name',
  company_name: 'company_name',
  investor_name: 'investor_name',
  status: [ 'approved', 'published' ],
  start_fund_date: today.toString(),
  end_fund_date: todayEnd.toString(),
  start_expiration_date: expiration.toString(),
  end_expiration_date: expirationEnd.toString()
};

const allParamsOrderByCompanyName = Object.assign({}, allParams);

allParamsOrderByCompanyName.order_by = 'company_name';

const allParamsOrderByInvestorName = Object.assign({}, allParams);

allParamsOrderByInvestorName.order_by = 'investor_name';

const allParamsOrderByStatus = Object.assign({}, allParams);

allParamsOrderByStatus.order_by = 'operation_status';

module.exports = {
  getOperationsModel: {
    findAll: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [ { all: true } ]
    },
    findAllWithAttributes: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [ { all: true } ],
      offset: 2,
      limit: 2,
      order: [ [ 'number', 'DESC' ] ]
    },
    findAllWithWhere: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [ { all: true } ],
      where: {
        company_rfc: rfc
      },
      offset: 2,
      limit: 2,
      order: [ [ 'number', 'DESC' ] ]
    },
    cxcUser,
    cxpUser,
    operations: [ operation ],
    commonError,
    operationElement,
    generalInfoParams: [ [ clientCompany, 'client' ] ],
    where: {
      company_rfc: rfc
    },
    limit: 2,
    offset: 2,
    order_by: 'number',
    order_desc: true,
    requestClient: {
      limit: 2,
      offset: 2,
      order_by: 'client_name',
      order_desc: true
    },
    requestCompany: {
      limit: 2,
      offset: 2,
      order_by: 'company_name',
      order_desc: true
    },
    user: {
      role: 'CXC'
    }
  },
  getOperationsCxP: {
    options: {
      operationStatusCondition: 'IN'
    },
    optionsCxp: {
      compute_payment_values: true
    },
    findAll: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [ { all: true } ]
    },
    findAllWithAttributes: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [ { all: true } ],
      offset: 2,
      limit: 2,
      order: [ [ 'number', 'DESC' ] ]
    },
    findAllWithWhere: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [ { all: true } ],
      where: {
        company_rfc: rfc
      },
      offset: 2,
      limit: 2,
      order: [ [ 'number', 'DESC' ] ]
    },
    cxcUser,
    cxpUser,
    invoices: [ invoice ],
    commonError,
    invoiceElement,
    generalInfoParams: [ [ clientCompany, 'client' ] ],
    where: {
      company_rfc: rfc
    },
    limit: 2,
    offset: 2,
    order_by: 'number',
    order_desc: true,
    requestClient: {
      limit: 2,
      offset: 2,
      order_by: 'client_name',
      order_desc: true
    },
    requestCompany: {
      limit: 2,
      offset: 2,
      order_by: 'company_name',
      order_desc: true
    },
    user: {
      role: 'CXP'
    }
  },
  getAdminOperations: {
    operations: [ operation ],
    commonError,
    operationInfo: operation,
    allParams: allParams,
    allParamsOrderByCompanyName,
    allParamsOrderByInvestorName,
    allParamsOrderByStatus,
    allParamsFindAllQuery: {
      attributes: [ 'id', 'invoice_id', 'expiration_date', 'published_date', 'days_limit', 'status' ],
      include,
      offset: 1,
      limit: 25,
      where: {
        status: [ 'APPROVED', 'PUBLISHED' ]
      },
      order: [ [ sequelize.col('Invoice.Client.name' || 'Operation.created_at'), 'DESC' ] ]
    },
    allParamsCountQuery: {
      include: [
        {
          model: Company,
          as: 'Client',
          where: { name: { $iLike: '%client_name%' } }
        },
        {
          model: Company,
          as: 'Company',
          where: { name: { $iLike: '%company_name%' } }
        },
        {
          model: Company,
          as: 'Investor',
          where: { name: { $iLike: '%investor_name%' } }
        },
        {
          model: Operation
        }
      ],
      where: {
        status: [ 'APPROVED', 'PUBLISHED' ],
        expiration: { $lte: expirationEnd, $gte: expiration },
        fund_date: { $lte: todayEnd, $gte: today }
      }
    },
    response: {
      operations: [ operation ],
      count: 1
    }
  },
  getBasicInfo: {
    operation: {
      invoice_id: 1,
      expiration_date: sixtyDays,
      published_date: today,
      annual_cost_percentage: 15.00,
      reserve_percentage: 10.00,
      fd_commission_percentage: 0.50,
      annual_cost: 1256.10,
      reserve: 837.40,
      fd_commission: 41.87,
      factorable: 7536.60,
      subtotal: 7330.74,
      fund_payment: 7323.88,
      commission: 6.86,
      fund_total: 8161.28,
      operation_cost: 212.72,
      status: 'PENDING',
      formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
                    ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
      user_id: 2,
      days_limit: 59,
      id: 1
    },
    result: {
      invoice_id: 1,
      expiration_date: sixtyDays.toString(),
      published_date: today.toString(),
      annual_cost_percentage: '15.00',
      reserve_percentage: '10.00',
      fd_commission_percentage: '0.50',
      annual_cost: '1256.10',
      reserve: '837.40',
      fd_commission: '41.87',
      factorable: '7536.60',
      subtotal: '7330.74',
      fund_payment: '7323.88',
      commission: '6.86',
      fund_total: '8161.28',
      operation_cost: '212.72',
      formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
                    ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
      user_id: 2,
      days_limit: 59,
      status: 'published',
      id: 1
    }
  },
  getGeneralInfo: {
    operation: {
      id: 1,
      invoice_id: 2,
      expiration_date: today,
      published_date: todayEnd,
      days_limit: 1,
      status: 'PENDING',
      Invoice: {
        number: 'ABC123',
        uuid: 'ASDZXCQWE',
        total: 48239,
        Company: {
          name: 'name_company',
          id: 11
        }
      }
    },
    resultOperation: {
      id: 1,
      invoice_id: 2,
      expiration_date: today.toString(),
      published_date: todayEnd.toString(),
      days_limit: 1,
      prepaid: '0.00',
      saving: '0.00',
      status: 'published',
      number: 'ABC123',
      uuid: 'ASDZXCQWE',
      total: '48239.00',
      company_name: 'name_company',
      company_id: 11
    }
  },
  getGeneralInfoAsAdmin: {
    operation: {
      id: 1,
      invoice_id: 2,
      expiration_date: today,
      published_date: todayEnd,
      days_limit: 1,
      status: 'PENDING',
      created_at: 'yesterday',
      Invoice: {
        id: 1,
        uuid: 'ASDZXCQWE',
        client_company_id: clientCompanyId,
        client_name: 'client',
        client_color: 'client_color',
        company_id: companyId,
        company_name: 'company',
        company_color: 'company_color',
        investor_name: 'investor',
        number: 'ABC123',
        expiration: todayEnd,
        fund_date: today,
        status: 'pending',
        total: total,
        operation_status: 'published',
        Client: {
          name: 'client',
          color: 'client_color',
          id: clientCompanyId
        },
        Investor: {
          name: 'investor'
        },
        Company: {
          name: 'company',
          color: 'company_color',
          id: companyId
        }
      }
    },
    result: {
      id: 1,
      invoice_id: 2,
      expiration_date: today.toString(),
      published_date: todayEnd.toString(),
      days_limit: 1,
      status: 'published',
      company_name: 'company',
      number: 'ABC123',
      uuid: 'ASDZXCQWE',
      total: total.toFixed(2),
      created_at: 'yesterday',
      invoice: {
        id: 1,
        uuid: 'ASDZXCQWE',
        client_company_id: clientCompanyId,
        client_name: 'client',
        client_color: 'client_color',
        company_id: companyId,
        company_name: 'company',
        company_color: 'company_color',
        investor_name: 'investor',
        number: 'ABC123',
        expiration: todayEnd.toString(),
        fund_date: today.toString(),
        status: 'pending',
        total: total.toFixed(2),
        operation_status: 'published'
      }
    }
  }
};
