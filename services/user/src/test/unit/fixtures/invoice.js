const _ = require('lodash');
const { commonError, notFound, sequelizeError } = require('../../common/fixtures/errors');
const { Company, Operation, sequelize } = require('../../../models');
const { cxpActions, cxcActions } = require('../../../config/user-roles');

const sendengoStrategy = {
  selectObjectByCompanyId: () => {
    return true;
  },
  selectByCompanyId: () => {
    return true;
  }
};
const someCreatedAt = new Date();
const rfc = 'FODA900806965';
const companyId = 1;
const clientCompanyId = 2;
const investorCompanyId = 3;
const userId = 1;
const issuerRfc = rfc;
const recipientRfc = rfc + 'diff';
const date = new Date();
const clientCompany = {
  rfc: issuerRfc,
  name: 'Client Company',
  color: '#FFFFF'
};
const company = {
  rfc: recipientRfc,
  name: 'Company',
  color: '#000000'
};
const globalFeePercentage = '1/3';

date.setHours(0, 0, 0, 0);

const expiration = new Date();
const today = new Date();
const todayEnd = new Date();
const todayMidDay = new Date();
const expirationEnd = new Date();
const yesterday = new Date();
const expirationMidDay = new Date();
const sixtyDays = new Date();
const yesterdayAt2PM = new Date();
const todayAt2PM = new Date();
const newInvoicePublishedDate = new Date();
const eightDaysInitial = new Date();
const eightDaysFinal = new Date();

eightDaysInitial.setHours(0, 0, 0, 0);
eightDaysFinal.setHours(0, 0, 0, 0);
eightDaysInitial.setDate(eightDaysInitial.getDate() + 8);
eightDaysFinal.setDate(eightDaysFinal.getDate() + 8);
eightDaysInitial.setHours(0, 0, 0, 0);
eightDaysFinal.setHours(23, 59, 59, 59);

newInvoicePublishedDate.setHours(15, 23, 0, 0);
newInvoicePublishedDate.setDate(newInvoicePublishedDate.getDate());
yesterdayAt2PM.setHours(14, 0, 0, 0);
todayAt2PM.setHours(14, 0, 0, 0);
yesterdayAt2PM.setDate(yesterdayAt2PM.getDate() - 1);
todayAt2PM.setDate(todayAt2PM.getDate());

expiration.setHours(0, 0, 0, 0);
expirationEnd.setHours(23, 59, 59, 59);
expirationMidDay.setHours(12, 0, 0, 0);
today.setHours(0, 0, 0, 0);
todayEnd.setHours(23, 59, 59, 59);
todayMidDay.setHours(12, 0, 0, 0);
yesterday.setHours(0, 0, 0, 0);

expiration.setDate(date.getDate() + 59);
expirationEnd.setDate(date.getDate() + 60);
expirationMidDay.setDate(date.getDate() + 59);
yesterday.setDate(yesterday.getDate() - 1);

sixtyDays.setHours(0, 0, 0, 0);
sixtyDays.setDate(sixtyDays.getDate() + 59);

const newInvoice = {
  id: 1,
  created_at: date,
  expiration,
  published: newInvoicePublishedDate,
  company_id: companyId,
  client_company_id: companyId,
  Client: clientCompany,
  Company: company,
  total: 330,
  status: 'PUBLISHED',
  Operation: {
    status: 'PENDING'
  }
};

const invoice = {
  Operation: {},
  id: 1,
  getGeneralInfo() {
    return this;
  },
  getGeneralInfoAsAdmin() {
    return this;
  },
  getInvestorProfitEstimate() {
    return {};
  },
  getProtoBuffArrayOfInvoiceItems() {
    return [ {
      count: '1',
      description: 'qui culpa nulla',
      price: '50.00',
      total: '50.00'
    } ];
  },
  getInvestorFundEstimate() {
    return 0;
  },
  created_at: date,
  expiration,
  company_id: companyId,
  client_company_id: companyId,
  Client: clientCompany,
  Company: company,
  total: 330,
  status: 'PENDING'
};

const operation = {
  invoice_id: 1,
  expiration_date: sixtyDays,
  published_date: today,
  annual_cost_percentage: '15.00',
  reserve_percentage: '10.00',
  fd_commission_percentage: '0.50',
  annual_cost: '1256.10',
  reserve: '837.40',
  fd_commission: '41.87',
  factorable: '7536.60',
  subtotal: '7330.74',
  fund_payment: '7323.88',
  commission: '6.86',
  fund_total: '8161.28',
  operation_cost: '212.72',
  formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
              ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
  user_id: 2,
  days_limit: 59
};

const investorTransaction = {
  operation_id: 1,
  investor_company_id: 2,
  fund_request_date: today,
  days_limit: 59,
  funded_amount: '8374.00',
  interest: '205.86',
  interest_percentage: '2.46',
  isr: '0.00',
  isr_percentage: '0.00',
  global_fee_percentage: '1/3',
  fee: '20.59',
  fee_type: 'Variable fee',
  earnings: '185.27',
  earnings_percentage: '2.21',
  perception: '8559.27',
  fideicomiso_fee: null,
  fideicomiso_fee_type: null
};

const fideicomisoTransaction = {
  operation_id: 1,
  investor_company_id: 2,
  fund_request_date: today,
  days_limit: 59,
  funded_amount: '8374.00',
  interest: '205.86',
  interest_percentage: '2.46',
  isr: '0.00',
  isr_percentage: '0.00',
  global_fee_percentage: '1/3',
  fee: '20.59',
  fee_type: 'Variable fee',
  earnings: '179.10',
  earnings_percentage: '2.14',
  perception: '8553.10',
  fideicomiso_fee: '6.18',
  fideicomiso_fee_type: 'Variable fee'
};

const invoiceExpiresNextDay = {
  id: 1,
  getGeneralInfo() {
    return this;
  },
  getGeneralInfoAsAdmin() {
    return this;
  },
  getInvestorFundEstimate() {
    return 0;
  },
  created_at: date,
  expiration: 'nextDay',
  company_rfc: rfc,
  client_rfc: rfc,
  Client: clientCompany,
  Company: company,
  total: 330,
  status: 'PENDING'
};

const cxcUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXC',
  company_rfc: rfc
};
const cxpUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXP',
  company_rfc: rfc
};

const investorUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'INVESTOR',
  company_rfc: rfc
};
const invoiceElement = {
  created_at: date.toString(),
  client_rfc: rfc,
  client_name: 'Company',
  client_color: '#FFFFF',
  company_rfc: rfc,
  total: '330',
  status: 'pending'
};
const getFunction = function() {
  return this;
};

const transactionParams = {
  transaction: undefined
};

module.exports = {
  beforeCreate: {
    instanceWithSerie: {
      emission_date: '2017-02-03T10:42:22+06:00',
      serie: 'A',
      folio: '1'
    },
    instanceWithoutSerie: {
      emission_date: '2017-02-03T10:42:22+06:00',
      folio: '1'
    },
    resultWithSerie: {
      emission_date: new Date('2017-02-03T10:42:22+06:00'),
      serie: 'A',
      folio: '1',
      number: 'A-1'
    },
    resultWithoutSerie: {
      emission_date: new Date('2017-02-03T10:42:22+06:00'),
      folio: '1',
      number: '1'
    }
  },
  setGet: {
    number: 2,
    commonError,
    context: {
      setDataValue: () => {
        return true;
      },
      getDataValue: () => {
        return true;
      }
    }
  },
  validateCompanies: {
    user: {},
    company: {},
    instances: [ {}, {} ],
    userId,
    issuerRfc,
    recipientRfc,
    userQuery: {
      where: {
        id: userId
      },
      include: [ {
        model: Company,
        where: {
          rfc: issuerRfc
        }
      } ]
    },
    recipientQuery: {
      where: {
        rfc: recipientRfc
      }
    },
    commonError,
    noClientError: sequelizeError('client_rfc', 'Receptor not found'),
    noCompanyError: sequelizeError('company_rfc', 'Issuer not found'),
    sameRfcError: sequelizeError('invoice', 'Company and Client RFC must be different')
  },
  getInvoicesModel: {
    sendengoStrategy,
    daysDiffQueryResult: 'daysdiffquery',
    gainEstimateQueryResult: 'gainestimatequery',
    gainEstimatePercentageQueryResult: 'gainestimatepercentagequery',
    investorUser,
    findAll: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date',
        'client_company_id', 'emission_date' ],
      include: [
        {
          model: Company,
          as: 'Client'
        },
        {
          model: Company,
          as: 'Company'
        }
      ],
      order: [ [ 'created_at', 'ASC' ] ]
    },
    findAllIsInvestor: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date',
        'client_company_id', 'emission_date' ],
      include: [
        {
          model: Company,
          as: 'Client'
        },
        {
          model: Company,
          as: 'Company'
        },
        { model: Operation }
      ],
      order: [ [ 'created_at', 'ASC' ] ]
    },
    findCompanyNameSort: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date',
        'client_company_id', 'emission_date' ],
      include: [
        {
          model: Company,
          as: 'Client'
        },
        {
          model: Company,
          as: 'Company'
        }
      ],
      offset: 2,
      limit: 2,
      order: [ [ 'Company.name', 'DESC' ] ]
    },
    findClientSort: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date',
        'client_company_id', 'emission_date' ],
      include: [
        {
          model: Company,
          as: 'Client'
        },
        {
          model: Company,
          as: 'Company'
        }
      ],
      offset: 2,
      limit: 2,
      order: [ [ 'Client.name', 'DESC' ] ]
    },
    findAllWithAttributes: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date',
        'client_company_id', 'emission_date' ],
      include: [
        {
          model: Company,
          as: 'Client'
        },
        {
          model: Company,
          as: 'Company'
        }
      ],
      offset: 2,
      limit: 2,
      order: [ [ 'number', 'DESC' ] ]
    },
    findAllWithWhere: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date',
        'client_company_id', 'emission_date' ],
      include: [
        {
          model: Company,
          as: 'Client'
        },
        {
          model: Company,
          as: 'Company'
        }
      ],
      where: {
        company_rfc: rfc
      },
      offset: 2,
      limit: 2,
      order: [ [ 'number', 'DESC' ] ]
    },
    cxcUser,
    cxpUser,
    invoices: [ invoice ],
    commonError,
    invoiceElement,
    generalInfoParams: [ [ clientCompany, 'client', null ] ],
    generalInfoParamsInvestor: [ [ clientCompany, 'client', { investorGeneralInfo: true } ] ],
    where: {
      company_rfc: rfc
    },
    limit: 2,
    offset: 2,
    order_by: 'number',
    order_desc: true,
    requestClient: {
      limit: 2,
      offset: 2,
      order_by: 'client_name',
      order_desc: true
    },
    requestCompany: {
      limit: 2,
      offset: 2,
      order_by: 'company_name',
      order_desc: true
    }
  },
  getBasicOperationInfo: {
    invoice: {
      id: 1,
      Operation: {
        id: 3
      },
      folio: 'ABC',
      serie: '123',
      number: 'ABC123',
      emission_date: 'today',
      expiration: 'tomorrow',
      uuid: 'ASDZXCQWE',
      status: 'PENDING'
    },
    result: {
      id: 1,
      operation_id: 3
    }
  },
  getBasicInfo: {
    invoice: {
      Company: {
        id: companyId,
        rfc,
        name: 'name',
        color: 'color',
        business_name: 'name'
      },
      folio: 'ABC',
      serie: '123',
      number: 'ABC123',
      emission_date: 'today',
      expiration: 'tomorrow',
      uuid: 'ASDZXCQWE',
      status: 'PENDING',
      total: 5000
    },
    result: {
      company_id: companyId,
      company_name: 'name',
      company_color: 'color',
      business_name: 'name',
      number: 'ABC123',
      emission_date: 'today',
      expiration: 'tomorrow',
      uuid: 'ASDZXCQWE',
      status: 'pending',
      total: 5000
    }
  },
  getGeneralInfo: {
    company: {
      id: companyId,
      name: 'name',
      color: 'color'
    },
    invoice: {
      folio: 'ABC',
      number: 'ABC123',
      status: 'PENDING',
      company_id: companyId,
      client_company_id: clientCompanyId,
      created_at: date,
      get: getFunction,
      expiration: 'tomorrow',
      fund_date: 'today',
      published: 'yesterday',
      term_days: 100,
      Operation: {
        id: 1,
        status: 'PENDING'
      }
    },
    resultCompany: {
      number: 'ABC123',
      status: 'pending',
      company_id: companyId,
      client_company_id: clientCompanyId,
      created_at: date.toString(),
      company_name: 'name',
      company_color: 'color',
      client_name: undefined,
      client_color: undefined,
      id: undefined,
      uuid: undefined,
      total: 'undefined',
      expiration: 'tomorrow',
      fund_date: 'today',
      published: 'yesterday',
      gain: undefined,
      gain_percentage: undefined,
      term_days: 100,
      emission_date: '',
      expiration_extra: 'tomorrow',
      operation_id: 1
    },
    resultClient: {
      number: 'ABC123',
      status: 'pending',
      company_id: companyId,
      client_company_id: companyId,
      created_at: date.toString(),
      client_name: 'name',
      client_color: 'color',
      company_name: undefined,
      company_color: undefined,
      id: undefined,
      uuid: undefined,
      total: 'undefined',
      expiration: 'tomorrow',
      fund_date: 'today',
      published: 'yesterday',
      gain: undefined,
      gain_percentage: undefined,
      term_days: 100,
      emission_date: '',
      expiration_extra: 'tomorrow',
      operation_id: 1
    }
  },
  getGeneralInfoWithOperation: {
    company: {
      id: companyId,
      name: 'name',
      color: 'color'
    },
    invoice: {
      folio: 'ABC',
      number: 'ABC123',
      status: 'PENDING',
      company_id: companyId,
      client_company_id: clientCompanyId,
      created_at: date,
      get: getFunction,
      expiration: 'tomorrow',
      fund_date: 'today',
      published: 'yesterday',
      term_days: 100,
      total: 2000,
      uuid: 'A UUID',
      Operation: {
        id: 10000,
        invoice_id: 1,
        status: 'PUBLISHED',
        published_date: 'today',
        days_limit: 23
      },
      Client: {
        name: 'Sureplex',
        id: 1002
      }
    },
    invoiceWithoutOperation: {
      id: 1000,
      folio: 'ABC',
      number: 'ABC123',
      status: 'PENDING',
      company_id: companyId,
      client_company_id: clientCompanyId,
      created_at: date,
      get: getFunction,
      expiration: 'tomorrow',
      fund_date: 'today',
      published: 'yesterday',
      term_days: 100,
      total: 2000,
      uuid: 'A UUID',
      Client: {
        name: 'Sureplex',
        id: 1002
      }
    },
    resultWithOperation: {
      id: 10000,
      invoice_id: 1,
      expiration_date: 'tomorrow',
      published_date: 'today',
      days_limit: 23,
      status: 'published',
      uuid: 'A UUID',
      number: 'ABC123',
      total: '2000.00',
      prepaid: '0.00',
      saving: '0.00',
      company_name: 'Sureplex',
      company_id: 1002
    },
    resultWithoutOperation: {
      id: undefined,
      invoice_id: 1000,
      expiration_date: 'tomorrow',
      published_date: '',
      days_limit: undefined,
      status: 'published',
      uuid: 'A UUID',
      number: 'ABC123',
      total: '2000.00',
      prepaid: '0.00',
      saving: '0.00',
      company_name: 'Sureplex',
      company_id: 1002
    }
  },
  getBasicInfoWithClient: {
    expirationDate: '2018-05-14',
    expirationDateExtra: '2018-05-24',
    invoice: {
      Client: {
        id: companyId,
        rfc,
        name: 'name',
        color: 'color',
        business_name: 'name',
        description: 'description test',
        get: () => {
          return { created_at: someCreatedAt };
        }
      },
      getProtoBuffArrayOfInvoiceItems() {
        return [ {
          count: '1',
          description: 'qui culpa nulla',
          price: '50.00',
          total: '50.00'
        } ];
      },
      folio: 'ABC',
      serie: '123',
      number: 'ABC123',
      subtotal: 1000000,
      total: 1000000,
      emission_date: 'today',
      expiration: 'tomorrow',
      expiration_extra: 'tomorrow',
      expiration_days: 0,
      uuid: 'ASDZXCQWE',
      status: 'PUBLISHED',
      created_at: '',
      items: [ {
        count: '1',
        description: 'qui culpa nulla',
        price: '50.00',
        total: '50.00'
      } ],
      tax_total: '12.00',
      Operation: {
        id: 1,
        payment_date: 'in two days',
        status: 'PAYMENT_IN_PROCESS',
        InvestorTransaction: {
          fund_date: 'in one day'
        }
      }
    },
    result: {
      client_company_id: companyId,
      client_name: 'name',
      client_created_at: someCreatedAt.toString(),
      client_color: 'color',
      client_description: 'description test',
      business_name: 'name',
      number: 'ABC123',
      subtotal: '1000000',
      emission_date: 'today',
      expiration: 'tomorrow',
      expiration_extra: 'tomorrow',
      expiration_days: 0,
      uuid: 'ASDZXCQWE',
      status: 'payment_in_process',
      client_rfc: rfc,
      total: '1000000',
      fund_date: 'in one day',
      payment_date: 'in two days',
      published_date: '',
      approved_date: '',
      upload_date: '',
      items: [ {
        count: '1',
        description: 'qui culpa nulla',
        price: '50.00',
        total: '50.00'
      } ],
      taxes: '12.00',
      operation_id: 1
    }
  },
  validateCxp: {
    userId,
    invoiceId: invoice.id,
    actions: cxpActions,
    companyIdField: 'client_company_id',
    commonError,
    user: {
      role: 'CXP',
      company_id: companyId
    }
  },
  validateCxc: {
    userId,
    invoiceId: invoice.id,
    actions: cxcActions,
    companyIdField: 'company_id',
    commonError,
    user: {
      role: 'CXC',
      company_id: companyId
    }
  },
  validateUser: {
    user: {
      role: 'CXP',
      company_id: companyId
    },
    userId,
    userQuery: {
      where: {
        id: userId
      }
    },
    invoiceQuery: {
      where: {
        id: invoice.id
      },
      include: { all: true }
    },
    commonError,
    invoice,
    noInvoiceError: notFound('Invoice'),
    unauthorizedError: {
      errors: [ {
        path: 'user',
        message: 'Unauthorized role'
      } ]
    },
    roles: [ 'CXP' ],
    companyIdField: 'client_company_id'
  },
  validateExpirationDate: {
    invalidFormatError: {
      errors: [ {
        path: 'expiration',
        message: 'invalid format'
      } ]
    },
    invalidDateError: {
      errors: [ {
        path: 'expiration',
        message: 'invalid date'
      } ]
    }
  },
  getEstimate: {
    invoice: {
      Client: {
        OperationCost: {
          annual_cost: 15,
          fd_commission: 0.5
        }
      },
      total: 100000,
      expiration
    },
    result: {
      total: '100000',
      operation_cost: '2540.28',
      fund_total: '97459.72'
    }
  },
  getDetail: {
    sendengoStrategy,
    invoice: {
      Company: {
        rfc,
        id: companyId,
        name: 'name',
        color: 'color',
        business_name: 'name'
      },
      company_regime: 'regimen',
      company_postal_code: '12345',
      folio: 'ABC',
      serie: '123',
      number: '123ABC',
      emission_date: 'today',
      uuid: 'ASDZXCQWE',
      status: 'PENDING',
      subtotal: 10,
      tax_total: 5,
      cadena_original: 'cadena_original',
      sat_digital_stamp: 'sat_digital_stamp',
      cfdi_digital_stamp: 'cfdi_digital_stamp',
      expiration: sixtyDays,
      items: [ { } ]
    },
    sendengoInvoice: {
      Company: {
        rfc,
        id: companyId,
        name: 'name',
        color: 'color',
        business_name: 'name'
      },
      company_regime: 'regimen',
      company_postal_code: '12345',
      folio: 'ABC',
      serie: '123',
      number: '123ABC',
      emission_date: 'today',
      uuid: 'ASDZXCQWE',
      status: 'PUBLISHED',
      subtotal: 10,
      tax_total: 5,
      total: 15,
      cadena_original: 'cadena_original',
      sat_digital_stamp: 'sat_digital_stamp',
      cfdi_digital_stamp: 'cfdi_digital_stamp',
      expiration: sixtyDays,
      items: [ { } ],
      fund_date: new Date(),
      getFundEstimate: () => {
        return true;
      },
      company_id: 12000
    },
    sendengoInvoiceNoFundDate: {
      Company: {
        rfc,
        id: companyId,
        name: 'name',
        color: 'color',
        business_name: 'name'
      },
      company_regime: 'regimen',
      company_postal_code: '12345',
      folio: 'ABC',
      serie: '123',
      number: '123ABC',
      emission_date: 'today',
      uuid: 'ASDZXCQWE',
      status: 'PENDING',
      subtotal: 10,
      tax_total: 5,
      total: 15,
      cadena_original: 'cadena_original',
      sat_digital_stamp: 'sat_digital_stamp',
      cfdi_digital_stamp: 'cfdi_digital_stamp',
      expiration: sixtyDays,
      items: [ { } ],
      company_id: 12000
    },
    estimates: {
      operation_cost: '100.00'
    },
    result: {
      company_rfc: rfc,
      company_id: companyId,
      company_name: 'name',
      company_color: 'color',
      business_name: 'name',
      number: '123ABC',
      company_regime: 'regimen',
      company_postal_code: '12345', emission_date: 'today', uuid: 'ASDZXCQWE',
      status: 'pending',
      subtotal: '10',
      taxes: '5',
      total: '15.00',
      cadena_original: 'cadena_original',
      sat_digital_stamp: 'sat_digital_stamp',
      cfdi_digital_stamp: 'cfdi_digital_stamp',
      expiration: sixtyDays.toString(),
      items: [ { } ]
    },
    resultSendengo: {
      company_rfc: rfc,
      company_id: companyId,
      company_name: 'name',
      company_color: 'color',
      business_name: 'name',
      number: '123ABC',
      company_regime: 'regimen',
      company_postal_code: '12345',
      emission_date: 'today',
      uuid: 'ASDZXCQWE',
      status: 'published',
      subtotal: '10',
      taxes: '5',
      total: '15.00',
      cadena_original: 'cadena_original',
      sat_digital_stamp: 'sat_digital_stamp',
      cfdi_digital_stamp: 'cfdi_digital_stamp',
      expiration: sixtyDays.toString(),
      items: [ { } ],
      placeholder_payment_cxp: '115.00'
    }
  },
  getFundEstimate: {
    approvedInvoice: {
      client_company_id: clientCompanyId,
      fee: 350,
      total: 100000,
      annual_cost: 15,
      reserve: 10,
      fd_commission: 0.5,
      tax_total: 160,
      expiration: sixtyDays,
      get: getFunction
    },
    invoice: {
      client_company_id: clientCompanyId,
      fee: 350,
      total: 100000,
      tax_total: 160,
      expiration: sixtyDays,
      get: getFunction
    },
    invoiceWithoutExpiration: {
      client_company_id: clientCompanyId,
      fee: 350,
      total: 100000,
      tax_total: 160,
      get: getFunction
    },
    costs: {
      annual_cost: 15,
      reserve: 10,
      fd_commission: 0.5
    },
    result: {
      days: 59,
      fee: undefined,
      total: '100000.00',
      interest: '2458.33',
      commission: '81.94',
      fund_total: '97459.73',
      reserve: '10000.00',
      fund_payment: '87459.73',
      operation_cost: '2540.27',
      expiration: sixtyDays.toString(),
      expiration_payment: '10000.00',
      tax_total: '160.00'
    },
    costQuery: [ {
      where: {
        company_id: clientCompanyId
      }
    } ],
    commonError,
    notApproved: sequelizeError('Invoice', 'Not approved yet')
  },
  getMarketplace: {
    findAll: {
      attributes: [
        'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'published',
        [ 'daysdiffquery', 'term_days' ],
        [ 'gainestimatequery', 'gain' ],
        [ 'gainestimatepercentagequery', 'gain_percentage' ]
      ],
      include: [
        { model: Company, as: 'Client', where: undefined },
        { model: Operation, where: { status: 'PENDING' } }
      ]
    },
    findAllWithWhere: {
      attributes: [
        'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'published',
        [ 'daysdiffquery', 'term_days' ],
        [ 'gainestimatequery', 'gain' ],
        [ 'gainestimatepercentagequery', 'gain_percentage' ]
      ],
      include: [
        { model: Company, as: 'Client', where: undefined },
        { model: Operation, where: { status: 'PENDING' } }
      ],
      where: {
        company_rfc: rfc
      }
    },
    findAllInclude: {
      attributes: [
        'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'published',
        [ 'daysdiffquery', 'term_days' ],
        [ 'gainestimatequery', 'gain' ],
        [ 'gainestimatepercentagequery', 'gain_percentage' ]
      ],
      include: [
        { model: Company, as: 'Client', where: {
          name: 'surplex'
        } },
        { model: Operation, where: { status: 'PENDING' } }
      ]
    },
    invoices: [ invoice ],
    commonError,
    invoiceElement,
    daysDiffQueryResult: 'daysdiffquery',
    gainEstimateQueryResult: 'gainestimatequery',
    gainEstimatePercentageQueryResult: 'gainestimatepercentagequery',
    daysDiffQueryParams: [ 'expiration', 'now()' ],
    gainEstimatePercentageQueryParams: [ 'CURRENT_DATE' ],
    gainEstimateQueryParams: [ 'CURRENT_DATE' ],
    generalInfoParams: [ [ clientCompany, 'client', { investorGeneralInfo: true } ] ],
    where: {
      company_rfc: rfc
    },
    whereInclude: {
      name: 'surplex'
    }
  },
  getInvestorFundEstimate: {
    invoice: {
      total: 100000,
      expiration,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5
      }
    },
    invoiceFundDate: {
      total: 100000,
      expiration,
      fund_date: today,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5
      }
    },
    isrResult: {
      total: '100000.00',
      earnings: '2458.33',
      commission: '250.00',
      isr: '491.67',
      include_isr: true,
      perception: '101716.66'
    },
    result: {
      total: '100000.00',
      earnings: '2458.33',
      isr: '0.00',
      include_isr: false,
      commission: '250.00',
      perception: '102208.33'
    },
    fundedByFideicomiso: {
      fideicomisoResult: {
        total: '100000.00',
        earnings: '2458.33',
        commission: '500.00',
        perception: '101466.66',
        isr: '491.67',
        include_isr: true
      }
    },
    investorPhysical: {
      id: companyId,
      OperationCost: {
        fee: 250,
        variable_fee_percentage: 10
      },
      taxpayer_type: 'physical'
    },
    investorMoral: {
      id: companyId,
      OperationCost: {
        fee: 250,
        variable_fee_percentage: 10
      },
      taxpayer_type: 'moral'
    },
    investorFideicomiso: {
      id: companyId,
      OperationCost: {
        fee: 350,
        variable_fee_percentage: 10,
        fideicomiso_fee: 150,
        fideicomiso_variable_fee: 3
      },
      taxpayer_type: 'physical'
    },
    global_fee_percentage: '1/3'
  },
  getInvestorFundEstimateFromTransaction: {
    invoice: {
      total: 100000,
      expiration,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5,
        InvestorTransaction: {
          fund_request_date: today,
          days_limit: 89,
          status: 'PENDING',
          funded_amount: 50000,
          interest: 1854.17,
          isr: 0,
          isr_percentage: 0,
          global_fee_percentage: '1/3',
          fee: 350,
          fee_type: 'Fixed fee',
          earnings: 1504.17,
          earnings_percentage: 3.01,
          perception: 51504.17
        }
      }
    },
    result: {
      total: '100000.00',
      earnings: '1854.17',
      commission: '350.00',
      perception: '51504.17',
      isr: '0.00',
      include_isr: false
    },
    invoiceWithIsr: {
      total: 100000,
      expiration,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5,
        InvestorTransaction: {
          fund_request_date: today,
          days_limit: 89,
          funded_amount: 100000,
          interest: 3708.33,
          isr: 741.67,
          isr_percentage: 0.74,
          global_fee_percentage: '1/3',
          fee: 250,
          fee_type: 'Fixed fee',
          earnings: 3458.33,
          earnings_percentage: 3.46,
          perception: 102716.66,
          status: 'PENDING'
        }
      }
    },
    resultWithIsr: {
      total: '100000.00',
      earnings: '3708.33',
      commission: '250.00',
      perception: '102716.66',
      isr: '741.67',
      include_isr: true
    }
  },
  getInvestorProfitEstimateFromTransaction: {
    invoice: {
      total: 100000,
      expiration,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5,
        InvestorTransaction: {
          fund_request_date: today,
          days_limit: 89,
          status: 'PENDING',
          funded_amount: 50000,
          interest: 1854.17,
          interest_percentage: 3.71,
          isr: 0,
          isr_percentage: 0,
          global_fee_percentage: '1/3',
          fee: 350,
          fee_type: 'Fixed fee',
          earnings: 1504.17,
          earnings_percentage: 3.01,
          perception: 51504.17
        }
      }
    },
    result: {
      gain: '1854.17',
      gain_percentage: '3.71',
      annual_gain: '15.00'
    }
  },
  getInvestorProfitEstimate: {
    invoice: {
      total: 100000,
      expiration,
      Operation: {
        annual_cost_percentage: 15
      }
    },
    invoiceFundDate: {
      total: 100000,
      expiration,
      fund_date: today,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5
      }
    },
    result: {
      gain: '2458.33',
      gain_percentage: '2.46',
      annual_gain: '15.00'
    }
  },
  getAdminInvoices: {
    invoices: [ invoice ],
    commonError,
    invoiceInfo: invoice,
    allParams: {
      limit: 25,
      offset: 1,
      order_by: 'client_name',
      order_desc: true,
      client_name: 'client_name',
      company_name: 'company_name',
      investor_name: 'investor_name',
      status: [ 'approved', 'rejected' ],
      start_fund_date: today.toString(),
      end_fund_date: todayEnd.toString(),
      start_expiration_date: expiration.toString(),
      end_expiration_date: expirationEnd.toString()
    },
    allParamsFindAllQuery: {
      attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
      include: [
        {
          model: Company,
          as: 'Client',
          where: { name: { $iLike: '%client_name%' } }
        },
        {
          model: Company,
          as: 'Company',
          where: { name: { $iLike: '%company_name%' } }
        }
      ],
      where: {
        status: { $notLike: 'PUBLISHED', $in: [ 'APPROVED', 'REJECTED' ] },
        expiration: { $lte: expirationEnd, $gte: expiration }
      },
      offset: 1,
      limit: 25,
      order: [ [ sequelize.col('Client.name'), 'DESC' ] ]
    },
    allParamsCountQuery: {
      include: [
        {
          model: Company,
          as: 'Client',
          where: { name: { $iLike: '%client_name%' } }
        },
        {
          model: Company,
          as: 'Company',
          where: { name: { $iLike: '%company_name%' } }
        }
      ],
      where: {
        status: { $notLike: 'PUBLISHED', $in: [ 'APPROVED', 'REJECTED' ] },
        expiration: { $lte: expirationEnd, $gte: expiration }
      }
    },
    response: {
      invoices: [ invoice ],
      count: 1
    }
  },
  getGeneralInfoAsAdmin: {
    invoice: {
      id: 1,
      Client: {
        id: clientCompanyId,
        name: 'client',
        color: 'client_color'
      },
      Company: {
        id: companyId,
        name: 'company',
        color: 'company_color'
      },
      Investor: {
        id: investorCompanyId,
        name: 'investor',
        color: 'investor_color'
      },
      Operation: {
        id: 1,
        status: 'PENDING'
      },
      folio: 'ABC',
      serie: '123',
      number: 'ABC123',
      emission_date: 'today',
      expiration: 'tomorrow',
      fund_date: 'later',
      uuid: 'ASDZXCQWE',
      status: 'PENDING',
      total: 200
    },
    result: {
      id: 1,
      uuid: 'ASDZXCQWE',
      client_company_id: clientCompanyId,
      client_name: 'client',
      client_color: 'client_color',
      company_id: companyId,
      company_name: 'company',
      company_color: 'company_color',
      investor_name: 'investor',
      number: 'ABC123',
      expiration: 'tomorrow',
      fund_date: 'later',
      status: 'pending',
      total: '200'
    }
  },
  getInvestorGain: {
    invoice: {
      total: 100000,
      expiration,
      Operation: {
        annual_cost_percentage: 15,
        fd_commission_percentage: 0.5
      }
    },
    taxpayerType: 'physical',
    isrResult: '1966.66',
    global_fee_percentage: globalFeePercentage,
    result: '2458.33',
    fee: 250
  },
  getInvoiceAmounts: {
    invoice: {
      id: 1,
      total: 100000,
      is_available: true,
      expiration: expirationEnd,
      Operation: {
        annual_cost_percentage: 15
      },
      Client: {
        name: 'Sureplex'
      },
      get: function() {
        return this;
      }
    },
    investor: {
      id: 1000,
      taxpayer_type: 'moral',
      OperationCost: {
        fee: 350,
        variable_fee_percentage: 10
      }
    },
    globalFeePercentage,
    result: {
      id: 1,
      client_name: 'Sureplex',
      fee: '350.00',
      gain: '2150.00',
      interest: '2500.00',
      is_available: true,
      isr: '0',
      perception: '102150.00',
      term_days: 60,
      total: '100000.00'
    }
  },
  getPublishEstimate: {
    sendengoStrategy,
    invoice: {
      id: 1,
      total: 100000,
      is_available: true,
      expiration: expirationEnd,
      number: '123-45',
      dataValues: {
        is_available: true
      },
      Operation: {
        annual_cost_percentage: 15
      },
      Client: {
        name: 'Sureplex'
      },
      get: function() {
        return this;
      },
      client_company_id: 4
    },
    estimates: {
      total: 1,
      reserve: 2,
      fundPayment: 3,
      operationCost: 4
    },
    diffDays: 10,
    percentages: {
    },
    queryParams: {
      where: {
        company_id: 4
      }
    },
    result: {
      is_available: true,
      client_name: 'Sureplex',
      number: '123-45',
      total: '1.00',
      operation_cost: '4.00',
      reserve: '2.00',
      fund_payment: '3.00'
    }
  },
  getInvestorFundDetail: {
    invoice: {
      Operation: {
        expiration_date: expiration,
        InvestorTransaction: {
          fund_request_date: today,
          days_limit: 89
        }
      }
    },
    result: {
      fund_date: today.toString(),
      expiration: expiration.toString(),
      operation_term: 89
    }
  },
  getInvoicePaymentSummary: {
    invoice: {
      company_rfc: rfc,
      total: 100000,
      Operation: {
        annual_cost_percentage: 15,
        reserve_percentage: 10,
        fd_commission_percentage: 0.5,
        reserve: 10000,
        fund_payment: 87459.73,
        commission: 81.94,
        operation_cost: 2540.27,
        fund_total: 97459.73,
        InvestorTransaction: {
          interest: 2458.33
        }
      },
      expiration: expiration,
      fund_date: today,
      get: getFunction
    },
    result: {
      iva: '16000.00',
      total: '100000.00',
      interest: '2458.33',
      commission: '81.94',
      fund_total: '97459.73',
      reserve: '10000.00',
      fund_payment: '87459.73',
      expiration_payment: '10000.00',
      operation_cost: '2540.27'
    }
  },
  getExpirableInvoices: {
    invoices: [
      _.cloneDeep(invoice),
      _.cloneDeep(invoice),
      _.cloneDeep(invoice),
      _.cloneDeep(invoice),
      _.cloneDeep(invoice),
      _.cloneDeep(invoice),
      invoiceExpiresNextDay
    ],
    profits: [
      {
        perception: 100
      }, {
        perception: 0
      }, {
        perception: -200
      }, {
        perception: 10
      }, {
        perception: -1
      }, {
        perception: 300
      }, {
        perception: 400
      }
    ],
    commonError,
    expiredInvoiceError: sequelizeError('expiration', 'invalid date'),
    fee: 250,
    taxpayerType: 'physical',
    findAll: {
      where: {
        status: [ 'APPROVED', 'PUBLISHED' ]
      },
      include: [
        {
          model: Operation, required: true,
          where: {
            $and: [
              { status: 'FUNDED' },
              { status: { $notIn: [ 'EXPIRED' ] } }
            ]
          }
        }
      ]
    },
    unprofitableInvoicesCount: 2,
    expiredInvoicesCount: 1
  },
  getAdminBasicInfo: {
    invoice: {
      Company: {
        id: companyId,
        name: company.name,
        color: company.color,
        business_name: 'ASD'
      },
      Client: {
        id: companyId,
        name: company.name,
        color: company.color,
        business_name: 'QWE'
      },
      Operation: {
        id: 1,
        status: 'PENDING'
      },
      id: 1,
      number: 'ASD123',
      emission_date: today,
      expiration: today,
      uuid: 'ASD',
      status: 'ASD',
      total: 100
    },
    result: {
      company_id: companyId,
      company_name: company.name,
      company_color: company.color,
      company_business_name: 'ASD',
      client_company_id: companyId,
      client_name: company.name,
      client_color: company.color,
      client_business_name: 'QWE',
      number: 'ASD123',
      emission_date: today.toString(),
      expiration: today.toString(),
      uuid: 'ASD',
      status: 'asd',
      total: '100.00',
      id: 1,
      operation_id: 1
    }
  },
  getAdminCxcPayment: {
    sendengoStrategy,
    invoice: {
      fund_date: today,
      expiration,
      total: 25000,
      Operation: {
        annual_cost_percentage: 15,
        reserve_percentage: 10,
        fd_commission_percentage: 0.5,
        annual_cost: 3750,
        reserve: 2500,
        fd_commission: 125,
        factorable: 22500,
        subtotal: 21572.92,
        fund_payment: 21542.02,
        commission: 30.90,
        operation_cost: 957.98,
        fund_total: 24042.02,
        InvestorTransaction: {
          funded_amount: 25000,
          interest: 927.08,
          interest_percentage: 3.71,
          isr: 0,
          isr_percentage: 0,
          global_fee_percentage: '1/3',
          fee: 92.71,
          fee_type: 'Variable fee',
          earnings: 834.37,
          earnings_percentage: 3.34,
          perception: 25834.37
        }
      }
    },
    result: {
      annual_cost: '15.00',
      interest: '927.08',
      interest_percentage: '3.71',
      reserve: '2500.00',
      reserve_percentage: '10.00',
      fd_commission: '30.90',
      fd_commission_percentage: '0.12',
      total: '25000.00',
      fund_payment: '21542.02',
      expiration_payment: '2500.00'
    }
  },
  getAdminInvestorPayment: {
    invoice: {
      fund_date: today,
      expiration,
      total: 25000,
      Investor: {
        name: 'Investor'
      },
      Operation: {
        annual_cost_percentage: 15,
        reserve_percentage: 10,
        fd_commission_percentage: 0.5,
        annual_cost: 3750,
        reserve: 2500,
        fd_commission: 125,
        factorable: 22500,
        subtotal: 21572.92,
        fund_payment: 21542.02,
        commission: 30.90,
        operation_cost: 957.98,
        fund_total: 24042.02,
        InvestorTransaction: {
          funded_amount: 25000,
          interest: 927.08,
          interest_percentage: 3.71,
          isr: 0,
          isr_percentage: 0,
          global_fee_percentage: '1/3',
          fee: 92.71,
          fee_type: 'Variable fee',
          earnings: 834.37,
          earnings_percentage: 3.34,
          perception: 25834.37
        }
      }
    },
    invoicePhysical: {
      fund_date: today,
      expiration,
      total: 25000,
      Investor: {
        name: 'Investor',
        taxpayer_type: 'physical'
      },
      Operation: {
        annual_cost_percentage: 15,
        reserve_percentage: 10,
        fd_commission_percentage: 0.5,
        annual_cost: 3750,
        reserve: 2500,
        fd_commission: 125,
        factorable: 22500,
        subtotal: 21572.92,
        fund_payment: 21542.02,
        commission: 30.90,
        operation_cost: 957.98,
        fund_total: 24042.02,
        InvestorTransaction: {
          funded_amount: 25000,
          interest: 927.08,
          interest_percentage: 3.71,
          isr: 185.42,
          isr_percentage: 0.74,
          global_fee_percentage: '1/3',
          fee: 92.71,
          fee_type: 'Variable fee',
          earnings: 834.37,
          earnings_percentage: 3.34,
          perception: 25834.37
        }
      }
    },
    result: {
      investor_name: 'Investor',
      fund_date: today.toString(),
      fund_total: '25000.00',
      earnings: '927.08',
      earnings_percentage: '3.71',
      gain: '834.37',
      gain_percentage: '3.34',
      fee: '92.71',
      fee_percentage: '0.37',
      fideicomiso_fee: '',
      fideicomiso_fee_percentage: '',
      isr: '0.00',
      total_payment: '25834.37',
      include_isr: false
    },
    resultPhysical: {
      investor_name: 'Investor',
      fund_date: today.toString(),
      fund_total: '25000.00',
      earnings: '927.08',
      earnings_percentage: '3.71',
      gain: '834.37',
      gain_percentage: '3.34',
      fee: '92.71',
      fee_percentage: '0.37',
      fideicomiso_fee: '',
      fideicomiso_fee_percentage: '',
      isr: '185.42',
      total_payment: '25834.37',
      include_isr: true
    }
  },
  getAdminOperationSummary: {
    invoice: {
      fund_date: today,
      expiration,
      total: 25000,
      Investor: {
        name: 'Investor'
      },
      Operation: {
        days_limit: 89,
        annual_cost_percentage: 15,
        reserve_percentage: 10,
        fd_commission_percentage: 0.5,
        annual_cost: 3750,
        reserve: 2500,
        fd_commission: 125,
        factorable: 22500,
        subtotal: 21572.92,
        fund_payment: 21542.02,
        commission: 30.90,
        operation_cost: 957.98,
        fund_total: 24042.02,
        InvestorTransaction: {
          funded_amount: 25000,
          interest: 927.08,
          interest_percentage: 3.71,
          isr: 0,
          isr_percentage: 0,
          global_fee_percentage: '1/3',
          fee: 92.71,
          fee_type: 'Variable fee',
          earnings: 834.37,
          earnings_percentage: 3.34,
          perception: 25834.37,
          days_limit: 89
        }
      }
    },
    result: {
      fund_date: today.toString(),
      expiration: expiration.toString(),
      operation_term: '89.00',
      commission: '30.90',
      fee: '92.71',
      earnings_fd: '123.61'
    }
  },
  updatePaymentDueInvoices: {
    invoices: [ invoice ],
    commonError,
    query: [ {
      where: {
        expiration: {
          $lte: yesterday
        }
      },
      include: [
        { model: Company, as: 'Company' },
        { model: Operation, where: { status: [ 'FUNDED' ] } }
      ]
    } ]
  },
  createOperation: {
    sendengoStrategy,
    user: {
      id: 2
    },
    invoice: {
      id: 1,
      client_company_id: companyId,
      total: 8374,
      tax_total: 160,
      subtotal: 1000,
      expiration: sixtyDays,
      published: today
    },
    operationResponse: [ operation ],
    operation,
    costs: {
      annual_cost: 15,
      reserve: 10,
      fd_commission: 0.5
    },
    userQuery: [ {
      where: {
        company_id: companyId
      }
    } ],
    commonError
  },
  createInvestorTransaction: {
    user: {
      id: 2,
      Company: {
        taxpayer_type: 'MORAL',
        id: 2
      }
    },
    invoice: {
      id: 1,
      client_company_id: companyId,
      total: 8374,
      tax_total: 160,
      subtotal: 1000,
      fund_date: today,
      expiration: sixtyDays,
      published: today,
      Operation: {
        id: 1,
        fd_commission_percentage: 0.5,
        reserve_percentage: 2,
        annual_cost_percentage: 15
      }
    },
    investorTransactionResponse: [ investorTransaction ],
    investorTransaction,
    fideicomisoTransactionResponse: [ fideicomisoTransaction ],
    fideicomisoTransaction,
    costs: {
      fee: 350,
      variable_fee_percentage: 10
    },
    fideicomisoPercentages: {
      fee: 350,
      variable_fee_percentage: 10,
      fideicomiso_fee: 150,
      fideicomiso_variable_fee: 3
    },
    global_fee_percentage: globalFeePercentage,
    userQuery: [ {
      where: {
        company_id: 2
      }
    } ],
    commonError
  },
  removeFundRequest: {
    invoice: {
      id: 1,
      client_company_id: companyId,
      total: 8374,
      tax_total: 160,
      subtotal: 1000,
      fund_date: today,
      expiration: sixtyDays,
      published: today,
      save: function() {
        return this;
      },
      Operation: {
        id: 1,
        fd_commission_percentage: 0.5,
        reserve_percentage: 2,
        annual_cost_percentage: 15
      }
    },
    transaction: undefined,
    transactionParams,
    destroyQuery: [
      { where: { operation_id: 1 }, transaction: undefined }
    ],
    commonError
  },
  getNewestInvoices: {
    transaction: undefined,
    transactionParams,
    invoices: [ newInvoice ],
    invoicesQuery: [ {
      where: {
        status: [ 'PUBLISHED' ],
        published: {
          $between: [ yesterdayAt2PM, todayAt2PM ]
        }
      },
      include: [
        { model: Operation, where: { status: [ 'PENDING' ] } }
      ]
    } ],
    commonError
  },
  getTrackerInfo: {
    responseOfQuery: [ {} ],
    mapped: [ {} ],
    sequelizeError
  },
  getNotifiableInvoices: {
    invoices: [ newInvoice ],
    invoicesQuery: [ {
      where: {
        status: [ 'PUBLISHED' ],
        expiration: {
          $between: [ eightDaysInitial, eightDaysFinal ]
        }
      },
      include: [
        { model: Operation, where: { status: [ 'FUNDED' ] } }
      ]
    } ],
    commonError
  }
};
