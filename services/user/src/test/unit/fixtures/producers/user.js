const _ = require('lodash');
const moment = require('/var/lib/core/js/moment');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const rfc = 'FODA900806965';
const companyId = 1;
const today = new Date();
const ValidDummyEvent = {
  message: {
    type: 'dummyEvent',
    body: {
      username: 'SYSTEM'
    },
    guid: 'dummy'
  },
  key: 'DLMD'
};
const user = {
  id: 1,
  username: 'test',
  email: 'test@email.com'
};
const company = {
  id: companyId,
  rfc,
  name: 'A company',
  business_name: 'A company S.A. de C.V.',
  holder: 'A company S.A. de C.V.',
  bank: 'BANORTE',
  bank_account: '70035593643',
  clabe: '072090700355936431',
  isFideicomiso: false
};
const proposed = {
  business_name: 'business_name',
  type: 'Proveedor',
  contact_name: 'contact_name',
  position: 'position',
  email: 'email',
  phone: 'phone'
};
const date = new Date();
const reason = 'A problem';
const invoice = {
  id: 1,
  number: 'ABC123',
  total: 5000,
  uuid: 'UUID-ANCD-1424',
  company_id: companyId,
  client_company_id: 2,
  investor_company_id: 3,
  expiration: date.toString(),
  getClient: function() {
    return company;
  },
  getCompany: function() {
    return company;
  },
  getInvestor: function() {
    return company;
  },
  getBasicInfo: function() {
    return;
  },
  getFundEstimate: function() {
    return;
  },
  getInvestorFundEstimate: function() {
    return;
  },
  Company: {
    name: 'A company',
    rfc,
    id: companyId
  },
  fund_payment: 100
};
const sendengoInvoice = {
  id: 1,
  number: 'ABC123',
  company_id: 12000,
  client_company_id: 2,
  investor_company_id: 3,
  expiration: date.toString(),
  getClient: function() {
    return company;
  },
  getInvestor: function() {
    return company;
  },
  getFundEstimate: function() {
    return;
  },
  Company: {
    name: 'A company',
    rfc,
    id: companyId
  },
  fund_payment: 100
};
const basicOperationInfoInvoices = [
  { id: 1, operation_id: 10 },
  { id: 2, operation_id: 20 }
];
const movement = {
  id: 1,
  amount: 100
};
const companyRole = 'ADMIN';
const token = {
  token: 'token',
  email: 'john@doe.com',
  data: {
    username: 'John Doe'
  }
};
const pendingTransaction = {
  company_id: companyId,
  type: 'DEPOSIT',
  amount: 100,
  id: 1
};
const invitationBody = {
  token: token.token,
  email: token.email,
  username: token.data.name,
  company_role: companyRole
};
const invitationCreatedEvent = {
  message: {
    type: 'InvitationCreated',
    body: invitationBody,
    guid
  },
  key: token.token
};
const resendInvitationEvent = {
  message: {
    type: 'ResendInvitation',
    body: {
      token: token.token,
      email: token.email,
      username: token.data.name,
      company_role: companyRole
    },
    guid
  },
  key: token.token
};
const passwordChangedEvent = {
  message: {
    type: 'PasswordChanged',
    body: {
      username: user.name,
      email: user.email
    },
    guid
  },
  key: user.id
};
const recoverPasswordEvent = {
  message: {
    type: 'RecoverPassword',
    body: {
      token: token.token,
      email: token.email,
      username: token.data.user_name
    },
    guid
  },
  key: token.token
};
const invoiceCreatedEvent = {
  message: {
    type: 'InvoiceCreated',
    body: {
      emails: user.email,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      company_name: company.name
    },
    guid
  },
  key: invoice.id
};
const invoiceApprovedEvent = {
  message: {
    type: 'InvoiceApproved',
    body: {
      emails: user.email,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      client_name: company.name
    },
    guid
  },
  key: invoice.id
};
const invoiceRejectedEvent = {
  message: {
    type: 'InvoiceRejected',
    body: {
      emails: user.email,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      client_name: company.name,
      reason: 'Valid reason'
    },
    guid
  },
  key: invoice.id
};
const publishedInvoiceRejectedEvent = {
  message: {
    type: 'PublishedInvoiceRejected',
    body: {
      emails: user.email,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      reason: 'Valid reason'
    },
    guid
  },
  key: invoice.id
};
const fundRequestedInvoiceRejectedEvent = {
  message: {
    type: 'FundRequestedInvoiceRejected',
    body: {
      emails: user.email,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      reason: 'Valid reason'
    },
    guid
  },
  key: invoice.id
};
const fundRequestedInvoiceApprovedEvent = {
  message: {
    type: 'FundRequestedInvoiceApproved',
    body: {
      cxc_emails: user.email,
      cxp_emails: user.email,
      investor_emails: user.email,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      invoice_expiration: invoice.expiration,
      invoice_company_name: invoice.Company.name,
      invoice_company_id: invoice.Company.id,
      cxc_payment: invoice.fund_payment.toFixed(2),
      bank_info: {
        holder: company.holder,
        bank: company.bank,
        bank_account: company.bank_account,
        clabe: company.clabe
      }
    },
    guid
  },
  key: invoice.id
};
const withdrawCreatedEvent = {
  message: {
    type: 'WithdrawCreated',
    body: {
      emails: user.email,
      mailerFlagFideicomiso: false,
      company_name: company.name,
      company_id: company.id
    },
    guid
  },
  key: movement.id
};
const invoiceFundRequestEvent = {
  message: {
    type: 'InvoiceFundRequest',
    body: {
      emails: user.email,
      invoices: basicOperationInfoInvoices,
      investor_name: company.name
    },
    guid
  },
  key: movement.id
};
const fundEstimate = {
  total: 1000,
  interest: 234,
  fee: 50,
  include_isr: false
};
const users = [ {
  name: 'foo',
  email: 'foo@candy.bar'
},
{
  name: 'baz',
  email: 'buzz@candy.bar'
} ];
const attachments = [ {
  filename: 'Anexo_A_1_1541453226336.pdf',
  path: 'S3 Super URL'
} ];

const now = moment();
const id = Number(now);

const requested_at = {
  day: now.format('D'),
  month: now.format('MMMM'),
  time: now.format('HH:mm')
};

const approvedInvoicesTable = `
  <table class="invoices-table">
    <tr>
      <th>Proveedor</th>
      <th>Factura</th>
      <th>Folio Fiscal</th>
      <th>Monto</th>
      <th>Fecha de pago</th>
    </tr>
    <tr>
      <td>A company S.A. de C.V.</td>
      <td>ABC123</td>
      <td>UUID-ANCD-1424</td>
      <td>$5,000.00</td>
      <td>22 de noviembre de 2018</td>
    </tr>
  </table>`;

const notifyInvoiceApprovedParams = {
  template: 'cxp_approve',
  locals: {
    TABLE_INVOICES: approvedInvoicesTable
  }
};
const notifyInvoiceApprovedKey = `companies/${invoice.company_id}/annexums/cxp_${id}.pdf`;

const notifyFundRequestPDFParams = {
  template: 'investor_fund',
  locals:
  {
    ANEXO_NUMERO: 1541457406697,
    NO_FACTURA: 'ABC123',
    NOMBRE_PROVEEDOR: 'A company S.A. de C.V.',
    NOMBRE_INVERSIONISTA: 'A company S.A. de C.V.',
    PERSONA_FISICA_NOMBRE_O_RAZON_SOCIAL: 'A company S.A. de C.V.',
    FECHA_FACTURA: '5 de noviembre de 2018',
    FECHA_PAGO_FACTURA: '15 de noviembre de 2018',
    MONTO_FACTURA: '1,000.00',
    COSTO_INTERES: '234.00',
    COMISION_FACTURA: '50.00',
    INCLUDE_ISR: false,
    RETENCION_ISR: null }
};

const fundRequestKeyId = notifyFundRequestPDFParams.locals.ANEXO_NUMERO;
const notifyFundRequestKey = `companies/${invoice.company_id}/annexums/inv_${fundRequestKeyId}.pdf`;

const notifyFundRequestEvent = {
  message: {
    type: 'notifyFundRequest',
    body: {
      recipients: users,
      mailerFlagFideicomiso: false,
      attachments: attachments,
      investor_name: 'Zum Zum',
      requested_at
    },
    guid
  },
  key: companyId
};

const notifyInvoiceApprovedEvent = {
  message: {
    type: 'NotifyInvoiceApproved',
    body: {
      recipients: users,
      attachments: attachments,
      invoices: [ invoice ],
      requested_at
    },
    guid
  },
  key: companyId
};
const depositCreatedEvent = {
  message: {
    type: 'DepositCreated',
    body: {
      emails: user.email,
      company_name: company.name,
      mailerFlagFideicomiso: false,
      company_id: company.id
    },
    guid
  },
  key: movement.id
};
const pendingTransactionApprovedEvent = {
  message: {
    type: 'PendingTransactionApproved',
    body: {
      emails: user.email,
      mailerFlagFideicomiso: false,
      type: pendingTransaction.type,
      amount: String(pendingTransaction.amount)
    },
    guid
  },
  key: pendingTransaction.id
};
const pendingTransactionRejectedEvent = {
  message: {
    type: 'PendingTransactionRejected',
    body: {
      emails: user.email,
      type: pendingTransaction.type,
      mailerFlagFideicomiso: false,
      reason,
      amount: String(pendingTransaction.amount)
    },
    guid
  },
  key: pendingTransaction.id
};
const clientInvoicePaymentCreatedEvent = {
  message: {
    type: 'ClientInvoicePaymentCreated',
    body: {
      emails: user.email,
      mailerFlagFideicomiso: false,
      client_name: company.name,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      amount: 100
    },
    guid
  },
  key: invoice.id
};
const companyProposedEvent = {
  message: {
    type: 'CompanyProposed',
    body: {
      emails: user.email,
      proposer_name: 'name',
      business_name: proposed.business_name,
      type: proposed.type,
      contact_name: proposed.contact_name,
      position: proposed.position,
      email: proposed.email,
      phone: proposed.phone
    },
    guid
  },
  key: 'name'
};
const invoiceExpiredEvent = {
  message: {
    type: 'InvoiceExpired',
    body: {
      emails: user.email,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      client_name: company.name
    },
    guid
  },
  key: invoice.id
};
const companyRoleSuspensionUpdatedEvent = {
  message: {
    type: 'CompanyRoleSuspensionUpdated',
    body: {
      emails: user.email,
      company_id: companyId,
      suspended: false,
      role: 'CXC'
    },
    guid
  },
  key: companyId
};
const investorRoleSuspensionUpdatedEvent = {
  message: {
    type: 'InvestorRoleSuspensionUpdated',
    body: {
      emails: user.email,
      company_id: companyId,
      suspended: false
    },
    guid
  },
  key: companyId
};
const invoicePaymentDueEvent = {
  message: {
    type: 'InvoicePaymentDue',
    body: {
      emails: user.email,
      invoice_id: invoice.id,
      invoice_number: invoice.number,
      company_name: invoice.Company.name
    },
    guid
  },
  key: invoice.id
};
const invoiceCompletedEvent = {
  message: {
    type: 'InvoiceCompleted',
    body: {
      cxp_emails: user.email,
      cxc_emails: user.email,
      investor_emails: user.email,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxp_payment: '40.00',
      cxc_payment: '30.00',
      investor_payment: '50.00'
    },
    guid
  },
  key: invoice.id
};
const invoiceCompletedEventSendengoCase = {
  message: {
    type: 'InvoiceCompleted',
    body: {
      cxp_emails: user.email,
      cxc_emails: '',
      investor_emails: user.email,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxp_payment: '40.00',
      cxc_payment: '0.00',
      investor_payment: '50.00'
    },
    guid
  },
  key: invoice.id
};
const invoiceLostEvent = {
  message: {
    type: 'InvoiceLost',
    body: {
      cxc_emails: user.email,
      investor_emails: user.email,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxc_payment: '30.00',
      investor_payment: '50.00',
      payment_date: today
    },
    guid
  },
  key: invoice.id
};
const invoiceLatePaymentEvent = {
  message: {
    type: 'InvoiceLatePayment',
    body: {
      cxc_emails: user.email,
      investor_emails: user.email,
      invoice_number: invoice.number,
      invoice_id: invoice.id,
      cxc_payment: '30.00',
      investor_payment: '50.00'
    },
    guid
  },
  key: invoice.id
};
const notifyNewInvoicesEvent = {
  message: {
    type: 'NotifyNewInvoices',
    users: {
      investors: [ user ]
    },
    guid
  },
  key: user.id
};
const newInvoiceToken = {
  token: 'token'
};
const createNewInvoiceTokenParams = _.cloneDeep(user);
const createNewInvoiceTokenFailingParams = _.cloneDeep(user);

createNewInvoiceTokenParams.token = newInvoiceToken.token;

const messageParams = (event) => {
  return [
    'Producing user event',
    event,
    'Event',
    guid
  ];
};

const messageDummyParams = (event) => {
  return [
    'Producing dummy event',
    event,
    'Event'
  ];
};

module.exports = {
  dummyEvent: {
    ValidDummyEvent,
    logMessageParams: messageDummyParams(ValidDummyEvent)
  },
  invitationCreated: {
    companyRole,
    guid,
    token,
    company: {
      role: 'ADMIN',
      isFideicomiso: false
    },
    logMessageParams: messageParams(invitationCreatedEvent),
    event: invitationCreatedEvent
  },
  resendInvitation: {
    companyRole,
    company: {
      role: 'ADMIN',
      isFideicomiso: false
    },
    guid,
    token,
    logMessageParams: messageParams(resendInvitationEvent),
    event: resendInvitationEvent
  },
  passwordChanged: {
    guid,
    user,
    logMessageParams: messageParams(passwordChangedEvent),
    event: passwordChangedEvent
  },
  recoverPassword: {
    guid,
    token,
    logMessageParams: messageParams(recoverPasswordEvent),
    event: recoverPasswordEvent
  },
  invoiceCreated: {
    guid,
    users: [ user ],
    client: company,
    invoice,
    logMessageParams: [
      'Producing user event',
      invoiceCreatedEvent,
      'Event',
      guid
    ],
    event: invoiceCreatedEvent,
    userQuery: {
      where: {
        role: 'CXP',
        company_id: invoice.client_company_id
      },
      attributes: [ 'email' ],
      raw: true
    },
    companyWhere: {
      where: {
        id: invoice.company_id
      }
    }
  },
  invoiceApproved: {
    guid,
    users: [ user ],
    client: company,
    invoice,
    logMessageParams: [
      'Producing user event',
      invoiceApprovedEvent,
      'Event',
      guid
    ],
    event: invoiceApprovedEvent,
    userQuery: {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  invoiceFundRequest: {
    guid,
    users: [ user ],
    company,
    basicOperationInfoInvoices,
    logMessageParams: [
      'Producing user event',
      invoiceFundRequestEvent,
      'Event',
      guid
    ],
    event: invoiceFundRequestEvent,
    userQuery: {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  invoiceRejected: {
    guid,
    users: [ user ],
    client: company,
    invoice,
    logMessageParams: [
      'Producing user event',
      invoiceRejectedEvent,
      'Event',
      guid
    ],
    reason: invoiceRejectedEvent.message.body.reason,
    event: invoiceRejectedEvent,
    userQuery: {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  publishedInvoiceRejected: {
    guid,
    users: [ user ],
    client: company,
    invoice,
    logMessageParams: [
      'Producing user event',
      publishedInvoiceRejectedEvent,
      'Event',
      guid
    ],
    reason: publishedInvoiceRejectedEvent.message.body.reason,
    event: publishedInvoiceRejectedEvent,
    userQuery: {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  fundRequestedInvoiceRejected: {
    guid,
    users: [ user ],
    investor: {
      isFideicomiso: false
    },
    companyQuery: {
      where: {
        id: invoice.investor_company_id
      }
    },
    invoice,
    investorId: invoice.investor_company_id,
    logMessageParams: [
      'Producing user event',
      fundRequestedInvoiceRejectedEvent,
      'Event',
      guid
    ],
    reason: fundRequestedInvoiceRejectedEvent.message.body.reason,
    event: fundRequestedInvoiceRejectedEvent,
    userQuery: {
      where: {
        role: 'INVESTOR',
        company_id: invoice.investor_company_id
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  fundRequestedInvoiceApproved: {
    guid,
    users: [ user ],
    invoice,
    company,
    payment: invoice.fund_payment,
    logMessageParams: [
      'Producing user event',
      fundRequestedInvoiceApprovedEvent,
      'Event',
      guid
    ],
    event: fundRequestedInvoiceApprovedEvent,
    cxcQuery: [ {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    } ],
    cxpQuery: [ {
      where: {
        role: 'CXP',
        company_id: invoice.client_company_id
      },
      attributes: [ 'email' ],
      raw: true
    } ],
    investorQuery: [ {
      where: {
        role: 'INVESTOR',
        company_id: invoice.investor_company_id
      },
      attributes: [ 'email' ],
      raw: true
    } ],
    adminCompanyQuery: [ {
      where: {
        role: 'ADMIN'
      }
    } ]
  },
  withdrawCreated: {
    guid,
    users: [ user ],
    company,
    withdraw: movement,
    logMessageParams: [
      'Producing user event',
      withdrawCreatedEvent,
      'Event',
      guid
    ],
    event: withdrawCreatedEvent,
    userQuery: {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  depositCreated: {
    guid,
    users: [ user ],
    company,
    deposit: movement,
    logMessageParams: [
      'Producing user event',
      depositCreatedEvent,
      'Event',
      guid
    ],
    event: depositCreatedEvent,
    userQuery: {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  pendingTransactionApproved: {
    guid,
    users: [ user ],
    company,
    pendingTransaction,
    logMessageParams: [
      'Producing user event',
      pendingTransactionApprovedEvent,
      'Event',
      guid
    ],
    event: pendingTransactionApprovedEvent,
    userQuery: {
      where: {
        company_rfc: pendingTransaction.company_rfc
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  pendingTransactionRejected: {
    guid,
    reason,
    companyQuery: {
      where: {
        id: pendingTransaction.company_id
      }
    },
    company,
    investor: {
      isFideicomiso: false
    },
    users: [ user ],
    pendingTransaction,
    logMessageParams: [
      'Producing user event',
      pendingTransactionRejectedEvent,
      'Event',
      guid
    ],
    event: pendingTransactionRejectedEvent,
    userQuery: {
      where: {
        company_id: pendingTransaction.company_id
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  clientInvoicePaymentCreated: {
    guid,
    users: [ user ],
    amount: 100,
    invoice,
    company,
    clientName: company.name,
    logMessageParams: [
      'Producing user event',
      clientInvoicePaymentCreatedEvent,
      'Event',
      guid
    ],
    event: clientInvoicePaymentCreatedEvent,
    userQuery: {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    },
    companyQuery: {
      where: {
        id: invoice.investor_company_id
      }
    }
  },
  invoiceExpired: {
    guid,
    users: [ user ],
    client: company,
    invoice,
    logMessageParams: [
      'Producing user event',
      invoiceExpiredEvent,
      'Event',
      guid
    ],
    event: invoiceExpiredEvent,
    userQuery: {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  companyRoleSuspensionUpdated: {
    guid,
    id: companyId,
    role: 'CXC',
    suspended: false,
    users: [ user ],
    logMessageParams: [
      'Producing user event',
      companyRoleSuspensionUpdatedEvent,
      'Event',
      guid
    ],
    event: companyRoleSuspensionUpdatedEvent,
    userQuery: {
      where: {
        role: 'CXC',
        company_id: companyId
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  investorRoleSuspensionUpdated: {
    guid,
    id: companyId,
    suspended: false,
    users: [ user ],
    logMessageParams: [
      'Producing user event',
      investorRoleSuspensionUpdatedEvent,
      'Event',
      guid
    ],
    event: investorRoleSuspensionUpdatedEvent,
    userQuery: {
      where: {
        role: 'INVESTOR',
        company_id: companyId
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  invoicePaymentDue: {
    guid,
    users: [ user ],
    invoice,
    logMessageParams: [
      'Producing user event',
      invoicePaymentDueEvent,
      'Event',
      guid
    ],
    event: invoicePaymentDueEvent,
    userQuery: {
      where: {
        $or: [ {
          role: 'CXP',
          company_id: invoice.client_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  invoiceCompleted: {
    guid,
    users: [ user ],
    invoice,
    sendengoInvoice,
    logMessageParams: [
      'Producing user event',
      invoiceCompletedEvent,
      'Event',
      guid
    ],
    logMessageParamsSendengo: [
      'Producing user event',
      invoiceCompletedEventSendengoCase,
      'Event',
      guid
    ],
    event: invoiceCompletedEvent,
    eventSendengo: invoiceCompletedEventSendengoCase,
    cxpQuery: {
      where: {
        $or: [ {
          role: 'CXP',
          company_id: invoice.client_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    cxcQuery: {
      where: {
        $or: [ {
          role: 'CXC',
          company_id: invoice.company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    investorQuery: {
      where: {
        $or: [ {
          role: 'INVESTOR',
          company_id: invoice.investor_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    cxcPayment: 30,
    cxpPayment: 40,
    sendengoCxcPayment: 0,
    investorPayment: 50
  },
  invoiceLost: {
    guid,
    users: [ user ],
    invoice,
    logMessageParams: [
      'Producing user event',
      invoiceLostEvent,
      'Event',
      guid
    ],
    event: invoiceLostEvent,
    cxcQuery: {
      where: {
        $or: [ {
          role: 'CXC',
          company_id: invoice.company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    investorQuery: {
      where: {
        $or: [ {
          role: 'INVESTOR',
          company_id: invoice.investor_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    cxcTransaction: {
      amount: 30,
      created_at: today
    },
    investorTransaction: {
      amount: 50,
      created_at: today
    }
  },
  invoiceLatePayment: {
    guid,
    users: [ user ],
    invoice,
    logMessageParams: [
      'Producing user event',
      invoiceLatePaymentEvent,
      'Event',
      guid
    ],
    event: invoiceLatePaymentEvent,
    cxcQuery: {
      where: {
        $or: [ {
          role: 'CXC',
          company_id: invoice.company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    investorQuery: {
      where: {
        $or: [ {
          role: 'INVESTOR',
          company_id: invoice.investor_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    },
    cxcTransaction: {
      amount: 30,
      created_at: today
    },
    investorTransaction: {
      amount: 50,
      created_at: today
    }
  },
  companyProposed: {
    guid,
    proposed,
    users: [ user ],
    name: 'name',
    logMessageParams: [
      'Producing user event',
      companyProposedEvent,
      'Event',
      guid
    ],
    event: companyProposedEvent,
    userQuery: {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    }
  },
  notifyNewInvoices: {
    guid,
    tokens: newInvoiceToken,
    invoices: [ invoice ],
    createNewInvoiceTokenParams,
    createNewInvoiceTokenFailingParams,
    users: [ user ],
    logMessageParams: [
      'Producing user event',
      notifyNewInvoicesEvent,
      'Event',
      guid
    ],
    event: notifyNewInvoicesEvent,
    userQuery: {
      where: {
        role: {
          $iLike: 'INVESTOR'
        }
      },
      attributes: [ 'name', 'email' ],
      raw: true
    }
  },
  notifyFundRequest: {
    invoices: [ invoice ],
    users,
    fundEstimate,
    company,
    s3Url: 'S3 Super URL',
    guid,
    event: notifyFundRequestEvent,
    notifyFundRequestKey,
    notifyFundRequestPDFParams,
    type: 'NotifyFundRequest'
  },
  notifyInvoiceApproved: {
    invoices: [ invoice ],
    users,
    company,
    s3Url: 'S3 Super URL',
    guid,
    event: notifyInvoiceApprovedEvent,
    notifyInvoiceApprovedKey,
    notifyInvoiceApprovedParams,
    type: 'NotifyInvoiceApproved'
  }
};
