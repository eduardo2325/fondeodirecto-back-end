const { commonError } = require('../../common/fixtures/errors');
const guid = 'guid';
const wb = 'wb';
const ws = 'ws';
const idsPendingTransactions = {};
const reportData = {};
const sequelizeTransaction = {};
const created = { id: 1 };
const generatedAt = new Date();

module.exports = {
  uploadCxcExcelReportDataToS3: {
    guid,
    arrayOfArrays: [ [ ] ],
    commonError,
    buf: {},
    s3Url: 's3Url',
    key: 'excel-bank-report.xlsx',
    options: {
      ACL: 'public-read',
      ContentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    },
    secondParameterWrite: { type: 'buffer', bookType: 'xlsx' },
    wb,
    ws
  },
  savePendingTransactionsToBankReport: {
    idsPendingTransactions,
    commonError,
    reportData,
    sequelizeTransaction,
    created,
    createParams: [ reportData, { transaction: sequelizeTransaction } ],
    updateParams: [
      { bank_report_id: 1 },
      {
        where: {
          id: { $in: idsPendingTransactions }
        },
        transaction: sequelizeTransaction
      }
    ]
  },
  findNumberOfApprovedTransactions: {
    reports: [ { id: 1, generated_at: generatedAt, total_generated: 3,
      total_amount: 1, link: 'url', User: { name: 'name' } } ],
    count: 2,
    response: [
      {
        bank_report_id: 1,
        no_transactions: '3/2',
        amount: '1.00',
        emmission_date: generatedAt.toString(),
        url: 'url',
        generated_by: 'name'
      }
    ],
    commonError,
    countParams: {
      where: { bank_report_id: 1, status: 'APPROVED' }
    }
  }
};
