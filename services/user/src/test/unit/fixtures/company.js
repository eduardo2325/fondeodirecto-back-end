const sequelize = require('sequelize');
const moment = require('/var/lib/core/js/moment');
const { commonError, notFound, sequelizeError } = require('../../common/fixtures/errors');
const { User, Token } = require('../../../models');
const rfc = 'FODA900806965';
const companyId = 1;
const bankInfo = {
  name: 'BANAMEX',
  account: '70035593643'
};
const clabe = '002090700355936431';
const company = {
  id: companyId,
  rfc,
  role: 'ADMIN',
  name: 'Fondeo Directo',
  business_name: 'Fondeo Directo',
  holder: 'Fondeo Directo',
  clabe,
  description: 'description'
};
const fullInformation = {
  rfc,
  role: company.role,
  name: company.name,
  business_name: company.business_name,
  holder: company.holder,
  clabe,
  bank: bankInfo.name,
  bank_account: bankInfo.account,
  description: company.description
};
const countCompany = {
  get: () => {
    return {
      rfc,
      name: company.name,
      business_name: company.business_name,
      holder: company.holder,
      clabe,
      user_count: 1,
      invitation_count: 1,
      status: 'active',
      Users: [],
      Tokens: []
    };
  }
};
const companyElement = {
  rfc,
  name: company.name,
  business_name: company.business_name,
  holder: company.holder,
  clabe,
  status: 'active',
  user_count: 1,
  invitation_count: 1
};
const usersList = {
  users: [ {
    name: 'John Doe',
    email: 'john@doe.com',
    color: '#1234',
    role: 'cxc',
    status: 'active'
  }, {
    name: 'Jane Doe',
    email: 'jane@doe.com',
    color: '#4321',
    role: 'cxc',
    status: 'pending'
  } ]
};
const userCount = [
  sequelize.literal('(SELECT COUNT(*) FROM users WHERE users.deleted_at IS NULL ' +
            'AND users.company_id = \"Company\".\"id\")'),
  'user_count' ];
const invitationCount = [
  sequelize.literal(
    '(SELECT COUNT(*) FROM tokens WHERE tokens.type = \'registration\'' +
'AND tokens.company_id = \"Company\".\"id\"' +
'AND tokens.deleted_at IS NULL)'
  ),
  'invitation_count' ];

const transaction = {
  id: 1
};

const allowedLowerBound = moment().subtract(1, 'months');
const allowedUpperBound = moment();
const momentSubtractedOneYear = moment().subtract(1, 'years');

module.exports = {
  beforeCreate: {
    instance: {
      clabe,
      rfc: 'xmpl12345678',
      setDataValue() {
        throw new Error('Please stub me!');
      }
    },
    clabe,
    bankInfo,
    commonError
  },
  beforeUpdate: {
    instance: {
      clabe,
      rfc: 'xmpl12345678',
      setDataValue() {
        throw new Error('Please stub me!');
      }
    },
    clabe,
    bankInfo,
    commonError
  },
  getInformation: {
    instance: {
      get: () => {
        return fullInformation;
      }
    },
    bankInfo,
    commonError,
    fullInformation
  },
  analyzeClabe: {
    bankInfo,
    clabe,
    incorrectClabe: '123456789',
    invalidFormat: {
      errors: [ {
        path: 'clabe',
        message: 'invalid clabe'
      } ]
    },
    invalidClabe: {
      errors: [ {
        path: 'clabe',
        message: 'invalid clabe'
      } ]
    },
    query: {
      where: {
        id: clabe.substring(0, 3)
      }
    },
    commonError
  },
  getCompaniesModel: {
    findAll: {
      attributes: {
        exclude: [ 'updated_at', 'role', 'bank', 'bank_account', 'isFideicomiso' ],
        include: [ userCount, invitationCount ]
      },
      include: [
        { model: User },
        { model: Token }
      ],
      order: [ [ 'created_at', 'ASC' ] ],
      group: [ 'Company.id' ]
    },
    findAllWithAttributes: {
      attributes: {
        exclude: [ 'updated_at', 'role', 'bank', 'bank_account', 'isFideicomiso' ],
        include: [ userCount, invitationCount ]
      },
      include: [
        { model: User },
        { model: Token }
      ],
      where: {
        role: 'COMPANY'
      },
      limit: 2,
      offset: 2,
      order: [ [ 'name', 'DESC' ] ],
      group: [ 'Company.id' ]
    },
    companies: [ countCompany ],
    clabe,
    bankInfo,
    companyElement,
    commonError,
    limit: 2,
    offset: 2,
    order_by: 'name',
    order_desc: true,
    role: 'COMPANY'
  },
  getUsersModel: {
    id: companyId,
    orderBy: 'name',
    orderDesc: 'ASC',
    companyUsers: {
      get: () => {
        return {
          Users: [ usersList[0] ]
        };
      }
    },
    tokens: [ usersList[1] ],
    usersList,
    orderedList: [
      usersList[1],
      usersList[0]
    ],
    userQuery: {
      where: {
        id: companyId
      },
      include: [ {
        model: User,
        attributes: [
          'name',
          'email',
          [ 'active', 'status' ],
          'role',
          'color'
        ]
      } ],
      order: [ [ '"Users.name"', 'ASC' ] ]
    },
    tokenQuery: {
      where: {
        company_id: companyId
      },
      attributes: [
        [ 'data.user_name', 'name' ],
        'email',
        [ 'pending', 'status' ],
        [ 'data.user_type', 'role' ],
        [ '#1234', 'color' ]
      ],
      order: [ [ 'data.user_name', 'ASC' ] ],
      raw: true
    },
    color: '#1234',
    noCompanyError: notFound('company'),
    commonError
  },
  validationFailed: {
    instance: {},
    options: {},
    commonError,
    clabeValidationError: {
      errors: [ {
        path: 'clabe'
      } ]
    },
    invalidClabeError: {
      errors: [ {
        path: 'clabe',
        message: 'invalid clabe'
      } ]
    }
  },
  getBalance: {
    instance: {
      balance: 100,
      rfc,
      id: companyId
    },
    pendingWithdraws: [
      {
        amount: 10
      },
      {
        amount: 20
      }
    ],
    balance: '100.00',
    decreasedBalance: '70.00',
    commonError,
    findAllQuery: {
      where: {
        company_id: companyId,
        status: 'PENDING',
        type: 'WITHDRAW'
      }
    }
  },
  setGet: {
    taxpayerType: 'physical',
    response: 'PHYSICAL',
    noTaxpayer: undefined,
    commonError,
    context: {
      setDataValue: () => {
        return true;
      },
      getDataValue: () => {
        return true;
      }
    },
    contextNotTaxpayer: {
      setDataValue: () => {
        return true;
      },
      getDataValue: () => {
        return undefined;
      }
    }
  },
  suspendedRoleValidateIsValidRole: {
    validArray: [ 'CXC', 'CXP' ],
    invalidArray: [ 'INVALID', 'CXC' ],
    invalidRoleError: new Error(sequelizeError('suspended_roles', 'invalid role'))
  },
  updateRoleSuspension: {
    context: {
      save: () => this
    },
    suspendedRoles: [ 'CXC' ],
    transaction,
    suspendSuspendedRole: [
      'CXC', true, transaction
    ],
    suspendUnsuspendedRole: [
      'CXP', true, transaction
    ],
    unsuspendSuspendedRole: [
      'CXC', false, transaction
    ],
    unsuspendUnsuspendedRole: [
      'CXP', false, transaction
    ],
    commonError,
    errorAlreadySuspended: sequelizeError('role', 'already suspended'),
    errorAlreadyUnsuspended: sequelizeError('role', 'already unsuspended')
  },
  allowedCXP: {
    contextSuspendedCXP: {
      suspended_roles: [ 'CXP' ]
    },
    contextUnsuspendedCXP: {
      suspended_roles: [ 'CXC' ]
    }
  },
  getCashFlowFromRange: {
    getCashFlowFromRangeContext: {
      id: 1
    },
    allowedLowerBound,
    allowedUpperBound,
    withdrawalsArray: [ { cash: 0 } ],
    depositsArray: [ { cash: 0 } ],
    momentSubtractedOneYear,
    result: {
      withdrawals: 0,
      deposits: 0
    }
  }
};
