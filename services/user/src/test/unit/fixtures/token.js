const userRoles = require('../../../config/user-roles');
const { commonError, notEmptyError, invalidValueError, notFound } = require('../../common/fixtures/errors');
const rfc = 'FODA900806965';
const companyId = 1;
const token = 'cgquRC7iJG21GHlm';
const expiration_time = 24 * (60 * 60 * 1000);
const user = {
  name: 'John Doe',
  email: 'john@doe.com',
  type: userRoles.publicRoles[0].value,
  companyId: companyId
};
const registrationToken = {
  email: user.email,
  type: 'registration',
  company_id: companyId,
  data: {
    user_name: user.name,
    user_type: user.type
  }
};
const generatedToken = {
  token,
  email: user.email,
  type: registrationToken.type,
  company_rfc: rfc,
  data: {
    user_name: user.name,
    user_type: user.type
  }
};
const company = {
  name: 'MegaCorp'
};
const recoverToken = {
  token,
  email: user.email,
  type: 'recover_password',
  data: {
    user_name: user.name
  },
  created_at: new Date()
};
const expiredRecoverToken = {
  token,
  email: user.email,
  type: 'recover_password',
  data: {
    user_name: user.name
  },
  created_at: new Date( new Date().getTime() - expiration_time)
};

module.exports = {
  beforeCreate: {
    instance: {},
    token
  },
  userRegistration: {
    user,
    companyRole: 'COMPANY',
    transaction: 'transaction-obj',
    query: registrationToken,
    generatedToken,
    commonError,
    emptyNameError: notEmptyError('name'),
    emptyTypeError: notEmptyError('type'),
    emptyCompanyIdError: notEmptyError('companyId'),
    invalidTypeError: invalidValueError('type')
  },
  getInvitationInfo: {
    instance: {
      get: (query) => {
        if (query === 'email') {
          return registrationToken.email;
        }

        return registrationToken;
      }
    },
    invitationInfo: {
      name: registrationToken.data.user_name,
      email: registrationToken.email,
      color: registrationToken.data.user_color,
      status: 'pending',
      company: {
        rfc: registrationToken.company_rfc
      }
    }
  },
  verifyEmail: {
    email: user.email,
    company,
    emailQuery: {
      where: {
        email: user.email
      }
    },
    companyIdQuery: {
      where: {
        id: companyId
      }
    },
    objFound: {
      company_id: companyId
    },
    commonError,
    notUniqueError: {
      errors: [ {
        path: 'email',
        message: 'email must be unique',
        data: {
          company_name: company.name
        }
      } ]
    }
  },
  recoverPassword: {
    user,
    destroyQuery: [ {
      where: {
        email: user.email,
        type: 'recover_password'
      }
    } ],
    createData: [ {
      email: user.email,
      type: 'recover_password',
      data: {
        user_name: user.name
      }
    } ]
  },
  validateRecoverToken: {
    recoverToken,
    expiredRecoverToken,
    query: [ {
      where: {
        token,
        type: 'recover_password'
      }
    } ],
    commonError,
    notFoundError: notFound('token')
  }
};
