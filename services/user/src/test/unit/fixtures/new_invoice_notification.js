const { commonError } = require('../../common/fixtures/errors');

const investor = {
  name: 'investorUser',
  email: 'investor@fondeodirecto.com'
};

const existingToken = {
  email: 'investor@fondeodirecto.com',
  type: 'new-invoice-notification',
  token: 'p6oBm05zxN9tNmsR',
  get: function() {
    return this;
  },
  destroy: function() {
    return true;
  }
};

const token = {
  token: 'p6oBm05zxN9tNmsR',
  get: function() {
    return this;
  }
};

const tokenInformation = {
  token: 'p6oBm05zxN9tNmsR'
};

module.exports = {
  createNewInvoiceToken: {
    token,
    tokenInformation,
    investor,
    existingToken,
    findOneQuery: {
      where: {
        email: investor.email, type: 'new-invoice-notification'
      },
      paranoid: true
    },
    createData: {
      email: 'investor@fondeodirecto.com',
      type: 'new-invoice-notification',
      issuer: 1,
      data: { user_name: 'investorUser', user_type: 'INVESTOR' }
    },
    commonError
  }
};
