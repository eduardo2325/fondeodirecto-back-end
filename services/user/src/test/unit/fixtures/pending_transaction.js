const { commonError, notFound } = require('../../common/fixtures/errors');
const { Company, Operation, InvestorTransaction, sequelize } = require('../../../models');

const sendengoStrategy = {
  selectObjectByCompanyId: () => {
    return true;
  },
  selectByCompanyId: () => {
    return true;
  }
};
const rfc = 'FODA900806965';
const companyId = 1;
const cxpId = 2;
const investorId = 3;
const fondeoId = 4;
const withdraw = 'WITHDRAW';
const deposit = 'DEPOSIT';
const approved = 'APPROVED';
const rejected = 'REJECTED';
const amount = 10000;
const deposit_date = new Date();
const yesterday = new Date();
const now = new Date();

function self() {
  return this;
}

deposit_date.setHours(0, 0, 0, 0);
yesterday.setHours(0, 0, 0, 0);
yesterday.setDate(yesterday.getDate() - 1);

const fundInvoiceTransaction = {
  amount: 1000,
  type: 'WITHDRAW',
  company_id: investorId,
  data: {
    company_id: companyId,
    invoice_id: 1
  },
  save: function() {
    return true;
  }
};
const paymentInProcessPendingTransaction = {
  amount: 1000,
  type: 'DEPOSIT',
  company_id: cxpId,
  status: 'PENDING',
  data: {
    invoice_id: 100,
    payment_date: deposit_date
  },
  get: self
};
const paymentInProcessPendingTransactionCxc = {
  amount: 2000,
  type: 'DEPOSIT',
  company_id: cxpId,
  status: 'PENDING',
  data: {
    invoice_id: 100,
    payment_date: deposit_date
  },
  get: self
};
const pendingTransactionInstance = {
  amount: 2000,
  type: 'DEPOSIT',
  company_id: cxpId,
  status: 'PENDING',
  data: {
    invoice_id: 100,
    payment_date: deposit_date
  },
  get: self,
  save: function() {
    return this;
  },
  set: function() {
    return;
  }
};
const paymentInProcessPendingTransactionInvestor = {
  amount: 3000,
  type: 'DEPOSIT',
  company_id: cxpId,
  status: 'PENDING',
  data: {
    invoice_id: 100,
    payment_date: deposit_date
  },
  get: self
};
const invoice = {
  id: 100,
  client_company_id: cxpId,
  investor_company_id: investorId,
  company_id: companyId
};
const sequelizeTransaction = {
  id: 3,
  rollback: () => null
};

const buildBankReportQuery = (queryParams) => {
  const whereParams = {
    $and: [
      {
        $or: [
          {
            $and: [
              { status: 'PENDING' },
              { type: 'DEPOSIT' },
              { bank_report_id: queryParams.bank_report_id ? queryParams.bank_report_id : null },
              { 'data.invoice_id': { $ne: null } },
              { 'data.key': { $ne: null } }
            ] },
          { $and: [
            { status: 'PENDING' },
            { type: 'WITHDRAW' },
            { bank_report_id: queryParams.bank_report_id ? queryParams.bank_report_id : null },
            { 'data.invoice_id': { $ne: null } },
            { 'data.fund_date': { $ne: null } },
            { 'data.investor_company_id': { $ne: null } }
          ] }
        ] }
    ]
  };

  const queryParamsReturnValue = {
    where: whereParams,
    order: [ [ 'created_at', 'DESC' ] ]
  };

  if ( queryParams.by_id ) {
    whereParams.id = queryParams.by_id;
  }

  Object.assign(queryParamsReturnValue, queryParams);

  return queryParamsReturnValue;
};


const queryParams1 = {
  bank_report_id: 1
};
const operation1 = {
  Invoice: {
    getAdminCxcPayment: () => {
      return true;
    },
    getFundEstimate: () => {
      return true;
    },
    total: 100,
    id: 1,
    Company: {
      business_name: 'company name',
      bank: 'bank',
      clabe: 'clabe',
      rfc: 'rfc'
    },
    Client: {
      business_name: 'client name',
      bank: 'bank',
      clabe: 'clabe',
      rfc: 'rfc'
    }
  },
  invoice_id: 1,
  id: 2
};
const operation2 = {
  Invoice: {
    total: 100,
    getAdminCxcPayment: () => {
      return true;
    },
    getFundEstimate: () => {
      return true;
    },
    id: 2,
    Company: {
      business_name: 'company name',
      bank: 'bank',
      clabe: 'clabe',
      rfc: 'rfc'
    },
    Client: {
      business_name: 'client name',
      bank: 'bank',
      clabe: 'clabe',
      rfc: 'rfc'
    }
  },
  invoice_id: 2,
  id: 3
};
const bankRawDataOperations = [
  operation1,
  operation2
];

module.exports = {
  transaction: {
    commonError,
    createParams: [ {
      type: withdraw,
      amount,
      company_id: companyId,
      data: { key: 'to file' }
    }, { transaction: undefined } ],
    keyObj: { key: 'to file' },
    type: withdraw,
    amount,
    company_id: companyId
  },
  approveOrReject: {
    commonError,
    notFoundError: notFound('PendingTransaction'),
    transaction: {
      save: () => {
        return this;
      }
    },
    id: 1,
    approved,
    rejected,
    queryParams: [ {
      where: {
        id: 1
      }
    } ]
  },
  depositOrWithdraw: {
    commonError,
    amount,
    company_rfc: rfc,
    key: 'to file',
    deposit_date,
    depositParams: [
      deposit,
      amount,
      rfc,
      { key: 'to file', deposit_date }
    ],
    withdrawParams: [
      withdraw,
      amount,
      rfc
    ]
  },
  invoiceOperation: {
    commonError,
    company_id: companyId,
    invoice: {
      id: 1,
      total: amount,
      fund_date: deposit_date,
      company_id: companyId,
      client_company_id: cxpId,
      investor_company_id: investorId
    },
    invoiceOperationParams: [
      'WITHDRAW',
      amount,
      companyId,
      {
        invoice_id: 1,
        fund_date: deposit_date,
        company_id: companyId,
        client_company_id: cxpId,
        investor_company_id: investorId
      },
      undefined
    ]
  },
  getBasicInfo: {
    transactionNoData: {
      get: () => {
        return {
          id: 1,
          type: 'WITHDRAW',
          status: 'PENDING',
          created_at: now,
          amount: 100
        };
      }
    },
    resultNoData: {
      id: 1,
      type: 'withdraw',
      status: 'pending',
      amount: '100.00',
      created_at: now.toString(),
      data: '{}'
    },
    transactionWithData: {
      get: () => {
        return {
          id: 1,
          type: 'WITHDRAW',
          status: 'PENDING',
          amount: 100,
          created_at: now,
          data: {
            id: 1
          }
        };
      }
    },
    resultWithData: {
      id: 1,
      type: 'withdraw',
      status: 'pending',
      amount: '100.00',
      created_at: now.toString(),
      data: JSON.stringify({ id: 1 })
    }
  },
  rejectInvoiceFund: {
    invoiceId: 1,
    sqlTransaction: undefined,
    invoiceInstance: {
      id: 1
    },
    invoiceQuery: [ {
      where: {
        id: 1,
        status: 'PUBLISHED'
      },
      include: [
        { model: Operation, where: { status: 'FUND_REQUESTED' }, include: InvestorTransaction }
      ]
    } ],
    invoiceNotFoundError: notFound('Invoice'),
    transactionParams: [ {
      status: 'REJECTED'
    }, {
      where: {
        data: {
          $contains: '{"invoice_id":1}'
        },
        status: 'PENDING'
      },
      transaction: undefined
    } ],
    commonError
  },
  findAndValidateInvoiceFund: {
    invoiceId: 1,
    pendingTransaction: fundInvoiceTransaction,
    invoiceInstance: {
      id: 1,
      investor_company_id: investorId,
      getFundEstimate: () => {
        return 1;
      },
      Operation: {
        status: 'FUND_REQUESTED',
        InvestorTransaction: {
          status: 'PENDING'
        }
      }
    },
    invoiceQuery: [ {
      where: {
        id: 1,
        status: 'PUBLISHED'
      },
      include: [
        { model: Company, as: 'Client' },
        { model: Company, as: 'Company' },
        { model: Operation, where: { status: 'FUND_REQUESTED' }, include: InvestorTransaction }
      ]
    } ],
    transactionQuery: [ {
      where: {
        data: {
          invoice_id: 1
        },
        status: 'PENDING'
      }
    } ],
    commonError,
    invoiceNotFoundError: notFound('Invoice'),
    pendingTransactionNotFoundError: notFound('PendingTransaction')
  },
  createInvoiceFund: {
    sendengoStrategy,
    invoiceId: 1,
    companyId,
    sqlTransaction: undefined,
    findAndValidateParams: [
      1
    ],
    pendingTransaction: fundInvoiceTransaction,
    invoiceInstance: {
      investor_company_id: companyId,
      getFundEstimate: () => {
        return 1;
      }
    },
    estimate: {
      fund_payment: 100
    },
    investorTransaction: [
      fundInvoiceTransaction,
      investorId,
      companyId,
      1,
      false,
      undefined
    ],
    cxcTransaction: [
      {
        type: 'DEPOSIT',
        amount: 100
      },
      companyId,
      companyId,
      1,
      true,
      undefined
    ],
    companyUpdateParams: [ {
      balance: 'balance - 1000'
    }, {
      where: {
        id: investorId
      },
      transaction: undefined
    } ],
    commonError
  },
  createClientInvoicePayment: {
    commonError,
    amount,
    invoice,
    receipt: 'to file',
    date: deposit_date,
    transaction: sequelizeTransaction,
    paymentParamsWithKey: [
      deposit,
      amount,
      invoice.client_company_id,
      {
        key: 'to file',
        payment_date: deposit_date,
        invoice_id: invoice.id
      },
      sequelizeTransaction
    ],
    paymentParamsWithoutKey: [
      deposit,
      amount,
      invoice.client_company_id,
      {
        payment_date: deposit_date,
        invoice_id: invoice.id
      },
      sequelizeTransaction
    ]
  },
  createInvoiceCompleted: {
    sendengoStrategy,
    invoiceId: invoice.id,
    fondeoId,
    payments: {
      cxpPayment: 20,
      cxpPaymentDate: yesterday.toString(),
      cxcPayment: 2000,
      investorPayment: 3000,
      fondeoPaymentDate: deposit_date.toString()
    },
    sqlTransaction: sequelizeTransaction,
    invoiceQuery: [ {
      where: {
        id: invoice.id,
        status: 'PUBLISHED'
      },
      include: [
        { model: Company, as: 'Client' },
        { model: Company, as: 'Company' },
        { model: Operation, where: { status: 'PAYMENT_IN_PROCESS' } }
      ]
    } ],
    pendingTransactionQuery: [
      {
        where: {
          status: 'PENDING',
          company_id: cxpId,
          'data.invoice_id': {
            $eq: invoice.id
          }
        }
      }
    ],
    pendingTransactionInstance,
    pendingTransactionUpdateNoResults: [
      0,
      null
    ],
    invoiceInstance: invoice,
    paymentInProcessPendingTransaction,
    createCXPTransactionParams: [
      paymentInProcessPendingTransaction,
      cxpId,
      fondeoId,
      invoice.id,
      true,
      sequelizeTransaction,
      yesterday
    ],
    createCXCTransactionParams: [
      paymentInProcessPendingTransactionCxc,
      fondeoId,
      companyId,
      invoice.id,
      true,
      sequelizeTransaction,
      deposit_date
    ],
    createInvestorTransactionParams: [
      paymentInProcessPendingTransactionInvestor,
      fondeoId,
      investorId,
      invoice.id,
      false,
      sequelizeTransaction,
      deposit_date
    ],
    updateInvestorBalanceParams: [
      {
        balance: sequelize.literal(`balance + ${paymentInProcessPendingTransactionInvestor.amount}`)
      }, {
        where: {
          id: investorId
        },
        transaction: sequelizeTransaction
      }
    ],
    invoiceNotFoundError: notFound('Invoice'),
    pendingTransactionNotFoundError: notFound('PendingTransaction')
  },
  isForInvestorInvoiceFund: {
    pendingTransaction: {
      type: 'WITHDRAW',
      status: 'PENDING',
      data: {
        invoice_id: 1,
        fund_date: 'somedate',
        company_id: 4,
        client_company_id: 2,
        investor_company_id: 5
      }
    },
    pendingTransaction2: {
      type: 'WITHDRAW',
      status: 'PENDING',
      data: {
        invoice_id: 1,
        fund_date: 'somedate',
        key: 'key setted',
        company_id: 2,
        client_company_id: 2,
        investor_company_id: 2
      }
    }
  },
  bankRawData: {
    sendengoStrategy,
    operations: bankRawDataOperations,
    operation1,
    operation2,
    rawData: [
      {
        reference: 2,
        transaction_id: 1,
        destinated_to: 'company name',
        bank_name: 'bank',
        clabe: 'clabe',
        rfc: 'rfc',
        amount: 9,
        selected: false,
        status: 'PENDING'
      },
      {
        reference: 3,
        transaction_id: 2,
        destinated_to: 'company name',
        bank_name: 'bank',
        clabe: 'clabe',
        rfc: 'rfc',
        amount: 8,
        selected: false,
        status: 'PENDING'
      }
    ],
    pendingTransactions: [ {
      id: 1,
      status: 'PENDING',
      type: 'WITHDRAW',
      data: {
        type: 'WITHDRAW',
        invoice_id: 1
      },
      created_at: new Date() - 30
    },
    {
      id: 2,
      status: 'PENDING',
      type: 'DEPOSIT',
      data: {
        type: 'DEPOSIT',
        invoice_id: 2
      }
    } ],
    bankReport: false,
    cxcPayment: {
      reserve: 8
    },
    estimate: {
      fund_payment: 9
    }
  },
  buildBankReportQuery: {
    queryParams1,
    response1: buildBankReportQuery(queryParams1)
  }
};
