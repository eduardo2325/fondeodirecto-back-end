const { commonError } = require('../../common/fixtures/errors');
const { User, Company } = require('../../../models');
const rfc = 'FODA900806965';
const companyId = 1;
const token = 'cgquRC7iJG21GHlm';
const email = 'dev@fondeodirecto.com';
const date = new Date();
const userModel = {
  id: 1,
  name: 'Fondeo',
  email,
  Company: {
    rfc,
    id: companyId,
    role: 'ADMIN',
    suspended_roles: []
  },
  role: 'ADMIN'
};
const suspendedUserModel = {
  id: 2,
  name: 'Fondeo',
  email,
  Company: {
    rfc,
    id: companyId,
    role: 'ADMIN',
    suspended_roles: [ 'ADMIN' ]
  },
  role: 'ADMIN'
};

const companyData = {
  rfc,
  id: companyId,
  role: 'COMPANY',
  isFideicomiso: false,
  created_at: new Date(),
  get: function() {
    return this;
  }
};

module.exports = {
  beforeCreate: {
    instance: {},
    token
  },
  verifyAndCreate: {
    email: 'email',
    password: '123456',
    unsuspendedUserModel: userModel,
    suspendedUserModel: suspendedUserModel,
    getAgreementResult: {
      agreed_at: new Date(),
      agreement_url: 'SOME_S3_URL'
    },
    context: {
      create: () => {
        return;
      }
    },
    unsuspendedSessionModel: {
      user_id: 1,
      user_role: 'ADMIN',
      company_agreed: true,
      company_role: 'ADMIN',
      company_rfc: rfc,
      company_id: companyId,
      suspended: false
    },
    suspendedSessionModel: {
      user_id: 2,
      user_role: 'ADMIN',
      company_agreed: true,
      company_role: 'ADMIN',
      company_rfc: rfc,
      company_id: companyId,
      suspended: true
    }
  },
  checkTokenModel: {
    context: {
      findOne: () => {
        return;
      }
    },
    findOneCall: {
      where: {
        token
      }
    },
    token,
    validSession: {
      expiration_date: date.setMinutes(date.getMinutes() + 30).toString(),
      save: () => {
        return;
      }
    },
    invalidSession: {
      expiration_date: date.setMinutes(date.getMinutes() - 30).toString(),
      save: () => {
        return;
      }
    }
  },
  updateSuspensions: {
    context: {
      update: () => {
        return;
      }
    },
    companyId: companyId,
    role: 'CXC',
    suspend: true,
    commonError,
    updateOptions: {
      where: {
        company_id: companyId,
        user_role: 'CXC',
        expiration_date: { }
      },
      transaction: {
        id: 1
      }
    },
    updateToSuspend: {
      suspended: true
    },
    updateToUnsuspend: {
      suspended: false
    },
    transaction: {
      id: 1
    }
  },
  getInformation: {
    token,
    query: [ {
      where: {
        token
      },
      include: [ {
        model: User,
        include: [ { model: Company } ]
      } ]
    } ],
    sessionData: {
      token,
      expiration_date: date.toString(),
      suspended: false,
      User: {
        id: 1,
        name: 'John',
        email: 'some@email.com',
        role: 'CXC',
        Company: companyData
      }
    },
    response: {
      token: {
        token,
        expiration_date: date.toString()
      },
      company_created_at: companyData.created_at.toString(),
      user: {
        id: 1,
        name: 'John',
        email: 'some@email.com',
        role: 'CXC',
        suspended: false,
        company: {
          rfc,
          isFideicomiso: false,
          id: companyId,
          role: 'COMPANY',
          prepayment: false
        }
      }
    },
    commonError
  }
};
