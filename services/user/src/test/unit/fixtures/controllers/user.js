const userRoles = require('../../../../config/user-roles');
const { commonError, notFound } = require('../../../common/fixtures/errors');
const { User } = require('../../../../models');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const rfc = 'ASDQWEZXC';
const companyId = 1;

const trackerInfo = [];
const invitationInfo = {
  name: 'user name',
  email: 'user@email.com',
  status: 'pending',
  company: {
    rfc: 'rfc',
    id: 1
  }
};
const checkEmailInfoExists = {
  exists: true
};
const checkEmailInfoDoesNotExists = {
  exists: false
};
const registrationToken = {
  token: 'registration token',
  Company: {
    role: 'COMPANY',
    rfc,
    id: companyId,
    isFideicomiso: false
  },
  company_id: companyId,
  getInvitationInfo: () => Promise.resolve(invitationInfo),
  destroy: () => Promise.resolve(),
  get() {
    return this;
  }
};
const getInvitationRequest = {
  request: {
    guid: 'guid',
    token: registrationToken.token
  }
};
const registrationRequest = {
  request: {
    guid: 'guid',
    token: 'testtoken',
    name: 'name',
    email: 'email',
    password: '123456'
  }
};
const registrationResponse = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'type'
};

const getRolesRequest = {
  request: {
    guid: 'guid'
  }
};
const getRolesResponse = {
  roles: userRoles.publicRoles
};
const userInformation = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'type'
};
const userInstance = {
  getInformation: () => Promise.resolve(userInformation),
  destroy: () => Promise.resolve(),
  Company: {
    id: companyId
  },
  company_id: companyId
};
const requestWithEmail = {
  request: {
    guid: 'guid',
    email: 'test@email.com'
  }
};
const passwordValues = {
  actual_password: 'current password',
  new_password: 'new super secret password'
};
const forgotPasswordToken = {
  token: 'token',
  email: userInformation.email,
  destroy: () => Promise.resolve(),
  get() {
    return this;
  }
};

const getInvitationResponse = invitationInfo;

const bankRawDataTransactions = [ { amount: '1.00', rfc: '1234'
}, { amount: 0, rfc: '5678' } ];
const responseTransactions = [ { amount: '1.00', selected: true } ];
const findExcelTransactionsResponse = {
  total_amount: '1.00',
  transactions: responseTransactions
};
const bankRawDataTransactions2 = [
  { amount: '1.00', rfc: '1234',
    clabe: 'clabe', reference: 'reference'
  }, { amount: '0', rfc: '5678' } ];

const somePendingTransactions = [ { id: 1 }, { id: 2 } ];
const buildBankReportQuery = {
  by_id: { $in: [ 1, 2 ] },
  skip_where_created_at: true
};
const generateExcelTransactionsResponse = {
  excel_url: 'some-url'
};

const excelData = [
  [ 'clabe', 'concepto', 'reference', '1.00', 'NO', '1234', 'iva' ]
];

const excelBankReportsData = [];
const excelBankReportsResponse = {
  excel_files: [ { id: 1 } ]
};


const totalAmount = 3;
const generatedDate = new Date(2018, 10, 1);
const generatedDateResponse = new Date(2018, 10, 1);
const findExcelTransactionsResponseFromId = {
  total_amount: totalAmount.toFixed(2),
  emission_date: generatedDateResponse.toString(),
  generated_by: 'Admin',
  bank_report_id: 3,
  transactions: [ { id: 1 }, { id: 2 } ]
};

module.exports = {
  getInvitation: {
    request: getInvitationRequest,
    response: getInvitationResponse,
    token: registrationToken,
    invitationInfo,
    findOneQuery: {
      where: {
        token: registrationToken.token,
        type: {
          $in: [ 'registration', 'investor-invitation' ]
        }
      }
    },
    logRequestMessageParams: [
      'Get invitation',
      getInvitationRequest.request,
      'request',
      getInvitationRequest.request.guid
    ],
    logResponseMessageParams: [
      'Get invitation',
      getInvitationResponse,
      'response',
      getInvitationRequest.request.guid
    ],
    commonError,
    notFoundError: notFound('invitation')
  },
  registration: {
    request: registrationRequest,
    response: registrationResponse,
    token: {
      email: 'email',
      company_rfc: 'rfc',
      company_id: 1,
      data: {
        user_type: 'type'
      }
    },
    userInformation,
    user: userInstance,
    logRequestMessageParams: [
      'Registration',
      registrationRequest.request,
      'request',
      registrationRequest.request.guid
    ],
    logResponseMessageParams: [
      'Registration',
      registrationResponse,
      'response',
      registrationRequest.request.guid
    ],
    findOneCall: {
      where: {
        token: registrationRequest.request.token
      }
    },
    createCall: {
      name: 'name',
      password: '123456',
      email: 'email',
      company_id: 1,
      role: 'type'
    },
    guid: 'guid',
    commonError,
    notFoundError: notFound('Token')
  },
  getRoles: {
    request: getRolesRequest,
    response: getRolesResponse,
    logRequestMessageParams: [
      'Get user roles',
      getRolesRequest.request,
      'request',
      getRolesRequest.request.guid
    ],
    logResponseMessageParams: [
      'Get user roles',
      getRolesResponse,
      'response',
      getRolesRequest.request.guid
    ]
  },
  delete: {
    request: requestWithEmail,
    logRequestMessageParams: [
      'Delete user',
      requestWithEmail.request,
      'request',
      requestWithEmail.request.guid
    ],
    logUserResponseParams: [
      'Delete user',
      userInformation,
      'response',
      requestWithEmail.request.guid
    ],
    logTokenResponseParams: [
      'Delete user',
      invitationInfo,
      'response',
      requestWithEmail.request.guid
    ],
    user: userInstance,
    token: registrationToken,
    userFindOneCall: {
      where: {
        email: requestWithEmail.request.email
      }
    },
    tokenFindOneCall: {
      where: {
        email: requestWithEmail.request.email,
        type: 'registration'
      }
    },
    getUsersCall: [ 1, 'name', 'ASC' ],
    commonError,
    unableError: {
      errors: [ {
        path: 'user',
        message: 'Unable to delete the last user'
      } ]
    },
    notFoundError: notFound('user'),
    guid: requestWithEmail.request.guid,
    userInformation,
    invitationInfo
  },
  checkEmail: {
    guid: requestWithEmail.request.guid,
    request: requestWithEmail,
    logRequestMessageParams: [
      'Check email user',
      requestWithEmail.request,
      'request',
      requestWithEmail.request.guid
    ],
    whereParam: {
      where: {
        email: requestWithEmail.request.email
      }
    },
    logResponseParams: [
      'Check email user',
      checkEmailInfoExists,
      'response',
      requestWithEmail.request.guid
    ],
    logResponseParamsDoesNotExists: [
      'Check email user',
      checkEmailInfoDoesNotExists,
      'response',
      requestWithEmail.request.guid
    ],
    response: {
      exists: true
    },
    responseDoesNotExists: {
      exists: false
    },
    commonError
  },
  resendInvitation: {
    request: requestWithEmail,
    logRequestMessageParams: [
      'Resend invitation',
      requestWithEmail.request,
      'request',
      requestWithEmail.request.guid
    ],
    logResponseMessageParams: [
      'Resend invitation',
      registrationToken,
      'response',
      requestWithEmail.request.guid
    ],
    token: registrationToken,
    query: {
      where: {
        type: 'registration',
        email: requestWithEmail.request.email
      }
    },
    commonError,
    notFoundError: notFound('token'),
    role: 'COMPANY',
    guid: requestWithEmail.request.guid
  },
  changePassword: {
    request: {
      request: {
        guid,
        user_id: userInformation.id,
        actual_password: passwordValues.actual_password,
        new_password: passwordValues.new_password
      }
    },
    logRequestMessageParams: [
      'Change password',
      {
        guid,
        user_id: userInformation.id,
        actual_password: passwordValues.actual_password,
        new_password: passwordValues.new_password
      },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Change password',
      userInformation,
      'response',
      guid
    ],
    methodValues: [
      userInformation.id,
      passwordValues.actual_password,
      passwordValues.new_password
    ],
    userInformation,
    commonError,
    guid
  },
  generatePasswordToken: {
    request: {
      request: {
        guid,
        email: userInformation.email
      }
    },
    logRequestMessageParams: [
      'Generate password token',
      {
        guid,
        email: userInformation.email
      },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Generate password token',
      forgotPasswordToken,
      'response',
      guid
    ],
    errorLogResponseMessageParams: [
      'Generate password token',
      {
        errors: [ {
          path: 'email',
          message: 'email not found'
        } ]
      },
      'response',
      guid
    ],
    methodValues: [
      userInformation
    ],
    userQuery: [ {
      where: {
        email: userInformation.email
      }
    } ],
    notFoudnError: {
      errors: [ {
        path: 'email',
        message: 'email not found'
      } ]
    },
    tokenInformation: forgotPasswordToken,
    userInformation,
    commonError,
    guid
  },
  validateRecoverToken: {
    tokenInformation: forgotPasswordToken,
    commonError,
    guid,
    request: {
      request: {
        guid,
        token: forgotPasswordToken.token
      }
    },
    logRequestMessageParams: [
      'Validate recover password token',
      {
        guid,
        token: forgotPasswordToken.token
      },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Validate recover password token',
      forgotPasswordToken,
      'response',
      guid
    ]
  },
  resetPassword: {
    request: {
      request: {
        guid,
        token: forgotPasswordToken.token,
        password: '123456'
      }
    },
    logRequestMessageParams: [
      'Reset password',
      {
        guid,
        token: forgotPasswordToken.token,
        password: '123456'
      },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Reset password',
      userInformation,
      'response',
      guid
    ],
    tokenInformation: forgotPasswordToken,
    userInformation,
    guid,
    commonError
  },
  downloadTracker: {
    request: {
      request: {
        guid
      }
    },
    trackerInfo,
    response: {
      trackerInfo
    },
    logRequestMessageParams: [
      'Download Tracker',
      { guid },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Download Tracker',
      trackerInfo,
      'response',
      guid
    ],
    commonError
  },
  findExcelTransactions: {
    request: {
      request: {
        guid
      }
    },
    pendingTransactions: [],
    queryParamsPendingTransactions: {},
    bankReport: {},
    bankRawData: bankRawDataTransactions,
    responseTransactions: responseTransactions,
    totalAmount: 1.0,
    trackerInfo,
    response: findExcelTransactionsResponse,
    findOneQuery: { order: [ [ 'created_at', 'DESC' ] ] },
    logRequestMessageParams: [
      'Find excel transactions',
      { guid },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Find excel transactions',
      findExcelTransactionsResponse,
      'response',
      guid
    ],
    commonError
  },
  generateExcelTransactions: {
    request: {
      request: {
        guid,
        pending_transactions: somePendingTransactions,
        user_id: 1
      }
    },
    emptyPendingTransactions: [],
    pendingTransactions: somePendingTransactions,
    queryParamsPendingTransactions: {},
    buildBankReportQuery,
    bankReport: {},
    excelData,
    newData: {
      generated_at: 'YYYY-MM-DD HH:mm:ss',
      total_amount: 1.0,
      total_generated: 1,
      generated_by: 1,
      link: 'some-url'
    },
    ids: [ 1, 2 ],
    bankRawData: bankRawDataTransactions2,
    responseTransactions: responseTransactions,
    totalAmount: 1.0,
    trackerInfo,
    s3Url: 'some-url',
    response: generateExcelTransactionsResponse,
    findOneQuery: { order: [ [ 'created_at', 'DESC' ] ] },
    logRequestMessageParams: [
      'Generate excel transactions',
      { guid,
        pending_transactions: somePendingTransactions,
        user_id: 1
      },
      'request',
      guid
    ],
    excelBankReportsData,
    logResponseMessageParams: [
      'Generate excel transactions',
      generateExcelTransactionsResponse,
      'response',
      guid
    ],
    commonError
  },
  excelBankReports: {
    request: {
      request: {
        guid
      }
    },
    pendingTransactions: [],
    queryParamsPendingTransactions: {},
    bankReport: {},
    bankRawData: bankRawDataTransactions,
    responseTransactions: responseTransactions,
    totalAmount: 1.0,
    trackerInfo,
    response: excelBankReportsResponse,
    findAllQuery: {
      order: [ [ 'generated_at', 'DESC' ] ],
      include: [
        { model: User }
      ]
    },
    reports: [ { id: 1 } ],
    emptyReports: [],
    logRequestMessageParams: [
      'Excel bank reports',
      { guid },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Excel bank reports',
      excelBankReportsResponse,
      'response',
      guid
    ],
    commonError
  },
  findExcelTransactionsFromBankReportId: {
    request: {
      request: {
        guid,
        bank_report_id: 3
      }
    },
    pendingTransactions: [ { amount: 1 }, { amount: 2 } ],
    queryParamsPendingTransactions: {
      where: {
        bank_report_id: 3
      }
    },
    generatedDate,
    generatedDateResponse,
    bankReport: { id: 3, User: { name: 'Admin' }, generated_at: generatedDate.toString() },
    emptyBankReport: {},
    bankRawData: [ { id: 1, rfc: '1' }, { id: 2, rfc: '2' } ],
    responseTransactions: responseTransactions,
    totalAmount: 1.0,
    trackerInfo,
    response: findExcelTransactionsResponse,
    findOneQuery: {
      where: { id: 3 },
      include: [
        { model: User, required: true }
      ]
    },
    logRequestMessageParams: [
      'Excel transactions from bank report',
      { guid, bank_report_id: 3 },
      'request',
      guid
    ],
    logResponseMessageParams: [
      'Excel transactions from bank report',
      findExcelTransactionsResponseFromId,
      'response',
      guid
    ],
    commonError
  }
};
