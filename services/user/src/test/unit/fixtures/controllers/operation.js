const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const companyId = 1;
const today = new Date();
const expiration = new Date();
const requestType = 'request';
const responseType = 'response';
const { commonError, notFound } = require('../../../common/fixtures/errors');
const { Company, Invoice, Operation } = require('../../../../models');
const rfc = 'FODA900806965';
const date = new Date();

const operation = {
  invoice_id: 50000,
  expiration_date: expiration,
  published_date: today,
  status: 'pending',
  user_id: 1
};

const publishedDate = new Date();
const expirationDate = new Date();

expirationDate.setHours(0, 0, 0, 0);
expirationDate.setDate(expirationDate.getDate() + 59);
publishedDate.setHours(0, 0, 0, 0);
publishedDate.setDate(publishedDate.getDate() - 5);

const operationGeneralInfo = {
  id: 50000,
  invoice_id: 10049,
  expiration_date: expirationDate,
  published_date: publishedDate,
  annual_cost_percentage: 15,
  reserve_percentage: 10,
  fd_commission_percentage: 0.5,
  annual_cost: 5.71,
  reserve: 3.81,
  fd_commission: 0.19,
  factorable: 34.29,
  subtotal: 33.35,
  fund_payment: 33.32,
  days_limit: 59,
  status: 'PUBLISHED',
  formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
    ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
  user_id: 10001
};

const invoiceElement = {
  created_at: date.toString(),
  client_rfc: rfc,
  client_name: 'Company',
  client_color: '#FFFFF',
  company_rfc: rfc,
  total: '330',
  status: 'pending'
};

const invoice = {
  getClient: () => {
    return;
  },
  getCompany: () => {
    return;
  },
  getBasicInfoWithClient: () => {
    return;
  },
  getBasicInfo: () => {
    return;
  },
  company_id: companyId
};

const getOperationResponse = Object.assign( { invoice: invoiceElement }, operation);

const publishedOperation = operationGeneralInfo;

const userInformation = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'ADMIN',
  company_id: companyId
};
const cxcUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXC',
  company_id: companyId
};
const cxpUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXP',
  company_id: companyId
};

module.exports = {
  getOperations: {
    request: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1
      }
    },
    requestPayedOperations: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1,
        payed_operations: true
      }
    },
    response: {
      operations: [ operation ],
      total_operations: 4,
      total_pages: 2
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    cxcUser,
    cxpUser,
    includeCxc: [ {
      model: Invoice,
      attributes: [ 'uuid', 'number', 'total' ],
      where: { company_id: companyId },
      include: [ { model: Company, as: 'Client', attributes: [ 'name' ], required: true } ]
    } ],
    includeCxp: [
      {
        model: Company,
        as: 'Company',
        attributes: [ 'name' ],
        required: true },
      {
        model: Operation,
        attributes:
         [ 'id',
           'invoice_id',
           'expiration_date',
           'published_date',
           'days_limit',
           'status' ],
        required: false
      } ],
    includeCxpPayed: [
      {
        model: Company,
        as: 'Company',
        attributes: [ 'name' ],
        required: true },
      {
        model: Operation,
        attributes:
          [ 'id',
            'invoice_id',
            'expiration_date',
            'published_date',
            'days_limit',
            'status' ],
        required: true
      } ],
    whereCxp: {
      user_id: 1
    },
    getOperationsOptions: {
      compute_payment_values: true,
      operationStatusCondition: 'NOT IN',
      role: 'CXP'
    },
    getPayedOperationsOptions: {
      compute_payment_values: true,
      operationStatusCondition: 'IN',
      role: 'CXP'
    },
    limit: 2,
    offset: 2,
    message: 'Get operations',
    guid,
    requestType,
    responseType,
    operations: [ operation ],
    commonError
  },
  getOperation: {
    cxcUser,
    cxpUser,
    anotherUser: {
      role: 'INVESTOR',
      company_id: 999
    },
    request: {
      request: {
        guid,
        user_id: 1,
        operation_id: 1
      }
    },
    response: getOperationResponse,
    whereOperation: {
      where: {
        id: 1
      }
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    invoice,
    operation: {
      getInvoice: () => {
        return;
      },
      getBasicInfo: () => {
        return;
      }
    },
    fundedOperation: {
      getBasicInfo: () => {
        return;
      },
      getBasicInfoWithClient: () => {
        return;
      },
      company_id: companyId,
      investor_company_id: 2
    },
    operationWrongCompanyId: {
      company_id: 2
    },
    operationGeneralInfo,
    operationBasicInfo: operation,
    publishedOperation,
    userInformation,
    userInvestor: {
      role: 'INVESTOR',
      company_id: 2
    },
    userCxc: {
      role: 'CXC',
      company_id: companyId
    },
    message: 'Get operation',
    guid,
    requestType,
    responseType,
    commonError,
    invoiceElement,
    notFound: notFound('Operation')
  },
  getAdminOperations: {
    allParamsRequest: {
      request: {
        guid,
        user_id: 1,
        limit: 2,
        offset: 2,
        status: [ 'approved', 'published' ],
        start_fund_date: 'start_fund_date',
        end_fund_date: 'end_fund_date',
        start_expiration_date: 'start_expiration_date',
        end_expiration_date: 'end_expiration_date',
        client_name: 'client',
        investor_name: 'investor',
        company_name: 'company',
        order_by: 'client_name',
        order_desc: true
      }
    },
    noPaginationParamsRequest: {
      request: {
        guid,
        user_id: 1,
        status: [ 'approved', 'published' ],
        start_fund_date: 'start_fund_date',
        end_fund_date: 'end_fund_date',
        start_expiration_date: 'start_expiration_date',
        end_expiration_date: 'end_expiration_date',
        client_name: 'client',
        investor_name: 'investor',
        company_name: 'company',
        order_by: 'client_name',
        order_desc: true
      }
    },
    emptyStatusRequest: {
      request: {
        guid,
        user_id: 1,
        limit: 2,
        offset: 2,
        status: [],
        start_fund_date: 'start_fund_date',
        end_fund_date: 'end_fund_date',
        start_expiration_date: 'start_expiration_date',
        end_expiration_date: 'end_expiration_date',
        client_name: 'client',
        investor_name: 'investor',
        company_name: 'company',
        order_by: 'client_name',
        order_desc: true
      }
    },
    adminOperations: {
      operations: [ operation ],
      count: 4
    },
    response: {
      operations: [ operation ],
      total_operations: 4,
      total_pages: 2
    },
    responseDefaultPagination: {
      operations: [ operation ],
      total_operations: 4,
      total_pages: 1
    },
    message: 'Get admin operations',
    allParams: {
      guid,
      user_id: 1,
      limit: 2,
      offset: 2,
      status: [ 'approved', 'published' ],
      start_fund_date: 'start_fund_date',
      end_fund_date: 'end_fund_date',
      start_expiration_date: 'start_expiration_date',
      end_expiration_date: 'end_expiration_date',
      client_name: 'client',
      investor_name: 'investor',
      company_name: 'company',
      order_by: 'client_name',
      order_desc: true
    },
    noPaginationParams: {
      guid,
      user_id: 1,
      limit: 25,
      status: [ 'approved', 'published' ],
      start_fund_date: 'start_fund_date',
      end_fund_date: 'end_fund_date',
      start_expiration_date: 'start_expiration_date',
      end_expiration_date: 'end_expiration_date',
      client_name: 'client',
      investor_name: 'investor',
      company_name: 'company',
      order_by: 'client_name',
      order_desc: true
    },
    nullStatusParams: {
      guid,
      user_id: 1,
      limit: 2,
      offset: 2,
      status: null,
      start_fund_date: 'start_fund_date',
      end_fund_date: 'end_fund_date',
      start_expiration_date: 'start_expiration_date',
      end_expiration_date: 'end_expiration_date',
      client_name: 'client',
      investor_name: 'investor',
      company_name: 'company',
      order_by: 'client_name',
      order_desc: true
    },
    guid,
    requestType,
    responseType,
    commonError
  }
};
