const _ = require('lodash');
const { commonError } = require('../../../common/fixtures/errors');
const rfc = 'FODA900806965';
const companyId = 1;
const token = 'cgquRC7iJG21GHlm';
const email = 'dev@fondeodirecto.com';
const expiration_date = 'today';
const guid = '123456';
const requestType = 'request';
const responseType = 'response';
const userModel = {
  id: 1,
  name: 'Fondeo',
  email,
  Company: {
    rfc,
    id: companyId,
    role: 'ADMIN',
    suspended_roles: [],
    taxpayer_type: 'PHYSICAL'
  },
  role: 'ADMIN'
};

module.exports = {
  checkToken: {
    request: {
      request: {
        guid,
        token
      }
    },
    session: {
      get: () => {
        return {
          expiration_date
        };
      }
    },
    response: {
      expiration_date
    },
    message: 'Check Token',
    requestType,
    responseType,
    commonError
  },
  deleteToken: {
    request: {
      request: {
        guid,
        token
      }
    },
    response: { },
    sessionCall: {
      where: {
        token
      }
    },
    guid,
    message: 'Delete Token',
    requestType,
    responseType,
    commonError
  },
  verifyAndCreate: {
    makePreview(forRole, extraParams) {
      const { response, sessionResponse, previewParams } = this;

      const fixedResponse = _.cloneDeep(response);
      const fixedSession = _.cloneDeep(sessionResponse);

      fixedSession.agreement.save = () => null;
      fixedResponse.user.role = fixedSession.user.role = forRole;
      fixedResponse.company.role = fixedSession.user.Company.role = forRole;
      fixedResponse.agreement.agreement_url = 'SOME_S3_URL';

      const fixedParams = {
        ...previewParams,
        ...extraParams
      };

      return { fixedResponse, fixedSession, fixedParams };
    },
    request: {
      request: {
        guid: '123456',
        email: 'email',
        password: '123456'
      }
    },
    response: {
      token: {
        token,
        expiration_date: 'today'
      },
      user: {
        id: 1,
        name: 'Fondeo',
        email,
        role: 'ADMIN',
        suspended: false
      },
      company: {
        rfc: 'FODA900806965',
        id: 1,
        role: 'ADMIN'
      },
      agreement: {
        agreed_at: undefined,
        agreement_url: undefined
      }
    },
    sessionResponse: {
      session: {
        token,
        expiration_date: 'today',
        suspended: false
      },
      user: userModel,
      agreement: {
        agreed_at: undefined,
        agreement_url: undefined
      }
    },
    previewParams: {
      template: 'investor_physical',
      locals: {
        FACTORAJE_FIRMANTE_EMPRESA: undefined,
        FACTORAJE_FIRMANTE_NOMBRE: 'Fondeo',
        PERSONA_MORAL_RAZON_SOCIAL: undefined,
        PERSONA_FISICA_NOMBRE: 'Fondeo'
      }
    },
    error: {
      errors: [ {
        path: 'Email or Password',
        message: 'email or password doesn\'t match'
      } ]
    },
    message: 'Verify and Create',
    requestType,
    responseType
  },
  me: {
    request: {
      request: {
        guid,
        token
      }
    },
    response: {
      token
    },
    guid,
    token,
    sessionData: {
      token
    },
    message: 'Get user credentials',
    requestType,
    responseType,
    commonError
  }
};
