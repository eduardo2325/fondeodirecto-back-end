const userRoles = require('../../../../config/user-roles');
const moment = require('/var/lib/core/js/moment');
const { Company, Operation, InvestorTransaction } = require('../../../../models');
const { commonError, notFound } = require('../../../common/fixtures/errors');
const rfc = 'FODA900806965';
const companyId = 1;
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const requestType = 'request';
const responseType = 'response';
const bankInfo = {
  name: 'BANAMEX',
  account: '70035593643'
};

const someDate = new Date().toString();
const clabe = '002090700355936431';
const company = {
  id: companyId,
  rfc,
  role: 'ADMIN',
  name: 'Fondeo Directo',
  business_name: 'Fondeo Directo',
  holder: 'Fondeo Directo',
  clabe,
  description: 'description',
  getBalance: () => '100.00',
  updateRoleSuspension: () => null,
  update: () => true
};
const prepaymentCompany = {
  id: companyId,
  rfc,
  role: 'ADMIN',
  name: 'Fondeo Directo',
  business_name: 'Fondeo Directo',
  holder: 'Fondeo Directo',
  clabe,
  description: 'description',
  getBalance: () => '100.00',
  updateRoleSuspension: () => null,
  get: () => true
};
const variable_fee_percentage = 12.00;
const fee = 120.00;
const fd_commission = 0;
const reserve = 0;
const annual_cost = 0;
const newCompany = {
  id: companyId,
  rfc,
  role: 'ADMIN',
  name: 'Fondeo Directo',
  business_name: 'Fondeo Directo',
  holder: 'Fondeo Directo',
  clabe,
  description: 'description',
  getBalance: () => '100.00',
  updateRoleSuspension: () => null,
  get: () => true
};
const newCompanyPlain = {
  id: companyId
};
const operationCostValues = {
  company_id: companyId,
  fd_comission: fd_commission.toFixed(2),
  reserve: reserve.toFixed(2),
  annual_cost: annual_cost.toFixed(2),
  fee: fee.toFixed(2),
  variable_fee_percentage: variable_fee_percentage.toFixed(2)
};
const tokenInstance = {
  email: 'investor-invitation@fondeodirecto.com',
  data: {
    variable_fee_percentage: '12.00',
    fee: '120.00',
    fd_commision: '0',
    reserve: '0',
    annual_cost: '0',
    company_name: 'Some Company from data'
  },
  destroy: () => true
};
const anotherEmailtokenInstance = {
  email: 'another-no-email-invitation@fondeodirecto.com',
  data: {
    variable_fee_percentage: '12.00',
    fee: '120.00',
    company_name: 'Some Company from data'
  },
  destroy: () => true
};
const userData = {
  name: 'John Doe',
  email: 'investor-invitation@fondeodirecto.com',
  password: '123456',
  company_id: companyId,
  role: 'INVESTOR',
  age: 30,
  phone: '123567',
  address: 'address info'
};
const requestMoralType = {
  request: {
    guid,
    token: 'registerInvestorByInvitationToken',
    taxpayer_type: 'moral',
    name: 'John Doe',
    email: 'investor-invitation@fondeodirecto.com',
    password: '123456',
    phone: '123567',
    age: 30,
    address: 'address info',
    rfc: 'XMPL56647604',
    company_business_name: 'John Doe S.A de C.V'
  }
};

const statementSummaries = {
  factoraje: {
    initial_ammount: '0.00',
    final_ammount: '0.00',
    payments: '0.00',
    acquisitions: '0.00'
  },
  cash: {
    initial_ammount: '0.00',
    final_ammount: '0.00',
    increments: '0.00',
    decrements: '0.00'
  }
};
const subtractMonthsToCurrentMoment = (nMonths) => {
  const subtractedMoment = moment().subtract(nMonths, 'months');

  return {
    month: subtractedMoment.month(),
    year: subtractedMoment.year()
  };
};


let subtracted = subtractMonthsToCurrentMoment(1);
const summaryValidYear = subtracted.year;
const summaryValidMonth = subtracted.month;
const requestedMoment = moment([ summaryValidYear, summaryValidMonth ]);

subtracted = subtractMonthsToCurrentMoment(4);
const belowOfRangeYear = subtracted.year;
const belowOfRangeMonth = subtracted.month;

subtracted = subtractMonthsToCurrentMoment(-1);
const aboveOfRangeYear = subtracted.year;
const aboveOfRangeMonth = subtracted.month;

const newCompanyData = {
  rfc: 'XMPL56647604',
  name: 'Some Company from data',
  business_name: 'John Doe S.A de C.V',
  role: 'INVESTOR',
  taxpayer_type: 'moral',
  suspended_roles: [],
  description: ''
};

const requestPhysicalType = {
  request: {
    guid,
    token: 'registerInvestorByInvitationToken',
    taxpayer_type: 'physical',
    name: 'John Doe',
    email: 'investor-invitation@fondeodirecto.com',
    password: '123456',
    phone: '123567',
    age: 30,
    address: 'address info',
    rfc: 'XMPL56647604',
    company_business_name: 'John Doe S.A de C.V'
  }
};

const newCompanyDataPhysical = {
  rfc: 'XMPL56647604',
  name: 'Some Company from data',
  business_name: 'John Doe',
  role: 'INVESTOR',
  taxpayer_type: 'physical',
  suspended_roles: [],
  description: ''
};

const fullInformation = {
  id: companyId,
  rfc,
  role: company.role,
  name: company.name,
  business_name: company.business_name,
  holder: company.holder,
  clabe,
  bank: bankInfo.name,
  bank_account: bankInfo.account,
  description: company.description
};
const newUser = {
  name: 'John Doe',
  email: 'john@doe.com',
  type: userRoles.publicRoles[0].value
};
const usersList = {
  users: [ {
    name: 'John Doe',
    email: 'john@doe.com',
    color: '#1234',
    role: 'cxc',
    status: 'active'
  }, {
    name: 'Jane Doe',
    email: 'jane@doe.com',
    color: '#4321',
    role: 'cxc',
    status: 'pending'
  } ]
};
const color = '#1234';
const operationCost = {
  company_id: companyId
};

module.exports = {
  create: {
    guid,
    request: {
      request: {
        guid,
        rfc,
        name: fullInformation.name,
        business_name: fullInformation.business_name,
        holder: fullInformation.holder,
        clabe,
        role: 'COMPANY',
        operation_cost: operationCost,
        description: fullInformation.description
      }
    },
    investorRequest: {
      request: {
        guid,
        rfc,
        name: fullInformation.name,
        business_name: fullInformation.business_name,
        holder: fullInformation.holder,
        clabe,
        role: 'INVESTOR',
        operation_cost: operationCost,
        taxpayer_type: 'moral',
        description: fullInformation.description
      }
    },
    token: 'token',
    newUser,
    company: {
      id: companyId,
      getInformation: () => {
        return fullInformation;
      }
    },
    transaction: {
      rollback: () => {
        return false;
      }
    },
    fullInformation,
    operationCost,
    message: 'Create company',
    requestType,
    responseType,
    commonError,
    notNullError: {
      errors: [ {
        path: 'taxpayer_type',
        message: 'taxpayer_type cannot be null'
      } ]
    }
  },
  exists: {
    guid,
    query: {
      where: {
        rfc
      }
    },
    request: {
      request: {
        guid,
        rfc
      }
    },
    company: {},
    notExists: {
      exists: false
    },
    exists: {
      exists: true
    },
    commonError,
    bankInfo,
    message: 'Company Exists',
    requestType,
    responseType
  },
  update: {
    guid,
    query: {
      where: {
        id: companyId
      }
    },
    request: {
      request: {
        guid,
        id: companyId,
        name: company.name,
        business_name: company.business_name,
        holder: company.holder,
        clabe,
        description: company.description
      }
    },
    requestWithoutName: {
      request: {
        guid,
        id: companyId,
        name: '',
        business_name: company.business_name,
        holder: company.holder,
        clabe,
        description: company.description
      }
    },
    requestWithoutBussinessName: {
      request: {
        guid,
        id: companyId,
        name: company.name,
        business_name: '',
        holder: company.holder,
        clabe,
        description: company.description
      }
    },
    requestWithoutHolder: {
      request: {
        guid,
        id: companyId,
        name: company.name,
        business_name: company.business_name,
        holder: '',
        clabe,
        description: company.description
      }
    },
    requestWithoutClabe: {
      request: {
        guid,
        id: companyId,
        name: company.name,
        business_name: company.business_name,
        holder: company.holder,
        clabe: '',
        description: company.description
      }
    },
    requestWithoutDescription: {
      request: {
        guid,
        id: companyId,
        name: company.name,
        business_name: company.business_name,
        holder: company.holder,
        clabe,
        description: ''
      }
    },
    companyObj: {
      update: () => {
        return;
      },
      get: () => {
        return company;
      }
    },
    company,
    companyValues: {
      name: company.name,
      business_name: company.business_name,
      holder: company.holder,
      clabe,
      description: company.description
    },
    companyWithoutName: {
      business_name: company.business_name,
      holder: company.holder,
      clabe,
      description: company.description
    },
    companyWithoutBussinessName: {
      name: company.name,
      holder: company.holder,
      clabe,
      description: company.description
    },
    companyWithoutHolder: {
      name: company.name,
      business_name: company.business_name,
      clabe,
      description: company.description
    },
    companyWithoutClabe: {
      name: company.name,
      business_name: company.business_name,
      holder: company.holder,
      description: company.description
    },
    companyWithoutDescription: {
      name: company.name,
      business_name: company.business_name,
      holder: company.holder,
      clabe
    },
    returning: {
      returning: true
    },
    notFound: {
      errors: [ {
        path: 'company',
        message: 'Not found'
      } ]
    },
    commonError,
    message: 'Update company',
    requestType,
    responseType
  },
  updateOperationCost: {
    guid,
    query: {
      where: {
        company_id: companyId
      },
      include: [ {
        model: Company,
        where: {
          role: 'COMPANY'
        },
        required: true
      } ]
    },
    request: {
      request: {
        guid,
        id: companyId,
        annual_cost: 1,
        reserve: 2,
        fd_commission: 3,
        role: 'COMPANY'
      }
    },
    operationCost: {
      update: () => {
        return;
      },
      get: () => {
        return {
          annual_cost: 1,
          reserve: 2,
          fd_commission: 3
        };
      }
    },
    operationCostValues: {
      annual_cost: '1.00',
      reserve: '2.00',
      fd_commission: '3.00'
    },
    returning: {
      returning: true
    },
    notFound: {
      errors: [ {
        path: 'company',
        message: 'Not found'
      } ]
    },
    commonError,
    message: 'Update operation cost',
    requestType,
    responseType
  },
  getFullInformation: {
    guid,
    query: {
      where: {
        id: companyId
      }
    },
    request: {
      request: {
        guid,
        id: companyId
      }
    },
    company: {
      getInformation: () => {
        return fullInformation;
      }
    },
    fullInformation,
    commonError,
    notFound: {
      errors: [ {
        path: 'company',
        message: 'Not found'
      } ]
    },
    bankInfo,
    message: 'Get company full information',
    requestType,
    responseType
  },
  getBankInfo: {
    request: {
      request: {
        guid,
        clabe
      }
    },
    guid,
    clabe,
    company,
    companyQuery: {
      where: {
        clabe: company.clabe
      }
    },
    commonError,
    clabeNotUniqueError: {
      errors: [ {
        path: 'clabe',
        message: 'clabe must be unique',
        data: {
          company_name: company.name
        }
      } ]
    },
    response: bankInfo,
    bankInfo,
    message: 'Get Bank Info',
    requestType,
    responseType
  },
  analyzeClabe: {
    request: {
      request: {
        guid,
        clabe,
        exists: true
      }
    },
    requestFalse: {
      request: {
        guid,
        clabe,
        exists: false
      }
    },
    guid,
    clabe,
    company,
    companyQuery: {
      where: {
        clabe: company.clabe
      }
    },
    commonError,
    clabeNotUniqueError: {
      errors: [ {
        path: 'clabe',
        message: 'clabe must be unique',
        data: {
          company_name: company.name
        }
      } ]
    },
    response: bankInfo,
    bankInfo,
    message: 'Analyze Clabe',
    requestType,
    responseType
  },
  getCompanies: {
    request: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        role: 'COMPANY'
      }
    },
    response: {
      companies: [ company ],
      total_companies: 4,
      total_pages: 2
    },
    limit: 2,
    offset: 2,
    message: 'Get Companies',
    guid,
    requestType,
    responseType,
    companies: [ company ],
    commonError
  },
  getUsers: {
    request: {
      request: {
        guid,
        id: companyId,
        order_by: 'role',
        order_desc: true
      }
    },
    id: companyId,
    orderBy: 'role',
    orderDesc: 'DESC',
    response: { users: usersList },
    message: 'Get company users',
    guid,
    requestType,
    responseType,
    usersList,
    commonError
  },
  getOperationCost: {
    guid,
    query: {
      attributes: [ 'annual_cost', 'reserve', 'fd_commission', 'fee', 'variable_fee_percentage' ],
      where: {
        company_id: companyId
      },
      include: [ {
        model: Company,
        where: {
          role: 'COMPANY'
        },
        required: true
      } ],
      raw: true
    },
    request: {
      request: {
        guid,
        id: companyId,
        role: 'COMPANY'
      }
    },
    operationCost: {
      annual_cost: '1.00',
      reserve: '2.00',
      fd_commission: '3.00',
      variable_fee_percentage: '0.00'
    },
    commonError,
    notFoundError: notFound('company'),
    message: 'Get operation cost',
    requestType,
    responseType
  },
  registerInvestor: {
    guid,
    request: requestMoralType,
    requestPhysicalType,
    newCompanyData,
    newCompanyDataPhysical,
    newCompanyPlain,
    tokenInstance,
    anotherEmailtokenInstance,
    taxpayerError: {
      errors: [ {
        path: 'taxpayer_type',
        message: 'taxpayer_type cannot be null'
      } ]
    },
    userProducerParams: {
      file: 'SOME_URL',
      role: 'INVESTOR',
      id: companyId,
      agreed_at: 'SomeDate',
      recipients: [ [ requestMoralType.request.name, requestMoralType.request.email ] ]
    },
    agreementObj: {
      company_id: companyId,
      user_role: 'INVESTOR'
    },
    agreementObjWithUrl: {
      company_id: companyId,
      user_role: 'INVESTOR',
      agreement: 'SOME_URL',
      agreed_at: 'SomeDate'
    },
    previewContractParams: {
      template: 'INVESTOR_moral'.toLowerCase(),
      locals: {
        FACTORAJE_FIRMANTE_EMPRESA: newCompanyPlain.business_name,
        FACTORAJE_FIRMANTE_NOMBRE: 'John Doe',
        PERSONA_MORAL_RAZON_SOCIAL: newCompanyPlain.business_name,
        PERSONA_FISICA_NOMBRE: 'John Doe'
      }
    },
    previewContractParamsPhysical: {
      template: 'INVESTOR_physical'.toLowerCase(),
      locals: {
        FACTORAJE_FIRMANTE_EMPRESA: newCompanyPlain.business_name,
        FACTORAJE_FIRMANTE_NOMBRE: 'John Doe',
        PERSONA_MORAL_RAZON_SOCIAL: newCompanyPlain.business_name,
        PERSONA_FISICA_NOMBRE: 'John Doe'
      }
    },
    s3Url: 'SOME_URL',
    taxpayerTypes: [ 'moral', 'physical' ],
    tokenQuery: { where: { token: 'registerInvestorByInvitationToken', type: 'investor-invitation' } },
    companyInstanceGetParams: { plain: true },
    emailNotAllowedError: {
      errors: [ {
        path: 'invitation',
        message: 'Email not allowed'
      } ]
    },
    newUser: {
      name: 'John Doe',
      email: 'investor-invitation@fondeodirecto.com',
      status: 'pending',
      role: 'investor',
      color
    },
    responseUser: {
      name: 'John Doe',
      email: 'investor-invitation@fondeodirecto.com',
      status: 'pending',
      role: 'investor',
      color
    },
    newCompany,
    color,
    message: 'Register new investor by invitation',
    secondMessage: 'Create investor',
    requestType,
    responseType,
    commonError,
    operationCostValues,
    transaction: {
      id: 1,
      rollback: () => {
        return false;
      }
    },
    userData,
    tokenNotFoundError: {
      errors: [ {
        path: 'company',
        message: 'token not found'
      } ]
    }
  },
  addUser: {
    guid,
    request: {
      request: {
        guid,
        company_id: companyId,
        user: newUser
      }
    },
    query: {
      where: {
        id: companyId
      }
    },
    token: 'token',
    newUser,
    responseUser: {
      name: newUser.name,
      email: newUser.email,
      status: 'pending',
      role: newUser.type,
      color
    },
    company,
    color,
    message: 'Add user to company',
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('company')
  },
  getFactorajeAndCashSummaries: {
    guid,
    message: 'Get investor summaries',
    requestedMoment,
    commonError,
    notFound,
    requestType,
    invoiceFindParam: {
      include: [
        {
          model: Operation,
          required: true,
          include: [ {
            model: InvestorTransaction,
            required: true
          } ]
        }
      ],
      where: {
        $and: [
          [ `("Operation.InvestorTransaction".fund_date IS NOT NULL
              AND "Operation.InvestorTransaction".fund_date
                < '${requestedMoment.format('YYYY-MM')}-01 00:00:00'::timestamp)` ],
          { investor_company_id: companyId },
          [ `( ("Operation".status != 'COMPLETED') OR
              "Operation".updated_at >= '${requestedMoment.format('YYYY-MM')}-01 00:00:00'::timestamp)` ]

        ]
      }
    },
    payedInvoices: [],
    purchasedInvoices: [],
    cashflowDataOne: {
      deposits: 0,
      withdrawals: 0
    },
    responseType: 'response',
    responseMessage: 'Get investor summaries',
    cashflowDataTwo: {
      deposits: 0,
      withdrawals: 0
    },
    investor: Object.assign({
      id: companyId,
      balance: 0,
      getCashFlowFromRange: () => {
        return true;
      }
    }, newCompanyData ),
    invoices: [],
    total: 0,
    totalPurchased: 0,
    totalPayed: 0,
    companyQuery: {
      where: {
        id: companyId
      }
    },
    response: statementSummaries,
    request: {
      request: {
        guid,
        company_id: companyId,
        year: summaryValidYear,
        month: summaryValidMonth
      }
    },
    belowOfRangeRequest: {
      request: {
        guid,
        company_id: companyId,
        year: belowOfRangeYear,
        month: belowOfRangeMonth
      }
    },
    aboveOfRangeRequest: {
      request: {
        guid,
        company_id: companyId,
        year: aboveOfRangeYear,
        month: aboveOfRangeMonth
      }
    }
  },
  createInvestorInvitation: {
    guid,
    request: {
      request: {
        guid,
        company_id: companyId,
        user: newUser,
        name: 'John Doe',
        email: 'investor-unit@fondeodirecto.com',
        fee: 230,
        variable_fee_percentage: 12.22
      }
    },
    query: {
      where: {
        id: companyId
      }
    },
    userFindQuery: { where: { email: 'investor-unit@fondeodirecto.com' } },
    tokenFindQuery: { where: {
      email: 'investor-unit@fondeodirecto.com', type: 'investor-invitation' }, paranoid: true },
    token: 'token',
    newToken: {
      get: () => {
        return;
      }
    },
    tokenGet: {
      id: 1,
      token: 'JFIO2JFOIJ923',
      created_at: someDate
    },
    tokenData: {
      email: 'investor-unit@fondeodirecto.com',
      data: {
        user_name: 'John Doe',
        user_type: 'investor',
        fee: 230,
        variable_fee_percentage: 12.22
      },
      type: 'investor-invitation'
    },
    invitation: {
      name: 'John Doe',
      email: 'investor-unit@fondeodirecto.com',
      token: 'JFIO2JFOIJ923',
      id: 1
    },
    foundedToken: {
      destroy: () => {
        return;
      }
    },
    newUser,
    response: {
      id: 1,
      created_at: someDate
    },
    message: 'Create investor invitation',
    requestType,
    responseType,
    commonError,
    existentUserError: {
      path: 'company',
      error: 'Already registered'
    },
    notFoundError: notFound('company')
  },
  getBalance: {
    guid,
    query: {
      where: {
        id: 1
      },
      include: [ {
        model: Company,
        required: false,
        on: {
          id: 3
        }
      } ]
    },
    validInvestorRequest: {
      request: {
        guid,
        id: companyId,
        user_id: 1
      }
    },
    validInvestorRequestQuery: {
      where: {
        id: 1
      },
      include: [ {
        model: Company,
        required: false,
        on: {
          id: companyId
        }
      } ]
    },
    request: {
      request: {
        guid,
        id: 3,
        user_id: 1
      }
    },
    company,
    balance: '100.00',
    response: {
      total: '100.00'
    },
    adminUser: {
      id: 1,
      company_id: 2,
      role: 'ADMIN',
      Company: company
    },
    investorUser: {
      id: 2,
      company_id: companyId,
      role: 'INVESTOR',
      Company: company
    },
    commonError,
    notFoundError: notFound('company'),
    message: 'Get company balance',
    requestType,
    responseType
  },
  updateCompanyRoleSuspension: {
    guid,
    request: {
      request: {
        guid,
        id: companyId,
        role: 'CXC',
        suspended: true
      }
    },
    query: {
      where: {
        id: companyId
      }
    },
    role: 'CXC',
    id: companyId,
    suspended: true,
    company,
    message: 'Update company role suspension',
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Company'),
    transaction: {
      id: 1
    }
  },
  updateInvestorRoleSuspension: {
    guid,
    request: {
      request: {
        guid,
        id: companyId,
        suspended: true
      }
    },
    query: {
      where: {
        id: companyId,
        role: 'INVESTOR'
      }
    },
    role: 'INVESTOR',
    id: companyId,
    suspended: true,
    company,
    message: 'Update investor role suspension',
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Investor'),
    transaction: {
      id: 1
    }
  },
  propose: {
    request: {
      request: {
        guid,
        company_id: companyId
      }
    },
    findOneCall: {
      where: { id: companyId }
    },
    response: { success: true },
    company: {
      name: 'name'
    },
    message: 'Propose a company',
    guid,
    requestType,
    responseType,
    commonError
  },
  agreed: {
    request: {
      request: {
        role: 'CXC',
        user: 123,
        guid,
        id: companyId
      }
    },
    findOneCall: {
      where: { id: companyId }
    },
    response: {
      agreed_at: new Date().toString(),
      agreement_url: 'SOME_S3_URL'
    },
    agreement: {
      finalizeContract() {
        throw new Error('Please stub this!');
      }
    },
    company: {
      name: 'name'
    },
    id: companyId,
    guid,
    where: {
      company_id: companyId,
      user_id: 123
    },
    data: {
      company_agreed: true
    },
    user: 123,
    role: 'CXC',
    commonError,
    foundAgreement: {
      agreed_at: new Date(),
      agreement_url: 'SOME_S3_URL',
      updateAttributes() {
        throw new Error('Please stub this!');
      }
    }
  },
  getInvestorTransactions: {
    guid,
    message: 'Get an investor\'s transactions',
    commonError,
    notFound,
    requestType,
    payedInvoices: [],
    purchasedInvoices: [],
    investorCashWithDrawals: [],
    investorCashDeposits: [],
    responseType: 'response',
    responseMessage: 'Get an investor\'s transactions',
    invoices: [],
    total: 0,
    totalPurchased: 0,
    totalPayed: 0,
    response: [],
    request: {
      request: {
        guid,
        id: companyId,
        year: new Date().getFullYear(),
        month: new Date().getMonth()
      }
    }
  },
  updateCompanyPrepayment: {
    guid,
    request: {
      request: {
        guid,
        id: companyId,
        prepayment: true
      }
    },
    query: {
      where: {
        id: companyId,
        role: 'COMPANY'
      }
    },
    role: 'COMPANY',
    id: companyId,
    updateParams: {
      prepayment: true
    },
    response: {
      success: true
    },
    company,
    prepaymentCompany,
    message: 'Update company prepayment',
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Company')
  }
};
