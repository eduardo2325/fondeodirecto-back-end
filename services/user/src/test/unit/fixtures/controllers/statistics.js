const _ = require('lodash');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const { commonError } = require('../../../common/fixtures/errors');
const request = {
  guid,
  statistics_type: 'register-investor-by-invitation',
  token: '123456',
  event_type: 'click'
};
const newInvoiceRequest = _.cloneDeep(request);

newInvoiceRequest.statistics_type = 'investor-clicked-new-invoice-notification';

const newInvoiceCreateParams = {
  new_invoice_token: '123456',
  event_type: 'click',
  notification_type_id: 1
};

const requestInvalid = {
  guid,
  statistics_type: 'non-existent-statistics-type',
  token: '123456',
  event_type: 'click'
};

module.exports = {
  checkToken: {
    message: 'Check token',
    requestInvalid,
    request,
    newInvoiceRequest,
    response: {
      id: 1
    },
    requestType: 'request',
    responseType: 'response',
    guid,
    tokenInstance: { },
    metricInstance: {
      id: 1,
      get: () => true
    },
    plainParams: { plain: true },
    metricPlain: {
      id: 1
    },
    invitationNotificationCreateParams: { token: '123456', event_type: 'click' },
    newInvoiceCreateParams,
    metricNoValidError: {
      errors: [ {
        path: 'statistics',
        message: 'Metric no valid'
      } ]
    },
    tokenNotFoundError: {
      errors: [ {
        path: 'token',
        message: 'Token not found'
      } ]
    },
    findTokenQuery: {
      where: {
        token: '123456'
      }
    },
    commonError
  }
};
