const { sequelizeError, commonError, notFound } = require('../../../common/fixtures/errors');
const { Company, Operation } = require('../../../../models');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const requestType = 'request';
const responseType = 'response';
const rfc = 'FODA900806965';
const companyId = 1;
const cxpCompanyId = 2;

const balance = 100;
const movement = {
  id: 1,
  amount: 100
};

const company = {
  rfc,
  id: companyId,
  name: 'A company',
  getBalance: () => balance
};
const user = {
  id: 1,
  company_id: companyId,
  Company: company,
  role: 'ADMIN'
};
const invoice = {
  id: 2,
  client_company_id: cxpCompanyId,
  Client: {
    id: cxpCompanyId,
    name: 'cxp user'
  },
  status: 'PUBLISHED',
  Operation: {
    status: 'FUNDED'
  },
  getGeneralInfo: () => null
};
const invoiceBasicInfo = {
  id: 2,
  client_company_id: cxpCompanyId,
  status: 'funded'
};
const paymentInProcessInvoice = {
  id: invoice.id,
  Client: {
    id: invoice.Client.id,
    name: invoice.Client.name
  },
  status: 'PAYMENT_IN_PROCESS',
  getGeneralInfo: () => null
};
const insufficientBalance = movement.amount - 1;
const companyObj = {
  rfc,
  id: companyId,
  balance: 1000,
  save: () => {
    return;
  }
};
const transactions = [
  {
    id: 1,
    amount: 100,
    getBasicInfo: () => null
  }, {
    id: 2,
    amount: 200,
    getBasicInfo: () => null
  }, {
    id: 3,
    amount: 300,
    getBasicInfo: () => null
  }
];
const basicInfoTransactions = [
  {
    id: 1,
    amount: 100
  }, {
    id: 2,
    amount: 200
  }, {
    id: 3,
    amount: 300
  }
];
const deposit_date = new Date();
const key = 'path/to/file.png';
const transaction = {
  rollback: () => {
    return;
  }
};
const reason = 'A problem';

module.exports = {
  withdraw: {
    request: {
      request: {
        guid,
        user_id: user.id,
        amount: movement.amount
      }
    },
    response: movement,
    withdraw: {
      getBasicInfo: () => movement
    },
    withdrawBasicInfo: movement,
    user,
    message: 'Create withdraw',
    guid,
    requestType,
    responseType,
    commonError,
    balance,
    insufficientBalance,
    insufficientFundsError: sequelizeError('Transaction', 'Insufficient funds'),
    company,
    userFindOneParams: {
      where: {
        id: user.id
      },
      include: [
        { model: Company, as: 'Company' }
      ]
    }
  },
  deposit: {
    request: {
      request: {
        guid,
        user_id: user.id,
        amount: movement.amount,
        deposit_date,
        receipt: key
      }
    },
    response: movement,
    deposit: {
      getBasicInfo: () => movement
    },
    depositBasicInfo: movement,
    user,
    message: 'Create deposit',
    guid,
    requestType,
    responseType,
    commonError,
    userFindOneParams: {
      where: {
        id: user.id
      },
      include: [ {
        model: Company
      } ]
    },
    depositParams: [
      movement.amount,
      companyId,
      key,
      deposit_date
    ],
    eventParams: [
      movement,
      company,
      guid
    ]
  },
  getTransactions: {
    request: {
      request: {
        guid,
        id: companyId,
        status: 'PENDING'
      }
    },
    response: {
      transactions: basicInfoTransactions,
      balance: '0.00'
    },
    invesorResponse: {
      transactions: basicInfoTransactions,
      balance: '300.00'
    },
    investorCompany: {
      getBalance: () => {
        return;
      },
      role: 'INVESTOR'
    },
    company: {
      getBalance: () => {
        return;
      },
      role: 'COMPANY'
    },
    balance: '300.00',
    transactions,
    basicInfoTransactions,
    message: 'Get company transactions',
    guid,
    requestType,
    responseType,
    commonError,
    findAllQuery: {
      where: {
        company_id: companyId,
        status: 'PENDING',
        data: { invoice_id: { $eq: null } }
      }
    },
    findOneQuery: {
      where: {
        id: companyId
      }
    }
  },
  approve: {
    request: {
      request: {
        guid,
        id: 1,
        company_id: companyId
      }
    },
    message: 'Approve transaction',
    guid,
    requestType,
    responseType,
    query: {
      where: {
        id: 1,
        status: 'PENDING'
      }
    },
    companyCall: {
      where: {
        id: companyId
      }
    },
    transaction,
    pendingDepositTransaction: {
      type: 'DEPOSIT',
      status: 'PENDING',
      amount: 100,
      company_id: companyId,
      save: () => {
        return;
      },
      getBasicInfo: () => {
        return;
      }
    },
    pendingWithdrawTransaction: {
      type: 'WITHDRAW',
      status: 'PENDING',
      amount: 100,
      company_id: companyId,
      save: () => {
        return;
      },
      getBasicInfo: () => {
        return;
      }
    },
    pendingHighWithdrawTransaction: {
      type: 'WITHDRAW',
      status: 'PENDING',
      amount: 100000,
      company_id: companyId,
      save: () => {
        return;
      },
      getBasicInfo: () => {
        return;
      }
    },
    insufficientFundsError: sequelizeError('PendingTransaction', 'Insufficient funds'),
    save: {
      transaction,
      returning: true
    },
    company: companyObj,
    commonError,
    notFound: notFound('PendingTransaction'),
    companyId,
    increasedBalance: 1100,
    decreaseBalance: 900
  },
  reject: {
    request: {
      request: {
        guid,
        id: 1,
        company_rfc: rfc,
        reason
      }
    },
    message: 'Reject transaction',
    guid,
    requestType,
    responseType,
    reason,
    query: {
      where: {
        id: 1,
        status: 'PENDING'
      }
    },
    pendingDepositTransaction: {
      type: 'DEPOSIT',
      status: 'PENDING',
      amount: 100,
      company_rfc: rfc,
      save: () => {
        return;
      },
      getBasicInfo: () => {
        return;
      }
    },
    save: {
      returning: true
    },
    commonError,
    notFound: notFound('PendingTransaction')
  },
  createClientInvoicePayment: {
    requestAdmin: {
      request: {
        guid,
        user_id: user.id,
        user_role: 'ADMIN',
        invoice_id: invoice.id,
        amount: movement.amount,
        payment_date: deposit_date,
        receipt: key
      }
    },
    requestCXP: {
      request: {
        guid,
        user_id: user.id,
        user_role: 'CXP',
        company_id: cxpCompanyId,
        invoice_id: invoice.id,
        amount: movement.amount,
        payment_date: deposit_date,
        receipt: key
      }
    },
    requestInvalidCXP: {
      request: {
        guid,
        user_id: user.id,
        user_role: 'CXP',
        company_id: 0,
        invoice_id: invoice.id,
        amount: movement.amount,
        payment_date: deposit_date,
        receipt: key
      }
    },
    invoiceBasicInfo,
    response: invoiceBasicInfo,
    payment: {
      getBasicInfo: () => movement
    },
    paymentBasicInfo: movement,
    message: 'Create client invoice payment',
    guid,
    requestType,
    responseType,
    commonError,
    invoice,
    updatedInvoice: paymentInProcessInvoice,
    invoiceFindOneParams: {
      where: {
        id: invoice.id,
        status: 'PUBLISHED'
      },
      include: [
        { model: Company, as: 'Client' },
        { model: Operation,
          where: {
            status: {
              $or: [
                'FUNDED',
                'PAYMENT_DUE'
              ]
            }
          }
        }
      ]
    },
    stateMachineParams: [
      invoice,
      transaction
    ],
    transaction,
    paymentParams: [
      invoice,
      movement.amount,
      deposit_date,
      key,
      transaction
    ],
    eventParams: [
      movement.amount,
      invoice,
      invoice.Client.name,
      guid
    ],
    notFoundError: notFound('Invoice')
  },
  getClientInvoicePayment: {
    request: {
      request: {
        guid,
        user_id: user.id,
        invoice_id: invoice.id
      }
    },
    response: movement,
    payment: {
      getBasicInfo: () => movement
    },
    paymentBasicInfo: movement,
    message: 'Get client invoice payment',
    guid,
    requestType,
    responseType,
    commonError,
    invoice,
    invoiceFindOneParams: {
      where: {
        id: invoice.id,
        status: 'PUBLISHED'
      },
      include: [
        { model: Operation, where: { status: 'PAYMENT_IN_PROCESS' } }
      ]
    },
    pendingTransactionFindOneParams: {
      where: {
        type: 'DEPOSIT',
        company_id: invoice.client_company_id,
        data: {
          invoice_id: invoice.id
        }
      }
    },
    invoiceNotFoundError: notFound('Invoice'),
    paymentNotFoundError: notFound('PendingTransaction')
  }
};
