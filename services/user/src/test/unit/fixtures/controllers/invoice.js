const _ = require('lodash');
const { commonError, notFound, sequelizeError } = require('../../../common/fixtures/errors');
const { Company, OperationCost, Operation, InvestorTransaction, sequelize } = require('../../../../models');
const rfc = 'FODA900806965';
const companyId = 1;
const invoiceId = 1;
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const requestType = 'request';
const responseType = 'response';
const today = new Date();
const expiration = new Date();
const fee = 100;
const gain = 100000;
const balance = 100000;

today.setHours(0, 0, 0, 0);
expiration.setHours(0, 0, 0, 0);

const user_id = 1;
const userInformation = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'ADMIN',
  company_id: companyId
};
const cxcUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXC',
  company_id: companyId
};
const cxpUser = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'CXP',
  company_id: companyId
};
const investor = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'investor',
  company_id: companyId
};
const company = {
  id: companyId,
  rfc,
  role: 'ADMIN',
  name: 'Fondeo Directo',
  business_name: 'Fondeo Directo',
  holder: 'Fondeo Directo',
  clabe: '002090700355936431',
  color: '#1234',
  allowedCXP: () => null
};
const issuer = {
  id: 1,
  name: 'name',
  email: 'email',
  role: 'ADMIN',
  company_id: 2
};
const receptor = _.cloneDeep(company);
const invoice = {
  emission_date: '',
  Operation: {
    id: 1
  },
  status: 'PENDING',
  created_at: new Date(),
  investor_company_id: companyId,
  company_id: companyId,
  term_days: 100,
  uuid: guid,
  getGeneralInfo: function() {
    return this;
  },
  getGeneralInfoAsAdmin: () => {
    return this;
  }
};
const invoiceApproved = {
  status: 'APPROVED',
  getGeneralInfo: () => {
    return this;
  },
  getCompany: () => {
    return this;
  },
  investor_company_id: companyId,
  company_id: companyId,
  client_company_id: companyId,
  id: 1,
  Client: {
    id: companyId,
    rfc,
    role: 'ADMIN',
    name: 'Fondeo Directo',
    business_name: 'Fondeo Directo',
    holder: 'Fondeo Directo',
    clabe: '002090700355936431',
    color: '#1234',
    allowedCXP: () => true
  },
  Operation: {
    id: 1,
    deleted_at: null
  }
};
const invoiceInstance = function(status) {
  return {
    id: 1,
    status: status,
    created_at: new Date(),
    investor_company_id: companyId,
    company_id: companyId,
    term_days: 100,
    uuid: guid,
    getClient: () => {
      return this;
    },
    getPublishEstimate: () => {
      return this;
    },
    getCompany: () => {
      return this;
    },
    getGeneralInfo: () => {
      return this;
    },
    getGeneralInfoAsAdmin: () => {
      return this;
    },
    dataValues: {
      is_available: true
    }
  };
};
const publishEstimate = {
  is_available: false,
  client_name: 'Sureplex',
  number: '6381472A-F8671280A1679164',
  total: '150000.00',
  operation_cost: '5747.92',
  reserve: '15000.00',
  fund_payment: '129252.08'
};
const publishSummaryTotals = {
  totalInvoices: 2,
  summary: {
    invoices_total: '158374.00',
    operation_cost_total: '5960.64',
    reserve_total: '15837.40',
    fund_payment_total: '136575.96'
  }
};
const publishSummaryResponse = {
  invoices: [
    { is_available: false,
      client_name: 'Sureplex',
      number: '6381472A-F8671280A1679164',
      total: '150000.00',
      operation_cost: '5747.92',
      reserve: '15000.00',
      fund_payment: '129252.08' } ],
  total_invoices: 2,
  summary: { invoices_total: '158374.00',
    operation_cost_total: '5960.64',
    reserve_total: '15837.40',
    fund_payment_total: '136575.96'
  }
};
const publishedInvoice = {
  status: 'PUBLISHED',
  created_at: new Date(),
  getBasicInfo: function() {
    return this;
  },
  getBasicInfoWithClient: () => {
    return;
  }
};
const operation = function(status) {
  return {
    invoice_id: 1,
    expiration_date: expiration,
    published_date: today,
    status: status,
    user_id: 1
  };
};
const newInvoice = {
  client_name: company.name,
  client_color: company.color,
  status: invoice.status.toLowerCase(),
  created_at: invoice.created_at.toString()
};
const createRequest = {
  guid,
  user_id,
  company_rfc: rfc,
  client_rfc: rfc
};
const createData = {
  guid,
  user_id,
  company_id: 2,
  client_company_id: companyId
};
const invoiceGeneralInfo = {
  company_id: companyId,
  company_name: company.name,
  company_color: company.color,
  business_name: company.business_name,
  number: 'ABC123',
  subtotal: '1000000',
  emission_date: 'today',
  expiration: 'tomorrow',
  expiration_extra: 'tomorrow',
  expiration_days: 0,
  uuid: guid,
  status: 'PENDING'
};
const investorFundEstimate = {
  subtotal: '100000',
  earnings: '2500',
  commission: '500',
  perception: '102500'
};
const marketplaceInvoices = [
  {
    id: 1,
    subtotal: 300,
    status: 'PUBLISHED'
  }, {
    id: 2,
    subtotal: 100,
    status: 'PUBLISHED'
  }, {
    id: 3,
    subtotal: 500,
    status: 'PUBLISHED'
  }, {
    id: 4,
    subtotal: 400,
    status: 'PUBLISHED'
  }
];
const availabilityInvoicesRequest = [
  { id: 1 },
  { id: 2 },
  { id: 3 }
];
const invoicesAvailability = [ 1, 2, 3 ];
const availabilityInvoiceInstances = [
  {
    dataValues: {
      id: 1,
      is_available: true
    }
  }, {
    dataValues: {
      id: 2,
      is_available: false
    }
  }, {
    dataValues: {
      id: 3,
      is_available: true
    }
  }
];
const availabilityInvoices = [
  {
    id: 1,
    is_available: true
  }, {
    id: 2,
    is_available: false
  }, {
    id: 3,
    is_available: true
  }
];
const startDate = new Date('2017-06-05');
const endDate = new Date('2017-06-15');
const operationTerm = {
  fund_date: today,
  expiration: today,
  operation_term: 90
};
const paymentSummary = {
  total: 1000
};
const adminInvoiceDetail = {
  invoiceDetail: {
    total: 100
  },
  operationSummary: {
    total: 200
  },
  cxcPayment: {
    total: 300
  },
  investorPayment: {
    total: 400
  }
};
const transaction = {
  rollback: () => {
    return false;
  }
};

const availableInvoice =  {
  id: 1,
  is_available: true,
  client_name: 'Sureplex',
  total: '732.10',
  term_days: 59,
  interest: '18.00',
  isr: '0',
  fee: '1.80',
  gain: '16.20',
  perception: '748.30'
};

const invoices = [
  {
    id: 1,
    status: 'PUBLISHED',
    getInvoiceAmounts: () => {
      return;
    },
    getBasicOperationInfo: () => {
      return;
    },
    save: () => {
      return this;
    },
    total: 1000,
    Operation: {
      status: 'PENDING'
    },
    dataValues: {
      is_available: true
    }
  }
];

const shoppingCartResponse = {
  total: 2,
  invoices: [
    { id: 10053, is_available: true },
    { id: 10054, is_available: true } ],
  summary: {
    invoices_total: '1165.20',
    interests_total: '28.65',
    isr_total: '0.00',
    fee_total: '2.87',
    earnings_total: '1190.99',
    perception_total: '25.79'
  }
};
const shoppingCartNegativeGain = _.cloneDeep(shoppingCartResponse);

delete shoppingCartNegativeGain.summary.perception_total;
shoppingCartNegativeGain.summary.perception_total = -1;

const shoppingCartUnavailableInvoices = _.cloneDeep(shoppingCartResponse);

shoppingCartUnavailableInvoices.invoices[0].is_available = false;

const successfulResponse = _.cloneDeep(shoppingCartResponse);
const failResponse = _.cloneDeep(shoppingCartResponse);

successfulResponse.fund = true;
delete successfulResponse.summary.perception_total;
failResponse.fund = false;
failResponse.invoices[0].is_available = false;
delete failResponse.summary.perception_total;

const basicOperationInfo = [
  { id: 10053, operation_id: 50004 },
  { id: 10054, operation_id: 50005 }
];

const invoicesIds = [ 1, 2 ];
const pending_transactions = [ { id: 1 }, { id: 2 } ];
const pendingTransactions = [ {
  id: 1,
  amount: 10,
  data: {
    invoice_id: 7,
    company_id: 8
  },
  created_at: 'created-at',
  isForInvestorInvoiceFund: function() {
    return true;
  }
}, {
  id: 2,
  created_at: 'created-at',
  amount: 20,
  data: {
    invoice_id: 9,
    company_id: 10
  },
  isForInvestorInvoiceFund: function() {
    return false;
  }
} ];

const bank_report_id = 4;
const bulkAppoveResponse = {
  success: [],
  fail: []
};

const invoiceBulkCompleted = {
  getAdminCxcPayment: function() {
    return true;
  },
  getAdminInvestorPayment: function() {
    return true;
  }
};

const cxcPayment = {
  reserve: 10
};
const investorPayment = {
  total_payment: 100
};
const invoiceFindOneQuery = {
  where: {
    id: 9
  },
  include: [
    { model: Company, as: 'Company', include: OperationCost },
    { model: Company, as: 'Client', include: OperationCost },
    { model: Company, as: 'Investor', include: OperationCost },
    { model: Operation, include: InvestorTransaction }
  ]
};


const callbackApprove1 = function() {
  return true;
};
const callbackApprove2 = function() {
  return true;
};
const callbackDataOne = { callback: callbackApprove1, completed: false, pending_transaction: {} };
const callbackDataTwo = { callback: callbackApprove2, completed: false, pending_transaction: {} };
const requestControllerCompleted = {
  cxp_payment: 20,
  cxp_payment_date: 'created-at',
  cxc_payment: 10,
  investor_payment: 100,
  fondeo_payment_date: new Date().toDateString()
};
const arrayCallbacks = [
  callbackDataOne, callbackDataTwo
];

const invoicesBulkDelete = [
  { id: 1, update: () => true },
  { id: 2, update: () => true },
  { id: 3, update: () => true }
];
const invoicesIdsRequest = [
  { id: 1 },
  { id: 2 },
  { id: 3 }
];

module.exports = {
  create: {
    guid,
    request: {
      request: createRequest
    },
    validateParams: [
      user_id,
      rfc,
      rfc
    ],
    createParams: [ createData ],
    generalInfoParams: [ receptor, 'client' ],
    instances: [ issuer, receptor ],
    invoice,
    newInvoice,
    commonError,
    companyCantApproveInvoiceError: sequelizeError('client_company_id', 'Receptor is suspended')
  },
  getInvoices: {
    request: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1
      }
    },
    response: {
      invoices: [ invoice ],
      total_invoices: 4,
      total_pages: 2
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    cxcUser,
    cxpUser,
    whereCxc: {
      company_id: companyId
    },
    whereCxp: {
      client_company_id: companyId
    },
    limit: 2,
    offset: 2,
    message: 'Get invoices',
    guid,
    requestType,
    responseType,
    invoices: [ invoice ],
    commonError
  },
  getInvoice: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1
      }
    },
    response: invoiceGeneralInfo,
    whereInvoice: {
      include: [
        { all: true },
        { model: Operation, include: InvestorTransaction }
      ],
      where: {
        id: 1
      }
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    invoice: {
      getBasicInfo: () => {
        return;
      },
      getBasicInfoWithClient: () => {
        return;
      },
      company_id: companyId
    },
    fundedInvoice: {
      getBasicInfo: () => {
        return;
      },
      getBasicInfoWithClient: () => {
        return;
      },
      company_id: companyId,
      investor_company_id: 2
    },
    invoiceWrongCompanyId: {
      company_id: 2
    },
    invoiceGeneralInfo,
    publishedInvoice,
    userInformation,
    userInvestor: {
      role: 'INVESTOR',
      company_id: 2
    },
    userCxc: {
      role: 'CXC',
      company_id: companyId
    },
    message: 'Get invoice',
    guid,
    requestType,
    responseType,
    commonError,
    notFound: notFound('Invoice')
  },
  approve: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoices: [
          { id: 1 }
        ],
        expiration
      }
    },
    response: { success: true },
    invoice: {
      getCompany: () => {
        return this;
      },
      getGeneralInfo: () => {
        return this;
      },
      status: 'PENDING'
    },
    expiration,
    user_id: 1,
    invoice_id: 1,
    message: 'Approve invoice',
    guid,
    requestType,
    responseType,
    commonError
  },
  getEstimate: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1
      }
    },
    response: invoiceGeneralInfo,
    whereInvoice: {
      include: [
        { model: Company, as: 'Company' },
        { model: Company, as: 'Client', include: OperationCost }
      ],
      where: {
        id: 1
      }
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    invoice: {
      getEstimate: () => {
        return;
      },
      company_id: companyId
    },
    invoiceGeneralInfo,
    userInformation,
    message: 'Get estimate',
    guid,
    requestType,
    responseType,
    commonError
  },
  getDetail: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1
      }
    },
    response: invoiceGeneralInfo,
    whereInvoice: {
      include: [ { all: true } ],
      where: {
        id: 1
      }
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    invoice: {
      getDetail: () => {
        return;
      },
      client_company_id: companyId
    },
    invoiceGeneralInfo,
    userInformation,
    message: 'Get detail',
    guid,
    requestType,
    responseType,
    commonError
  },
  reject: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1,
        reason: 'Valid reason'
      }
    },
    response: invoiceGeneralInfo,
    invoice: {
      getCompany: () => {
        return this;
      },
      getGeneralInfo: () => {
        return this;
      }
    },
    instances: [ invoiceInstance('PENDING') ],
    invoiceGeneralInfo,
    company,
    reason: 'Valid reason',
    user_id: 1,
    invoice_id: 1,
    message: 'Reject invoice',
    guid,
    requestType,
    responseType,
    commonError,
    reasonError: {
      errors: [ {
        path: 'reason',
        message: 'reason cannot be null'
      } ]
    }
  },
  getPublishSummary: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoices: [
          { id: 1 },
          { id: 2 }
        ]
      }
    },
    invoicesIds,
    invoices: [ invoiceInstance('PENDING') ],
    company,
    companyId,
    userQuery: {
      where: {
        id: 1
      },
      include: [
        { model: Company, include: OperationCost }
      ]
    },
    invoiceQuery: {
      attributes: [
        'id', 'total', 'expiration', 'company_id', 'client_company_id', 'tax_total', 'number', 'emission_date',
        [ sequelize.literal(`CASE WHEN "Invoice"."status" IS NOT NULL
           AND "Invoice"."status" LIKE '%APPROVED%' THEN true ELSE false END`), 'is_available' ]
      ],
      where: {
        id: {
          $in: invoicesIds
        }
      },
      include: [
        { model: Company, as: 'Client', include: OperationCost }
      ]
    },
    publishEstimate,
    publishSummaryTotals,
    publishSummaryResponse,
    message: 'Get invoices publish summary',
    guid,
    requestType,
    responseType,
    commonError,
    user: {
      Company: {
        getBalance: () => {
          return;
        },
        OperationCost: {
          fee,
          variable_fee_percentage: 10
        },
        taxpayer_type: 'MORAL',
        id: companyId
      }
    },
    notFound: notFound('Invoice')
  },
  publish: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoices: [
          { id: 1 }
        ]
      }
    },
    invoicesIds,
    invoices: [ invoiceInstance('PENDING') ],
    company,
    companyId,
    userQuery: {
      where: {
        id: 1
      },
      include: [
        { model: Company, include: OperationCost }
      ]
    },
    invoiceQuery: {
      where: {
        id: {
          $in: [ 1 ]
        }
      }
    },
    message: 'Publish invoices',
    guid,
    requestType,
    responseType,
    commonError,
    user: {
      Company: {
        getBalance: () => {
          return;
        },
        OperationCost: {
          fee,
          variable_fee_percentage: 10
        },
        taxpayer_type: 'MORAL',
        id: companyId
      }
    },
    response: { success: true },
    notFound: notFound('Invoice'),
    companyCantApproveInvoiceError: sequelizeError('client_company_id', 'Receptor is suspended')
  },
  fund: {
    request: {
      request: {
        guid,
        user_id: 1,
        investor_company_id: 1,
        invoices: [
          {
            id: 1
          },
          {
            id: 2
          }
        ],
        fund: true
      }
    },
    invoices,
    availableInvoice,
    shoppingCartUnavailableInvoices,
    companyId,
    transaction: {
      rollback: () => {
        return false;
      }
    },
    basicOperationInfo,
    shoppingCartResponse,
    shoppingCartNegativeGain,
    successfulResponse,
    failResponse,
    userQuery: {
      where: {
        id: 1
      },
      include: [
        { model: Company, include: OperationCost }
      ]
    },
    invoiceQuery: {
      attributes: [
        'id', 'total', 'expiration', 'company_id', 'client_company_id', 'tax_total', 'number', 'emission_date',
        [ sequelize.literal(`CASE WHEN "Operation"."status" IS NOT NULL
           AND "Operation"."status" LIKE '%PENDING%' THEN true ELSE false END`), 'is_available' ]
      ],
      where: {
        id: {
          $in: invoicesIds
        },
        status: 'PUBLISHED',
        expiration: {
          $gt: today
        }
      },
      include: [
        { model: Company, as: 'Company', include: OperationCost },
        { model: Company, as: 'Client', include: OperationCost },
        { model: Operation, required: true }
      ]
    },
    invoiceGeneralInfo,
    message: 'Fund invoices',
    guid,
    requestType,
    responseType,
    commonError,
    user: {
      Company: {
        getBalance: () => {
          return;
        },
        OperationCost: {
          fee,
          variable_fee_percentage: 10
        },
        taxpayer_type: 'MORAL',
        id: companyId
      }
    },
    notFound: notFound('Invoice'),
    notEnoughBalanceError: sequelizeError('Investor', 'Can not afford'),
    negativeGainError: sequelizeError('Investor', 'Can not afford'),
    alreadyFundedError: sequelizeError('Invoice', 'Invoice already funded'),
    hasExpiredError: sequelizeError('Invoice', 'Invoice has expired'),
    balance,
    gain,
    fee,
    variable_fee_percentage: 10,
    taxpayer_type: 'MORAL',
    balanceNotEnough: 10,
    gainNegative: -150
  },
  completed: {
    sendengoStrategy: {
      execCallback: () => {
        return true;
      }
    },
    request: {
      request: {
        guid,
        invoice_id: invoice.id,
        company_id: companyId,
        cxp_payment: 20,
        cxp_payment_date: today,
        cxc_payment: 30,
        investor_payment: 50,
        fondeo_payment_date: today
      }
    },
    invoice,
    operation: operation('PAYMENT_IN_PROCESS'),
    response: invoiceGeneralInfo,
    producerParams: [
      invoice,
      {
        cxcPayment: 30,
        cxpPayment: 20,
        investorPayment: 50,
        cxpPaymentDate: today,
        fondeoPaymentDate: today
      },
      guid
    ],
    createInvoiceCompletedParams: [
      invoice.id,
      companyId,
      {
        cxpPayment: 20,
        cxpPaymentDate: today,
        cxcPayment: 30,
        investorPayment: 50,
        fondeoPaymentDate: today
      },
      transaction
    ],
    invoiceGeneralInfo,
    company,
    transaction,
    invoice_id: 1,
    message: 'Completed invoice',
    guid,
    requestType,
    responseType,
    commonError
  },
  lost: {
    request: {
      request: {
        guid,
        invoice_id: 1,
        company_id: companyId,
        cxc_payment: 30,
        investor_payment: 50,
        payment_date: today
      }
    },
    response: invoiceGeneralInfo,
    invoice: {
      status: 'PUBLISHED',
      getAdminBasicInfo: () => {
        return this;
      },
      investor_company_id: companyId,
      company_id: companyId,
      id: 1,
      Operation: {
        status: 'PAYMENT_DUE'
      }
    },
    findOneCall: {
      where: {
        id: 1,
        status: 'PUBLISHED'
      },
      include: [
        { model: Company, as: 'Company' },
        { model: Company, as: 'Client' },
        { model: Operation, where: { status: 'PAYMENT_DUE' } }
      ]
    },
    invoiceGeneralInfo,
    paymentTransaction: [
      30,
      companyId,
      companyId,
      1,
      today,
      true,
      transaction
    ],
    investorTransaction: [
      50,
      companyId,
      companyId,
      1,
      today,
      false,
      transaction
    ],
    company,
    transaction,
    invoice_id: 1,
    message: 'Lost operation',
    guid,
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Invoice')
  },
  latePayment: {
    request: {
      request: {
        guid,
        invoice_id: 1,
        company_id: companyId,
        cxc_payment: 30,
        investor_payment: 50,
        cxp_payment: 30,
        fondeo_payment_date: today,
        cxp_payment_date: today
      }
    },
    response: invoiceGeneralInfo,
    invoice: {
      getAdminBasicInfo: () => {
        return this;
      },
      save: () => {
        return this;
      },
      investor_company_id: companyId,
      company_id: companyId,
      client_company_id: companyId,
      id: 1,
      Operation: {
        save: () => {
          return this;
        }
      }
    },
    findOneCall: {
      where: {
        id: 1,
        status: 'PUBLISHED'
      },
      include: [
        { model: Company, as: 'Company' },
        { model: Company, as: 'Client' },
        { model: Operation, where: { status: 'PAYMENT_DUE' } }
      ]
    },
    invoiceGeneralInfo,
    paymentTransaction: [
      30,
      companyId,
      companyId,
      1,
      today,
      true,
      transaction
    ],
    investorTransaction: [
      50,
      companyId,
      companyId,
      1,
      today,
      false,
      transaction
    ],
    company,
    transaction,
    invoice_id: 1,
    message: 'Late payment operation',
    guid,
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Invoice')
  },
  rejectPublished: {
    request: {
      request: {
        guid,
        invoice_id: 1,
        reason: 'Valid reason'
      }
    },
    cxcRequest: {
      request: {
        guid,
        invoice_id: 1,
        reason: 'Valid reason',
        user_role: 'CXC',
        company_id: companyId
      }
    },
    response: invoiceGeneralInfo,
    invoice: {
      published: new Date(),
      annual_cost: 15,
      fd_commision: 10,
      reserve: 5,
      getCompany: () => {
        return this;
      },
      getGeneralInfo: () => {
        return this;
      }
    },
    instances: [ invoiceApproved, null, null ],
    invoiceGeneralInfo,
    company,
    reason: 'Valid reason',
    destroyQuery: [ {
      where: {
        id: 1
      }
    } ],
    invoiceCxcQuery: [ {
      where: {
        id: 1,
        status: 'PUBLISHED',
        company_id: companyId
      },
      include: { model: Operation, where: { status: 'PENDING' } }
    } ],
    invoiceQuery: [ {
      where: {
        id: invoiceId,
        status: 'PUBLISHED'
      },
      include: { model: Operation, where: { status: 'PENDING' } }
    } ],
    message: 'Reject published invoice',
    guid,
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Invoice'),
    reasonError: {
      errors: [ {
        path: 'reason',
        message: 'reason cannot be null'
      } ]
    }
  },
  rejectFunded: {
    request: {
      request: {
        guid,
        invoice_id: 1,
        reason: 'Valid reason'
      }
    },
    response: invoiceGeneralInfo,
    invoiceToClone: {
      fund_date: new Date(),
      investor_company_id: companyId,
      fee: 250,
      getCompany: () => {
        return this;
      },
      getGeneralInfo: () => {
        return this;
      },
      save: () => {
        return this;
      },
      Operation: {
        id: 1,
        status: 'FUND_REQUESTED',
        InvestorTransaction: {
          operation_id: 1,
          status: 'PENDING'
        }
      }
    },
    investorCompanyId: companyId,
    invoiceGeneralInfo,
    company,
    reason: 'Valid reason',
    pendingTransactionParams: [
      1,
      undefined
    ],
    transaction: undefined,
    message: 'Reject fund requested invoice',
    guid,
    requestType,
    responseType,
    commonError,
    reasonError: {
      errors: [ {
        path: 'reason',
        message: 'reason cannot be null'
      } ]
    }
  },
  approveFund: {
    request: {
      request: {
        guid,
        invoice_id: 1,
        company_id: companyId
      }
    },
    response: invoiceGeneralInfo,
    invoice: {
      getGeneralInfoAsAdmin: () => {
        return this;
      },
      Operation: {
        status: 'FUND_REQUESTED',
        InvestorTransaction: {
          status: 'PENDING'
        }
      }
    },
    transaction,
    invoiceGeneralInfo,
    company,
    payment: 1000,
    message: 'Approve fund request invoice',
    guid,
    requestType,
    responseType,
    commonError,
    notFoundError: notFound('Invoice'),
    transactionArguments: [
      1,
      companyId,
      transaction
    ]
  },
  getFundEstimate: {
    sendengoStrategy: {
      selectByCompanyId: () => {
        return true;
      }
    },
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1
      }
    },
    response: {
      expiration,
      total: '10000000',
      interest: '10000000',
      commission: '100000',
      fund_total: '100000',
      reserve: '100000000',
      fund_payment: '1000',
      expiration_payment: '1',
      tax_total: '1000000'
    },
    invoice: {
      getFundEstimate: () => {
        return this;
      },
      total: 100000000
    },
    estimate: {
      expiration,
      total: '10000000',
      interest: '10000000',
      commission: '100000',
      fund_total: '100000',
      reserve: '100000000',
      fund_payment: '1000',
      expiration_payment: '1',
      tax_total: '1000000'
    },
    user_id: 1,
    invoice_id: 1,
    message: 'Get fund estimate',
    guid,
    requestType,
    responseType,
    commonError
  },
  getXml: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1
      }
    },
    response: {
      xml: 'ASDQWEZXC'
    },
    whereInvoice: {
      attributes: [ 'id', 'created_at', 'client_company_id', 'cfdi' ],
      where: {
        id: 1
      }
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    invoice: {
      getDetail: () => {
        return;
      },
      client_company_id: companyId,
      cfdi: 'ASDQWEZXC'
    },
    userInformation,
    message: 'Get xml',
    guid,
    requestType,
    responseType,
    commonError
  },
  getMarketplace: {
    request: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1
      }
    },
    requestTotal: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1,
        min_total: '300',
        max_total: '500'
      }
    },
    requestMinTotal: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1,
        min_total: '300'
      }
    },
    requestStartDate: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1,
        start_date: '2017-06-05'
      }
    },
    requestStartEndDate: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1,
        start_date: '2017-06-05',
        end_date: '2017-06-15'
      }
    },
    count: 4,
    maxTotal: 500,
    response: {
      invoices: marketplaceInvoices,
      total_invoices: 4,
      max_total: '500'
    },
    responseWithZero: {
      invoices: marketplaceInvoices,
      total_invoices: 4,
      max_total: '0'
    },
    countQuery: {
      where: {
        status: 'PUBLISHED'
      }
    },
    maxQuery: {
      where: {
        status: 'PUBLISHED'
      }
    },
    where: {
      status: 'PUBLISHED'
    },
    whereTotal: {
      status: 'PUBLISHED',
      expiration: {
        $gte: today
      },
      total: {
        $lte: 500,
        $gte: 300
      }
    },
    whereMinTotal: {
      status: 'PUBLISHED',
      expiration: {
        $gte: today
      }
    },
    whereStartDate: {
      status: 'PUBLISHED',
      expiration: {
        $gte: new Date(startDate.setHours(0, 0, 0, 0))
      }
    },
    whereStartEndDate: {
      status: 'PUBLISHED',
      expiration: {
        $gte: new Date(startDate.setHours(0, 0, 0, 0)),
        $lte: new Date(endDate.setHours(23, 59, 59, 59))
      }
    },
    message: 'Get marketplace',
    guid,
    requestType,
    responseType,
    invoices: marketplaceInvoices,
    commonError
  },
  getInvestorFundEstimate: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1,
        company_id: companyId
      }
    },
    response: investorFundEstimate,
    whereInvoice: {
      where: {
        id: 1,
        $or: {
          status: 'PUBLISHED',
          investor_company_id: companyId
        }
      },
      include: [
        { model: Operation, include: InvestorTransaction }
      ]
    },
    invoice: {
      status: 'PUBLISHED',
      getInvestorFundEstimate: () => {
        return;
      },
      company_id: companyId,
      Operation: {
        status: 'PENDING'
      }
    },
    fundRequestedInvoice: {
      status: 'FUND_REQUESTED',
      getInvestorFundEstimateFromTransaction: () => {
        return;
      },
      Operation: {
        status: 'FUND_REQUESTED',
        InvestorTransaction: {
          status: 'PENDING'
        }
      },
      company_id: companyId
    },
    invoiceFee: {
      getInvestorFundEstimate: () => {
        return;
      },
      company_id: companyId,
      fee: 400
    },
    user: {
      Company: {
        OperationCost: {
          fee: 250,
          variable_fee_percentage: 10
        },
        taxpayer_type: 'Moral'
      }
    },
    userWhere: {
      where: {
        id: 1
      },
      include: [
        { model: Company, include: OperationCost }
      ]
    },
    investorFundEstimate,
    message: 'Get investor fund estimate',
    guid,
    requestType,
    responseType,
    commonError,
    percentages: {
      fee: 250,
      variable_fee_percentage: 10
    },
    invoiceFeeCall: 400
  },
  getInvestorProfitEstimate: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1,
        company_id: companyId
      }
    },
    response: investorFundEstimate,
    whereInvoice: {
      where: {
        id: 1,
        $or: {
          status: 'PUBLISHED',
          investor_company_id: companyId
        }
      },
      include: [
        { model: Operation, include: InvestorTransaction }
      ]
    },
    invoice: {
      status: 'PUBLISHED',
      getInvestorProfitEstimate: () => {
        return;
      },
      company_id: companyId,
      Operation: {
        status: 'PENDING'
      }
    },
    fundRequestedInvoice: {
      status: 'FUND_REQUESTED',
      getInvestorProfitEstimateFromTransaction: () => {
        return;
      },
      company_id: companyId,
      Operation: {
        status: 'FUND_REQUESTED',
        InvestorTransaction: {
          status: 'PENDING'
        }
      }
    },
    investorFundEstimate,
    message: 'Get investor profit estimate',
    guid,
    requestType,
    responseType,
    commonError
  },
  getInvestorInvoices: {
    request: {
      request: {
        guid,
        limit: 2,
        offset: 2,
        user_id: 1
      }
    },
    response: {
      invoices: [ invoice ],
      total_invoices: 4,
      total_pages: 2
    },
    whereUser: {
      where: {
        id: 1
      }
    },
    investor,
    whereInvestor: {
      investor_company_id: companyId
    },
    limit: 2,
    offset: 2,
    message: 'Get investor invoices',
    guid,
    requestType,
    responseType,
    invoices: [ invoice ],
    commonError
  },
  getInvestorFundDetail: {
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1,
        company_id: companyId
      }
    },
    response: operationTerm,
    whereInvoice: {
      where: {
        id: 1,
        investor_company_id: companyId
      }, include: [
        { model: Operation, include: InvestorTransaction }
      ]
    },
    invoice: {
      getInvestorFundDetail: () => {
        return;
      },
      company_id: companyId
    },
    operationTerm,
    message: 'Get investor fund detail',
    guid,
    requestType,
    responseType,
    commonError
  },
  getInvoicePaymentSummary: {
    sendengoStrategy: {
      selectByCompanyId: () => {
        return true;
      }
    },
    request: {
      request: {
        guid,
        user_id: 1,
        invoice_id: 1,
        company_id: companyId
      }
    },
    response: {
      payment_summary: paymentSummary,
      financial_summary: _.merge(operationTerm, investorFundEstimate)
    },
    whereInvoice: {
      where: {
        id: 1,
        company_id: companyId,
        investor_company_id: {
          $ne: null
        }
      }
    },
    invoice: {
      getInvoicePaymentSummary: () => {
        return;
      },
      getInvestorFundDetail: () => {
        return;
      },
      getInvestorProfitEstimateFromTransaction: () => {
        return;
      },
      company_id: companyId
    },
    operationTerm,
    investorFundEstimate,
    paymentSummary,
    message: 'Get invoice payment summary',
    guid,
    requestType,
    responseType,
    commonError
  },
  getAdminInvoices: {
    allParamsRequest: {
      request: {
        guid,
        user_id: 1,
        limit: 2,
        offset: 2,
        status: [ 'approved', 'published' ],
        start_fund_date: 'start_fund_date',
        end_fund_date: 'end_fund_date',
        start_expiration_date: 'start_expiration_date',
        end_expiration_date: 'end_expiration_date',
        client_name: 'client',
        investor_name: 'investor',
        company_name: 'company',
        order_by: 'client_name',
        order_desc: true
      }
    },
    noPaginationParamsRequest: {
      request: {
        guid,
        user_id: 1,
        status: [ 'approved', 'published' ],
        start_fund_date: 'start_fund_date',
        end_fund_date: 'end_fund_date',
        start_expiration_date: 'start_expiration_date',
        end_expiration_date: 'end_expiration_date',
        client_name: 'client',
        investor_name: 'investor',
        company_name: 'company',
        order_by: 'client_name',
        order_desc: true
      }
    },
    emptyStatusRequest: {
      request: {
        guid,
        user_id: 1,
        limit: 2,
        offset: 2,
        status: [],
        start_fund_date: 'start_fund_date',
        end_fund_date: 'end_fund_date',
        start_expiration_date: 'start_expiration_date',
        end_expiration_date: 'end_expiration_date',
        client_name: 'client',
        investor_name: 'investor',
        company_name: 'company',
        order_by: 'client_name',
        order_desc: true
      }
    },
    adminInvoices: {
      invoices: [ invoice ],
      count: 4
    },
    response: {
      invoices: [ invoice ],
      total_invoices: 4,
      total_pages: 2
    },
    responseDefaultPagination: {
      invoices: [ invoice ],
      total_invoices: 4,
      total_pages: 1
    },
    message: 'Get admin invoices',
    allParams: {
      guid,
      user_id: 1,
      limit: 2,
      offset: 2,
      status: [ 'approved', 'published' ],
      start_fund_date: 'start_fund_date',
      end_fund_date: 'end_fund_date',
      start_expiration_date: 'start_expiration_date',
      end_expiration_date: 'end_expiration_date',
      client_name: 'client',
      investor_name: 'investor',
      company_name: 'company',
      order_by: 'client_name',
      order_desc: true
    },
    noPaginationParams: {
      guid,
      user_id: 1,
      limit: 25,
      status: [ 'approved', 'published' ],
      start_fund_date: 'start_fund_date',
      end_fund_date: 'end_fund_date',
      start_expiration_date: 'start_expiration_date',
      end_expiration_date: 'end_expiration_date',
      client_name: 'client',
      investor_name: 'investor',
      company_name: 'company',
      order_by: 'client_name',
      order_desc: true
    },
    nullStatusParams: {
      guid,
      user_id: 1,
      limit: 2,
      offset: 2,
      status: null,
      start_fund_date: 'start_fund_date',
      end_fund_date: 'end_fund_date',
      start_expiration_date: 'start_expiration_date',
      end_expiration_date: 'end_expiration_date',
      client_name: 'client',
      investor_name: 'investor',
      company_name: 'company',
      order_by: 'client_name',
      order_desc: true
    },
    guid,
    requestType,
    responseType,
    commonError
  },

  bulkDelete: {
    request: {
      request: {
        guid,
        user_id: 1,
        company_id: 2,
        invoices: invoicesIdsRequest
      }
    },
    activityData: { user_id },
    options: { sequelizeTransaction: transaction },
    user_id: 1,
    guid,
    notFound,
    message: 'Bulk delete invoices',
    requestType,
    response: {
      invoices: invoicesIdsRequest
    },
    responseType,
    invoices: invoicesBulkDelete,
    transaction,
    findParams: {
      where: {
        company_id: 2,
        id: {
          $in: [ 1, 2, 3 ]
        }
      }
    },
    destroyParams: {
      where: {
        company_id: 2,
        id: {
          $in: [ 1, 2, 3 ]
        }
      },
      paranoid: true,
      transaction
    }
  },
  getInvoiceDetailAsAdmin: {
    request: {
      request: {
        guid,
        invoice_id: 1
      }
    },
    response: {
      invoice_detail: adminInvoiceDetail.invoiceDetail,
      operation_summary: adminInvoiceDetail.operationSummary,
      cxc_payment: adminInvoiceDetail.cxcPayment,
      investor_payment: adminInvoiceDetail.investorPayment
    },
    incompleteResponse: {
      invoice_detail: adminInvoiceDetail.invoiceDetail
    },
    whereInvoice: {
      where: {
        id: 1
      },
      include: [
        { model: Company, as: 'Company', include: OperationCost },
        { model: Company, as: 'Client', include: OperationCost },
        { model: Company, as: 'Investor', include: OperationCost },
        { model: Operation, include: InvestorTransaction }
      ]
    },
    adminInvoiceDetail,
    invoice: {
      getAdminBasicInfo: () => {
        return;
      },
      getAdminOperationSummary: () => {
        return;
      },
      getAdminCxcPayment: () => {
        return;
      },
      getAdminInvestorPayment: () => {
        return;
      },
      company_id: companyId,
      expiration: today,
      fund_date: today,
      Operation: {
        annual_cost_percentage: 10,
        reserve_percentage: 10,
        fd_commission_percentage: 10
      }
    },
    message: 'Get invoice detail as admin',
    guid,
    requestType,
    responseType,
    commonError
  },
  getInvoicesAvailability: {
    request: {
      request: {
        guid,
        invoices: availabilityInvoicesRequest
      }
    },
    invalidRequest: {
      request: {
        guid,
        invoices: []
      }
    },
    response: availabilityInvoices,
    emptyArrayError: {
      errors: [ {
        path: 'Invoice',
        message: 'Invoices array cannot be empty'
      } ]
    },
    query: {
      attributes: [
        'id',
        [ sequelize.literal(`CASE WHEN "Operation"."status" IS NOT NULL
           AND "Operation"."status" LIKE '%PENDING%' THEN true ELSE false END`), 'is_available' ]
      ],
      where: {
        id: {
          $in: invoicesAvailability
        },
        status: 'PUBLISHED'
      },
      include: [
        { attributes: [], model: Operation, required: true }
      ]
    },
    message: 'Get invoices availability',
    guid,
    requestType,
    responseType,
    invoices: availabilityInvoiceInstances,
    commonError
  },
  bulkApprove: {
    request: {
      request:
      { guid,
        pending_transactions,
        bank_report_id }
    },
    invalidRequest: {
      request: {
        guid
      }
    },
    response: bulkAppoveResponse,
    queryParams: {},
    emptyArrayError: {
      errors: [ {
        path: 'PendingTransaction',
        message: 'Not found'
      } ]
    },
    pendingTransactions,
    emptyPendingTransactions: [],
    ids: [ 1, 2 ],
    query: {},
    buildQueryParams: {
      bank_report_id,
      skip_where_created_at: true,
      by_id: { $in: [ 1, 2 ] }
    },
    message: 'Bulk approve',
    guid,
    requestType,
    responseType,
    invoice: invoiceBulkCompleted,
    cxcPayment,
    invoiceFindOneQuery,
    investorPayment,
    callbackDataOne,
    callbackDataTwo,
    callbackApprove2,
    callbackApprove1,
    requestControllerCompleted,
    arrayCallbacks,
    commonError
  },
  _buildCallbackData: {
    request: {},
    pendingTransaction: {
      id: 1
    },
    pendingTransactionOut: {
      id: 1
    }
  },
  _processCallbackResults: {
    callbacks: [
      { processed: true, pending_transaction: { id: 1 } },
      { processed: false, pending_transaction: { id: 2 } }
    ],
    response: {
      success: [ { id: 1 } ],
      fail: [ { id: 2 } ]
    }
  }
};
