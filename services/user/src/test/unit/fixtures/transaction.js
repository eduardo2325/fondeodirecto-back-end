const { commonError } = require('../../common/fixtures/errors');

const companyId = 1;
const uuid = 'ASDZXCQWE';
const transaction = {
  type: 'DEPOSIT',
  amount: 10000
};
const today = new Date();
const transactionSequelize = {
  rollback: () => true
};

module.exports = {
  formatAndCreate: {
    commonError,
    createParams: [ {
      type: transaction.type,
      amount: transaction.amount,
      originated_from_id: companyId,
      destinated_to_id: companyId,
      invoice_id: undefined,
      is_payment: undefined
    }, { transaction: undefined } ],
    createParamsWithUuid: [ {
      type: transaction.type,
      amount: transaction.amount,
      originated_from_id: companyId,
      destinated_to_id: companyId,
      invoice_id: uuid,
      is_payment: undefined
    }, { transaction: undefined } ],
    createParamsWithPayment: [ {
      type: transaction.type,
      amount: transaction.amount,
      originated_from_id: companyId,
      destinated_to_id: companyId,
      invoice_id: uuid,
      is_payment: true
    }, { transaction: undefined } ],
    createParamsWithCreatedAt: [ {
      type: transaction.type,
      amount: transaction.amount,
      originated_from_id: companyId,
      destinated_to_id: companyId,
      invoice_id: uuid,
      is_payment: true,
      created_at: today
    }, { transaction: undefined } ],
    senderCompanyId: companyId,
    receiverCompanyId: companyId,
    invoice_id: uuid,
    created_at: today,
    transaction
  },
  createDeposit: {
    commonError,
    createParams: [ {
      type: 'DEPOSIT',
      amount: '300',
      originated_from_id: companyId,
      destinated_to_id: companyId,
      invoice_id: 1,
      is_payment: true
    }, { transaction: transactionSequelize } ],
    amount: '300',
    originatedFrom: companyId,
    destinatedTo: companyId,
    invoiceId: 1,
    createdAt: today,
    isPayment: true,
    transaction: transactionSequelize
  }
};
