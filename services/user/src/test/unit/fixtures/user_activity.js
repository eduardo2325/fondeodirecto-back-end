const _ = require('lodash');
const { commonError } = require('../../common/fixtures/errors');
const { sequelize } = require('../../../models');

const invoices = [ { id: 1 }, { id: 2 }, { id: 3 } ];
const transaction = {};
const options = {
  sequelizeTransaction: transaction
};

const activity = { id: 2 };
const activity_id = 2;
const _ids = _.map(invoices, _i => [ activity_id, _i.id ] );
const data = _.flatten(_ids);
const _valuesString = _.reduce(invoices, (accumulator, value, index) => {
  return accumulator + '(?, ?)' + (index === invoices.length - 1 ? '' : ',');
}, '');
const rawQuery = `INSERT INTO invoice_activity (activity_id, invoice_id) VALUES ${_valuesString};`;
const rawQueryOptions = { replacements: data, type: sequelize.QueryTypes.INSERT, transaction };


module.exports = {
  registerDeleteInvoices: {
    rawQuery,
    commonError,
    activityData: {
      user_id: 1
    },
    activity,
    createParamsData: [ {
      triggered_by: 1,
      action: 'DELETE_INVOICE'
    }, { transaction: {} } ],
    createParamsOptions: { transaction: {} },
    options,
    invoices,
    rawQueryOptions
  }
};
