const { commonError } = require('../../../common/fixtures/errors');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';

const invalidHour = '21:00:00';
const validHour = '21:39:00';

const invoice = {
  id: 2,
  number: 'A2',
  status: 'PUBLISHED',
  save: () => {
    return;
  },
  Operation: {
    status: 'PENDING',
    save: () => {
      return;
    }
  },
  getOperation: () => {
    return;
  }
};

module.exports = {
  randomizeBehavior: {
    guid,
    commonError,
    invoices: [ invoice ],
    invalidHour,
    validHour,
    startedLogParams: [
      'Newest invoices monitor job started',
      {},
      'Scheduler',
      guid
    ],
    finishedLogParams: [
      'Newest invoices monitor job finished',
      {},
      'Scheduler',
      guid
    ],
    failedLogParams: [
      commonError,
      guid,
      {
        message: 'Newest invoies monitor job failed'
      }
    ]
  }
};
