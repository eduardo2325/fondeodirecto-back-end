const { commonError } = require('../../../common/fixtures/errors');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const invoices = [
  {
    id: 1,
    number: 'A1',
    status: 'FUNDED',
    save: () => {
      return;
    },
    Company: {
      name: 'B1'
    },
    Operation: {
      status: 'PENDING',
      save: () => {
        return;
      }
    }
  },
  {
    id: 2,
    number: 'A2',
    status: 'FUNDED',
    save: () => {
      return;
    },
    Company: {
      name: 'B2'
    },
    Operation: {
      status: 'PENDING',
      save: () => {
        return;
      }
    }
  }
];
const invoiceIds = [ 1, 2 ];

module.exports = {
  paymentDueInvoicesMonitor: {
    guid,
    commonError,
    invoices,
    operationStatus: 'PAYMENT_DUE',
    invoiceStatus: 'FUNDED',
    transaction: {
      id: 100
    },
    startedLogParams: [
      'Invoice payment due monitor job started',
      {},
      'Scheduler',
      guid
    ],
    finishedLogParams: [
      'Invoice payment due monitor job finished',
      { paymentDueInvoicesIds: invoiceIds },
      'Scheduler',
      guid
    ],
    failedLogParams: [
      commonError,
      guid,
      {
        message: 'Invoice payment due monitor job failed'
      }
    ]
  }
};
