const { commonError } = require('../../../common/fixtures/errors');
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const invoices = [
  {
    id: 1,
    number: 'A1',
    status: 'APPROVED',
    save: () => {
      return;
    },
    Operation: {
      status: 'PENDING',
      save: () => {
        return;
      }
    },
    getOperation: () => {
      return;
    }
  },
  {
    id: 2,
    number: 'A2',
    status: 'PUBLISHED',
    save: () => {
      return;
    },
    Operation: {
      status: 'PENDING',
      save: () => {
        return;
      }
    },
    getOperation: () => {
      return;
    }
  }
];
const invoiceIds = [ 1, 2 ];
const invoiceError = [ {
  path: 'Invoice',
  message: 'No operation found'
} ];

module.exports = {
  pendingTransactionRejected: {
    guid,
    commonError,
    invoices,
    getInvoicesParams: [ 250, 'physical' ],
    operationStatus: 'EXPIRED',
    invoiceStatus: 'COMPLETED',
    startedLogParams: [
      'Invoice expiration monitor job started',
      {},
      'Scheduler',
      guid
    ],
    finishedLogParams: [
      'Invoice expiration monitor job finished',
      { expiredInvoiceIds: invoiceIds },
      'Scheduler',
      guid
    ],
    failedLogParamsInvoice: [
      invoiceError,
      guid,
      {
        message: 'Invoice expiration monitor job failed'
      }
    ],
    failedLogParams: [
      commonError,
      guid,
      {
        message: 'Invoice expiration monitor job failed'
      }
    ],
    transaction: {
      id: 100
    }
  }
};
