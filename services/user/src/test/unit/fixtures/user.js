const userRoles = require('../../../config/user-roles');
const colors = require('../../../vendor/colors');
const { commonError } = require('../../common/fixtures/errors');

const id = 1;
const email = 'admin@email.com';
const name = 'Fondeo Directo';
const password = '123456';
const encryptedPassword = 'asdfqwer1234';
const role = userRoles.publicRoles[0].value;
const guid = 'a62ca90f-854e-4227-aa28-81487d94c4f4';
const color = colors[0];
const { Company } = require('../../../models');

const cleanUser = {
  id,
  name,
  email,
  role,
  color
};
const sequelizeUser = {
  id,
  name,
  email,
  role,
  color,
  password,
  getInformation: () => {
    return {
      id,
      name,
      email,
      role,
      color
    };
  },
  save: function() {
    return this;
  }
};
const user = {
  id,
  name,
  email,
  role,
  color,
  company: {
    rfc: 'CUPU800825569',
    role: 'admin'
  }
};

const userModel = {
  id,
  name,
  email,
  role,
  Company: {
    rfc: 'CUPU800825569',
    role: 'admin'
  }
};

module.exports = {
  createUser: {
    request: {
      request: {
        email: 'email'
      }
    },
    user
  },
  getUserByEmail: {
    request: {
      request: {
        guid,
        email,
        password
      }
    },
    response: {
      id,
      name,
      email,
      company: user.company
    },
    user,
    userModel,
    message: 'Get user by credentials',
    typeRequest: 'request',
    typeResponse: 'response',
    guid
  },
  getUserById: {
    request: {
      request: {
        id: 1
      }
    },
    user
  },
  verify: {
    request: {
      email,
      password
    },
    context: {
      findOne: () => {
        return;
      }
    },
    userModel,
    findOneCall: {
      where: {
        email
      },
      include: [
        { model: Company }
      ]
    }
  },
  beforeCreate: {
    instance: {
      password
    },
    saltRounds: 10,
    password
  },
  beforeValidate: {
    instance: {
      role: 'cxc'
    }
  },
  getInformation: {
    instance: {
      get: () => {
        return user;
      }
    },
    fullInformation: {
      id,
      name,
      email,
      color,
      role,
      status: 'active'
    }
  },
  updatePassword: {
    request: {
      user_id: id,
      actual_password: password,
      new_password: 'new super secret password'
    },
    query: [ {
      where: {
        id
      }
    } ],
    user: sequelizeUser,
    password,
    cleanUser,
    encryptedPassword,
    commonError,
    invalidPasswordError: {
      errors: [ {
        path: 'actual_password',
        message: 'actual_password is incorrect'
      } ]
    }
  },
  encryptPassword: {
    password,
    commonError
  },
  resetPassword: {
    request: {
      email,
      password
    },
    query: [ {
      where: {
        email
      }
    } ],
    user: sequelizeUser,
    password,
    cleanUser,
    encryptedPassword,
    commonError
  }
};
