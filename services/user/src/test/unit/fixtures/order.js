const { commonError } = require('../../common/fixtures/errors');

const investor = {
  id: 10,
  name: 'investorUser',
  email: 'investor@fondeodirecto.com'
};

const invoice = {
  id: 10053,
  is_available: true
};

const order = {
  id: 1
};
const orderInvoice = {
  order_id: 1,
  invoice_id: 10053
};

module.exports = {
  newFundOrder: {
    invoices: [ invoice ],
    investor,
    order,
    orderInvoice,
    orderCreateParams: [
      {
        investor_company_id: 10,
        type: 'FUND'
      }
    ],
    orderInvoiceCreateParams: [
      {
        order_id: 1,
        invoice_id: 10053
      }
    ],
    commonError
  }
};
