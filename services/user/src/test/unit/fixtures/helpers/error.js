module.exports = {
  format: {
    err: {
      errors: [ {
        path: 'field',
        message: 'error message',
        data: {}
      } ]
    },
    errMessage: new Error('error message'),
    errorFormatted: JSON.stringify({
      path: 'Error',
      message: 'error message'
    }),
    guid: 'a62ca90f-854e-4227-aa28-81487d94c4f4'
  }
};
