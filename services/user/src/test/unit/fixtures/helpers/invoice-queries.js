module.exports = {
  daysDiffQuery: {
    startDateColumn: 'CURRENT_DATE',
    endDateColumn: 'expiration',
    query: "(DATE_PART('day', date_trunc('day', expiration) - date_trunc('day', CURRENT_DATE)))",
    result: 'query'
  },
  gainEstimateQuery: {
    fundDateColumn: 'CURRENT_DATE',
    gainEstimateQueryString: 'gainEstimateQuery',
    query: 'gainEstimateQuery',
    result: 'query'
  },
  gainEstimatePercentageQuery: {
    fundDateColumn: 'CURRENT_DATE',
    gainEstimateResult: 'gainestimatequery',
    // Indentation has to be this way to pass the match assert
    query: `
    round(
      CAST(
        float8 (gainestimatequery / total * 100) as numeric
      ),
      2
    )
  `,
    result: 'query'
  }
};
