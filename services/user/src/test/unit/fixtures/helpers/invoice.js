/* eslint-disable */
const fixedToday = new Date('2017-06-14');
const startDate = new Date('2017-06-05');
const fundDate = new Date('2017-06-10');
const endDate = new Date('2017-06-15');
const oneDay = 24 * 60 * 60 * 1000;

const fixedNow = new Date();

fixedNow.setHours(0, 0, 0);
fixedNow.setMilliseconds(0);

const taxpayerType = 'physical';

const operationData = {
  fd_commission_percentage: 0.6,
  reserve_percentage: 5,
  annual_cost_percentage: 15
}

const operationCostData = {
  fd_commission: 0.6,
  reserve: 5,
  annual_cost: 15
}

const invoiceData = {
  total: 100000,
  is_available: undefined
};

const invoiceWithOperationData = {
  ...invoiceData,
  Operation: {
    ...operationData
  }
};

const investorInvoiceData = {
  total: 100000,
  Operation: {
    fd_commission_percentage: 1000,
    annual_cost_percentage: 15,
    reserve_percentage: 2500,
  }
};

const investorPercentages = {
  fee: 350,
  variable_fee_percentage: 10
};

const investorFideicomisoPercentages = {
  fee: 350,
  variable_fee_percentage: 10,
  fideicomiso_fee: 150,
  fideicomiso_variable_fee: 3
};

const globalFeePercentage = '1/3';

const investorInvoiceDataWithoutVariableFee = {
  fd_commission: 1000,
  annual_cost: 15,
  reserve: 2500,
  total: 100000,
  fee: 350,
  variable_fee_percentage: null,
  global_fee_percentage: null
};

const investorInvoiceDataWithoutAnyFee = {
  fd_commission: 1000,
  annual_cost: 15,
  reserve: 2500,
  total: 100000,
  fee: null,
  variable_fee_percentage: null,
  global_fee_percentage: null
};

module.exports = {
  percentages: investorPercentages,
  global_fee_percentage: globalFeePercentage,
  invoice: {
    ...invoiceData,
    fund_date: fundDate,
    expiration: endDate
  },
  invoiceWithOperation: {
    ...invoiceWithOperationData,
    fund_date: fundDate,
    expiration: endDate
  },
  instance: {
    invoice: {
      ...invoiceData
    },
    operation: {
      ...operationData
    },
    oneDay,
    today: fixedNow,
    fundDay: fundDate,
    expiration: endDate
  },
  investorInvoice: {
    ...investorInvoiceData,
    fund_date: fundDate,
    expiration: endDate,
  },
  shoppingCartInvoice: {
    ...investorInvoiceData,
    expiration: endDate,
    dataValues: {
      is_available: true
    }
  },
  investorInvoiceWithoutVariableFee: {
    ...investorInvoiceDataWithoutVariableFee,
    fund_date: fundDate,
    expiration: endDate
  },
  investorInvoiceWithoutAnyFee: {
    ...investorInvoiceDataWithoutAnyFee,
    fund_date: fundDate,
    expiration: endDate
  },
  newInvoice: {
    input: {
      total: 350000.00, // C5
      fund_date: '16-May-2018', // C2
      expiration: '31-Jul-2018', // C3
      ...invoiceWithOperationData
    },
    result: {
      commission: 126.67, // C12 = C10 * F15 * (C4 / 360)
      diffDays: 76, // C4 = (C3 - C2) + 1
      expirationPayment: 5000,
      fundPayment: 91706.66, // C15 = C13 - C14
      fundTotal: 96706.66, // C13 = C10 - C11 - C12
      interest: 3166.67, // C11 = C10 * (I12 * C4 / 360)
      interestPayment: 3293.34, // I11 = C11 + C12
      interestPercentage: 3.29, // I10 = I11 / C10
      reserve: 5000, // C14 = C10 * F14
      total: 100000 // C10 = C5
    }
  },
  paymentEstimates: {
    n1: 94956.66,
    n10: 94566.66,
    n100: 90666.66
  },
  daysDiffCalculation: {
    operationCost: {
      ...invoiceData
    },
    percentages: {
      ...operationCostData
    },
    today: fixedToday,
    daysDiff: 1,
    daysDiffFund: 5
  },
  operationCalculation: {
    diffDays: 4,
    operationCost: 173.33
  },
  interestCalculation: {
    invoiceTotal: 97000,
    annualCost: 15,
    invoiceDuration: 60,
    estimateInterest: 2500,
    interest: 242500
  },
  fdComissionCalculation: {
    invoiceTotal: 97000,
    fdComissionPercentage: 6,
    invoiceDuration: 60,
    comission: 97000
  },
  gainCalculation: {
    invoiceTotal: 97000,
    annualCost: 15,
    invoiceDuration: 60,
    gain: 242500
  },
  earningsCalculation: {
    invoiceTotal: 97000,
    annualCost: 15,
    invoiceDuration: 60,
    earnings: 242500
  },
  investorFeeNoVariableFeeCase: {
    earnings: 1000,
    fee: 350
  },
  investorNoFeeOrVariableFee: {
    earnings: 1000,
    fee: 0
  },
  investorFeeFirstCase: {
    earnings: 1000,
    fee: 100
  },
  investorFeeSecondCase: {
    earnings: 2890,
    fee: 350
  },
  investorFeeThirdCase: {
    earnings: 5667.01,
    fee: 566.701
  },
  investorTransactionEstimate: {
    earnings: 125,
    earningsPercentage: 0.125,
    feeType: 'Variable fee',
    isr: 0,
    isrTotalPercent: 0,
    netIncome: 112.5,
    netIncomePercentage: 0.1125,
    perception: 100112.5,
    variableFee: 12.5,
    fideicomisoFee: undefined,
    fideicomisoFeeType: undefined
  },
  fideicomisoTransactionEstimate: {
    earnings: 125,
    earningsPercentage: 0.125,
    feeType: 'Variable fee',
    isr: 0,
    isrTotalPercent: 0,
    netIncome: 108.75,
    netIncomePercentage: 0.10875,
    perception: 100108.75,
    variableFee: 12.5,
    fideicomisoFee: 3.75,
    fideicomisoFeeType: 'Variable fee'
  },
  results: {
    fundEstimates: {
      variableFee: 0,
      total: 100000,
      annualCostPercentage: 0.15,
      annualCost: 15000,
      reservePercentage: 0.05,
      fdCommissionPercentage: 0.006,
      fdCommission: 600,
      factorable: 95000,
      subtotal: 94875,
      interest: 125,
      commission: 5,
      fundTotal: 99870,
      reserve: 5000,
      fundPayment: 94870,
      expirationPayment: 5000,
      operationCost: 130,
    },
    investorProfitEstimates: {
      gain: 125,
      gain_percentage: 0.125
    },
    investorGains: {
      withoutTaxpayer: {
        result: {
          isr: 0,
          perception: 112.5
        },
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage,
      },
      withTaxpayer: {
        taxpayerType,
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage,
        result: {
          isr: 25,
          perception: 87.5
        }
      }
    },
    investorFundEstimates: {
      withoutTaxpayer: {
        result: {
          isr: 0,
          include_isr: false,
          perception: 100119,
          earnings: 125,
          earningsPercentage: 0.125,
          variableFee: 12.5,
          fideicomisoFee: undefined,
          netIncome: 112.5,
          netIncomePercentage: 0.1125,
          perception: 100112.5
        },
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage,
      },
      withTaxpayer: {
        taxpayerType,
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage,
        result: {
          isr: 25,
          include_isr: true,
          perception: 100094,
          earnings: 125,
          earningsPercentage: 0.125,
          variableFee: 12.5,
          fideicomisoFee: undefined,
          netIncome: 87.5,
          netIncomePercentage: 0.0875,
          perception: 100087.5
        }
      }
    },
    adminInvestorPaymentEstimates: {
      withoutTaxpayer: {
        result: {
          isr: 0,
          include_isr: false,
          perception: 100112.5,
          fundTotal: 100000,
          earnings: 125,
          earningsPercentage: 0.00125,
          gainPercentage: 0.001125,
          feePercentage: 0.000125,
          variableFee: 12.5,
          fideicomisoFee: undefined,
          gain: 112.5
        },
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage,
      },
      withTaxpayer: {
        taxpayerType,
        result: {
          isr: 25,
          include_isr: true,
          perception: 100087.5,
          fundTotal: 100000,
          earnings: 125,
          earningsPercentage: 0.00125,
          gainPercentage: 0.000875,
          feePercentage: 0.000125,
          gain: 87.5,
          variableFee: 12.5,
          fideicomisoFee: undefined
        },
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage,
      }
    },
    adminCxcPaymentEstimates: {
      notFunded: {
        result: {
          interest: 0,
          interestPercentage: 0,
          reserve: 5000,
          commission: 600,
          total: 100000,
          fundPayment: 0,
          expirationPayment: 5000,
          diffDays: 0,
          fundTotal: 0,
          interestPayment: 0
        },
      },
      funded: {
        result: {
          interest: 208.33,
          interestPercentage: 0.22,
          reserve: 5000,
          commission: 8.33,
          total: 100000,
          fundPayment: 94783.34,
          expirationPayment: 5000,
          diffDays: 5,
          fundTotal: 99783.34,
          interestPayment: 216.66
        }
      }
    },
    adminOperationEstimates: {
      notFunded: {
        result: {
          diffDays: 0,
          commission: 0,
          variableFee: 0,
          totalFee: 0,
          earningsFd: 0
        },
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage
      },
      funded: {
        result: {
          diffDays: 5,
          commission: 8.33,
          variableFee: 20.83,
          earningsFd: 29.16,
          totalFee: 0
        },
        percentages: investorPercentages,
        global_fee_percentage: globalFeePercentage
      },
      fundedByFideicomiso: {
        result: {
          diffDays: 5,
          commission: 8.33,
          variableFee: 20.83,
          earningsFd: 29.16,
          totalFee: 27.08
        },
        percentages: investorFideicomisoPercentages,
        global_fee_percentage: globalFeePercentage
      }
    },
    investorFideicomisoFee: {
      percentages: investorFideicomisoPercentages,
      fixedFee: {
        result: 150,
        earnings: 1854,
        fondeoFee: 350
      },
      variableFee: {
        result: 166.875,
        earnings: 5562.5,
        fondeoFee: 556.25
      }
    },
    getInvoiceAmounts: {
      percentages: investorFideicomisoPercentages,
      result: {
        isAvailable: true,
        total: 100000,
        interest: 2500,
        isr: 0,
        gain: 2000,
        fondeoFee: 500,
        perception: 102000
      }
    }
  }
};
