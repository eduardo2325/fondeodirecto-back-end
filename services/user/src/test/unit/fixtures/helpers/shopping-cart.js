const invoice = function(isAvailable) {
  return {
    isAvailable: isAvailable,
    total: 732.1,
    earnings: 18,
    isr: 0,
    fondeoFee: 1.8,
    perception: 16.2
  };
};

module.exports = {
  getOnlyAvailableInvoices: {
    availableInvoices: {
      request: [
        invoice(true),
        invoice(true)
      ],
      onlyAvailable: [
        {
          isAvailable: true,
          total: 732.1,
          earnings: 18,
          isr: 0,
          fondeoFee: 1.8,
          perception: 16.2
        },
        {
          isAvailable: true,
          total: 732.1,
          earnings: 18,
          isr: 0,
          fondeoFee: 1.8,
          perception: 16.2
        }
      ]
    },
    mixedInvoices: {
      request: [
        invoice(true),
        invoice(false)
      ],
      mixedAvailable: [
        {
          isAvailable: true,
          total: 732.1,
          earnings: 18,
          isr: 0,
          fondeoFee: 1.8,
          perception: 16.2
        }
      ]
    },
    unavailableInvoices: {
      request: [
        invoice(false),
        invoice(false)
      ],
      onlyUnavailable: [
      ]
    }
  },
  getAvailableInvoicesSummary: {
    invoices: [
      {
        id: 10053,
        is_available: true,
        client_name: 'Sureplex',
        total: '433.10',
        term_days: 59,
        interest: '10.65',
        isr: '0',
        fee: '1.07',
        gain: '9.59',
        perception: '442.69'
      },
      {
        id: 10054,
        is_available: true,
        client_name: 'Sureplex',
        total: '732.10',
        term_days: 59,
        interest: '18.00',
        isr: '0',
        fee: '1.80',
        gain: '16.20',
        perception: '748.30'
      } ],
    summary: {
      earnings_total: '1190.98',
      fee_total: '2.87',
      interests_total: '28.65',
      invoices_total: '1165.20',
      isr_total: '0.00',
      perception_total: '1190.99'
    }
  },
  getCartResponse: {
    invoices: [
      {
        id: 10053,
        is_available: true,
        client_name: 'Sureplex',
        total: '433.10',
        term_days: 59,
        interest: '10.65',
        isr: '0',
        fee: '1.07',
        gain: '9.59',
        perception: '442.69'
      },
      {
        id: 10054,
        is_available: true,
        client_name: 'Sureplex',
        total: '732.10',
        term_days: 59,
        interest: '18.00',
        isr: '0',
        fee: '1.80',
        gain: '16.20',
        perception: '748.30'
      } ],
    response: {
      invoices: [
        {
          id: 10053,
          is_available: true,
          client_name: 'Sureplex',
          total: '433.10',
          term_days: 59,
          interest: '10.65',
          isr: '0',
          fee: '1.07',
          gain: '9.59',
          perception: '442.69'
        },
        {
          id: 10054,
          is_available: true,
          client_name: 'Sureplex',
          total: '732.10',
          term_days: 59,
          interest: '18.00',
          isr: '0',
          fee: '1.80',
          gain: '16.20',
          perception: '748.30'
        } ],
      summary: {
        earnings_total: '1190.98',
        fee_total: '2.87',
        interests_total: '28.65',
        invoices_total: '1165.20',
        isr_total: '0.00',
        perception_total: '1190.99'
      },
      total: 2
    }
  },
  getPublishSummaryTotals: {
    invoices: [
      {
        is_available: true,
        client_name: 'Sureplex',
        number: '6381472A-F8671280A1679164',
        total: '150000.00',
        operation_cost: '5747.92',
        reserve: '15000.00',
        fund_payment: '129252.08'
      }
    ],
    response: {
      summary: {
        fund_payment_total: '129252.08',
        invoices_total: '150000.00',
        operation_cost_total: '5747.92',
        reserve_total: '15000.00'
      },
      totalInvoices: 1
    }
  }
};
