module.exports = {
  selectObjectByCompanyId: {
    sendengoId: 12000,
    otherCompanyId: 3,
    result: 1,
    defaultObject: 2
  },
  selectByCompanyId: {
    sendengoId: 12000,
    otherCompanyId: 3,
    result: 1,
    params: {
      sendengo: 'A',
      default: 'B'
    },
    sendengo: 'A',
    defaultValue: 'B'
  },
  execCallback: {
    sendengoId: 12000,
    otherCompanyId: 11000
  }
};
