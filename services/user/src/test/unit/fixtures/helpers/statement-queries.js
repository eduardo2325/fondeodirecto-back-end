const moment = require('/var/lib/core/js/moment');

const id = 1;
const year = 2018;
const month = 9;
const requestedMoment = moment().month(month);

const data = {
  day: 10,
  description: 'Retiro de efectivo',
  operation: 50,
  factoraje: 560,
  cash: 300
};

const formattedData = {
  day: '10',
  description: 'Retiro de efectivo',
  operation: '50',
  factoraje: '560',
  cash: '300'
};

const statementResponse = [
  { day: '3',
    description: 'Compra de factura',
    operation: '150',
    factoraje: '25000',
    cash: '25000' },
  { day: '10',
    description: 'Retiro de efectivo',
    operation: '',
    factoraje: '',
    cash: '333' },
  { day: '10',
    description: 'Depósito de efectivo',
    operation: '',
    factoraje: '',
    cash: '578' },
  { day: '27',
    operation: '150',
    factoraje: '25000',
    description: 'Liquidación de factura',
    cash: '25000' },
  { day: '27',
    operation: '150',
    factoraje: '',
    description: 'Retención ISR - 20%',
    cash: '0' },
  { day: '27',
    operation: '150',
    factoraje: '',
    description: 'Utilidad de operación',
    cash: '834.37' },
  { day: '27',
    operation: '150',
    factoraje: '',
    description: 'Comisión - Fondeo Directo',
    cash: '92.71' } ];

const queriesResult = [
  [ { day: 3,
    description: 'Compra de factura',
    operation: 150,
    factoraje: 25000,
    cash: 25000 } ],
  [ { day: 10,
    description: 'Retiro de efectivo',
    operation: null,
    factoraje: null,
    cash: 333 } ],
  [ { day: 10,
    description: 'Depósito de efectivo',
    operation: null,
    factoraje: null,
    cash: 578 } ],
  [ { day: 27,
    operation: 150,
    total: 25000,
    interest: 927.08,
    earnings: 834.37,
    fondeo_fee: 92.71,
    fideicomiso_fee: null,
    isr: 0 } ] ];

const purchasedInvoices = {
  query: `SELECT EXTRACT(DAY FROM pt.created_at) AS day, 'Compra de factura' AS description,
    it.id AS operation, i.total AS factoraje,
    i.total AS cash
    FROM pending_transactions AS pt
    INNER JOIN invoices i ON i.id = CAST(pt.data->>'invoice_id' AS INT)
    INNER JOIN operations AS op ON op.invoice_id = i.id
    INNER JOIN investor_transactions AS it ON it.operation_id = op.id
    WHERE i.investor_company_id = ${id}
    AND pt.type LIKE '%WITHDRAW%'
    AND pt.status LIKE '%APPROVED%'
    AND EXTRACT(MONTH FROM pt.created_at) = ${requestedMoment.format('M')}
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`,
  result: 'query'
};

const investorCashWithDrawals = {
  query: `SELECT EXTRACT(DAY FROM pt.created_at) AS day, 'Retiro de efectivo' AS description,
    NULL AS operation,
    NULL AS factoraje,
    pt.amount AS cash
    FROM pending_transactions AS pt
    WHERE pt.company_id = ${id}
    AND pt.status LIKE '%APPROVED%'
    AND pt.type LIKE '%WITHDRAW%'
    AND pt.data->>'invoice_id' IS NULL
    AND EXTRACT(MONTH FROM pt.created_at) = ${requestedMoment.format('M')}
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`,
  result: 'query'
};

const investorCashDeposits = {
  query: `SELECT EXTRACT(DAY FROM pt.created_at) AS day, 'Depósito de efectivo' AS description,
    NULL AS operation,
    NULL AS factoraje,
    pt.amount AS cash
    FROM pending_transactions AS pt
    WHERE pt.company_id = ${id}
    AND pt.status = 'APPROVED'
    AND pt.type = 'DEPOSIT'
    AND pt.data->>'invoice_id' IS NULL
    AND EXTRACT(MONTH FROM pt.created_at) = ${requestedMoment.format('M')}
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`,
  result: 'query'
};

const payedInvoices = {
  query: ` SELECT EXTRACT(DAY FROM t.created_at) AS day, it.id AS operation, i.total,
    it.interest, it.earnings, it.fee AS fondeo_fee,
    it.fideicomiso_fee AS fideicomiso_fee,
    it.isr
    FROM transactions AS t
    INNER JOIN invoices AS i ON i.id = t.invoice_id
    INNER JOIN operations AS op ON op.invoice_id = i.id
    INNER JOIN investor_transactions AS it ON it.operation_id = op.id
    WHERE t.destinated_to_id = ${id}
    AND EXTRACT(MONTH FROM t.created_at) = ${requestedMoment.format('M')}
    AND EXTRACT(YEAR FROM t.created_at) = ${year}`,
  result: 'query'
};

const investorCashDepositsFromInvoices = {
  query: `SELECT t.created_at, amount as cash,
      'Suma de balance por factura' AS description 
    from transactions t where
    t.is_payment=false 
    AND t.type='DEPOSIT'
    AND t.destinated_to_id = ${id}
    AND EXTRACT(MONTH FROM t.created_at) = ${ parseInt(requestedMoment.format('M'), 10) }
    AND EXTRACT(YEAR FROM t.created_at) = ${year}`,
  result: 'query'
};
const investorCashWithDrawalsFromInvoices = {
  query: `
    SELECT pt.created_at, EXTRACT(DAY FROM pt.created_at) AS day, 'Resta de balance por factura' AS description,
    pt.funded_amount as cash 
    FROM investor_transactions pt 
    WHERE pt.investor_company_id = ${id} 
    AND pt.status = 'COMPLETED'
    AND EXTRACT(MONTH FROM pt.created_at) = ${ parseInt(requestedMoment.format('M'), 10) }
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`,
  result: 'query'
};

const formatData = {
  result: [ formattedData ],
  data
};

const formStatementResponse = {
  result: statementResponse,
  queriesResult
};

module.exports = {
  id,
  year,
  month,
  purchasedInvoices,
  investorCashWithDrawals,
  investorCashDeposits,
  payedInvoices,
  investorCashWithDrawalsFromInvoices,
  investorCashDepositsFromInvoices,
  formatData,
  formStatementResponse
};
