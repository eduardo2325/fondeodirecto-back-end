const { commonError } = require('../../../common/fixtures/errors');
const pending = 'PENDING';
const approved = 'APPROVED';
const rejected = 'REJECTED';
const canceled = 'CANCELED';
const published = 'PUBLISHED';
const fundRequested = 'FUND_REQUESTED';
const funded = 'FUNDED';
const paymentInProcess = 'PAYMENT_IN_PROCESS';
const completed = 'COMPLETED';
const sixtyDays = new Date();
const today = new Date();
const thirtyDays = new Date();

sixtyDays.setHours(0, 0, 0, 0);
sixtyDays.setDate(sixtyDays.getDate() + 60);
thirtyDays.setHours(0, 0, 0, 0);
thirtyDays.setDate(sixtyDays.getDate() + 30);
today.setHours(0, 0, 0, 0);

const invoice = function(status) {
  return {
    id: 10055,
    status: status,
    expiration: sixtyDays,
    published: today,
    save: function() {
      return this;
    },
    fund_date: sixtyDays,
    investor_company_id: 1,
    fee: 250
  };
};

const operation = function(status) {
  return {
    invoice_id: 10055,
    expiration_date: sixtyDays,
    published_date: today,
    days_limit: 59,
    annual_cost_percentage: 15,
    reserve_percentage: 10,
    fd_commission_percentage: 0.5,
    annual_cost: 125.86,
    reserve: 83.91,
    fd_commission: 4.2,
    factorable: 755.19,
    subtotal: 734.56,
    fund_payment: 733.87,
    fund_request_date: thirtyDays,
    formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
      ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
    user_id: 1,
    status: status,
    save: function() {
      return this;
    }
  };
};

const transaction = function(status) {
  return {
    investor_company_id: 1,
    fund_request_date: sixtyDays,
    days_limit: 5,
    status: status,
    fee: 250,
    save: function() {
      return this;
    }
  };
};

module.exports = {
  validateStatus: {
    instancesWithInvoice: {
      invoice: invoice(pending),
      operation: null,
      investorTransaction: null
    },
    instancesWithOperation: {
      invoice: invoice(published),
      operation: operation(pending),
      investorTransaction: null
    },
    instancesWithInvestorTransaction: {
      invoice: invoice(fundRequested),
      operation: operation(fundRequested),
      investorTransaction: transaction(pending)
    },
    instancesPublished: {
      invoice: invoice(approved),
      operation: null,
      investorTransaction: null
    },
    baseStatus: {
      invoiceStatus: null,
      operationStatus: null,
      investorTransactionStatus: null
    },
    invoice: invoice(pending),
    pending,
    approved,
    published,
    rejected,
    fundRequested,
    funded,
    paymentInProcess,
    completed,
    invalidError: {
      errors: [ {
        path: 'Invoice',
        message: 'invalid state'
      } ]
    },
    invalidInvoiceError: {
      errors: [ {
        path: 'Invoice',
        message: 'invalid invoice state'
      } ]
    },
    invalidOperationError: {
      errors: [ {
        path: 'Invoice',
        message: 'invalid operation state'
      } ]
    },
    invalidInvestorTransactionError: {
      errors: [ {
        path: 'Invoice',
        message: 'invalid investor transaction state'
      } ]
    }
  },
  next: {
    instancesWithInvoice: {
      invoice: invoice(pending),
      operation: null,
      investorTransaction: null
    },
    instancesWithOperation: {
      invoice: null,
      operation: operation(pending),
      investorTransaction: null
    },
    instancesWithTransaction: {
      invoice: null,
      operation: null,
      investorTransaction: transaction(pending)
    },
    baseStatus: {
      invoiceStatus: null,
      operationStatus: null,
      investorTransactionStatus: null
    },
    invoice: invoice(pending),
    operation: operation(pending),
    investorTransaction: transaction(pending),
    approved,
    fundRequested,
    completed,
    commonError
  },
  back: {
    instancesWithInvoice: {
      invoice: invoice(pending),
      operation: null,
      investorTransaction: null
    },
    instancesWithOperation: {
      invoice: null,
      operation: operation(fundRequested),
      investorTransaction: null
    },
    instancesWithTransaction: {
      invoice: null,
      operation: null,
      investorTransaction: transaction(pending)
    },
    baseStatus: {
      invoiceStatus: null,
      operationStatus: null,
      investorTransactionStatus: null
    },
    invoice: invoice(pending),
    operation: operation(pending),
    investorTransaction: transaction(pending),
    pending,
    canceled,
    rejected,
    commonError
  }
};
