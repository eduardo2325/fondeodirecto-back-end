const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/transaction');

const TransactionModel = require('../../../api/models/Transaction');
const optionsModel = TransactionModel.options;

const { Transaction } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Transaction model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('formatAndCreate', () => {
    const fixtures = helperFixtures.formatAndCreate;
    const { classMethods } = optionsModel;
    const {
      commonError, createParams, transaction, senderCompanyId, createParamsWithCreatedAt,
      receiverCompanyId, invoice_id, createParamsWithUuid, createParamsWithPayment, created_at
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(Transaction, 'create').callsFake(() => Promise.resolve());
    });

    it('should return an error if Transaction.create returns an error', () => {
      Transaction.create.restore();
      sandbox.stub(Transaction, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.formatAndCreate(transaction, senderCompanyId, receiverCompanyId)
        .should.be.rejected
        .then((error) => {
          Transaction.create.args[0].should.be.eql(createParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue creating the record(simple)', () => {
      return classMethods.formatAndCreate(transaction, senderCompanyId, receiverCompanyId)
        .should.be.fulfilled
        .then(() => {
          Transaction.create.args[0].should.be.eql(createParams);
        });
    });

    it('should return a success if there was no issue creating the record(uuid)', () => {
      return classMethods.formatAndCreate(transaction, senderCompanyId, receiverCompanyId, invoice_id)
        .should.be.fulfilled
        .then(() => {
          Transaction.create.args[0].should.be.eql(createParamsWithUuid);
        });
    });

    it('should return a success if there was no issue creating the record(payment)', () => {
      return classMethods.formatAndCreate(transaction, senderCompanyId, receiverCompanyId, invoice_id, true)
        .should.be.fulfilled
        .then(() => {
          Transaction.create.args[0].should.be.eql(createParamsWithPayment);
        });
    });

    it('should return a success if there was no issue creating the record(created_at)', () => {
      return classMethods.formatAndCreate(
        transaction, senderCompanyId, receiverCompanyId, invoice_id, true, undefined, created_at
      )
        .should.be.fulfilled
        .then(() => {
          Transaction.create.args[0].should.be.eql(createParamsWithCreatedAt);
        });
    });
  });

  describe('createDeposit', () => {
    const fixtures = helperFixtures.createDeposit;
    const { classMethods } = optionsModel;
    const { commonError, createParams, amount, originatedFrom, destinatedTo, invoiceId,
      createdAt, isPayment, transaction } = fixtures;

    beforeEach(() => {
      sandbox.stub(Transaction, 'create').callsFake(() => Promise.resolve());
    });

    it('should return an error if PendingTransaction.create returns an error', () => {
      Transaction.create.restore();
      sandbox.stub(Transaction, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.createDeposit(amount, originatedFrom, destinatedTo, invoiceId, createdAt,
        isPayment, transaction)
        .should.be.rejected
        .then((error) => {
          Transaction.create.args[0].should.be.eql(createParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue creating the record(simple)', () => {
      return classMethods.createDeposit(amount, originatedFrom, destinatedTo, invoiceId, createdAt,
        isPayment, transaction)
        .should.be.fulfilled
        .then(() => {
          Transaction.create.args[0].should.be.eql(createParams);
        });
    });
  });
});
