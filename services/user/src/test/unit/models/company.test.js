const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/company');
const statementQueries = require('../../../api/helpers/statement-queries');

const CompanyModel = require('../../../api/models/Company');
const optionsModel = CompanyModel.options;
const attributesModel = CompanyModel.attributes;

const companyColors = require('../../../vendor/colors');
const { Company, BankCode, Token, PendingTransaction, sequelize } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Company model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('beforeCreate', () => {
    const fixtures = helperFixtures.beforeCreate;
    const { hooks } = optionsModel;
    const { instance, bankInfo, clabe, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.resolve(bankInfo));
    });

    it('should return an error if there was an issue analyzing the clabe ', () => {
      Company.analyzeClabe.restore();
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.reject(commonError));

      return hooks.beforeCreate(instance)
        .should.be.rejected
        .then(() => {
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          assert.isDefined(instance.color);
          companyColors.should.include(instance.color);
        });
    });

    it('should return a succesful response', () => {
      // ensure the RFC gets updated through:
      sandbox.stub(instance, 'setDataValue').callsFake((key, value) => {
        instance[key] = value;
      });

      return hooks.beforeCreate(instance)
        .should.be.fulfilled
        .then(() => {
          instance.rfc.should.be.equal('XMPL12345678');
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          assert.isDefined(instance.color);
          companyColors.should.include(instance.color);
        });
    });
  });

  describe('beforeUpdate', () => {
    const fixtures = helperFixtures.beforeUpdate;
    const { hooks } = optionsModel;
    const { instance, bankInfo, clabe, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.resolve(bankInfo));
    });

    it('should return an error if there was an issue analyzing the clabe ', () => {
      Company.analyzeClabe.restore();
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.reject(commonError));

      return hooks.beforeUpdate(instance)
        .should.be.rejected
        .then(() => {
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          assert.isUndefined(instance.color);
        });
    });

    it('should return a succesful response', () => {
      // ensure the RFC gets updated through:
      sandbox.stub(instance, 'setDataValue').callsFake((key, value) => {
        instance[key] = value;
      });

      return hooks.beforeUpdate(instance)
        .should.be.fulfilled
        .then(() => {
          instance.rfc.should.be.equal('XMPL12345678');
          Company.analyzeClabe.calledOnce.should.be.true;
          Company.analyzeClabe.calledWithMatch(clabe).should.be.true;
          assert.isUndefined(instance.color);
        });
    });
  });

  describe('getInformation', () => {
    const fixtures = helperFixtures.getInformation;
    const { instanceMethods } = optionsModel;
    const { instance, bankInfo, fullInformation } = fixtures;

    beforeEach(() => {
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.resolve(bankInfo));
    });

    it('should return a successful response', () => {
      const method = instanceMethods.getInformation.bind(instance);

      return method().should.be.fulfilled
        .then((response) => {
          response.should.be.deep.equal(fullInformation);
        });
    });
  });

  describe('analyzeClabe', () => {
    const fixtures = helperFixtures.analyzeClabe;
    const { classMethods } = optionsModel;
    const { bankInfo, clabe, incorrectClabe, invalidFormat, invalidClabe, query, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(BankCode, 'findOne').callsFake(() => Promise.resolve(bankInfo));
    });

    it('should return an error if clabe doesn\'t have the correct format', () => {
      return classMethods.analyzeClabe(incorrectClabe)
        .should.be.rejected
        .then((error) => {
          BankCode.findOne.called.should.be.false;
          assert.deepEqual(error, invalidFormat);
        });
    });

    it('should return an error if BankCode.findOne has an error', () => {
      BankCode.findOne.restore();
      sandbox.stub(BankCode, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.analyzeClabe(clabe)
        .should.be.rejected
        .then((error) => {
          BankCode.findOne.calledOnce.should.be.true;
          BankCode.findOne.calledWithMatch(query).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if clabe doesn\'t belongs to a bank', () => {
      BankCode.findOne.restore();
      sandbox.stub(BankCode, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.analyzeClabe(clabe)
        .should.be.rejected
        .then((error) => {
          BankCode.findOne.calledOnce.should.be.true;
          BankCode.findOne.calledWithMatch(query).should.be.true;
          assert.deepEqual(error, invalidClabe);
        });
    });

    it('should return a successful response', () => {
      return classMethods.analyzeClabe(clabe)
        .should.be.fulfilled
        .then((information) => {
          BankCode.findOne.calledOnce.should.be.true;
          BankCode.findOne.calledWithMatch(query).should.be.true;
          assert.deepEqual(information, bankInfo);
        });
    });
  });

  describe('getCompanies', () => {
    const fixtures = helperFixtures.getCompaniesModel;
    const { classMethods } = optionsModel;
    const { findAll, findAllWithAttributes, companies, bankInfo, companyElement,
      commonError, limit, offset, order_by, order_desc, role } = fixtures;

    beforeEach(() => {
      sandbox.stub(Company, 'findAll').callsFake(() => Promise.resolve(companies));
      sandbox.stub(Company, 'analyzeClabe').callsFake(() => Promise.resolve(bankInfo));
      sandbox.stub(Token, 'count').callsFake(() => Promise.resolve(1));
      sandbox.stub(sequelize, 'col').callsFake(value => {
        return value;
      });
    });

    it('should return an error if Company.findAll has an error', () => {
      Company.findAll.restore();
      sandbox.stub(Company, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getCompanies()
        .should.be.rejected
        .then(() => {
          Company.findAll.calledOnce.should.be.true;
          Company.findAll.calledWithMatch(findAll).should.be.true;
        });
    });

    it('should order call with diffferent params if we send parameters', () => {
      return classMethods.getCompanies(limit, offset, order_by, order_desc, role)
        .should.be.fulfilled
        .then(response => {
          Company.findAll.calledOnce.should.be.true;
          Company.findAll.calledWithMatch(findAllWithAttributes).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], companyElement);
        });
    });

    it('should return a successful response', () => {
      return classMethods.getCompanies()
        .should.be.fulfilled
        .then(response => {
          Company.findAll.calledOnce.should.be.true;
          Company.findAll.calledWithMatch(findAll).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], companyElement);
        });
    });
  });

  describe('getUsers', () => {
    const fixtures = helperFixtures.getUsersModel;
    const { classMethods } = optionsModel;
    const { id, orderBy, orderDesc, companyUsers, tokens, userQuery, tokenQuery,
      color, usersList, orderedList, noCompanyError, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(companyUsers));
      sandbox.stub(Token, 'findAll').callsFake(() => Promise.resolve(tokens));
      sandbox.stub(sequelize, 'literal').callsFake(value => {
        return value;
      });
      sandbox.stub(sequelize, 'json').callsFake(value => {
        return value;
      });
      sandbox.stub(sequelize, 'fn').callsFake((fn, value) => {
        return value;
      });
      sandbox.stub(_, 'sample').callsFake(() => color);
    });

    it('should return an error if Company.findOne has an error', () => {
      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.getUsers(id, orderBy, orderDesc)
        .should.be.rejected
        .then(error => {
          Company.findOne.calledOnce.should.be.true;
          Token.findAll.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(userQuery).should.be.true;
          Token.findAll.calledWithMatch(tokenQuery).should.be.true;
          error.should.be.deep.eql(commonError);
        });
    });

    it('should return an error if Token.findAll has an error', () => {
      Token.findAll.restore();
      sandbox.stub(Token, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getUsers(id, orderBy, orderDesc)
        .should.be.rejected
        .then(error => {
          Company.findOne.calledOnce.should.be.true;
          Token.findAll.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(userQuery).should.be.true;
          Token.findAll.calledWithMatch(tokenQuery).should.be.true;
          error.should.be.deep.eql(commonError);
        });
    });

    it('should return an error if there is no company', () => {
      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.getUsers(id, orderBy, orderDesc)
        .should.be.rejected
        .then(error => {
          Company.findOne.calledOnce.should.be.true;
          Token.findAll.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(userQuery).should.be.true;
          Token.findAll.calledWithMatch(tokenQuery).should.be.true;
          error.should.be.deep.eql(noCompanyError);
        });
    });

    it('should return a successful response with an ordered list', () => {
      sandbox.spy(_, 'orderBy');

      return classMethods.getUsers(id, orderBy, orderDesc)
        .should.be.fulfilled
        .then(list => {
          Company.findOne.calledOnce.should.be.true;
          Token.findAll.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(userQuery).should.be.true;
          Token.findAll.calledWithMatch(tokenQuery).should.be.true;
          _.orderBy.calledWithMatch(usersList, [ orderBy ], [ orderDesc ]);
          list.should.be.deep.eql(orderedList);
        });
    });
  });

  describe('validationFailed', () => {
    const fixtures = helperFixtures.validationFailed;
    const { hooks } = optionsModel;
    const { instance, options, clabeValidationError, commonError, invalidClabeError } = fixtures;

    it('should return success if error is not a clabe validation error', () => {
      return hooks.validationFailed(instance, options, commonError).should.be.fulfilled;
    });

    it('should return an invalid clabe error', () => {
      return hooks.validationFailed(instance, options, clabeValidationError)
        .should.be.rejected
        .then(error => {
          error.should.be.eql(invalidClabeError);
        });
    });
  });

  describe('getBalance', () => {
    const fixtures = helperFixtures.getBalance;
    const { instanceMethods } = optionsModel;
    const { instance, pendingWithdraws, balance, decreasedBalance, commonError, findAllQuery } = fixtures;

    it('should return error if PendingTransaction.findAll fails', () => {
      const method = instanceMethods.getBalance.bind(instance);

      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.reject(commonError));

      return method()
        .should.be.rejected
        .then(error => {
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWithMatch(findAllQuery).should.be.true;
          commonError.should.be.deep.equal(error);
        });
    });

    it('should return a same balance if no pending withdraws exist', () => {
      const method = instanceMethods.getBalance.bind(instance);

      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve([]));

      return method()
        .should.be.fulfilled
        .then(response => {
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWithMatch(findAllQuery).should.be.true;
          response.should.be.deep.equal(balance);
        });
    });

    it('should return a decreased balance', () => {
      const method = instanceMethods.getBalance.bind(instance);

      sandbox.stub(PendingTransaction, 'findAll').callsFake(() => Promise.resolve(pendingWithdraws));

      return method()
        .should.be.fulfilled
        .then(response => {
          PendingTransaction.findAll.calledOnce.should.be.true;
          PendingTransaction.findAll.calledWithMatch(findAllQuery).should.be.true;
          response.should.be.deep.equal(decreasedBalance);
        });
    });
  });

  describe('taxpayer_type set and get', () => {
    const fixtures = helperFixtures.setGet;
    const { taxpayerType, noTaxpayer, response, commonError, context, contextNotTaxpayer } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'setDataValue').callsFake((field, value) => Promise.resolve(value));
      sandbox.stub(context, 'getDataValue').callsFake(() => {
        return 'PHYSICAL';
      });
    });

    it('should return an error if the setDataValue return a reject', () => {
      context.setDataValue.restore();
      sandbox.stub(context, 'setDataValue').callsFake(() => Promise.reject(commonError));

      return attributesModel.taxpayer_type.set.call(context, taxpayerType)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a undefined response if taxpayer_type does not exists', () => {
      const result =  attributesModel.taxpayer_type.get.call(contextNotTaxpayer);

      assert.deepEqual(result, noTaxpayer);
    });

    it('should return a success if there was no issue with getDataValue', () => {
      const result =  attributesModel.taxpayer_type.get.call(context);

      assert.deepEqual(result, taxpayerType);
    });

    it('should return a success if there was no issue with setDataValue', () => {
      return attributesModel.taxpayer_type.set.call(context, taxpayerType)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('suspended_roles validate isValidRole', () => {
    const fixtures = helperFixtures.suspendedRoleValidateIsValidRole;
    const { validArray, invalidArray, invalidRoleError } = fixtures;

    it('should return an error if array includes an invalid role', () => {
      return Promise.resolve()
        .then(() => attributesModel.suspended_roles.validate.isValidRole(invalidArray))
        .should.be.rejected
        .then(error => JSON.stringify(error).should.be.eql(JSON.stringify(invalidRoleError)));
    });

    it('should return a success if all roles are valid', () => {
      return Promise.resolve()
        .then(() => attributesModel.suspended_roles.validate.isValidRole(validArray))
        .should.be.fulfilled;
    });
  });

  describe('updateRoleSuspension', () => {
    const fixtures = helperFixtures.updateRoleSuspension;
    const { context, suspendSuspendedRole, suspendUnsuspendedRole, unsuspendSuspendedRole,
      unsuspendUnsuspendedRole, commonError, errorAlreadySuspended, errorAlreadyUnsuspended,
      suspendedRoles, transaction } = fixtures;
    const updateRoleSuspension = optionsModel.instanceMethods.updateRoleSuspension;

    beforeEach(() => {
      context.suspended_roles = _.clone(suspendedRoles);
      sandbox.stub(context, 'save').callsFake(() => Promise.resolve());
    });

    it('should return an error if suspending suspended role', () => {
      return updateRoleSuspension.apply(context, suspendSuspendedRole)
        .should.be.rejected
        .then(error => {
          context.save.called.should.be.false;
          error.should.be.eql(errorAlreadySuspended);
        });
    });

    it('should return an error if unsuspending unsuspended role', () => {
      return updateRoleSuspension.apply(context, unsuspendUnsuspendedRole)
        .should.be.rejected
        .then(error => {
          context.save.called.should.be.false;
          error.should.be.eql(errorAlreadyUnsuspended);
        });
    });

    it('should return an error if this.save fails when suspending', () => {
      context.save.restore();
      sandbox.stub(context, 'save').callsFake(() => Promise.reject(commonError));

      return updateRoleSuspension.apply(context, suspendUnsuspendedRole)
        .should.be.rejected
        .then(error => {
          context.save.calledOnce.should.be.true;
          context.save.calledWith({ transaction }).should.be.true;
          error.should.be.eql(commonError);
        });
    });

    it('should return an error if this.save fails when unsuspending', () => {
      context.save.restore();
      sandbox.stub(context, 'save').callsFake(() => Promise.reject(commonError));

      return updateRoleSuspension.apply(context, unsuspendSuspendedRole)
        .should.be.rejected
        .then(error => {
          context.save.calledOnce.should.be.true;
          context.save.calledWith({ transaction }).should.be.true;
          error.should.be.eql(commonError);
        });
    });

    it('should return a success if suspending unsuspended role', () => {
      return updateRoleSuspension.apply(context, suspendUnsuspendedRole)
        .should.be.fulfilled
        .then(() => {
          context.save.calledOnce.should.be.true;
          context.save.calledWith({ transaction }).should.be.true;
          context.suspended_roles.indexOf(suspendUnsuspendedRole[0]).should.not.be.eql(-1);
        });
    });

    it('should return a success if unsuspending suspended role', () => {
      return updateRoleSuspension.apply(context, unsuspendSuspendedRole)
        .should.be.fulfilled
        .then(() => {
          context.save.calledOnce.should.be.true;
          context.save.calledWith({ transaction }).should.be.true;
          context.suspended_roles.indexOf(unsuspendSuspendedRole[0]).should.be.eql(-1);
        });
    });
  });

  describe('allowedCXP', () => {
    const fixtures = helperFixtures.allowedCXP;
    const { contextSuspendedCXP, contextUnsuspendedCXP } = fixtures;
    const { allowedCXP } = optionsModel.instanceMethods;

    it('should return a false is CXP is suspended', () => {
      return allowedCXP.apply(contextSuspendedCXP)
        .should.be.fulfilled
        .then(result => result.should.be.false);
    });

    it('should return a true is CXP is not suspended', () => {
      return allowedCXP.apply(contextUnsuspendedCXP)
        .should.be.fulfilled
        .then(result => result.should.be.true);
    });
  });

  describe('getCashFlowFromRange', () => {
    const fixtures = helperFixtures.getCashFlowFromRange;
    const { allowedLowerBound, allowedUpperBound,
      getCashFlowFromRangeContext, result, withdrawalsArray, depositsArray
    } = fixtures;
    const { getCashFlowFromRange } = optionsModel.instanceMethods;

    beforeEach(() => {
      sandbox.stub(statementQueries, 'investorCashWithDrawals').resolves(withdrawalsArray);
      sandbox.stub(statementQueries, 'investorCashDeposits').resolves(depositsArray);
      sandbox.stub(statementQueries, 'investorCashWithDrawalsFromInvoices').resolves(withdrawalsArray);
      sandbox.stub(statementQueries, 'investorCashDepositsFromInvoices').resolves(depositsArray);
    });

    it('should return error if difference betweeen moment bounds is negative', () => {
      return getCashFlowFromRange.apply(getCashFlowFromRangeContext, [ allowedUpperBound, allowedLowerBound ])
        .should.be.rejected
        .then(error => {
          error.errors[0].path.should.be.equal('company');
          error.errors[0].message.should.be.equal('Not found');
        });
    });

    it('return object with withdrawals and deposits', () => {
      return getCashFlowFromRange.apply(getCashFlowFromRangeContext, [ allowedLowerBound, allowedUpperBound ])
        .should.be.fulfilled
        .then( data => {
          data.withdrawals.should.be.equal(result.withdrawals);
          data.deposits.should.be.equal(result.deposits);
        });
    });
  });
});
