const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/order');

const OrderModel = require('../../../api/models/Order');
const optionsModel = OrderModel.options;

const { Order, OrderInvoice } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Order model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('newFundOrder', () => {
    const fixtures = helperFixtures.newFundOrder;
    const { classMethods } = optionsModel;
    const { commonError, invoices, investor, order,
      orderInvoice, orderCreateParams, orderInvoiceCreateParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(Order, 'create').callsFake(() => Promise.resolve(order));
      sandbox.stub(OrderInvoice, 'create').callsFake(() => Promise.resolve(orderInvoice));
    });

    it('should return an error if Order.create fails', () => {
      Order.create.restore();
      sandbox.stub(Order, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.newFundOrder(invoices, investor)
        .should.be.rejected
        .then(error => {
          Order.create.calledOnce.should.be.true;
          Order.create.args[0].should.be.eql(orderCreateParams);
          OrderInvoice.create.calledOnce.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if OrderInvoice.create fails', () => {
      OrderInvoice.create.restore();
      sandbox.stub(OrderInvoice, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.newFundOrder(invoices, investor)
        .should.be.rejected
        .then(error => {
          Order.create.calledOnce.should.be.true;
          Order.create.args[0].should.be.eql(orderCreateParams);
          OrderInvoice.create.calledOnce.should.be.true;
          OrderInvoice.create.args[0].should.be.eql(orderInvoiceCreateParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success', () => {
      return classMethods.newFundOrder(invoices, investor)
        .should.be.fulfilled
        .then(result => {
          Order.create.calledOnce.should.be.true;
          Order.create.args[0].should.be.eql(orderCreateParams);
          OrderInvoice.create.calledOnce.should.be.true;
          OrderInvoice.create.args[0].should.be.eql(orderInvoiceCreateParams);
          assert.deepEqual(result, [ orderInvoice ]);
        });
    });
  });
});
