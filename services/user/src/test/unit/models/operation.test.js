const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/operation');

const OperationModel = require('../../../api/models/Operation');
const optionsModel = OperationModel.options;

const { Operation, Invoice, sequelize } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Operation model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('getOperations by CxP', () => {
    const fixtures = helperFixtures.getOperationsCxP;
    const { classMethods } = optionsModel;
    const { invoices, invoiceElement,
      commonError, limit, offset, order_by, order_desc, where,
      requestClient, requestCompany, user, options, optionsCxp } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(invoices[0], 'getGeneralInfoWithOperation').callsFake(() => Promise.resolve(invoiceElement));
      sandbox.stub(sequelize, 'col').callsFake(value => {
        return value;
      });
    });

    it('should return an error if Invoice.findAll has an error', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getOperations( undefined, {}, user, options)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.called.should.be.false;
        });
    });

    it('should return an error if there was an issue getting the general info', () => {
      invoices[0].getGeneralInfoWithOperation.restore();
      sandbox.stub(invoices[0], 'getGeneralInfoWithOperation').callsFake(() => Promise.reject(commonError));

      return classMethods.getOperations(undefined, {}, user, options)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.calledOnce.should.be.true;
        });
    });

    it('should order call with diffferent params if we send parameters', () => {
      return classMethods.getOperations(undefined, { limit, offset, order_by, order_desc }, user, options)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should order call with diffferent params and where if we send them', () => {
      return classMethods.getOperations(where, { limit, offset, order_by, order_desc }, user, options)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with client_name sort', () => {
      return classMethods.getOperations(undefined, requestClient, user, options)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });


    it('should return a successful response with options', () => {
      return classMethods.getOperations(undefined, requestClient, user, optionsCxp)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with company_name sort', () => {
      return classMethods.getOperations(undefined, requestCompany, user, options)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          invoices[0].getGeneralInfoWithOperation.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });
  });

  describe('getOperations by CxC', () => {
    const fixtures = helperFixtures.getOperationsModel;
    const { classMethods } = optionsModel;
    const { operations, operationElement,
      commonError, limit, offset, order_by, order_desc, where,
      requestClient, requestCompany, user } = fixtures;

    beforeEach(() => {
      sandbox.stub(Operation, 'findAll').callsFake(() => Promise.resolve(operations));
      sandbox.stub(operations[0], 'getGeneralInfo').callsFake(() => Promise.resolve(operationElement));
      sandbox.stub(sequelize, 'col').callsFake(value => {
        return value;
      });
    });

    it('should return an error if Operation.findAll has an error', () => {
      Operation.findAll.restore();
      sandbox.stub(Operation, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getOperations( undefined, {}, user)
        .should.be.rejected
        .then(() => {
          Operation.findAll.calledOnce.should.be.true;
          operations[0].getGeneralInfo.called.should.be.false;
        });
    });

    it('should return an error if there was an issue getting the general info', () => {
      operations[0].getGeneralInfo.restore();
      sandbox.stub(operations[0], 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return classMethods.getOperations(undefined, {}, user)
        .should.be.rejected
        .then(() => {
          Operation.findAll.calledOnce.should.be.true;
          operations[0].getGeneralInfo.calledOnce.should.be.true;
        });
    });

    it('should order call with diffferent params if we send parameters', () => {
      return classMethods.getOperations(undefined, { limit, offset, order_by, order_desc }, user)
        .should.be.fulfilled
        .then(response => {
          Operation.findAll.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], operationElement);
        });
    });

    it('should order call with diffferent params and where if we send them', () => {
      return classMethods.getOperations(where, { limit, offset, order_by, order_desc }, user)
        .should.be.fulfilled
        .then(response => {
          Operation.findAll.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], operationElement);
        });
    });

    it('should return a successful response with client_name sort', () => {
      return classMethods.getOperations(undefined, requestClient, user)
        .should.be.fulfilled
        .then(response => {
          Operation.findAll.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], operationElement);
        });
    });

    it('should return a successful response with company_name sort', () => {
      return classMethods.getOperations(undefined, requestCompany, user)
        .should.be.fulfilled
        .then(response => {
          Operation.findAll.calledOnce.should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], operationElement);
        });
    });
  });

  describe('getAdminOperations', () => {
    const fixtures = helperFixtures.getAdminOperations;
    const { classMethods } = optionsModel;
    const {
      operations, operationInfo, commonError, allParams, response,
      allParamsOrderByCompanyName, allParamsOrderByInvestorName, allParamsOrderByStatus
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(Operation, 'findAll').callsFake(() => Promise.resolve(operations));
      sandbox.stub(Operation, 'count').callsFake(() => Promise.resolve(operations.length));
      sandbox.stub(operations[0], 'getGeneralInfoAsAdmin').callsFake(() => Promise.resolve(operationInfo));
    });

    it('should return an error if Operation.findAll has an error', () => {
      Operation.findAll.restore();
      sandbox.stub(Operation, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getAdminOperations(allParams)
        .should.be.rejected
        .then(() => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.called.should.be.false;
        });
    });

    it('should return an error if Operation.count has an error', () => {
      Operation.count.restore();
      sandbox.stub(Operation, 'count').callsFake(() => Promise.reject(commonError));

      return classMethods.getAdminOperations(allParams)
        .should.be.rejected
        .then(() => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
        });
    });

    it('should return an error if there was an issue getting the operation\'s info', () => {
      operations[0].getGeneralInfoAsAdmin.restore();
      sandbox.stub(operations[0], 'getGeneralInfoAsAdmin').callsFake(() => Promise.reject(commonError));

      return classMethods.getAdminOperations(allParams)
        .should.be.rejected
        .then(() => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
        });
    });

    it('should include all filters in query if provided', () => {
      return classMethods.getAdminOperations(allParams)
        .should.be.fulfilled
        .then((result) => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
          result.should.be.eql(response);
        });
    });
    it('should include order by company_name in query if provided', () => {
      return classMethods.getAdminOperations(allParamsOrderByCompanyName)
        .should.be.fulfilled
        .then((result) => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
          result.should.be.eql(response);
        });
    });
    it('should include order by investor_name in query if provided', () => {
      return classMethods.getAdminOperations(allParamsOrderByInvestorName)
        .should.be.fulfilled
        .then((result) => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
          result.should.be.eql(response);
        });
    });
    it('should include order by status in query if provided', () => {
      return classMethods.getAdminOperations(allParamsOrderByStatus)
        .should.be.fulfilled
        .then((result) => {
          Operation.findAll.calledOnce.should.be.true;
          Operation.count.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          operations[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
          result.should.be.eql(response);
        });
    });
  });

  describe('getBasicInfo', () => {
    const fixtures = helperFixtures.getBasicInfo;
    const { instanceMethods } = optionsModel;
    const { operation, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getBasicInfo.call(_.cloneDeep(operation))
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getGeneralInfo', () => {
    const fixtures = helperFixtures.getGeneralInfo;
    const { instanceMethods } = optionsModel;
    const { operation, resultOperation } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getGeneralInfo.call(_.cloneDeep(operation))
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultOperation, response);
        });
    });
  });
  describe('getGeneralInfoAsAdmin', () => {
    const fixtures = helperFixtures.getGeneralInfoAsAdmin;
    const { instanceMethods } = optionsModel;
    const { operation, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getGeneralInfoAsAdmin.call(operation)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });
});
