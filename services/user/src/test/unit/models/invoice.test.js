const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/invoice');

const InvoiceModel = require('../../../api/models/Invoice');
const optionsModel = InvoiceModel.options;

const { Company, User, Invoice, OperationCost, Operation, InvestorTransaction,
  sequelize } = require('../../../models');
const invoiceQueries = require('../../../api/helpers/invoice-queries');
const InvoiceHelper = require('../../../api/helpers/invoice');
const SendengoStrategy = require('../../../api/helpers/sendengo-strategy');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Invoice model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('beforeCreate', () => {
    const fixtures = helperFixtures.beforeCreate;
    const { hooks } = optionsModel;
    const { instanceWithSerie, resultWithSerie, instanceWithoutSerie, resultWithoutSerie } = fixtures;

    it('should return a formated invoice without serie in number', () => {
      return hooks.beforeCreate(instanceWithoutSerie)
        .should.be.fulfilled
        .then((response) => {
          response.should.be.eql(resultWithoutSerie);
        });
    });

    it('should return a formated invoice with serie and folio in number', () => {
      return hooks.beforeCreate(instanceWithSerie)
        .should.be.fulfilled
        .then((response) => {
          response.should.be.eql(resultWithSerie);
        });
    });
  });

  describe('subtotal set and get', () => {
    const fixtures = helperFixtures.setGet;
    const { attributes } = optionsModel;
    const { number, commonError, context } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'setDataValue').callsFake((field, value) => Promise.resolve(value));
      sandbox.stub(context, 'getDataValue').callsFake((field, value) => {
        return value;
      });
    });

    it('should return an error if the setDataValue return a reject', () => {
      context.setDataValue.restore();
      sandbox.stub(context, 'setDataValue').callsFake(() => Promise.reject(commonError));

      return attributes.subtotal.set.call(context, number)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, commonError);
        });
    });

    it('should retur a success if there was no issue with setDataValue', () => {
      return attributes.subtotal.set.call(context, number)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, number);
        });
    });
  });

  describe('tax_total set and get', () => {
    const fixtures = helperFixtures.setGet;
    const { attributes } = optionsModel;
    const { number, commonError, context } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'setDataValue').callsFake((field, value) => Promise.resolve(value));
      sandbox.stub(context, 'getDataValue').callsFake((field, value) => {
        return value;
      });
    });

    it('should return an error if the setDataValue return a reject', () => {
      context.setDataValue.restore();
      sandbox.stub(context, 'setDataValue').callsFake(() => Promise.reject(commonError));

      return attributes.tax_total.set.call(context, number)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, commonError);
        });
    });

    it('should retur a success if there was no issue with setDataValue', () => {
      return attributes.tax_total.set.call(context, number)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, number);
        });
    });
  });

  describe('tax_percentage set and get', () => {
    const fixtures = helperFixtures.setGet;
    const { attributes } = optionsModel;
    const { number, commonError, context } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'setDataValue').callsFake((field, value) => Promise.resolve(value));
      sandbox.stub(context, 'getDataValue').callsFake((field, value) => {
        return value;
      });
    });

    it('should return an error if the setDataValue return a reject', () => {
      context.setDataValue.restore();
      sandbox.stub(context, 'setDataValue').callsFake(() => Promise.reject(commonError));

      return attributes.tax_percentage.set.call(context, number)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, commonError);
        });
    });

    it('should retur a success if there was no issue with setDataValue', () => {
      return attributes.tax_percentage.set.call(context, number)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, number);
        });
    });
  });

  describe('total set and get', () => {
    const fixtures = helperFixtures.setGet;
    const { attributes } = optionsModel;
    const { number, commonError, context } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'setDataValue').callsFake((field, value) => Promise.resolve(value));
      sandbox.stub(context, 'getDataValue').callsFake((field, value) => {
        return value;
      });
    });

    it('should return an error if the setDataValue return a reject', () => {
      context.setDataValue.restore();
      sandbox.stub(context, 'setDataValue').callsFake(() => Promise.reject(commonError));

      return attributes.total.set.call(context, number)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, commonError);
        });
    });

    it('should retur a success if there was no issue with setDataValue', () => {
      return attributes.total.set.call(context, number)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, number);
        });
    });
  });

  describe('validateCompanies', () => {
    const fixtures = helperFixtures.validateCompanies;
    const { classMethods } = optionsModel;
    const { user, company, instances, userId, issuerRfc, recipientRfc, userQuery, recipientQuery,
      commonError, noClientError, noCompanyError, sameRfcError } = fixtures;

    beforeEach(() => {
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
    });

    it('should return an error if client and company rfc are the same', () => {
      return classMethods.validateCompanies(userId, issuerRfc, issuerRfc)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.false;
          Company.findOne.calledOnce.should.be.false;
          assert.deepEqual(error, sameRfcError);
        });
    });

    it('should return an error if the User.findOne return a reject', () => {
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.validateCompanies(userId, issuerRfc, recipientRfc)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWith(recipientQuery).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the Company.findOne return a reject', () => {
      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.validateCompanies(userId, issuerRfc, recipientRfc)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWith(recipientQuery).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the User.findOne return a null value ', () => {
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.validateCompanies(userId, issuerRfc, recipientRfc)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWith(recipientQuery).should.be.true;
          assert.deepEqual(error, noCompanyError);
        });
    });

    it('should return an error if the Company.findOne return a null value ', () => {
      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.validateCompanies(userId, issuerRfc, recipientRfc)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWith(recipientQuery).should.be.true;
          assert.deepEqual(error, noClientError);
        });
    });

    it('should return a success if there was no issue finding the companies', () => {
      return classMethods.validateCompanies(userId, issuerRfc, recipientRfc)
        .should.be.fulfilled
        .then((result) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWith(recipientQuery).should.be.true;
          assert.deepEqual(result, instances);
        });
    });
  });

  describe('getInvoices', () => {
    const fixtures = helperFixtures.getInvoicesModel;
    const { classMethods } = optionsModel;
    const { findAll, findAllWithAttributes, invoices, invoiceElement, generalInfoParams,
      commonError, limit, offset, order_by, order_desc, cxcUser, cxpUser, where, findAllWithWhere,
      requestClient, requestCompany, findClientSort, findCompanyNameSort, investorUser, daysDiffQueryResult,
      gainEstimateQueryResult, gainEstimatePercentageQueryResult, findAllIsInvestor,
      generalInfoParamsInvestor, sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(invoices[0], 'getGeneralInfo').callsFake(() => Promise.resolve(invoiceElement));
      sandbox.stub(invoices[0], 'getInvestorProfitEstimate').callsFake(() => Promise.resolve({}));
      sandbox.stub(sequelize, 'col').callsFake(value => {
        return value;
      });
      sandbox.stub(invoiceQueries, 'daysDiffQuery').callsFake(() => daysDiffQueryResult);
      sandbox.stub(invoiceQueries, 'gainEstimateQuery').callsFake(() => gainEstimateQueryResult);
      sandbox.stub(invoiceQueries, 'gainEstimatePercentageQuery').callsFake(() => gainEstimatePercentageQueryResult);
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectObjectByCompanyId' ).callsFake(( id, o, d) => d );
    });

    it('should return an error if Invoice.findAll has an error', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getInvoices(cxcUser, undefined, {})
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          invoices[0].getGeneralInfo.called.should.be.false;
        });
    });

    it('should return an error if there was an issue getting the general info', () => {
      invoices[0].getGeneralInfo.restore();
      sandbox.stub(invoices[0], 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return classMethods.getInvoices(cxcUser, undefined, {})
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          invoices[0].getGeneralInfo.calledOnce.should.be.true;
          invoices[0].getGeneralInfo.args.should.be.eql(generalInfoParams);
        });
    });


    it('should return an error if there was an issue getting the investor profit estimate', () => {
      invoices[0].getInvestorProfitEstimate.restore();
      sandbox.stub(invoices[0], 'getInvestorProfitEstimate').rejects(commonError);

      return classMethods.getInvoices(investorUser, undefined, {})
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAllIsInvestor).should.be.true;
          invoices[0].getGeneralInfo.calledOnce.should.be.true;
          invoices[0].getGeneralInfo.args.should.be.eql(generalInfoParamsInvestor);
          invoices[0].getInvestorProfitEstimate.calledOnce.should.be.true;
        });
    });

    it('should order call with diffferent params if we send parameters', () => {
      return classMethods.getInvoices(cxcUser, undefined, { limit, offset, order_by, order_desc })
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAllWithAttributes).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should order call with diffferent params and where if we send them', () => {
      return classMethods.getInvoices(cxcUser, where, { limit, offset, order_by, order_desc })
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAllWithWhere).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with client_name sort', () => {
      return classMethods.getInvoices(cxcUser, undefined, requestClient)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findClientSort).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with company_name sort', () => {
      return classMethods.getInvoices(cxcUser, undefined, requestCompany)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findCompanyNameSort).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with cxc user', () => {
      return classMethods.getInvoices(cxcUser, undefined, {})
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with cxp user', () => {
      return classMethods.getInvoices(cxpUser, undefined, {})
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });


    it('should return a successful response with investor user', () => {
      return classMethods.getInvoices(investorUser, undefined, {})
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAllIsInvestor).should.be.true;
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });
  });

  describe('getBasicOperationInfo', () => {
    const fixtures = helperFixtures.getBasicOperationInfo;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getBasicOperationInfo.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getBasicInfo', () => {
    const fixtures = helperFixtures.getBasicInfo;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getBasicInfo.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getBasicInfoWithClient', () => {
    const fixtures = helperFixtures.getBasicInfoWithClient;
    const { instanceMethods } = optionsModel;
    const { invoice, result, expirationDate, expirationDateExtra } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getBasicInfoWithClient.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response (isInvestor)', () => {
      invoice.expiration = expirationDate;

      return instanceMethods.getBasicInfoWithClient.call(invoice, true)
        .should.be.fulfilled
        .then(response => {
          // when isInvestor=true the expiration outcome would have 10 extra days
          new Date(response.expiration_extra).toISOString().should.contains(expirationDateExtra);
        });
    });
  });

  describe('getGeneralInfoWithOperation', () => {
    const fixtures = helperFixtures.getGeneralInfoWithOperation;
    const { instanceMethods } = optionsModel;
    const { invoice, resultWithOperation, resultWithoutOperation, company, invoiceWithoutOperation } = fixtures;

    it('should return a successful response without Operation', () => {
      return instanceMethods.getGeneralInfoWithOperation.call(_.cloneDeep(invoiceWithoutOperation), company, 'company')
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultWithoutOperation, response);
        });
    });

    it('should return a successful response with Operation', () => {
      return instanceMethods.getGeneralInfoWithOperation.call(_.cloneDeep(invoice), company, 'client')
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultWithOperation, response);
        });
    });
  });

  describe('getGeneralInfo', () => {
    const fixtures = helperFixtures.getGeneralInfo;
    const { instanceMethods } = optionsModel;
    const { invoice, resultCompany, resultClient, company } = fixtures;

    it('should return a successful response (company)', () => {
      return instanceMethods.getGeneralInfo.call(_.cloneDeep(invoice), company, 'company')
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultCompany, response);
        });
    });

    it('should return a successful response (client)', () => {
      return instanceMethods.getGeneralInfo.call(_.cloneDeep(invoice), company, 'client')
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultClient, response);
        });
    });
  });

  describe('getFundEstimate', () => {
    const fixtures = helperFixtures.getFundEstimate;
    const { instanceMethods } = optionsModel;
    const { invoice, costs, approvedInvoice, result, costQuery, commonError, invoiceWithoutExpiration,
      notApproved } = fixtures;

    beforeEach(() => {
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve(costs));
    });

    it('should return a successful response(from invoice)', () => {
      return instanceMethods.getFundEstimate.call(_.cloneDeep(approvedInvoice))
        .should.be.fulfilled
        .then(response => {
          OperationCost.findOne.called.should.be.false;
          assert.deepEqual(result, response);
        });
    });

    it('should return an error if invoice does not have expiration', () => {
      return instanceMethods.getFundEstimate.call(_.cloneDeep(invoiceWithoutExpiration))
        .should.be.rejected
        .then(error => {
          OperationCost.findOne.called.should.be.false;
          assert.deepEqual(error, notApproved);
        });
    });

    it('should return an error if OperationCost.findOne return a reject(from company)', () => {
      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.reject(commonError));

      return instanceMethods.getFundEstimate.call(_.cloneDeep(invoice))
        .should.be.rejected
        .then(error => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(costQuery);
          assert.deepEqual(error, commonError);
        });
    });

    it('should not modify invoice expiration date', () => {
      const invoiceClone = _.cloneDeep(invoice);
      const expiration = invoiceClone.expiration.toString();

      return instanceMethods.getFundEstimate.call(invoiceClone)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoiceClone.expiration.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response(from company)', () => {
      return instanceMethods.getFundEstimate.call(_.cloneDeep(invoice))
        .should.be.fulfilled
        .then(response => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(costQuery);
          assert.deepEqual(result, response);
        });
    });
  });

  describe('validateCxp', () => {
    const fixtures = helperFixtures.validateCxp;
    const { classMethods } = optionsModel;
    const { invoiceId, userId, actions, companyIdField, user, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'validateUser').callsFake(() => Promise.resolve(user));
    });

    it('should return an error if the Invoice.validateUser return a reject', () => {
      Invoice.validateUser.restore();
      sandbox.stub(Invoice, 'validateUser').callsFake(() => Promise.reject(commonError));

      return classMethods.validateCxp(userId, invoiceId)
        .should.be.rejected
        .then((error) => {
          Invoice.validateUser.calledOnce.should.be.true;
          Invoice.validateUser.calledWith(userId, invoiceId, actions, companyIdField).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return user', () => {
      return classMethods.validateCxp(userId, invoiceId)
        .should.be.fulfilled
        .then(response => {
          Invoice.validateUser.calledOnce.should.be.true;
          Invoice.validateUser.calledWith(userId, invoiceId, actions, companyIdField).should.be.true;
          assert.deepEqual(response, user);
        });
    });
  });

  describe('validateCxc', () => {
    const fixtures = helperFixtures.validateCxc;
    const { classMethods } = optionsModel;
    const { invoiceId, userId, actions, companyIdField, user, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'validateUser').callsFake(() => Promise.resolve(user));
    });

    it('should return an error if the Invoice.validateUser return a reject', () => {
      Invoice.validateUser.restore();
      sandbox.stub(Invoice, 'validateUser').callsFake(() => Promise.reject(commonError));

      return classMethods.validateCxc(userId, invoiceId)
        .should.be.rejected
        .then((error) => {
          Invoice.validateUser.calledOnce.should.be.true;
          Invoice.validateUser.calledWith(userId, invoiceId, actions, companyIdField).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return user', () => {
      return classMethods.validateCxc(userId, invoiceId)
        .should.be.fulfilled
        .then(response => {
          Invoice.validateUser.calledOnce.should.be.true;
          Invoice.validateUser.calledWith(userId, invoiceId, actions, companyIdField).should.be.true;
          assert.deepEqual(response, user);
        });
    });
  });

  describe('validateUser', () => {
    const fixtures = helperFixtures.validateUser;
    const { classMethods } = optionsModel;
    const { invoice, user, userId, userQuery, invoiceQuery, commonError,
      unauthorizedError, noInvoiceError, roles, rfcField } = fixtures;

    beforeEach(() => {
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(user));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoice));
    });

    it('should return an error if the User.findOne return a reject', () => {
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.validateUser(userId, invoice.id, roles, rfcField)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWith(invoiceQuery).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the Invoice.findOne return a reject', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.validateCxp(userId, invoice.id, roles, rfcField)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWith(invoiceQuery).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the user is not an allowed role to do that action', () => {
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve({ role: 'CXC' }));

      return classMethods.validateCxp(userId, invoice.id, roles, rfcField)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWith(invoiceQuery).should.be.true;
          assert.deepEqual(error, unauthorizedError);
        });
    });

    it('should return an error if Invoice.findOne does not return an object', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.validateCxp(userId, invoice.id, roles, rfcField)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWith(invoiceQuery).should.be.true;
          assert.deepEqual(error, noInvoiceError);
        });
    });

    it('should return an error if invoice client rfc and user rfc are not the same', () => {
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(
        () => Promise.resolve({ role: 'CXP', company_id: 2 })
      );

      return classMethods.validateCxp(userId, invoice.id, roles, rfcField)
        .should.be.rejected
        .then((error) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWith(invoiceQuery).should.be.true;
          assert.deepEqual(error, noInvoiceError);
        });
    });

    it('should return a success if there was no issue', () => {
      return classMethods.validateCxp(userId, invoice.id, roles, rfcField)
        .should.be.fulfilled
        .then((invoiceInstance) => {
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWith(userQuery).should.be.true;
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.calledWith(invoiceQuery).should.be.true;
          assert.deepEqual(invoiceInstance, invoice);
        });
    });
  });

  describe('validateExpirationDate', () => {
    const fixtures = helperFixtures.validateExpirationDate;
    const { classMethods } = optionsModel;
    const { invalidFormatError, invalidDateError } = fixtures;
    const day = 24 * 60 * 60 * 1000;
    const nextDay = new Date(new Date().getTime() + day);
    const invalidDate = new Date();

    it('should return an error if the expiration is not a date', () => {
      return classMethods.validateExpirationDate('Not a date')
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidFormatError);
        });
    });

    it('should return an error if the expiration is lesser than tomorrow', () => {
      return classMethods.validateExpirationDate(invalidDate)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidDateError);
        });
    });

    it('should return a valid date if there was no issue with the validations', () => {
      return classMethods.validateExpirationDate(nextDay)
        .should.be.fulfilled
        .then((date) => {
          assert.deepEqual(date, nextDay);
        });
    });
  });

  describe('getEstimate', () => {
    const fixtures = helperFixtures.getEstimate;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should not modify invoice expiration date', () => {
      const expiration = invoice.expiration.toString();

      return instanceMethods.getEstimate.call(invoice)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getEstimate.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getDetail', () => {
    const fixtures = helperFixtures.getDetail;
    const { instanceMethods } = optionsModel;
    const { invoice, sendengoInvoice, result, resultSendengo, estimates, sendengoInvoiceNoFundDate,
      sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(sendengoInvoice, 'getFundEstimate').resolves(estimates);
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should return a successful response', () => {
      return instanceMethods.getDetail.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });


    it('should return a successful response with sendengo invoice without fund date', () => {
      return instanceMethods.getDetail.call(sendengoInvoiceNoFundDate)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });


    it('should return a successful response, sendengo use case', () => {
      sendengoStrategy.selectByCompanyId.restore();
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.sendengo );

      return instanceMethods.getDetail.call(sendengoInvoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultSendengo, response);
        });
    });
  });

  describe('getMarketplace', () => {
    const fixtures = helperFixtures.getMarketplace;
    const { classMethods } = optionsModel;
    const { findAll, invoices, invoiceElement, generalInfoParams,
      commonError, where, findAllWithWhere, whereInclude, findAllInclude, daysDiffQueryResult,
      gainEstimateQueryResult, gainEstimatePercentageQueryResult, daysDiffQueryParams,
      gainEstimatePercentageQueryParams, gainEstimateQueryParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(invoices[0], 'getGeneralInfo').callsFake(() => Promise.resolve(invoiceElement));
      sandbox.stub(sequelize, 'col').callsFake(value => {
        return value;
      });
      sandbox.stub(invoiceQueries, 'daysDiffQuery').callsFake(() => daysDiffQueryResult);
      sandbox.stub(invoiceQueries, 'gainEstimateQuery').callsFake(() => gainEstimateQueryResult);
      sandbox.stub(invoiceQueries, 'gainEstimatePercentageQuery').callsFake(() => gainEstimatePercentageQueryResult);
    });

    it('should return an error if Invoice.findAll has an error', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getMarketplace(undefined, undefined)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          invoices[0].getGeneralInfo.called.should.be.false;
        });
    });

    it('should return an error if invoiceQueries.daysDiffQuery has an error', () => {
      invoiceQueries.daysDiffQuery.restore();
      sandbox.stub(invoiceQueries, 'daysDiffQuery').callsFake(() => {
        throw new Error(commonError);
      });

      return classMethods.getMarketplace(undefined, undefined)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.called.should.be.false;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.called.should.be.false;
          invoiceQueries.gainEstimatePercentageQuery.called.should.be.false;
          invoices[0].getGeneralInfo.called.should.be.false;
        });
    });

    it('should return an error if invoiceQueries.gainEstimateQuery has an error', () => {
      invoiceQueries.gainEstimateQuery.restore();
      sandbox.stub(invoiceQueries, 'gainEstimateQuery').callsFake(() => {
        throw new Error(commonError);
      });

      return classMethods.getMarketplace(undefined, undefined)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.called.should.be.false;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimateQuery.args[0].should.be.eql(gainEstimateQueryParams);
          invoiceQueries.gainEstimatePercentageQuery.called.should.be.false;
          invoices[0].getGeneralInfo.called.should.be.false;
        });
    });

    it('should return an error if invoiceQueries.gainEstimatePercentageQuery has an error', () => {
      invoiceQueries.gainEstimatePercentageQuery.restore();
      sandbox.stub(invoiceQueries, 'gainEstimatePercentageQuery').callsFake(() => {
        throw new Error(commonError);
      });

      return classMethods.getMarketplace(undefined, undefined)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.called.should.be.false;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimateQuery.args[0].should.be.eql(gainEstimateQueryParams);
          invoiceQueries.gainEstimatePercentageQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimatePercentageQuery.args[0].should.be.eql(gainEstimatePercentageQueryParams);
          invoices[0].getGeneralInfo.called.should.be.false;
        });
    });

    it('should return an error if there was an issue getting the general info', () => {
      invoices[0].getGeneralInfo.restore();
      sandbox.stub(invoices[0], 'getGeneralInfo').callsFake(() => Promise.reject(commonError));

      return classMethods.getMarketplace(undefined, undefined)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimateQuery.args[0].should.be.eql(gainEstimateQueryParams);
          invoiceQueries.gainEstimatePercentageQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimatePercentageQuery.args[0].should.be.eql(gainEstimatePercentageQueryParams);
          invoices[0].getGeneralInfo.calledOnce.should.be.true;
          invoices[0].getGeneralInfo.args.should.be.eql(generalInfoParams);
        });
    });

    it('should return a successful response if where is included', () => {
      return classMethods.getMarketplace(where)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAllWithWhere).should.be.true;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimateQuery.args[0].should.be.eql(gainEstimateQueryParams);
          invoiceQueries.gainEstimatePercentageQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimatePercentageQuery.args[0].should.be.eql(gainEstimatePercentageQueryParams);
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response with whereInclude variable added to the query', () => {
      return classMethods.getMarketplace(undefined, whereInclude)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAllInclude).should.be.true;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimateQuery.args[0].should.be.eql(gainEstimateQueryParams);
          invoiceQueries.gainEstimatePercentageQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimatePercentageQuery.args[0].should.be.eql(gainEstimatePercentageQueryParams);
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });

    it('should return a successful response', () => {
      return classMethods.getMarketplace(undefined, undefined)
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          invoiceQueries.daysDiffQuery.calledOnce.should.be.true;
          invoiceQueries.daysDiffQuery.args[0].should.be.eql(daysDiffQueryParams);
          invoiceQueries.gainEstimateQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimateQuery.args[0].should.be.eql(gainEstimateQueryParams);
          invoiceQueries.gainEstimatePercentageQuery.calledOnce.should.be.true;
          invoiceQueries.gainEstimatePercentageQuery.args[0].should.be.eql(gainEstimatePercentageQueryParams);
          response.length.should.be.equal(1);
          assert.deepEqual(response[0], invoiceElement);
        });
    });
  });

  describe('getInvestorFundEstimate', () => {
    const fixtures = helperFixtures.getInvestorFundEstimate;
    const { instanceMethods } = optionsModel;
    const { invoice, result, investorPhysical, investorMoral, isrResult, invoiceFundDate,
      investorFideicomiso } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoiceFundDate.expiration.toString();
      const fundDate = invoiceFundDate.fund_date.toString();

      return instanceMethods.getInvestorFundEstimate.call(invoiceFundDate, investorMoral)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoiceFundDate.expiration.toString());
          fundDate.should.be.equal(invoiceFundDate.fund_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response(of a company)', () => {
      return instanceMethods.getInvestorFundEstimate.call(invoice, investorMoral)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response(of an individual)', () => {
      return instanceMethods.getInvestorFundEstimate.call(invoice, investorPhysical)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(isrResult, response);
        });
    });

    it('should return a successful response(of a fideicomiso)', () => {
      const { fideicomisoResult } = helperFixtures.getInvestorFundEstimate.fundedByFideicomiso;

      return instanceMethods.getInvestorFundEstimate.call(invoice, investorFideicomiso)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(fideicomisoResult, response);
        });
    });

    it('should return a successful response if fund_date is present', () => {
      return instanceMethods.getInvestorFundEstimate.call(invoiceFundDate, investorMoral)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getInvestorFundEstimateFromTransaction', () => {
    const fixtures = helperFixtures.getInvestorFundEstimateFromTransaction;
    const { instanceMethods } = optionsModel;
    const { invoice, result, invoiceWithIsr, resultWithIsr } = fixtures;

    it('should return isr if included', () => {
      return instanceMethods.getInvestorFundEstimateFromTransaction.call(_.cloneDeep(invoiceWithIsr))
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultWithIsr, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getInvestorFundEstimateFromTransaction.call(_.cloneDeep(invoice))
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getInvestorProfitEstimateFromTransaction', () => {
    const fixtures = helperFixtures.getInvestorProfitEstimateFromTransaction;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getInvestorProfitEstimateFromTransaction.call(_.cloneDeep(invoice))
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getInvestorProfitEstimate', () => {
    const fixtures = helperFixtures.getInvestorProfitEstimate;
    const { instanceMethods } = optionsModel;
    const { invoice, result, invoiceFundDate } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoiceFundDate.expiration.toString();
      const fundDate = invoiceFundDate.fund_date.toString();

      return instanceMethods.getInvestorProfitEstimate.call(invoiceFundDate)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoiceFundDate.expiration.toString());
          fundDate.should.be.equal(invoiceFundDate.fund_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getInvestorProfitEstimate.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response if fund_date is present', () => {
      return instanceMethods.getInvestorProfitEstimate.call(invoiceFundDate)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getInvestorGain', () => {
    const fixtures = helperFixtures.getInvestorGain;
    const { instanceMethods } = optionsModel;
    const { invoice, result, fee, taxpayerType, isrResult, global_fee_percentage } = fixtures;

    it('should not modify invoice expiration date', () => {
      const expiration = invoice.expiration.toString();

      return instanceMethods.getInvestorGain.call(invoice, fee, global_fee_percentage)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response(of a company)', () => {
      return instanceMethods.getInvestorGain.call(invoice, fee)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response(of an individual)', () => {
      return instanceMethods.getInvestorGain.call(invoice, fee, taxpayerType, global_fee_percentage)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(isrResult, response);
        });
    });
  });

  describe('getInvoiceAmounts', () => {
    const fixtures = helperFixtures.getInvoiceAmounts;
    const { instanceMethods } = optionsModel;
    const { investor, globalFeePercentage, invoice, result } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoice.expiration.toString();

      return instanceMethods.getInvoiceAmounts.call(invoice, investor, globalFeePercentage)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getInvoiceAmounts.call(invoice, investor, globalFeePercentage)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getPublishEstimate', () => {
    const fixtures = helperFixtures.getPublishEstimate;
    const { instanceMethods } = optionsModel;
    const { estimates, queryParams, percentages, diffDays, invoice, result, sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(OperationCost, 'findOne').resolves(percentages);
      sandbox.stub(InvoiceHelper.prototype, 'getStartAndExpirationDayDifference').callsFake( () => diffDays);
      sandbox.stub(InvoiceHelper.prototype, 'getFundEstimates').callsFake( () => estimates);
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should return a successful response', () => {
      return instanceMethods.getPublishEstimate.call(invoice)
        .should.be.fulfilled
        .then(response => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.calledWith(queryParams).should.be.true;
          InvoiceHelper.prototype.getStartAndExpirationDayDifference.calledOnce.should.be.true;
          InvoiceHelper.prototype.getFundEstimates.calledOnce.should.be.true;
          InvoiceHelper.prototype.getFundEstimates.calledWith(diffDays).should.be.true;
          response.should.be.deep.eq(result);
        });
    });
  });

  describe('getInvestorFundDetail', () => {
    const fixtures = helperFixtures.getInvestorFundDetail;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoice.Operation.expiration_date.toString();
      const fundDate = invoice.Operation.InvestorTransaction.fund_request_date.toString();

      return instanceMethods.getInvestorFundDetail.call(invoice)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.Operation.expiration_date.toString());
          fundDate.should.be.equal(invoice.Operation.InvestorTransaction.fund_request_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getInvestorFundDetail.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getInvoicePaymentSummary', () => {
    const fixtures = helperFixtures.getInvoicePaymentSummary;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoice.expiration.toString();
      const fundDate = invoice.fund_date.toString();

      return instanceMethods.getInvoicePaymentSummary.call(invoice)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          fundDate.should.be.equal(invoice.fund_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getInvoicePaymentSummary.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getGeneralInfoAsAdmin', () => {
    const fixtures = helperFixtures.getGeneralInfoAsAdmin;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getGeneralInfoAsAdmin.call(invoice)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(result, response);
        });
    });
  });

  describe('getAdminInvoices', () => {
    const fixtures = helperFixtures.getAdminInvoices;
    const { classMethods } = optionsModel;
    const {
      invoices, invoiceInfo, commonError, allParams, allParamsFindAllQuery, allParamsCountQuery, response
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.resolve(invoices.length));
      sandbox.stub(invoices[0], 'getGeneralInfoAsAdmin').callsFake(() => Promise.resolve(invoiceInfo));
    });

    it('should return an error if Invoice.findAll has an error', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getAdminInvoices(allParams)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(allParamsFindAllQuery).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(allParamsCountQuery).should.be.true;
          invoices[0].getGeneralInfoAsAdmin.called.should.be.false;
        });
    });

    it('should return an error if Invoice.count has an error', () => {
      Invoice.count.restore();
      sandbox.stub(Invoice, 'count').callsFake(() => Promise.reject(commonError));

      return classMethods.getAdminInvoices(allParams)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(allParamsFindAllQuery).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(allParamsCountQuery).should.be.true;
          invoices[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          invoices[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
        });
    });

    it('should return an error if there was an issue getting the invoice\'s info', () => {
      invoices[0].getGeneralInfoAsAdmin.restore();
      sandbox.stub(invoices[0], 'getGeneralInfoAsAdmin').callsFake(() => Promise.reject(commonError));

      return classMethods.getAdminInvoices(allParams)
        .should.be.rejected
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(allParamsFindAllQuery).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(allParamsCountQuery).should.be.true;
          invoices[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          invoices[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
        });
    });

    it('should include all filters in query if provided', () => {
      return classMethods.getAdminInvoices(allParams)
        .should.be.fulfilled
        .then((result) => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(allParamsFindAllQuery).should.be.true;
          Invoice.count.calledOnce.should.be.true;
          Invoice.count.calledWithMatch(allParamsCountQuery).should.be.true;
          invoices[0].getGeneralInfoAsAdmin.calledOnce.should.be.true;
          invoices[0].getGeneralInfoAsAdmin.calledWith().should.be.true;
          result.should.be.eql(response);
        });
    });
  });

  describe('getExpirableInvoices', () => {
    const fixtures = helperFixtures.getExpirableInvoices;
    const { classMethods } = optionsModel;
    const { findAll, invoices, commonError,
      expiredInvoiceError } = fixtures;
    let invoicesClone;

    beforeEach(() => {
      invoicesClone = _.cloneDeep(invoices);

      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoicesClone));
      sandbox.stub(Invoice, 'validateExpirationDate').callsFake((date) => {
        if (date === 'nextDay') {
          return Promise.reject(expiredInvoiceError);
        }

        return Promise.resolve();
      });
    });

    it('should return an error if Invoice.findAll has an error', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.getExpirableInvoices()
        .should.be.rejected
        .then(err => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;

          Invoice.validateExpirationDate.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should return an error if Invoice.validateExpirationDate fails with unexpected error', () => {
      Invoice.validateExpirationDate.restore();
      sandbox.stub(Invoice, 'validateExpirationDate').callsFake(() => Promise.reject(commonError));

      return classMethods.getExpirableInvoices()
        .should.be.rejected
        .then(err => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;

          Invoice.validateExpirationDate.args.length.should.be.equal(invoices.length);
          err.should.be.equal(commonError);
        });
    });

    it('should only return a expired invoices', () => {
      return classMethods.getExpirableInvoices()
        .should.be.fulfilled
        .then(response => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.calledWithMatch(findAll).should.be.true;
          Invoice.validateExpirationDate.args.length.should.be.equal(invoices.length);
          response.length.should.be.eql(1);
        });
    });
  });

  describe('getAdminBasicInfo', () => {
    const fixtures = helperFixtures.getAdminBasicInfo;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should return a successful response', () => {
      return instanceMethods.getAdminBasicInfo.call(invoice)
        .should.be.fulfilled
        .then(response => assert.deepEqual(result, response));
    });
  });

  describe('getAdminCxcPayment', () => {
    const fixtures = helperFixtures.getAdminCxcPayment;
    const { instanceMethods } = optionsModel;
    const { invoice, result, sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should not modify invoice dates', () => {
      const expiration = invoice.expiration.toString();
      const fundDate = invoice.fund_date.toString();

      return instanceMethods.getAdminCxcPayment.call(invoice)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          fundDate.should.be.equal(invoice.fund_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getAdminCxcPayment.call(invoice)
        .should.be.fulfilled
        .then(response => assert.deepEqual(result, response));
    });
  });

  describe('getAdminInvestorPayment', () => {
    const fixtures = helperFixtures.getAdminInvestorPayment;
    const { instanceMethods } = optionsModel;
    const { invoice, invoicePhysical,
      result, resultPhysical } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoice.expiration.toString();
      const fundDate = invoice.fund_date.toString();

      return instanceMethods.getAdminInvestorPayment.call(invoice)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          fundDate.should.be.equal(invoice.fund_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getAdminInvestorPayment.call(invoice)
        .should.be.fulfilled
        .then(response => assert.deepEqual(result, response));
    });

    it('should return a successful response when investor invoice is a physical', () => {
      return instanceMethods.getAdminInvestorPayment.call(invoicePhysical)
        .should.be.fulfilled
        .then(response => assert.deepEqual(resultPhysical, response));
    });
  });

  describe('getAdminOperationSummary', () => {
    const fixtures = helperFixtures.getAdminOperationSummary;
    const { instanceMethods } = optionsModel;
    const { invoice, result } = fixtures;

    it('should not modify invoice dates', () => {
      const expiration = invoice.expiration.toString();
      const fundDate = invoice.fund_date.toString();

      return instanceMethods.getAdminOperationSummary.call(invoice)
        .should.be.fulfilled
        .then(response => {
          expiration.should.be.equal(invoice.expiration.toString());
          fundDate.should.be.equal(invoice.fund_date.toString());
          assert.deepEqual(result, response);
        });
    });

    it('should return a successful response', () => {
      return instanceMethods.getAdminOperationSummary.call(invoice)
        .should.be.fulfilled
        .then(response => assert.deepEqual(result, response));
    });
  });

  describe('updatePaymentDueInvoices', () => {
    const fixtures = helperFixtures.updatePaymentDueInvoices;
    const { classMethods } = optionsModel;
    const { query, invoices, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.reject(commonError));

      return classMethods.updatePaymentDueInvoices()
        .should.be.rejected
        .then((error) => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.args[0].should.be.eql(query);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.updatePaymentDueInvoices()
        .should.be.fulfilled
        .then((response) => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.args[0].should.be.eql(query);
          assert.deepEqual(response, invoices);
        });
    });
  });

  describe('createOperation', () => {
    const fixtures = helperFixtures.createOperation;
    const { classMethods } = optionsModel;
    const { invoice, costs, userQuery, commonError, user, operation, operationResponse, sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve(costs));
      sandbox.stub(Operation, 'create').callsFake(() => Promise.resolve(operation));
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectObjectByCompanyId' ).callsFake(( id, o, d) => d );
    });

    it('should return an error if OperationCost.findOne return a reject', () => {
      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.createOperation(_.cloneDeep(invoice), user.id)
        .should.be.rejected
        .then(error => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if Operation.create return a reject', () => {
      Operation.create.restore();
      sandbox.stub(Operation, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.createOperation(_.cloneDeep(invoice), user.id)
        .should.be.rejected
        .then(error => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          Operation.create.calledOnce.should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.createOperation(_.cloneDeep(invoice), user.id)
        .should.be.fulfilled
        .then(response => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          Operation.create.calledOnce.should.be.true;
          Operation.create.args[0].should.be.eql(operationResponse);
          assert.deepEqual(response, operation);
        });
    });
  });

  describe('createInvestorTransaction', () => {
    const fixtures = helperFixtures.createInvestorTransaction;
    const { classMethods } = optionsModel;
    const { invoice, costs, userQuery, commonError, user, investorTransaction,
      investorTransactionResponse, fideicomisoPercentages, fideicomisoTransaction,
      fideicomisoTransactionResponse } = fixtures;

    beforeEach(() => {
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve(costs));
      sandbox.stub(InvestorTransaction, 'create').callsFake(() => Promise.resolve(investorTransaction));
    });

    it('should return an error if OperationCost.findOne return a reject', () => {
      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvestorTransaction(_.cloneDeep(invoice), user.Company)
        .should.be.rejected
        .then(error => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if Operation.create return a reject', () => {
      InvestorTransaction.create.restore();
      sandbox.stub(InvestorTransaction, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvestorTransaction(_.cloneDeep(invoice), user.Company)
        .should.be.rejected
        .then(error => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          InvestorTransaction.create.calledOnce.should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response of a regular investor', () => {
      return classMethods.createInvestorTransaction(_.cloneDeep(invoice), user.Company)
        .should.be.fulfilled
        .then(response => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          InvestorTransaction.create.calledOnce.should.be.true;
          InvestorTransaction.create.args[0].should.be.eql(investorTransactionResponse);
          assert.deepEqual(response, investorTransaction);
        });
    });

    it('should return a successful response of a fideicomiso investor', () => {
      OperationCost.findOne.restore();
      sandbox.stub(OperationCost, 'findOne').callsFake(() => Promise.resolve(fideicomisoPercentages));
      InvestorTransaction.create.restore();
      sandbox.stub(InvestorTransaction, 'create').callsFake(() => Promise.resolve(fideicomisoTransaction));

      return classMethods.createInvestorTransaction(_.cloneDeep(invoice), user.Company)
        .should.be.fulfilled
        .then(response => {
          OperationCost.findOne.calledOnce.should.be.true;
          OperationCost.findOne.args[0].should.be.eql(userQuery);
          InvestorTransaction.create.calledOnce.should.be.true;
          InvestorTransaction.create.args[0].should.be.eql(fideicomisoTransactionResponse);
          assert.deepEqual(response, fideicomisoTransaction);
        });
    });
  });

  describe('removeFundRequest', () => {
    const fixtures = helperFixtures.removeFundRequest;
    const { classMethods } = optionsModel;
    const { invoice, transaction, transactionParams, destroyQuery, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(invoice, 'save').callsFake(() => Promise.resolve());
      sandbox.stub(InvestorTransaction, 'destroy').callsFake(() => Promise.resolve());
    });

    it('should return an error if invoice.save returns a reject', () => {
      invoice.save.restore();
      sandbox.stub(invoice, 'save').callsFake(() => Promise.reject(commonError));

      return classMethods.removeFundRequest(_.cloneDeep(invoice), transaction)
        .should.be.rejected
        .then(error => {
          invoice.save.calledOnce.should.be.true;
          invoice.save.args[0].should.be.eql([ transactionParams ]);
          InvestorTransaction.destroy.calledOnce.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if InvestorTransaction.destroy returns a reject', () => {
      InvestorTransaction.destroy.restore();
      sandbox.stub(InvestorTransaction, 'destroy').callsFake(() => Promise.reject(commonError));

      return classMethods.removeFundRequest(_.cloneDeep(invoice), transaction)
        .should.be.rejected
        .then(error => {
          invoice.save.calledOnce.should.be.true;
          invoice.save.args[0].should.be.eql([ transactionParams ]);
          InvestorTransaction.destroy.calledOnce.should.be.true;
          InvestorTransaction.destroy.args[0].should.be.eql(destroyQuery);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.removeFundRequest(_.cloneDeep(invoice), transaction)
        .should.be.fulfilled
        .then(() => {
          invoice.save.calledOnce.should.be.true;
          invoice.save.args[0].should.be.eql([ transactionParams ]);
          InvestorTransaction.destroy.calledOnce.should.be.true;
          InvestorTransaction.destroy.args[0].should.be.eql(destroyQuery);
        });
    });
  });

  describe('getNewestInvoices', () => {
    const fixtures = helperFixtures.getNewestInvoices;
    const { classMethods } = optionsModel;
    const { invoices, invoicesQuery, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(commonError));

      return classMethods.getNewestInvoices()
        .should.be.fulfilled
        .then((error) => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.args[0].should.be.eql(invoicesQuery);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.getNewestInvoices()
        .should.be.fulfilled
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.args[0].should.be.eql(invoicesQuery);
        });
    });
  });

  describe('getTrackerInfo', () => {
    const fixtures = helperFixtures.getTrackerInfo;
    const { responseOfQuery, mapped, sequelizeError } = fixtures;
    const { classMethods } = optionsModel;

    beforeEach(() => {
      sandbox.stub(sequelize, 'query').resolves(responseOfQuery);
      sandbox.stub(_, 'map').callsFake(() => mapped );
    });

    it('should return a successful response', () => {
      return classMethods.getTrackerInfo()
        .should.be.fulfilled
        .then(() => {
          sequelize.query.calledOnce.should.be.true;
          _.map.calledOnce.should.be.true;
        });
    });

    it('should return an error if sequelize.query rejects', () => {
      sequelize.query.restore();
      sandbox.stub(sequelize, 'query').rejects(sequelizeError);

      return classMethods.getTrackerInfo()
        .should.be.rejected
        .then(error => {
          sequelize.query.calledOnce.should.be.true;
          _.map.calledOnce.should.be.false;
          assert.deepEqual(error, sequelizeError);
        });
    });
  });

  describe('getNotifiableInvoices', () => {
    const fixtures = helperFixtures.getNotifiableInvoices;
    const { classMethods } = optionsModel;
    const { invoices, invoicesQuery, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(invoices));
    });

    it('should return an error if Invoice.findAll returns a reject', () => {
      Invoice.findAll.restore();
      sandbox.stub(Invoice, 'findAll').callsFake(() => Promise.resolve(commonError));

      return classMethods.getNotifiableInvoices()
        .should.be.fulfilled
        .then((error) => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.args[0].should.be.eql(invoicesQuery);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.getNotifiableInvoices()
        .should.be.fulfilled
        .then(() => {
          Invoice.findAll.calledOnce.should.be.true;
          Invoice.findAll.args[0].should.be.eql(invoicesQuery);
        });
    });
  });
});
