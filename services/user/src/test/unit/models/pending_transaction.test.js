const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/pending_transaction');

const PendingTransactionModel = require('../../../api/models/PendingTransaction');
const optionsModel = PendingTransactionModel.options;

const { Operation, PendingTransaction, Invoice, Transaction, Company, sequelize } = require('../../../models');
const SendengoStrategy = require('../../../api/helpers/sendengo-strategy');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/PendingTransaction model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('transaction', () => {
    const fixtures = helperFixtures.transaction;
    const { classMethods } = optionsModel;
    const { commonError, createParams, type, amount, company_id, keyObj } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'create').callsFake(() => Promise.resolve());
    });

    it('should return an error if PendingTransaction.create returns an error', () => {
      PendingTransaction.create.restore();
      sandbox.stub(PendingTransaction, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.transaction(type, amount, company_id, keyObj)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.create.args[0].should.be.eql(createParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue creating the record', () => {
      return classMethods.transaction(type, amount, company_id, keyObj)
        .should.be.fulfilled
        .then(() => {
          PendingTransaction.create.args[0].should.be.eql(createParams);
        });
    });
  });

  describe('approve', () => {
    const fixtures = helperFixtures.approveOrReject;
    const { classMethods } = optionsModel;
    const { commonError, notFoundError, transaction, id, approved,
      queryParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(transaction, 'save').callsFake(() => Promise.resolve(transaction));
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(transaction));
    });

    it('should return an error if PendingTransaction.findOne returns an error', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.approve(id)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.called.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if PendingTransaction.findOne does not return a transaction', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.approve(id)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.called.should.be.false;
          assert.deepEqual(error, notFoundError);
        });
    });

    it('should return an error if transaction.save returns an error', () => {
      transaction.save.restore();
      sandbox.stub(transaction, 'save').callsFake(() => Promise.reject(commonError));

      return classMethods.approve(id)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.calledOnce.should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue creating the record', () => {
      return classMethods.approve(id)
        .should.be.fulfilled
        .then((instance) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.calledOnce.should.be.true;
          assert.equal(instance.status, approved);
        });
    });
  });

  describe('reject', () => {
    const fixtures = helperFixtures.approveOrReject;
    const { classMethods } = optionsModel;
    const { commonError, notFoundError, transaction, id, rejected,
      queryParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(transaction, 'save').callsFake(() => Promise.resolve(transaction));
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(transaction));
    });

    it('should return an error if PendingTransaction.findOne returns an error', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.reject(id)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.called.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if PendingTransaction.findOne does not return a transaction', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.reject(id)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.called.should.be.false;
          assert.deepEqual(error, notFoundError);
        });
    });

    it('should return an error if transaction.save returns an error', () => {
      transaction.save.restore();
      sandbox.stub(transaction, 'save').callsFake(() => Promise.reject(commonError));

      return classMethods.reject(id)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.calledOnce.should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue creating the record', () => {
      return classMethods.reject(id)
        .should.be.fulfilled
        .then((instance) => {
          PendingTransaction.findOne.args[0].should.be.eql(queryParams);
          transaction.save.calledOnce.should.be.true;
          assert.equal(instance.status, rejected);
        });
    });
  });

  describe('deposit', () => {
    const fixtures = helperFixtures.depositOrWithdraw;
    const { classMethods } = optionsModel;
    const { commonError, depositParams, amount, company_rfc, key, deposit_date } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.resolve());
    });

    it('should return an error if PendingTransaction.transaction returns an error', () => {
      PendingTransaction.transaction.restore();
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.reject(commonError));

      return classMethods.deposit(amount, company_rfc, key, deposit_date)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.transaction.args[0].should.be.eql(depositParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue', () => {
      return classMethods.deposit(amount, company_rfc, key, deposit_date)
        .should.be.fulfilled
        .then(() => {
          PendingTransaction.transaction.args[0].should.be.eql(depositParams);
        });
    });
  });

  describe('withdraw', () => {
    const fixtures = helperFixtures.depositOrWithdraw;
    const { classMethods } = optionsModel;
    const { commonError, withdrawParams, amount, company_rfc } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.resolve());
    });

    it('should return an error if PendingTransaction.transaction returns an error', () => {
      PendingTransaction.transaction.restore();
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.reject(commonError));

      return classMethods.withdraw(amount, company_rfc)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.transaction.args[0].should.be.eql(withdrawParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue', () => {
      return classMethods.withdraw(amount, company_rfc)
        .should.be.fulfilled
        .then(() => {
          PendingTransaction.transaction.args[0].should.be.eql(withdrawParams);
        });
    });
  });

  describe('invoiceOperation', () => {
    const fixtures = helperFixtures.invoiceOperation;
    const { classMethods } = optionsModel;
    const { commonError, invoiceOperationParams, invoice, company_id } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.resolve());
    });

    it('should return an error if PendingTransaction.invoiceOperation returns an error', () => {
      PendingTransaction.transaction.restore();
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.reject(commonError));

      return classMethods.invoiceOperation(company_id, invoice)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.transaction.args[0].should.be.eql(invoiceOperationParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if there was no issue', () => {
      return classMethods.invoiceOperation(company_id, invoice)
        .should.be.fulfilled
        .then(() => {
          PendingTransaction.transaction.args[0].should.be.eql(invoiceOperationParams);
        });
    });
  });

  describe('createClientInvoicePayment', () => {
    const fixtures = helperFixtures.createClientInvoicePayment;
    const { classMethods } = optionsModel;
    const {
      commonError, paymentParamsWithKey, paymentParamsWithoutKey, invoice, amount, date,
      receipt, transaction
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.resolve());
    });

    it('should return an error if PendingTransaction.transaction returns an error', () => {
      PendingTransaction.transaction.restore();
      sandbox.stub(PendingTransaction, 'transaction').callsFake(() => Promise.reject(commonError));

      return classMethods.createClientInvoicePayment(invoice, amount, date, receipt, transaction)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.transaction.args[0].should.be.eql(paymentParamsWithKey);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success with no key if there was no issue', () => {
      return classMethods.createClientInvoicePayment(invoice, amount, date, null, transaction)
        .should.be.fulfilled
        .then(() => {
          PendingTransaction.transaction.args[0].should.be.eql(paymentParamsWithoutKey);
        });
    });

    it('should return a success with key if there was no issue', () => {
      return classMethods.createClientInvoicePayment(invoice, amount, date, receipt, transaction)
        .should.be.fulfilled
        .then(() => {
          PendingTransaction.transaction.args[0].should.be.eql(paymentParamsWithKey);
        });
    });
  });

  describe('getBasicInfo', () => {
    const fixtures = helperFixtures.getBasicInfo;
    const { instanceMethods } = optionsModel;
    const { transactionNoData, resultNoData, transactionWithData, resultWithData } = fixtures;

    it('should return a successful response with empty data', () => {
      return instanceMethods.getBasicInfo.call(transactionNoData)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultNoData, response);
        });
    });

    it('should return a successful response with stringified data', () => {
      return instanceMethods.getBasicInfo.call(transactionWithData)
        .should.be.fulfilled
        .then(response => {
          assert.deepEqual(resultWithData, response);
        });
    });
  });

  describe('rejectInvoiceFund', () => {
    const fixtures = helperFixtures.rejectInvoiceFund;
    const { classMethods } = optionsModel;
    const { invoiceId, invoiceInstance, sqlTransaction, invoiceQuery,
      invoiceNotFoundError, transactionParams, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoiceInstance));
      sandbox.stub(PendingTransaction, 'update').callsFake(() => Promise.resolve());
    });

    it('should return an error if Invoice.findOne return a reject', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.rejectInvoiceFund(invoiceId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.update.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if Invoice.findOne return an undefined value', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.rejectInvoiceFund(invoiceId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.update.called.should.be.false;

          assert.deepEqual(error, invoiceNotFoundError);
        });
    });

    it('should return a succes if there wasno issue', () => {
      return classMethods.rejectInvoiceFund(invoiceId, sqlTransaction)
        .should.be.fulfilled
        .then(invoice => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.update.calledOnce.should.be.true;
          PendingTransaction.update.args[0].should.be.eql(transactionParams);

          assert.deepEqual(invoice, invoiceInstance);
        });
    });
  });

  describe('findAndValidateInvoiceFund', () => {
    const fixtures = helperFixtures.findAndValidateInvoiceFund;
    const { classMethods } = optionsModel;
    const { invoiceId, transactionQuery, invoiceQuery, pendingTransaction,
      invoiceInstance, commonError, invoiceNotFoundError, pendingTransactionNotFoundError } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(pendingTransaction));
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoiceInstance));
    });

    it('should return an error if there was an issue with the PendingTransaction.findOne method', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.findAndValidateInvoiceFund(invoiceId)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(transactionQuery);
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if there was an issue with the Invoice.findOne method', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.findAndValidateInvoiceFund(invoiceId)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(transactionQuery);
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the PendingTransaction.findOne method return a null value', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.findAndValidateInvoiceFund(invoiceId)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(transactionQuery);
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);

          assert.deepEqual(error, pendingTransactionNotFoundError);
        });
    });

    it('should return an error if the Invoice.findOne method return a null value', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.findAndValidateInvoiceFund(invoiceId)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(transactionQuery);
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);

          assert.deepEqual(error, invoiceNotFoundError);
        });
    });

    it('should return an error if the invoice and pending transaction are not related', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve({ company_rfc: 'other rfc' }));

      return classMethods.findAndValidateInvoiceFund(invoiceId)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(transactionQuery);
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);

          assert.deepEqual(error, invoiceNotFoundError);
        });
    });

    it('should return an invoice and pending transaction instance if there was no issue', () => {
      return classMethods.findAndValidateInvoiceFund(invoiceId)
        .should.be.fulfilled
        .then(([ invoice, transaction ]) => {
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(transactionQuery);
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);

          assert.deepEqual(invoice, invoiceInstance);
          assert.deepEqual(transaction, pendingTransaction);
        });
    });
  });

  describe('createInvoiceFund', () => {
    const fixtures = helperFixtures.createInvoiceFund;
    const { classMethods } = optionsModel;
    const { invoiceId, companyId, sqlTransaction, findAndValidateParams,
      pendingTransaction, invoiceInstance, commonError, estimate,
      investorTransaction, cxcTransaction, companyUpdateParams, sendengoStrategy } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'findAndValidateInvoiceFund').callsFake(() => {
        return Promise.resolve([ invoiceInstance, pendingTransaction ]);
      });
      sandbox.stub(invoiceInstance, 'getFundEstimate').callsFake(() => Promise.resolve(estimate));
      sandbox.stub(Transaction, 'formatAndCreate').callsFake(() => Promise.resolve());
      sandbox.stub(pendingTransaction, 'save').callsFake(() => Promise.resolve());
      sandbox.stub(Company, 'update').callsFake(() => Promise.resolve());
      sandbox.stub(sequelize, 'literal').callsFake((str) => {
        return str;
      });
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should return an error if there PendingTransaction.findAndValidateInvoiceFund method returns an issue', () => {
      PendingTransaction.findAndValidateInvoiceFund.restore();
      sandbox.stub(PendingTransaction, 'findAndValidateInvoiceFund').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceFund(invoiceId, companyId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findAndValidateInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.findAndValidateInvoiceFund.args[0].should.be.eql(findAndValidateParams);
          invoiceInstance.getFundEstimate.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the invoice.getFundEstimate method returns an issue', () => {
      invoiceInstance.getFundEstimate.restore();
      sandbox.stub(invoiceInstance, 'getFundEstimate').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceFund(invoiceId, companyId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findAndValidateInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.findAndValidateInvoiceFund.args[0].should.be.eql(findAndValidateParams);
          invoiceInstance.getFundEstimate.calledOnce.should.be.true;
          Transaction.formatAndCreate.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the Transaction.formatAndCreate method returns an issue', () => {
      Transaction.formatAndCreate.restore();
      sandbox.stub(Transaction, 'formatAndCreate').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceFund(invoiceId, companyId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findAndValidateInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.findAndValidateInvoiceFund.args[0].should.be.eql(findAndValidateParams);
          invoiceInstance.getFundEstimate.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledTwice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(investorTransaction);
          Transaction.formatAndCreate.args[1].should.be.eql(cxcTransaction);
          pendingTransaction.save.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the pendingTransaction.save method returns an issue', () => {
      pendingTransaction.save.restore();
      sandbox.stub(pendingTransaction, 'save').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceFund(invoiceId, companyId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findAndValidateInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.findAndValidateInvoiceFund.args[0].should.be.eql(findAndValidateParams);
          invoiceInstance.getFundEstimate.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledTwice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(investorTransaction);
          Transaction.formatAndCreate.args[1].should.be.eql(cxcTransaction);
          pendingTransaction.save.calledOnce.should.be.true;
          pendingTransaction.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Company.update.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the Company.update method returns an issue', () => {
      Company.update.restore();
      sandbox.stub(Company, 'update').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceFund(invoiceId, companyId, sqlTransaction)
        .should.be.rejected
        .then(error => {
          PendingTransaction.findAndValidateInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.findAndValidateInvoiceFund.args[0].should.be.eql(findAndValidateParams);
          invoiceInstance.getFundEstimate.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledTwice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(investorTransaction);
          Transaction.formatAndCreate.args[1].should.be.eql(cxcTransaction);
          pendingTransaction.save.calledOnce.should.be.true;
          pendingTransaction.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Company.update.calledOnce.should.be.true;
          Company.update.args[0].should.be.eql(companyUpdateParams);

          assert.deepEqual(error, commonError);
        });
    });

    it('should return invoice and payment if there was no issue', () => {
      return classMethods.createInvoiceFund(invoiceId, companyId, sqlTransaction)
        .should.be.fulfilled
        .then(([ invoice, payment ]) => {
          PendingTransaction.findAndValidateInvoiceFund.calledOnce.should.be.true;
          PendingTransaction.findAndValidateInvoiceFund.args[0].should.be.eql(findAndValidateParams);
          invoiceInstance.getFundEstimate.calledOnce.should.be.true;
          Transaction.formatAndCreate.calledTwice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(investorTransaction);
          Transaction.formatAndCreate.args[1].should.be.eql(cxcTransaction);
          pendingTransaction.save.calledOnce.should.be.true;
          pendingTransaction.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Company.update.calledOnce.should.be.true;
          Company.update.args[0].should.be.eql(companyUpdateParams);

          assert.deepEqual(invoice, invoiceInstance);
          assert.deepEqual(payment, estimate.fund_payment);
        });
    });
  });

  describe('createInvoiceCompleted', () => {
    const fixtures = helperFixtures.createInvoiceCompleted;
    const { classMethods } = optionsModel;
    const {
      invoiceId, fondeoId, payments, sqlTransaction, pendingTransactionQuery,
      invoiceQuery, invoiceInstance, commonError,
      invoiceNotFoundError, pendingTransactionNotFoundError, createCXPTransactionParams,
      createCXCTransactionParams, createInvestorTransactionParams, updateInvestorBalanceParams,
      pendingTransactionInstance, paymentInProcessPendingTransaction, sendengoStrategy
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve(invoiceInstance));
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve(pendingTransactionInstance));
      sandbox.stub(pendingTransactionInstance, 'save').callsFake(
        () => Promise.resolve(paymentInProcessPendingTransaction));
      sandbox.stub(Transaction, 'formatAndCreate').callsFake(() => Promise.resolve());
      sandbox.stub(Company, 'update').callsFake(() => Promise.resolve());
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectObjectByCompanyId' ).callsFake(( id, o, d) => d );
    });

    it('should return an error if there was an issue with the Invoice.findOne method', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.false;
          pendingTransactionInstance.save.calledOnce.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          Company.update.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the Invoice.findOne method return a null value', () => {
      Invoice.findOne.restore();
      sandbox.stub(Invoice, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.false;
          pendingTransactionInstance.save.calledOnce.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          Company.update.called.should.be.false;

          assert.deepEqual(error, invoiceNotFoundError);
        });
    });

    it('should return an error if there was an issue with the PendingTransaction.findOne method', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          Company.update.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the PendingTransaction.findOne method return a null value', () => {
      PendingTransaction.findOne.restore();
      sandbox.stub(PendingTransaction, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.false;
          Transaction.formatAndCreate.called.should.be.false;
          Company.update.called.should.be.false;

          assert.deepEqual(error, pendingTransactionNotFoundError);
        });
    });

    it('should return an error if there was an issue with the instance.save method', () => {
      pendingTransactionInstance.save.restore();
      sandbox.stub(pendingTransactionInstance, 'save').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.true;
          pendingTransactionInstance.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Transaction.formatAndCreate.called.should.be.false;
          Company.update.called.should.be.false;

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the instance.save method return a null value', () => {
      pendingTransactionInstance.save.restore();
      sandbox.stub(pendingTransactionInstance, 'save').callsFake(() => Promise.resolve());

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.true;
          pendingTransactionInstance.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Transaction.formatAndCreate.called.should.be.false;
          Company.update.called.should.be.false;

          assert.deepEqual(error, pendingTransactionNotFoundError);
        });
    });

    it('should return an error if the Transaction.formatAndCreate method rejects', () => {
      Transaction.formatAndCreate.restore();
      sandbox.stub(Transaction, 'formatAndCreate').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.true;
          pendingTransactionInstance.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Transaction.formatAndCreate.calledThrice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(createCXPTransactionParams);
          Transaction.formatAndCreate.args[1].should.be.eql(createCXCTransactionParams);
          Transaction.formatAndCreate.args[2].should.be.eql(createInvestorTransactionParams);
          Company.update.calledOnce.should.be.true;
          Company.update.args[0].should.be.eql(updateInvestorBalanceParams);

          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if the Company.update method rejects', () => {
      Company.update.restore();
      sandbox.stub(Company, 'update').callsFake(() => Promise.reject(commonError));

      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.rejected
        .then(error => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.true;
          pendingTransactionInstance.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Transaction.formatAndCreate.calledThrice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(createCXPTransactionParams);
          Transaction.formatAndCreate.args[1].should.be.eql(createCXCTransactionParams);
          Transaction.formatAndCreate.args[2].should.be.eql(createInvestorTransactionParams);
          Company.update.calledOnce.should.be.true;
          Company.update.args[0].should.be.eql(updateInvestorBalanceParams);

          assert.deepEqual(error, commonError);
        });
    });

    it('should return invoice if there was no issue', () => {
      return classMethods.createInvoiceCompleted(invoiceId, fondeoId, payments, sqlTransaction)
        .should.be.fulfilled
        .then(invoice => {
          Invoice.findOne.calledOnce.should.be.true;
          Invoice.findOne.args[0].should.be.eql(invoiceQuery);
          PendingTransaction.findOne.calledOnce.should.be.true;
          PendingTransaction.findOne.args[0].should.be.eql(pendingTransactionQuery);
          pendingTransactionInstance.save.calledOnce.should.be.true;
          pendingTransactionInstance.save.args[0].should.be.eql([ { transaction: sqlTransaction } ]);
          Transaction.formatAndCreate.calledThrice.should.be.true;
          Transaction.formatAndCreate.args[0].should.be.eql(createCXPTransactionParams);
          Transaction.formatAndCreate.args[1].should.be.eql(createCXCTransactionParams);
          Transaction.formatAndCreate.args[2].should.be.eql(createInvestorTransactionParams);
          Company.update.calledOnce.should.be.true;
          Company.update.args[0].should.be.eql(updateInvestorBalanceParams);

          assert.deepEqual(invoice, invoiceInstance);
        });
    });
  });

  describe('isForInvestorInvoiceFund', () => {
    const fixtures = helperFixtures.isForInvestorInvoiceFund;
    const { instanceMethods } = optionsModel;
    const { pendingTransaction, pendingTransaction2 } = fixtures;


    it('should return false', function() {
      const result = instanceMethods.isForInvestorInvoiceFund.call(pendingTransaction2);

      return Promise.resolve()
        .should.be.fulfilled
        .then( () => {
          result.should.be.eql(false);
        });
    });

    it('should return true', function() {
      const result = instanceMethods.isForInvestorInvoiceFund.call(pendingTransaction);

      return Promise.resolve()
        .should.be.fulfilled
        .then( () => {
          result.should.be.eql(true);
        });
    });
  });

  describe('bankRawData', () => {
    const fixtures = helperFixtures.bankRawData;
    const { classMethods } = optionsModel;
    const { commonError, pendingTransactions, bankReport, cxcPayment, estimate,
      sendengoStrategy, operations, operation1, operation2, rawData } = fixtures;

    beforeEach(() => {
      sandbox.stub(Operation, 'findAll').resolves(operations);
      sandbox.stub(operation1.Invoice, 'getAdminCxcPayment').resolves(cxcPayment);
      sandbox.stub(operation1.Invoice, 'getFundEstimate').resolves(estimate);
      sandbox.stub(operation2.Invoice, 'getAdminCxcPayment').resolves(cxcPayment);
      sandbox.stub(operation2.Invoice, 'getFundEstimate').resolves(estimate);
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig' ).resolves(sendengoStrategy);
      sandbox.stub(sendengoStrategy, 'selectByCompanyId' ).callsFake(( id, object ) => object.default );
    });

    it('should reject if SendengoStrategy.generateStrategyObjectByConfig rejects', function() {
      SendengoStrategy.generateStrategyObjectByConfig.restore();
      sandbox.stub(SendengoStrategy, 'generateStrategyObjectByConfig').rejects(commonError);

      classMethods.bankRawData(pendingTransactions, bankReport)
        .should.be.rejected
        .then(() => {
          SendengoStrategy.generateStrategyObjectByConfig.calledOnce.should.be.true;
          Operation.findAll.calledOnce.should.be.false;
        });
    });

    it('should reject if Operation.findOne rejects', function() {
      Operation.findAll.restore();
      sandbox.stub(Operation, 'findAll').rejects(commonError);

      classMethods.bankRawData(pendingTransactions, bankReport)
        .should.be.rejected
        .then(() => {
          Operation.findAll.calledOnce.should.be.true;
        });
    });

    it('should return a successful response', function() {
      return classMethods.bankRawData(pendingTransactions, bankReport)
        .should.be.fulfilled
        .then((result) => {
          Operation.findAll.calledOnce.should.be.true;
          rawData.should.be.deep.eq(result);
        });
    });
  });

  describe('buildBankReportQuery', () => {
    const fixtures = helperFixtures.buildBankReportQuery;
    const { classMethods } = optionsModel;
    const { queryParams1, response1 } = fixtures;

    it('should return a successful response', function() {
      const result = classMethods.buildBankReportQuery(queryParams1);

      return Promise.resolve(result)
        .should.be.fulfilled
        .then(() => {
          result.should.be.eql(response1);
        });
    });
  });
});
