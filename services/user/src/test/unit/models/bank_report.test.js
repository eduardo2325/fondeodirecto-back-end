const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/bank_report');
const XLSX = require('xlsx');
const s3 = require('/var/lib/core/js/s3');

const BankReportModel = require('../../../api/models/BankReport');
const optionsModel = BankReportModel.options;

const { BankReport, PendingTransaction } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/BankRepor model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('uploadCxcExcelReportDataToS3', () => {
    const fixtures = helperFixtures.uploadCxcExcelReportDataToS3;
    const { classMethods } = optionsModel;
    const { arrayOfArrays, guid, commonError, buf, s3Url, options, wb, ws, secondParameterWrite } = fixtures;

    beforeEach(() => {
      sandbox.stub(XLSX.utils, 'aoa_to_sheet').callsFake(() => ws);
      sandbox.stub(XLSX.utils, 'book_new').callsFake(() => wb);
      sandbox.stub(XLSX.utils, 'book_append_sheet').callsFake(() => true);
      sandbox.stub(XLSX, 'write').callsFake(() => buf);
      sandbox.stub(s3, 'upload').resolves( s3Url );
    });

    it('should reject if s3.upload rejects', () => {
      s3.upload.restore();
      sandbox.stub(s3, 'upload').rejects( commonError );

      return classMethods.uploadCxcExcelReportDataToS3(arrayOfArrays, guid)
        .should.be.rejected
        .then((error) => {
          s3.upload.called.should.be.true;
          const args = s3.upload.args[0];

          args[1].should.be.eq(buf);
          args[2].should.be.eq(guid);
          args[3].should.be.deep.equal(options);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return success', () => {
      return classMethods.uploadCxcExcelReportDataToS3(arrayOfArrays, guid)
        .should.be.fulfilled
        .then(() => {
          s3.upload.called.should.be.true;
          const args = s3.upload.args[0];

          args[1].should.be.eq(buf);
          args[2].should.be.eq(guid);
          args[3].should.be.deep.equal(options);

          XLSX.utils.aoa_to_sheet.called.should.be.true;
          XLSX.utils.aoa_to_sheet.calledWith(arrayOfArrays).should.be.true;
          XLSX.utils.book_new.called.should.be.true;
          XLSX.utils.book_append_sheet.calledWith(wb, ws, 'SheetJS').should.be.true;
          XLSX.write.called.should.be.true;
          XLSX.write.calledWith(wb, secondParameterWrite).should.be.true;
        });
    });
  });

  describe('savePendingTransactionsToBankReport', () => {
    const fixtures = helperFixtures.savePendingTransactionsToBankReport;
    const { classMethods } = optionsModel;
    const { idsPendingTransactions, reportData, sequelizeTransaction, created,
      createParams, updateParams, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(BankReport, 'create').resolves(created);
      sandbox.stub(PendingTransaction, 'update').resolves(true);
    });

    it('should reject if BankReport.create rejects', () => {
      BankReport.create.restore();
      sandbox.stub(BankReport, 'create').rejects( commonError );

      return classMethods.savePendingTransactionsToBankReport(idsPendingTransactions, reportData, sequelizeTransaction)
        .should.be.rejected
        .then((error) => {
          BankReport.create.called.should.be.true;
          BankReport.create.args[0].should.be.eql(createParams);
          PendingTransaction.update.called.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should reject if PendingTransaction.update rejects', () => {
      PendingTransaction.update.restore();
      sandbox.stub(PendingTransaction, 'update').rejects( commonError );

      return classMethods.savePendingTransactionsToBankReport(idsPendingTransactions, reportData, sequelizeTransaction)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.update.called.should.be.true;
          PendingTransaction.update.args[0].should.be.eql(updateParams);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return success', () => {
      return classMethods.savePendingTransactionsToBankReport(idsPendingTransactions, reportData, sequelizeTransaction)
        .should.be.fulfilled
        .then((response) => {
          BankReport.create.called.should.be.true;
          BankReport.create.args[0].should.be.eql(createParams);
          PendingTransaction.update.called.should.be.true;
          PendingTransaction.update.args[0].should.be.eql(updateParams);
          assert.deepEqual(response, true);
        });
    });
  });

  describe('findNumberOfApprovedTransactions', () => {
    const fixtures = helperFixtures.findNumberOfApprovedTransactions;
    const { classMethods } = optionsModel;
    const { response, count, reports, commonError, countParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(PendingTransaction, 'count').resolves(count);
    });

    it('should reject if PendingTransaction.count rejects', () => {
      PendingTransaction.count.restore();
      sandbox.stub(PendingTransaction, 'count').rejects( commonError );

      return classMethods.findNumberOfApprovedTransactions(reports)
        .should.be.rejected
        .then((error) => {
          PendingTransaction.count.called.should.be.true;
          PendingTransaction.count.calledWith(countParams).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.findNumberOfApprovedTransactions(reports)
        .should.be.fulfilled
        .then((data) => {
          PendingTransaction.count.called.should.be.true;
          PendingTransaction.count.calledWith(countParams).should.be.true;
          assert.deepEqual(data, response);
        });
    });
  });
});
