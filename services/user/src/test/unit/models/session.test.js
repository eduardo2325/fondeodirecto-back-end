const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/session');

const randtoken = require('rand-token');
const SessionModel = require('../../../api/models/Session');
const { User, Session, Agreement } = require('../../../models');
const optionsModel = SessionModel.options;

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Session model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('beforeCreate', () => {
    const fixtures = helperFixtures.beforeCreate;
    const { hooks } = optionsModel;
    const { instance, token } = fixtures;

    beforeEach(() => {
      sandbox.stub(randtoken, 'generate').callsFake(() => token);
    });

    it('should return a successful response', () => {
      return hooks.beforeCreate(instance)
        .should.be.fulfilled
        .then(() => {
          randtoken.generate.calledOnce.should.be.true;
          randtoken.generate.calledWith(16).should.be.true;
        });
    });
  });

  describe('verifyAndCreate', () => {
    const fixtures = helperFixtures.verifyAndCreate;
    const { classMethods } = optionsModel;
    const { email, password, unsuspendedUserModel, context, unsuspendedSessionModel,
      suspendedUserModel, suspendedSessionModel, getAgreementResult } = fixtures;

    beforeEach(() => {
      sandbox.stub(User, 'verify').callsFake(() => Promise.resolve(unsuspendedUserModel));
      sandbox.stub(context, 'create').callsFake(() => Promise.resolve());
      sandbox.stub(Agreement, 'findContract').callsFake(() => Promise.resolve(getAgreementResult));
    });

    it('should return error if User.verify has an error', () => {
      const error = new Error('verify error');

      User.verify.restore();
      sandbox.stub(User, 'verify').callsFake(() => Promise.reject(error));

      return classMethods.verifyAndCreate.call(context, email, password)
        .should.be.rejected
        .then(() => {
          User.verify.calledOnce.should.be.true;
          User.verify.calledWith(email, password).should.be.true;
          context.create.called.should.be.false;
        });
    });

    it('should return error if Agreement.findContract has an error', () => {
      Agreement.findContract.restore();
      sandbox.stub(Agreement, 'findContract').callsFake(() => Promise.reject());

      return classMethods.verifyAndCreate.call(context, email, password)
        .should.be.rejected
        .then(() => {
          User.verify.calledOnce.should.be.true;
          User.verify.calledWith(email, password).should.be.true;
          context.create.called.should.be.false;
        });
    });

    it('should return error if self.create has an error', () => {
      const error = new Error('create error');

      context.create.restore();
      sandbox.stub(context, 'create').callsFake(() => Promise.reject(error));

      return classMethods.verifyAndCreate.call(context, email, password)
        .should.be.rejected
        .then(() => {
          User.verify.calledOnce.should.be.true;
          User.verify.calledWith(email, password).should.be.true;
          context.create.calledOnce.should.be.true;
          context.create.calledWith(unsuspendedSessionModel).should.be.true;
        });
    });

    it('should return a suspended successful response', () => {
      User.verify.restore();
      sandbox.stub(User, 'verify').callsFake(() => Promise.resolve(suspendedUserModel));

      return classMethods.verifyAndCreate.call(context, email, password)
        .should.be.fulfilled
        .then(() => {
          User.verify.calledOnce.should.be.true;
          User.verify.calledWith(email, password).should.be.true;
          context.create.calledOnce.should.be.true;
          context.create.calledWith(suspendedSessionModel).should.be.true;
        });
    });

    it('should return an unsuspended successful response', () => {
      return classMethods.verifyAndCreate.call(context, email, password)
        .should.be.fulfilled
        .then(() => {
          User.verify.calledOnce.should.be.true;
          User.verify.calledWith(email, password).should.be.true;
          context.create.calledOnce.should.be.true;
          context.create.calledWith(unsuspendedSessionModel).should.be.true;
        });
    });
  });

  describe('checkToken', () => {
    const fixtures = helperFixtures.checkTokenModel;
    const { classMethods } = optionsModel;
    const { context, token, findOneCall, validSession, invalidSession } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'findOne').callsFake(() => Promise.resolve(validSession));
      sandbox.stub(validSession, 'save').callsFake(() => Promise.resolve());
    });

    it('should return error if findOne return an error', () => {
      context.findOne.restore();
      sandbox.stub(context, 'findOne').callsFake(() => Promise.reject());

      return classMethods.checkToken.call(context, token)
        .should.be.rejected
        .then(() => {
          context.findOne.calledOnce.should.be.true;
          context.findOne.calledWith(findOneCall).should.be.true;
          validSession.save.called.should.be.false;
        });
    });

    it('should return error if session does not exists', () => {
      context.findOne.restore();
      sandbox.stub(context, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.checkToken.call(context, token)
        .should.be.rejected
        .then(() => {
          context.findOne.calledOnce.should.be.true;
          context.findOne.calledWith(findOneCall).should.be.true;
          validSession.save.called.should.be.false;
        });
    });

    it('should return error if session was expired', () => {
      context.findOne.restore();
      sandbox.stub(context, 'findOne').callsFake(() => Promise.resolve(invalidSession));

      return classMethods.checkToken.call(context, token)
        .should.be.rejected
        .then(() => {
          context.findOne.calledOnce.should.be.true;
          context.findOne.calledWith(findOneCall).should.be.true;
          validSession.save.called.should.be.false;
        });
    });

    it('should return a successful response', () => {
      return classMethods.checkToken.call(context, token)
        .should.be.fulfilled
        .then(() => {
          context.findOne.calledOnce.should.be.true;
          context.findOne.calledWith(findOneCall).should.be.true;
          validSession.save.calledOnce.should.be.true;
        });
    });
  });

  describe('updateSuspensions', () => {
    const fixtures = helperFixtures.updateSuspensions;
    const { classMethods } = optionsModel;
    const { context, companyId, role, suspend, updateToSuspend, updateToUnsuspend,
      updateOptions, transaction, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(context, 'update').callsFake(() => Promise.resolve());
    });

    it('should return error if update return an error', () => {
      context.update.restore();
      sandbox.stub(context, 'update').callsFake(() => Promise.reject(commonError));

      return classMethods.updateSuspensions.call(context, companyId, role, suspend, transaction)
        .should.be.rejected
        .then(err => {
          context.update.calledOnce.should.be.true;
          context.update.calledWithMatch(updateToSuspend, updateOptions).should.be.true;
          err.should.be.eql(commonError);
        });
    });

    it('should unsuspend sessions successfully', () => {
      return classMethods.updateSuspensions.call(context, companyId, role, !suspend, transaction)
        .should.be.fulfilled
        .then(() => {
          context.update.calledOnce.should.be.true;
          context.update.calledWithMatch(updateToUnsuspend, updateOptions).should.be.true;
        });
    });

    it('should suspend sessions successfully', () => {
      return classMethods.updateSuspensions.call(context, companyId, role, suspend, transaction)
        .should.be.fulfilled
        .then(() => {
          context.update.calledOnce.should.be.true;
          context.update.calledWithMatch(updateToSuspend, updateOptions).should.be.true;
        });
    });
  });

  describe('getInformation', () => {
    const fixtures = helperFixtures.getInformation;
    const { classMethods } = optionsModel;
    const { token, query, sessionData, response, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Session, 'findOne').callsFake(() => Promise.resolve(sessionData));
    });

    it('should return error if Session.findOne return an error', () => {
      Session.findOne.restore();
      sandbox.stub(Session, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.getInformation(token)
        .should.be.rejected
        .then(err => {
          Session.findOne.args[0].should.be.eql(query);
          err.should.be.eql(commonError);
        });
    });

    it('should get sessions information successfully', () => {
      return classMethods.getInformation(token)
        .should.be.fulfilled
        .then(result => {
          Session.findOne.args[0].should.be.eql(query);
          response.should.be.eql(result);
        });
    });
  });
});
