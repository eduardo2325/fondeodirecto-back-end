const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/new_invoice_notification');

const NewInvoiceNotificationModel = require('../../../api/models/NewInvoiceNotification');
const optionsModel = NewInvoiceNotificationModel.options;

const { Token } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/NewInvoiceNotification model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('createNewInvoiceToken', () => {
    const fixtures = helperFixtures.createNewInvoiceToken;
    const { classMethods } = optionsModel;
    const { commonError, existingToken, createData, findOneQuery, investor, token, tokenInformation } = fixtures;

    beforeEach(() => {
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());
      sandbox.stub(Token, 'create').callsFake(() => Promise.resolve(token));
      sandbox.stub(existingToken, 'destroy').callsFake(() => Promise.resolve());
    });

    it('should return an error if Token.findOne fails', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.createNewInvoiceToken(investor)
        .should.be.rejected
        .then(error => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql([ findOneQuery ]);
          Token.create.calledOnce.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if existingToken.destroy fails', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(existingToken));
      existingToken.destroy.restore();
      sandbox.stub(existingToken, 'destroy').callsFake(() => Promise.reject(commonError));

      return classMethods.createNewInvoiceToken(investor)
        .should.be.rejected
        .then(error => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql([ findOneQuery ]);
          existingToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if Token.create fails', () => {
      Token.create.restore();
      sandbox.stub(Token, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.createNewInvoiceToken(investor)
        .should.be.rejected
        .then(error => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql([ findOneQuery ]);
          Token.create.calledOnce.should.be.true;
          Token.create.args[0].should.be.eql([ createData ]);
          assert.deepEqual(error, commonError);
        });
    });

    it('should destroy previous token and return newest one', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(existingToken));

      return classMethods.createNewInvoiceToken(investor)
        .should.be.fulfilled
        .then(result => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql([ findOneQuery ]);
          existingToken.destroy.calledOnce.should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.args[0].should.be.eql([ createData ]);
          assert.deepEqual(result, tokenInformation);
        });
    });

    it('should return a token for an specific investor', () => {
      return classMethods.createNewInvoiceToken(investor)
        .should.be.fulfilled
        .then(result => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql([ findOneQuery ]);
          Token.create.calledOnce.should.be.true;
          Token.create.args[0].should.be.eql([ createData ]);
          assert.deepEqual(result, tokenInformation);
        });
    });
  });
});
