const chai = require('chai');

const pdf = require('../../../pdf/builder');
const s3 = require('/var/lib/core/js/s3');

const { Agreement } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

const td = require('testdouble');

describe('unit/Agreement model', () => {
  let sourceCallback;
  let findCallback;
  let getCallback;
  let previewCallback;
  let uploadCallback;
  let buildCallback;
  let urlCallback;

  const IF_OK = Symbol('ALL_SEEMS_GOOD');

  const guid = '294e499f-518a-4a8d-91b6-95e2b2ac05fd';
  const companyId = 1;
  const role = 'INVESTOR';

  const agreement = {
    company_id: companyId,
    user_role: role
  };

  const params = {
    template: 'dummy',
    locals: {
      FOO: 'Bar bazz'
    }
  };

  const key = `companies/${companyId}/agreements/${role}`;

  beforeEach(() => {
    sourceCallback = td.func('Agreement.getSource');
    findCallback = td.func('Agreement.findOrCreate');
    getCallback = td.func('pdf.getTemplate');
    previewCallback = td.func('pdf.preview');
    buildCallback = td.func('pdf.build');
    uploadCallback = td.func('s3.upload');
    urlCallback = td.func('s3.getUrl');

    td.replace(Agreement, 'findOrCreate', findCallback);
    td.replace(Agreement, 'getSource', sourceCallback);
    td.replace(pdf, 'getTemplate', getCallback);
    td.replace(pdf, 'preview', previewCallback);
    td.replace(pdf, 'build', buildCallback);
    td.replace(s3, 'upload', uploadCallback);
    td.replace(s3, 'getUrl', urlCallback);

    td.when(pdf.getTemplate(td.matchers.isA(String)))
      .thenReturn('Example template: [FOO]');

    td.when(pdf.preview(td.matchers.isA(String), td.matchers.isA(Object)))
      .thenReturn(IF_OK);

    td.when(pdf.build(td.matchers.isA(String), td.matchers.isA(Object)))
      .thenReturn('FINAL_PDF_BUFFER');

    td.when(Agreement.findOrCreate(td.matchers.isA(Object)))
      .thenResolve([ agreement ]);

    td.when(Agreement.getSource(td.matchers.isA(String)))
      .thenResolve('SOME_HTML_SOURCE');

    td.when(s3.upload(`${key}.html`, IF_OK, guid, td.matchers.isA(Object)))
      .thenResolve('SOME_S3_URL');

    td.when(s3.getUrl('SOME_S3_URL', guid))
      .thenResolve('SOME_S3_URL');
  });

  afterEach(() => {
    td.reset();
  });

  describe('getContract', () => {
    it('should render anything it receives', () => {
      Agreement.getContract(params).should.be.equal(IF_OK);
    });
  });

  describe('findContract', () => {
    it('should findOrCreate a valid agreement', () => {
      return Agreement.findContract(companyId, role)
        .should.be.fulfilled
        .then(result => {
          result.should.be.equal(agreement);
        });
    });
  });

  describe('previewContract', () => {
    it('should render and upload', () => {
      return Agreement.previewContract(agreement, params, guid)
        .then(result => {
          result.should.be.equal('SOME_S3_URL');
        });
    });
  });

  describe('finalizeContract', () => {
    it('should fail if given agreement is not valid', () => {
      return Agreement.finalizeContract(agreement, guid)
        .should.be.rejected
        .then(error => {
          error.message.should.contain('Agreement source is invalid');
        });
    });

    it('should upload the given agreement from its preview', () => {
      const fixedAgreement = {
        agreement_url: 'amazonaws.com/path/to/file.html?x'
      };

      td.when(s3.upload('path/to/file.pdf', 'FINAL_PDF_BUFFER', guid))
        .thenResolve(IF_OK);

      td.when(s3.getUrl(IF_OK, guid))
        .thenResolve('SOME_S3_URL');

      return Agreement.finalizeContract(fixedAgreement, guid)
        .should.be.fulfilled
        .then(result => {
          result.should.be.equal('SOME_S3_URL');
        });
    });
  });
});
