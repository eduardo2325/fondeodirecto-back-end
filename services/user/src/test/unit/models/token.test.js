const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/token');

const TokenModel = require('../../../api/models/Token');
const optionsModel = TokenModel.options;

const { Token, User, Company } = require('../../../models');
const randtoken = require('rand-token');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Token model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('beforeCreate', () => {
    const fixtures = helperFixtures.beforeCreate;
    const { hooks } = optionsModel;
    const { instance, token } = fixtures;

    beforeEach(() => {
      sandbox.stub(randtoken, 'generate').callsFake(() => token);
    });

    it('should return a successful response', () => {
      return hooks.beforeCreate(instance)
        .should.be.fulfilled
        .then((response) => {
          randtoken.generate.calledOnce.should.be.true;
          randtoken.generate.calledWith(16).should.be.true;
          assert.isDefined(response.token);
          response.token.should.be.equal(token);
        });
    });
  });

  describe('userRegistration', () => {
    const fixtures = helperFixtures.userRegistration;
    const { classMethods } = optionsModel;
    const { user, transaction, generatedToken, emptyNameError, emptyTypeError,
      companyRole, emptyCompanyIdError, invalidTypeError, commonError, query } = fixtures;

    beforeEach(() => {
      sandbox.stub(Token, 'verifyEmail').callsFake(() => Promise.resolve(generatedToken));
      sandbox.stub(Token, 'create').callsFake(() => Promise.resolve(generatedToken));
    });

    it('should return error if user name is not defined', () => {
      const clonedUser = _.cloneDeep(user);

      delete clonedUser.name;

      return classMethods.userRegistration(clonedUser, companyRole, transaction)
        .should.be.rejected
        .then((error) => {
          Token.verifyEmail.called.should.be.false;
          error.should.be.deep.equal(emptyNameError);
        });
    });

    it('should return error if user type is not defined', () => {
      const clonedUser = _.cloneDeep(user);

      delete clonedUser.type;

      return classMethods.userRegistration(clonedUser, companyRole, transaction)
        .should.be.rejected
        .then((error) => {
          Token.verifyEmail.called.should.be.false;
          error.should.be.deep.equal(emptyTypeError);
        });
    });

    it('should return error if user companyId is not defined', () => {
      const clonedUser = _.cloneDeep(user);

      delete clonedUser.companyId;

      return classMethods.userRegistration(clonedUser, companyRole, transaction)
        .should.be.rejected
        .then((error) => {
          Token.verifyEmail.called.should.be.false;
          error.should.be.deep.equal(emptyCompanyIdError);
        });
    });

    it('should return error if user type is different than a valid value', () => {
      const clonedUser = _.cloneDeep(user);

      clonedUser.type = 'invalid';

      return classMethods.userRegistration(clonedUser, companyRole, transaction)
        .should.be.rejected
        .then((error) => {
          Token.verifyEmail.called.should.be.false;
          error.should.be.deep.equal(invalidTypeError);
        });
    });

    it('should return error if Token.verifyEmail return a reject', () => {
      Token.verifyEmail.restore();
      sandbox.stub(Token, 'verifyEmail').callsFake(() => Promise.reject(commonError));

      return classMethods.userRegistration(user, companyRole, transaction)
        .should.be.rejected
        .then((error) => {
          Token.verifyEmail.calledOnce.should.be.true;
          Token.verifyEmail.calledWithMatch(query.email).should.be.true;
          Token.create.called.should.be.false;
          error.should.be.deep.equal(commonError);
        });
    });

    it('should return error if Token.create return a reject', () => {
      Token.create.restore();
      sandbox.stub(Token, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.userRegistration(user, companyRole, transaction)
        .should.be.rejected
        .then((error) => {
          Token.verifyEmail.calledOnce.should.be.true;
          Token.verifyEmail.calledWithMatch(query.email).should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(query, { transaction: transaction }).should.be.true;
          error.should.be.deep.equal(commonError);
        });
    });

    it('should return a successful response', () => {
      return classMethods.userRegistration(user, companyRole, transaction)
        .should.be.fulfilled
        .then((response) => {
          Token.verifyEmail.calledOnce.should.be.true;
          Token.verifyEmail.calledWithMatch(query.email).should.be.true;
          Token.create.calledOnce.should.be.true;
          Token.create.calledWithMatch(query, { transaction: transaction }).should.be.true;
          response.should.be.deep.equal(generatedToken);
        });
    });
  });

  describe('getInvitationInfo', () => {
    const fixtures = helperFixtures.getInvitationInfo;
    const { instanceMethods } = optionsModel;
    const { instance, invitationInfo } = fixtures;

    it('should return a successful response', () => {
      const method = instanceMethods.getInvitationInfo.bind(instance);

      return method()
        .should.be.fulfilled
        .then((response) => {
          response.should.be.deep.equal(invitationInfo);
        });
    });
  });

  describe('verifyEmail', () => {
    const fixtures = helperFixtures.verifyEmail;
    const { classMethods } = optionsModel;
    const { email, company, emailQuery, companyIdQuery, objFound, commonError, notUniqueError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(objFound));
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve(objFound));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
    });

    it('should return success if there is no user with that email', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.verifyEmail(email)
        .should.be.fulfilled
        .then(() => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(emailQuery).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(emailQuery).should.be.true;
          Company.findOne.called.should.be.false;
        });
    });

    it('should return error if Company.findOne return a reject', () => {
      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.verifyEmail(email)
        .should.be.rejected
        .then((error) => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(emailQuery).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(emailQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyIdQuery).should.be.true;
          error.should.be.deep.equal(commonError);
        });
    });

    it('should return error if there was a user in the system', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.verifyEmail(email)
        .should.be.rejected
        .then((error) => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(emailQuery).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(emailQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyIdQuery).should.be.true;
          error.should.be.deep.equal(notUniqueError);
        });
    });

    it('should return error if there was a user in the system', () => {
      User.findOne.restore();
      sandbox.stub(User, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.verifyEmail(email)
        .should.be.rejected
        .then((error) => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.calledWithMatch(emailQuery).should.be.true;
          User.findOne.calledOnce.should.be.true;
          User.findOne.calledWithMatch(emailQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyIdQuery).should.be.true;
          error.should.be.deep.equal(notUniqueError);
        });
    });
  });

  describe('recoverPassword', () => {
    const fixtures = helperFixtures.recoverPassword;
    const { classMethods } = optionsModel;
    const { user, destroyQuery, createData, commonError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Token, 'destroy').callsFake(() => Promise.resolve());
      sandbox.stub(Token, 'create').callsFake(() => Promise.resolve());
    });

    it('should an error if there was an issue in the Token.destroy method', () => {
      Token.destroy.restore();
      sandbox.stub(Token, 'destroy').callsFake(() => Promise.reject(commonError));

      return classMethods.recoverPassword(user)
        .should.be.rejected
        .then(() => {
          Token.destroy.calledOnce.should.be.true;
          Token.destroy.args[0].should.be.eql(destroyQuery);
          Token.create.called.should.be.false;
        });
    });

    it('should an error if there was an issue in the Token.create method', () => {
      Token.create.restore();
      sandbox.stub(Token, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.recoverPassword(user)
        .should.be.rejected
        .then(() => {
          Token.destroy.calledOnce.should.be.true;
          Token.destroy.args[0].should.be.eql(destroyQuery);
          Token.create.calledOnce.should.be.true;
          Token.create.args[0].should.be.eql(createData);
        });
    });

    it('should return success if there is no issue creating the token', () => {
      return classMethods.recoverPassword(user)
        .should.be.fulfilled
        .then(() => {
          Token.destroy.calledOnce.should.be.true;
          Token.destroy.args[0].should.be.eql(destroyQuery);
          Token.create.calledOnce.should.be.true;
          Token.create.args[0].should.be.eql(createData);
        });
    });
  });

  describe('validateRecoverToken', () => {
    const fixtures = helperFixtures.validateRecoverToken;
    const { classMethods } = optionsModel;
    const { recoverToken, expiredRecoverToken, query, commonError, notFoundError } = fixtures;

    beforeEach(() => {
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(recoverToken));
    });

    it('should an error if there was an issue in the Token.findOne method', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.reject(commonError));

      return classMethods.validateRecoverToken(recoverToken.token)
        .should.be.rejected
        .then(error => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql(query);
          error.should.be.deep.eql(commonError);
        });
    });

    it('should an error if Token.findOne did not find a token', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve());

      return classMethods.validateRecoverToken(recoverToken.token)
        .should.be.rejected
        .then(error => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql(query);
          error.should.be.deep.eql(notFoundError);
        });
    });

    it('should an error if Token.findOne returned an expired token', () => {
      Token.findOne.restore();
      sandbox.stub(Token, 'findOne').callsFake(() => Promise.resolve(expiredRecoverToken));

      return classMethods.validateRecoverToken(recoverToken.token)
        .should.be.rejected
        .then(error => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql(query);
          error.should.be.deep.eql(notFoundError);
        });
    });

    it('should return success if there is no issue with the token', () => {
      return classMethods.validateRecoverToken(recoverToken.token)
        .should.be.fulfilled
        .then(response => {
          Token.findOne.calledOnce.should.be.true;
          Token.findOne.args[0].should.be.eql(query);
          response.should.be.deep.eql(recoverToken);
        });
    });
  });
});
