const chai = require('chai');
const _ = require('lodash');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const helperFixtures = require('../fixtures/user_activity');

const UserActivityModel = require('../../../api/models/UserActivity');
const optionsModel = UserActivityModel.options;

const { UserActivity, sequelize } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/UserActivity model', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('registerDeleteInvoices', () => {
    const fixtures = helperFixtures.registerDeleteInvoices;
    const { classMethods } = optionsModel;
    const { commonError, createParamsData,
      options, activityData, rawQuery, rawQueryOptions, invoices, activity
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(UserActivity, 'create').callsFake(() => Promise.resolve(activity));
      sandbox.stub(sequelize, 'query').callsFake(() => Promise.resolve(true));
    });

    it('should return an error if UserActivity.create rejects', () => {
      UserActivity.create.restore();
      sandbox.stub(UserActivity, 'create').callsFake(() => Promise.reject(commonError));

      return classMethods.registerDeleteInvoices(activityData, invoices, options)
        .should.be.rejected
        .then((error) => {
          UserActivity.create.called.should.be.true;
          const args = _.pick(UserActivity.create.args[0][0], [ 'triggered_by', 'action' ]);

          args.should.be.eql(createParamsData[0]);
          sequelize.query.called.should.be.false;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return an error if sequelize.query rejects', () => {
      sequelize.query.restore();
      sandbox.stub(sequelize, 'query').rejects(commonError);

      return classMethods.registerDeleteInvoices(activityData, invoices, options)
        .should.be.rejected
        .then((error) => {
          const args = _.pick(UserActivity.create.args[0][0], [ 'triggered_by', 'action' ]);

          args.should.be.eql(createParamsData[0]);
          sequelize.query.called.should.be.true;
          sequelize.query.calledWith(rawQuery, rawQueryOptions).should.be.true;
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a succesful response', () => {
      return classMethods.registerDeleteInvoices(activityData, invoices, options)
        .should.be.fulfilled
        .then((value) => {
          const args = _.pick(UserActivity.create.args[0][0], [ 'triggered_by', 'action' ]);

          args.should.be.eql(createParamsData[0]);
          sequelize.query.called.should.be.true;
          sequelize.query.calledWith(rawQuery, rawQueryOptions).should.be.true;
          assert.deepEqual(value, true);
        });
    });
  });
});
