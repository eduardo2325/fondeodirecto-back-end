const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../../api/helpers/statement-queries');
const helperFixtures = require('../fixtures/helpers/statement-queries');
const { sequelize } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Statement queries helper', () => {
  describe('unit/Statement database queries', () => {
    const id = helperFixtures.id;
    const year = helperFixtures.year;
    const month = helperFixtures.month;

    afterEach(() => {
      sandbox.restore();
    });

    describe('purchasedInvoices', () => {
      const fixtures = helperFixtures.purchasedInvoices;
      const { query, result } = fixtures;

      beforeEach(() => {
        sandbox.stub(sequelize, 'query').callsFake(() => result);
      });

      it('should return an error if sequelize.query fails', (done) => {
        const error = new Error('sequelize error');

        sequelize.query.restore();
        sandbox.stub(sequelize, 'query').callsFake(() => {
          throw error;
        });

        try {
          helper.purchasedInvoices(id, year, month);
        } catch (e) {
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          e.should.be.eql(error);
          done();
        }
      });

      it('should return data', (done) => {
        try {
          helper.purchasedInvoices(id, year, month).should.be.eql(result);
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          done();
        } catch (e) {
          done(e);
        }
      });
    });

    describe('investorCashWithDrawals', () => {
      const fixtures = helperFixtures.investorCashWithDrawals;
      const { query, result } = fixtures;

      beforeEach(() => {
        sandbox.stub(sequelize, 'query').callsFake(() => result);
      });

      it('should return an error if sequelize.query fails', (done) => {
        const error = new Error('sequelize error');

        sequelize.query.restore();
        sandbox.stub(sequelize, 'query').callsFake(() => {
          throw error;
        });

        try {
          helper.investorCashWithDrawals(id, year, month);
        } catch (e) {
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          e.should.be.eql(error);
          done();
        }
      });

      it('should return data', (done) => {
        try {
          helper.investorCashWithDrawals(id, year, month).should.be.eql(result);
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          done();
        } catch (e) {
          done(e);
        }
      });
    });

    describe('investorCashDeposits', () => {
      const fixtures = helperFixtures.investorCashDeposits;
      const { query, result } = fixtures;

      beforeEach(() => {
        sandbox.stub(sequelize, 'query').callsFake(() => result);
      });

      it('should return an error if sequelize.query fails', (done) => {
        const error = new Error('sequelize error');

        sequelize.query.restore();
        sandbox.stub(sequelize, 'query').callsFake(() => {
          throw error;
        });

        try {
          helper.investorCashDeposits(id, year, month);
        } catch (e) {
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          e.should.be.eql(error);
          done();
        }
      });

      it('should return data', (done) => {
        try {
          helper.investorCashDeposits(id, year, month).should.be.eql(result);
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          done();
        } catch (e) {
          done(e);
        }
      });
    });

    describe('payedInvoices', () => {
      const fixtures = helperFixtures.payedInvoices;
      const { query, result } = fixtures;

      beforeEach(() => {
        sandbox.stub(sequelize, 'query').callsFake(() => result);
      });

      it('should return an error if sequelize.query fails', (done) => {
        const error = new Error('sequelize error');

        sequelize.query.restore();
        sandbox.stub(sequelize, 'query').callsFake(() => {
          throw error;
        });

        try {
          helper.payedInvoices(id, year, month);
        } catch (e) {
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          e.should.be.eql(error);
          done();
        }
      });

      it('should return data', (done) => {
        try {
          helper.payedInvoices(id, year, month).should.be.eql(result);
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          done();
        } catch (e) {
          done(e);
        }
      });
    });


    describe('investorCashDepositsFromInvoices', () => {
      const fixtures = helperFixtures.investorCashDepositsFromInvoices;
      const { query, result } = fixtures;

      beforeEach(() => {
        sandbox.stub(sequelize, 'query').callsFake(() => result);
      });

      it('should return an error if sequelize.query fails', (done) => {
        const error = new Error('sequelize error');

        sequelize.query.restore();
        sandbox.stub(sequelize, 'query').callsFake(() => {
          throw error;
        });

        try {
          helper.investorCashDepositsFromInvoices(id, year, month);
        } catch (e) {
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          e.should.be.eql(error);
          done();
        }
      });

      it('should return data', (done) => {
        try {
          helper.investorCashDepositsFromInvoices(id, year, month).should.be.eql(result);
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          done();
        } catch (e) {
          done(e);
        }
      });
    });


    describe('investorCashWithDrawalsFromInvoices', () => {
      const fixtures = helperFixtures.investorCashWithDrawalsFromInvoices;
      const { query, result } = fixtures;

      beforeEach(() => {
        sandbox.stub(sequelize, 'query').callsFake(() => result);
      });

      it('should return an error if sequelize.query fails', (done) => {
        const error = new Error('sequelize error');

        sequelize.query.restore();
        sandbox.stub(sequelize, 'query').callsFake(() => {
          throw error;
        });

        try {
          helper.investorCashWithDrawalsFromInvoices(id, year, month);
        } catch (e) {
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          e.should.be.eql(error);
          done();
        }
      });

      it('should return data', (done) => {
        try {
          helper.investorCashWithDrawalsFromInvoices(id, year, month).should.be.eql(result);
          sequelize.query.calledOnce.should.be.true;
          sequelize.query.calledWithMatch(query).should.be.true;
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });

  describe('unit/Statement helper methods', () => {
    describe('formatData', () => {
      const fixtures = helperFixtures.formatData;
      const { data, result } = fixtures;

      it('should format data correctly', () => {
        helper.formatData([ data ]).should.be.eql(result);
      });
    });

    describe('formStatementResponse', () => {
      const fixtures = helperFixtures.formStatementResponse;
      const { queriesResult, result } = fixtures;

      it('should form statement response correctly', () => {
        helper.formStatementResponse(queriesResult).should.be.eql(result);
      });
    });
  });
});
