const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../../api/helpers/shopping-cart');
const helperFixtures = require('../fixtures/helpers/shopping-cart');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/ShoppingCart helper', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('getOnlyAvailableInvoices', () => {
    const fixtures = helperFixtures.getOnlyAvailableInvoices;

    it('should return a successful response with only available invoices', () => {
      const { request, onlyAvailable } = fixtures.availableInvoices;

      helper.getOnlyAvailableInvoices(request).should.be.eql(onlyAvailable);
    });

    it('should return a successful response with mixed available and unavailable invoices', () => {
      const { request, mixedAvailable } = fixtures.mixedInvoices;

      helper.getOnlyAvailableInvoices(request).should.be.eql(mixedAvailable);
    });

    it('should return a successful response with only unavailable invoices', () => {
      const { request, onlyUnavailable } = fixtures.unavailableInvoices;

      helper.getOnlyAvailableInvoices(request).should.be.eql(onlyUnavailable);
    });
  });

  describe('getAvailableInvoicesSummary', () => {
    const fixtures = helperFixtures.getAvailableInvoicesSummary;
    const { invoices, summary } = fixtures;

    it('should return a invoices summary successfully', () => {
      helper.getAvailableInvoicesSummary(invoices).should.be.eql(summary);
    });
  });

  describe('getCartResponse', () => {
    const fixtures = helperFixtures.getCartResponse;
    const { invoices, response } = fixtures;

    it('should return a invoices summary successfully', () => {
      helper.getCartResponse(invoices).should.be.eql(response);
    });
  });

  describe('getPublishSummaryTotals', () => {
    const fixtures = helperFixtures.getPublishSummaryTotals;
    const { invoices, response } = fixtures;

    it('should return a invoices summary successfully', () => {
      helper.getPublishSummaryTotals(invoices).should.be.eql(response);
    });
  });
});
