const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const helper = require('../../../api/helpers/invoice-queries');
const helperFixtures = require('../fixtures/helpers/invoice-queries');
const { sequelize } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Invoice queries helper', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('daysDiffQuery', () => {
    const fixtures = helperFixtures.daysDiffQuery;
    const { startDateColumn, endDateColumn, query, result } = fixtures;

    beforeEach(() => {
      sandbox.stub(sequelize, 'literal').callsFake(() => result);
    });

    it('should return an error if sequelize.literal fails', (done) => {
      const error = new Error('sequelize error');

      sequelize.literal.restore();
      sandbox.stub(sequelize, 'literal').callsFake(() => {
        throw error;
      });

      try {
        helper.daysDiffQuery(endDateColumn, startDateColumn);
      } catch (e) {
        sequelize.literal.calledOnce.should.be.true;
        sequelize.literal.calledWithMatch(query).should.be.true;
        e.should.be.eql(error);
        done();
      }
    });

    it('should return query', (done) => {
      try {
        helper.daysDiffQuery(endDateColumn, startDateColumn).should.be.eql(result);
        sequelize.literal.calledOnce.should.be.true;
        sequelize.literal.calledWithMatch(query).should.be.true;
        done();
      } catch (e) {
        done(e);
      }
    });
  });

  describe('gainEstimateQuery', () => {
    const fixtures = helperFixtures.gainEstimateQuery;
    const { fundDateColumn, gainEstimateQueryString, query, result } = fixtures;

    beforeEach(() => {
      sandbox.stub(sequelize, 'literal').callsFake(() => result);
      sandbox.stub(helper, 'gainEstimateQueryString').callsFake(() => gainEstimateQueryString);
    });

    it('should return an error if gainEstimateQueryString fails', (done) => {
      const error = new Error('error');

      helper.gainEstimateQueryString.restore();
      sandbox.stub(helper, 'gainEstimateQueryString').callsFake(() => {
        throw error;
      });

      try {
        helper.gainEstimateQuery(fundDateColumn);
        done();
      } catch (e) {
        sequelize.literal.called.should.be.false;
        helper.gainEstimateQueryString.calledOnce.should.be.true;
        helper.gainEstimateQueryString.calledWithMatch(fundDateColumn).should.be.true;
        e.should.be.eql(error);
        done();
      }
    });

    it('should return an error if sequelize.literal fails', (done) => {
      const error = new Error('sequelize error');

      sequelize.literal.restore();
      sandbox.stub(sequelize, 'literal').callsFake(() => {
        throw error;
      });

      try {
        helper.gainEstimateQuery(fundDateColumn);
      } catch (e) {
        sequelize.literal.calledOnce.should.be.true;
        sequelize.literal.calledWithMatch(query).should.be.true;
        e.should.be.eql(error);
        done();
      }
    });

    it('should return query', (done) => {
      try {
        helper.gainEstimateQuery(fundDateColumn).should.be.eql(result);
        sequelize.literal.calledOnce.should.be.true;
        sequelize.literal.calledWithMatch(query).should.be.true;
        done();
      } catch (e) {
        done(e);
      }
    });
  });

  describe('gainEstimatePercentageQuery', () => {
    const fixtures = helperFixtures.gainEstimatePercentageQuery;
    const { fundDateColumn, query, gainEstimateResult, result } = fixtures;

    beforeEach(() => {
      sandbox.stub(sequelize, 'literal').callsFake(() => result);
      sandbox.stub(helper, 'gainEstimateQueryString').callsFake(() => gainEstimateResult);
    });

    it('should return an error if gainEstimateQueryString fails', (done) => {
      const error = new Error('error');

      helper.gainEstimateQueryString.restore();
      sandbox.stub(helper, 'gainEstimateQueryString').callsFake(() => {
        throw error;
      });

      try {
        helper.gainEstimatePercentageQuery(fundDateColumn);
        done();
      } catch (e) {
        sequelize.literal.called.should.be.false;
        helper.gainEstimateQueryString.calledOnce.should.be.true;
        helper.gainEstimateQueryString.calledWithMatch(fundDateColumn).should.be.true;
        e.should.be.eql(error);
        done();
      }
    });

    it('should return an error if sequelize.literal fails', (done) => {
      const error = new Error('sequelize error');

      sequelize.literal.restore();
      sandbox.stub(sequelize, 'literal').callsFake(() => {
        throw error;
      });

      try {
        helper.gainEstimatePercentageQuery(fundDateColumn);
      } catch (e) {
        sequelize.literal.calledOnce.should.be.true;
        sequelize.literal.calledWithMatch(query).should.be.true;
        helper.gainEstimateQueryString.calledOnce.should.be.true;
        helper.gainEstimateQueryString.calledWithMatch(fundDateColumn).should.be.true;
        e.should.be.eql(error);
        done();
      }
    });

    it('should return query', (done) => {
      try {
        helper.gainEstimatePercentageQuery(fundDateColumn).should.be.eql(result);
        sequelize.literal.calledOnce.should.be.true;
        sequelize.literal.calledWithMatch(query).should.be.true;
        helper.gainEstimateQueryString.calledOnce.should.be.true;
        helper.gainEstimateQueryString.calledWithMatch(fundDateColumn).should.be.true;
        done();
      } catch (e) {
        done(e);
      }
    });
  });
});
