const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const log = require('/var/lib/core/js/log').prototype;
const helper = require('../../../api/helpers/error');
const helperFixtures = require('../fixtures/helpers/error');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Error helper', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('format', () => {
    const fixtures = helperFixtures.format;
    const { err, errMessage, errorFormatted, guid } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'error').callsFake(() => true);
    });

    it('should return a successful response', () => {
      const callback = sandbox.spy();

      return helper.format(errMessage, guid, callback)
        .should.be.fulfilled
        .then(() => {
          log.error.calledOnce.should.be.true;
          log.error.calledWithMatch(errMessage, guid).should.be.true;
          callback.calledOnce.should.be.true;
          errorFormatted.should.be.eql(callback.getCall(0).args[0].message);
        });
    });

    it('should return a successful response with sequelize error', () => {
      const callback = sandbox.spy();

      return helper.format(err, guid, callback)
        .should.be.fulfilled
        .then(() => {
          log.error.calledOnce.should.be.true;
          log.error.calledWithMatch(err, guid).should.be.true;
          callback.calledOnce.should.be.true;
          JSON.parse(errorFormatted).message.should.be.eql(JSON.parse(callback.getCall(0).args[0].message).message);
        });
    });
  });
});
