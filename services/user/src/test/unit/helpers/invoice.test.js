const InvoiceHelper = require('../../../api/helpers/invoice');
const helperFixtures = require('../fixtures/helpers/invoice');
const _ = require('lodash');
const chai = require('chai');

chai.should();
chai.use(require('chai-as-promised'));

const { invoice, invoiceWithOperation } = helperFixtures;

describe('user/unit/Invoice helper', () => {
  const { instance } = helperFixtures;

  describe('constructor', () => {
    it('should work with invoices', () => {
      const helper = new InvoiceHelper(invoice);

      helper.invoice.should.be.deep.equal(instance.invoice);

      Number(helper.today).should.be.equal(Number(instance.today));
      Number(helper.fundDay).should.be.equal(Number(instance.fundDay));
      Number(helper.expiration).should.be.equal(Number(instance.expiration));
    });

    it('should work with operations', () => {
      const helper = new InvoiceHelper(invoiceWithOperation);

      helper.invoice.should.be.deep.equal(instance.invoice);

      helper.operation.should.be.deep.equal(instance.operation);

      Number(helper.today).should.be.equal(Number(instance.today));
      Number(helper.fundDay).should.be.equal(Number(instance.fundDay));
      Number(helper.expiration).should.be.equal(Number(instance.expiration));
    });
  });

  describe('new formulas', () => {
    it('should apply the new formula for CXC', () => {
      const { input, result } = helperFixtures.newInvoice;

      const helper = new InvoiceHelper(input);
      const output = helper.getAdminCxcPaymentEstimates();

      output.should.be.deep.equal(result);
    });
  });

  const { today } = helperFixtures.daysDiffCalculation;

  function makeHelper(extraParams) {
    // reassign this.today to a well-known date
    const helper = new InvoiceHelper(invoiceWithOperation, extraParams);

    helper.today = today;
    return helper;
  }

  function makeHelperDifferentInvoice(invoiceInstance, percentages, global_fee_percentage) {
    const helper = new InvoiceHelper(invoiceInstance, { percentages, global_fee_percentage });

    return helper;
  }

  describe('basic math operations', () => {
    const { percentages, operationCost } = helperFixtures.daysDiffCalculation;
    const helper = makeHelper({ percentages, operationCost });

    it('getStartAndExpirationDayDifference', () => {
      const { daysDiff, daysDiffFund } = helperFixtures.daysDiffCalculation;

      helper.getStartAndExpirationDayDifference().should.be.equal(daysDiff);
      helper.getStartAndExpirationDayDifference(true).should.be.equal(daysDiffFund);
    });

    it('getEstimateInterestCalculation', () => {
      const { estimateInterest, invoiceDuration } = helperFixtures.interestCalculation;

      helper.getEstimateInterestCalculation(invoiceDuration).should.be.equal(estimateInterest);
    });

    it('getInterestCalculation', () => {
      const { interest, invoiceTotal, annualCost, invoiceDuration } = helperFixtures.interestCalculation;

      helper.getInterestCalculation(invoiceTotal, annualCost, invoiceDuration).should.be.equal(interest);
    });

    it('getFDComissionAmount', () => {
      const { comission, invoiceTotal, fdComissionPercentage, invoiceDuration } = helperFixtures.fdComissionCalculation;

      helper.getFDComissionAmount(invoiceTotal, fdComissionPercentage, invoiceDuration).should.be.equal(comission);
    });

    it('getTotalOperationCost', () => {
      const {
        diffDays,
        operationCost: fixedOperationCost
      } = helperFixtures.operationCalculation;

      helper.getTotalOperationCost(diffDays).should.be.equal(fixedOperationCost);
    });

    it('getInvoicePaymentEstimates', () => {
      const estimates = helperFixtures.paymentEstimates;

      const n1 = helper.getInvoicePaymentEstimates(1);
      const n10 = helper.getInvoicePaymentEstimates(10);
      const n100 = helper.getInvoicePaymentEstimates(100);

      n1.fundPayment.should.be.equal(estimates.n1);
      n10.fundPayment.should.be.equal(estimates.n10);
      n100.fundPayment.should.be.equal(estimates.n100);
    });

    it('getFundEstimates', () => {
      const { fundEstimates: result } = helperFixtures.results;

      helper.getFundEstimates(3).should.be.deep.equal(result);
    });

    it('getInvestorProfitEstimates', () => {
      const { investorProfitEstimates: result } = helperFixtures.results;

      helper.getInvestorProfitEstimates(3).should.be.deep.equal(result);
    });
  });

  describe('get investor fee cases', () => {
    const { investorInvoice, percentages, global_fee_percentage } = helperFixtures;
    const helper = new InvoiceHelper(investorInvoice, { percentages, global_fee_percentage });

    it('FD variable_fee_percentage is not defined', () => {
      const { earnings, fee } = helperFixtures.investorFeeNoVariableFeeCase;
      const percentagesWithoutFee = _.cloneDeep(percentages);

      delete percentagesWithoutFee.variable_fee_percentage;

      makeHelperDifferentInvoice(investorInvoice, percentagesWithoutFee, global_fee_percentage)
        .getInvestorFee(earnings).should.be.deep.equal(fee);
    });

    it('should return a 0 fee if variable_fee_percentage and fee are not defined', () => {
      const { earnings, fee } = helperFixtures.investorNoFeeOrVariableFee;

      makeHelperDifferentInvoice(investorInvoice, {}, global_fee_percentage)
        .getInvestorFee(earnings).should.be.deep.equal(fee);
    });

    it('FD comission is greater than 33.3%', () => {
      const { earnings, fee } = helperFixtures.investorFeeFirstCase;

      helper.getInvestorFee(earnings).should.be.deep.equal(fee);
    });
    it('FD comission is less than 33.3% and variable_fee_percentage earnings < fixed fee percentage earnings', () => {
      const { earnings, fee } = helperFixtures.investorFeeSecondCase;

      helper.getInvestorFee(earnings).should.be.deep.equal(fee);
    });
    it('FD comission is less than 33.3% and variable_fee_percentage earnings > fixed fee percentage earnings', () => {
      const { earnings, fee } = helperFixtures.investorFeeThirdCase;

      helper.getInvestorFee(earnings).should.be.deep.equal(fee);
    });
  });

  describe('investor fideicomiso cases', () => {
    const { investorInvoice, global_fee_percentage } = helperFixtures;
    const { percentages } = helperFixtures.results.investorFideicomisoFee;
    const helper = new InvoiceHelper(investorInvoice, { percentages, global_fee_percentage });

    it('should return a fideicomiso fixed fee if fondeo fee is fixed', () => {
      const { earnings, fondeoFee, result } = helperFixtures.results.investorFideicomisoFee.fixedFee;

      helper.getInvestorFideicomisoFee(earnings, fondeoFee).should.be.deep.equal(result);
    });

    it('should return a fideicomiso variable fee if fondeo fee is variable', () => {
      const { earnings, fondeoFee, result } = helperFixtures.results.investorFideicomisoFee.variableFee;

      helper.getInvestorFideicomisoFee(earnings, fondeoFee).should.be.deep.equal(result);
    });
  });

  describe('transaction estimates', () => {
    it('getInvestorTransactionEstimates', () => {
      const { investorInvoice, percentages, global_fee_percentage,
        investorTransactionEstimate } = helperFixtures;
      const helper = new InvoiceHelper(investorInvoice, { percentages, global_fee_percentage });

      helper.getInvestorTransactionEstimates(3).should.be.deep.equal(investorTransactionEstimate);
    });

    it('getInvestorTransactionEstimates for a fideicomiso investor', () => {
      const { investorInvoice, global_fee_percentage, fideicomisoTransactionEstimate } = helperFixtures;
      const { percentages } = helperFixtures.results.investorFideicomisoFee;
      const helper = new InvoiceHelper(investorInvoice, { percentages, global_fee_percentage });

      helper.getInvestorTransactionEstimates(3).should.be.deep.equal(fideicomisoTransactionEstimate);
    });
  });

  describe('special cases', () => {
    [ 'withoutTaxpayer', 'withTaxpayer' ].forEach(method => {
      describe(method, () => {
        it('getInvestorGains', () => {
          const { taxpayerType, percentages, global_fee_percentage,
            result } = helperFixtures.results.investorGains[method];

          makeHelper({ percentages, global_fee_percentage, taxpayerType }).getInvestorGains(3).should.be.deep
            .equal(result);
        });

        it('getInvestorFundEstimates', () => {
          const { taxpayerType, percentages, global_fee_percentage,
            result } = helperFixtures.results.investorFundEstimates[method];

          makeHelper({ percentages, global_fee_percentage, taxpayerType }).getInvestorFundEstimates(3)
            .should.be.deep.equal(result);
        });

        it('getAdminInvestorPaymentEstimates', () => {
          const { taxpayerType, percentages, global_fee_percentage,
            result } = helperFixtures.results.adminInvestorPaymentEstimates[method];

          makeHelper({ percentages, global_fee_percentage, taxpayerType })
            .getAdminInvestorPaymentEstimates(3).should.be.deep.equal(result);
        });
      });
    });

    describe('notFunded', () => {
      const { input } = helperFixtures.newInvoice;
      const helper = new InvoiceHelper(input);

      delete helper.fundDay;

      it('getAdminOperationEstimates', () => {
        const { result } = helperFixtures.results.adminOperationEstimates.notFunded;

        helper.getAdminOperationEstimates().should.be.deep.equal(result);
      });

      it('getAdminCxcPaymentEstimates', () => {
        const { result } = helperFixtures.results.adminCxcPaymentEstimates.notFunded;

        helper.getAdminCxcPaymentEstimates().should.be.deep.equal(result);
      });
    });

    describe('funded', () => {
      it('getAdminOperationEstimates', () => {
        const { result, percentages, global_fee_percentage }
          = helperFixtures.results.adminOperationEstimates.funded;

        makeHelper({ percentages, global_fee_percentage })
          .getAdminOperationEstimates().should.be.deep.equal(result);
      });

      it('getAdminOperationEstimates on invoice funded by fideicomiso', () => {
        const { result, percentages, global_fee_percentage }
          = helperFixtures.results.adminOperationEstimates.fundedByFideicomiso;

        makeHelper({ percentages, global_fee_percentage })
          .getAdminOperationEstimates().should.be.deep.equal(result);
      });

      it('getAdminCxcPaymentEstimates', () => {
        const { result, percentages, global_fee_percentage }
          = helperFixtures.results.adminCxcPaymentEstimates.funded;

        makeHelper({ percentages, global_fee_percentage })
          .getAdminCxcPaymentEstimates().should.be.deep.equal(result);
      });
    });
  });

  describe('getInvoiceAmounts', () => {
    const { shoppingCartInvoice, global_fee_percentage } = helperFixtures;
    const { percentages } = helperFixtures.results.getInvoiceAmounts;
    const helper = new InvoiceHelper(shoppingCartInvoice, { percentages, global_fee_percentage });

    it('should return a fideicomiso fixed fee if fondeo fee is fixed', () => {
      const { result } = helperFixtures.results.getInvoiceAmounts;

      helper.getInvoiceAmounts(60).should.be.deep.equal(result);
    });
  });
});

