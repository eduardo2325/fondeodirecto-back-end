const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const sandbox = sinon.sandbox.create();
const _ = require('lodash');
const helper = require('../../../api/helpers/state-machine');
const helperFixtures = require('../fixtures/helpers/state-machine');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/StateMachine helper', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('validateStatus', () => {
    const fixtures = helperFixtures.validateStatus;
    const {
      invoice, approved, rejected, published, fundRequested, funded, instancesPublished,
      paymentInProcess, completed, invalidInvoiceError, invalidOperationError, invalidInvestorTransactionError,
      baseStatus, instancesWithInvoice, instancesWithOperation, instancesWithInvestorTransaction
    } = fixtures;

    it('should return an error if the invoice status is not definied', () => {
      return helper.validateStatus(instancesWithInvoice, baseStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidInvoiceError);
        });
    });

    it('should return an error if the operation status is not definied', () => {
      const invalidOperationStatus = _.cloneDeep(baseStatus);

      invalidOperationStatus.invoiceStatus = published;

      return helper.validateStatus(instancesWithOperation, invalidOperationStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidOperationError);
        });
    });

    it('should return an error if the investor transaction status is not definied', () => {
      const invalidInvestorTransactionStatus = _.cloneDeep(baseStatus);

      invalidInvestorTransactionStatus.invoiceStatus = fundRequested;
      invalidInvestorTransactionStatus.operationStatus = fundRequested;

      return helper.validateStatus(instancesWithInvestorTransaction, invalidInvestorTransactionStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidInvestorTransactionError);
        });
    });

    it('should return a success if the validations were completed (approved)', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = approved;

      return helper.validateStatus(instancesWithInvoice, validInvoiceStatus)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, instancesWithInvoice);
        });
    });

    it('should return a success if the validations were completed (rejected)', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = rejected;

      return helper.validateStatus(instancesWithInvoice, validInvoiceStatus)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, instancesWithInvoice);
        });
    });

    it('should return an error on published rule invoice expiration date is lesser than today', () => {
      const instancesIncompleteInvoice = _.cloneDeep(instancesWithInvoice);
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      delete instancesIncompleteInvoice.invoice.expiration;
      instancesIncompleteInvoice.invoice.expiration = new Date();
      validInvoiceStatus.invoiceStatus = published;

      return helper.validateStatus(instancesIncompleteInvoice, validInvoiceStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidInvoiceError);
        });
    });

    it('should return a success if the validations were completed (published)', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = published;

      return helper.validateStatus(instancesPublished, validInvoiceStatus)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, instancesPublished);
        });
    });

    it('should return an error on fund requested rule invoice doesn\'t have fund_date', () => {
      const instancesIncompleteInvoice = _.cloneDeep(instancesWithInvoice);
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      delete instancesIncompleteInvoice.invoice.fund_date;
      validInvoiceStatus.invoiceStatus = fundRequested;

      return helper.validateStatus(instancesIncompleteInvoice, validInvoiceStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidInvoiceError);
        });
    });

    it('should return an error on fund requested rule invoice doesn\'t have an Operation', () => {
      const instancesIncompleteInvoice = _.cloneDeep(instancesWithInvoice);
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      delete instancesIncompleteInvoice.invoice.fund_date;
      validInvoiceStatus.invoiceStatus = fundRequested;

      return helper.validateStatus(instancesIncompleteInvoice, validInvoiceStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidInvoiceError);
        });
    });

    it('should return an error on fund requested rule invoice doesn\'t have investor_company_id', () => {
      const instancesIncompleteInvoice = _.cloneDeep(instancesWithInvoice);
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      delete instancesIncompleteInvoice.invoice.investor_company_id;
      validInvoiceStatus.invoiceStatus = fundRequested;

      return helper.validateStatus(instancesIncompleteInvoice, validInvoiceStatus)
        .should.be.rejected
        .then((error) => {
          assert.deepEqual(error, invalidInvoiceError);
        });
    });


    it('should return a success if the validations were completed (fund_requested)', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = fundRequested;
      validInvoiceStatus.operationStatus = fundRequested;

      return helper.validateStatus(instancesWithOperation, validInvoiceStatus)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, instancesWithOperation);
        });
    });

    it('should return a success (funded)', () => {
      return helper.validateStatus(invoice, funded)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, invoice);
        });
    });

    it('should return a success (payment_in_process)', () => {
      return helper.validateStatus(invoice, paymentInProcess)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, invoice);
        });
    });

    it('should return a success (completed)', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = completed;
      validInvoiceStatus.operationStatus = completed;

      return helper.validateStatus(instancesWithOperation, validInvoiceStatus)
        .should.be.fulfilled
        .then((result) => {
          assert.deepEqual(result, instancesWithOperation);
        });
    });
  });

  describe('next', () => {
    const fixtures = helperFixtures.next;
    const { invoice, approved, baseStatus, commonError, operation, investorTransaction, fundRequested,
      instancesWithInvoice, instancesWithOperation, instancesWithTransaction, completed } = fixtures;

    beforeEach(() => {
      sandbox.stub(helper, 'validateStatus').callsFake(() => Promise.resolve());
      sandbox.stub(instancesWithInvoice.invoice, 'save').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(instancesWithOperation.operation, 'save').callsFake(() => Promise.resolve(operation));
      sandbox.stub(instancesWithTransaction.investorTransaction, 'save').callsFake(() =>
        Promise.resolve(investorTransaction));
    });

    it('should return an error if theere was an issue validating ', () => {
      helper.validateStatus.restore();
      sandbox.stub(helper, 'validateStatus').callsFake(() => Promise.reject(commonError));

      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = approved;

      return helper.next.call(helper, instancesWithInvoice.invoice)
        .should.be.rejected
        .then((error) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithInvoice, validInvoiceStatus ]);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if the invoice validations were completed', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = approved;

      return helper.next.call(helper, instancesWithInvoice.invoice)
        .should.be.fulfilled
        .then((result) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithInvoice, validInvoiceStatus ]);
          assert.deepEqual(result, [ invoice ]);
        });
    });

    it('should return a success if the operation validations were completed', () => {
      const validOperationStatus = _.cloneDeep(baseStatus);

      validOperationStatus.operationStatus = fundRequested;

      return helper.next.call(helper, null, instancesWithOperation.operation)
        .should.be.fulfilled
        .then((result) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithOperation, validOperationStatus ]);
          assert.deepEqual(result, [ operation ]);
        });
    });

    it('should return a success if the investorTransaction validations were completed', () => {
      const validInvestorTransactionStatus = _.cloneDeep(baseStatus);

      validInvestorTransactionStatus.investorTransactionStatus = completed;

      return helper.next.call(helper, null, null, instancesWithTransaction.investorTransaction)
        .should.be.fulfilled
        .then((result) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithTransaction, validInvestorTransactionStatus ]);
          assert.deepEqual(result, [ investorTransaction ]);
        });
    });
  });

  describe('back', () => {
    const fixtures = helperFixtures.back;
    const { invoice, pending, baseStatus, commonError, operation, investorTransaction, canceled,
      instancesWithInvoice, instancesWithOperation, instancesWithTransaction, rejected } = fixtures;

    beforeEach(() => {
      sandbox.stub(helper, 'validateStatus').callsFake(() => Promise.resolve());
      sandbox.stub(instancesWithInvoice.invoice, 'save').callsFake(() => Promise.resolve(invoice));
      sandbox.stub(instancesWithOperation.operation, 'save').callsFake(() => Promise.resolve(operation));
      sandbox.stub(instancesWithTransaction.investorTransaction, 'save').callsFake(() =>
        Promise.resolve(investorTransaction));
    });

    it('should return an error if theere was an issue validating ', () => {
      helper.validateStatus.restore();
      sandbox.stub(helper, 'validateStatus').callsFake(() => Promise.reject(commonError));

      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = rejected;

      return helper.back.call(helper, instancesWithInvoice.invoice)
        .should.be.rejected
        .then((error) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithInvoice, validInvoiceStatus ]);
          assert.deepEqual(error, commonError);
        });
    });

    it('should return a success if the invoice validations were completed', () => {
      const validInvoiceStatus = _.cloneDeep(baseStatus);

      validInvoiceStatus.invoiceStatus = rejected;

      return helper.back.call(helper, instancesWithInvoice.invoice)
        .should.be.fulfilled
        .then((result) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithInvoice, validInvoiceStatus ]);
          assert.deepEqual(result, [ invoice ]);
        });
    });

    it('should return a success if the operation validations were completed', () => {
      const validOperationStatus = _.cloneDeep(baseStatus);

      validOperationStatus.operationStatus = pending;

      return helper.back.call(helper, null, instancesWithOperation.operation)
        .should.be.fulfilled
        .then((result) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithOperation, validOperationStatus ]);
          assert.deepEqual(result, [ operation ]);
        });
    });

    it('should return a success if the investorTransaction validations were completed', () => {
      const validInvestorTransactionStatus = _.cloneDeep(baseStatus);

      validInvestorTransactionStatus.investorTransactionStatus = canceled;

      return helper.back.call(helper, null, null, instancesWithTransaction.investorTransaction)
        .should.be.fulfilled
        .then((result) => {
          helper.validateStatus.calledOnce.should.be.true;
          helper.validateStatus.args[0].should.be.eql([ instancesWithTransaction, validInvestorTransactionStatus ]);
          assert.deepEqual(result, [ investorTransaction ]);
        });
    });
  });
});
