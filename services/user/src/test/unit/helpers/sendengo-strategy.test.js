const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const SendengoStrategy = require('../../../api/helpers/sendengo-strategy');
const helperFixtures = require('../fixtures/helpers/sendengo-strategy');

chai.should();
chai.use(require('chai-as-promised'));

describe('unit/Sendengo strategy helper', () => {
  describe('selectObjectByCompanyId', () => {
    const fixtures = helperFixtures.selectObjectByCompanyId;
    const { result, defaultObject, sendengoId, otherCompanyId } = fixtures;
    const helper = new SendengoStrategy();

    beforeEach(() => {
      helper.sendengoId = 0;
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should return the sendengo object', (done) => {
      helper.sendengoId = 12000;
      const value = helper.selectObjectByCompanyId(sendengoId, result, defaultObject);

      value.should.be.eq(result);
      done();
    });

    it('should return the default object if the id does not match', (done) => {
      const value = helper.selectObjectByCompanyId(otherCompanyId, result, defaultObject);

      value.should.be.eq(defaultObject);
      done();
    });
  });


  describe('selectByCompanyId', () => {
    const fixtures = helperFixtures.selectByCompanyId;
    const { sendengoId, otherCompanyId, params, sendengo, defaultValue } = fixtures;
    const helper = new SendengoStrategy();

    beforeEach(() => {
      helper.sendengoId = 0;
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should return the sendengo object', (done) => {
      helper.sendengoId = 12000;
      const value = helper.selectByCompanyId(sendengoId, params);

      value.should.be.eq(sendengo);
      done();
    });

    it('should return the default value', (done) => {
      const value = helper.selectByCompanyId(otherCompanyId, params);

      value.should.be.eq(defaultValue);
      done();
    });
  });

  describe('execCallback', () => {
    const fixtures = helperFixtures.execCallback;
    const { sendengoId, otherCompanyId } = fixtures;
    const helper = new SendengoStrategy();

    beforeEach(() => {
      helper.sendengoId = 0;
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should execute callback', (done) => {
      helper.sendengoId = 12000;
      const callback = sandbox.spy();

      helper.execCallback(sendengoId, callback);

      callback.called.should.be.true;
      done();
    });


    it('should not execute callback', (done) => {
      const callback = sandbox.spy();

      helper.execCallback(otherCompanyId, callback);

      callback.called.should.be.false;
      done();
    });
  });
});
