const chai = require('chai');
const sinon = require('sinon');
const sandbox = sinon.sandbox.create();
const producer = require('../../../api/producers/user');
const helperFixtures = require('../fixtures/producers/user');

const log = require('/var/lib/core/js/log').prototype;
const s3 = require('/var/lib/core/js/s3');
const pdf = require('../../../pdf/builder');

const { User, Company, Agreement, NewInvoiceNotification } = require('../../../models');

chai.should();
chai.use(require('chai-as-promised'));

const td = require('testdouble');

const GUID = '1d60c653-f7cf-4b33-a62f-c878d60590e2';
const companyId = 1;
const ROLE = 'CXC';

const IF_OK = Symbol('IT_SHALL_NOT_PASS');

describe('unit/User producer', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('Generic tests', () => {
    describe('_findUsers', () => {
      // this does not need failure tests because errors should be propagated
      it('should work as expected', () => {
        const findCallback = td.func('User.findAll');

        td.replace(User, 'findAll', findCallback);
        td.when(User.findAll(td.matchers.isA(Object)))
          .thenResolve([ { name: 'Foo', email: 'foo@candy.bar' } ]);

        return producer._findUsers()
          .then(recipients => {
            recipients.should.be.deep.equal([
              [ 'Foo', 'foo@candy.bar' ]
            ]);
          });
      });
    });

    describe('_getPDF', () => {
      let getCallback;
      let urlCallback;
      let buildCallback;
      let uploadCallback;

      beforeEach(() => {
        getCallback = td.func('Agreement.getContract');
        buildCallback = td.func('pdf.build');
        urlCallback = td.func('s3.getUrl');
        uploadCallback = td.func('s3.upload');

        td.replace(Agreement, 'getContract', getCallback);
        td.replace(pdf, 'build', buildCallback);
        td.replace(s3, 'upload', uploadCallback);
        td.replace(s3, 'getUrl', urlCallback);
      });

      afterEach(() => {
        td.reset();
      });

      it('should fail if Agreement.getContract() throws anything', () => {
        td.when(Agreement.getContract(td.matchers.isA(Object)))
          .thenThrow(new Error('Not found'));

        return producer._getPDF({}, GUID, 'x')
          .should.be.rejected;
      });

      it('should fail if pdf.build() rejects anything', () => {
        td.when(Agreement.getContract(td.matchers.isA(Object)))
          .thenReturn('PDF BUFFER');

        td.when(pdf.build('PDF BUFFER', td.matchers.isA(Object)))
          .thenReject();

        return producer._getPDF({}, GUID, 'x')
          .should.be.rejected;
      });

      it('should fail if s3.upload() rejects anything', () => {
        td.when(Agreement.getContract(td.matchers.isA(Object)))
          .thenReturn('PDF BUFFER');

        td.when(pdf.build('PDF BUFFER', td.matchers.isA(Object)))
          .thenResolve('FINAL PDF BUFFER');

        td.when(s3.upload('x', 'FINAL PDF BUFFER', GUID, td.matchers.anything()))
          .thenReject();

        return producer._getPDF({}, GUID, 'x')
          .should.be.rejected;
      });

      it('should fail if s3.getURL() rejects anything', () => {
        td.when(Agreement.getContract(td.matchers.isA(Object)))
          .thenReturn('PDF BUFFER');

        td.when(pdf.build('PDF BUFFER', td.matchers.isA(Object)))
          .thenResolve('FINAL PDF BUFFER');

        td.when(s3.upload('x', 'FINAL PDF BUFFER', GUID, td.matchers.anything()))
          .thenResolve('SOME_S3_URL');

        td.when(s3.getUrl('SOME_S3_URL', GUID))
          .thenReject();

        return producer._getPDF({}, GUID, 'x')
          .should.be.rejected;
      });
    });

    // test once, avoid duplicated code/tests ;-)
    describe('_produce', () => {
      it('is just a wrapper over produce()', () => {
        const eventShape = {
          message: {
            type: td.matchers.isA(String),
            body: td.matchers.isA(Object),
            guid: td.matchers.isA(String)
          },
          key: td.matchers.anything()
        };

        const logCallback = td.func('log.message');
        const produceCallback = td.func('producer.produce');

        td.replace(log, 'message', logCallback);
        td.replace(producer, 'produce', produceCallback);
        td.when(producer.produce(eventShape)).thenResolve(IF_OK);

        return producer._produce(1, 'Example', { foo: 'bar' }, GUID)
          .should.be.fulfilled
          .then(result => {
            result.should.be.equal(IF_OK);
            td.explain(logCallback).callCount.should.be.equal(1);
            td.explain(produceCallback).callCount.should.be.equal(1);
          });
      });
    });

    describe('notifications', () => {
      let _findUsersCallback;
      let _produceCallback;
      let _pdfCallback;
      let logCallback;

      beforeEach(() => {
        logCallback = td.func('log.message');
        _pdfCallback = td.func('producer._getPDF');
        _produceCallback = td.func('producer._produce');
        _findUsersCallback = td.func('producer._findUsers');

        td.replace(log, 'message', logCallback);
        td.replace(producer, '_getPDF', _pdfCallback);
        td.replace(producer, '_produce', _produceCallback);
        td.replace(producer, '_findUsers', _findUsersCallback);

        td.when(producer._getPDF(td.matchers.isA(Object), GUID, td.matchers.isA(String)))
          .thenResolve('FINAL_S3_URL');

        td.when(producer._findUsers(td.matchers.isA(Number), td.matchers.isA(String)))
          .thenResolve([ {
            name: 'foo',
            email: 'foo@candy.bar'
          }, {
            name: 'baz',
            email: 'buzz@candy.bar'
          } ]);
      });

      afterEach(() => {
        td.reset();
      });

      function shouldPass(expected) {
        return actual => {
          actual.should.be.equal(expected);
          td.explain(_findUsersCallback).callCount.should.be.equal(1);
        };
      }

      // once all used methods are tested we just check event-shapes
      describe('agreementAccepted', () => {
        it('should work as expected', () => {
          const agreed_at = new Date().toISOString();
          const params = { id: companyId, role: ROLE, agreed_at };

          const bodyShape = {
            attachments: td.matchers.isA(Array),
            recipients: td.matchers.isA(Array),
            contract: td.matchers.isA(String),
            user_role: ROLE,
            agreed_at: {
              day: td.matchers.isA(String),
              month: td.matchers.isA(String),
              time: td.matchers.isA(String)
            }
          };

          td.when(producer._produce(companyId, 'AgreementAccepted', bodyShape, GUID)).thenResolve(IF_OK);

          return producer.agreementAccepted(params, GUID)
            .should.be.fulfilled
            .then(shouldPass(IF_OK));
        });
      });

      // notifications below are enough similar to be abstracted away

      const CLIENT = {
        name: 'Client name'
      };

      const COMPANY = {
        name: 'Company name'
      };

      const INVESTOR = {
        name: 'Investor name',
        business_name: 'Some business',
        isFideicomiso: false
      };

      const ESTIMATE = {
        operation_cost: '10.00',
        fund_payment: '100.00',
        fund_total: '90.00',
        commission: '1.00'
      };

      const INVOICE = {
        investor_company_id: companyId,
        company_id: companyId,
        client_company_id: companyId,
        id: 1,
        number: 123,
        emission_date: new Date().toISOString(),
        expiration: new Date().toISOString(),
        published: new Date().toISOString(),
        updated_at: new Date().toISOString(),
        uuid: 'd3008d89-78eb-4500-be57-4ff92381ab83',
        getClient: () => CLIENT,
        getCompany: () => COMPANY,
        getInvestor: () => INVESTOR,
        getFundEstimate: () => ESTIMATE
      };

      describe('notifyInvoicePublished', () => {
        const baseShape = {
          recipients: td.matchers.isA(Array),
          invoice_id: td.matchers.anything(),
          invoice_number: td.matchers.anything(),
          client_name: td.matchers.isA(String),
          user_role: ROLE,
          published_at: {
            day: td.matchers.isA(String),
            month: td.matchers.isA(String),
            time: td.matchers.isA(String)
          }
        };

        it('should work as expected', () => {
          const fixedShape = {
            ...baseShape,
            attachments: td.matchers.isA(Array)
          };

          td.when(producer._produce(1, 'NotifyInvoicePublished', fixedShape, GUID)).thenResolve(IF_OK);

          return producer.notifyInvoicePublished(ROLE, [ INVOICE ], GUID)
            .should.be.fulfilled
            .then(shouldPass(IF_OK));
        });
      });

      describe('notifyFundApproved', () => {
        const eventShape = {
          attachments: td.matchers.isA(Array),
          recipients: td.matchers.isA(Array),
          invoice_id: td.matchers.anything(),
          invoice_number: td.matchers.anything(),
          nombre_proveedor: td.matchers.isA(String),
          client_name: td.matchers.isA(String),
          fecha_pago: td.matchers.isA(String),
          fecha_expedicion: td.matchers.isA(String),
          fund_at: {
            day: td.matchers.isA(String),
            month: td.matchers.isA(String),
            time: td.matchers.isA(String)
          }
        };

        it('should work as expected', () => {
          td.when(producer._produce(1, 'NotifyFundApproved', eventShape, GUID)).thenResolve(IF_OK);

          return producer.notifyFundApproved(INVOICE, GUID)
            .should.be.fulfilled
            .then(shouldPass(IF_OK));
        });
      });
    });
  });

  describe('notifyInvoiceApproved', () => {
    const fixtures = helperFixtures.notifyInvoiceApproved;
    const { guid, invoices, users, event, s3Url, company, notifyInvoiceApprovedKey,
      notifyInvoiceApprovedParams, type } = fixtures;
    const produceError = new Error('produce error');

    beforeEach(() => {
      invoices.forEach(invoice => {
        sandbox.stub(producer, '_findUsers').resolves(users);
        sandbox.stub(invoice, 'getCompany').resolves(company);
        sandbox.stub(invoice, 'getBasicInfo').resolves();
      });
      sandbox.stub(producer, '_getPDF').resolves(s3Url);
      sandbox.stub(producer, '_produce').resolves(event);
    });

    it('should fail if producer._produce returns a reject', () => {
      producer._produce.restore();
      sandbox.stub(producer, '_produce').rejects(produceError);

      return producer.notifyInvoiceApproved(invoices, guid)
        .should.be.rejected
        .then(error => {
          invoices.forEach(invoice => {
            producer._findUsers.calledOnce.should.be.true;
            producer._findUsers.calledWithMatch(invoice.client_company_id, 'CXP');
            invoice.getCompany.calledOnce.should.be.true;
            invoice.getBasicInfo.calledOnce.should.be.true;
          });
          producer._getPDF.calledOnce.should.be.true;
          producer._getPDF.calledWithMatch(notifyInvoiceApprovedParams, guid, notifyInvoiceApprovedKey);
          producer._produce.calledOnce.should.be.true;
          producer._produce.calledWithMatch(company.id, type, event.body);
          error.should.be.eql(produceError);
        });
    });

    it('should correctly produce notifyInvoiceApproved', () => {
      return producer.notifyInvoiceApproved(invoices, guid)
        .then(resultEvent => {
          invoices.forEach(invoice => {
            producer._findUsers.calledOnce.should.be.true;
            producer._findUsers.calledWithMatch(invoice.client_company_id, 'CXP');
            invoice.getCompany.calledOnce.should.be.true;
            invoice.getBasicInfo.calledOnce.should.be.true;
          });
          producer._getPDF.calledOnce.should.be.true;
          producer._getPDF.calledWithMatch(notifyInvoiceApprovedParams, guid, notifyInvoiceApprovedKey);
          producer._produce.calledOnce.should.be.true;
          producer._produce.calledWithMatch(company.id, type, event.body);
          resultEvent.should.be.eql(event);
        });
    });
  });

  describe('notifyFundRequest', () => {
    const fixtures = helperFixtures.notifyFundRequest;
    const { guid, invoices, users, event, fundEstimate, s3Url, company, notifyFundRequestKey,
      notifyFundRequestPDFParams, type } = fixtures;
    const produceError = new Error('produce error');

    beforeEach(() => {
      sandbox.stub(producer, '_findUsers').resolves(users);
      invoices.forEach(invoice => {
        sandbox.stub(invoice, 'getClient').resolves(company);
        sandbox.stub(invoice, 'getInvestorFundEstimate').resolves(fundEstimate);
      });
      sandbox.stub(producer, '_getPDF').resolves(s3Url);
      sandbox.stub(producer, '_produce').resolves(event);
    });

    it('should fail if producer._produce returns a reject', () => {
      producer._produce.restore();
      sandbox.stub(producer, '_produce').rejects(produceError);

      return producer.notifyFundRequest(invoices, guid)
        .should.be.rejected
        .then(error => {
          producer._findUsers.calledOnce.should.be.true;
          producer._findUsers.calledWithMatch(invoices[0].investor_company_id, 'INVESTOR');
          invoices.forEach(invoice => {
            invoice.getClient.calledOnce.should.be.true;
            invoice.getInvestorFundEstimate.calledOnce.should.be.true;
          });
          producer._getPDF.calledOnce.should.be.true;
          producer._getPDF.calledWithMatch(notifyFundRequestPDFParams, guid, notifyFundRequestKey);
          producer._produce.calledOnce.should.be.true;
          producer._produce.calledWithMatch(company.id, type, event.body);
          error.should.be.eql(produceError);
        });
    });

    it('should correctly produce notifyFundRequest', () => {
      return producer.notifyFundRequest(invoices, guid)
        .then(resultEvent => {
          producer._findUsers.calledOnce.should.be.true;
          producer._findUsers.calledWithMatch(invoices[0].investor_company_id, 'INVESTOR');
          invoices.forEach(invoice => {
            invoice.getClient.calledOnce.should.be.true;
            invoice.getInvestorFundEstimate.calledOnce.should.be.true;
          });
          producer._getPDF.calledOnce.should.be.true;
          producer._getPDF.calledWithMatch(notifyFundRequestPDFParams, guid, notifyFundRequestKey);
          producer._produce.calledOnce.should.be.true;
          producer._produce.calledWithMatch(company.id, type, event.body);
          resultEvent.should.be.eql(event);
        });
    });
  });

  describe('dummy event', () => {
    const fixtures = helperFixtures.dummyEvent;

    it('should produce dummy event', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
      sandbox.stub(log, 'message').callsFake(() => true);

      return producer.dummyEvent()
        .then(() => {
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWith(fixtures.ValidDummyEvent);
          log.message.calledOnce.should.be.true;
          log.message.args[0][1].should.be.eql(fixtures.ValidDummyEvent);
        });
    });

    it('should send error message if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');
      const { logMessageParams } = helperFixtures.dummyEvent;

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));
      sandbox.stub(log, 'message').callsFake(() => true);

      return producer.dummyEvent()
        .should.be.rejected
        .then(err => {
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
        });
    });
  });

  describe('invitationCreated', () => {
    const fixtures = helperFixtures.invitationCreated;
    const { token, guid, company, event, logMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invitationCreated(token, company, guid)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invitationCreated(token, company, guid)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('resendInvitation', () => {
    const fixtures = helperFixtures.resendInvitation;
    const { token, guid, company, event, logMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.resendInvitation(token, company, guid)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.resendInvitation(token, company, guid)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('passwordChanged', () => {
    const fixtures = helperFixtures.passwordChanged;
    const { guid, user, event, logMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.passwordChanged(user, guid)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.passwordChanged(user, guid)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('recoverPassword', () => {
    const fixtures = helperFixtures.recoverPassword;
    const { guid, token, event, logMessageParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.recoverPassword(token, guid)
        .should.be.rejected
        .then(err => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.recoverPassword(token, guid)
        .should.be.fulfilled
        .then(() => {
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceCreated', () => {
    const fixtures = helperFixtures.invoiceCreated;
    const { users, client, invoice, event, logMessageParams,
      guid, userQuery, companyWhere } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(client));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceCreated(invoice, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          Company.findOne.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if Company.findOne fails', () => {
      const commonError = new Error('common error');

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return producer.invoiceCreated(invoice, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyWhere).should.be.true;
          log.message.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceCreated(invoice, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyWhere).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceCreated(invoice, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyWhere).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceApproved', () => {
    const fixtures = helperFixtures.invoiceApproved;
    const { users, client, invoice, event, logMessageParams,
      guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.resolve(client));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceApproved(invoice, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if invoice.getClient fails', () => {
      const commonError = new Error('common error');

      invoice.getClient.restore();
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.reject(commonError));

      return producer.invoiceApproved(invoice, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceApproved(invoice, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceApproved(invoice, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceFundRequest', () => {
    const fixtures = helperFixtures.invoiceFundRequest;
    const { users, basicOperationInfoInvoices, company, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceFundRequest(basicOperationInfoInvoices, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceFundRequest(basicOperationInfoInvoices, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.invoiceFundRequest(basicOperationInfoInvoices, company, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceRejected', () => {
    const fixtures = helperFixtures.invoiceRejected;
    const { users, client, invoice, event, logMessageParams,
      guid, userQuery, reason } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.resolve(client));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceRejected(invoice, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if invoice.getClient fails', () => {
      const commonError = new Error('common error');

      invoice.getClient.restore();
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.reject(commonError));

      return producer.invoiceRejected(invoice, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceRejected(invoice, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceRejected(invoice, reason, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('publishedInvoiceRejected', () => {
    const fixtures = helperFixtures.publishedInvoiceRejected;
    const { users, client, invoice, event, logMessageParams,
      guid, userQuery, reason } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.resolve(client));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.publishedInvoiceRejected(invoice, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.publishedInvoiceRejected(invoice, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.publishedInvoiceRejected(invoice, reason, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('fundRequestedInvoiceRejected', () => {
    const fixtures = helperFixtures.fundRequestedInvoiceRejected;
    const { users, invoice, event, logMessageParams,
      guid, userQuery, reason, investorId } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.fundRequestedInvoiceRejected(invoice, reason, investorId, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.fundRequestedInvoiceRejected(invoice, reason, investorId, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.fundRequestedInvoiceRejected(invoice, reason, investorId, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('fundRequestedInvoiceApproved', () => {
    const fixtures = helperFixtures.fundRequestedInvoiceApproved;
    const { users, invoice, event, logMessageParams, company, guid,
      cxcQuery, cxpQuery, investorQuery, adminCompanyQuery, payment } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
      sandbox.stub(invoice, 'getInvestor').resolves(company);
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.fundRequestedInvoiceApproved(invoice, payment, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledThrice.should.be.true;
          User.findAll.args[0].should.be.eql(cxcQuery);
          User.findAll.args[1].should.be.eql(cxpQuery);
          User.findAll.args[2].should.be.eql(investorQuery);
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.args[0].should.be.eql(adminCompanyQuery);
          producer.produce.called.should.be.false;

          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.fundRequestedInvoiceApproved(invoice, payment, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledThrice.should.be.true;
          User.findAll.args[0].should.be.eql(cxcQuery);
          User.findAll.args[1].should.be.eql(cxpQuery);
          User.findAll.args[2].should.be.eql(investorQuery);
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.args[0].should.be.eql(adminCompanyQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.args[0].should.be.eql([ event ]);

          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.fundRequestedInvoiceApproved(invoice, payment, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledThrice.should.be.true;
          User.findAll.args[0].should.be.eql(cxcQuery);
          User.findAll.args[1].should.be.eql(cxpQuery);
          User.findAll.args[2].should.be.eql(investorQuery);
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.args[0].should.be.eql(adminCompanyQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.args[0].should.be.eql([ event ]);
        });
    });
  });

  describe('withdrawCreated', () => {
    const fixtures = helperFixtures.withdrawCreated;
    const { users, withdraw, company, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.withdrawCreated(withdraw, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.withdrawCreated(withdraw, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.withdrawCreated(withdraw, company, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('depositCreated', () => {
    const fixtures = helperFixtures.depositCreated;
    const { users, deposit, company, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.depositCreated(deposit, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.depositCreated(deposit, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.depositCreated(deposit, company, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('pendingTransactionApproved', () => {
    const fixtures = helperFixtures.pendingTransactionApproved;
    const { users, pendingTransaction, company, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.pendingTransactionApproved(pendingTransaction, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.pendingTransactionApproved(pendingTransaction, company, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.pendingTransactionApproved(pendingTransaction, company, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('pendingTransactionRejected', () => {
    const fixtures = helperFixtures.pendingTransactionRejected;
    const { users, pendingTransaction, reason, event,
      logMessageParams, guid, userQuery, company } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.resolve(company));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.pendingTransactionRejected(pendingTransaction, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.pendingTransactionRejected(pendingTransaction, reason, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.pendingTransactionRejected(pendingTransaction, reason, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('clientInvoicePaymentCreated', () => {
    const fixtures = helperFixtures.clientInvoicePaymentCreated;
    const { users, amount, invoice, company, companyQuery, clientName,
      event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(Company, 'findOne').resolves(company);
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.clientInvoicePaymentCreated(amount, invoice, clientName, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if Company.findOne fails', () => {
      const commonError = new Error('common error');

      Company.findOne.restore();
      sandbox.stub(Company, 'findOne').callsFake(() => Promise.reject(commonError));

      return producer.clientInvoicePaymentCreated(amount, invoice, clientName, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          Company.findOne.calledOnce.should.be.true;
          Company.findOne.calledWithMatch(companyQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.clientInvoicePaymentCreated(amount, invoice, clientName, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.clientInvoicePaymentCreated(amount, invoice, clientName, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceExpired', () => {
    const fixtures = helperFixtures.invoiceExpired;
    const { users, client, invoice, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.resolve(client));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceExpired([ invoice ], guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if invoice.getClient fails', () => {
      const commonError = new Error('common error');

      invoice.getClient.restore();
      sandbox.stub(invoice, 'getClient').callsFake(() => Promise.reject(commonError));

      return producer.invoiceExpired([ invoice ], guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceExpired([ invoice ], guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceExpired([ invoice ], guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          invoice.getClient.calledOnce.should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('companyRoleSuspensionUpdated', () => {
    const fixtures = helperFixtures.companyRoleSuspensionUpdated;
    const { id, role, suspended, users, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.companyRoleSuspensionUpdated(id, role, suspended, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if producer.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.companyRoleSuspensionUpdated(id, role, suspended, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.companyRoleSuspensionUpdated(id, role, suspended, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('investorRoleSuspensionUpdated', () => {
    const fixtures = helperFixtures.investorRoleSuspensionUpdated;
    const { id, suspended, users, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.investorRoleSuspensionUpdated(id, suspended, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if producer.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.investorRoleSuspensionUpdated(id, suspended, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.investorRoleSuspensionUpdated(id, suspended, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoicePaymentDue', () => {
    const fixtures = helperFixtures.invoicePaymentDue;
    const { users, invoice, event, logMessageParams, guid, userQuery } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoicePaymentDue([ invoice ], guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoicePaymentDue([ invoice ], guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoicePaymentDue([ invoice ], guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceCompleted', () => {
    const fixtures = helperFixtures.invoiceCompleted;
    const {
      users, invoice, event, logMessageParams, guid, cxpQuery, cxcQuery, investorQuery,
      cxcPayment, cxpPayment, investorPayment, sendengoInvoice, sendengoCxcPayment,
      logMessageParamsSendengo, eventSendengo
    } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceCompleted(invoice, { cxpPayment, cxcPayment, investorPayment }, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledThrice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxpQuery);
          User.findAll.secondCall.args[0].should.be.eql(cxcQuery);
          User.findAll.thirdCall.args[0].should.be.eql(investorQuery);
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceCompleted(invoice, { cxpPayment, cxcPayment, investorPayment }, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledThrice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxpQuery);
          User.findAll.secondCall.args[0].should.be.eql(cxcQuery);
          User.findAll.thirdCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceCompleted(invoice, { cxpPayment, cxcPayment, investorPayment }, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledThrice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxpQuery);
          User.findAll.secondCall.args[0].should.be.eql(cxcQuery);
          User.findAll.thirdCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });


    it('should produce event correctly if invoice cxc is sendengo', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceCompleted(sendengoInvoice, {
        cxpPayment, cxcPayment: sendengoCxcPayment, investorPayment }, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxpQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParamsSendengo);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(eventSendengo).should.be.true;
        });
    });
  });

  describe('invoiceLost', () => {
    const fixtures = helperFixtures.invoiceLost;
    const { users, invoice, event, logMessageParams, guid, cxcQuery, investorQuery, cxcTransaction,
      investorTransaction } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceLost(invoice, cxcTransaction, investorTransaction, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxcQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceLost(invoice, cxcTransaction, investorTransaction, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxcQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceLost(invoice, cxcTransaction, investorTransaction, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxcQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('invoiceLatePayment', () => {
    const fixtures = helperFixtures.invoiceLatePayment;
    const { users, invoice, event, logMessageParams, guid, cxcQuery, investorQuery, cxcTransaction,
      investorTransaction } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.invoiceLatePayment(invoice, cxcTransaction, investorTransaction, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxcQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          err.should.be.equal(commonError);
        });
    });

    it('should reject if kafkaProducer.produce fails', () => {
      const produceError = new Error('produce error');

      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.invoiceLatePayment(invoice, cxcTransaction, investorTransaction, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxcQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());

      return producer.invoiceLatePayment(invoice, cxcTransaction, investorTransaction, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledTwice.should.be.true;
          User.findAll.firstCall.args[0].should.be.eql(cxcQuery);
          User.findAll.secondCall.args[0].should.be.eql(investorQuery);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('companyProposed', () => {
    const fixtures = helperFixtures.companyProposed;
    const { users, event, logMessageParams, guid, userQuery, proposed, name } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.companyProposed(proposed, name, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.companyProposed(proposed, name, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.companyProposed(proposed, name, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });

  describe('notifyNewInvoices', () => {
    const fixtures = helperFixtures.notifyNewInvoices;
    const { users, event, logMessageParams, guid, userQuery, invoices, tokens,
      createNewInvoiceTokenParams, createNewInvoiceTokenFailingParams } = fixtures;

    beforeEach(() => {
      sandbox.stub(log, 'message').callsFake(() => true);
      sandbox.stub(User, 'findAll').callsFake(() => Promise.resolve(users));
      sandbox.stub(NewInvoiceNotification, 'createNewInvoiceToken').callsFake(() => Promise.resolve(tokens));
      sandbox.stub(producer, 'produce').callsFake(() => Promise.resolve());
    });

    it('should reject if User.findAll fails', () => {
      const commonError = new Error('common error');

      User.findAll.restore();
      sandbox.stub(User, 'findAll').callsFake(() => Promise.reject(commonError));

      return producer.notifyNewInvoices(invoices, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.calledOnce.should.be.false;
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if NewInvoiceNotification.createNewInvoiceToken fails', () => {
      const commonError = new Error('common error');

      NewInvoiceNotification.createNewInvoiceToken.restore();
      sandbox.stub(NewInvoiceNotification, 'createNewInvoiceToken').callsFake(() => Promise.reject(commonError));

      return producer.notifyNewInvoices(invoices, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.calledOnce.should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.args[0].should.be.eql([ createNewInvoiceTokenFailingParams ]);
          log.message.called.should.be.false;
          producer.produce.called.should.be.false;
          err.should.be.equal(commonError);
        });
    });

    it('should reject if this.produce fails', () => {
      const produceError = new Error('produce error');

      producer.produce.restore();
      sandbox.stub(producer, 'produce').callsFake(() => Promise.reject(produceError));

      return producer.notifyNewInvoices(invoices, guid)
        .should.be.rejected
        .then(err => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.calledOnce.should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.args[0].should.be.eql([ createNewInvoiceTokenParams ]);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
          err.should.be.equal(produceError);
        });
    });

    it('should produce event correctly', () => {
      return producer.notifyNewInvoices(invoices, guid)
        .should.be.fulfilled
        .then(() => {
          User.findAll.calledOnce.should.be.true;
          User.findAll.calledWithMatch(userQuery).should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.calledOnce.should.be.true;
          NewInvoiceNotification.createNewInvoiceToken.args[0].should.be.eql([ createNewInvoiceTokenParams ]);
          log.message.calledOnce.should.be.true;
          log.message.args[0].should.be.eql(logMessageParams);
          producer.produce.calledOnce.should.be.true;
          producer.produce.calledWithMatch(event).should.be.true;
        });
    });
  });
});
