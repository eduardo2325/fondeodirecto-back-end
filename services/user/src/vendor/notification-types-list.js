module.exports = [ {
  id: 1,
  type: 'Nuevas facturas',
  email_text: `Estimado Cesar,
    En hora buena! Tenemos nuevas facturas en el Market. No dejes ir esta oportunidad para invertir.
    Da clic en el botón y descubre lo que Fondeo Directo tiene para ti
  `
}, {
  id: 2,
  type: 'Invitación a inversionistas',
  email_text: `Hola Cesar.
    El equipo de Fondeo Directo te invita a formar parte de nuestra exclusiva red de
    inversionistas calificados.
    Da click abajo para iniciar tu proceso de registro.`
} ];
