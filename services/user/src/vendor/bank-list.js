module.exports = [ {
  id: '002',
  name: 'BANAMEX',
  business_name: 'Banco Nacional de México, S.A., Institución de Banca Múltiple, Grupo Financiero Banamex'
}, {
  id: '006',
  name: 'BANCOMEXT',
  business_name: 'Banco Nacional de Comercio Exterior, Sociedad Nacional de Crédito, Institución de Banca de Desarrollo'
}, {
  id: '009',
  name: 'BANOBRAS',
  business_name: 'Banco Nacional de Obras y Servicios Públicos, Sociedad Nacional de Crédito, Institución de Banca de Desarrollo'
}, {
  id: '012',
  name: 'BBVA BANCOMER',
  business_name: 'BBVA Bancomer, S.A., Institución de Banca Múltiple, Grupo Financiero BBVA Bancomer'
}, {
  id: '014',
  name: 'SANTANDER',
  business_name: 'Banco Santander (México), S.A., Institución de Banca Múltiple, Grupo Financiero Santander'
}, {
  id: '019',
  name: 'BANJERCITO',
  business_name: 'Banco Nacional del Ejército, Fuerza Aérea y Armada, Sociedad Nacional de Crédito, Institución de Banca de Desarrollo'
}, {
  id: '021',
  name: 'HSBC',
  business_name: 'HSBC México, S.A., institución De Banca Múltiple, Grupo Financiero HSBC'
}, {
  id: '030',
  name: 'BAJÍO',
  business_name: 'Banco del Bajío, S.A., Institución de Banca Múltiple'
}, {
  id: '032',
  name: 'IXE',
  business_name: 'IXE Banco, S.A., Institución de Banca Múltiple, IXE Grupo Financiero'
}, {
  id: '036',
  name: 'INBURSA',
  business_name: 'Banco Inbursa, S.A., Institución de Banca Múltiple, Grupo Financiero Inbursa'
}, {
  id: '037',
  name: 'INTERACCIONES',
  business_name: 'Banco Inbursa, S.A., Institución de Banca Múltiple, Grupo Financiero Inbursa Banco Interacciones, S.A., Institución de Banca Múltiple'
}, {
  id: '042',
  name: 'MIFEL',
  business_name: 'Banca Mifel, S.A., Institución de Banca Múltiple, Grupo Financiero Mifel'
}, {
  id: '044',
  name: 'SCOTIABANK',
  business_name: 'Scotiabank Inverlat, S.A.'
}, {
  id: '058',
  name: 'BANREGIO',
  business_name: 'Banco Regional de Monterrey, S.A., Institución de Banca Múltiple, Banregio Grupo Financiero'
}, {
  id: '059',
  name: 'INVEX',
  business_name: 'Banco Invex, S.A., Institución de Banca Múltiple, Invex Grupo Financiero'
}, {
  id: '060',
  name: 'BANSI',
  business_name: 'Bansi, S.A., Institución de Banca Múltiple'
}, {
  id: '062',
  name: 'AFIRME',
  business_name: 'Banca Afirme, S.A., Institución de Banca Múltiple'
}, {
  id: '072',
  name: 'BANORTE',
  business_name: 'Banco Mercantil del Norte, S.A., Institución de Banca Múltiple, Grupo Financiero Banorte'
}, {
  id: '102',
  name: 'THE ROYAL BANK',
  business_name: 'The Royal Bank of Scotland México, S.A., Institución de Banca Múltiple'
}, {
  id: '103',
  name: 'AMERICAN EXPRESS',
  business_name: 'American Express Bank (México), S.A., Institución de Banca Múltiple'
}, {
  id: '106',
  name: 'BAMSA',
  business_name: 'Bank of America México, S.A., Institución de Banca Múltiple, Grupo Financiero Bank of America'
}, {
  id: '108',
  name: 'TOKYO',
  business_name: 'Bank of Tokyo-Mitsubishi UFJ (México), S.A.'
}, {
  id: '110',
  name: 'JP MORGAN',
  business_name: 'Banco J.P. Morgan, S.A., Institución de Banca Múltiple, J.P. Morgan Grupo Financiero'
}, {
  id: '112',
  name: 'BMONEX',
  business_name: 'Banco Monex, S.A., Institución de Banca Múltiple'
}, {
  id: '113',
  name: 'VE POR MAS',
  business_name: 'Banco Ve Por Mas, S.A. Institución de Banca Múltiple'
}, {
  id: '116',
  name: 'ING',
  business_name: 'ING Bank (México), S.A., Institución de Banca Múltiple, ING Grupo Financiero'
}, {
  id: '124',
  name: 'DEUTSCHE',
  business_name: 'Deutsche Bank México, S.A., Institución de Banca Múltiple'
}, {
  id: '126',
  name: 'CREDIT SUISSE',
  business_name: 'Banco Credit Suisse (México), S.A. Institución de Banca Múltiple, Grupo Financiero Credit Suisse (México)'
}, {
  id: '127',
  name: 'AZTECA',
  business_name: 'Banco Azteca, S.A. Institución de Banca Múltiple.'
}, {
  id: '128',
  name: 'AUTOFIN',
  business_name: 'Banco Autofin México, S.A. Institución de Banca Múltiple'
}, {
  id: '129',
  name: 'BARCLAYS',
  business_name: 'Barclays Bank México, S.A., Institución de Banca Múltiple, Grupo Financiero Barclays México'
}, {
  id: '130',
  name: 'COMPARTAMOS',
  business_name: 'Banco Compartamos, S.A., Institución de Banca Múltiple'
}, {
  id: '131',
  name: 'BANCO FAMSA',
  business_name: 'Banco Ahorro Famsa, S.A., Institución de Banca Múltiple'
}, {
  id: '132',
  name: 'BMULTIVA',
  business_name: 'Banco Multiva, S.A., Institución de Banca Múltiple, Multivalores Grupo Financiero'
}, {
  id: '133',
  name: 'ACTINVER',
  business_name: 'Banco Actinver, S.A. Institución de Banca Múltiple, Grupo Financiero Actinver'
}, {
  id: '134',
  name: 'WAL-MART',
  business_name: 'Banco Wal-Mart de México Adelante, S.A., Institución de Banca Múltiple'
}, {
  id: '135',
  name: 'NAFIN',
  business_name: 'Nacional Financiera, Sociedad Nacional de Crédito, Institución de Banca de Desarrollo'
}, {
  id: '136',
  name: 'INTERBANCO',
  business_name: 'Inter Banco, S.A. Institución de Banca Múltiple'
}, {
  id: '137',
  name: 'BANCOPPEL',
  business_name: 'BanCoppel, S.A., Institución de Banca Múltiple'
}, {
  id: '138',
  name: 'ABC CAPITAL',
  business_name: 'ABC Capital, S.A., Institución de Banca Múltiple'
}, {
  id: '139',
  name: 'UBS BANK',
  business_name: 'UBS Bank México, S.A., Institución de Banca Múltiple, UBS Grupo Financiero'
}, {
  id: '140',
  name: 'CONSUBANCO',
  business_name: 'Consubanco, S.A. Institución de Banca Múltiple'
}, {
  id: '141',
  name: 'VOLKSWAGEN',
  business_name: 'Volkswagen Bank, S.A., Institución de Banca Múltiple'
}, {
  id: '143',
  name: 'CIBANCO',
  business_name: 'CIBanco, S.A.'
}, {
  id: '145',
  name: 'BBASE',
  business_name: 'Banco Base, S.A., Institución de Banca Múltiple'
}, {
  id: '166',
  name: 'BANSEFI',
  business_name: 'Banco del Ahorro Nacional y Servicios Financieros, Sociedad Nacional de Crédito, Institución de Banca de Desarrollo'
}, {
  id: '168',
  name: 'HIPOTECARIA FEDERAL',
  business_name: 'Sociedad Hipotecaria Federal, Sociedad Nacional de Crédito, Institución de Banca de Desarrollo'
}, {
  id: '600',
  name: 'MONEXCB',
  business_name: 'Monex Casa de Bolsa, S.A. de C.V. Monex Grupo Financiero'
}, {
  id: '601',
  name: 'GBM',
  business_name: 'GBM Grupo Bursátil Mexicano, S.A. de C.V. Casa de Bolsa'
}, {
  id: '602',
  name: 'MASARI',
  business_name: 'Masari Casa de Bolsa, S.A.'
}, {
  id: '605',
  name: 'VALUE',
  business_name: 'Value, S.A. de C.V. Casa de Bolsa'
}, {
  id: '606',
  name: 'ESTRUCTURADORES',
  business_name: 'Estructuradores del Mercado de Valores Casa de Bolsa, S.A. de C.V.'
}, {
  id: '607',
  name: 'TIBER',
  business_name: 'Casa de Cambio Tiber, S.A. de C.V.'
}, {
  id: '608',
  name: 'VECTOR',
  business_name: 'Vector Casa de Bolsa, S.A. de C.V.'
}, {
  id: '610',
  name: 'B&B',
  business_name: 'B y B, Casa de Cambio, S.A. de C.V.'
}, {
  id: '614',
  name: 'ACCIVAL',
  business_name: 'Acciones y Valores Banamex, S.A. de C.V., Casa de Bolsa'
}, {
  id: '615',
  name: 'MERRILL LYNCH',
  business_name: 'Merrill Lynch México, S.A. de C.V. Casa de Bolsa'
}, {
  id: '616',
  name: 'FINAMEX',
  business_name: 'Casa de Bolsa Finamex, S.A. de C.V.'
}, {
  id: '617',
  name: 'VALMEX',
  business_name: 'Valores Mexicanos Casa de Bolsa, S.A. de C.V.'
}, {
  id: '618',
  name: 'UNICA',
  business_name: 'Unica Casa de Cambio, S.A. de C.V.'
}, {
  id: '619',
  name: 'MAPFRE',
  business_name: 'MAPFRE Tepeyac, S.A.'
}, {
  id: '620',
  name: 'PROFUTURO',
  business_name: 'Profuturo G.N.P., S.A. de C.V., Afore'
}, {
  id: '621',
  name: 'CB ACTINVER',
  business_name: 'Actinver Casa de Bolsa, S.A. de C.V.'
}, {
  id: '622',
  name: 'OACTIN',
  business_name: 'OPERADORA ACTINVER, S.A. DE C.V.'
}, {
  id: '623',
  name: 'SKANDIA',
  business_name: 'Skandia Vida, S.A. de C.V.'
}, {
  id: '626',
  name: 'CBDEUTSCHE',
  business_name: 'Deutsche Securities, S.A. de C.V. CASA DE BOLSA'
}, {
  id: '627',
  name: 'ZURICH',
  business_name: 'Zurich Compañía de Seguros, S.A.'
}, {
  id: '628',
  name: 'ZURICHVI',
  business_name: 'Zurich Vida, Compañía de Seguros, S.A.'
}, {
  id: '629',
  name: 'SU CASITA',
  business_name: 'Hipotecaria Su Casita, S.A. de C.V. SOFOM ENR'
}, {
  id: '630',
  name: 'CB INTERCAM',
  business_name: 'Intercam Casa de Bolsa, S.A. de C.V.'
}, {
  id: '631',
  name: 'CI BOLSA',
  business_name: 'CI Casa de Bolsa, S.A. de C.V.'
}, {
  id: '632',
  name: 'BULLTICK CB',
  business_name: 'Bulltick Casa de Bolsa, S.A., de C.V.'
}, {
  id: '633',
  name: 'STERLING',
  business_name: 'Sterling Casa de Cambio, S.A. de C.V.'
}, {
  id: '634',
  name: 'FINCOMUN',
  business_name: 'Fincomún, Servicios Financieros Comunitarios, S.A. de C.V.'
}, {
  id: '636',
  name: 'HDI SEGUROS',
  business_name: 'HDI Seguros, S.A. de C.V.'
}, {
  id: '637',
  name: 'ORDER',
  business_name: 'Order Express Casa de Cambio, S.A. de C.V'
}, {
  id: '638',
  name: 'AKALA',
  business_name: 'Akala, S.A. de C.V., Sociedad Financiera Popular'
}, {
  id: '640',
  name: 'CB JPMORGAN',
  business_name: 'J.P. Morgan Casa de Bolsa, S.A. de C.V. J.P. Morgan Grupo Financiero'
}, {
  id: '642',
  name: 'REFORMA',
  business_name: 'Operadora de Recursos Reforma, S.A. de C.V., S.F.P.'
}, {
  id: '646',
  name: 'STP',
  business_name: 'Sistema de Transferencias y Pagos STP, S.A. de C.V.SOFOM ENR'
}, {
  id: '647',
  name: 'TELECOMM',
  business_name: 'Telecomunicaciones de México'
}, {
  id: '648',
  name: 'EVERCORE',
  business_name: 'Evercore Casa de Bolsa, S.A. de C.V.'
}, {
  id: '649',
  name: 'SKANDIA',
  business_name: 'Skandia Operadora de Fondos, S.A. de C.V.'
}, {
  id: '651',
  name: 'SEGMTY',
  business_name: 'Seguros Monterrey New York Life, S.A de C.V'
}, {
  id: '652',
  name: 'ASEA',
  business_name: 'Solución Asea, S.A. de C.V., Sociedad Financiera Popular'
}, {
  id: '653',
  name: 'KUSPIT',
  business_name: 'Kuspit Casa de Bolsa, S.A. de C.V.'
}, {
  id: '655',
  name: 'SOFIEXPRESS',
  business_name: 'J.P. SOFIEXPRESS, S.A. de C.V., S.F.P.'
}, {
  id: '656',
  name: 'UNAGRA',
  business_name: 'UNAGRA, S.A. de C.V., S.F.P.'
}, {
  id: '659',
  name: 'EMPRESARIALES',
  business_name: 'OPCIONES DEL NOROESTE OPCIONES EMPRESARIALES DEL NORESTE, S.A. DE C.V., S.F.P.'
}, {
  id: '901',
  name: 'CLS',
  business_name: 'Cls Bank International'
}, {
  id: '902',
  name: 'INDEVAL',
  business_name: 'SD. Indeval, S.A. de C.V.'
}, {
  id: '670',
  name: 'LIBERTAD',
  business_name: 'Libertad Servicios Financieros, S.A. De C.V.'
}, {
  id: '999',
  name: 'N/A'
} ];
