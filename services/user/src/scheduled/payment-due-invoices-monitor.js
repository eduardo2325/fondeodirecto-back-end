const uuid = require('uuid');
const log = new (require('/var/lib/core/js/log'))(module);
const { Invoice, sequelize } = require('../models');
const userProducer = require('../api/producers/user');

function paymentDueInvoicesMonitor() {
  const guid = uuid.v4();
  const paymentDueInvoicesIds = [];
  const operationStatus = 'PAYMENT_DUE';
  let operation;

  log.message('Invoice payment due monitor job started', {}, 'Scheduler', guid);

  return sequelize.transaction(transaction => {
    return Invoice.updatePaymentDueInvoices()
      .then((paymentDueInvoices) => {
        const statusUpdates = paymentDueInvoices.map(invoice => {
          operation = invoice.Operation;
          operation.status = operationStatus;
          paymentDueInvoicesIds.push(invoice.id);

          return Promise.all([
            Promise.resolve(invoice),
            operation.save({ transaction })
          ]);
        });

        return Promise.all(statusUpdates);
      });
  })
    .then(invoices => {
      return Promise.all(invoices.map(invoice => userProducer.invoicePaymentDue(invoice, guid)));
    })
    .then(() =>
      log.message('Invoice payment due monitor job finished', { paymentDueInvoicesIds }, 'Scheduler', guid)
    )
    .catch(err => log.error(err, guid, { message: 'Invoice payment due monitor job failed' }));
}

module.exports = paymentDueInvoicesMonitor;
