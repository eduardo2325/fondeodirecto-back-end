const expiredInvoicesMonitor = require('./expired-invoices-monitor');
const paymentDueInvoicesMonitor = require('./payment-due-invoices-monitor');
const dummyEvent = require('./dummy-event');
const newestInvoicesMonitor = require('./newest-invoices-monitor');
const paymentNotificationMonitor = require('./payment-notification-monitor');

const definitions = {
  '0 30 6 * * *': expiredInvoicesMonitor,
  '0 0 7 * * *': paymentDueInvoicesMonitor,
  '0 0 8 * * *': dummyEvent,
  '0 */2 13-14 * * 1-5': newestInvoicesMonitor,
  '0 0 12 * * *': paymentNotificationMonitor
};

module.exports = definitions;
