const uuid = require('uuid');
const log = new (require('/var/lib/core/js/log'))(module);
const { Invoice } = require('../models');
const userProducer = require('../api/producers/user');
let invoices;

function paymentNotificationMonitor() {
  const guid = uuid.v4();
  const paymentInvoicesIds = [];

  log.message('Invoice payment notification monitor job started', {}, 'Scheduler', guid);

  return Invoice.getNotifiableInvoices()
    .then((notifiableInvoices) => {
      invoices = notifiableInvoices;

      invoices.forEach(invoice => {
        paymentInvoicesIds.push(invoice.id);
      });
    })
    .then(() => {
      return Promise.all(invoices.map(invoice => userProducer.invoicePaymentNotification(invoice, guid)));
    })
    .then(() =>
      log.message('Invoice payment notification monitor job finished', { paymentInvoicesIds }, 'Scheduler', guid)
    )
    .catch(err => log.error(err, guid, { message: 'Invoice payment notification monitor job failed' }));
}

module.exports = paymentNotificationMonitor;
