const uuid = require('uuid');
const log = new (require('/var/lib/core/js/log'))(module);
const notificationProducer = require('../api/producers/user');

function dummyEvent() {
  const guid = uuid.v4();

  return Promise.resolve()
    .then(() => notificationProducer.dummyEvent())
    .then(() => log.message('Dummy event job finished', {}, 'Scheduler', guid))
    .catch(err => log.error(err, guid, { message: 'Dummy event job failed' }));
}

module.exports = dummyEvent;
