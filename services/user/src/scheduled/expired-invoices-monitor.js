const uuid = require('uuid');
const log = new (require('/var/lib/core/js/log'))(module);
const { Invoice, sequelize } = require('../models');
const userProducer = require('../api/producers/user');

function expiredInvoicesMonitor() {
  const operationStatus = 'EXPIRED';
  const expiredInvoiceIds = [];
  const guid = uuid.v4();
  let operation;

  log.message('Invoice expiration monitor job started', {}, 'Scheduler', guid);

  return sequelize.transaction(transaction => {
    return Invoice.getExpirableInvoices()
      .then(unprofitableInvoices => {
        const statusUpdates = unprofitableInvoices.map(invoice => {
          operation = invoice.Operation;
          operation.status = operationStatus;
          expiredInvoiceIds.push(invoice.id);

          return Promise.all([
            Promise.resolve(invoice),
            operation.save({ transaction })
          ]);
        });

        return Promise.all(statusUpdates);
      });
  })
    .then(invoices => Promise.all(invoices.map(invoice => userProducer.invoiceExpired(invoice))))
    .then(() => log.message('Invoice expiration monitor job finished', { expiredInvoiceIds }, 'Scheduler', guid))
    .catch(err => log.error(err, guid, { message: 'Invoice expiration monitor job failed' }));
}

module.exports = expiredInvoicesMonitor;
