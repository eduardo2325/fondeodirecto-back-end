const uuid = require('uuid');
const log = new (require('/var/lib/core/js/log'))(module);
const { Invoice } = require('../models');
const userProducer = require('../api/producers/user');
const minDate = new Date();
const maxDate = new Date();
const dateToCompare = new Date();

let doneToday = false;
let previousValue = new Date().getDay();

minDate.setHours(19, 0, 0, 0);
minDate.setDate(minDate.getDate());
maxDate.setHours(22, 0, 0, 0);
maxDate.setDate(maxDate.getDate());
dateToCompare.setHours(21, 37, 0, 0);
dateToCompare.setDate(dateToCompare.getDate());

const hourToCompare = dateToCompare.toLocaleTimeString();

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function newestInvoicesMonitor() {
  const guid = uuid.v4();

  log.message('Newest invoices monitor job started', {}, 'Scheduler', guid);

  return Invoice.getNewestInvoices()
    .then(invoices => {
      if (invoices.length !== 0) {
        userProducer.notifyNewInvoices(invoices);
      }
    })
    .then(() => log.message('Newest invoices monitor job finished', {}, 'Scheduler', guid))
    .catch(err => log.error(err, guid, { message: 'Newest invoices monitor job failed' }));
}

function randomizeBehavior() {
  const now = new Date();
  const currentValue = now.getDay();
  const randDate = randomDate(minDate, maxDate);
  const randomHour = randDate.toLocaleTimeString();

  if (previousValue !== currentValue) {
    doneToday = false;
    previousValue = currentValue;
  }

  if (!doneToday && randomHour >= hourToCompare) {
    doneToday = true;
    newestInvoicesMonitor();
  }

  return Promise.resolve(doneToday);
}

module.exports = randomizeBehavior;
