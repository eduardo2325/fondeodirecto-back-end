const Sequelize = require('sequelize');

const UserActivityModel = {
  attributes: {
    triggered_by: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    action: {
      type: Sequelize.STRING,
      allowNull: false
    },
    generated_at: {
      type: Sequelize.DATE
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'user_activities',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User, {
          foreignKey: 'triggered_by',
          targetKey: 'id',
          as: 'User'
        });
        this.belongsToMany(models.Invoice, {
          as: 'InvoiceActivities',
          through: 'invoice_activity',
          foreignKey: 'activity_id',
          otherKey: 'invoice_id'
        });
      }
    }
  }
};

module.exports = UserActivityModel;
