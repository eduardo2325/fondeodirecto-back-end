const Sequelize = require('sequelize');

const SiteConfigModel = {
  attributes: {
    key: {
      type: Sequelize.STRING,
      unique: true,
      primaryKey: true,
      validate: {
        notEmpty: true
      }
    },
    values: {
      type: Sequelize.JSONB
    }
  },
  options: {
    tableName: 'site_configs',
    underscored: true,
    paranoid: true
  }
};

module.exports = SiteConfigModel;
