const Sequelize = require('sequelize');

const TokenModel = {
  attributes: {
    token: {
      type: Sequelize.STRING,
      unique: true,
      validate: {
        notEmpty: true
      }
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        isIn: [ [ 'registration', 'recover_password', 'investor-invitation', 'new-invoice-notification' ] ]
      }
    },
    email: {
      type: Sequelize.STRING,
      validate: {
        notEmpty: true,
        isEmail: true
      }
    },
    company_rfc: {
      type: Sequelize.STRING
    },
    company_id: {
      type: Sequelize.INTEGER
    },
    data: {
      type: Sequelize.JSONB
    },
    issuer: {
      type: Sequelize.INTEGER
    }
  },
  options: {
    tableName: 'tokens',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          foreignKey: 'company_id',
          targetKey: 'id'
        });
      }
    }
  }
};

module.exports = TokenModel;
