const Sequelize = require('sequelize');

const BankCodeModel = {
  attributes: {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
      unique: true,
      validate: {
        notEmpty: true
      }
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    business_name: {
      type: Sequelize.STRING
    }
  },
  options: {
    tableName: 'bank_codes',
    underscored: true
  }
};

module.exports = BankCodeModel;
