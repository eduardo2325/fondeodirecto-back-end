const Sequelize = require('sequelize');

const NotificationTypeModel = {
  attributes: {
    type: {
      type: Sequelize.STRING
    },
    email_text: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 'ACTIVE',
      validate: {
        isIn: [ [ 'ACTIVE', 'INACTIVE' ] ]
      }
    }
  },
  options: {
    tableName: 'notification_types',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.hasMany(models.NewInvoiceNotification, {
          foreignKey: 'notification_type_id'
        });
      }
    }
  }
};

module.exports = NotificationTypeModel;
