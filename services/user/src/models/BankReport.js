const Sequelize = require('sequelize');

const BankReportModel = {
  attributes: {
    generated_at: {
      type: Sequelize.DATE,
      allowNull: false
    },
    total_amount: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    total_generated: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    generated_by: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    link: {
      type: Sequelize.STRING,
      allowNull: false
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'bank_reports',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User, {
          foreignKey: 'generated_by',
          targetKey: 'id'
        });
      }
    }
  }
};

module.exports = BankReportModel;
