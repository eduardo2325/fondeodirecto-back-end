const Sequelize = require('sequelize');

const OrderInvoiceModel = {
  attributes: {
    order_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'orders',
        key: 'id'
      }
    },
    invoice_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'invoices',
        key: 'id'
      }
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'order_invoices',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Order, {
          foreignKey: 'order_id',
          targetKey: 'id'
        });
        this.belongsTo(models.Invoice, {
          foreignKey: 'invoice_id',
          targetKey: 'id'
        });
      }
    }
  }
};

module.exports = OrderInvoiceModel;
