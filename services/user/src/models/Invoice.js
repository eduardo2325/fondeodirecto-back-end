const Sequelize = require('sequelize');

const InvoiceModel = {
  attributes: {
    uuid: {
      type: Sequelize.STRING,
      unique: true
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 'PENDING',
      validate: {
        isIn: [ [ 'PENDING', 'APPROVED', 'REJECTED', 'PUBLISHED' ] ]
      }
    },
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    client_company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    investor_company_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    subtotal: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    tax_total: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    tax_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    total: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    serie: {
      type: Sequelize.STRING
    },
    folio: {
      type: Sequelize.STRING
    },
    number: {
      type: Sequelize.STRING
    },
    items: {
      type: Sequelize.JSONB
    },
    cadena_original: {
      type: Sequelize.TEXT
    },
    sat_digital_stamp: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    cfdi_digital_stamp: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    company_address: {
      type: Sequelize.STRING
    },
    company_postal_code: {
      type: Sequelize.STRING
    },
    company_regime: {
      type: Sequelize.STRING
    },
    written_amount: {
      type: Sequelize.STRING
    },
    emission_date: {
      type: Sequelize.DATE
    },
    cfdi: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    expiration: {
      type: Sequelize.DATE
    },
    published: {
      type: Sequelize.DATE
    },
    fund_date: {
      type: Sequelize.DATE
    },
    approved_date: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'invoices',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          foreignKey: 'company_id',
          targetKey: 'id',
          as: 'Company'
        });
        this.belongsTo(models.Company, {
          foreignKey: 'client_company_id',
          targetKey: 'id',
          as: 'Client'
        });
        this.belongsTo(models.Company, {
          foreignKey: 'investor_company_id',
          targetKey: 'id',
          as: 'Investor'
        });
        this.hasOne(models.Operation, {
          foreignKey: 'invoice_id'
        });
      }
    }
  }
};

module.exports = InvoiceModel;
