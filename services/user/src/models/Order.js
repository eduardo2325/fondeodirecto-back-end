const Sequelize = require('sequelize');

const OrderModel = {
  attributes: {
    investor_company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    type: {
      type: Sequelize.STRING
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'orders',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          foreignKey: 'investor_company_id',
          targetKey: 'id'
        });
        this.hasMany(models.OrderInvoice, {
          foreignKey: 'order_id'
        });
      }
    }
  }
};

module.exports = OrderModel;
