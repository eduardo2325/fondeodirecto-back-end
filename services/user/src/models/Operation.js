const Sequelize = require('sequelize');

const OperationModel = {
  attributes: {
    invoice_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'invoices',
        key: 'id'
      }
    },
    published_date: {
      type: Sequelize.DATE
    },
    expiration_date: {
      type: Sequelize.DATE
    },
    days_limit: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    annual_cost_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fd_commission_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    reserve_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    annual_cost: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fd_commission: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    reserve: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    factorable: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    subtotal: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fund_payment: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    formula: {
      type: Sequelize.TEXT
    },
    commission: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    operation_cost: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fund_total: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 'PENDING',
      validate: {
        isIn: [ [ 'PENDING', 'FUND_REQUESTED', 'FUNDED', 'EXPIRED', 'PAYMENT_IN_PROCESS', 'PAYMENT_DUE',
          'COMPLETED', 'LOST', 'LATE_PAYMENT' ] ]
      }
    },
    payment_date: {
      type: Sequelize.DATE
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'operations',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Invoice, {
          foreignKey: 'invoice_id',
          targetKey: 'id'
        });
        this.belongsTo(models.User, {
          foreignKey: 'user_id',
          targetKey: 'id'
        });
        this.hasOne(models.InvestorTransaction, {
          foreignKey: 'operation_id'
        });
      }
    }
  }
};

module.exports = OperationModel;
