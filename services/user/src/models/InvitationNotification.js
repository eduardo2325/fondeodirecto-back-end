const Sequelize = require('sequelize');

const InvitationNotificationModel = {
  attributes: {
    token: {
      type: Sequelize.STRING,
      allowNull: false
    },
    event_type: {
      type: Sequelize.STRING
    }
  },
  options: {
    tableName: 'invitation_notifications',
    underscored: true,
    paranoid: true,
    classMethods: {
    }
  }
};

module.exports = InvitationNotificationModel;
