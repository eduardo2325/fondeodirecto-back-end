const Sequelize = require('sequelize');

const AgreementModel = {
  attributes: {
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    user_role: {
      type: Sequelize.STRING,
      primaryKey: true,
      unique: false
    },
    agreed_at: {
      type: Sequelize.DATE,
      allowNull: true
    },
    agreement_url: {
      type: Sequelize.STRING,
      allowNull: true
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'agreements',
    underscored: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          targetKey: 'id',
          foreignKey: 'company_id'
        });
      }
    }
  }
};

module.exports = AgreementModel;
