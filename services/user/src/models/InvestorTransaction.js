const Sequelize = require('sequelize');

const InvestorTransactionModel = {
  attributes: {
    operation_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'operations',
        key: 'id'
      }
    },
    investor_company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    fund_request_date: {
      type: Sequelize.DATE
    },
    fund_date: {
      type: Sequelize.DATE
    },
    days_limit: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    funded_amount: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    interest: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    interest_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fee: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fee_type: {
      type: Sequelize.STRING
    },
    global_fee_percentage: {
      type: Sequelize.STRING
    },
    earnings: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    earnings_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    isr: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    isr_percentage: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    perception: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 'PENDING',
      validate: {
        isIn: [ [ 'PENDING', 'COMPLETED', 'CANCELED' ] ]
      }
    },
    fideicomiso_fee: {
      type: Sequelize.DOUBLE
    },
    fideicomiso_fee_type: {
      type: Sequelize.STRING
    },
    created_at: {
      type: Sequelize.DATE
    },
    updated_at: {
      type: Sequelize.DATE
    },
    deleted_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'investor_transactions',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Operation, {
          foreignKey: 'operation_id',
          targetKey: 'id',
          as: 'Operation'
        });
        this.belongsTo(models.Company, {
          foreignKey: 'investor_company_id',
          targetKey: 'id',
          as: 'Investor'
        });
      }
    }
  }
};

module.exports = InvestorTransactionModel;
