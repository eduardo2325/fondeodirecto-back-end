const Sequelize = require('sequelize');

const NewInvoiceNotificationModel = {
  attributes: {
    new_invoice_token: {
      type: Sequelize.STRING,
      allowNull: false,
      references: {
        model: 'tokens',
        key: 'token'
      }
    },
    notification_type_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'notification_types',
        key: 'id'
      }
    },
    event_type: {
      type: Sequelize.STRING
    }
  },
  options: {
    tableName: 'new_invoice_notifications',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.NotificationType, {
          foreignKey: 'notification_type_id',
          targetKey: 'id'
        });
        this.belongsTo(models.Token, {
          foreignKey: 'new_invoice_token',
          targetKey: 'token'
        });
      }
    }
  }
};

module.exports = NewInvoiceNotificationModel;
