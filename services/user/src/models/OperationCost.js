const Sequelize = require('sequelize');

const OperationCostModel = {
  attributes: {
    annual_cost: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    reserve: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fd_commission: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    fee: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      },
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    variable_fee_percentage: {
      type: Sequelize.DOUBLE
    },
    fideicomiso_fee: {
      type: Sequelize.DOUBLE
    },
    fideicomiso_variable_fee: {
      type: Sequelize.DOUBLE
    }
  },
  options: {
    tableName: 'operation_costs',
    underscored: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          targetKey: 'id',
          foreignKey: 'company_id'
        });
      }
    }
  }
};

module.exports = OperationCostModel;
