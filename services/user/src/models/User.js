const Sequelize = require('sequelize');
const roles = require('../config/user-roles').roles;

const UserModel = {
  attributes: {
    name: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING,
      validate: {
        isEmail: true
      }
    },
    company_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    color: {
      type: Sequelize.STRING
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isIn: [ roles.map(role => role.value) ]
      }
    }
  },
  options: {
    tableName: 'users',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          targetKey: 'id',
          foreignKey: 'company_id'
        });
        this.hasMany(models.BankReport, {
          foreignKey: 'generated_by'
        });
      }
    }
  }
};

module.exports = UserModel;
