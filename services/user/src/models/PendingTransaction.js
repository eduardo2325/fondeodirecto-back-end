const Sequelize = require('sequelize');

const PendingTransactionModel = {
  attributes: {
    amount: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    type: {
      type: Sequelize.STRING,
      validate: {
        isIn: [ [ 'WITHDRAW', 'DEPOSIT' ] ]
      }
    },
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    data: {
      type: Sequelize.JSONB
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 'PENDING',
      validate: {
        isIn: [ [ 'PENDING', 'APPROVED', 'REJECTED' ] ]
      }
    },
    bank_report_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'bank_reports',
        key: 'id'
      }
    }
  },
  options: {
    tableName: 'pending_transactions',
    underscored: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          foreignKey: 'company_id',
          targetKey: 'id'
        });
        this.belongsTo(models.BankReport, {
          foreignKey: 'bank_report_id',
          targetKey: 'id',
          as: 'BankReport'
        });
      }
    }
  }
};

module.exports = PendingTransactionModel;
