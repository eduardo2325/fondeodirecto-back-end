const Sequelize = require('sequelize');
const userRoles = require('../config/user-roles').roles;

const SessionModel = {
  attributes: {
    user_id: {
      type: Sequelize.INTEGER,
      validate: {
        isInt: true
      }
    },
    token: {
      type: Sequelize.STRING
    },
    user_role: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isIn: [ userRoles.map(role => role.value) ]
      }
    },
    company_agreed: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    company_role: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isIn: [ [ 'ADMIN', 'COMPANY', 'INVESTOR' ] ]
      }
    },
    company_rfc: {
      type: Sequelize.STRING,
      validate: {
        notEmpty: true
      }
    },
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    expiration_date: {
      type: Sequelize.DATE
    },
    suspended: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    }
  },
  options: {
    tableName: 'sessions',
    underscored: true,
    paranoid: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User);
      }
    }
  }
};

module.exports = SessionModel;
