const Sequelize = require('sequelize');

const CompanyModel = {
  attributes: {
    rfc: {
      type: Sequelize.STRING,
      unique: true,
      validate: {
        notEmpty: true
      }
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    business_name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    holder: {
      type: Sequelize.STRING,
      validate: {
        notEmpty: true
      }
    },
    bank: {
      type: Sequelize.STRING
    },
    bank_account: {
      type: Sequelize.STRING
    },
    clabe: {
      type: Sequelize.STRING,
      validate: {
        notEmpty: true,
        is: /^\d{18}$/
      }
    },
    color: {
      type: Sequelize.STRING
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isIn: [ [ 'ADMIN', 'COMPANY', 'INVESTOR' ] ]
      }
    },
    balance: {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    taxpayer_type: {
      type: Sequelize.STRING
    },
    suspended_roles: {
      /* eslint new-cap: 0 */
      type: Sequelize.ARRAY(Sequelize.STRING),
      allowNull: false,
      defaultValue: []
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: ''
    },
    agreed_at: {
      type: Sequelize.DATE
    },
    agreement_url: {
      type: Sequelize.STRING
    },
    isFideicomiso: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    prepayment: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    }
  },
  options: {
    tableName: 'companies',
    underscored: true,
    classMethods: {
      associate: function(models) {
        this.hasMany(models.User, {
          foreignKey: 'company_id'
        });
        this.hasMany(models.Token, {
          foreignKey: 'company_id'
        });
        this.hasOne(models.OperationCost, {
          foreignKey: 'company_id'
        });
        this.hasMany(models.Agreement, {
          foreignKey: 'company_id'
        });
        this.hasMany(models.Order, {
          foreignKey: 'investor_company_id'
        });
      }
    }
  }
};

module.exports = CompanyModel;
