const Sequelize = require('sequelize');

const TransactionModel = {
  attributes: {
    amount: {
      type: Sequelize.DOUBLE,
      defaultValue: 0
    },
    type: {
      type: Sequelize.STRING,
      validate: {
        isIn: [ [ 'WITHDRAW', 'DEPOSIT' ] ]
      }
    },
    invoice_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'invoices',
        key: 'id'
      }
    },
    originated_from_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      },
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    destinated_to_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      },
      references: {
        model: 'companies',
        key: 'id'
      }
    },
    is_payment: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    created_at: {
      type: Sequelize.DATE
    }
  },
  options: {
    tableName: 'transactions',
    underscored: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Company, {
          foreignKey: 'originated_from',
          targetKey: 'id',
          as: 'SenderCompany'
        });
        this.belongsTo(models.Company, {
          foreignKey: 'destinated_to',
          targetKey: 'id',
          as: 'ReceiverCompany'
        });
        this.belongsTo(models.Invoice, {
          foreignKey: 'invoice_id',
          targetKey: 'id'
        });
      }
    }
  }
};

module.exports = TransactionModel;
