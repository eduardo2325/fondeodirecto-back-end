'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE agreements ADD COLUMN id SERIAL
      PRIMARY KEY`);
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn('agreements', 'id');
  }
};
