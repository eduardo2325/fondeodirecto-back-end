'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'operations',
      'commission', {
        type: Sequelize.DOUBLE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('operations', 'commission');
  }
};
