'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'sessions',
      'suspended', {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('sessions', 'suspended');
  }
};
