'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'transactions',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false
        },
        invoice_uuid: {
          type: Sequelize.STRING
        },
        originated_from: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'companies',
            key: 'rfc'
          }
        },
        destinated_to: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'companies',
            key: 'rfc'
          }
        },
        is_payment: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('transactions');
  }
};
