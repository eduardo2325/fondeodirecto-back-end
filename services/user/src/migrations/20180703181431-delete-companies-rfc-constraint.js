'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE companies DROP CONSTRAINT companies_pkey;');
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE companies ADD CONSTRAINT companies_pkey
      PRIMARY KEY(rfc);`);
  }
};
