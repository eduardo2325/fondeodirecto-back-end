'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('users', 'role')
      .then(() => {
        return queryInterface.addColumn(
          'users',
          'role', {
            type: Sequelize.STRING,
            allowNull: false
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'users',
      'role', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'roles',
          key: 'id'
        }
      }
    );
  }
};
