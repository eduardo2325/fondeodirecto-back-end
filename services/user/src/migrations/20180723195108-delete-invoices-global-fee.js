'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'global_fee_percentage');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'global_fee_percentage', {
        type: Sequelize.STRING
      }
    );
  }
};
