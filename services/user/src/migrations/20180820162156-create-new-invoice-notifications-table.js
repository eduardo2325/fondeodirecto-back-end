'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'new_invoice_notifications',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        notification_type_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'notification_types',
            key: 'id'
          }
        },
        event_type: {
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        },
        deleted_at: {
          type: Sequelize.DATE
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('new_invoice_notifications');
  }
};
