'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /* eslint new-cap: 0 */
    return queryInterface.addColumn(
      'companies',
      'suspended_roles', {
        type: Sequelize.ARRAY(Sequelize.STRING),
        allowNull: false,
        defaultValue: []
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('companies', 'suspended_roles');
  }
};
