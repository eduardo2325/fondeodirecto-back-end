'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'annual_cost');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'annual_cost', {
        type: Sequelize.DOUBLE
      }
    );
  }
};
