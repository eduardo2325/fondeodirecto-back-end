'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'companies',
      {
        rfc: {
          type: Sequelize.STRING,
          primaryKey: true,
          unique: true
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false
        },
        business_name: {
          type: Sequelize.STRING,
          allowNull: false
        },
        holder: {
          type: Sequelize.STRING,
          allowNull: false
        },
        clabe: {
          type: Sequelize.STRING,
          allowNull: false
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('companies');
  }
};
