'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'operations',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        invoice_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'invoices',
            key: 'id'
          }
        },
        published_date: {
          type: Sequelize.DATE
        },
        expiration_date: {
          type: Sequelize.DATE
        },
        days_limit: {
          type: Sequelize.INTEGER
        },
        annual_cost_percentage: {
          type: Sequelize.DOUBLE
        },
        fd_commission_percentage: {
          type: Sequelize.DOUBLE
        },
        reserve_percentage: {
          type: Sequelize.DOUBLE
        },
        annual_cost: {
          type: Sequelize.DOUBLE
        },
        fd_commission: {
          type: Sequelize.DOUBLE
        },
        reserve: {
          type: Sequelize.DOUBLE
        },
        factorable: {
          type: Sequelize.DOUBLE
        },
        subtotal: {
          type: Sequelize.DOUBLE
        },
        fund_payment: {
          type: Sequelize.DOUBLE
        },
        formula: {
          type: Sequelize.TEXT
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        status: {
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        },
        deleted_at: {
          type: Sequelize.DATE
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('operations');
  }
};
