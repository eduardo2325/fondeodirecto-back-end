'use strict';
const { Company } = require('../models');

module.exports = {
  up: (queryInterface) => {
    const findAll = 'SELECT * FROM companies';

    return queryInterface.sequelize
      .query(findAll)
      .then(result => {
        const companies = result[1].rows;
        const promises = companies.map(value => {
          return Company.analyzeClabe(value.clabe)
            .then(bankInfo => {
              value.bank = bankInfo.name;
              value.bank_account = bankInfo.account;

              return value.save();
            });
        });

        return Promise.all(promises);
      });
  },

  down: () => {
    return Company.update({
      bank: 'X',
      bank_account: 'X'
    });
  }
};
