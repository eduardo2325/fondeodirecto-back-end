'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'users',
      'age', {
        type: Sequelize.INTEGER
      }
    ).then( () => {
      return queryInterface.addColumn(
        'users',
        'phone', {
          type: Sequelize.STRING
        }
      );
    }).then( () => {
      return queryInterface.addColumn(
        'users',
        'address', {
          type: Sequelize.STRING
        }
      );
    });
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn('users', 'age')
      .then(() => {
        return queryInterface.removeColumn('users', 'phone');
      })
      .then(() => {
        return queryInterface.removeColumn('users', 'address');
      });
  }
};
