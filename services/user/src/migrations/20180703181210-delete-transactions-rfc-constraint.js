'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE transactions DROP CONSTRAINT
      transactions_originated_from_fkey`)
      .then(() => {
        return queryInterface.sequelize.query(`ALTER TABLE transactions DROP CONSTRAINT
          transactions_destinated_to_fkey`);
      });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE transactions ADD CONSTRAINT
      transactions_originated_from_fkey FOREIGN KEY (originated_from) REFERENCES companies (rfc);`)
      .then(() => {
        return queryInterface.sequelize.query(`ALTER TABLE transactions ADD CONSTRAINT
          transactions_destinated_to_fkey FOREIGN KEY (destinated_to) REFERENCES companies (rfc);`);
      });
  }
};
