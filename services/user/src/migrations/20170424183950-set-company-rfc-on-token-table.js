'use strict';
const { Token } = require('../models');

module.exports = {
  up: () => {
    return Token.findAll({
      attributes: [ 'company_rfc', 'data' ]
    })
      .then(tokens => {
        const promises = tokens.map(value => {
          value.company_rfc = value.data.user_company_rfc;

          return value.save();
        });

        return Promise.all(promises);
      });
  },

  down: () => {
    return Token.update({
      company_rfc: 'X'
    });
  }
};
