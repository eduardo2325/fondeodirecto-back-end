'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'operation_costs',
      'fideicomiso_variable_fee', {
        type: Sequelize.DOUBLE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('operation_costs', 'fideicomiso_variable_fee');
  }
};
