'use strict';
'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'transactions',
      'invoice_uuid'
    ).then(() => {
      return queryInterface.addColumn(
        'transactions',
        'invoice_id', {
          type: Sequelize.INTEGER,
          references: {
            model: 'invoices',
            key: 'id'
          }
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'transactions',
      'invoice_id'
    ).then(() => {
      return queryInterface.addColumn(
        'transactions',
        'invoice_uuid', {
          type: Sequelize.STRING
        }
      );
    });
  }
};
