'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('operation_costs', 'company_rfc');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('operation_costs',
      'company_rfc', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      });
  }
};
