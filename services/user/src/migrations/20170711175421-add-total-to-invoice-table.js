'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'total', {
        type: Sequelize.DOUBLE,
        defaultValue: 0
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('invoices', 'total');
  }
};
