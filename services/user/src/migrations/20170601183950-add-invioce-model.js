'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'invoices',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        uuid: {
          type: Sequelize.STRING,
          unique: true
        },
        status: {
          type: Sequelize.STRING
        },
        client_rfc: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'companies',
            key: 'rfc'
          }
        },
        company_rfc: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'companies',
            key: 'rfc'
          }
        },
        subtotal: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        tax_total: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        tax_percentage: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        serie: {
          type: Sequelize.STRING
        },
        folio: {
          type: Sequelize.STRING
        },
        items: {
          type: Sequelize.JSONB
        },
        cadena_original: {
          type: Sequelize.TEXT
        },
        sat_digital_stamp: {
          type: Sequelize.STRING,
          allowNull: false
        },
        cfdi_digital_stamp: {
          type: Sequelize.STRING,
          allowNull: false
        },
        company_address: {
          type: Sequelize.STRING
        },
        company_postal_code: {
          type: Sequelize.STRING
        },
        written_amount: {
          type: Sequelize.STRING
        },
        emission_date: {
          type: Sequelize.STRING
        },
        cfdi: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        },
        deleted_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('invoices');
  }
};
