'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE operation_costs DROP
      CONSTRAINT operation_costs_company_rfc_fkey`);
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE operation_costs ADD CONSTRAINT
      operation_costs_company_rfc_fkey FOREIGN KEY (company_rfc) REFERENCES companies (rfc);`);
  }
};
