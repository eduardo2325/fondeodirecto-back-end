'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'pending_transactions',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false
        },
        company_rfc: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'companies',
            key: 'rfc'
          }
        },
        data: {
          type: Sequelize.JSONB
        },
        status: {
          type: Sequelize.STRING,
          allowNull: false
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('pending_transactions');
  }
};
