'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'tokens',
      'id', {
        type: Sequelize.INTEGER,
        unique: true,
        autoIncrement: true
      }
    ).then( () => {
      return queryInterface.addColumn(
        'tokens',
        'issuer', {
          type: Sequelize.INTEGER,
          references: {
            model: 'users',
            key: 'id'
          }
        }
      );
    }).then( () => {
      return queryInterface.sequelize.query(`ALTER TABLE tokens 
        DROP CONSTRAINT tokens_pkey`);
    }).then( () => {
      return queryInterface.sequelize.query(`ALTER TABLE tokens 
        ADD CONSTRAINT tokens_pkey PRIMARY KEY (id)`);
    }).then( () => {
      return queryInterface.sequelize.query(`ALTER TABLE tokens 
        ADD CONSTRAINT tokens_unique UNIQUE (token)`);
    });
  },

  down: queryInterface => {
    return queryInterface.sequelize.query(`ALTER TABLE tokens 
      DROP CONSTRAINT tokens_unique`)
      .then( () => {
        return queryInterface.sequelize.query(`ALTER TABLE tokens 
          DROP CONSTRAINT tokens_pkey`);
      })
      .then( () => {
        return queryInterface.removeColumn('tokens', 'id');
      })
      .then( () => {
        return queryInterface.removeColumn('tokens', 'issuer');
      })
      .then( () => {
        return queryInterface.sequelize.query(`ALTER TABLE tokens 
          ADD CONSTRAINT tokens_pkey PRIMARY KEY (token)`);
      });
  }
};
