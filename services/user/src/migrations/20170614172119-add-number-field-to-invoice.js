'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'number', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: '0'
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'number');
  }
};
