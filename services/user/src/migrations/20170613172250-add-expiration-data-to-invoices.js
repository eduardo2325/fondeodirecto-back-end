'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'expiration', {
        type: Sequelize.DATE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'expiration');
  }
};
