'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'client_rfc')
      .then(() => {
        return queryInterface.removeColumn('invoices', 'company_rfc');
      })
      .then(() => {
        return queryInterface.removeColumn('invoices', 'investor_rfc');
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('invoices',
      'client_rfc', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      })
      .then(() => {
        return queryInterface.addColumn('invoices',
          'company_rfc', {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
              model: 'companies',
              key: 'rfc'
            }
          });
      })
      .then(() => {
        return queryInterface.addColumn('invoices',
          'investor_rfc', {
            type: Sequelize.STRING,
            references: {
              model: 'companies',
              key: 'rfc'
            }
          });
      });
  }
};
