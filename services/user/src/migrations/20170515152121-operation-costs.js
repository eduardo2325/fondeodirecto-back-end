'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'operation_costs',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        annual_cost: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        reserve: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        fd_commission: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        fee: {
          type: Sequelize.DOUBLE,
          defaultValue: 0
        },
        company_rfc: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
          references: {
            model: 'companies',
            key: 'rfc'
          }
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('operation_costs');
  }
};
