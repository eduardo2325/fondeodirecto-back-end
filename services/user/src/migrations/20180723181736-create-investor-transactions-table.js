'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'investor_transactions',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        operation_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'operations',
            key: 'id'
          }
        },
        investor_company_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'companies',
            key: 'id'
          }
        },
        fund_request_date: {
          type: Sequelize.DATE
        },
        fund_date: {
          type: Sequelize.DATE
        },
        days_limit: {
          type: Sequelize.INTEGER
        },
        funded_amount: {
          type: Sequelize.DOUBLE
        },
        interest: {
          type: Sequelize.DOUBLE
        },
        fee: {
          type: Sequelize.DOUBLE
        },
        fee_type: {
          type: Sequelize.STRING
        },
        global_fee_percentage: {
          type: Sequelize.STRING
        },
        earnings: {
          type: Sequelize.DOUBLE
        },
        earnings_percentage: {
          type: Sequelize.DOUBLE
        },
        isr: {
          type: Sequelize.DOUBLE
        },
        isr_percentage: {
          type: Sequelize.DOUBLE
        },
        status: {
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        },
        deleted_at: {
          type: Sequelize.DATE
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('investor_transactions');
  }
};
