'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'invoices',
      'sat_digital_stamp', {
        type: Sequelize.TEXT,
        allowNull: false
      }
    ).then(() => {
      return queryInterface.changeColumn(
        'invoices',
        'cfdi_digital_stamp', {
          type: Sequelize.TEXT,
          allowNull: false
        }
      );
    }).then(() => {
      return queryInterface.addColumn(
        'invoices',
        'company_regime', {
          type: Sequelize.STRING
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'invoices',
      'sat_digital_stamp', {
        type: Sequelize.STRING,
        allowNull: false
      }
    ).then(() => {
      return queryInterface.changeColumn(
        'invoices',
        'cfdi_digital_stamp', {
          type: Sequelize.STRING,
          allowNull: false
        }
      );
    }).then(() => {
      return queryInterface.removeColumn('invoices', 'company_regime');
    });
  }
};
