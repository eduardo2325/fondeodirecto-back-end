'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'role', {
        type: Sequelize.STRING,
        allowNull: false
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('companies', 'role');
  }
};
