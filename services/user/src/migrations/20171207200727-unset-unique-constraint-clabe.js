'use strict';

module.exports = {
  up: (queryInterface) => {
    queryInterface.sequelize.query(
      'ALTER TABLE companies DROP CONSTRAINT clabe_unique_idx;'
    );

    return queryInterface.removeIndex('companies', 'clabe_unique_idx');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'companies',
      'clabe', {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }
    );
  }
};
