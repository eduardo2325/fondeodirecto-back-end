'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'tokens',
      {
        token: {
          type: Sequelize.STRING,
          primaryKey: true,
          unique: true
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false
        },
        email: {
          type: Sequelize.STRING
        },
        data: {
          type: Sequelize.JSONB
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('tokens');
  }
};
