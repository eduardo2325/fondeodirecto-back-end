'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'investor_transactions',
      'perception', {
        type: Sequelize.DOUBLE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('investor_transactions', 'perception');
  }
};
