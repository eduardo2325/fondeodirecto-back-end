'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE sessions DROP CONSTRAINT sessions_company_rfc_fkey');
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE sessions ADD CONSTRAINT sessions_company_rfc_fkey
      FOREIGN KEY (company_rfc) REFERENCES companies (rfc);`);
  }
};
