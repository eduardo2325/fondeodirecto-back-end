'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'bank_codes',
      {
        id: {
          type: Sequelize.STRING,
          primaryKey: true,
          unique: true
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: ''
        },
        business_name: {
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('bank_codes');
  }
};
