'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'pending_transactions',
      'bank_report_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'bank_reports',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('pending_transactions', 'bank_report_id');
  }
};
