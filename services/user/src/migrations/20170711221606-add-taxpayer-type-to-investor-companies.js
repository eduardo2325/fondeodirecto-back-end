'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'taxpayer_type', {
        type: Sequelize.STRING
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('companies', 'taxpayer_type');
  }
};
