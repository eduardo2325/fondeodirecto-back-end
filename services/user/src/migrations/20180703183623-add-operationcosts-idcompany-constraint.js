'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'operation_costs',
      'company_id', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'id'
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn('operation_costs', 'company_id');
  }
};
