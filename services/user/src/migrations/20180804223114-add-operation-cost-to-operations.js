'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'operations',
      'operation_cost', {
        type: Sequelize.DOUBLE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('operations', 'operation_cost');
  }
};
