'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'transactions',
      'originated_from_id', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'id'
        }
      }
    )
      .then(() => {
        return queryInterface.addColumn(
          'transactions',
          'destinated_to_id', {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'companies',
              key: 'id'
            }
          }
        );
      });
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn('transactions', 'originated_from_id')
      .then(() => {
        return queryInterface.removeColumn('transactions', 'destinated_to_id');
      });
  }
};
