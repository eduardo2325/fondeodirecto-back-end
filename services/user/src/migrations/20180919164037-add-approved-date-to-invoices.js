'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'approved_date', {
        type: Sequelize.DATE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'approved_date');
  }
};
