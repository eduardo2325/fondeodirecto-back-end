'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE invoices DROP CONSTRAINT invoices_client_rfc_fkey')
      .then(() => {
        return queryInterface.sequelize.query('ALTER TABLE invoices DROP CONSTRAINT invoices_company_rfc_fkey');
      })
      .then(() => {
        return queryInterface.sequelize.query('ALTER TABLE invoices DROP CONSTRAINT invoices_investor_rfc_fkey');
      });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE invoices ADD CONSTRAINT invoices_client_rfc_fkey
      FOREIGN KEY (client_rfc) REFERENCES companies (rfc);`)
      .then(() => {
        return queryInterface.sequelize.query(`ALTER TABLE invoices ADD CONSTRAINT invoices_company_rfc_fkey
          FOREIGN KEY (company_rfc) REFERENCES companies (rfc);`);
      })
      .then(() => {
        return queryInterface.sequelize.query(`ALTER TABLE invoices ADD CONSTRAINT invoices_investor_rfc_fkey
          FOREIGN KEY (investor_rfc) REFERENCES companies (rfc);`);
      });
  }
};
