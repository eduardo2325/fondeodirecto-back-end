'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'pending_transactions',
      'company_id', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'id'
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn('pending_transactions', 'company_id');
  }
};
