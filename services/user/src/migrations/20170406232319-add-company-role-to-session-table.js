'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('sessions', 'role')
      .then(() => {
        return queryInterface.addColumn(
          'sessions',
          'user_role', {
            type: Sequelize.STRING,
            allowNull: false
          }
        );
      })
      .then(() => {
        return queryInterface.addColumn(
          'sessions',
          'company_role', {
            type: Sequelize.STRING,
            allowNull: false
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('sessions', 'user_role')
      .then(() => {
        return queryInterface.removeColumn('sessions', 'company_role');
      })
      .then(() => {
        return queryInterface.addColumn(
          'sessions',
          'role', {
            type: Sequelize.STRING,
            allowNull: false
          }
        );
      });
  }
};
