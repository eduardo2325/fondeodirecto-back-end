'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE users DROP CONSTRAINT company_rfc_foreign_idx');
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE users ADD CONSTRAINT company_rfc_foreign_idx FOREIGN KEY
      (company_rfc) REFERENCES companies (rfc);`);
  }
};
