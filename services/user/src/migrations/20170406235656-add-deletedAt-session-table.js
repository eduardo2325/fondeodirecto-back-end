'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'sessions',
      'deleted_at', {
        type: Sequelize.DATE
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('sessions', 'deleted_at');
  }
};
