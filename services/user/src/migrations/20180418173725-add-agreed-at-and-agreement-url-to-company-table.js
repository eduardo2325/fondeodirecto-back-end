'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'agreed_at', {
        type: Sequelize.DATE
      }
    )
      .then(() => {
        return queryInterface.addColumn(
          'companies',
          'agreement_url', {
            type: Sequelize.STRING
          }
        );
      });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('companies', 'agreed_at')
      .then(() => {
        return queryInterface.removeColumn('companies', 'agreement_url');
      });
  }
};
