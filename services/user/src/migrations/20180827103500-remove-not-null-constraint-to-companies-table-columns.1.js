'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN holder DROP NOT NULL')
      .then( () => {
        return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN clabe DROP NOT NULL');
      })
      .then( () => {
        return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN bank DROP NOT NULL');
      })
      .then( () => {
        return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN bank_account DROP NOT NULL');
      });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN holder SET NOT NULL')
      .then( () => {
        return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN clabe SET NOT NULL');
      })
      .then( () => {
        return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN bank SET NOT NULL');
      })
      .then( () => {
        return queryInterface.sequelize.query('ALTER TABLE companies ALTER COLUMN bank_account SET NOT NULL');
      });
  }
};
