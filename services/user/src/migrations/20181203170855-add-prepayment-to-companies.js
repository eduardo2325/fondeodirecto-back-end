'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'prepayment', {
        type: Sequelize.BOOLEAN
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn('companies', 'prepayment');
  }
};
