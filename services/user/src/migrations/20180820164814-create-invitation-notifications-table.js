'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'invitation_notifications',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        token: {
          type: Sequelize.STRING,
          allowNull: false
        },
        event_type: {
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('invitation_notifications');
  }
};
