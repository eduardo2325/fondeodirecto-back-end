'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'operation_costs',
      'variable_fee_percentage', {
        type: Sequelize.DOUBLE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('operation_costs', 'variable_fee_percentage');
  }
};
