'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE users DROP CONSTRAINT users_email_key;');
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE users ADD CONSTRAINT users_email_key UNIQUE (email);');
  }
};
