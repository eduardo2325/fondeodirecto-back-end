'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('sessions', 'company_agreed', {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('sessions', 'company_agreed');
  }
};
