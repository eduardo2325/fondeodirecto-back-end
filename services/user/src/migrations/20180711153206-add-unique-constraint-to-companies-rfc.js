'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE companies ADD CONSTRAINT companies_rfc_key UNIQUE (rfc);');
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.query('ALTER TABLE companies DROP CONSTRAINT companies_rfc_key;');
  }
};
