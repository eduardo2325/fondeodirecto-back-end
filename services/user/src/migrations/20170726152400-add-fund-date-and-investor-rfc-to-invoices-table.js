'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'fund_date', {
        type: Sequelize.DATE
      }
    )
      .then(() => {
        return queryInterface.addColumn(
          'invoices',
          'investor_rfc', {
            type: Sequelize.STRING,
            references: {
              model: 'companies',
              key: 'rfc'
            }
          }
        );
      });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'fund_date')
      .then(() => {
        return queryInterface.removeColumn('invoices', 'investor_rfc');
      });
  }
};
