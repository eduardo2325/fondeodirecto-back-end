'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('users', 'company_rfc');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('users',
      'company_rfc', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      }
    );
  }
};
