'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'bank', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'X'
      }
    )
      .then(() => {
        return queryInterface.addColumn(
          'companies',
          'bank_account', {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: 'X'
          }
        );
      });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('companies', 'bank')
      .then(() => {
        return queryInterface.removeColumn('companies', 'bank_account');
      });
  }
};
