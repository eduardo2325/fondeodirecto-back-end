'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'client_company_id', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'id'
        }
      }
    )
      .then(() => {
        return queryInterface.addColumn(
          'invoices',
          'company_id', {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'companies',
              key: 'id'
            }
          }
        );
      })
      .then(() => {
        return queryInterface.addColumn(
          'invoices',
          'investor_company_id', {
            type: Sequelize.INTEGER,
            references: {
              model: 'companies',
              key: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'client_company_id')
      .then(() => {
        return queryInterface.removeColumn('invoices', 'company_id');
      })
      .then(() => {
        return queryInterface.removeColumn('invoices', 'investor_company_id');
      });
  }
};
