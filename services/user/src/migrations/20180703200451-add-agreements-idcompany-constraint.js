'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'agreements',
      'company_id', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'id'
        }
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('agreements', 'company_id');
  }
};
