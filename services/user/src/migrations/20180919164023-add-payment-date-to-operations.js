'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'operations',
      'payment_date', {
        type: Sequelize.DATE
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('operations', 'payment_date');
  }
};
