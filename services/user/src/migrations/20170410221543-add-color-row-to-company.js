'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'color', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: '#5A52FF'
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('companies', 'color');
  }
};
