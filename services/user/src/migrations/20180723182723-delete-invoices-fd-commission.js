'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'fd_commission');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'fd_commission', {
        type: Sequelize.DOUBLE
      }
    );
  }
};
