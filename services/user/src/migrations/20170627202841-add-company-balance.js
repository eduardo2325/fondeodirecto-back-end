'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'balance', {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('companies', 'balance');
  }
};
