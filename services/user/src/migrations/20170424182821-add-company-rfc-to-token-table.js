'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'tokens',
      'company_rfc', {
        type: Sequelize.STRING,
        defaultValue: 'X'
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('tokens', 'company_rfc');
  }
};
