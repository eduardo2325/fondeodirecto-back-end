'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'reserve');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'reserve', {
        type: Sequelize.DOUBLE
      }
    );
  }
};
