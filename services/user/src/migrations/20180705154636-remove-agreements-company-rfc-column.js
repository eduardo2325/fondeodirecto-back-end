'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('agreements', 'company_rfc');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'agreements',
      'company_rfc', {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      }
    );
  }
};
