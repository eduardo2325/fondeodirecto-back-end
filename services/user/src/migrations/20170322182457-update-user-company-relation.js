'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'users',
      'company_rfc', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'users',
      'company_rfc', {
        type: Sequelize.STRING,
        allowNull: false
      }
    );
  }
};
