'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'sessions',
      'company_rfc', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('sessions', 'company_rfc');
  }
};
