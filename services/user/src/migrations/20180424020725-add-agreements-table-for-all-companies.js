'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'agreements',
      {
        company_rfc: {
          type: Sequelize.STRING,
          primaryKey: true,
          unique: false
        },
        user_role: {
          type: Sequelize.STRING,
          primaryKey: true,
          unique: false
        },
        agreed_at: {
          type: Sequelize.DATE,
          allowNull: true
        },
        agreement_url: {
          type: Sequelize.STRING,
          allowNull: true
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.dropTable('agreements');
  }
};
