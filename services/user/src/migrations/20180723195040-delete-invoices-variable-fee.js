'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'variable_fee_percentage');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'variable_fee_percentage', {
        type: Sequelize.DOUBLE
      }
    );
  }
};
