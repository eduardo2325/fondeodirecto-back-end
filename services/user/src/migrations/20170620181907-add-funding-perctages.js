'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'published', {
        type: Sequelize.DATE
      }
    ).then(() => {
      return queryInterface.addColumn(
        'invoices',
        'annual_cost', {
          type: Sequelize.DOUBLE
        }
      );
    }).then(() => {
      return queryInterface.addColumn(
        'invoices',
        'reserve', {
          type: Sequelize.DOUBLE
        }
      );
    }).then(() => {
      return queryInterface.addColumn(
        'invoices',
        'fd_commission', {
          type: Sequelize.DOUBLE
        }
      );
    });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'published').then(() => {
      return queryInterface.removeColumn('invoices', 'annual_cost');
    }).then(() => {
      return queryInterface.removeColumn('invoices', 'reserve');
    }).then(() => {
      return queryInterface.removeColumn('invoices', 'fd_commission');
    });
  }
};
