'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'tokens',
      'company_id', {
        type: Sequelize.INTEGER
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('tokens', 'company_id');
  }
};
