'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('site_configs', {
      key: {
        type: Sequelize.STRING,
        primaryKey: true,
        unique: true
      },
      values: {
        type: Sequelize.JSONB
      },
      created_at: {
        type: Sequelize.DATE
      },
      updated_at: {
        type: Sequelize.DATE
      },
      deleted_at: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('site_configs');
  }
};
