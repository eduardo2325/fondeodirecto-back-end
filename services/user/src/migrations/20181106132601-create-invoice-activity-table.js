'use strict';
// tabla pivote

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'invoice_activity',
      {
        invoice_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'invoices',
            key: 'id'
          }
        },
        activity_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'user_activities',
            key: 'id'
          }
        }
      }
    );
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('invoice_activity');
  }
};
