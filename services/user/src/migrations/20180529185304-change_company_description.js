'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'companies',
      'description', {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: ''
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'companies',
      'description', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ''
      }
    );
  }
};
