'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('pending_transactions', 'company_rfc');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('pending_transactions',
      'company_rfc', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      });
  }
};
