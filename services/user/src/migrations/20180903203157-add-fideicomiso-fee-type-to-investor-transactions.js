'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'investor_transactions',
      'fideicomiso_fee_type', {
        type: Sequelize.STRING
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn('investor_transactions', 'fideicomiso_fee_type');
  }
};
