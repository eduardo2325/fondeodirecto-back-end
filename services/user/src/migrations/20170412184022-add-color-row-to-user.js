'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'users',
      'color', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: '#5A52FF'
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('users', 'color');
  }
};
