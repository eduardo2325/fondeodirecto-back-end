'use strict';

module.exports = {
  up: (queryInterface) => {
    const alterQuery = `
      ALTER TABLE invoices ALTER COLUMN emission_date TYPE TIMESTAMP WITH TIME ZONE
        using emission_date::timestamptz;
    `;

    return queryInterface.sequelize.query(alterQuery);
  },

  down: (queryInterface) => {
    const alterQuery = `
      ALTER TABLE invoices ALTER COLUMN emission_date TYPE VARCHAR(255)
        using to_char(emission_date, 'YYYY-MM-DD"T"HH:MI:SS OF');
    `;

    return queryInterface.sequelize.query(alterQuery);
  }
};
