'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE pending_transactions DROP CONSTRAINT
      pending_transactions_company_rfc_fkey`);
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`ALTER TABLE pending_transactions ADD CONSTRAINT
      pending_transactions_company_rfc_fkey FOREIGN KEY (company_rfc) REFERENCES companies (rfc);`);
  }
};
