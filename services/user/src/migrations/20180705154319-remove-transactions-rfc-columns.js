'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('transactions', 'originated_from')
      .then(() => {
        return queryInterface.removeColumn('transactions', 'destinated_to');
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('transactions',
      'originated_from', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'companies',
          key: 'rfc'
        }
      })
      .then(() => {
        return queryInterface.addColumn('transactions',
          'destinated_to', {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
              model: 'companies',
              key: 'rfc'
            }
          });
      });
  }
};
