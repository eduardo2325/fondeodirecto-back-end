'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'companies',
      'description', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ''
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('companies', 'description');
  }
};
