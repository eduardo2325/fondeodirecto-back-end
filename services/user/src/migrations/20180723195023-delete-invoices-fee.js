'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn('invoices', 'fee');
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'invoices',
      'fee', {
        type: Sequelize.DOUBLE
      }
    );
  }
};
