'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'new_invoice_notifications',
      'new_invoice_token', {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'tokens',
          key: 'token'
        }
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('new_invoice_notifications', 'new_invoice_token');
  }
};
