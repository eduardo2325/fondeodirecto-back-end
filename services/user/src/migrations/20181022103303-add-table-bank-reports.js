'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'bank_reports',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          unique: true,
          autoIncrement: true
        },
        generated_at: {
          type: Sequelize.DATE,
          allowNull: false
        },
        total_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        total_generated: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        generated_by: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        link: {
          type: Sequelize.STRING,
          allowNull: false
        },
        created_at: {
          type: Sequelize.DATE
        },
        updated_at: {
          type: Sequelize.DATE
        },
        deleted_at: {
          type: Sequelize.DATE
        }
      }
    );
  },

  down: (queryInterface) => {
    return queryInterface.dropTable('bank_reports');
  }
};
