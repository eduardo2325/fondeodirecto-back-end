'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'tokens',
      'deleted_at', {
        type: Sequelize.DATE
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn('tokens', 'deleted_at');
  }
};
