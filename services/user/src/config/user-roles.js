const internalRoles = [
  {
    name: 'Admin',
    value: 'ADMIN'
  },
  {
    name: 'Inversionista',
    value: 'INVESTOR'
  }
];
const publicRoles = [
  {
    name: 'Cuentas por cobrar',
    value: 'CXC'
  },
  {
    name: 'Cuentas por pagar',
    value: 'CXP'
  }
];
const rolesByTypeOfCompany = {
  'ADMIN': [
    'ADMIN'
  ],
  'INVESTOR': [
    'INVESTOR'
  ],
  'COMPANY': [
    'CXC',
    'CXP'
  ]
};
const cxpActions = [
  'ADMIN',
  'CXP'
];
const cxcActions = [
  'ADMIN',
  'CXC'
];

module.exports = {
  internalRoles,
  publicRoles,
  roles: internalRoles.concat(publicRoles),
  byCompany: rolesByTypeOfCompany,
  cxpActions,
  cxcActions
};
