const { sequelize } = require('../../models');
const _ = require('lodash');
const moment = require('/var/lib/core/js/moment');

function purchasedInvoices(investorCompanyId, year, month) {
  const requestedMonth = moment().month(month);

  const rawQuery = `
    SELECT EXTRACT(DAY FROM pt.created_at) AS day, 'Compra de factura' AS description,
    it.id AS operation, i.total AS factoraje,
    i.total AS cash
    FROM pending_transactions AS pt
    INNER JOIN invoices i ON i.id = CAST(pt.data->>'invoice_id' AS INT)
    INNER JOIN operations AS op ON op.invoice_id = i.id
    INNER JOIN investor_transactions AS it ON it.operation_id = op.id
    WHERE i.investor_company_id = ${investorCompanyId}
    AND pt.type LIKE '%WITHDRAW%'
    AND pt.status LIKE '%APPROVED%'
    AND EXTRACT(MONTH FROM pt.created_at) = ${requestedMonth.format('M')}
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`;

  return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT });
}

function investorCashWithDrawals(investorCompanyId, year, month) {
  const requestedMonth = moment().month(month);

  const rawQuery = `
    SELECT EXTRACT(DAY FROM pt.created_at) AS day, 'Retiro de efectivo' AS description,
    NULL AS operation,
    NULL AS factoraje,
    pt.amount AS cash
    FROM pending_transactions AS pt
    WHERE pt.company_id = ${investorCompanyId}
    AND pt.status LIKE '%APPROVED%'
    AND pt.type LIKE '%WITHDRAW%'
    AND pt.data->>'invoice_id' IS NULL
    AND EXTRACT(MONTH FROM pt.created_at) = ${requestedMonth.format('M')}
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`;

  return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT });
}

function investorCashDeposits(investorCompanyId, year, month) {
  const requestedMonth = moment().month(month);

  const rawQuery = `
    SELECT EXTRACT(DAY FROM pt.created_at) AS day, 'Depósito de efectivo' AS description,
    NULL AS operation,
    NULL AS factoraje,
    pt.amount AS cash
    FROM pending_transactions AS pt
    WHERE pt.company_id = ${investorCompanyId}
    AND pt.status = 'APPROVED'
    AND pt.type = 'DEPOSIT'
    AND pt.data->>'invoice_id' IS NULL
    AND EXTRACT(MONTH FROM pt.created_at) = ${requestedMonth.format('M')}
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`;

  return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT });
}

function payedInvoices(investorCompanyId, year, month) {
  const requestedMonth = moment().month(month);

  const rawQuery = `
    SELECT EXTRACT(DAY FROM t.created_at) AS day, it.id AS operation, i.total,
    it.interest, it.earnings, it.fee AS fondeo_fee,
    it.fideicomiso_fee AS fideicomiso_fee,
    it.isr
    FROM transactions AS t
    INNER JOIN invoices AS i ON i.id = t.invoice_id
    INNER JOIN operations AS op ON op.invoice_id = i.id
    INNER JOIN investor_transactions AS it ON it.operation_id = op.id
    WHERE t.destinated_to_id = ${investorCompanyId}
    AND EXTRACT(MONTH FROM t.created_at) = ${requestedMonth.format('M')}
    AND EXTRACT(YEAR FROM t.created_at) = ${year}`;

  return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT });
}

function formatData(data) {
  const transactionsToString = [ 'day', 'operation', 'factoraje', 'cash' ];

  _.each(data, function(value) {
    _.each(value, function(v, i) {
      if (v && _.includes(transactionsToString, i)) {
        value[i] = String(v);
      }

      value[i] = !v && v !== 0 && _.includes(transactionsToString, i) ? '' : String(v);
    });
  });
  return data;
}

function formStatementResponse(transactions) {
  const response = [];

  let invoicePayment = {};
  let isr = {};
  let earnings = {};
  let fd_fee = {};
  let fideicomiso_fee = {};

  transactions.forEach((value, index) => {
    if (index !== 3) {
      value.forEach(v => {
        response.push(v);
      });
    } else {
      value.forEach(v => {
        const baseObject = _.pick(v, [ 'day', 'operation' ]);

        invoicePayment = _.clone(baseObject);
        invoicePayment = _.assign(invoicePayment, { 'factoraje': v.total },
          { 'description': 'Liquidación de factura' }, { 'cash': v.total }
        );

        isr = _.clone(baseObject);
        isr = _.assign(isr, { 'factoraje': null },
          { 'description': 'Retención ISR - 20%' }, { 'cash': v.isr }
        );

        earnings = _.clone(baseObject);
        earnings = _.assign(earnings, { 'factoraje': null },
          { 'description': 'Utilidad de operación' }, { 'cash': v.earnings }
        );

        fd_fee = _.clone(baseObject);
        fd_fee = _.assign(fd_fee, { 'factoraje': null },
          { 'description': 'Comisión - Fondeo Directo' }, { 'cash': v.fondeo_fee }
        );

        fideicomiso_fee = _.clone(baseObject);
        fideicomiso_fee = _.assign(fideicomiso_fee, { 'factoraje': null },
          { 'description': 'Comisión - Fideicomiso' }, { 'cash': v.fideicomiso_fee }
        );

        response.push(...[ invoicePayment, isr, earnings, fd_fee ]);

        if (v.fideicomiso_fee) {
          response.push(fideicomiso_fee);
        }
      });
    }
  });
  const ordered = _.orderBy(response, [ 'day', 'operation' ], [ 'asc', 'asc' ]);

  return formatData(ordered);
}

function investorCashWithDrawalsFromInvoices(investorCompanyId, year, month) {
  const requestedMonth = moment().month(month);

  const rawQuery = `
    SELECT pt.created_at, EXTRACT(DAY FROM pt.created_at) AS day, 'Resta de balance por factura' AS description,
    pt.funded_amount as cash 
    FROM investor_transactions pt 
    WHERE pt.investor_company_id = ${investorCompanyId} 
    AND pt.status = 'COMPLETED'
    AND EXTRACT(MONTH FROM pt.created_at) = ${ parseInt(requestedMonth.format('M'), 10) }
    AND EXTRACT(YEAR FROM pt.created_at) = ${year}`;

  return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT });
}

function investorCashDepositsFromInvoices(investorCompanyId, year, month) {
  const requestedMonth = moment().month(month);

  const rawQuery = `
    SELECT t.created_at, amount as cash,
      'Suma de balance por factura' AS description 
    from transactions t where
    t.is_payment=false 
    AND t.type='DEPOSIT'
    AND t.destinated_to_id = ${investorCompanyId}
    AND EXTRACT(MONTH FROM t.created_at) = ${ parseInt(requestedMonth.format('M'), 10) }
    AND EXTRACT(YEAR FROM t.created_at) = ${year}`;

  return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT });
}


module.exports = {
  purchasedInvoices,
  investorCashWithDrawals,
  investorCashDeposits,
  payedInvoices,
  formStatementResponse,
  investorCashWithDrawalsFromInvoices,
  investorCashDepositsFromInvoices,
  formatData
};
