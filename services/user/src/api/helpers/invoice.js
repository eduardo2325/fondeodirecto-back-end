const { isrPercentage } = require('../../config/constants');

// this helps to round from, e.g. 1.277777 -> 1.3
function round(value) {
  return Math.round(value * 100) / 100;
}

class InvoiceHelper {
  constructor(invoice, extraData) {
    this.invoice = {
      total: invoice.total,
      is_available: invoice.dataValues ? invoice.dataValues.is_available : undefined,
      ...extraData
    };

    this.operation = {
      fd_commission_percentage: invoice.Operation ? invoice.Operation.fd_commission_percentage : undefined,
      reserve_percentage: invoice.Operation ? invoice.Operation.reserve_percentage : undefined,
      annual_cost_percentage: invoice.Operation ? invoice.Operation.annual_cost_percentage : undefined
    };

    this.today = new Date();
    this.today.setHours(0, 0, 0, 0);

    this.fundDay = invoice.fund_date ? new Date(invoice.fund_date) : undefined;
    this.expiration = new Date(invoice.expiration);
  }

  static diffDays(fromDate, toDate) {
    if (!(toDate instanceof Date)) {
      return 0;
    }

    // passing null or undefined to Date constructor will not work, ever
    const initialDate = fromDate ? new Date(fromDate) : new Date();

    initialDate.setHours(0, 0, 0, 0);

    const endDate = new Date(toDate);

    endDate.setHours(0, 0, 0, 0);

    const expirationDate = endDate.getTime();
    const startDate = initialDate.getTime();
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

    return Math.round(Math.abs((expirationDate - startDate) / oneDay));
  }

  /* General */
  getStartAndExpirationDayDifference(isFundDay = false) {
    const expirationDate = this.expiration;
    const currentDay = isFundDay && this.fundDay;

    return InvoiceHelper.diffDays(currentDay || this.today, expirationDate);
  }

  getTotalOperationCost(diffDays) {
    const interestRate = this.getEstimateInterestCalculation(diffDays);
    const fdComissionPercentage = this.invoice.percentages.fd_commission / 100 * this.invoice.total;
    const totalOperationCost = round(interestRate + fdComissionPercentage * diffDays / 360);

    return totalOperationCost;
  }

  getInvoicePaymentEstimates(diffDays) {
    const total = this.invoice.total;
    const annualCost = this.operation.annual_cost_percentage / 100;
    const reservePercentage = this.operation.reserve_percentage / 100;
    const interest = this.getInterestCalculation(total, annualCost, diffDays);

    const fdCommission = this.operation.fd_commission_percentage / 100;
    const commission = round(this.invoice.total * fdCommission * (diffDays / 360));

    const fundTotal = total - interest - commission;
    const reserve = round(total * reservePercentage);
    const fundPayment = fundTotal - reserve;
    const expirationPayment = reserve;
    const operationCost = round(interest + commission);

    return {
      total,
      annualCost,
      reservePercentage,
      fdCommission,
      interest,
      commission,
      fundTotal,
      reserve,
      fundPayment,
      expirationPayment,
      operationCost
    };
  }

  getInvestorTransactionEstimates(diffDays) {
    const total = this.invoice.total;
    const {
      isr,
      perception,
      earnings,
      earningsPercentage,
      variableFee,
      fideicomisoFee,
      netIncome,
      netIncomePercentage
    } = this.getInvestorFundEstimates(diffDays);

    const feeType = variableFee === this.invoice.percentages.fee ? 'Fixed fee' : 'Variable fee';
    let fideicomisoFeeType;

    if (fideicomisoFee) {
      fideicomisoFeeType = fideicomisoFee === this.invoice.percentages.fideicomiso_fee ?
        'Fixed fee' : 'Variable fee';
    }

    const isrTotalPercent = isr / total * 100;

    return {
      isr,
      perception,
      earnings,
      earningsPercentage,
      variableFee,
      feeType,
      fideicomisoFee,
      fideicomisoFeeType,
      isrTotalPercent,
      netIncome,
      netIncomePercentage
    };
  }

  getFundEstimates(diffDays) {
    const total = this.invoice.total;
    const annualCostPercentage = this.invoice.percentages.annual_cost / 100;
    const reservePercentage = this.invoice.percentages.reserve / 100;
    const fdCommissionPercentage = this.invoice.percentages.fd_commission / 100;
    const interest = this.getInterestCalculation(total, annualCostPercentage, diffDays);
    const variableFee = this.getInvestorFee(interest);

    const commission = round(total * fdCommissionPercentage * diffDays / 360);
    const fundTotal = round(total - interest - commission);
    const reserve = total * reservePercentage;
    const annualCost = total * annualCostPercentage;
    const fdCommission = total * fdCommissionPercentage;
    const factorable = round(total - reserve);
    const subtotal = round(factorable - interest);
    const fundPayment = round(subtotal - commission);

    const expirationPayment = reserve;
    const operationCost = interest + commission;

    return {
      factorable,
      subtotal,
      variableFee,
      total,
      annualCostPercentage,
      annualCost,
      reservePercentage,
      reserve,
      fdCommissionPercentage,
      fdCommission,
      interest,
      commission,
      fundTotal,
      fundPayment,
      expirationPayment,
      operationCost
    };
  }

  getInvestorProfitEstimates(diffDays) {
    const total = this.invoice.total;
    const annualCost = this.operation.annual_cost_percentage / 100;
    const gain = this.getInterestCalculation(total, annualCost, diffDays);
    const gain_percentage = gain / total * 100;

    return {
      gain,
      gain_percentage
    };
  }

  getInvestorGains(diffDays) {
    const total = this.invoice.total;
    const annualCostPercentage = this.operation.annual_cost_percentage / 100;
    const earnings = this.getInterestCalculation(total, annualCostPercentage, diffDays);
    const variableFee = this.getInvestorFee(earnings);
    const fideicomisoFee = this.getInvestorFideicomisoFee(earnings, variableFee);

    let perception = fideicomisoFee ? earnings - variableFee - fideicomisoFee :
      earnings - variableFee;

    let isr = 0;

    if (this.invoice.taxpayerType === 'physical') {
      isr = earnings * isrPercentage;
      perception = perception - isr;
    }

    return { isr, perception };
  }

  getInvestorFundEstimates(diffDays) {
    const total = this.invoice.total;
    const annualCost = this.operation.annual_cost_percentage / 100;
    const earnings = this.getInterestCalculation(total, annualCost, diffDays);
    const variableFee = this.getInvestorFee(earnings);
    const fideicomisoFee = this.getInvestorFideicomisoFee(earnings, variableFee);
    const earningsPercentage = earnings / total * 100;
    const gain = fideicomisoFee ? earnings - variableFee - fideicomisoFee :
      earnings - variableFee;

    let perception = total + gain;
    let isr = 0;
    let include_isr = false;
    let netIncome = 0;
    let netIncomePercentage = 0;

    if (this.invoice.taxpayerType === 'physical') {
      isr = earnings * isrPercentage;
      include_isr = true;
      perception = perception - isr;
    }

    netIncome = perception - total;
    netIncomePercentage = netIncome * 100 / total;

    return {
      isr,
      include_isr,
      perception,
      earnings,
      earningsPercentage,
      variableFee,
      fideicomisoFee,
      netIncome,
      netIncomePercentage
    };
  }

  getAdminCxcPaymentEstimates() {
    const total = this.invoice.total;
    const annualCost = this.operation.annual_cost_percentage / 100;
    const reservePercentage = this.operation.reserve_percentage / 100;
    const fdCommission = this.operation.fd_commission_percentage / 100;
    let commission = total * fdCommission;
    const reserve = total * reservePercentage;
    const expirationPayment = reserve;

    let diffDays = 0;
    let interest = 0;
    let interestPercentage = 0;
    let interestPayment = 0;
    let fundTotal = 0;
    let fundPayment = 0;

    if (this.fundDay) {
      diffDays = this.getStartAndExpirationDayDifference(true);
      interest = this.getInterestCalculation(total, annualCost, diffDays);
      commission = round(commission * (diffDays / 360));
      interestPayment = round(interest + commission);
      interestPercentage = round(100 * (interestPayment / total));
      fundTotal = round(total - interest - commission);
      fundPayment = round(fundTotal - reserve);
    }

    return {
      interest,
      interestPercentage,
      interestPayment,
      reserve,
      commission,
      total,
      diffDays,
      fundTotal,
      fundPayment,
      expirationPayment
    };
  }

  getAdminInvestorPaymentEstimates(diffDays) {
    const total = this.invoice.total;
    const annualCost = this.operation.annual_cost_percentage / 100;
    const earnings = this.getInterestCalculation(total, annualCost, diffDays);
    const earningsPercentage = earnings / total;
    const fundTotal = total;
    const variableFee = this.getInvestorFee(earnings);
    const fideicomisoFee = this.getInvestorFideicomisoFee(earnings, variableFee);

    let gain = fideicomisoFee ? earnings - variableFee - fideicomisoFee :
      earnings - variableFee;
    const feePercentage = variableFee / total;
    let gainPercentage = gain / total;
    let perception = fundTotal + gain;
    let isr = 0;
    let include_isr = false;

    if (this.invoice.taxpayerType === 'physical') {
      isr = earnings * isrPercentage;
      include_isr = true;
      perception = perception - isr;

      gain = gain - isr;
      gainPercentage = gain / fundTotal;
    }

    return {
      isr,
      include_isr,
      perception,
      fundTotal,
      earnings,
      earningsPercentage,
      gainPercentage,
      feePercentage,
      variableFee,
      fideicomisoFee,
      gain
    };
  }

  getAdminOperationEstimates() {
    let diffDays = 0;
    let total = 0;
    let annualCost = 0;
    let earnings = 0;
    let variableFee = 0;
    let fideicomisoFee = 0;
    let totalFee = 0;

    if (this.fundDay) {
      total = this.invoice.total;
      annualCost = this.operation.annual_cost_percentage / 100;
      diffDays = this.getStartAndExpirationDayDifference(true);
      earnings = this.getInterestCalculation(total, annualCost, diffDays);
      variableFee = round(this.getInvestorFee(earnings));
      fideicomisoFee = round(this.getInvestorFideicomisoFee(earnings, variableFee));
      if (fideicomisoFee) {
        totalFee = variableFee + fideicomisoFee;
      }
    }

    const fdCommission = this.operation.fd_commission_percentage / 100;
    const commission = round(this.invoice.total * fdCommission * (diffDays / 360));
    const earningsFd = round(variableFee + commission);

    return {
      diffDays,
      commission,
      variableFee,
      totalFee,
      earningsFd
    };
  }

  /* Interest */
  getEstimateInterestCalculation(invoiceDuration) {
    const invoiceTotal = this.invoice.total;
    const annualCost = this.invoice.percentages.annual_cost;

    return annualCost / 100 / 360 * invoiceDuration * invoiceTotal;
  }

  getInterestCalculation(invoiceTotal, annualCostPercentage, invoiceDuration) {
    return round(annualCostPercentage / 360 * invoiceTotal * invoiceDuration);
  }

  /* Comission */
  getFDComissionAmount(invoiceTotal, fdComissionPercentage, invoiceDuration) {
    return invoiceTotal * fdComissionPercentage * (invoiceDuration / 360);
  }

  getInvestorFee(earnings) {
    let globalFee = 0;
    let feePartBasedOnGlobalFee = 0;
    const fixedFee = this.invoice.percentages.fee || 0;
    const variableFeePercentage = this.invoice.percentages.variable_fee_percentage;
    const variableFeePercentageBaseOnEarnings = fixedFee / earnings * 100;

    if (variableFeePercentage && this.invoice.global_fee_percentage) {
      globalFee = Number(this.invoice.global_fee_percentage.split('/').pop());
      feePartBasedOnGlobalFee = fixedFee * globalFee;
      let variableFee = earnings * (variableFeePercentage / 100);

      if (feePartBasedOnGlobalFee < earnings && variableFeePercentageBaseOnEarnings > variableFeePercentage) {
        variableFee = fixedFee;
      }
      return variableFee;
    }
    return fixedFee;
  }

  getInvestorFideicomisoFee(earnings, fondeoFee) {
    const fideicomisoFixedFee = this.invoice.percentages.fideicomiso_fee ?
      this.invoice.percentages.fideicomiso_fee : null;
    const fideicomisoVariableFeePercentage = this.invoice.percentages.fideicomiso_variable_fee ?
      this.invoice.percentages.fideicomiso_variable_fee : null;
    const fideicomisoVariableFee = earnings * (fideicomisoVariableFeePercentage / 100);

    let fideicomisoFee;

    if (fideicomisoFixedFee || fideicomisoVariableFeePercentage) {
      fideicomisoFee = fondeoFee === this.invoice.percentages.fee ? fideicomisoFixedFee : fideicomisoVariableFee;
    }
    return fideicomisoFee;
  }

  getInvoiceAmounts(diffDays) {
    const total = this.invoice.total;
    const isAvailable = this.invoice.is_available;
    const annualCostPercentage = this.operation.annual_cost_percentage / 100;
    const interest = this.getInterestCalculation(total, annualCostPercentage, diffDays);
    const variableFee = this.getInvestorFee(interest);
    const fideicomisoFee = this.getInvestorFideicomisoFee(interest, variableFee);
    const fondeoFee = fideicomisoFee ? variableFee + fideicomisoFee : variableFee;
    const gain = fideicomisoFee ? interest - variableFee - fideicomisoFee :
      interest - variableFee;

    let perception = total + gain;

    let isr = 0;

    if (this.invoice.taxpayerType === 'physical') {
      isr = interest * isrPercentage;
      perception = perception - isr;
    }

    return {
      isAvailable,
      total,
      interest,
      gain,
      isr,
      fondeoFee,
      perception
    };
  }
}

module.exports = InvoiceHelper;
