function gainEstimateQueryString(fundDateColumn) {
  return `
    round(
      CAST(
        float8 (
          annual_cost_percentage / 100 / 360 * total *
          (DATE_PART('day', date_trunc('day', expiration) - ${fundDateColumn}))
        ) as numeric
      ),
      2
    )
  `;
}

function daysDiffQuery(endDateColumn, startDateColumn) {
  const { sequelize } = require('../../models');

  return sequelize.literal(
    `(DATE_PART('day', date_trunc('day', ${endDateColumn}) - date_trunc('day', ${startDateColumn})))`
  );
}

function gainEstimateQuery(fundDateColumn) {
  const { sequelize } = require('../../models');

  return sequelize.literal(this.gainEstimateQueryString(fundDateColumn));
}

function gainEstimatePercentageQuery(fundDateColumn) {
  const { sequelize } = require('../../models');

  return sequelize.literal(`
    round(
      CAST(
        float8 (${this.gainEstimateQueryString(fundDateColumn)} / total * 100) as numeric
      ),
      2
    )
  `);
}

module.exports = {
  daysDiffQuery,
  gainEstimateQuery,
  gainEstimatePercentageQuery,
  gainEstimateQueryString
};
