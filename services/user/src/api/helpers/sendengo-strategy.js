
class SendengoStrategy {
  constructor(config) {
    this.sendengoId = config && config.values ? config.values.sendengo_id : null;
  }

  selectObjectByCompanyId(companyId, sendengoObject, defaultObject) {
    let returnValue;

    if ( this.sendengoId === companyId ) {
      returnValue = sendengoObject;
    } else {
      returnValue = defaultObject;
    }

    return returnValue;
  }

  selectByCompanyId(companyId, params) {
    let returnValue;

    if ( this.sendengoId === companyId ) {
      returnValue = params.sendengo;
    } else {
      returnValue = params.default;
    }

    return returnValue;
  }

  execCallback(companyId, callback) {
    if ( this.sendengoId === companyId ) {
      callback();
    }
  }

  static generateStrategyObjectByConfig() {
    const { SiteConfig } = require('../../models');

    return SiteConfig.findOne({
      where: {
        key: 'SENDENGO_FORMULA'
      }
    }).then( config => new SendengoStrategy(config));
  }
}

module.exports = SendengoStrategy;
