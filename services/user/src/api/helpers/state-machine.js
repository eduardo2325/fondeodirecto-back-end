class StateMachine {
  constructor() {
    this.invoiceForward = {
      PENDING: 'APPROVED',
      APPROVED: 'PUBLISHED',
      PUBLISHED: 'FUND_REQUESTED',
      FUND_REQUESTED: 'FUNDED',
      FUNDED: 'COMPLETED'
    };

    this.invoiceBackward = {
      PENDING: 'REJECTED',
      PUBLISHED: 'APPROVED',
      FUND_REQUESTED: 'PUBLISHED',
      COMPLETED: 'FUNDED'
    };

    this.operationForward = {
      PENDING: 'FUND_REQUESTED',
      FUND_REQUESTED: 'FUNDED',
      FUNDED: 'PAYMENT_IN_PROCESS',
      PAYMENT_IN_PROCESS: 'COMPLETED',
      PAYMENT_DUE: 'PAYMENT_IN_PROCESS'
    };

    this.operationBackward = {
      FUND_REQUESTED: 'PENDING',
      FUNDED: 'FUND_REQUESTED',
      PAYMENT_IN_PROCESS: 'FUNDED',
      PAYMENT_DUE: 'LOST'
    };

    this.investorTransactionForward = {
      PENDING: 'COMPLETED'
    };

    this.investorTransactionBackward = {
      PENDING: 'CANCELED'
    };
  }

  errorValidation(instances, rules, status) {
    if (instances.invoice && !rules.invoiceRules[status.invoiceStatus]) {
      const error = {
        errors: [ {
          path: 'Invoice',
          message: 'invalid invoice state'
        } ]
      };

      return Promise.reject(error);
    }
    if (instances.operation && !rules.operationRules[status.operationStatus]) {
      const error = {
        errors: [ {
          path: 'Invoice',
          message: 'invalid operation state'
        } ]
      };

      return Promise.reject(error);
    }
    if (instances.investorTransaction && !rules.investorTransactionRules[status.investorTransactionStatus]) {
      const error = {
        errors: [ {
          path: 'Invoice',
          message: 'invalid investor transaction state'
        } ]
      };

      return Promise.reject(error);
    }
    return Promise.resolve(instances);
  }

  validateStatus(instances, status) {
    const invoice = instances.invoice;
    const operation = instances.operation;
    const investorTransaction = instances.investorTransaction;
    const nextDay = new Date();
    const rules = {};
    let invoiceRules = {};
    let operationRules = {};
    let investorTransactionRules = {};

    nextDay.setDate(nextDay.getDate() + 1);
    nextDay.setHours(0, 0, 0, 0);

    if (invoice) {
      invoiceRules = {
        APPROVED: Boolean(invoice.expiration),
        REJECTED: true,
        PUBLISHED: Boolean(invoice.published) && invoice.expiration >= nextDay,
        FUND_REQUESTED: Boolean(operation) && Boolean(invoice.fund_date) &&
          Boolean(invoice.investor_company_id),
        FUNDED: true,
        COMPLETED: true
      };
    }
    if (operation) {
      operationRules = {
        PENDING: true,
        FUND_REQUESTED: true,
        FUNDED: Boolean(investorTransaction),
        PAYMENT_IN_PROCESS: true,
        COMPLETED: true,
        LOST: true
      };
    }
    if (investorTransaction) {
      investorTransactionRules = {
        COMPLETED: true,
        CANCELED: true
      };
    }

    rules.invoiceRules = invoice ? invoiceRules : null;
    rules.operationRules = operation ? operationRules : null;
    rules.investorTransactionRules = investorTransaction ? investorTransactionRules : null;

    return this.errorValidation(instances, rules, status);
  }

  next(invoice = null, operation = null, investorTransaction = null, transaction) {
    const instances = {
      invoice,
      operation,
      investorTransaction
    };

    const status = {};
    const newInvoiceStatus = invoice ? this.invoiceForward[invoice.status] : null;
    const newOperationStatus = operation ? this.operationForward[operation.status] : null;
    const newInvestorTransactionStatus = investorTransaction ?
      this.investorTransactionForward[investorTransaction.status] : null;

    status.invoiceStatus = newInvoiceStatus;
    status.operationStatus = newOperationStatus;
    status.investorTransactionStatus = newInvestorTransactionStatus;

    return this.validateStatus(instances, status).then(() => {
      const promises = [];

      if (invoice && newInvoiceStatus) {
        invoice.status = newInvoiceStatus;
        const invoicePromise = invoice.save({ transaction });

        promises.push(invoicePromise);
      }

      if (operation && newOperationStatus) {
        operation.status = newOperationStatus;
        const operationPromise = operation.save({ transaction });

        promises.push(operationPromise);
      }

      if (investorTransaction && newInvestorTransactionStatus) {
        investorTransaction.status = newInvestorTransactionStatus;
        const investorTransactionPromise = investorTransaction.save({ transaction });

        promises.push(investorTransactionPromise);
      }

      return Promise.all(promises);
    });
  }

  back(invoice, operation = null, investorTransaction = null, transaction) {
    const instances = {
      invoice,
      operation,
      investorTransaction
    };
    const status = {};
    const newInvoiceStatus = invoice ? this.invoiceBackward[invoice.status] : null;
    const newOperationStatus = operation ? this.operationBackward[operation.status] : null;
    const newInvestorTransactionStatus = investorTransaction ?
      this.investorTransactionBackward[investorTransaction.status] : null;

    status.invoiceStatus = newInvoiceStatus;
    status.operationStatus = newOperationStatus;
    status.investorTransactionStatus = newInvestorTransactionStatus;

    return this.validateStatus(instances, status).then(() => {
      const promises = [];

      if (invoice && newInvoiceStatus) {
        invoice.status = newInvoiceStatus;
        const invoicePromise = invoice.save({ transaction });

        promises.push(invoicePromise);
      }

      if (operation && newOperationStatus) {
        operation.status = newOperationStatus;
        const operationPromise = operation.save({ transaction });

        promises.push(operationPromise);
      }

      if (investorTransaction && newInvestorTransactionStatus) {
        investorTransaction.status = newInvestorTransactionStatus;
        const investorTransactionPromise = investorTransaction.save({ transaction });

        promises.push(investorTransactionPromise);
      }

      return Promise.all(promises);
    });
  }
}

module.exports = new StateMachine();
