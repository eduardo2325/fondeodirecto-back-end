function moneyFormat(value) {
  let fixedValue = String(value)
    .split('').reverse().join('')
    .replace(/\d{3}/g, '$&,')
    .split('').reverse().join('')
    .replace(/\.(\d{2})\d+$/, '.$1')
    .replace(/\.(\d)$/, '.$10')
    .replace(/^,/, '');

  if (fixedValue.indexOf('.') === -1) {
    fixedValue = `${fixedValue}.00`;
  }

  return fixedValue;
}

module.exports = moneyFormat;
