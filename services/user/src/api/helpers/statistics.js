const { InvitationNotification } = require('../../models');

class Statistics {
  saveInvitationNotification(eventType, notificationId) {
    return InvitationNotification.create({
      notification_id: notificationId,
      event_type: eventType
    });
  }
}

module.exports = new Statistics();
