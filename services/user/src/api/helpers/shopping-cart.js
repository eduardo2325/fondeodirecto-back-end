const _ = require('lodash');

function getOnlyAvailableInvoices(invoices) {
  return _.map(_.filter(invoices, invoice => {
    return invoice.isAvailable || invoice.is_available;
  }), i => {
    return i;
  });
}

function getAvailableInvoicesSummary(invoices) {
  const invoicesTotal = _.sumBy(invoices, invoice => {
    return parseFloat(invoice.total);
  });
  const interestsTotal = _.sumBy(invoices, invoice => {
    return parseFloat(invoice.interest);
  });
  const isrTotal = _.sumBy(invoices, invoice => {
    return parseFloat(invoice.isr);
  });
  const feeTotal = _.sumBy(invoices, invoice => {
    return parseFloat(invoice.fee);
  });
  const perceptionTotal = _.sumBy(invoices, invoice => {
    return parseFloat(invoice.perception);
  });

  const earningsTotal = invoicesTotal + interestsTotal - isrTotal - feeTotal;

  const summary = {
    'invoices_total': invoicesTotal.toFixed(2),
    'interests_total': interestsTotal.toFixed(2),
    'isr_total': isrTotal.toFixed(2),
    'fee_total': feeTotal.toFixed(2),
    'earnings_total': earningsTotal.toFixed(2),
    'perception_total': perceptionTotal.toFixed(2)
  };

  return summary;
}

function getCartResponse(invoices) {
  const response = {};
  const availableInvoices = getOnlyAvailableInvoices(invoices);
  const cartSummary = getAvailableInvoicesSummary(availableInvoices);

  response.total = invoices.length;
  response.invoices = invoices;
  response.summary = cartSummary;

  return response;
}

function getPublishSummaryTotals(invoices) {
  const availableInvoices = getOnlyAvailableInvoices(invoices);
  const totalInvoices = availableInvoices.length;

  const invoicesTotal = _.sumBy(availableInvoices, invoice => {
    return parseFloat(invoice.total);
  });
  const operationCostTotal = _.sumBy(availableInvoices, invoice => {
    return parseFloat(invoice.operation_cost);
  });
  const reserveTotal = _.sumBy(availableInvoices, invoice => {
    return parseFloat(invoice.reserve);
  });
  const fundPaymentTotal = _.sumBy(availableInvoices, invoice => {
    return parseFloat(invoice.fund_payment);
  });

  const summary = {
    'invoices_total': invoicesTotal.toFixed(2),
    'operation_cost_total': operationCostTotal.toFixed(2),
    'reserve_total': reserveTotal.toFixed(2),
    'fund_payment_total': fundPaymentTotal.toFixed(2)
  };

  return {
    totalInvoices,
    summary
  };
}

module.exports = {
  getOnlyAvailableInvoices,
  getAvailableInvoicesSummary,
  getCartResponse,
  getPublishSummaryTotals
};
