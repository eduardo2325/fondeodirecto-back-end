const _ = require('lodash');

module.exports = {
  options: {
    classMethods: {
      registerDeleteInvoices: function(activityData, invoices, { sequelizeTransaction }) {
        const { UserActivity, sequelize } = require('../../models');
        const today = new Date();

        return UserActivity.create({
          triggered_by: activityData.user_id,
          generated_at: today,
          action: 'DELETE_INVOICE'
        }, { transaction: sequelizeTransaction })
          .then((activity) => {
            const activity_id = activity.id;
            const _ids = _.map(invoices, _i => [ activity_id, _i.id ] );
            const data = _.flatten(_ids);
            const _valuesString = _.reduce(invoices, (accumulator, value, index) => {
              return accumulator + '(?, ?)' + (index === invoices.length - 1 ? '' : ',');
            }, '');
            const rawQuery = `INSERT INTO invoice_activity (activity_id, invoice_id) VALUES ${_valuesString};`;

            return sequelize.query(rawQuery, { replacements: data,
              type: sequelize.QueryTypes.INSERT, transaction: sequelizeTransaction });
          });
      }
    }
  }
};
