const _ = require('lodash');
const moment = require('/var/lib/core/js/moment');
const { cxpActions, cxcActions } = require('../../config/user-roles');
const InvoiceQueries = require('../helpers/invoice-queries');
const InvoiceHelper = require('../helpers/invoice');
const { global_fee_percentage } = require('../../config/constants');
const SendengoStrategy = require('../helpers/sendengo-strategy');

module.exports = {
  options: {
    hooks: {
      beforeCreate: instance => {
        let number = instance.serie ? instance.serie + '-' : '';

        number += instance.folio;
        instance.number = number;
        instance.emission_date = new Date(instance.emission_date);

        return Promise.resolve(instance);
      }
    },
    attributes: {
      subtotal: {
        set: function(value) {
          const number = Number(value);

          return this.setDataValue('subtotal', number);
        },
        get: function() {
          return this.getDataValue('subtotal').toFixed(2);
        }
      },
      tax_total: {
        set: function(value) {
          const number = Number(value);

          return this.setDataValue('tax_total', number);
        },
        get: function() {
          return this.getDataValue('tax_total').toFixed(2);
        }
      },
      tax_percentage: {
        set: function(value) {
          const number = Number(value);

          return this.setDataValue('tax_percentage', number);
        },
        get: function() {
          return this.getDataValue('tax_percentage').toFixed(2);
        }
      },
      total: {
        set: function(value) {
          const number = Number(value);

          return this.setDataValue('total', number);
        },
        get: function() {
          return this.getDataValue('total').toFixed(2);
        }
      }
    },
    instanceMethods: {
      getBasicOperationInfo: function() {
        return Promise.resolve({
          id: this.id,
          operation_id: this.Operation ? this.Operation.id : ''
        });
      },

      getBasicInfo: function() {
        return Promise.resolve({
          company_id: this.Company.id,
          company_name: this.Company.name,
          company_color: this.Company.color,
          business_name: this.Company.business_name,
          number: String(this.number),
          emission_date: (this.emission_date || '').toString(),
          expiration: (this.expiration || '').toString(),
          uuid: this.uuid,
          status: this.status.toLowerCase(),
          total: this.total
        });
      },

      getGeneralInfoWithOperation: function() {
        const operation = this.Operation;
        let status = operation ? operation.status.toLowerCase() : this.status.toLowerCase();

        if (status === 'pending') {
          status = 'published';
        }

        return Promise.resolve({
          id: operation ? operation.id : undefined,
          invoice_id: operation ? operation.invoice_id : this.id,
          expiration_date: this.expiration ? this.expiration.toString() : '',
          published_date: operation ? operation.published_date.toString() : '',
          days_limit: operation ? operation.days_limit : undefined,
          status,
          uuid: this.uuid,
          number: this.number,
          total: this.total.toFixed(2),
          company_name: this.Client ? this.Client.name : this.Company.name,
          prepaid: '0.00',
          saving: '0.00',
          company_id: this.Company ? this.Company.id : this.Client.id
        });
      },

      getGeneralInfo: function(company, field, options) {
        const plain = this.get({ plain: true });
        const status = this.Operation && this.Operation.status !== 'PENDING' ?
          this.Operation.status : this.status;

        const gain_percentage = this.Operation && options && options.investorGeneralInfo ?
          this.Operation.annual_cost_percentage.toFixed(2) : plain.gain_percentage;

        return new Promise((resolve) => {
          plain[field + '_id'] = company.id;
          plain[field + '_name'] = company.name;
          plain[field + '_color'] = company.color;
          plain.status = status.toLowerCase();
          plain.created_at = this.created_at.toString();
          if (field === 'client') {
            plain.client_company_id = company.id;
          }
          // FIXME: bad values...
          return resolve({
            id: plain.id,
            total: String(plain.total),
            client_company_id: plain.client_company_id,
            client_name: plain.client_name,
            client_color: plain.client_color,
            company_id: plain.company_id,
            company_name: plain.company_name,
            company_color: plain.company_color,
            status: plain.status,
            number: String(plain.number),
            created_at: plain.created_at.toString(),
            uuid: plain.uuid,
            fund_date: String(plain.fund_date || ''),
            published: String(plain.published || ''),
            expiration: String(plain.expiration || ''),
            expiration_extra: (this.expiration && options
              ? moment(this.expiration).utc().add(10, 'days')
              : this.expiration || '').toString(),
            gain: plain.gain,
            gain_percentage: gain_percentage,
            term_days: plain.term_days,
            emission_date: (this.emission_date || '').toString(),
            operation_id: plain.Operation ? plain.Operation.id : undefined
          });
        });
      },

      getGeneralInfoAsAdmin: function() {
        const status = this.Operation && this.Operation.status !== 'PENDING' ?
          this.Operation.status : this.status;

        return Promise.resolve({
          id: this.id,
          uuid: this.uuid,
          client_company_id: this.Client.id,
          client_name: this.Client.name,
          client_color: this.Client.color,
          company_id: this.Company.id,
          company_name: this.Company.name,
          company_color: this.Company.color,
          investor_name: this.Investor ? this.Investor.name : '',
          number: String(this.number),
          fund_date: this.fund_date ? this.fund_date.toString() : '',
          expiration: this.expiration ? this.expiration.toString() : '',
          status: status.toLowerCase(),
          total: this.total.toString()
        });
      },

      checkInvoiceItem(object) {
        return object.count && object.description && object.price
          && object.single && object.total;
      },

      getProtoBuffArrayOfInvoiceItems() {
        return this.items ? _.filter(_.map(this.items, (element) => {
          return this.extractInvoiceItemFields(element);
        }), (o) => {
          this.checkInvoiceItem(o);
        }) : [];
      },

      extractInvoiceItemFields(object) {
        return {
          count: object.count ? object.count : '',
          description: object.description ? object.description : '',
          price: object.price ? object.price : '',
          single: object.single ? object.single : '',
          total: object.total ? object.total : ''
        };
      },

      getBasicInfoWithClient: function(isInvestor) {
        const status = this.Operation && this.Operation.status !== 'PENDING' ?
          this.Operation.status : this.status;

        const clientPlain = this.Client.get( { plain: true });

        return Promise.resolve({
          client_rfc: this.Client.rfc,
          client_company_id: this.Client.id,
          client_name: this.Client.name,
          client_created_at: clientPlain.created_at.toString(),
          client_color: this.Client.color,
          client_description: this.Client.description,
          business_name: this.Client.business_name,
          number: String(this.number),
          subtotal: this.subtotal.toString(),
          emission_date: (this.emission_date || '').toString(),
          upload_date: String(this.created_at) || '',
          published_date: this.Operation && this.Operation.published_date ?
            String(this.Operation.published_date) : '',
          fund_date: this.Operation && this.Operation.InvestorTransaction &&
            this.Operation.InvestorTransaction.fund_date ?
            String(this.Operation.InvestorTransaction.fund_date) : '',
          approved_date: this.approved_date ? String(this.approved_date) : '',
          payment_date: this.Operation && this.Operation.payment_date ?
            String(this.Operation.payment_date) : '',
          items: this.getProtoBuffArrayOfInvoiceItems(),
          taxes: String(this.tax_total),
          expiration: (this.expiration || '').toString(),
          expiration_days: InvoiceHelper.diffDays(this.fund_date, this.expiration),
          expiration_extra: (this.expiration && isInvestor
            ? moment(this.expiration).utc().add(10, 'days')
            : this.expiration || '').toString(),
          uuid: this.uuid,
          status: status.toLowerCase(),
          total: String(this.total),
          operation_id: this.Operation ? this.Operation.id : undefined
        });
      },

      getEstimate: function() {
        return Promise.resolve()
          .then(() => {
            const percentages = this.Client.OperationCost;

            if (!this.expiration) {
              return Promise.reject({
                errors: [ {
                  path: 'Invoice',
                  message: 'Not approved yet'
                } ]
              });
            }

            const helper = new InvoiceHelper(this, { percentages });
            const diffDays = helper.getStartAndExpirationDayDifference();

            if (diffDays <= 0) {
              return Promise.reject({
                errors: [ {
                  path: 'Invoice',
                  message: 'Expired'
                } ]
              });
            }

            const totalOperationCost = helper.getTotalOperationCost(diffDays);

            const response = {
              total: this.total.toString(),
              operation_cost: totalOperationCost.toString(),
              fund_total: (this.total - totalOperationCost).toString()
            };

            return Promise.resolve(response);
          });
      },

      getDetail: function() {
        const status = this.Operation && this.Operation.status !== 'PENDING' ?
          this.Operation.status : this.status;

        const data = {
          company_rfc: this.Company.rfc,
          company_id: this.Company.id,
          company_name: this.Company.name,
          company_color: this.Company.color,
          business_name: this.Company.business_name,
          company_regime: this.company_regime ? this.company_regime : '',
          company_postal_code: this.company_postal_code ? this.company_postal_code : '',
          number: String(this.number),
          emission_date: (this.emission_date || '').toString(),
          uuid: this.uuid,
          status: status.toLowerCase(),
          subtotal: this.subtotal.toString(),
          taxes: this.tax_total.toString(),
          total: (this.subtotal + this.tax_total).toFixed(2).toString(),
          cadena_original: this.cadena_original,
          sat_digital_stamp: this.sat_digital_stamp,
          cfdi_digital_stamp: this.cfdi_digital_stamp,
          items: this.items,
          expiration: (this.expiration || '').toString()
        };

        return SendengoStrategy.generateStrategyObjectByConfig()
          .then( sendengoStrategy => {
            const callback = sendengoStrategy.selectByCompanyId(this.client_company_id, {
              sendengo: () => {
                if (!this.fund_date) {
                  return Promise.resolve(data);
                }

                return this.getFundEstimate()
                  .then( estimates => {
                    const _operationCosts = parseFloat(estimates.operation_cost);

                    data.placeholder_payment_cxp = (this.total + _operationCosts).toFixed(2);

                    return Promise.resolve(data);
                  });
              },
              default: () => Promise.resolve(data)
            });

            return callback();
          });
      },

      getFundEstimate: function(isInvestor) {
        const { OperationCost } = require('../../models');

        const costQuery = {
          where: {
            company_id: this.client_company_id
          }
        };

        if (!this.expiration) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not approved yet'
            } ]
          });
        }

        return Promise.resolve().then(() => {
          if (this.annual_cost) {
            return Promise.resolve(this);
          }

          return OperationCost.findOne(costQuery);
        }).then(percentages => {
          const helper = new InvoiceHelper(this, { percentages });
          const diffDays = helper.getStartAndExpirationDayDifference(true);

          const {
            variableFee,
            total,
            interest,
            commission,
            fundTotal,
            reserve,
            fundPayment,
            expirationPayment,
            operationCost
          } = helper.getFundEstimates(diffDays);

          const baseInvFields = isInvestor
            ? helper.getInvestorFundEstimates(diffDays)
            : undefined;

          const invFields = {
            ...baseInvFields,
            // FIXME: by design, this field MUST be passed according its invoice.proto definition
            // this can't be null, so we just left it undefined...
            fee: isInvestor ? variableFee.toFixed(2) : undefined
          };

          return {
            ...invFields,
            days: diffDays,
            total: total.toFixed(2),
            interest: interest.toFixed(2),
            commission: commission.toFixed(2),
            fund_total: fundTotal.toFixed(2),
            reserve: reserve.toFixed(2),
            fund_payment: fundPayment.toFixed(2),
            operation_cost: operationCost.toFixed(2),
            expiration_payment: expirationPayment.toFixed(2),
            tax_total: this.tax_total ? this.tax_total.toFixed(2) : '',
            expiration: this.expiration.toString()
          };
        });
      },

      getInvestorFundEstimate: function(investor) {
        const percentages = investor.OperationCost;
        const taxpayerType = investor.taxpayer_type ? investor.taxpayer_type : undefined;

        return Promise.resolve()
          .then(() => {
            const helper = new InvoiceHelper(this, { percentages, global_fee_percentage, taxpayerType });
            const diffDays = helper.getStartAndExpirationDayDifference(true);

            const {
              isr,
              include_isr,
              perception,
              earnings,
              variableFee,
              fideicomisoFee
            } = helper.getInvestorFundEstimates(diffDays);

            const result = {
              total: this.total.toFixed(2),
              earnings: earnings.toFixed(2),
              commission: fideicomisoFee ? (variableFee + fideicomisoFee).toFixed(2) : variableFee.toFixed(2),
              perception: perception.toFixed(2),
              isr: isr.toFixed(2),
              include_isr
            };

            return Promise.resolve(result);
          });
      },

      getInvestorFundEstimateFromTransaction: function() {
        const investorTransaction = this.Operation.InvestorTransaction;

        return new Promise((resolve) => {
          return resolve({
            total: this.total.toFixed(2),
            earnings: investorTransaction.interest.toFixed(2),
            commission: investorTransaction.fideicomiso_fee ? (investorTransaction.fee +
              investorTransaction.fideicomiso_fee).toFixed(2) : investorTransaction.fee.toFixed(2),
            perception: investorTransaction.perception.toFixed(2),
            isr: investorTransaction.isr.toFixed(2),
            include_isr: investorTransaction.isr > 0
          });
        });
      },

      getInvestorProfitEstimateFromTransaction: function() {
        const investorTransaction = this.Operation.InvestorTransaction;

        return new Promise((resolve) => {
          return resolve({
            gain: investorTransaction.interest.toFixed(2),
            gain_percentage: investorTransaction.interest_percentage.toFixed(2),
            annual_gain: this.Operation.annual_cost_percentage.toFixed(2)
          });
        });
      },

      getInvestorProfitEstimate: function() {
        return Promise.resolve()
          .then(() => {
            const helper = new InvoiceHelper(this);
            const diffDays = helper.getStartAndExpirationDayDifference(true);

            const { gain, gain_percentage } = helper.getInvestorProfitEstimates(diffDays);

            const result = {
              gain: gain.toFixed(2),
              gain_percentage: gain_percentage.toFixed(2),
              annual_gain: this.Operation.annual_cost_percentage.toFixed(2)
            };

            return Promise.resolve(result);
          });
      },

      getInvestorGain: function(percentages, taxpayerType) {
        return Promise.resolve()
          .then(() => {
            const helper = new InvoiceHelper(this, { percentages, taxpayerType, global_fee_percentage });
            const diffDays = helper.getStartAndExpirationDayDifference();

            const { perception } = helper.getInvestorGains(diffDays);

            const result = perception.toFixed(2);

            return Promise.resolve(result);
          });
      },

      getInvoiceAmounts: function(investor) {
        const percentages = investor.OperationCost;
        const taxpayerType = investor.taxpayer_type;
        const plain = this.get({ plain: true });

        return Promise.resolve()
          .then(() => {
            const helper = new InvoiceHelper(this, { percentages, taxpayerType, global_fee_percentage });
            const diffDays = helper.getStartAndExpirationDayDifference();
            const {
              interest,
              isr,
              fondeoFee,
              gain,
              perception
            } = helper.getInvoiceAmounts(diffDays);

            const result = {
              id: plain.id,
              is_available: plain.is_available,
              client_name: plain.Client.name,
              total: plain.total.toFixed(2),
              term_days: diffDays,
              interest: interest.toFixed(2),
              isr: isr > 0 ? isr.toFixed(2) : String(isr),
              fee: fondeoFee.toFixed(2),
              gain: gain.toFixed(2),
              perception: perception.toFixed(2)
            };

            return Promise.resolve(result);
          });
      },

      getPublishEstimate: function() {
        const { OperationCost } = require('../../models');

        const costQuery = {
          where: {
            company_id: this.client_company_id
          }
        };

        if (!this.expiration) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not approved yet'
            } ]
          });
        }

        let sendengoStrategy;

        return SendengoStrategy.generateStrategyObjectByConfig().then(_strategy => {
          sendengoStrategy = _strategy;

          if (this.annual_cost) {
            return Promise.resolve(this);
          }

          return OperationCost.findOne(costQuery);
        }).then(percentages => {
          const helper = new InvoiceHelper(this, { percentages });
          const diffDays = helper.getStartAndExpirationDayDifference();

          const {
            total,
            reserve,
            fundPayment,
            operationCost
          } = helper.getFundEstimates(diffDays);

          const element = {
            is_available: this.dataValues.is_available,
            client_name: this.Client.name,
            number: this.number,
            total: total.toFixed(2),
            operation_cost: operationCost.toFixed(2),
            reserve: reserve.toFixed(2),
            fund_payment: fundPayment.toFixed(2)
          };

          const _assign = sendengoStrategy.selectByCompanyId(this.client_company_id, {
            sendengo: {
              reserve: '0.00',
              fund_payment: element.total
            },
            default: {}
          });

          Object.assign(element, _assign);

          return element;
        });
      },

      getInvestorFundDetail: function() {
        return new Promise((resolve) => {
          return resolve({
            fund_date: this.Operation.InvestorTransaction.fund_request_date.toString() || undefined,
            expiration: this.Operation.expiration_date.toString() || undefined,
            operation_term: this.Operation.InvestorTransaction.days_limit || undefined
          });
        });
      },

      // TODO: Use getFundEstimate method instead of this. Adding validation when operation cost exists on invoice.
      getInvoicePaymentSummary: function() {
        return new Promise(resolve => {
          const operation = this.Operation;
          const investorTransaction = this.Operation.InvestorTransaction;

          const response = {
            iva: (this.total * 0.16).toFixed(2),
            total: this.total.toFixed(2),
            interest: investorTransaction.interest.toFixed(2),
            commission: operation.commission.toFixed(2),
            reserve: operation.reserve.toFixed(2),
            fund_payment: operation.fund_payment.toFixed(2),
            expiration_payment: operation.reserve.toFixed(2),
            operation_cost: operation.operation_cost.toFixed(2),
            fund_total: operation.fund_total.toFixed(2)
          };

          return resolve(response);
        });
      },

      getAdminBasicInfo: function() {
        return new Promise(resolve => {
          const status = this.Operation && this.Operation.status !== 'PENDING' ?
            this.Operation.status : this.status;

          return resolve({
            company_id: this.Company.id,
            company_name: this.Company.name,
            company_color: this.Company.color,
            company_business_name: this.Company.business_name,
            client_company_id: this.Client.id,
            client_name: this.Client.name,
            client_color: this.Client.color,
            client_business_name: this.Client.business_name,
            number: String(this.number),
            emission_date: (this.emission_date || '').toString(),
            expiration: (this.expiration || '').toString(),
            uuid: this.uuid,
            id: this.id,
            status: status.toLowerCase(),
            total: this.total.toFixed(2),
            operation_id: this.Operation ? this.Operation.id : undefined
          });
        });
      },

      getAdminCxcPayment: function() {
        return new Promise(resolve => {
          const operation = this.Operation;
          const investorTransaction = this.Operation.InvestorTransaction;
          const _reserve = operation.reserve.toFixed(2);

          const response = {
            annual_cost: operation.annual_cost_percentage.toFixed(2),
            interest: investorTransaction.interest.toFixed(2),
            interest_percentage: (100 * (investorTransaction.interest / this.total)).toFixed(2),
            reserve: _reserve,
            reserve_percentage: operation.reserve_percentage.toFixed(2),
            fd_commission: operation.commission.toFixed(2),
            fd_commission_percentage: (100 * (operation.commission / this.total)).toFixed(2),
            total: this.total.toFixed(2),
            fund_payment: operation.fund_payment.toFixed(2),
            expiration_payment: _reserve
          };

          SendengoStrategy.generateStrategyObjectByConfig().then(sendengoStrategy => {
            const replacement = sendengoStrategy.selectByCompanyId(this.client_company_id, {
              sendengo: {
                reserve: '0.00',
                reserve_percentage: '0.00',
                expiration_payment: '0.00'
              },
              default: { }
            });

            Object.assign(response, replacement);

            resolve(response);
          });
        });
      },

      getAdminInvestorPayment: function() {
        return new Promise(resolve => {
          const investorTransaction = this.Operation.InvestorTransaction;

          const response = {
            investor_name: this.Investor.name,
            fund_date: this.fund_date ? this.fund_date.toString() : '',
            fund_total: this.total.toFixed(2),
            earnings: investorTransaction.interest.toFixed(2),
            earnings_percentage: investorTransaction.interest_percentage.toFixed(2),
            gain: investorTransaction.earnings.toFixed(2),
            gain_percentage: investorTransaction.earnings_percentage.toFixed(2),
            fee: investorTransaction.fee.toFixed(2),
            fee_percentage: (investorTransaction.fee / this.total * 100).toFixed(2),
            fideicomiso_fee: investorTransaction.fideicomiso_fee ? investorTransaction.fideicomiso_fee.toFixed(2)
              : '',
            fideicomiso_fee_percentage: investorTransaction.fideicomiso_fee ?
              (investorTransaction.fideicomiso_fee / this.total * 100).toFixed(2) : '',
            isr: investorTransaction.isr.toFixed(2),
            total_payment: investorTransaction.perception.toFixed(2),
            include_isr: investorTransaction.isr > 0
          };

          return resolve(response);
        });
      },

      getAdminOperationSummary: function() {
        return new Promise(resolve => {
          const operation = this.Operation;
          const investorTransaction = this.Operation.InvestorTransaction;

          const response = {
            fund_date: this.fund_date ? this.fund_date.toString() : '',
            expiration: this.expiration ? this.expiration.toString() : '',
            operation_term: operation && investorTransaction ? investorTransaction.days_limit.toFixed(2) : undefined,
            commission: operation.commission.toFixed(2),
            fee: investorTransaction.fee.toFixed(2),
            earnings_fd: (operation.commission + investorTransaction.fee).toFixed(2)
          };

          return resolve(response);
        });
      }
    },
    classMethods: {
      validateCompanies: function(userId, issuerRfc, recipientRfc) {
        const { Company, User } = require('../../models');

        if (issuerRfc === recipientRfc) {
          const error = {
            errors: [ {
              path: 'invoice',
              message: 'Company and Client RFC must be different'
            } ]
          };

          return Promise.reject(error);
        }

        const userQuery = {
          where: {
            id: userId
          },
          include: [ {
            model: Company,
            where: {
              rfc: issuerRfc
            }
          } ]
        };
        const recipientQuery = {
          where: {
            rfc: recipientRfc
          }
        };

        return Promise.all([
          User.findOne(userQuery),
          Company.findOne(recipientQuery)
        ])
          .then(([ issuer, receptor ]) => {
            if (!issuer || !receptor) {
              const path = issuer ? 'client_rfc' : 'company_rfc';
              const message = issuer ? 'Receptor not found' : 'Issuer not found';
              const error = {
                errors: [ {
                  path,
                  message
                } ]
              };

              return Promise.reject(error);
            }

            return [ issuer, receptor ];
          });
      },
      getInvoices: (user, where, params) => {
        const { Invoice, Company, Operation, sequelize } = require('../../models');
        const { limit, offset, order_by, order_desc } = params;
        const order = order_desc ? 'DESC' : 'ASC';
        let orderParam = order_by;
        const include = [
          {
            model: Company,
            as: 'Client'
          },
          {
            model: Company,
            as: 'Company'
          }
        ];
        const isInvestor = user.role === 'INVESTOR';
        const isCxp = user.role === 'CXP';
        const isCxc = user.role === 'CXC';

        if (orderParam === 'client_name') {
          orderParam = 'Client.name';
        }
        if (orderParam === 'company_name') {
          orderParam = 'Company.name';
        }

        const attributes = [ 'id', 'uuid', 'created_at', 'number',
          'total', 'status', 'expiration', 'fund_date', 'client_company_id', 'emission_date' ];

        if (isInvestor) {
          include.push({ model: Operation });
        }

        return Invoice.findAll({
          attributes: attributes,
          include: include,
          where,
          offset,
          limit: params.remove_limit_of_invoices ? null : limit,
          order: [ [ sequelize.col(orderParam || 'created_at'), order ] ]
        })
          .then(_invoices => {
            return Promise.all([
              Promise.resolve(_invoices),
              isCxc ? SendengoStrategy.generateStrategyObjectByConfig() : Promise.resolve(false)
            ]);
          })
          .then(([ invoices, sendengoStrategy ]) => {
            const inv = _.map(invoices, value => {
              let retValue;

              if (isCxp) {
                retValue = value.getGeneralInfo(value.Company, 'company');
              } else {
                const options = isInvestor ? { investorGeneralInfo: true } : null;

                retValue = value.getGeneralInfo(value.Client, 'client', options);
              }

              if ( isInvestor && value.Operation ) {
                retValue = retValue.then( values => {
                  return value.getInvestorProfitEstimate().then( profitValues => {
                    const helper = new InvoiceHelper(value);
                    const diffDays = helper.getStartAndExpirationDayDifference(true);

                    values.annual_gain = profitValues.annual_gain;
                    values.gain = profitValues.gain;
                    values.gain_percentage = profitValues.gain_percentage;
                    values.term_days = diffDays ? diffDays : 0;

                    return Promise.resolve(values);
                  });
                });
              }

              if (isCxc) {
                retValue = retValue.then(values => {
                  const promise = value.status === 'APPROVED' ? value.getFundEstimate() : Promise.resolve(undefined);

                  return promise.then(fundEstimate => {
                    values.fund_payment = fundEstimate ? fundEstimate.fund_payment : '';
                    values.fund_payment = sendengoStrategy.selectObjectByCompanyId(user.client_company_id,
                      fundEstimate ? values.total : '',
                      values.fund_payment);

                    return Promise.resolve(values);
                  });
                });
              }
              return retValue;
            });

            return Promise.all(inv);
          });
      },
      getMarketplace: (where, whereInclude) => {
        const { Invoice, Company, Operation } = require('../../models');

        return Promise.resolve()
          .then(() =>
            Invoice.findAll({
              attributes: [
                'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'published',
                [ InvoiceQueries.daysDiffQuery('expiration', 'now()'), 'term_days' ],
                [ InvoiceQueries.gainEstimateQuery('CURRENT_DATE'), 'gain' ],
                [ InvoiceQueries.gainEstimatePercentageQuery('CURRENT_DATE'), 'gain_percentage' ]
              ],
              include: [
                { model: Company, as: 'Client', where: whereInclude },
                { model: Operation, where: { status: 'PENDING' } }
              ],
              where
            })
          )
          .then(invoices => {
            const inv = _.map(invoices, value => {
              value.total = value.total.toString();
              return value.getGeneralInfo(value.Client, 'client', { investorGeneralInfo: true });
            });

            return Promise.all(inv);
          });
      },
      getAdminInvoices(params) {
        const { Invoice, Company, sequelize } = require('../../models');
        const {
          limit, offset, order_by, order_desc, client_name, company_name, status, start_expiration_date,
          end_expiration_date
        } = params;

        const order = order_desc ? 'DESC' : 'ASC';
        let orderParam = order_by;
        const include = [
          {
            model: Company,
            as: 'Client',
            where: client_name ? { name: { $iLike: `%${client_name}%` } } : null
          },
          {
            model: Company,
            as: 'Company',
            where: company_name ? { name: { $iLike: `%${company_name}%` } } : null
          }
        ];
        const where = {};

        where.status = {};
        where.status.$notLike = 'PUBLISHED';

        if (status) {
          where.status.$in = status.map(s => s.toUpperCase());
        }

        if (start_expiration_date || end_expiration_date) {
          where.expiration = {};

          if (end_expiration_date) {
            const endDate = new Date(end_expiration_date);

            endDate.setHours(23, 59, 59, 59);
            where.expiration.$lte = endDate;
          }
          if (start_expiration_date) {
            const startDate = new Date(start_expiration_date);

            startDate.setHours(0, 0, 0, 0);
            where.expiration.$gte = startDate;
          }
        }

        if (orderParam === 'client_name') {
          orderParam = 'Client.name';
        }
        if (orderParam === 'company_name') {
          orderParam = 'Company.name';
        }

        const invoicesPromise = Invoice.findAll({
          attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ],
          where,
          include,
          offset,
          limit,
          order: [ [ sequelize.col(orderParam || 'created_at'), order ] ]
        })
          .then(invoices => {
            const inv = _.map(invoices, i => i.getGeneralInfoAsAdmin());

            return Promise.all(inv);
          });

        const countPromise = Invoice.count({ include, where });

        return Promise.all([ invoicesPromise, countPromise ])
          .then(([ invoices, count ]) => {
            return { invoices, count };
          });
      },
      validateCxp: function(userId, invoiceId) {
        const { Invoice } = require('../../models');

        return Invoice.validateUser(userId, invoiceId, cxpActions, 'client_company_id');
      },
      validateCxc: function(userId, invoiceId) {
        const { Invoice } = require('../../models');

        return Invoice.validateUser(userId, invoiceId, cxcActions, 'company_id');
      },
      validateUser: function(userId, invoiceId, roles, companyIdField) {
        const { User, Invoice } = require('../../models');

        const userQuery = {
          where: {
            id: userId
          }
        };
        const invoiceQuery = {
          where: {
            id: invoiceId
          },
          include: { all: true }
        };

        return Promise.all([
          User.findOne(userQuery),
          Invoice.findOne(invoiceQuery)
        ]).then(results => {
          const companyId = results[0].company_id;

          if (!_.includes(roles, results[0].role)) {
            const error = {
              errors: [ {
                path: 'user',
                message: 'Unauthorized role'
              } ]
            };

            return Promise.reject(error);
          }

          if (!results[1] || companyId !== results[1][companyIdField]) {
            const error = {
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          return Promise.resolve(results[1]);
        });
      },
      validateExpirationDate: function(date) {
        if (!Date.parse(date)) {
          const error = {
            errors: [ {
              path: 'expiration',
              message: 'invalid format'
            } ]
          };

          return Promise.reject(error);
        }

        const expiration = new Date(date);
        const nextDay = new Date();

        nextDay.setDate(nextDay.getDate() + 1);
        nextDay.setHours(0, 0, 0, 0);

        if (expiration < nextDay) {
          const error = {
            errors: [ {
              path: 'expiration',
              message: 'invalid date'
            } ]
          };

          return Promise.reject(error);
        }

        return Promise.resolve(expiration);
      },
      getExpirableInvoices() {
        const { Invoice, Operation } = require('../../models');

        let invoices = [];

        return Invoice.findAll({
          where: {
            status: [ 'APPROVED', 'PUBLISHED' ]
          },
          include: [
            {
              model: Operation, required: true,
              where: {
                $and: [
                  { status: 'FUNDED' },
                  { status: { $notIn: [ 'EXPIRED' ] } }
                ]
              }
            }
          ]
        })
          .then(result => {
            const validations = result.map((invoice) => {
              return Invoice.validateExpirationDate(invoice.expiration)
                .then(() => true)
                .catch(error => {
                  if (error.errors && error.errors[0].message === 'invalid date') {
                    return false;
                  }

                  throw error;
                });
            });

            invoices = result;

            return Promise.all(validations);
          })
          .then(validations => invoices.filter((i, index) => !validations[index]));
      },
      updatePaymentDueInvoices: function() {
        const { Invoice, Operation, Company } = require('../../models');
        const yesterday = new Date();

        yesterday.setDate(yesterday.getDate() - 1);
        yesterday.setHours(0, 0, 0, 0);

        const invoiceQuery = {
          where: {
            expiration: {
              $lte: yesterday
            }
          },
          include: [
            { model: Company, as: 'Company' },
            { model: Operation, where: { status: [ 'FUNDED' ] } }
          ]
        };

        return Invoice.findAll(invoiceQuery);
      },
      createOperation: function(invoice, userId) {
        const { OperationCost, Operation } = require('../../models');

        const operationCostQuery = {
          where: {
            company_id: invoice.client_company_id
          }
        };

        return Promise.all([
          OperationCost.findOne(operationCostQuery),
          SendengoStrategy.generateStrategyObjectByConfig()
        ])
          .then(([ percentages, sendengoStrategy ]) => {
            const helper = new InvoiceHelper(invoice, { percentages });
            const diffDays = helper.getStartAndExpirationDayDifference();

            const {
              factorable,
              subtotal,
              fundPayment,
              reserve,
              annualCost,
              fdCommission,
              commission,
              operationCost,
              fundTotal
            } = helper.getFundEstimates(diffDays);

            const fund_payment = sendengoStrategy.selectObjectByCompanyId(invoice.client_company_id,
              invoice.total.toFixed(2), fundPayment.toFixed(2));

            const operation = {
              invoice_id: invoice.id,
              expiration_date: invoice.expiration,
              published_date: invoice.published,
              annual_cost_percentage: percentages.annual_cost.toFixed(2),
              reserve_percentage: percentages.reserve.toFixed(2),
              fd_commission_percentage: percentages.fd_commission.toFixed(2),
              annual_cost: annualCost.toFixed(2),
              reserve: reserve.toFixed(2),
              fd_commission: fdCommission.toFixed(2),
              factorable: factorable.toFixed(2),
              subtotal: subtotal.toFixed(2),
              fund_payment,
              commission: commission.toFixed(2),
              operation_cost: operationCost.toFixed(2),
              fund_total: fundTotal.toFixed(2),
              formula: `Comisión fija, comisión variable en MXN o % maximizada cobrada al finalizar la operación.
              ($350.00 o 10% configurables por inversionista) acordé al mejor escenario`,
              user_id: userId,
              days_limit: diffDays
            };

            return Operation.create(operation);
          });
      },
      createInvestorTransaction: function(invoice, investorCompany) {
        const { OperationCost, InvestorTransaction } = require('../../models');

        const operationCostQuery = {
          where: {
            company_id: investorCompany.id
          }
        };

        return OperationCost.findOne(operationCostQuery)
          .then(percentages => {
            const taxpayerType = investorCompany.taxpayer_type;
            const helper = new InvoiceHelper(invoice, { percentages, global_fee_percentage, taxpayerType });
            const diffDays = helper.getStartAndExpirationDayDifference(true);

            const {
              earnings,
              earningsPercentage,
              isr,
              isrTotalPercent,
              variableFee,
              feeType,
              fideicomisoFee,
              fideicomisoFeeType,
              netIncome,
              netIncomePercentage,
              perception
            } = helper.getInvestorTransactionEstimates(diffDays);

            const investorTransaction = {
              operation_id: invoice.Operation.id,
              investor_company_id: investorCompany.id,
              fund_request_date: invoice.fund_date,
              days_limit: diffDays,
              funded_amount: invoice.total.toFixed(2),
              interest: earnings.toFixed(2),
              interest_percentage: earningsPercentage.toFixed(2),
              isr: isr.toFixed(2),
              isr_percentage: isrTotalPercent.toFixed(2),
              global_fee_percentage: global_fee_percentage,
              fee: variableFee.toFixed(2),
              fee_type: feeType,
              earnings: netIncome.toFixed(2),
              earnings_percentage: netIncomePercentage.toFixed(2),
              perception: perception.toFixed(2),
              fideicomiso_fee: fideicomisoFee ? fideicomisoFee.toFixed(2) : null,
              fideicomiso_fee_type: fideicomisoFeeType ? fideicomisoFeeType : null
            };

            return InvestorTransaction.create(investorTransaction);
          });
      },
      removeFundRequest: function(invoice, transaction) {
        const { InvestorTransaction } = require('../../models');

        invoice.fund_date = null;
        invoice.investor_company_id = null;

        return invoice.save({ transaction })
          .then(() => {
            return InvestorTransaction.destroy({
              where: {
                operation_id: invoice.Operation.id
              },
              transaction
            });
          });
      },

      getNewestInvoices: function() {
        const { Invoice, Operation } = require('../../models');

        const yesterdayAt2PM = new Date();
        const todayAt2PM = new Date();

        yesterdayAt2PM.setHours(14, 0, 0, 0);
        todayAt2PM.setHours(14, 0, 0, 0);
        yesterdayAt2PM.setDate(yesterdayAt2PM.getDate() - 1);
        todayAt2PM.setDate(todayAt2PM.getDate());

        const invoicesQuery = {
          where: {
            status: [ 'PUBLISHED' ],
            published: {
              $between: [ yesterdayAt2PM, todayAt2PM ]
            }
          },
          include: [
            { model: Operation, where: { status: [ 'PENDING' ] } }
          ]
        };

        return Invoice.findAll(invoicesQuery);
      },

      getTrackerInfo() {
        const { sequelize } = require('../../models');
        const rawQuery = `SELECT
          op.id,
          op.published_date,
          i.id as invoice_id,
          i.status as invoice_status,
          i.total,
          op.reserve,
          cxp.name as cxp_name,
          cxc.name as cxc_name,
          i.expiration as payment_date,
          op.fund_payment,
          op.annual_cost_percentage as annual_cost,
          op.status as op_status,
          investor.name as investor_name,
          inv_t.isr,
          inv_t.fee as fd_commission,
          inv_t.fideicomiso_fee as fideicomiso_fee,
          inv_t.fund_request_date,
          inv_t.status as transaction_status,
          op.payment_date as payment_date,

          pend_t.data as pending_transaction_data,
          pend_t.type  as pending_transaction_type,
          pend_t.status  as pending_transaction_status,
          i.created_at,
          i.updated_at,
          cxc.id as cxc_id,
          cxp.id as cxp_id

        from invoices i
          left join operations op on op.invoice_id = i.id
          inner join companies cxp on i.company_id = cxp.id
          inner join companies cxc on i.client_company_id = cxc.id
          left join companies investor on i.investor_company_id = investor.id
          left join operation_costs op_c on op_c.company_id=investor.id
          left join investor_transactions inv_t on inv_t.operation_id=op.id

          left join pending_transactions pend_t on
          CAST(pend_t.data->>'invoice_id' AS INT)=i.id AND pend_t.data->>'key' IS NOT NULL;
          `;

        return sequelize.query(rawQuery, { type: sequelize.QueryTypes.SELECT }).then(results => {
          const mapped = _.map( results, function(result) {
            return {
              id: result.id ? String(result.id) : '',
              published_date: result.published_date ? String(result.published_date) : '',
              invoice_id: result.invoice_id ? String(result.invoice_id) : '',
              invoice_status: result.invoice_status ? result.invoice_status : '',
              total: result.total ? String(result.total) : '',
              reserve: result.reserve ? String(result.reserve) : '',
              cxp_name: result.cxp_name ? result.cxp_name : '',
              cxc_name: result.cxc_name ? result.cxc_name : '',
              payment_date: result.payment_date ? String(result.payment_date) : '',
              fund_payment: result.fund_payment ? String(result.fund_payment) : '',
              annual_cost: result.annual_cost ? String(result.annual_cost) : '',
              op_status: result.op_status ? result.op_status : '',
              investor_name: result.investor_name ? result.investor_name : '',
              isr: result.isr ? String(result.isr) : '',
              fd_commission: result.fd_commission ? String(result.fd_commission) : '',
              fund_request_date: result.fund_request_date ? String(result.fund_request_date) : '',
              transaction_status: result.transaction_status ? result.transaction_status : '',
              cxp_payment_date: result.payment_date ? String( result.payment_date ) : '',
              pending_transaction_data: result.pending_transaction_data ? String(result.pending_transaction_data) : '',
              pending_transaction_type: result.pending_transaction_type ? result.pending_transaction_type : '',
              pending_transaction_status: result.pending_transaction_status ? result.pending_transaction_status : '',
              created_at: result.created_at ? String(result.created_at) : '',
              updated_at: result.updated_at ? String(result.updated_at) : '',
              cxc_id: result.cxc_id ? String(result.cxc_id) : '',
              cxp_id: result.cxp_id ? String(result.cxp_id) : ''
            };
          });

          return Promise.resolve( mapped );
        });
      },

      getNotifiableInvoices: function() {
        const { Invoice, Operation } = require('../../models');

        const eightDaysInitial = new Date();
        const eightDaysFinal = new Date();

        eightDaysInitial.setHours(0, 0, 0, 0);
        eightDaysFinal.setHours(0, 0, 0, 0);
        eightDaysInitial.setDate(eightDaysInitial.getDate() + 8);
        eightDaysFinal.setDate(eightDaysFinal.getDate() + 8);
        eightDaysInitial.setHours(0, 0, 0, 0);
        eightDaysFinal.setHours(23, 59, 59, 59);

        const invoicesQuery = {
          where: {
            status: [ 'PUBLISHED' ],
            expiration: {
              $between: [ eightDaysInitial, eightDaysFinal ]
            }
          },
          include: [
            { model: Operation, where: { status: [ 'FUNDED' ] } }
          ]
        };

        return Invoice.findAll(invoicesQuery);
      }
    }
  }
};

