const _ = require('lodash');
const randtoken = require('rand-token');
const userRoles = require('../../config/user-roles');
const rolesByCompany = userRoles.byCompany;

module.exports = {
  options: {
    hooks: {
      beforeCreate: instance => {
        instance.token = randtoken.generate(16);

        return Promise.resolve(instance);
      }
    },
    instanceMethods: {
      getInvitationInfo: function() {
        return new Promise((resolve) => {
          const plainInv = this.get({ plain: true });
          const data = plainInv.data;
          const invitation = {
            name: data.user_name,
            email: this.get('email'),
            status: 'pending',
            color: data.user_color,
            company: {
              rfc: plainInv.company_rfc
            }
          };

          return resolve(invitation);
        });
      }
    },
    classMethods: {
      userRegistration: function(user, companyRole, transaction) {
        const { Token } = require('../../models');
        const allowedRoles = rolesByCompany[companyRole.toUpperCase()];

        if (!user.name || !user.type || !user.companyId) {
          let missingField = !user.name ? 'name' : false;

          if (!missingField) {
            missingField = !user.type ? 'type' : 'companyId';
          }

          const error = {
            errors: [ {
              path: missingField,
              message: 'Validation notEmpty failed'
            } ]
          };

          return Promise.reject(error);
        }

        if (!_.includes(allowedRoles, user.type.toUpperCase())) {
          const error = {
            errors: [ {
              path: 'type',
              message: 'Validation isIn failed'
            } ]
          };

          return Promise.reject(error);
        }

        const data = {
          email: user.email,
          type: 'registration',
          company_id: user.companyId,
          data: {
            user_name: user.name,
            user_type: user.type
          }
        };

        return Token.verifyEmail(data.email)
          .then(() => {
            return Token.create(data, { transaction: transaction });
          });
      },
      verifyEmail: function(email) {
        const { Token, User, Company } = require('../../models');
        const query = {
          where: {
            email
          }
        };

        return Promise.all([
          User.findOne(query),
          Token.findOne(query)
        ]).then((response) => {
          const responseObj = response[0] || response[1];

          if (!responseObj) {
            return Promise.resolve();
          }

          const companyQuery = {
            where: {
              id: responseObj.company_id
            }
          };

          return Company.findOne(companyQuery);
        }).then(company => {
          if (!company) {
            return Promise.resolve();
          }

          const error = {
            errors: [ {
              path: 'email',
              message: 'email must be unique',
              data: {
                company_name: company.name
              }
            } ]
          };

          return Promise.reject(error);
        });
      },
      recoverPassword: function(user) {
        const { Token } = require('../../models');
        const query = {
          where: {
            email: user.email,
            type: 'recover_password'
          }
        };

        return Token.destroy(query).then(() => {
          const data = {
            email: user.email,
            type: 'recover_password',
            data: {
              user_name: user.name
            }
          };

          return Token.create(data);
        });
      },
      validateRecoverToken: function(token) {
        const { Token } = require('../../models');
        const query = {
          where: {
            token,
            type: 'recover_password'
          }
        };

        return Token.findOne(query).then(tokenInstance => {
          const day = 24 * (60 * 60 * 1000);
          const now = new Date();

          if (!tokenInstance || now - tokenInstance.created_at > day) {
            const error = {
              errors: [ {
                path: 'token',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          return tokenInstance;
        });
      }
    }
  }
};
