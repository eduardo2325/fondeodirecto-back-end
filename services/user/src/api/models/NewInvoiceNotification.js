const _ = require('lodash');

module.exports = {
  options: {
    hooks: {
    },
    instanceMethods: {
    },
    classMethods: {
      createNewInvoiceToken: function(investor) {
        const { Token } = require('../../models');

        const findQuery = {
          where: {
            email: investor.email, type: 'new-invoice-notification'
          },
          paranoid: true
        };

        return Token.findOne(findQuery)
          .then(token => {
            if (token) {
              return token.destroy();
            }
            return Promise.resolve(true);
          })
          .then(() => {
            const data = {
              email: investor.email,
              type: 'new-invoice-notification',
              issuer: 1,
              data: {
                user_name: investor.name,
                user_type: 'INVESTOR'
              }
            };

            return Token.create(data);
          })
          .then(tokenInstance => {
            const token = _.pick(tokenInstance.get(), [ 'token' ]);

            return token;
          });
      }
    }
  }
};
