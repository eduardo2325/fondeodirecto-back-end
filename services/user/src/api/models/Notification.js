module.exports = {
  options: {
    classMethods: {
      saveInvitationNotificationStatistic(eventType, notificationId) {
        const { InvitationNotification } = require('../../models');

        return InvitationNotification.create({
          notification_id: notificationId,
          event_type: eventType
        });
      }
    }
  }
};
