const _ = require('lodash');

const companyColors = require('../../vendor/colors');
const userRoles = require('../../config/user-roles').roles.map(role => role.value);

module.exports = {
  attributes: {
    taxpayer_type: {
      set: function(value) {
        return this.setDataValue('taxpayer_type', value.toUpperCase());
      },
      get: function() {
        let taxpayerType = this.getDataValue('taxpayer_type');

        if (taxpayerType) {
          taxpayerType = taxpayerType.toLowerCase();
        }

        return taxpayerType;
      }
    },
    suspended_roles: {
      validate: {
        isValidRole: function(array) {
          array.forEach(function(value) {
            if (userRoles.indexOf(value) === -1) {
              throw new Error({
                errors: [ {
                  path: 'suspended_roles',
                  message: 'invalid role'
                } ]
              });
            }

            return true;
          });
        }
      }
    }
  },
  options: {
    hooks: {
      beforeCreate: instance => {
        const { Company } = require('../../models');

        instance.color = _.sample(companyColors);

        if ( !instance.clabe ) {
          return Promise.resolve(instance);
        }

        return Company.analyzeClabe(instance.clabe)
          .then(bankInfo => {
            instance.bank = bankInfo.name;
            instance.bank_account = bankInfo.account;

            if (instance.rfc) {
              instance.setDataValue('rfc', instance.rfc.toUpperCase());
            }

            return Promise.resolve(instance);
          });
      },
      beforeUpdate: instance => {
        const { Company } = require('../../models');

        if ( !instance.clabe ) {
          return Promise.resolve(instance);
        }

        return Company.analyzeClabe(instance.clabe)
          .then(bankInfo => {
            instance.bank = bankInfo.name;
            instance.bank_account = bankInfo.account;

            if (instance.rfc) {
              instance.setDataValue('rfc', instance.rfc.toUpperCase());
            }

            return Promise.resolve(instance);
          });
      },
      validationFailed: (instance, options, error) => {
        return Promise.resolve()
          .then(() => {
            const validationError = error.errors ? error.errors[0] : null;

            if (validationError.path === 'clabe') {
              return Promise.reject({
                errors: [ {
                  path: 'clabe',
                  message: 'invalid clabe'
                } ]
              });
            }

            return Promise.resolve();
          });
      }
    },
    instanceMethods: {
      getInformation: function() {
        const plainObj = this.get({ plain: true });

        plainObj.taxpayer_type = plainObj.taxpayer_type || '';
        plainObj.balance = plainObj.balance ? plainObj.balance.toFixed(2) : '0.00';

        plainObj.bank = plainObj.bank ? plainObj.bank : '';
        plainObj.bank_account = plainObj.bank_account ? plainObj.bank_account : '';
        plainObj.holder = plainObj.holder ? plainObj.holder : '';
        plainObj.clabe = plainObj.clabe ? plainObj.clabe : '';
        plainObj.prepayment = plainObj.prepayment ? plainObj.prepayment : false;

        return Promise.resolve(_.omit(plainObj, [ 'created_at', 'updated_at', 'agreed_at',
          'agreement_url', 'isFideicomiso' ]));
      },
      getBalance: function() {
        const { PendingTransaction } = require('../../models');
        const { balance, id } = this;

        return PendingTransaction.findAll({
          where: {
            company_id: id,
            status: 'PENDING',
            type: 'WITHDRAW'
          }
        })
          .then(results => {
            const pendingWithdrawAmount = results.reduce((total, t) => total + t.amount, 0);

            return (balance - pendingWithdrawAmount).toFixed(2);
          });
      },
      updateRoleSuspension: function(role, suspend, transaction) {
        const { suspended_roles } = this;
        const roleIndex = suspended_roles.indexOf(role);
        const roleIsSuspended = roleIndex !== -1;

        if (roleIsSuspended && !suspend) {
          suspended_roles.splice(roleIndex, 1);
          this.suspended_roles = suspended_roles;

          return this.save({ transaction });
        }

        if (!roleIsSuspended && suspend) {
          suspended_roles.push(role);
          this.suspended_roles = suspended_roles;

          return this.save({ transaction });
        }

        return Promise.reject({
          errors: [ {
            path: 'role',
            message: `already ${suspend ? 'suspended' : 'unsuspended'}`
          } ]
        });
      },
      allowedCXP() {
        const { suspended_roles } = this;

        return Promise.resolve(suspended_roles.indexOf('CXP') === -1);
      },
      getCashFlowFromRange(lowerMomentBound, upperMomentBound) {
        const statementQueries = require('../helpers/statement-queries');
        const investorCompanyId = this.id;

        const withdrawalsPromises = [];
        const cashDepositsPromises = [];

        if ( upperMomentBound.diff(lowerMomentBound, 'months') < 0 ||
            upperMomentBound.diff(lowerMomentBound, 'years') < 0 ) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        const pushPromises = (counter) => {
          const date = upperMomentBound.clone().subtract(counter, 'months');
          const _year = date.get('year');
          const _month = date.get('month');

          withdrawalsPromises.push(
            statementQueries.investorCashWithDrawals(investorCompanyId, _year, _month)
          );
          withdrawalsPromises.push(
            statementQueries.investorCashWithDrawalsFromInvoices(investorCompanyId, _year, _month)
          );

          cashDepositsPromises.push(
            statementQueries.investorCashDeposits(investorCompanyId, _year, _month)
          );
          cashDepositsPromises.push(
            statementQueries.investorCashDepositsFromInvoices(investorCompanyId, _year, _month)
          );

          return { year: _year, month: _month };
        };

        let condition = false;
        let counter = 0;

        const _upperMoment = upperMomentBound.clone();

        while (!condition) {
          pushPromises(counter);
          _upperMoment.subtract(counter, 'months');

          condition = _upperMoment.isSameOrBefore(lowerMomentBound) || counter === 3;

          counter = counter + 1;
        }

        return Promise.all([ Promise.all(withdrawalsPromises), Promise.all(cashDepositsPromises) ])
          .then( ([ withdrawals, deposits ]) => {
            const reduceCashFlowArrayRange = (arrayOfCashflows) => {
              const total = _.reduce( arrayOfCashflows, (sum, arrayElement) => {
                const _reduced = _.reduce(arrayElement, (sum2, element) => {
                  return sum2 + element.cash;
                }, 0);

                return _reduced + sum;
              }, 0);

              return total;
            };

            const withdrawalsTotal = reduceCashFlowArrayRange(withdrawals);
            const depositsTotal = reduceCashFlowArrayRange(deposits);

            return Promise.resolve({
              withdrawals: withdrawalsTotal,
              deposits: depositsTotal
            });
          });
      }
    },
    classMethods: {
      analyzeClabe: function(clabe) {
        const { BankCode } = require('../../models');
        const splited = clabe.match(/^(\d{3})(\d{3})(\d{11})(\d)$/);

        if (!splited) {
          const error = {
            errors: [ {
              path: 'clabe',
              message: 'invalid clabe'
            } ]
          };

          return Promise.reject(error);
        }

        return BankCode.findOne({
          where: {
            id: splited[1]
          }
        }).then(bank => {
          if (!bank) {
            const error = {
              errors: [ {
                path: 'clabe',
                message: 'invalid clabe'
              } ]
            };

            return Promise.reject(error);
          }

          return Promise.resolve({
            name: bank.name,
            account: splited[3]
          });
        });
      },
      getCompanies: (limit, offset, order_by, order_desc, role) => {
        const { Company, User, Token, sequelize } = require('../../models');
        const userCountText = 'user_count';
        const invitationCountText = 'invitation_count';

        const userCount = [
          sequelize.literal('(SELECT COUNT(*) FROM users WHERE users.deleted_at IS NULL ' +
            'AND users.company_id = \"Company\".\"id\")'),
          userCountText ];
        const invitationCount = [
          sequelize.literal(
            '(SELECT COUNT(*) FROM tokens WHERE tokens.type = \'registration\'' +
        'AND tokens.company_id = \"Company\".\"id\"' +
        'AND tokens.deleted_at IS NULL)'
          ),
          invitationCountText ];

        const order = order_desc ? 'DESC' : 'ASC';

        return Company.findAll({
          attributes: {
            exclude: [ 'updated_at', 'role', 'bank', 'bank_account', 'isFideicomiso' ],
            include: [ userCount, invitationCount ]
          },
          include: [
            { model: User },
            { model: Token }
          ],
          where: {
            role
          },
          offset,
          limit,
          order: [ [ sequelize.col(order_by || 'created_at'), order ] ],
          group: [ 'Company.id' ]
        })
          .then(companies => {
            const com = _.map(companies, value => {
              const plain = _.omit(value.get({ plain: true }), [
                'Users',
                'Tokens',
                'created_at',
                'description',
                'taxpayer_type',
                'balance',
                'agreed_at',
                'bank',
                'bank_account',
                'agreement_url',
                'prepayment'
              ]);

              plain.user_count = parseInt(plain.user_count, 10);
              plain.invitation_count = parseInt(plain.invitation_count, 10);

              plain.holder = plain.holder ? plain.holder : '';
              plain.clabe = plain.clabe ? plain.clabe : '';

              return plain;
            });

            return Promise.all(com);
          });
      },
      getUsers: function(companyId, orderBy, orderDesc) {
        const { Company, User, Token, sequelize } = require('../../models');

        const userOrder = sequelize.literal(`"Users.${orderBy}"`);
        const jsonData = _.includes([ 'name', 'role' ], orderBy);
        const jsonField = jsonData && orderBy === 'name' ? 'data.user_name' : 'data.user_type';
        const tokenOrder = jsonData ? sequelize.json(jsonField) : sequelize.col(orderBy);
        const usersQuery = {
          where: {
            id: companyId
          },
          include: [ {
            model: User,
            attributes: [
              'name',
              'email',
              [ sequelize.fn('text', 'active'), 'status' ],
              'role',
              'color'
            ]
          } ],
          order: [ [ userOrder, orderDesc ] ]
        };
        const tokensQuery = {
          where: {
            type: 'registration',
            company_id: companyId
          },
          attributes: [
            [ sequelize.json('data.user_name'), 'name' ],
            'email',
            [ sequelize.fn('text', 'pending'), 'status' ],
            [ sequelize.json('data.user_type'), 'role' ],
            [ sequelize.fn('text', _.sample(companyColors)), 'color' ]
          ],
          order: [ [ tokenOrder, orderDesc ] ],
          raw: true
        };

        return Promise.all([ Company.findOne(usersQuery), Token.findAll(tokensQuery) ])
          .then((result) => {
            const company = result[0];
            const tokens = result[1] || [];

            let users = [];

            if (!company) {
              const error = {
                errors: [ {
                  path: 'company',
                  message: 'Not found'
                } ]
              };

              return Promise.reject(error);
            }

            users = company.get({ plain: true }).Users;
            users = users.concat(tokens);

            return _.orderBy(users, [ orderBy ], [ orderDesc.toLowerCase() ]);
          });
      }
    }
  }
};
