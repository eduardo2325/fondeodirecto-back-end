const _ = require('lodash');
const XLSX = require('xlsx');
const s3 = require('/var/lib/core/js/s3');
const randtoken = require('rand-token');
const moment = require('/var/lib/core/js/moment');
const config = require('/var/lib/core/js/config');

module.exports = {
  options: {
    classMethods: {
      uploadCxcExcelReportDataToS3: function(arrayOfArrays, guid) {
        const ws = XLSX.utils.aoa_to_sheet(arrayOfArrays);
        const wb = XLSX.utils.book_new();

        XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');

        const randomString = randtoken.generate(16);
        const buf = XLSX.write(wb, { type: 'buffer', bookType: 'xlsx' });
        const nowMoment = moment().format();
        const key = `cxc_bank_reports/cxc-bank-report_${nowMoment}_${randomString}.xlsx`;
        const options = {
          ACL: 'public-read',
          ContentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        };

        return s3.upload(key, buf, guid, options)
          .then(() => {
            const url = `https://${config.aws.s3.bucket}.s3.amazonaws.com/${key}`;

            return Promise.resolve(url);
          });
      },
      savePendingTransactionsToBankReport(idsPendingTransactions, reportData, sequelizeTransaction) {
        const { BankReport, PendingTransaction } = require('../../models');

        return BankReport.create( reportData, { transaction: sequelizeTransaction })
          .then( response => {
            return PendingTransaction.update(
              { bank_report_id: response.id },
              {
                where: {
                  id: { $in: idsPendingTransactions }
                },
                transaction: sequelizeTransaction
              }
            );
          });
      },
      findNumberOfApprovedTransactions(reports) {
        const { PendingTransaction } = require('../../models');

        const promises = _.map(reports, (element) => {
          return PendingTransaction.count({
            where: { bank_report_id: element.id, status: 'APPROVED' }
          })
            .then( count => {
              return Promise.resolve({
                bank_report_id: element.id,
                no_transactions: element.total_generated + '/' + count,
                amount: element.total_amount.toFixed(2),
                emmission_date: element.generated_at.toString(),
                url: element.link,
                generated_by: element.User.name
              });
            });
        });

        return Promise.all(promises);
      }
    }
  }
};
