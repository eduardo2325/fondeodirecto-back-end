const _ = require('lodash');
const colors = require('../../vendor/colors');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
  options: {
    hooks: {
      beforeCreate: instance => {
        instance.color = _.sample(colors);

        return Promise.resolve()
          .then(() => bcrypt.genSaltSync(saltRounds))
          .then(salt => bcrypt.hashSync(instance.password, salt))
          .then(hash => {
            instance.password = hash;
            return instance;
          });
      },
      beforeValidate: user => {
        user.role = user.role.toUpperCase();

        return Promise.resolve(user);
      }
    },
    instanceMethods: {
      getInformation: function() {
        return new Promise((resolve) => {
          let user = this.get({ plain: true });

          user = _.pick(user, [
            'id', 'name', 'email', 'role', 'color'
          ]);

          user.status = 'active';

          return resolve(user);
        });
      }
    },
    classMethods: {
      verify: function(email, password) {
        const { Company } = require('../../models');
        let user = null;

        return this.findOne({
          where: {
            email
          },
          include: [
            { model: Company }
          ]
        })
          .then(u => {
            user = u;
            if (!user) {
              const error = {
                errors: [ {
                  path: 'Email or Password',
                  message: 'email or password doesn\'t match'
                } ]
              };

              return Promise.reject(error);
            }

            return bcrypt.compareSync(password, user.password);
          })
          .then(result => {
            if (!result) {
              const error = {
                errors: [ {
                  path: 'Email or Password',
                  message: 'email or password doesn\'t match'
                } ]
              };

              return Promise.reject(error);
            }

            return user;
          });
      },
      updatePassword: function(userId, actualPassword, newPassword) {
        const { User } = require('../../models');
        const query = {
          where: {
            id: userId
          }
        };

        let userInstance = {};

        return User.findOne(query).then(user => {
          userInstance = user;

          return bcrypt.compareSync(actualPassword, user.password);
        }).then(result => {
          if (!result) {
            const error = {
              errors: [ {
                path: 'actual_password',
                message: 'actual_password is incorrect'
              } ]
            };

            return Promise.reject(error);
          }

          return User.encryptPassword(newPassword);
        }).then(encrypted => {
          userInstance.password = encrypted;

          return userInstance.save();
        }).then(user => {
          return user.getInformation();
        }).catch(err => {
          return Promise.reject(err);
        });
      },
      resetPassword: function(email, password) {
        const { User } = require('../../models');
        const query = {
          where: {
            email
          }
        };

        let userInstance = {};

        return User.findOne(query).then(user => {
          userInstance = user;

          return User.encryptPassword(password);
        }).then(encrypted => {
          userInstance.password = encrypted;

          return userInstance.save();
        }).then(user => {
          return user.getInformation();
        }).catch(err => {
          return Promise.reject(err);
        });
      },
      encryptPassword: function(password) {
        return Promise.resolve()
          .then(() => bcrypt.genSaltSync(saltRounds))
          .then(salt => bcrypt.hashSync(password, salt));
      }
    }
  }
};
