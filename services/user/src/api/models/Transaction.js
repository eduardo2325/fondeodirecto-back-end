module.exports = {
  options: {
    classMethods: {
      /* eslint max-params: 0 */
      formatAndCreate: function(transaction, senderCompanyId, receiverCompanyId, invoice_id,
        is_payment, t, created_at) {
        const { Transaction } = require('../../models');

        const data = {
          type: transaction.type,
          amount: transaction.amount,
          originated_from_id: senderCompanyId,
          destinated_to_id: receiverCompanyId,
          invoice_id,
          is_payment
        };

        if (created_at) {
          data.created_at = created_at;
        }

        return Transaction.create(data, { transaction: t });
      },

      createDeposit: function(amount, originatedFrom, destinatedTo, invoiceId, createdAt, isPayment, transaction) {
        const { Transaction } = require('../../models');

        const data = {
          type: 'DEPOSIT',
          amount: String(amount),
          originated_from_id: originatedFrom,
          destinated_to_id: destinatedTo,
          invoice_id: invoiceId,
          is_payment: isPayment
        };

        return Transaction.create(data, { transaction });
      }
    }
  }
};
