const _  = require('lodash');
const SendengoStrategy = require('../helpers/sendengo-strategy');

module.exports = {
  options: {
    classMethods: {
      transaction: function(type, amount, companyId, data, transaction) {
        const { PendingTransaction } = require('../../models');

        return PendingTransaction.create({
          type,
          amount,
          company_id: companyId,
          data
        }, {
          transaction
        });
      },
      approve: function(id) {
        const { PendingTransaction } = require('../../models');
        const query = {
          where: {
            id
          }
        };

        return PendingTransaction.findOne(query).then(transaction => {
          if (!transaction) {
            const error = {
              errors: [ {
                path: 'PendingTransaction',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          transaction.status = 'APPROVED';

          return transaction.save();
        });
      },
      reject: function(id) {
        const { PendingTransaction } = require('../../models');
        const query = {
          where: {
            id
          }
        };

        return PendingTransaction.findOne(query).then(transaction => {
          if (!transaction) {
            const error = {
              errors: [ {
                path: 'PendingTransaction',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          transaction.status = 'REJECTED';

          return transaction.save();
        });
      },
      deposit: function(amount, companyId, key, depositDate) {
        const { PendingTransaction } = require('../../models');
        const data = {
          deposit_date: depositDate,
          key
        };

        return PendingTransaction.transaction('DEPOSIT', amount, companyId, data);
      },
      withdraw: function(amount, companyId) {
        const { PendingTransaction } = require('../../models');

        return PendingTransaction.transaction('WITHDRAW', amount, companyId);
      },
      invoiceOperation: function(companyId, invoice, transaction) {
        const { id, fund_date, company_id, client_company_id, investor_company_id, total } = invoice;
        const { PendingTransaction } = require('../../models');
        const data = {
          invoice_id: id,
          fund_date,
          company_id,
          client_company_id,
          investor_company_id
        };

        return PendingTransaction.transaction('WITHDRAW', total, companyId, data, transaction);
      },
      rejectInvoiceFund: function(invoiceId, t) {
        const { PendingTransaction, Operation, InvestorTransaction, Invoice } = require('../../models');
        const query = {
          where: {
            id: invoiceId,
            status: 'PUBLISHED'
          },
          include: [
            { model: Operation, where: { status: 'FUND_REQUESTED' }, include: InvestorTransaction }
          ]
        };
        let invoice = {};

        return Invoice.findOne(query).then(invoiceInstance => {
          if (!invoiceInstance) {
            const error = {
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          invoice = invoiceInstance;

          return PendingTransaction.update({
            status: 'REJECTED'
          }, {
            where: {
              data: {
                $contains: JSON.stringify({ invoice_id: invoiceId })
              },
              status: 'PENDING'
            },
            transaction: t
          });
        }).then(() => {
          return invoice;
        });
      },
      findAndValidateInvoiceFund: function(invoiceId) {
        const { PendingTransaction, Invoice, Company, Operation, InvestorTransaction } = require('../../models');
        const query = {
          where: {
            data: {
              invoice_id: invoiceId
            },
            status: 'PENDING'
          }
        };
        const invoiceQuery = {
          where: {
            id: invoiceId,
            status: 'PUBLISHED'
          },
          include: [
            { model: Company, as: 'Client' },
            { model: Company, as: 'Company' },
            { model: Operation, where: { status: 'FUND_REQUESTED' }, include: InvestorTransaction }
          ]
        };

        return Promise.all([
          PendingTransaction.findOne(query),
          Invoice.findOne(invoiceQuery)
        ]).then( ([ pendingTransaction, invoice ]) => {
          if (!pendingTransaction || !invoice) {
            const path = !pendingTransaction ? 'PendingTransaction' : 'Invoice';
            const error = {
              errors: [ {
                path,
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          const belongsToCompany = pendingTransaction.company_id === invoice.investor_company_id;

          if (!belongsToCompany) {
            const error = {
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          return [ invoice, pendingTransaction ];
        });
      },
      createInvoiceFund: function(invoiceId, companyId, t) {
        const { PendingTransaction, Transaction, Company, sequelize } = require('../../models');
        let invoice = {};
        let transaction = {};
        let fundPayment = 0;
        let receiver;

        return PendingTransaction.findAndValidateInvoiceFund(invoiceId)
          .then(([ invoiceInstance, pendingTransaction ]) => {
            invoice = invoiceInstance;
            transaction = pendingTransaction;

            return Promise.all([
              invoiceInstance.getFundEstimate(),
              SendengoStrategy.generateStrategyObjectByConfig()
            ]);
          }).then(([ estimate, sendengoStrategy ]) => {
            const selectedObjects = sendengoStrategy.selectByCompanyId(invoice.client_company_id, {
              sendengo: {
                fundPayment: invoice.total,
                receiver: invoice.client_company_id
              },
              default: {
                fundPayment: Number(estimate.fund_payment),
                receiver: transaction.data.company_id
              }
            });

            receiver = selectedObjects.receiver;
            fundPayment = selectedObjects.fundPayment;

            const cxcData = {
              type: 'DEPOSIT',
              amount: fundPayment
            };

            // First the money is discounted from the investor and then the fund payment is made to the cxc
            return Promise.all([
              Transaction.formatAndCreate(
                transaction,
                transaction.company_id,
                companyId,
                transaction.data.invoice_id,
                false,
                t
              ),
              Transaction.formatAndCreate(
                cxcData,
                companyId,
                receiver,
                transaction.data.invoice_id,
                true,
                t
              )
            ]);
          }).then(() => {
            transaction.status = 'APPROVED';

            return transaction.save({ transaction: t });
          }).then(() => {
            return Company.update({
              balance: sequelize.literal(`balance - ${transaction.amount}`)
            }, {
              where: {
                id: transaction.company_id
              },
              transaction: t
            });
          }).then(() => {
            return [ invoice, fundPayment ];
          });
      },
      createClientInvoicePayment: function(invoice, amount, date, receipt, transaction) {
        const { PendingTransaction } = require('../../models');
        const data = {
          invoice_id: invoice.id,
          payment_date: date
        };

        if (receipt) {
          data.key = receipt;
        }

        return PendingTransaction.transaction('DEPOSIT', amount, invoice.client_company_id, data, transaction);
      },
      createInvoiceCompleted: function(invoiceId, fondeoId, payments, sequelizeTransaction) {
        const { PendingTransaction, Invoice, Operation, Transaction, Company,
          sequelize } = require('../../models');
        let invoice;

        return Invoice.findOne({
          where: {
            id: invoiceId,
            status: 'PUBLISHED'
          },
          include: [
            { model: Company, as: 'Client' },
            { model: Company, as: 'Company' },
            { model: Operation, where: { status: 'PAYMENT_IN_PROCESS' } }
          ]
        })
          .then(invoiceInstance => {
            invoice = invoiceInstance;

            if (!invoice) {
              return Promise.reject({
                errors: [ {
                  path: 'Invoice',
                  message: 'Not found'
                } ]
              });
            }

            const pendingTransactionQuery = {
              where: {
                status: 'PENDING',
                company_id: invoice.client_company_id,
                'data.invoice_id': {
                  $eq: invoiceId
                }
              }
            };

            const promise = PendingTransaction.findOne(pendingTransactionQuery)
              .then(pendingTransactionInstance => {
                if (!pendingTransactionInstance) {
                  return Promise.reject({
                    errors: [ {
                      path: 'PendingTransaction',
                      message: 'Not found'
                    } ]
                  });
                }

                pendingTransactionInstance.status = 'APPROVED';
                pendingTransactionInstance.amount = payments.cxpPayment;
                pendingTransactionInstance.set('data.payment_date', payments.cxpPaymentDate);
                return pendingTransactionInstance.save({ transaction: sequelizeTransaction });
              });
            const promiseSendengo = SendengoStrategy.generateStrategyObjectByConfig();

            return Promise.all([ promise, promiseSendengo ]);
          })
          .then(([ result, sendengoStrategy ]) => {
            if (!result) {
              return Promise.reject({
                errors: [ {
                  path: 'PendingTransaction',
                  message: 'Not found'
                } ]
              });
            }

            const pendingTransaction = result;
            const investorTransaction = _.cloneDeep(pendingTransaction.get());
            const companyTransaction = _.cloneDeep(pendingTransaction.get());

            investorTransaction.amount = payments.investorPayment;
            companyTransaction.amount = payments.cxcPayment;

            const noneTransactionCreated = function() {
              return Promise.resolve(true);
            };
            const transactionFromFondeoToCxc = function() {
              return Transaction.formatAndCreate(
                companyTransaction,
                fondeoId,
                invoice.company_id,
                invoice.id,
                true,
                sequelizeTransaction,
                new Date(payments.fondeoPaymentDate)
              );
            };
            const callback = sendengoStrategy.selectObjectByCompanyId(invoice.client_company_id,
              noneTransactionCreated, transactionFromFondeoToCxc);


            return Promise.all([
              Transaction.formatAndCreate(
                pendingTransaction,
                invoice.client_company_id,
                fondeoId,
                invoice.id,
                true,
                sequelizeTransaction,
                new Date(payments.cxpPaymentDate)
              ),
              callback(),
              Transaction.formatAndCreate(
                investorTransaction,
                fondeoId,
                invoice.investor_company_id,
                invoice.id,
                false,
                sequelizeTransaction,
                new Date(payments.fondeoPaymentDate)
              ),
              Company.update({
                balance: sequelize.literal(`balance + ${payments.investorPayment}`)
              }, {
                where: {
                  id: invoice.investor_company_id
                },
                transaction: sequelizeTransaction
              })
            ]);
          })
          .then(() => invoice);
      },
      bankRawData: function(pendingTransactions, bankReport) {
        const { Operation, Invoice, Company, InvestorTransaction } = require('../../models');
        const promise = SendengoStrategy.generateStrategyObjectByConfig()
          .then( sendengoStrategy => {
            const invoices_ids = _.map(pendingTransactions, (element) => {
              return element.data.invoice_id;
            });


            const operationsPromise = Operation.findAll({
              where: {
                invoice_id: {
                  $in: invoices_ids
                }
              },
              include: [
                { model: Invoice,
                  include: [ { model: Company, as: 'Company' },
                    { model: Company, as: 'Client' }
                  ] },
                { model: InvestorTransaction }
              ]
            }).then( operations => {
              const _mapped = _.map(operations, operation => {
                const pendingTransaction = _.find(pendingTransactions, (p) => {
                  return p.data.invoice_id === operation.invoice_id;
                });
                const invoice = operation.Invoice;

                invoice.Operation = operation;
                let promiseAmount;

                if ( pendingTransaction.type === 'DEPOSIT') {
                  promiseAmount = Promise.resolve( invoice.getAdminCxcPayment() )
                    .then( cxcPayment => Promise.resolve(cxcPayment.reserve) );
                } else {
                  promiseAmount = Promise.resolve(invoice.getFundEstimate()).then(
                    estimate => Promise.resolve(estimate.fund_payment));
                }

                return promiseAmount.then((_amount) => {
                  const selected = bankReport && bankReport.created_at < pendingTransaction.created_at;

                  const selectedObjects = sendengoStrategy.selectByCompanyId(invoice.client_company_id, {
                    sendengo: {
                      company: invoice.Client,
                      amount: pendingTransaction.type === 'DEPOSIT' ? '0.00' : invoice.total.toFixed(2)
                    },
                    default: {
                      amount: _amount,
                      company: invoice.Company
                    }
                  });

                  const { amount, company } = selectedObjects;

                  return Promise.resolve({
                    reference: operation.id,
                    transaction_id: pendingTransaction.id,
                    destinated_to: company.business_name,
                    bank_name: company.bank,
                    clabe: company.clabe,
                    rfc: company.rfc,
                    amount,
                    selected,
                    status: pendingTransaction.status
                  });
                });
              });

              return Promise.all(_mapped);
            });

            return operationsPromise;
          });

        return promise;
      },
      buildBankReportQuery(queryParams) {
        const whereParams = {
          $and: [
            {
              $or: [
                {
                  $and: [
                    { status: 'PENDING' },
                    { type: 'DEPOSIT' },
                    { bank_report_id: queryParams.bank_report_id ? queryParams.bank_report_id : null },
                    { 'data.invoice_id': { $ne: null } },
                    { 'data.key': { $ne: null } }
                  ] },
                { $and: [
                  { status: 'PENDING' },
                  { type: 'WITHDRAW' },
                  { bank_report_id: queryParams.bank_report_id ? queryParams.bank_report_id : null },
                  { 'data.invoice_id': { $ne: null } },
                  { 'data.fund_date': { $ne: null } },
                  { 'data.investor_company_id': { $ne: null } }
                ] }
              ] }
          ]
        };

        const queryParamsReturnValue = {
          where: whereParams,
          order: [ [ 'created_at', 'DESC' ] ]
        };

        if ( queryParams.by_id ) {
          whereParams.id = queryParams.by_id;
        }

        Object.assign(queryParamsReturnValue, queryParams);

        return queryParamsReturnValue;
      }
    },
    instanceMethods: {
      isForInvestorInvoiceFund() {
        return this.type === 'WITHDRAW'
          && this.data.invoice_id
          && !this.data.key
          && this.data.fund_date
          && this.data.company_id
          && this.data.client_company_id
          && this.data.investor_company_id
          && this.status === 'PENDING';
      },
      getBasicInfo: function() {
        const plain = _.omit(this.get({ plain: true }), [ 'updated_at', 'bank_report_id' ]);

        plain.created_at = plain.created_at.toString();

        return new Promise((resolve) => {
          plain.amount = plain.amount.toFixed(2);
          plain.status = plain.status.toLowerCase();
          plain.type = plain.type.toLowerCase();
          plain.data = plain.data ? JSON.stringify(plain.data) : '{}';

          return resolve(plain);
        });
      }
    }
  }
};
