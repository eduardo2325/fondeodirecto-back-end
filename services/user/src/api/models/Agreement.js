const reqFast = require('req-fast');

const pdf = require('../../pdf/builder');
const s3 = require('/var/lib/core/js/s3');
const moment = require('/var/lib/core/js/moment');

module.exports = {
  options: {
    classMethods: {
      getSource(url) {
        /* istanbul ignore next */
        return new Promise((resolve, reject) => {
          reqFast(url, (err, resp) => {
            if (err) {
              reject(err);
            } else {
              resolve(resp.body);
            }
          });
        });
      },
      getContract(params) {
        return pdf.preview(pdf.getTemplate(params.template), {
          ...params.locals,
          FIRMA_CIUDAD: 'Guadalajara Jalisco, México',
          FIRMA_FECHA_DIAS: moment().format('D'),
          FIRMA_FECHA_MES: moment().format('MMMM'),
          FIRMA_FECHA_ANIO: moment().format('Y')
        });
      },
      findContract(companyId, role, params) {
        const { Agreement } = require('../../models');

        const data = {
          user_role: role,
          company_id: companyId
        };

        const options = {
          ...params,
          where: data,
          defaults: data
        };

        return Agreement.findOrCreate(options)
          .then(foundRows => foundRows && foundRows[0]);
      },
      previewContract(agreement, params, guid) {
        const opts = {
          Expires: Date.now() + 300
        };

        const key = `companies/${agreement.company_id}/agreements/${agreement.user_role}`;

        return s3.upload(`${key}.html`, this.getContract(params), guid, opts)
          .then(s3Url => s3.getUrl(s3Url, guid))
          .then(s3Url => {
            agreement.agreed_at = null;
            agreement.agreement_url = s3Url;
            return s3Url;
          });
      },
      finalizeContract(agreement, guid) {
        if (!agreement.agreement_url || agreement.agreement_url.indexOf('.html') === -1) {
          return Promise.reject(new Error('Agreement source is invalid'));
        }

        const key = agreement.agreement_url.split('?')[0].split('amazonaws.com/')[1];

        return Promise.resolve()
          .then(() => this.getSource(agreement.agreement_url))
          .then(htmlSource => pdf.build(htmlSource, pdf.getOptions()))
          .then(pdfBuffer => s3.upload(key.replace('.html', '.pdf'), pdfBuffer, guid))
          .then(s3Url => s3.getUrl(s3Url, guid))
          .then(s3Url => {
            agreement.agreed_at = new Date();
            agreement.agreement_url = s3Url;
            return s3Url;
          });
      }
    }
  }
};
