const randtoken = require('rand-token');

module.exports = {
  options: {
    hooks: {
      beforeCreate: instance => {
        const thirtyMinutesLater = new Date();

        thirtyMinutesLater.setMinutes(thirtyMinutesLater.getMinutes() + 30);
        instance.token = randtoken.generate(16);
        instance.expiration_date = thirtyMinutesLater;

        return Promise.resolve(instance);
      }
    },
    classMethods: {
      updateSuspensions: function(companyId, role, suspended, transaction) {
        return this.update({
          suspended
        }, {
          where: {
            company_id: companyId,
            user_role: role,
            expiration_date: {
              $gt: new Date()
            }
          },
          transaction
        });
      },
      verifyAndCreate: function(email, password) {
        const { Agreement, User } = require('../../models');
        const self = this;
        const response = {};

        return User.verify(email, password).then(user => {
          return Agreement.findContract(user.Company.id, user.role)
            .then(foundAgreement => {
              const sessionData = {
                user_id: user.id,
                user_role: user.role,
                company_agreed: Boolean(foundAgreement.agreed_at),
                company_role: user.Company.role,
                company_rfc: user.Company.rfc,
                company_id: user.Company.id,
                suspended: user.Company.suspended_roles.indexOf(user.role) !== -1
              };

              response.user = user;
              response.agreement = foundAgreement;

              return self.create(sessionData);
            });
        }).then(session => {
          response.session = session;

          return Promise.resolve(response);
        });
      },
      getInformation: function(token) {
        const { User, Session, Company } = require('../../models');
        const sessionQuery = {
          where: {
            token
          },
          include: [ {
            model: User,
            include: [ { model: Company } ]
          } ]
        };

        return Session.findOne(sessionQuery).then(session => {
          const company_plain = session.User.Company.get({ plain: true });

          const response = {
            token: {
              token: session.token,
              expiration_date: session.expiration_date.toString()
            },
            company_created_at: company_plain.created_at.toString(),
            user: {
              id: session.User.id,
              name: session.User.name,
              email: session.User.email,
              role: session.User.role,
              suspended: session.suspended,
              company: {
                rfc: session.User.Company.rfc,
                id: session.User.Company.id,
                role: session.User.Company.role,
                isFideicomiso: session.User.Company.isFideicomiso,
                prepayment: session.User.Company.prepayment ? session.User.Company.prepayment : false
              }
            }
          };

          return response;
        });
      },
      checkToken: function(token) {
        return this.findOne({
          where: {
            token
          }
        })
          .then(session => {
            if (!session || new Date() >= session.expiration_date) {
              return Promise.reject({
                errors: [ {
                  path: 'Token',
                  message: 'Token expired'
                } ]
              });
            }

            const thirtyMinutesLater = new Date();

            session.expiration_date = thirtyMinutesLater.setMinutes(thirtyMinutesLater.getMinutes() + 30);
            return session.save();
          });
      }
    }
  }
};
