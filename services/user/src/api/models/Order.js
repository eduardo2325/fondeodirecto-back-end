const _ = require('lodash');

module.exports = {
  options: {
    classMethods: {
      newFundOrder(invoices, investor) {
        const { Order, OrderInvoice } = require('../../models');

        return Order.create({
          investor_company_id: investor.id,
          type: 'FUND'
        })
          .then(order => {
            const orderInvoices = _.map(invoices, invoice => {
              return OrderInvoice.create({
                order_id: order.id,
                invoice_id: invoice.id
              });
            });

            return Promise.all(orderInvoices);
          });
      }
    }
  }
};
