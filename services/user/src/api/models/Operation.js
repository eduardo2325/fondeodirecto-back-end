const _ = require('lodash');

module.exports = {
  options: {
    hooks: {
    },
    attributes: {
    },
    classMethods: {
      getOperations: function(include, params, user, options) {
        const { Operation, Invoice, sequelize } = require('../../models');
        const { offset, order_by, order_desc } = params;
        const order = order_desc ? 'DESC' : 'ASC';
        let orderParam = order_by;

        if (orderParam === 'client_name') {
          orderParam = 'Invoice.Client.name';
        }
        if (orderParam === 'company_name') {
          const _order = options && options.role === 'CXP' ? 'Client' : 'Company';

          orderParam = `Invoice.${_order}.name`;
        }
        if (orderParam === 'total') {
          orderParam = 'Invoice.total';
        }
        if (orderParam === 'number') {
          orderParam = 'Invoice.number';
        }

        return Promise.resolve()
          .then(() => {
            let modelPromise;

            if (user.role === 'CXP') {
              modelPromise = Invoice.findAll({
                where: {
                  $and: [
                    { client_company_id: user.company_id },
                    { status: { $in: [ 'APPROVED', 'PUBLISHED' ] } },
                    [ `("Operation"."status" IS NULL OR "Operation"."status" ${options.operationStatusCondition}
                      ('PAYMENT_IN_PROCESS', 'COMPLETED'))` ]
                  ]
                },
                include,
                offset,
                order: [ [ sequelize.col('Invoice.created_at'), 'DESC' ] ]
              });
            } else if (user.role === 'CXC') {
              modelPromise = Operation.findAll({
                attributes: [ 'id',
                  'invoice_id',
                  'expiration_date',
                  'published_date',
                  'days_limit',
                  'status'
                ],
                include,
                offset,
                order: [ [ sequelize.col(orderParam || 'Operation.created_at'), order ] ]
              });
            }

            return modelPromise;
          })
          .then( operations => {
            let generalInfo;
            const newOperations = _.map(operations, value => {
              if (user.role === 'CXP') {
                generalInfo = value.getGeneralInfoWithOperation();

                if (options.compute_payment_values) {
                  let _values;

                  generalInfo = generalInfo.then( values => {
                    _values = values;

                    // TODO: compute the prepayment the next sprint
                    // return paymentsHelper.computePrepaidValues(_params);
                    return Promise.resolve({
                      prepaid: '0.00',
                      saving: '0.00'
                    });
                  }).then( prepaidValues => {
                    Object.assign(_values, prepaidValues);

                    return Promise.resolve(_values);
                  });
                }
              } else if (user.role === 'CXC') {
                generalInfo = value.getGeneralInfo();
              }

              return generalInfo;
            });

            return Promise.all(newOperations);
          });
      },
      getAdminOperations(params) {
        const { Invoice, Company, Operation, sequelize } = require('../../models');
        const {
          limit, offset, order_by, order_desc, client_name, company_name, investor_name,
          status, start_fund_date, end_fund_date, start_expiration_date, end_expiration_date
        } = params;
        const order = order_desc ? 'DESC' : 'ASC';
        let investorRequired = false;
        let orderParam = order_by;
        let orderStatus;

        if (investor_name) {
          investorRequired = true;
        }

        const invoiceInclude = [
          {
            model: Company,
            as: 'Client',
            required: true
          },
          {
            model: Company,
            as: 'Company',
            required: true
          },
          {
            model: Company,
            as: 'Investor',
            required: investorRequired
          }
        ];

        if (client_name) {
          invoiceInclude[0].where = { name: { $iLike: `%${client_name}%` } };
        }
        if (company_name) {
          invoiceInclude[1].where = { name: { $iLike: `%${company_name}%` } };
        }
        if (investor_name) {
          invoiceInclude[2].where = { name: { $iLike: `%${investor_name}%` } };
        }

        const where = {};
        const include = [ {
          model: Invoice,
          include: invoiceInclude,
          required: true,
          attributes: [ 'id', 'uuid', 'created_at', 'number', 'total', 'status', 'expiration', 'fund_date' ]
        } ];

        const whereOperations = {};

        if (status) {
          orderStatus = status.map(s => {
            return s.toUpperCase() === 'PUBLISHED' ? 'PENDING' : s.toUpperCase();
          });

          whereOperations.status = orderStatus;
        }

        if (start_expiration_date || end_expiration_date) {
          where.expiration = {};

          if (end_expiration_date) {
            const endDate = new Date(end_expiration_date);

            endDate.setHours(23, 59, 59, 59);
            where.expiration.$lte = endDate;
          }
          if (start_expiration_date) {
            const startDate = new Date(start_expiration_date);

            startDate.setHours(0, 0, 0, 0);
            where.expiration.$gte = startDate;
          }
        }

        if (start_fund_date || end_fund_date) {
          where.fund_date = {};

          if (end_fund_date) {
            const endDate = new Date(end_fund_date);

            endDate.setHours(23, 59, 59, 59);
            where.fund_date.$lte = endDate;
          }
          if (start_fund_date) {
            const startDate = new Date(start_fund_date);

            startDate.setHours(0, 0, 0, 0);
            where.fund_date.$gte = startDate;
          }
        }

        include[0].where = where;

        if (orderParam === 'client_name') {
          orderParam = 'Invoice.Client.name';
        }
        if (orderParam === 'company_name') {
          orderParam = 'Invoice.Company.name';
        }
        if (orderParam === 'investor_name') {
          orderParam = 'Invoice.Investor.name';
        }
        if (orderParam === 'operation_status') {
          orderParam = 'status';
        }
        const paramsFindAll = {
          attributes: [ 'id',
            'invoice_id',
            'expiration_date',
            'published_date',
            'days_limit',
            'status'
          ],
          include: include,
          offset,
          limit,
          where: whereOperations,
          order: [ [ sequelize.col(orderParam || 'Operation.created_at'), order ] ]
        };

        const operationsPromise = Operation.findAll(paramsFindAll).then( operations => {
          const newOperations = _.map( operations, value => value.getGeneralInfoAsAdmin() );

          return Promise.all(newOperations);
        });

        const operationsPromiseCount = Operation.count({ include: include, where: whereOperations });

        return Promise.all([ operationsPromise, operationsPromiseCount ])
          .then(([ operations, count ]) => {
            return { operations, count };
          });
      }
    },
    instanceMethods: {
      getGeneralInfo: function() {
        let status = this.status.toLowerCase();

        if (status === 'pending') {
          status = 'published';
        }

        return Promise.resolve({
          id: this.id,
          invoice_id: this.invoice_id,
          expiration_date: this.expiration_date.toString(),
          published_date: this.published_date.toString(),
          days_limit: this.days_limit,
          status,
          uuid: this.Invoice.uuid,
          number: this.Invoice.number,
          total: this.Invoice.total.toFixed(2),
          company_name: this.Invoice.Client ? this.Invoice.Client.name : this.Invoice.Company.name,
          prepaid: '0.00',
          saving: '0.00',
          company_id: this.Invoice.Company ? this.Invoice.Company.id : this.Invoice.Client.id
        });
      },
      getBasicInfo: function() {
        let status = this.status.toLowerCase();

        if (status === 'pending') {
          status = 'published';
        }

        return Promise.resolve({
          id: this.id,
          invoice_id: this.invoice_id,

          annual_cost_percentage: this.annual_cost_percentage.toFixed(2),
          reserve_percentage: this.reserve_percentage.toFixed(2),
          fd_commission_percentage: this.fd_commission_percentage.toFixed(2),
          annual_cost: this.annual_cost.toFixed(2),
          reserve: this.reserve.toFixed(2),
          fd_commission: this.fd_commission.toFixed(2),
          factorable: this.factorable.toFixed(2),
          subtotal: this.subtotal.toFixed(2),
          fund_payment: this.fund_payment.toFixed(2),
          commission: this.commission.toFixed(2),
          operation_cost: this.operation_cost.toFixed(2),
          fund_total: this.fund_total.toFixed(2),
          expiration_date: this.expiration_date.toString(),
          published_date: this.published_date.toString(),
          days_limit: this.days_limit,
          status,
          formula: this.formula,
          user_id: Number(this.user_id)
        });
      },

      getGeneralInfoAsAdmin: function() {
        let status = this.status.toLowerCase();

        if (status === 'pending') {
          status = 'published';
        }

        return Promise.resolve({
          id: this.id,
          invoice_id: this.invoice_id,
          expiration_date: this.expiration_date.toString(),
          published_date: this.published_date.toString(),
          days_limit: this.days_limit,
          status,
          uuid: this.Invoice.uuid,
          company_name: this.Invoice.company_name,
          number: this.Invoice.number,
          total: this.Invoice.total.toFixed(2),
          created_at: this.created_at,
          invoice: {
            id: this.Invoice.id,
            uuid: this.Invoice.uuid,
            client_company_id: this.Invoice.Client.id,
            client_name: this.Invoice.Client.name,
            client_color: this.Invoice.Client.color,
            company_id: this.Invoice.Company.id,
            company_name: this.Invoice.Company.name,
            company_color: this.Invoice.Company.color,
            investor_name: this.Invoice.Investor ? this.Invoice.Investor.name : '',
            number: String(this.Invoice.number),
            fund_date: this.Invoice.fund_date ? this.Invoice.fund_date.toString() : '',
            expiration: this.Invoice.expiration ? this.Invoice.expiration.toString() : '',
            status: this.Invoice.status.toLowerCase(),
            total: this.Invoice.total.toFixed(2),
            operation_status: status
          }
        });
      }
    }
  }
};
