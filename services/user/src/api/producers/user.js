const BaseProducer = require('/var/lib/core/js/kafka/base-producer');
const _ = require('lodash');
const log = new (require('/var/lib/core/js/log'))(module);
const pdf = require('../../pdf/builder');
const s3 = require('/var/lib/core/js/s3');
const moment = require('/var/lib/core/js/moment');
const moneyFormat = require('../helpers/money-format');
const { User, Company, Agreement, NewInvoiceNotification } = require('../../models');

class UserProducer extends BaseProducer {
  constructor() {
    super('user');
  }

  _produce(key, type, body, guid) {
    return Promise.resolve()
      .then(() => {
        /* istanbul ignore next */
        if (body.attachments) {
          body.cc = 'legal@fondeodirecto.com';
        }

        const event = {
          message: {
            type,
            body,
            guid
          },
          key
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  _getPDF(params, guid, key) {
    return Promise.resolve()
      .then(() => pdf.build(Agreement.getContract(params), pdf.getOptions()))
      .then(pdfBuffer => s3.upload(key, pdfBuffer, guid, { Expires: Date.now() + 300 }))
      .then(s3Url => s3.getUrl(s3Url, guid));
  }

  _findUsers(companyId, role) {
    const userQuery = {
      where: {
        company_id: companyId,
        role
      },
      attributes: [ 'name', 'email' ],
      raw: true
    };

    return User.findAll(userQuery)
      .then(users => users.map(user => [ user.name, user.email ]));
  }

  dummyEvent() {
    const event = {
      message: {
        type: 'dummyEvent',
        body: {
          username: 'SYSTEM'
        },
        guid: 'dummy'
      },
      key: 'DLMD'
    };

    log.message('Producing dummy event', event, 'Event');
    return this.produce(event);
  }

  invitationCreated(token, company, guid) {
    const companyRole = company.role;

    const event = {
      message: {
        type: 'InvitationCreated',
        body: {
          token: token.token,
          email: token.email,
          username: token.data.user_name,
          company_role: companyRole
        },
        guid
      },
      key: token.token
    };

    log.message('Producing user event', event, 'Event', guid);

    return this.produce(event);
  }

  resendInvitation(token, company, guid) {
    const companyRole = company.role;
    const event = {
      message: {
        type: 'ResendInvitation',
        body: {
          token: token.token,
          email: token.email,
          username: token.data.user_name,
          company_role: companyRole
        },
        guid
      },
      key: token.token
    };

    log.message('Producing user event', event, 'Event', guid);

    return this.produce(event);
  }

  passwordChanged(user, guid) {
    const event = {
      message: {
        type: 'PasswordChanged',
        body: {
          username: user.name,
          email: user.email
        },
        guid
      },
      key: user.id
    };

    log.message('Producing user event', event, 'Event', guid);

    return this.produce(event);
  }

  recoverPassword(token, guid) {
    const event = {
      message: {
        type: 'RecoverPassword',
        body: {
          token: token.token,
          email: token.email,
          username: token.data.user_name
        },
        guid
      },
      key: token.token
    };

    log.message('Producing user event', event, 'Event', guid);

    return this.produce(event);
  }

  invoiceCreated(invoice, guid) {
    const userQuery = {
      where: {
        role: 'CXP',
        company_id: invoice.client_company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        return Company.findOne({
          where: {
            id: invoice.company_id
          }
        });
      }).then(company => {
        const event = {
          message: {
            type: 'InvoiceCreated',
            body: {
              emails: emails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              company_name: company.name
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoiceApproved(invoice, guid) {
    const userQuery = {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        return invoice.getClient();
      }).then(client => {
        const event = {
          message: {
            type: 'InvoiceApproved',
            body: {
              emails: emails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              client_name: client.name
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  // FIXME: abstract and reduce code, notify* methods are very similar!
  notifyInvoicePublished(userRole, invoices, guid) {
    const now = moment();
    const invoice = invoices[0];

    const published_at = {
      day: now.format('D'),
      month: now.format('MMMM'),
      time: now.format('HH:mm')
    };

    const companyId = invoice.company_id;

    let emails;

    let tableRawHtml = `
    <table class="invoices-table">
      <tr>
        <th>No. Factura</th>
        <th>Fecha de expedición</th>
        <th>Costo financiero</th>
        <th>Reserva</th>
        <th>Fecha de pago señalada por la sociedad pagadora</th>
        <th>Total de la factura</th>
        <th>Sociedad pagadoras</th>
      </tr> `;

    const promises = _.map(invoices, _invoice => {
      return Promise.all([
        _invoice.getClient(),
        _invoice.getCompany(),
        _invoice.getFundEstimate()
      ])
        .then(([ client, company, fundEstimate ]) => {
          return Promise.resolve( { invoice: _invoice, client, company, fundEstimate });
        });
    });

    const promiseTable = Promise.all(promises)
      .then( _data_array => {
        let numbers = '';

        _.each(_data_array, _data => {
          const rowHtml = `
            <tr>
              <td>${_data.invoice.number}</td>
              <td>${moment(_data.invoice.emission_date).format('LL')}</td>
              <td>${moneyFormat(_data.fundEstimate.operation_cost)}</td>
              <td>${moneyFormat(_data.fundEstimate.reserve)}</td>
              <td>${moment(_data.invoice.expiration).format('LL')}</td>
              <td>${moneyFormat(_data.fundEstimate.total)}</td>
              <td>${_data.company.business_name}</td>
            </tr>`;

          numbers = `${_data.invoice.number}|${_data.client.name}, ${numbers}`;

          tableRawHtml += rowHtml;
        });

        tableRawHtml += '</table>';
        return Promise.resolve(tableRawHtml);
      });

    return this._findUsers(companyId, userRole)
      .then(recipients => {
        emails = recipients;

        return Promise.all([
          invoice.getClient(),
          invoice.getCompany(),
          promiseTable
        ]);
      })
      .then(([ client, company, _tableRawHtml ]) => {
        const params = {
          template: 'cxc_publish',
          locals: {
            NO_FACTURA: '-NOT-USED-',
            ANEXO_NUMERO: Number(now),
            ANEXO_FECHA: now.format('LL'),
            PROVEEDOR_EMPRESA_NOMBRE: company.business_name,
            FECHA_FACTURA: '-NOT-USED-',
            FECHA_PAGO_FACTURA: '-NOT-USED-',
            COSTO_FINANCIERO: '-NOT-USED-',
            FACTURA_RESERVA: '-NOT-USED-',
            FACTURA_GARANTIA: '-NOT-USED-',
            FACTURA_PRECIO: '-NOT-USED-',
            EMPRESA_PAGADORA: '-NOT-USED-',
            TABLE_INVOICES: _tableRawHtml
          }
        };

        const id = params.locals.ANEXO_NUMERO;
        const key = `companies/${invoice.company_id}/annexums/cxc_${id}.pdf`;

        return Promise.resolve()
          .then(() => this._getPDF(params, guid, key))
          .then(s3Url => {
            return this._produce(invoice.id, 'NotifyInvoicePublished', {
              recipients: emails,
              attachments: [ {
                filename: `Anexo_A_${invoice.company_id}_${id}.pdf`,
                path: s3Url
              } ],
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              client_name: client.name,
              user_role: userRole,
              published_at
            }, guid);
          });
      });
  }

  notifyInvoiceApproved(invoices, guid) {
    const now = moment();
    const _invoice = invoices[0];

    const approved_at = {
      day: now.format('D'),
      month: now.format('MMMM'),
      time: now.format('HH:mm')
    };

    let tableRawHtml = `
    <table class="invoices-table">
      <tr>
        <th>Proveedor</th>
        <th>Factura</th>
        <th>Folio Fiscal</th>
        <th>Monto</th>
        <th>Fecha de pago</th>
      </tr>
  `;

    let emails;
    let invs;
    const htmlRows = invoices.map(invoice => {
      return this._findUsers(invoice.client_company_id, 'CXP')
        .then(recipients => {
          emails = recipients;

          const promises = invoices.map(inv => {
            return inv.getBasicInfo();
          });

          return Promise.all([
            invoice.getCompany(),
            Promise.all(promises)
          ]);
        })
        .then(([ company, invoiceInstances ]) => {
          invs = invoiceInstances;
          const rowHtml = `
            <tr>
              <td>${company.business_name}</td>
              <td>${invoice.number}</td>
              <td>${invoice.uuid}</td>
              <td>$${moneyFormat(invoice.total)}</td>
              <td>${moment(invoice.expiration).format('LL')}</td>
            </tr>
          `;

          return Promise.resolve(rowHtml);
        });
    });

    return Promise.resolve()
      .then(() => {
        return Promise.all(htmlRows);
      })
      .then(rawHtmlRows => {
        _.each(rawHtmlRows, row => {
          tableRawHtml += row;
        });

        tableRawHtml += '</table>';

        return true;
      })
      .then(() => {
        const params = {
          template: 'cxp_approve',
          locals: {
            TABLE_INVOICES: tableRawHtml
          }
        };

        const id = Number(now);
        const key = `companies/${_invoice.company_id}/annexums/cxp_${id}.pdf`;

        return Promise.resolve()
          .then(() => this._getPDF(params, guid, key))
          .then(s3Url => {
            return this._produce(_invoice.company_id, 'NotifyInvoiceApproved', {
              recipients: emails,
              invoices: invs,
              attachments: [ {
                filename: `Anexo_A_${_invoice.company_id}_${id}.pdf`,
                path: s3Url
              } ],
              approved_at
            }, guid);
          });
      });
  }

  notifyFundApproved(invoice, guid) {
    const now = moment();

    const fund_at = {
      day: now.format('D'),
      month: now.format('MMMM'),
      time: now.format('HH:mm')
    };

    let emails;

    return this._findUsers(invoice.client_company_id, 'CXP')
      .then(recipients => {
        emails = recipients;

        return Promise.all([
          invoice.getClient(),
          invoice.getCompany()
        ]);
      })
      .then(([ client, company ]) => {
        const params = {
          template: 'fd_fund',
          locals: {
            NO_FACTURA: invoice.number,
            ANEXO_NUMERO: Number(now),
            ANEXO_FECHA: now.format('LL'),
            EMPRESA_PAGADORA: company.business_name,
            NOMBRE_PROVEEDOR: client.business_name,
            FECHA_FACTURA: moment(invoice.emission_date).format('LL'),
            FECHA_PAGO_FACTURA: moment(invoice.expiration).format('LL')
          }
        };

        const id = params.locals.ANEXO_NUMERO;
        const key = `companies/${invoice.company_id}/annexums/cxp_${id}.pdf`;

        return Promise.resolve()
          .then(() => this._getPDF(params, guid, key))
          .then(s3Url => {
            return this._produce(invoice.id, 'NotifyFundApproved', {
              recipients: emails,
              attachments: [ {
                filename: `Anexo_A_${invoice.company_id}_${id}.pdf`,
                path: s3Url
              } ],
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              client_name: client.name,
              nombre_proveedor: client.name,
              fecha_expedicion: params.locals.FECHA_FACTURA,
              fecha_pago: params.locals.FECHA_PAGO_FACTURA,
              fund_at
            }, guid);
          });
      });
  }

  notifyFundRequest(invoices, investor, guid) {
    const now = moment();

    const requested_at = {
      day: now.format('D'),
      month: now.format('MMMM'),
      time: now.format('HH:mm')
    };

    let emails;
    const attachments = [];
    const tableRawHtml = `<table class="invoices-table">
        <tr>
          <td>No. Factura</td>
          <td>Fecha de expedición</td>
          <td>Fecha de pago señalada por la Sociedad Pagadora</td>
          <td>Monto de la factura</td>
          <td>Intereses</td>
          <td>Costo por Operación</td>
        </tr>
      `;

    const invoicesData = invoices.map(invoice => {
      return this._findUsers(invoice.investor_company_id, 'INVESTOR')
        .then(recipients => {
          emails = recipients;

          return Promise.all([
            invoice.getClient(),
            invoice.getInvestorFundEstimate(investor)
          ]);
        })
        .then(([ client, fundEstimate ]) => {
          const pugInvoice = {
            number: invoice.number,
            emmission_date: moment(invoice.emission_date).format('LL'),
            expiration_date: moment(invoice.expiration).add(10, 'days').format('LL'),
            total: moneyFormat(fundEstimate.total),
            interest: moneyFormat(fundEstimate.earnings),
            fee: moneyFormat(fundEstimate.commission)
          };

          const rawRow = `
            <tr>
              <td>${pugInvoice.number}</td>
              <td>${pugInvoice.emmission_date}</td>
              <td>${pugInvoice.expiration_date}</td>
              <td>${pugInvoice.total}</td>
              <td>${pugInvoice.interest}</td>
              <td>${pugInvoice.fee}</td>
            </tr>`;

          return Promise.resolve({
            rawRow,
            client_name: client.name,
            invoice,
            pugInvoice
          });
        });
    });

    const pugInvoices = [];

    return Promise.resolve()
      .then(() => {
        return Promise.all(invoicesData);
      })
      .then((_invoicesData) => {
        const clients = [];
        const invoices_numbers = [];
        let rawHtml = '';

        _.forEach(_invoicesData, (value) => {
          rawHtml += value.rawRow;
          invoices_numbers.push(value.invoice.number);
          clients.push(value.client_name);
          pugInvoices.push(value.pugInvoice);
        });

        rawHtml = tableRawHtml + `${rawHtml}</table>`;
        const _invoice = invoices[0];
        const params = {
          template: 'investor_fund',
          locals: {
            ANEXO_NUMERO: Date.now(),
            NOMBRE_INVERSIONISTA: investor.business_name,
            PERSONA_FISICA_NOMBRE_O_RAZON_SOCIAL: investor.business_name,
            TABLE_INVOICES_RAW: rawHtml,
            NUMEROS_DE_FACTURAS: invoices_numbers.join(),
            NOMBRES_PROVEEDORES: _.uniq(clients).join()
          }
        };

        const id = params.locals.ANEXO_NUMERO;
        const key = `companies/${_invoice.company_id}/annexums/inv_${id}.pdf`;

        return Promise.resolve()
          .then(() => this._getPDF(params, guid, key))
          .then(s3Url => {
            const attachment = {
              filename: `Anexo_A_${_invoice.company_id}_${id}.pdf`,
              path: s3Url
            };

            attachments.push(attachment);

            return Promise.resolve(attachments);
          });
      })
      .then((_attachments) => {
        return this._produce(investor.id, 'NotifyFundRequest', {
          recipients: emails,
          mailerFlagFideicomiso: investor.isFideicomiso,
          attachments: _attachments,
          invoices: pugInvoices,
          investor_name: investor.name,
          requested_at
        }, guid);
      });
  }

  invoiceFundRequest(invoices, investor, guid) {
    const userQuery = {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        const event = {
          message: {
            type: 'InvoiceFundRequest',
            body: {
              emails: emails.join(),
              invoices,
              investor_name: investor.name
            },
            guid
          },
          key: investor.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoiceRejected(invoice, reason, guid) {
    const userQuery = {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        return invoice.getClient();
      }).then(client => {
        const event = {
          message: {
            type: 'InvoiceRejected',
            body: {
              emails: emails.join(),
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              client_name: client.name,
              reason
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  publishedInvoiceRejected(invoice, reason, guid) {
    const userQuery = {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(userQuery)
      .then(users => {
        const emails = users.map(user => user.email);
        const event = {
          message: {
            type: 'PublishedInvoiceRejected',
            body: {
              emails: emails.join(),
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              reason
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  fundRequestedInvoiceRejected(invoice, reason, investorCompanyId, guid) {
    const userQuery = {
      where: {
        role: 'INVESTOR',
        company_id: investorCompanyId
      },
      attributes: [ 'email' ],
      raw: true
    };

    return Promise.all([
      User.findAll(userQuery)
    ])
      .then(([ users ]) => {
        const emails = users.map(user => user.email);
        const event = {
          message: {
            type: 'FundRequestedInvoiceRejected',
            body: {
              emails: emails.join(),
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              reason
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  fundRequestedInvoiceApproved(invoice, payment, guid) {
    const cxcQuery = {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    const cxpQuery = {
      where: {
        role: 'CXP',
        company_id: invoice.client_company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    const investorQuery = {
      where: {
        role: 'INVESTOR',
        company_id: invoice.investor_company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    const adminCompanyQuery = {
      where: {
        role: 'ADMIN'
      }
    };

    return Promise.all([
      User.findAll(cxcQuery),
      User.findAll(cxpQuery),
      User.findAll(investorQuery),
      Company.findOne(adminCompanyQuery)
    ])
      .then(([ cxcEmails, cxpEmails, investorsEmails, adminCompany ]) => {
        const cxcs = cxcEmails.map(user => user.email);
        const cxps = cxpEmails.map(user => user.email);
        const investors = investorsEmails.map(user => user.email);
        const bankInfo = {
          holder: adminCompany.holder,
          bank: adminCompany.bank,
          bank_account: adminCompany.bank_account,
          clabe: adminCompany.clabe
        };

        const event = {
          message: {
            type: 'FundRequestedInvoiceApproved',
            body: {
              cxc_emails: cxcs.join(),
              cxp_emails: cxps.join(),
              investor_emails: investors.join(),
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              invoice_expiration: invoice.expiration,
              invoice_company_name: invoice.Company.name,
              invoice_company_id: invoice.company_id,
              cxc_payment: moneyFormat(payment),
              bank_info: bankInfo
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  withdrawCreated(withdraw, company, guid) {
    const usersQuery = {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(usersQuery)
      .then(users => {
        const emails = users.map(u => u.email);
        const event = {
          message: {
            type: 'WithdrawCreated',
            body: {
              mailerFlagFideicomiso: company.role === 'INVESTOR' && company.isFideicomiso,
              emails: emails.join(),
              company_name: company.name,
              company_id: company.id
            },
            guid
          },
          key: withdraw.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  depositCreated(deposit, company, guid) {
    const usersQuery = {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(usersQuery)
      .then(users => {
        const emails = users.map(u => u.email);
        const event = {
          message: {
            type: 'DepositCreated',
            body: {
              emails: emails.join(),
              company_name: company.name,
              mailerFlagFideicomiso: company.role === 'INVESTOR' && company.isFideicomiso,
              company_id: company.id
            },
            guid
          },
          key: deposit.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  pendingTransactionApproved(pendingTransaction, company, guid) {
    const usersQuery = {
      where: {
        company_id: pendingTransaction.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(usersQuery)
      .then(users => {
        const emails = users.map(u => u.email);
        const event = {
          message: {
            type: 'PendingTransactionApproved',
            body: {
              emails: emails.join(),
              mailerFlagFideicomiso: company.role === 'INVESTOR' && company.isFideicomiso,
              type: pendingTransaction.type,
              amount: String(pendingTransaction.amount)
            },
            guid
          },
          key: pendingTransaction.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  pendingTransactionRejected(pendingTransaction, reason, guid) {
    const usersQuery = {
      where: {
        company_id: pendingTransaction.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };

    return Promise.all([
      User.findAll(usersQuery),
      Company.findOne( { where: { id: pendingTransaction.company_id } })
    ])
      .then( ([ users, company ]) => {
        const emails = users.map(u => u.email);
        const event = {
          message: {
            type: 'PendingTransactionRejected',
            body: {
              emails: emails.join(),
              type: pendingTransaction.type,
              mailerFlagFideicomiso: company.role === 'INVESTOR' && company.isFideicomiso,
              reason,
              amount: String(pendingTransaction.amount)
            },
            guid
          },
          key: pendingTransaction.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  clientInvoicePaymentCreated(amount, invoice, clientName, guid) {
    const usersQuery = {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    };

    const companyQuery = {
      where: {
        id: invoice.investor_company_id
      }
    };

    return Promise.all([
      User.findAll(usersQuery),
      Company.findOne(companyQuery)
    ])
      .then( ([ users, investor ]) => {
        const emails = users.map(u => u.email);
        const event = {
          message: {
            type: 'ClientInvoicePaymentCreated',
            body: {
              emails: emails.join(),
              client_name: clientName,
              mailerFlagFideicomiso: investor.isFideicomiso,
              invoice_id: invoice.id,
              invoice_number: invoice.number,
              amount
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoiceExpired([ invoice ], guid) {
    const userQuery = {
      where: {
        role: 'CXC',
        company_id: invoice.company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        return invoice.getClient();
      }).then(client => {
        const event = {
          message: {
            type: 'InvoiceExpired',
            body: {
              emails: emails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              client_name: client.name
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  companyRoleSuspensionUpdated(id, role, suspended, guid) {
    const userQuery = {
      where: {
        role: role,
        company_id: id
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(userQuery)
      .then(users => users.map(user => user.email))
      .then(emails => {
        const event = {
          message: {
            type: 'CompanyRoleSuspensionUpdated',
            body: {
              emails: emails.join(),
              company_id: id,
              suspended,
              role
            },
            guid
          },
          key: id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  investorInvitationCreated(invitation, guid) {
    const event = {
      message: {
        type: 'InvestorInvitationCreated',
        body: {
          emails: [ invitation.email ].join(),
          name: invitation.name,
          token: invitation.token
        },
        guid
      },
      key: invitation.id
    };

    log.message('Producing user event', event, 'Event', guid);

    return this.produce(event);
  }

  investorRoleSuspensionUpdated(id, suspended, guid) {
    const userQuery = {
      where: {
        role: 'INVESTOR',
        company_id: id
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(userQuery)
      .then(users => users.map(user => user.email))
      .then(emails => {
        const event = {
          message: {
            type: 'InvestorRoleSuspensionUpdated',
            body: {
              emails: emails.join(),
              company_id: id,
              suspended
            },
            guid
          },
          key: id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoicePaymentDue([ invoice ], guid) {
    const userQuery = {
      where: {
        $or: [ {
          role: 'CXP',
          company_id: invoice.client_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        const event = {
          message: {
            type: 'InvoicePaymentDue',
            body: {
              emails: emails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              company_name: invoice.Company.name
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoiceCompleted(invoice, paymentInfo, guid) {
    const { cxpPayment, cxcPayment, investorPayment } = paymentInfo;
    const cxpQuery = {
      where: {
        $or: [ {
          role: 'CXP',
          company_id: invoice.client_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };
    const cxcQuery = {
      where: {
        $or: [ {
          role: 'CXC',
          company_id: invoice.company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };
    const investorQuery = {
      where: {
        $or: [ {
          role: 'INVESTOR',
          company_id: invoice.investor_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };

    const _defaultPromiseCallback = function() {
      return User.findAll(cxcQuery);
    };
    const _emptyPromiseCallback = function() {
      return Promise.resolve([]);
    };

    const _callback = paymentInfo.cxcPayment === 0 ? _emptyPromiseCallback : _defaultPromiseCallback;

    return Promise.all([
      User.findAll(cxpQuery),
      _callback(),
      User.findAll(investorQuery)
    ])
      .then(([ cxp, cxc, investor ]) => {
        const cxpEmails = cxp.map(user => user.email);
        const cxcEmails = cxc.map(user => user.email);
        const investorEmails = investor.map(user => user.email);

        const event = {
          message: {
            type: 'InvoiceCompleted',
            body: {
              cxp_emails: cxpEmails.join(),
              cxc_emails: cxcEmails.join(),
              investor_emails: investorEmails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              cxp_payment: moneyFormat(cxpPayment),
              cxc_payment: moneyFormat(cxcPayment),
              investor_payment: moneyFormat(investorPayment)
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoiceLost(invoice, cxcTransaction, investorTransaction, guid) {
    const cxcQuery = {
      where: {
        $or: [ {
          role: 'CXC',
          company_id: invoice.company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };
    const investorQuery = {
      where: {
        $or: [ {
          role: 'INVESTOR',
          company_id: invoice.investor_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };

    return Promise.all([
      User.findAll(cxcQuery),
      User.findAll(investorQuery)
    ])
      .then(([ cxc, investor ]) => {
        const cxcEmails = cxc.map(user => user.email);
        const investorEmails = investor.map(user => user.email);

        const event = {
          message: {
            type: 'InvoiceLost',
            body: {
              cxc_emails: cxcEmails.join(),
              investor_emails: investorEmails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              cxc_payment: moneyFormat(cxcTransaction.amount),
              investor_payment: moneyFormat(investorTransaction.amount),
              payment_date: investorTransaction.created_at
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoiceLatePayment(invoice, cxcTransaction, investorTransaction, guid) {
    const cxcQuery = {
      where: {
        $or: [ {
          role: 'CXC',
          company_id: invoice.company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };
    const investorQuery = {
      where: {
        $or: [ {
          role: 'INVESTOR',
          company_id: invoice.investor_company_id
        }, {
          role: 'ADMIN'
        }
        ]
      },
      attributes: [ 'email' ],
      raw: true
    };

    return Promise.all([
      User.findAll(cxcQuery),
      User.findAll(investorQuery)
    ])
      .then(([ cxc, investor ]) => {
        const cxcEmails = cxc.map(user => user.email);
        const investorEmails = investor.map(user => user.email);

        const event = {
          message: {
            type: 'InvoiceLatePayment',
            body: {
              cxc_emails: cxcEmails.join(),
              investor_emails: investorEmails.join(),
              invoice_number: invoice.number,
              invoice_id: invoice.id,
              cxc_payment: moneyFormat(cxcTransaction.amount),
              investor_payment: moneyFormat(investorTransaction.amount)
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  companyProposed(proposed, name, guid) {
    const userQuery = {
      where: {
        role: 'ADMIN'
      },
      attributes: [ 'email' ],
      raw: true
    };

    return User.findAll(userQuery)
      .then(users => {
        const emails = users.map(user => user.email);
        const event = {
          message: {
            type: 'CompanyProposed',
            body: {
              emails: emails.join(),
              proposer_name: name,
              business_name: proposed.business_name,
              type: proposed.type,
              contact_name: proposed.contact_name,
              position: proposed.position,
              email: proposed.email,
              phone: proposed.phone
            },
            guid
          },
          key: name
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  agreementAccepted(params, guid) {
    let promise;

    if ( params.recipients ) {
      promise = Promise.resolve(params.recipients);
    } else {
      promise = this._findUsers(params.id, params.role);
    }

    return promise
      .then(recipients => {
        const titles = {
          CXC: 'Contrato de Factoraje con FONDEO DIRECTO (Proveedor)',
          CXP: 'Convenio Comercial con FONDEO DIRECTO (Comprador / Pagadora)',
          INVESTOR: 'Contrato de Comisión Mercantil con FONDEO DIRECTO (Inversionista)'
        };

        const now = moment(params.agreed_at);

        return this._produce(params.id, 'AgreementAccepted', {
          attachments: [ {
            filename: `${titles[params.role].replace(/[^a-z()]+/ig, '-').toLowerCase()}.pdf`,
            path: params.file
          } ],
          contract: titles[params.role],
          user_role: params.role,
          agreed_at: {
            day: now.format('D'),
            month: now.format('MMMM'),
            time: now.format('HH:mm')
          },
          recipients
        }, guid);
      });
  }

  notifyNewInvoices(invoices, guid) {
    let investors;

    const query = {
      where: {
        role: {
          $iLike: 'INVESTOR'
        }
      },
      attributes: [ 'name', 'email' ],
      raw: true
    };

    return User.findAll(query)
      .then(investorsInstance => {
        investors = investorsInstance;

        const promises = investors.map(investor => {
          return NewInvoiceNotification.createNewInvoiceToken(investor)
            .then(token => {
              investor.token = token.token;
            });
        });

        return Promise.all(promises);
      })
      .then(() => {
        const event = {
          message: {
            type: 'NotifyNewInvoices',
            users: {
              investors
            },
            guid
          },
          key: invoices[0].id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }

  invoicePaymentNotification(invoice, guid) {
    const userQuery = {
      where: {
        role: 'CXP',
        company_id: invoice.client_company_id
      },
      attributes: [ 'email' ],
      raw: true
    };
    let emails = [];

    return User.findAll(userQuery)
      .then(users => {
        emails = users.map(user => user.email);

        return invoice.getClient();
      }).then(client => {
        const event = {
          message: {
            type: 'InvoicePaymentNotification',
            body: {
              emails: emails.join(),
              invoice_id: invoice.id,
              client_name: client.name
            },
            guid
          },
          key: invoice.id
        };

        log.message('Producing user event', event, 'Event', guid);

        return this.produce(event);
      });
  }
}

module.exports = new UserProducer();
