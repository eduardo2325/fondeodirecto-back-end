const { Agreement, Session } = require('../../models');
const errorHandler = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);
const _ = require('lodash');

class SessionController {
  checkToken({ request }, callback) {
    log.message('Check Token', request, 'request', request.guid);
    return Session.checkToken(request.token)
      .then(session => {
        const response = _.omit(session.get(), [ 'id', 'created_at', 'updated_at', 'deleted_at' ]);

        response.expiration_date = response.expiration_date.toString();

        log.message('Check Token', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  deleteToken({ request }, callback) {
    log.message('Delete Token', request, 'request', request.guid);
    return Session.destroy({
      where: {
        token: request.token
      }
    })
      .then(() => {
        log.message('Delete Token', {}, 'response', request.guid);

        return callback(null, {});
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  verifyAndCreate({ request }, callback) {
    log.message('Verify and Create', request, 'request', request.guid);

    return Session.verifyAndCreate(request.email, request.password)
      .then(session => {
        const response = {
          token: {
            token: session.session.token,
            expiration_date: session.session.expiration_date.toString()
          },
          user: {
            id: session.user.id,
            name: session.user.name,
            email: session.user.email,
            role: session.user.role,
            suspended: session.session.suspended
          },
          company: {
            rfc: session.user.Company.rfc,
            id: session.user.Company.id,
            role: session.user.Company.role
          },
          agreement: {
            agreed_at: session.agreement.agreed_at instanceof Date
              ? session.agreement.agreed_at.toString()
              : undefined,
            agreement_url: session.agreement.agreement_url || undefined
          }
        };

        log.message('Verify and Create', response, 'response', request.guid);

        return Promise.resolve()
          .then(() => {
            let promise;

            if ([ 'INVESTOR', 'CXC', 'CXP' ].indexOf(session.user.role) !== -1 && !session.agreement.agreed_at) {
              let template = session.user.role;

              if (session.user.role === 'INVESTOR') {
                template += `_${session.user.Company.taxpayer_type}`;
              }

              const params = {
                template: template.toLowerCase(),
                locals: {
                  FACTORAJE_FIRMANTE_EMPRESA: session.user.Company.business_name,
                  FACTORAJE_FIRMANTE_NOMBRE: session.user.name,
                  PERSONA_MORAL_RAZON_SOCIAL: session.user.Company.business_name,
                  PERSONA_FISICA_NOMBRE: session.user.name
                }
              };

              promise = Agreement.previewContract(session.agreement, params, request)
                .then(s3Url => {
                  response.agreement.agreement_url = s3Url;
                  return session.agreement.save();
                });
            }

            return promise;
          })
          .then(() => {
            return callback(null, response);
          });
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  me({ request }, callback) {
    log.message('Get user credentials', request, 'request', request.guid);

    return Session.getInformation(request.token).then(response => {
      log.message('Get user credentials', response, 'response', request.guid);

      return callback(null, response);
    })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }
}

module.exports = new SessionController();
