const { sequelize, User, PendingTransaction, Operation, Company, Invoice,
  Transaction } = require('../../models');
const errorHandler = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);
const userProducer = require('../producers/user');
const stateMachine = require('../helpers/state-machine');

class TransactionController {
  withdraw({ request }, callback) {
    const { user_id, amount, guid } = request;
    let response;
    let user;

    log.message('Create withdraw', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then(() => User.findOne({
        where: {
          id: user_id
        },
        include: [
          { model: Company, as: 'Company' }
        ]
      })).then(u => {
        user = u;

        return user.Company.getBalance();
      }).then(balance => {
        if (Number(balance) < Number(amount)) {
          return Promise.reject({
            errors: [ {
              path: 'Transaction',
              message: 'Insufficient funds'
            } ]
          });
        }

        return PendingTransaction.withdraw(amount, user.company_id);
      }).then(withdraw => {
        return withdraw.getBasicInfo();
      }).then(withdraw => {
        response = withdraw;

        return userProducer.withdrawCreated(response, user.Company, guid);
      }).then(() => {
        log.message('Create withdraw', response, 'response', guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  deposit({ request }, callback) {
    log.message('Create deposit', request, 'request', request.guid);

    const query = {
      where: {
        id: request.user_id

      },
      include: [ {
        model: Company
      } ]
    };
    let response = {};
    let company = {};

    return userProducer.ensureConnection()
      .then(() => User.findOne(query)).then(user => {
        const amount = request.amount;
        const key = request.receipt;
        const depositDate = request.deposit_date;

        company = user.Company;

        return PendingTransaction.deposit(amount, user.company_id, key, depositDate);
      }).then(deposit => {
        return deposit.getBasicInfo();
      }).then(deposit => {
        response = deposit;

        return userProducer.depositCreated(response, company, request.guid);
      }).then(() => {
        log.message('Create deposit', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getTransactions({ request }, callback) {
    const { id, status, guid } = request;
    const query = {
      where: {
        company_id: id,
        status,
        data: { invoice_id: { $eq: null } }
      }
    };
    const companyQuery = {
      where: {
        id
      }
    };

    let response;
    let company;

    log.message('Get company transactions', request, 'request', request.guid);

    return Promise.all([ PendingTransaction.findAll(query), Company.findOne(companyQuery) ])
      .then(([ transactions, companyInstance ]) => {
        company = companyInstance;

        return Promise.all(transactions.map(t => t.getBasicInfo()));
      }).then(transactions => {
        response = {
          transactions
        };

        let promise;

        if (company.role === 'INVESTOR') {
          promise = company.getBalance();
        }

        return promise;
      })
      .then(balance => {
        response.balance = balance || '0.00';

        log.message('Get company transactions', response, 'response', guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, guid, callback);
      });
  }

  approve({ request }, callback) {
    const { id, guid, company_id } = request;
    const query = {
      where: {
        id,
        status: 'PENDING'
      }
    };

    let pendingTransaction;
    let company;

    log.message('Approve transaction', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => {
          return PendingTransaction.findOne(query);
        })
        .then(pendTran => {
          pendingTransaction = pendTran;

          if (!pendingTransaction) {
            return Promise.reject({
              errors: [ {
                path: 'PendingTransaction',
                message: 'Not found'
              } ]
            });
          }

          return Company.findOne({
            where: {
              id: pendTran.company_id
            }
          });
        })
        .then(com => {
          company = com;

          if (pendingTransaction.type === 'DEPOSIT') {
            com.balance += parseFloat(pendingTransaction.amount);
          }
          if (pendingTransaction.type === 'WITHDRAW') {
            if (com.balance < pendingTransaction.amount) {
              return Promise.reject({
                errors: [ {
                  path: 'PendingTransaction',
                  message: 'Insufficient funds'
                } ]
              });
            }
            com.balance -= pendingTransaction.amount;
          }

          return com.save({ transaction });
        })
        .then(() => {
          pendingTransaction.status = 'APPROVED';

          return pendingTransaction.save({
            transaction,
            returning: true
          });
        })
        .then(pendTran => {
          pendingTransaction = pendTran;

          if (pendTran.type === 'DEPOSIT') {
            return Transaction.formatAndCreate(pendingTransaction, company.id, company_id, null, true);
          }

          return Transaction.formatAndCreate(pendingTransaction, company_id, company.id, null, true);
        })
        .then(() => pendingTransaction.getBasicInfo())
        .then(pendTran => {
          pendingTransaction = pendTran;

          return userProducer.pendingTransactionApproved(pendingTransaction, company, request.guid);
        })
        .then(() => {
          log.message('Approve transaction', pendingTransaction, 'response', guid);

          return callback(null, pendingTransaction);
        })
        .catch(err => {
          transaction.rollback();

          return errorHandler.format(err, guid, callback);
        });
    });
  }

  reject({ request }, callback) {
    const { id, guid, reason } = request;
    const query = {
      where: {
        id,
        status: 'PENDING'
      }
    };

    let pendingTransaction;

    log.message('Reject transaction', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then(() => {
        return PendingTransaction.findOne(query);
      })
      .then(pendTran => {
        if (!pendTran) {
          return Promise.reject({
            errors: [ {
              path: 'PendingTransaction',
              message: 'Not found'
            } ]
          });
        }

        pendTran.status = 'REJECTED';

        return pendTran.save({
          returning: true
        });
      })
      .then(pendTran => pendTran.getBasicInfo())
      .then(pendTran => {
        pendingTransaction = pendTran;

        return userProducer.pendingTransactionRejected(pendingTransaction, reason, request.guid);
      })
      .then(() => {
        log.message('Reject transaction', pendingTransaction, 'response', guid);

        return callback(null, pendingTransaction);
      })
      .catch(err => errorHandler.format(err, guid, callback));
  }

  createClientInvoicePayment({ request }, callback) {
    log.message('Create client invoice payment', request, 'request', request.guid);

    const { user_role, company_id, invoice_id, amount, receipt, payment_date } = request;
    const invoiceQuery = {
      where: {
        id: invoice_id,
        status: 'PUBLISHED'
      },
      include: [
        { model: Company, as: 'Client' },
        { model: Operation,
          where: {
            status: {
              $or: [
                'FUNDED',
                'PAYMENT_DUE'
              ]
            }
          }
        }
      ]
    };
    let response = {};
    let invoice = {};

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => Invoice.findOne(invoiceQuery))
        .then(invoiceInstance => {
          const userIsAdmin = user_role === 'ADMIN';
          const userIsInvoiceClient = invoiceInstance ? company_id === invoiceInstance.Client.id : false;

          if (!invoiceInstance || !userIsAdmin && !userIsInvoiceClient) {
            return Promise.reject({
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            });
          }

          invoice = invoiceInstance;
          invoiceInstance.Operation.payment_date = payment_date;

          return Promise.all([
            PendingTransaction.createClientInvoicePayment(invoiceInstance, amount, payment_date, receipt, transaction),
            stateMachine.next(null, invoiceInstance.Operation, null, transaction)
          ]);
        }).then(() => {
          return invoice.getGeneralInfo(invoice.Client, 'client');
        }).then(invoiceInfo => {
          response = invoiceInfo;

          return userProducer.clientInvoicePaymentCreated(amount, invoice, invoice.Client.name, request.guid);
        });
    }).then(() => {
      log.message('Create client invoice payment', response, 'response', request.guid);

      return callback(null, response);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  getClientInvoicePayment({ request }, callback) {
    log.message('Get client invoice payment', request, 'request', request.guid);

    const { guid, invoice_id } = request;

    return Invoice.findOne({
      where: {
        id: invoice_id,
        status: 'PUBLISHED'
      },
      include: [
        { model: Operation, where: { status: 'PAYMENT_IN_PROCESS' } }
      ]
    }).then(invoice => {
      if (!invoice) {
        return Promise.reject({
          errors: [ {
            path: 'Invoice',
            message: 'Not found'
          } ]
        });
      }

      return PendingTransaction.findOne({
        where: {
          type: 'DEPOSIT',
          company_id: invoice.client_company_id,
          data: {
            invoice_id
          }
        }
      });
    }).then(payment => {
      if (!payment) {
        return Promise.reject({
          errors: [ {
            path: 'PendingTransaction',
            message: 'Not found'
          } ]
        });
      }

      return payment.getBasicInfo();
    }).then(response => {
      log.message('Get client invoice payment', response, 'response', guid);

      return callback(null, response);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }
}

module.exports = new TransactionController();
