const log = new (require('/var/lib/core/js/log'))(module);
const errorHandler = require('../helpers/error');
const { Operation, User, Invoice, Company } = require('../../models');

class OperationController {
  getOperations({ request }, callback) {
    log.message('Get operations', request, 'request', request.guid);
    let operations = {};
    let user;
    let invoiceWhere;
    let includeModelCompany;
    let required = false;
    let include;
    let operationStatusCondition;

    request.limit = request.limit || 25;

    if (request.offset > 0) {
      request.offset = request.limit * (request.offset - 1);
    }

    if (request.payed_operations) {
      required = true;
    }

    return User.findOne({
      where: {
        id: request.user_id
      }
    }).then(u => {
      user = u;
      const _options = user.role === 'CXP' ? { role: user.role } : null;

      if (user.role === 'CXP') {
        operationStatusCondition = request.payed_operations ? 'IN' : 'NOT IN';

        _options.compute_payment_values = true;
        _options.operationStatusCondition = operationStatusCondition;

        include = [
          { model: Company, as: 'Company', attributes: [ 'name' ], required: true },
          { model: Operation,
            attributes: [
              'id',
              'invoice_id',
              'expiration_date',
              'published_date',
              'days_limit',
              'status'
            ],
            required }
        ];
      } else if (user.role === 'CXC') {
        invoiceWhere = {
          company_id: user.company_id
        };
        includeModelCompany = { model: Company, as: 'Client', attributes: [ 'name' ], required: true };

        include = [ {
          model: Invoice,
          attributes: [ 'uuid', 'number', 'total' ],
          where: invoiceWhere,
          include: [ includeModelCompany ]
        } ];
      }
      return Operation.getOperations(include, request, user, _options);
    })
      .then(o => {
        operations = o;
        if (user.role === 'CXP') {
          return Invoice.count({
            where: {
              $and: [
                { client_company_id: user.company_id },
                { status: { $in: [ 'APPROVED', 'PUBLISHED' ] } },
                [ `("Operation"."status" IS NULL OR "Operation"."status" ${operationStatusCondition}
                  ('PAYMENT_IN_PROCESS', 'COMPLETED'))` ]
              ]
            },
            include
          });
        } else if (user.role === 'CXC') {
          return Operation.count({ include });
        }
        return true;
      })
      .then(count => {
        const response = {
          operations,
          total_operations: count,
          total_pages: Math.ceil(count / request.limit)
        };

        log.message('Get operations', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getPayedOperations({ request }, callback) {
    request.payed_operations = true;

    return this.getOperations( { request }, callback);
  }

  getOperation({ request }, callback) {
    log.message('Get operation', request, 'request', request.guid);

    const userQuery = {
      where: {
        id: request.user_id
      }
    };

    const operationQuery = {
      where: {
        id: request.operation_id
      },
      include: [ {
        model: Invoice,
        include: [ { all: true } ]
      } ]
    };

    let operation;
    let invoiceInst;
    let notFound;
    let user;
    let isInvestor;

    return Promise.all([ User.findOne(userQuery), Operation.findOne(operationQuery) ]).then( result => {
      user = result[0];

      operation = result[1];
      notFound = {
        errors: [ {
          path: 'Operation',
          message: 'Not found'
        } ]
      };

      if ( !operation || !user) {
        return Promise.reject(notFound);
      }

      isInvestor = user.role === 'INVESTOR';

      return operation.getInvoice();
    })
      .then( invoice => {
        invoiceInst = invoice;
        return Promise.all( [ invoice.getClient(), invoice.getCompany() ] );
      })
      .then( results => {
        invoiceInst.Client = results[0];
        invoiceInst.Company = results[1];

        const companyId = invoiceInst.company_id;
        const investorCompanyId = invoiceInst.investor_company_id;
        const isCxc = user.role === 'CXC';
        const doesBelongToCompany = user.company_id === companyId;
        const doesBelongToInvestor = invoiceInst.status === 'PUBLISHED' || user.company_id === investorCompanyId;
        const invoiceDoesBelongToUser = isInvestor ? doesBelongToInvestor : doesBelongToCompany;

        if (!invoiceDoesBelongToUser) {
          return Promise.reject(notFound);
        }

        let promiseInvoice;

        if (isInvestor || isCxc) {
          promiseInvoice = invoiceInst.getBasicInfoWithClient(isInvestor);
        } else {
          promiseInvoice = invoiceInst.getBasicInfo();
        }

        return Promise.all( [ operation.getBasicInfo(), promiseInvoice ]);
      })
      .then( results => {
        const operationBasic = results[0];
        const invoiceBasic = results[1];

        if (!invoiceBasic || !operationBasic) {
          return Promise.reject(notFound);
        }

        if (!invoiceBasic.client_created_at ) {
          invoiceBasic.client_created_at = '';
        }

        operationBasic.invoice = invoiceBasic;

        log.message('Get operation', operationBasic, 'response', request.guid);
        return callback(null, operationBasic);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getAdminOperations({ request }, callback) {
    log.message('Get admin operations', request, 'request', request.guid);

    request.limit = request.limit || 25;

    if (request.offset > 0) {
      request.offset = request.limit * (request.offset - 1);
    }

    request.status = request.status && request.status.length > 0 ? request.status : null;

    return Operation.getAdminOperations(request)
      .then(({ operations, count }) => {
        const response = {
          operations,
          total_operations: count,
          total_pages: Math.ceil(count / request.limit)
        };

        log.message('Get admin operations', response, 'response', request.guid);

        return callback(null, response );
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }
}

module.exports = new OperationController();
