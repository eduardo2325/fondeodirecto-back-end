const _ = require('lodash');
const { Invoice, User, Company, Operation, OperationCost, PendingTransaction,
  Transaction, InvestorTransaction, Order, UserActivity, sequelize } = require('../../models');
const errorHandler = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);
const userProducer = require('../producers/user');
const stateMachine = require('../helpers/state-machine');
const shoppingCartHelper = require('../helpers/shopping-cart');
const SendengoStrategy = require('../helpers/sendengo-strategy');

class InvoiceController {
  create({ request }, callback) {
    const userId = request.user_id;
    const companyRfc = request.company_rfc;
    const clientRfc = request.client_rfc;
    const createData = _.cloneDeep(request);

    let company = {};
    let clientCompany = {};
    let invoice;


    return Invoice.validateCompanies(userId, companyRfc, clientRfc)
      .then(instances => {
        company = instances[0];
        clientCompany = instances[1];

        delete createData.client_rfc;
        delete createData.company_rfc;
        createData.company_id = company.company_id;
        createData.client_company_id = clientCompany.id;

        return clientCompany.allowedCXP();
      })
      .then(allowedCXP => {
        if (!allowedCXP) {
          return Promise.reject({
            errors: [ {
              path: 'client_company_id',
              message: 'Receptor is suspended'
            } ]
          });
        }

        return true;
      })
      .then(() => {
        return Invoice.create(createData);
      })
      .then(inv => {
        return inv.getGeneralInfo(clientCompany, 'client');
      })
      .then(inv => {
        invoice = inv;

        return userProducer.invoiceCreated(invoice, request.guid);
      })
      .then(() => callback(null, invoice))
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  bulkDelete({ request }, callback) {
    const { guid, user_id, company_id, invoices } = request;

    log.message('Bulk delete invoices', request, 'request', guid);

    const invoices_ids = _.map(invoices, i => i.id);
    const today = new Date();

    const whereParams = {
      company_id,
      id: {
        $in: invoices_ids
      },
      status: {
        $or: [
          'PENDING',
          'APPROVED'
        ]
      }
    };
    const findParams = {
      where: whereParams
    };
    let processedInvoices;

    return sequelize.transaction( function(sequelizeTransaction) {
      return Invoice.findAll(findParams)
        .then( _invoices => {
          if (!_invoices || _invoices.length === 0) {
            const notFound = {
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            };

            return Promise.reject(notFound);
          }

          processedInvoices = _invoices;

          const promises = _.map(_invoices, invoice => {
            const uuid = invoice.uuid;
            const newUuid = `${uuid}/${today.getTime()}`;

            return invoice.update({ uuid: newUuid }, { transaction: sequelizeTransaction });
          });

          return Promise.all(promises);
        })
        .then(() => {
          const _ids = _.map(processedInvoices, _i => _i.id);

          return Invoice.destroy({
            where: {
              company_id,
              id: {
                $in: _ids
              }
            },
            paranoid: true,
            transaction: sequelizeTransaction
          });
        })
        .then(() => UserActivity.registerDeleteInvoices({ user_id }, processedInvoices, { sequelizeTransaction } ) );
    })
      .then( () => {
        const _invoices = _.map(processedInvoices, i => {
          return { id: i.id };
        });

        const response = {
          invoices: _invoices
        };

        log.message('Bulk delete invoices', response, 'response', guid);

        return callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  getInvoices({ request }, callback) {
    log.message('Get invoices', request, 'request', request.guid);
    let invoices = {};
    let user;
    let where;

    request.limit = request.limit || 25;

    if (request.offset > 0) {
      request.offset = request.limit * (request.offset - 1);
    }

    return User.findOne({
      where: {
        id: request.user_id
      }
    })
      .then(u => {
        user = u;

        if (user.role === 'CXP') {
          request.remove_limit_of_invoices = true;
          where = {
            client_company_id: user.company_id,
            status: { $notIn: [ 'APPROVED', 'PUBLISHED' ] }
          };
        } else if (user.role === 'CXC') {
          request.remove_limit_of_invoices = true;
          where = {
            company_id: user.company_id,
            status: { $notLike: 'PUBLISHED' }
          };
        }


        return Invoice.getInvoices(user, where, request);
      })
      .then(i => {
        invoices = i;

        return Invoice.count({
          where
        });
      })
      .then(count => {
        const response = {
          invoices,
          total_invoices: count,
          total_pages: Math.ceil(count / request.limit)
        };

        log.message('Get invoices', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvoice({ request }, callback) {
    log.message('Get invoice', request, 'request', request.guid);

    const userQuery = {
      where: {
        id: request.user_id
      }
    };

    const invoiceQuery = {
      include: [
        { all: true },
        { model: Operation, include: InvestorTransaction }
      ],
      where: {
        id: request.invoice_id
      }
    };

    return Promise.all([ User.findOne(userQuery), Invoice.findOne(invoiceQuery) ])
      .then(result => {
        const user = result[0];
        const invoice = result[1];
        const notFound = {
          errors: [ {
            path: 'Invoice',
            message: 'Not found'
          } ]
        };

        if (!user || !invoice ) {
          return Promise.reject(notFound);
        }

        const companyId = invoice.company_id;
        const investorCompanyId = invoice.investor_company_id;
        const isInvestor = user.role === 'INVESTOR';
        const isCxc = user.role === 'CXC';
        const doesBelongToCompany = user.company_id === companyId;
        const doesBelongToInvestor = invoice.status === 'PUBLISHED' || user.company_id === investorCompanyId;
        const invoiceDoesBelongToUser = isInvestor ? doesBelongToInvestor : doesBelongToCompany;

        if (!invoiceDoesBelongToUser) {
          return Promise.reject(notFound);
        }

        if (isInvestor || isCxc) {
          return invoice.getBasicInfoWithClient(isInvestor);
        }
        return invoice.getBasicInfo();
      })
      .then(response => {
        log.message('Get invoice', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  approve({ request }, callback) {
    const userId = request.user_id;
    const invoicesIds = _.map(request.invoices, i => i.id);
    const expiration = request.expiration;
    const response = {};

    let invoices = {};
    let expirationDate = '';

    log.message('Approve invoices', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then(() => {
        return Invoice.validateExpirationDate(expiration);
      }).then(date => {
        expirationDate = date;

        const inv = _.map(invoicesIds, invoiceId => {
          return Invoice.validateCxp(userId, invoiceId);
        });

        return Promise.all(inv);
      }).then(invoiceInstances => {
        invoices = invoiceInstances;
        const inv = _.map(invoiceInstances, invoice => {
          invoice.expiration = expirationDate;
          invoice.approved_date = new Date();
          return stateMachine.next(invoice);
        });

        return Promise.all(inv);
      })
      .then(() => {
        const inv = _.map(invoices, invoice => {
          return userProducer.invoiceApproved(invoice, request.guid);
        });

        return Promise.all(inv);
      })
      .then(() => userProducer.notifyInvoiceApproved(invoices, request.guid))
      .then(() => {
        response.success = true;
        log.message('Approve invoices', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  reject({ request }, callback) {
    const userId = request.user_id;
    const invoiceId = request.invoice_id;
    const reason = request.reason;

    let invoice = {};
    let response = {};

    log.message('Reject invoice', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then(() => {
        if (!reason) {
          const error = {
            errors: [ {
              path: 'reason',
              message: 'reason cannot be null'
            } ]
          };

          return Promise.reject(error);
        }

        return Invoice.validateCxp(userId, invoiceId);
      }).then(invoiceInstance => {
        return stateMachine.back(invoiceInstance);
      }).then(([ invoiceInstance ]) => {
        invoice = invoiceInstance;

        return invoiceInstance.getCompany();
      }).then(company => {
        return invoice.getGeneralInfo(company, 'company');
      }).then(plain => {
        response = plain;

        return userProducer.invoiceRejected(invoice, reason, request.guid);
      }).then(() => {
        log.message('Reject invoice', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  publish({ request }, callback) {
    const userId = request.user_id;
    const invoicesIds = _.map(request.invoices, i => i.id);

    let invoices = {};
    const response = {};
    const invoicesQuery = {
      where: {
        id: {
          $in: invoicesIds
        }
      }
    };

    log.message('Publish invoices', request, 'request', request.guid);

    return Invoice.findAll(invoicesQuery)
      .then(invoiceInstances => {
        if (invoiceInstances.length === 0) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }
        invoices = invoiceInstances;
        return true;
      })
      .then(() => {
        const inv = _.map(invoicesIds, invoiceId => {
          return Invoice.validateCxc(userId, invoiceId);
        });

        return Promise.all(inv);
      })
      .then(() => {
        const inv = _.map(invoices, invoice => {
          return invoice.getClient();
        });

        return Promise.all(inv);
      })
      .then(clients => {
        const inv = _.map(clients, client => {
          return client.allowedCXP()
            .then(allowedCXP => {
              if (!allowedCXP) {
                return Promise.reject({
                  errors: [ {
                    path: 'client_company_id',
                    message: 'Receptor is suspended'
                  } ]
                });
              }
              return true;
            });
        });

        return Promise.all(inv);
      })
      .then(() => {
        const saveInvoices = _.map(invoices, invoice => {
          invoice.published = new Date();

          return stateMachine.next(invoice);
        });

        return Promise.all(saveInvoices);
      })
      .then(() => {
        const saveInvoices = _.map(invoices, invoice => {
          return Invoice.createOperation(invoice, userId);
        });

        return Promise.all(saveInvoices);
      })
      .then(() => userProducer.notifyInvoicePublished('CXC', invoices, request.guid))
      .then(() => {
        response.success = true;
        log.message('Publish invoices', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getPublishSummary({ request }, callback) {
    const invoicesIds = _.map(request.invoices, i => i.id);

    const response = {};
    let validInvoices = 0;
    const invoicesQuery = {
      attributes: [
        'id', 'total', 'expiration', 'company_id', 'client_company_id', 'tax_total', 'number', 'emission_date',
        [ sequelize.literal(`CASE WHEN "Invoice"."status" IS NOT NULL
           AND "Invoice"."status" LIKE '%APPROVED%' THEN true ELSE false END`), 'is_available' ]
      ],
      where: {
        id: {
          $in: invoicesIds
        }
      },
      include: [
        { model: Company, as: 'Client', include: OperationCost }
      ]
    };

    log.message('Get invoices publish summary', request, 'request', request.guid);

    return Invoice.findAll(invoicesQuery)
      .then(invoiceInstances => {
        _.each(invoiceInstances, function(invoice) {
          validInvoices = invoice.dataValues.is_available ? validInvoices += 1 : validInvoices;
        });

        if (validInvoices === 0) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        const inv = _.map(invoiceInstances, i => i.getPublishEstimate());

        return Promise.all(inv);
      })
      .then(invoiceEstimates => {
        response.invoices = invoiceEstimates;
        response.total_invoices = invoiceEstimates.length;
        return shoppingCartHelper.getPublishSummaryTotals(invoiceEstimates);
      })
      .then(result => {
        response.total_invoices = result.totalInvoices;
        response.summary = result.summary;

        log.message('Get invoices publish summary', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  fund({ request }, callback) {
    const invoicesIds = _.map(request.invoices, i => i.id);

    let invoices;
    let unavailableInvoices = 0;
    let investorCompany;
    let investorGain;
    let response = {};
    const today = new Date();

    today.setHours(0, 0, 0, 0);
    const invoicesQuery = {
      attributes: [
        'id', 'total', 'expiration', 'company_id', 'client_company_id', 'tax_total', 'number', 'emission_date',
        [ sequelize.literal(`CASE WHEN "Operation"."status" IS NOT NULL
           AND "Operation"."status" LIKE '%PENDING%' THEN true ELSE false END`), 'is_available' ]
      ],
      where: {
        id: {
          $in: invoicesIds
        },
        status: 'PUBLISHED',
        expiration: {
          $gt: today
        }
      },
      include: [
        { model: Company, as: 'Company', include: OperationCost },
        { model: Company, as: 'Client', include: OperationCost },
        { model: Operation, required: true }
      ]
    };
    const userQuery = {
      where: {
        id: request.user_id
      },
      include: [
        { model: Company, include: OperationCost }
      ]
    };

    log.message('Fund invoices', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => Promise.all([ User.findOne(userQuery), Invoice.findAll(invoicesQuery) ]))
        .then(([ user, invoiceInstances ]) => {
          investorCompany = user.Company;
          invoices = invoiceInstances;

          const inv = _.map(invoiceInstances, i => i.getInvoiceAmounts(investorCompany));

          return Promise.all(inv);
        })
        .then(invoiceAmounts => {
          return shoppingCartHelper.getCartResponse(invoiceAmounts);
        })
        .then(result => {
          response = result;
          investorGain = result.summary.perception_total;
          delete response.summary.perception_total;

          _.each(result.invoices, function(invoice) {
            unavailableInvoices = !invoice.is_available ? unavailableInvoices += 1 : unavailableInvoices;
          });
          if (!request.fund || unavailableInvoices !== 0) {
            return Promise.reject();
          }
          return investorCompany.getBalance();
        })
        .then((balance) => {
          if (Number(investorGain) <= 0 || Number(balance) < Number(response.summary.invoices_total)) {
            return Promise.reject({
              errors: [ {
                path: 'Investor',
                message: 'Can not afford'
              } ]
            });
          }

          const saveInvoices = _.map(invoices, invoice => {
            invoice.fund_date = new Date();
            invoice.investor_company_id = investorCompany.id;

            return Promise.all([
              invoice.save({ transaction }),
              stateMachine.next(null, invoice.Operation, null, transaction)
            ]);
          });

          return Promise.all(saveInvoices);
        })
        .then(() => {
          const saveInvoices = _.map(invoices, invoice => {
            return Promise.all([
              Invoice.createInvestorTransaction(invoice, investorCompany),
              PendingTransaction.invoiceOperation(investorCompany.id, invoice, transaction)
            ]);
          });

          return Promise.all(saveInvoices);
        })
        .then(() => {
          return Order.newFundOrder(response.invoices, investorCompany);
        })
        .then(() => {
          const promises = _.map(invoices, invoice => {
            return invoice.getBasicOperationInfo();
          });

          return Promise.all(promises);
        })
        .then(inv => userProducer.invoiceFundRequest(inv, investorCompany, request.guid))
        .then(() => userProducer.notifyFundRequest(invoices, investorCompany, request.guid))
        .then(() => {
          response.fund = true;
          log.message('Fund invoices', response, 'response', request.guid);

          return callback(null, response);
        }).catch(err => {
          response.fund = false;
          if (!request.fund || unavailableInvoices !== 0) {
            transaction.rollback();
            return callback(null, response);
          }
          transaction.rollback();
          return errorHandler.format(err, request.guid, callback);
        });
    });
  }

  completed({ request }, callback) {
    const { invoice_id, company_id, guid } = request;
    const paymentsInfo = {
      cxpPayment: request.cxp_payment,
      cxpPaymentDate: request.cxp_payment_date,
      cxcPayment: request.cxc_payment,
      investorPayment: request.investor_payment,
      fondeoPaymentDate: request.fondeo_payment_date
    };

    let invoice;

    log.message('Completed invoice', request, 'request', guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => {
          return PendingTransaction.createInvoiceCompleted(invoice_id, company_id, paymentsInfo, transaction);
        })
        .then(invoiceInstance => {
          invoice = invoiceInstance;
          return stateMachine.next(null, invoiceInstance.Operation, null, transaction);
        }).then(() => {
          return SendengoStrategy.generateStrategyObjectByConfig();
        }).then(( sendengoStrategy ) => {
          const assignZero = () => {
            paymentsInfo.cxcPayment = 0;
          };

          sendengoStrategy.execCallback(invoice.client_company_id, assignZero);

          return Promise.all([
            invoice.getGeneralInfoAsAdmin(),
            userProducer.invoiceCompleted(invoice, paymentsInfo, guid)
          ]);
        })
        .then(([ invoiceInfo ]) => {
          invoiceInfo.emission_date = invoice.emission_date.toString();
          invoiceInfo.operation_id = invoice.Operation ? invoice.Operation.id : 0;

          log.message('Completed invoice', invoiceInfo, 'response', guid);

          return callback(null, invoiceInfo);
        });
    }).catch(err => errorHandler.format(err, guid, callback));
  }

  lost({ request }, callback) {
    const invoiceId = request.invoice_id;
    const fondeoId = request.company_id;

    let response = {};
    let invoice;
    let operation;

    log.message('Lost operation', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => {
          return Invoice.findOne({
            where: {
              id: invoiceId,
              status: 'PUBLISHED'
            },
            include: [
              { model: Company, as: 'Company' },
              { model: Company, as: 'Client' },
              { model: Operation, where: { status: 'PAYMENT_DUE' } }
            ]
          });
        })
        .then(inv => {
          if (!inv) {
            return Promise.reject({
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            });
          }
          invoice = inv;
          operation = inv.Operation;
          return stateMachine.back(null, operation, null, transaction);
        }).then(() => {
          return Promise.all([
            invoice.getAdminBasicInfo(),
            Transaction.createDeposit(
              request.cxc_payment, fondeoId, invoice.company_id, invoice.id, request.payment_date,
              true, transaction),
            Transaction.createDeposit(
              request.investor_payment, fondeoId, invoice.investor_company_id, invoice.id, request.payment_date,
              false, transaction)
          ]);
        })
        .then(([ plain, cxcTransaction, investorTransaction ]) => {
          response = plain;

          return userProducer.invoiceLost(invoice, cxcTransaction, investorTransaction, request.guid);
        })
        .then(() => {
          log.message('Lost operation', response, 'response', request.guid);

          return callback(null, response);
        }).catch(err => errorHandler.format(err, request.guid, callback));
    });
  }

  latePayment({ request }, callback) {
    const invoiceId = request.invoice_id;
    const fondeoId = request.company_id;

    let response = {};
    let invoice;
    let operation;

    log.message('Late payment operation', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => {
          return Invoice.findOne({
            where: {
              id: invoiceId,
              status: 'PUBLISHED'
            },
            include: [
              { model: Company, as: 'Company' },
              { model: Company, as: 'Client' },
              { model: Operation, where: { status: 'PAYMENT_DUE' } }
            ]
          });
        })
        .then(inv => {
          if (!inv) {
            return Promise.reject({
              errors: [ {
                path: 'Invoice',
                message: 'Not found'
              } ]
            });
          }

          invoice = inv;
          operation = inv.Operation;
          operation.status = 'LATE_PAYMENT';

          return operation.save({ transaction });
        })
        .then(() => {
          return Promise.all([
            invoice.getAdminBasicInfo(),
            Transaction.createDeposit(
              request.cxp_payment, invoice.client_company_id, fondeoId, invoice.id, request.cxp_payment_date,
              true, transaction),
            Transaction.createDeposit(
              request.cxc_payment, fondeoId, invoice.company_id, invoice.id, request.fondeo_payment_date,
              true, transaction),
            Transaction.createDeposit(
              request.investor_payment, fondeoId, invoice.investor_company_id, invoice.id, request.fondeo_payment_date,
              false, transaction)
          ]);
        })
        .then(([ plain, {}, cxcTransaction, investorTransaction ]) => {
          response = plain;

          return userProducer.invoiceLatePayment(invoice,
            cxcTransaction, investorTransaction, request.guid);
        });
    })
      .then(() => {
        log.message('Late payment operation', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => errorHandler.format(err, request.guid, callback));
  }

  rejectPublished({ request }, callback) {
    const invoiceId = request.invoice_id;
    const reason = request.reason;
    const userRole = request.user_role;

    let invoice = {};
    let response = {};

    log.message('Reject published invoice', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then(() => {
        if (!reason) {
          const error = {
            errors: [ {
              path: 'reason',
              message: 'reason cannot be null'
            } ]
          };

          return Promise.reject(error);
        }

        const query = {
          where: {
            id: invoiceId,
            status: 'PUBLISHED'
          },
          include: { model: Operation, where: { status: 'PENDING' } }
        };

        if (userRole === 'CXC') {
          query.where.company_id = request.company_id;
        }

        return Invoice.findOne(query);
      }).then(invoiceInstance => {
        if (!invoiceInstance) {
          const error = {
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        invoiceInstance.published = null;

        return stateMachine.back(invoiceInstance);
      }).then(([ invoiceInstance ]) => {
        invoice = invoiceInstance;

        return Operation.destroy({
          where: {
            id: invoice.Operation.id
          }
        });
      }).then(() => {
        return invoice.getCompany();
      }).then(company => {
        return invoice.getGeneralInfo(company, 'company');
      }).then(plain => {
        response = plain;

        return userProducer.publishedInvoiceRejected(invoice, reason, request.guid);
      }).then(() => {
        log.message('Reject published invoice', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  rejectFunded({ request }, callback) {
    const invoiceId = request.invoice_id;
    const reason = request.reason;

    let invoice = {};
    let response = {};
    let investorCompanyId;

    log.message('Reject fund requested invoice', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection().then(() => {
        if (!reason) {
          const error = {
            errors: [ {
              path: 'reason',
              message: 'reason cannot be null'
            } ]
          };

          return Promise.reject(error);
        }

        return PendingTransaction.rejectInvoiceFund(invoiceId, transaction);
      }).then(invoiceInstance => {
        invoice = invoiceInstance;
        investorCompanyId = invoiceInstance.investor_company_id;

        return stateMachine.back(null, invoiceInstance.Operation, invoiceInstance.Operation.InvestorTransaction,
          transaction);
      }).then(() => {
        return Invoice.removeFundRequest(invoice, transaction);
      }).then(() => {
        return invoice.getCompany();
      }).then(company => {
        return invoice.getGeneralInfo(company, 'company');
      }).then(plain => {
        response = plain;

        return userProducer.fundRequestedInvoiceRejected(invoice, reason, investorCompanyId, request.guid);
      });
    }).then(() => {
      log.message('Reject fund requested invoice', response, 'response', request.guid);

      return callback(null, response);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  approveFund({ request }, callback) {
    const invoiceId = request.invoice_id;
    const companyId = request.company_id;

    let response = {};
    let payment = {};
    let invoiceInstance = {};

    log.message('Approve fund request invoice', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection().then(() => {
        return PendingTransaction.createInvoiceFund(invoiceId, companyId, transaction);
      }).then(([ invoice, fundPayment ]) => {
        invoiceInstance = invoice;
        payment = fundPayment;

        invoice.Operation.InvestorTransaction.fund_date = new Date();

        return stateMachine.next(null, invoice.Operation, invoice.Operation.InvestorTransaction,
          transaction);
      }).then(() => {
        return invoiceInstance.getGeneralInfoAsAdmin();
      }).then(plain => {
        response = plain;

        return userProducer.fundRequestedInvoiceApproved(invoiceInstance, payment, request.guid);
      });
    }).then(() => {
      return userProducer.notifyFundApproved(invoiceInstance, request.guid)
        .then(() => {
          log.message('Approve fund request invoice', response, 'response', request.guid);

          return callback(null, response);
        });
    })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getEstimate({ request }, callback) {
    log.message('Get estimate', request, 'request', request.guid);

    const userQuery = {
      where: {
        id: request.user_id
      }
    };

    const invoiceQuery = {
      include: [
        { model: Company, as: 'Company' },
        { model: Company, as: 'Client', include: OperationCost }
      ],
      where: {
        id: request.invoice_id
      }
    };

    return Promise.all([ User.findOne(userQuery), Invoice.findOne(invoiceQuery) ])
      .then(result => {
        const user = result[0];
        const invoice = result[1];

        if (!user || !invoice || user.company_id !== invoice.company_id) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        return invoice.getEstimate();
      })
      .then(response => {
        log.message('Get estimate', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getDetail({ request }, callback) {
    log.message('Get detail', request, 'request', request.guid);

    const userQuery = {
      where: {
        id: request.user_id
      }
    };

    const invoiceQuery = {
      include: [ { all: true } ],
      where: {
        id: request.invoice_id
      }
    };

    return Promise.all([ User.findOne(userQuery), Invoice.findOne(invoiceQuery) ])
      .then(result => {
        const user = result[0];
        const invoice = result[1];

        if (!user || !invoice || user.role === 'CXP' && user.company_id !== invoice.client_company_id) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        return invoice.getDetail();
      })
      .then(response => {
        _.each(response.items, function(item, i) {
          _.each(item, function(value, j) {
            response.items[i][j] = String(value);
          });
        });
        log.message('Get detail', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getFundEstimate({ request }, callback) {
    const userId = request.user_id;
    const invoiceId = request.invoice_id;
    let invoice;

    log.message('Get fund estimate', request, 'request', request.guid);

    return Invoice.validateCxc(userId, invoiceId)
      .then(invoiceInstance => {
        invoice = invoiceInstance;

        return Promise.all([
          invoiceInstance.getFundEstimate(),
          SendengoStrategy.generateStrategyObjectByConfig()
        ]);
      }).then(([ estimates, sendengoStrategy ]) => {
        const _assign = sendengoStrategy.selectByCompanyId(invoice.client_company_id, {
          sendengo: {
            reserve: '0.00',
            expiration_payment: '0.00',
            fund_payment: invoice.total.toFixed(2),
            fund_total: invoice.total.toFixed(2)
          },
          default: {}
        });

        Object.assign(estimates, _assign);

        log.message('Get fund estimate', estimates, 'response', request.guid);

        return callback(null, estimates);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getXml({ request }, callback) {
    log.message('Get xml', request, 'request', request.guid);

    const userQuery = {
      where: {
        id: request.user_id
      }
    };

    const invoiceQuery = {
      attributes: [ 'id', 'created_at', 'client_company_id', 'cfdi' ],
      where: {
        id: request.invoice_id
      }
    };

    return Promise.all([ User.findOne(userQuery), Invoice.findOne(invoiceQuery) ])
      .then(result => {
        const user = result[0];
        const invoice = result[1];

        if (!user || !invoice || user.role === 'CXP' && user.company_id !== invoice.client_company_id) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        const response = {
          xml: invoice.cfdi
        };

        log.message('Get xml', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getMarketplace({ request }, callback) {
    log.message('Get marketplace', request, 'request', request.guid);
    let invoices = {};
    let whereInclude;
    const today = new Date();

    today.setHours(0, 0, 0, 0);
    const where = {
      status: 'PUBLISHED',
      expiration: {
        $gte: today
      }
    };

    if (request.client_name) {
      whereInclude = {
        name: {
          $iLike: `%${request.client_name}%`
        }
      };
    }
    if (request.min_total && request.max_total) {
      where.total = {
        $lte: Number(request.max_total),
        $gte: Number(request.min_total)
      };
    }
    if (request.start_date || request.end_date) {
      where.expiration = {};

      if (request.end_date) {
        const endDate = new Date(request.end_date);

        endDate.setHours(23, 59, 59, 59);
        where.expiration.$lte = endDate;
      }
      if (request.start_date) {
        const startDate = new Date(request.start_date);

        startDate.setHours(0, 0, 0, 0);
        where.expiration.$gte = startDate;
      }
    }

    return Invoice.getMarketplace(where, whereInclude)
      .then(i => {
        invoices = i.map(x => {
          return _.omit(x, [ 'created_at', 'fund_date' ]);
        });
        return Promise.all([
          Invoice.count({
            where,
            include: [
              { model: Company, as: 'Client', where: whereInclude },
              { model: Operation, where: { status: 'PENDING' } }
            ]
          }),
          Invoice.max('total', {
            where: {
              status: 'PUBLISHED',
              expiration: {
                $gte: today
              }
            },
            include: [
              { attributes: [], model: Operation, where: { status: 'PENDING' } }
            ]
          })
        ]);
      })
      .then(results => {
        const [ count, maxTotal ] = results;
        const response = {
          invoices,
          total_invoices: count,
          // total_pages: Math.ceil(count / request.limit),
          max_total: maxTotal ? maxTotal.toString() : '0'
        };

        log.message('Get marketplace', response, 'response', request.guid);

        return callback(null, response );
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvestorFundEstimate({ request }, callback) {
    log.message('Get investor fund estimate', request, 'request', request.guid);
    const userQuery = {
      where: {
        id: request.user_id
      },
      include: [
        { model: Company, include: OperationCost }
      ]
    };

    const invoiceQuery = {
      where: {
        id: request.invoice_id,
        $or: {
          status: 'PUBLISHED',
          investor_company_id: request.company_id
        }
      },
      include: [
        { model: Operation, include: InvestorTransaction }
      ]
    };

    return Promise.all([ User.findOne(userQuery), Invoice.findOne(invoiceQuery) ])
      .then(result => {
        const user = result[0];
        const invoice = result[1];

        if (!user || !invoice) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        if (!invoice.Operation.InvestorTransaction) {
          return invoice.getInvestorFundEstimate(user.Company);
        }
        return invoice.getInvestorFundEstimateFromTransaction();
      })
      .then(response => {
        log.message('Get investor fund estimate', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvestorProfitEstimate({ request }, callback) {
    log.message('Get investor profit estimate', request, 'request', request.guid);

    const invoiceQuery = {
      where: {
        id: request.invoice_id,
        $or: {
          status: 'PUBLISHED',
          investor_company_id: request.company_id
        }
      },
      include: [
        { model: Operation, include: InvestorTransaction }
      ]
    };

    return Invoice.findOne(invoiceQuery)
      .then(invoice => {
        if (!invoice) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        if (!invoice.Operation.InvestorTransaction) {
          return invoice.getInvestorProfitEstimate();
        }
        return invoice.getInvestorProfitEstimateFromTransaction();
      })
      .then(response => {
        log.message('Get investor profit estimate', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvestorInvoices({ request }, callback) {
    let invoices = {};
    let where = {};

    request.limit = request.limit || 25;

    if (request.offset > 0) {
      request.offset = request.limit * (request.offset - 1);
    }

    log.message('Get investor invoices', request, 'request', request.guid);

    return User.findOne({
      where: {
        id: request.user_id
      }
    })
      .then(user => {
        where = {
          investor_company_id: user.company_id
        };

        return Invoice.getInvoices(user, where, request);
      })
      .then(i => {
        invoices = i;

        return Invoice.count({
          where
        });
      })
      .then(count => {
        const response = {
          invoices,
          total_invoices: count,
          total_pages: Math.ceil(count / request.limit)
        };

        log.message('Get investor invoices', response, 'response', request.guid);

        return callback(null, response );
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvestorFundDetail({ request }, callback) {
    log.message('Get investor fund detail', request, 'request', request.guid);

    const invoiceQuery = {
      where: {
        id: request.invoice_id,
        investor_company_id: request.company_id
      },
      include: [
        { model: Operation, include: InvestorTransaction }
      ]
    };

    return Invoice.findOne(invoiceQuery)
      .then(invoice => {
        if (!invoice) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        return invoice.getInvestorFundDetail();
      })
      .then(response => {
        log.message('Get investor fund detail', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvoicePaymentSummary({ request }, callback) {
    log.message('Get invoice payment summary', request, 'request', request.guid);
    let invoiceInstance;

    const invoiceQuery = {
      where: {
        id: request.invoice_id,
        company_id: request.company_id,
        investor_company_id: {
          $ne: null
        }
      },
      include: [
        { model: Operation, include: InvestorTransaction }
      ]
    };

    return Invoice.findOne(invoiceQuery)
      .then(invoice => {
        invoiceInstance = invoice;

        if (!invoice) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        return Promise.all([
          invoice.getInvoicePaymentSummary(),
          invoice.getInvestorFundDetail(),
          invoice.getInvestorProfitEstimateFromTransaction(),
          SendengoStrategy.generateStrategyObjectByConfig()
        ]);
      })
      .then(([ paymentSummary, fundDetail, profit, sendengoStrategy ]) => {
        const _assign = sendengoStrategy.selectByCompanyId(invoiceInstance.client_company_id, {
          sendengo: {
            expiration_payment: '0.00',
            reserve: '0.00',
            fund_total: paymentSummary.total
          },
          default: {}
        });

        Object.assign(paymentSummary, _assign);

        const response = {
          payment_summary: paymentSummary,
          financial_summary: _.merge(fundDetail, profit)
        };

        log.message('Get invoice payment summary', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getAdminInvoices({ request }, callback) {
    log.message('Get admin invoices', request, 'request', request.guid);

    request.limit = request.limit || 25;

    if (request.offset > 0) {
      request.offset = request.limit * (request.offset - 1);
    }

    request.status = request.status && request.status.length > 0 ? request.status : null;

    return Invoice.getAdminInvoices(request)
      .then(({ invoices, count }) => {
        const response = {
          invoices,
          total_invoices: count,
          total_pages: Math.ceil(count / request.limit)
        };

        log.message('Get admin invoices', response, 'response', request.guid);

        return callback(null, response );
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvoiceDetailAsAdmin({ request }, callback) {
    log.message('Get invoice detail as admin', request, 'request', request.guid);

    let invoice;

    const invoiceQuery = {
      where: {
        id: request.invoice_id
      },
      include: [
        { model: Company, as: 'Company', include: OperationCost },
        { model: Company, as: 'Client', include: OperationCost },
        { model: Company, as: 'Investor', include: OperationCost },
        { model: Operation, include: InvestorTransaction }
      ]
    };
    const response = {};

    return Invoice.findOne(invoiceQuery)
      .then(invoiceInstance => {
        invoice = invoiceInstance;

        if (!invoice) {
          return Promise.reject({
            errors: [ {
              path: 'Invoice',
              message: 'Not found'
            } ]
          });
        }

        const promises = [ invoice.getAdminBasicInfo() ];
        const includeOperationCosts =
        invoice.fund_date &&
        invoice.expiration;

        if (includeOperationCosts) {
          promises.push(
            invoice.getAdminOperationSummary(),
            invoice.getAdminCxcPayment(),
            invoice.getAdminInvestorPayment()
          );
        }

        return Promise.all(promises);
      })
      .then(([ detail, operationSummary, cxcPayment, investorPayment ]) => {
        response.invoice_detail = detail || {};
        response.operation_summary = operationSummary || {};
        response.cxc_payment = cxcPayment || {};
        response.investor_payment = investorPayment || {};

        log.message('Get invoice detail as admin', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getInvoicesAvailability({ request }, callback) {
    log.message('Get invoices availability', request, 'request', request.guid);

    const invoices = _.map(request.invoices, value => {
      return value.id;
    });

    const query = {
      attributes: [
        'id',
        [ sequelize.literal(`CASE WHEN "Operation"."status" IS NOT NULL
           AND "Operation"."status" LIKE '%PENDING%' THEN true ELSE false END`), 'is_available' ]
      ],
      where: {
        id: {
          $in: invoices
        },
        status: 'PUBLISHED'
      },
      include: [
        { attributes: [], model: Operation, required: true }
      ]
    };

    return Invoice.findAll(query)
      .then(invoiceInstances => {
        return invoiceInstances.map(x => {
          return _.pick(x.dataValues, [ 'id', 'is_available' ]);
        });
      })
      .then(response => {
        log.message('Get invoices availability', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }


  _buildCallbackData(controller, _request, _pendingTransaction) {
    const callbackData = {
      callback: null,
      completed: false
    };

    const callback = function() {
      return new Promise( (resolve) => {
        controller({ request: _request }, () => Promise.resolve(true))
          .then(() => {
            this.processed = true;
            this.pending_transaction = { id: _pendingTransaction.id };
            resolve(true);
          })
          .catch(() => {
            this.processed = false;
            this.pending_transaction = { id: _pendingTransaction.id };
            resolve(true);
          });
      });
    };

    callbackData.callback = callback.bind(callbackData);

    return callbackData;
  }

  _processCallbackResults(_callbacks) {
    const success = [];
    const fail = [];

    _callbacks.forEach( result => {
      if (result.processed) {
        success.push(result.pending_transaction);
      } else {
        fail.push(result.pending_transaction);
      }
    });

    return {
      success,
      fail
    };
  }

  bulkApprove({ request }, callback) {
    const { guid, pending_transactions, bank_report_id } = request;

    log.message('Bulk approve', request, 'request', guid);

    const ids = _.map(pending_transactions, (element) => element.id);

    const queryParams = PendingTransaction.buildBankReportQuery({
      bank_report_id,
      skip_where_created_at: true,
      by_id: { $in: ids }
    });
    let callbacks;


    return PendingTransaction.findAll(queryParams)
      .then( pendingTransactions => {
        if ( pendingTransactions.length === 0) {
          const error = {
            errors: [ {
              path: 'PendingTransaction',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        const promises = [];

        pendingTransactions.forEach( pendingTransaction => {
          let promise;


          if ( pendingTransaction.isForInvestorInvoiceFund() ) {
            const _request = {
              invoice_id: pendingTransaction.data.invoice_id,
              company_id: pendingTransaction.data.company_id
            };

            const callbackData = this._buildCallbackData(this.approveFund, _request, pendingTransaction);

            promise = Promise.resolve(callbackData);
            promises.push(promise);
          } else {
            const _request = {
              cxp_payment: pendingTransaction.amount,
              cxp_payment_date: pendingTransaction.created_at,
              cxc_payment: 0,
              investor_payment: 0,
              fondeo_payment_date: new Date().toDateString()
            };

            promise = Invoice.findOne( {
              where: {
                id: pendingTransaction.data.invoice_id
              },
              include: [
                { model: Company, as: 'Company', include: OperationCost },
                { model: Company, as: 'Client', include: OperationCost },
                { model: Company, as: 'Investor', include: OperationCost },
                { model: Operation, include: InvestorTransaction }
              ]
            }).then( invoice => {
              return Promise.all([
                invoice.getAdminCxcPayment(),
                invoice.getAdminInvestorPayment()
              ]);
            }).then( ([ cxcPaymentInfo, investorPayment ]) => {
              _request.cxp_payment = cxcPaymentInfo.reserve;
              _request.investor_payment = investorPayment.total_payment;
              _request.invoice_id = pendingTransaction.data.invoice_id;
              _request.company_id = pendingTransaction.company_id;

              const callbackData = this._buildCallbackData(this.completed, _request, pendingTransaction);

              return Promise.resolve(callbackData);
            });

            promises.push(promise);
          }
        });

        return Promise.all(promises);
      })
      .then( functionCallbacks => {
        callbacks = functionCallbacks;
        const promises = [];

        promises.push( functionCallbacks[0].callback() );

        for (let i = 1; i < functionCallbacks.length; i = i + 1) {
          const callbackData = functionCallbacks[i];
          const previousPromise = promises[i - 1];

          const promise = previousPromise.then( () => callbackData.callback() );

          promises.push(promise);
        }

        return Promise.all(promises);
      })
      .then( () => {
        const response = this._processCallbackResults(callbacks);

        log.message('Bulk approve', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        if (callbacks) {
          const response = this._processCallbackResults(callbacks);

          log.message('Bulk approve', response, 'response', request.guid);

          return callback(null, response);
        }

        return errorHandler.format(err, request.guid, callback);
      });
  }
}

module.exports = new InvoiceController();
