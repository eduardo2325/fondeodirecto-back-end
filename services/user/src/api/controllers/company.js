const _ = require('lodash');
const { Agreement, Company, Token, Operation, InvestorTransaction, Invoice, OperationCost, User,
  Session, sequelize } = require('../../models');
const errorHandler = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);
const userProducer = require('../producers/user');
const companyColors = require('../../vendor/colors');
const statementHelper = require('../helpers/statement-queries');
const moment = require('/var/lib/core/js/moment');

function fixNums(value) {
  Object.keys(value).forEach(k => {
    if (typeof value[k] === 'number' && (k !== 'id' && k !== 'company_id')) {
      value[k] = value[k].toFixed(2);
    }
  });
}

class CompanyController {
  create({ request }, callback) {
    log.message('Create company', request, 'request', request.guid);

    if (request.role === 'INVESTOR' && !_.includes([ 'moral', 'physical' ], request.taxpayer_type)) {
      const error = {
        errors: [ {
          path: 'taxpayer_type',
          message: 'taxpayer_type cannot be null'
        } ]
      };

      return errorHandler.format(error, request.guid, callback);
    }

    let response = {};

    return sequelize.transaction((transaction) => {
      return Company.create(request, { transaction })
        .then(companyObj => {
          response = companyObj;

          return userProducer.ensureConnection();
        }).then(() => {
          const { fd_commission, reserve, annual_cost, fee, variable_fee_percentage,
            fideicomiso_fee, fideicomiso_variable_fee } = request.operation_cost;
          const operationCostValues = {
            company_id: response.id,
            fd_commission: fd_commission ? parseFloat(fd_commission).toFixed(2) : undefined,
            reserve: reserve ? parseFloat(reserve).toFixed(2) : undefined,
            annual_cost: annual_cost ? parseFloat(annual_cost).toFixed(2) : undefined,
            fee: fee ? parseFloat(fee).toFixed(2) : undefined,
            variable_fee_percentage: variable_fee_percentage ?
              parseFloat(variable_fee_percentage).toFixed(2) : undefined,
            fideicomiso_fee: fideicomiso_fee ? parseFloat(fideicomiso_fee).toFixed(2) : undefined,
            fideicomiso_variable_fee: fideicomiso_variable_fee ?
              parseFloat(fideicomiso_variable_fee).toFixed(2) : undefined
          };


          return OperationCost.create(operationCostValues, { transaction });
        }).then(() => {
        // ensure we skip those props, they're not always needed
          return response.getInformation();
        }).then(plainCompany => {
          log.message('Create company', plainCompany, 'response', request.guid);

          return callback(null, plainCompany);
        }).catch(err => {
          transaction.rollback();

          return errorHandler.format(err, request.guid, callback);
        });
    });
  }

  exists({ request }, callback) {
    log.message('Company Exists', request, 'request', request.guid);

    const query = {
      where: {
        rfc: request.rfc
      }
    };

    return Company.findOne(query)
      .then(companyObj => {
        const response = {
          exists: Boolean(companyObj)
        };

        log.message('Company Exists', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  update({ request }, callback) {
    log.message('Update company', request, 'request', request.guid);

    let companyValues = _.clone(request);

    delete companyValues.id;

    companyValues = _.omitBy(companyValues, _.isEmpty);
    companyValues.taxpayer_type = companyValues.taxpayer_type || '';

    return Company.findOne({
      where: {
        id: request.id
      }
    })
      .then(companyObj => {
        if (!companyObj) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        return companyObj.update(companyValues, {
          returning: true
        });
      })
      .then(companyObj => {
        const response = _.omit(companyObj.get(), [ 'created_at', 'updated_at', 'agreed_at', 'agreement_url',
          'isFideicomiso' ]);

        response.balance = response.balance ? response.balance.toFixed(2) : '0.00';
        response.bank = response.bank ? response.bank : '';
        response.bank_account = response.bank_account ? response.bank_account : '';
        response.holder = response.holder ? response.holder : '';
        response.clabe = response.clabe ? response.clabe : '';

        log.message('Update company', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  updateOperationCost({ request }, callback) {
    log.message('Update operation cost', request, 'request', request.guid);

    const query = {
      where: {
        company_id: request.id
      },
      include: [ {
        model: Company,
        where: {
          role: request.role
        },
        required: true
      } ]
    };

    return OperationCost.findOne(query)
      .then(operationCost => {
        if (!operationCost) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        const values = [ 'annual_cost', 'reserve', 'fd_commission', 'fee', 'variable_fee_percentage',
          'fideicomiso_fee', 'fideicomiso_variable_fee' ];
        const updateValues = {};

        // ensure only passed values are really used, in this case protobuf is passing empty strings...
        values.forEach(key => {
          if (request[key]) {
            updateValues[key] = parseFloat(request[key]).toFixed(2);
          }
        });

        return operationCost.update(updateValues, {
          returning: true
        });
      })
      .then(operationCost => {
        operationCost.variable_fee_percentage = operationCost.variable_fee_percentage ?
          operationCost.variable_fee_percentage : 0;

        operationCost.fideicomiso_fee = operationCost.fideicomiso_fee ?
          operationCost.fideicomiso_fee : 0;

        operationCost.fideicomiso_variable_fee = operationCost.fideicomiso_variable_fee ?
          operationCost.fideicomiso_variable_fee : 0;

        const response = _.omit(operationCost.get({ plain: true }), [ 'created_at', 'updated_at', 'Company' ]);

        fixNums(response);

        log.message('Update operation cost', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getFullInformation({ request }, callback) {
    log.message('Get company full information', request, 'request', request.guid);

    const query = {
      where: {
        id: request.id
      }
    };

    return Company.findOne(query)
      .then(companyObj => {
        if (!companyObj) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        return companyObj.getInformation();
      }).then(plainCompany => {
        log.message('Get company full information', plainCompany, 'response', request.guid);

        return callback(null, plainCompany);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  analyzeClabe({ request }, callback) {
    const query = {
      where: {
        clabe: request.clabe
      }
    };

    log.message('Analyze Clabe', request, 'request', request.guid);

    return Promise.resolve()
      .then(() => {
        if (!request.exists) {
          return Promise.resolve();
        }

        return Company.findOne(query);
      })
      .then(company => {
        if (company) {
          const error = {
            errors: [ {
              path: 'clabe',
              message: 'clabe must be unique',
              data: {
                company_name: company.name
              }
            } ]
          };

          return Promise.reject(error);
        }

        return Company.analyzeClabe(request.clabe);
      }).then(bankInfo => {
        log.message('Analyze Clabe', bankInfo, 'response', request.guid);

        return callback(null, bankInfo);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getCompanies({ request }, callback) {
    log.message('Get Companies', request, 'request', request.guid);
    let companies = {};

    request.limit = request.limit || 25;

    if (request.offset > 0) {
      request.offset = request.limit * (request.offset - 1);
    }

    return Company.getCompanies(request.limit, request.offset, request.order_by, request.order_desc, request.role)
      .then(c => {
        companies = c;

        return Company.count({
          where: {
            role: request.role
          }
        });
      })
      .then(count => {
        const response = {
          companies,
          total_companies: count,
          total_pages: Math.ceil(count / request.limit)
        };

        log.message('Get Companies', response, 'response', request.guid);

        return callback(null, response );
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getUsers({ request }, callback) {
    const order_by = request.order_by || 'name';
    const order_desc = request.order_desc ? 'DESC' : 'ASC';

    log.message('Get company users', request, 'request', request.guid);

    return Company.getUsers(request.id, order_by, order_desc)
      .then(users => {
        const response = { users };

        log.message('Get company users', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getOperationCost({ request }, callback) {
    log.message('Get operation cost', request, 'request', request.guid);

    const query = {
      attributes: [ 'annual_cost', 'reserve', 'fd_commission', 'fee', 'variable_fee_percentage' ],
      where: {
        company_id: request.id
      },
      include: [ {
        model: Company,
        where: {
          role: request.role
        },
        required: true
      } ],
      raw: true
    };

    return OperationCost.findOne(query)
      .then(operationCost => {
        if (!operationCost) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        operationCost.variable_fee_percentage = operationCost.variable_fee_percentage ?
          operationCost.variable_fee_percentage : 0;

        log.message('Get operation cost', operationCost, 'response', request.guid);

        const _operationCost = _.pick(operationCost, [ 'annual_cost', 'reserve', 'fd_commission', 'fee',
          'variable_fee_percentage' ]);

        fixNums(_operationCost);

        return callback(null, _operationCost);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  registerInvestor({ request }, callback) {
    log.message('Register new investor by invitation', request, 'request', request.guid);
    let tokenInstance;
    let companyInstance;
    let userInstance;
    let response = {};
    let foundAgreement;

    if (!_.includes([ 'moral', 'physical' ], request.taxpayer_type)) {
      const error = {
        errors: [ {
          path: 'taxpayer_type',
          message: 'taxpayer_type cannot be null'
        } ]
      };

      return errorHandler.format(error, request.guid, callback);
    }

    return sequelize.transaction((transaction) => {
      return userProducer.ensureConnection().then( () => {
        return Token.findOne({ where: { token: request.token, type: 'investor-invitation' } })
          .then( token => {
            if ( !token ) {
              const error = {
                errors: [ {
                  path: 'company',
                  message: 'token not found'
                } ]
              };

              return Promise.reject(error);
            }

            tokenInstance = token;

            const newCompanyData = {
              rfc: request.rfc,
              name: token.data.company_name,
              business_name: request.company_business_name,
              role: 'INVESTOR',
              taxpayer_type: request.taxpayer_type,
              suspended_roles: [],
              description: ''
            };

            if ( request.taxpayer_type === 'physical') {
              newCompanyData.business_name = request.name;
            }

            return Company.create(newCompanyData, { transaction });
          }).then( company => {
            return company.get({ plain: true });
          }).then( companyObj => {
            companyInstance = companyObj;
            const { fd_commission, reserve, annual_cost, fee, variable_fee_percentage } = tokenInstance.data;

            const operationCostValues = {
              company_id: companyObj.id,
              fd_commission: fd_commission ? parseFloat(fd_commission).toFixed(2) : undefined,
              reserve: reserve ? parseFloat(reserve).toFixed(2) : undefined,
              annual_cost: annual_cost ? parseFloat(annual_cost).toFixed(2) : undefined,
              fee: fee ? parseFloat(fee).toFixed(2) : undefined,
              variable_fee_percentage: variable_fee_percentage ?
                parseFloat(variable_fee_percentage).toFixed(2) : undefined
            };

            return OperationCost.create(operationCostValues, { transaction });
          }).then( () => {
            if (tokenInstance.email !== request.email) {
              const error = {
                errors: [ {
                  path: 'invitation',
                  message: 'Email not allowed'
                } ]
              };

              return Promise.reject(error);
            }

            const userData = {
              name: request.name,
              password: request.password,
              email: request.email,
              company_id: companyInstance.id,
              role: 'INVESTOR',
              age: request.age,
              phone: request.phone,
              address: request.address
            };

            return User.create(userData, { transaction });
          }).then( userObj => {
            userInstance = userObj;

            response = {
              name: userObj.name,
              email: userObj.email,
              status: userObj.status,
              role: userObj.role,
              color: userObj.color,
              company_id: companyInstance.id
            };

            return tokenInstance.destroy({ transaction });
          })
          .then( () => {
            foundAgreement = {};
            foundAgreement.company_id = companyInstance.id;
            foundAgreement.user_role = 'INVESTOR';

            const template = `INVESTOR_${request.taxpayer_type}`;

            const params = {
              template: template.toLowerCase(),
              locals: {
                FACTORAJE_FIRMANTE_EMPRESA: companyInstance.business_name,
                FACTORAJE_FIRMANTE_NOMBRE: request.name,
                PERSONA_MORAL_RAZON_SOCIAL: companyInstance.business_name,
                PERSONA_FISICA_NOMBRE: request.name
              }
            };

            const promise = Agreement.previewContract(foundAgreement, params, request.guid);

            return promise;
          }).then((s3Url) => {
            foundAgreement.agreement_url = s3Url;
            return Agreement.finalizeContract(foundAgreement, request.guid);
          }).then((s3Url) => {
            const params = {
              file: s3Url,
              role: 'INVESTOR',
              id: companyInstance.id,
              agreed_at: foundAgreement.agreed_at.toString(),
              recipients: [ [ userInstance.name, userInstance.email ] ]
            };

            return userProducer.agreementAccepted(params, request.guid);
          }).then(() => {
            return Agreement.create(foundAgreement, { transaction });
          });
      }).then(() => {
        log.message('Create investor', response, 'response', request.guid);
        return callback(null, response);
      }).catch(err => {
        transaction.rollback();
        return errorHandler.format(err, request.guid, callback);
      });
    });
  }

  addUser({ request }, callback) {
    const user = request.user;
    const query = {
      where: {
        id: request.company_id
      }
    };
    const response = {
      name: user.name,
      email: user.email,
      status: 'pending',
      role: user.type,
      color: _.sample(companyColors)
    };
    let companyRole = '';
    let companyInstance;

    user.companyId = request.company_id;

    log.message('Add user to company', request, 'request', request.guid);

    return Company.findOne(query)
      .then(companyObj => {
        if (!companyObj) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        companyInstance = companyObj;
        companyRole = companyObj.role;

        return userProducer.ensureConnection();
      }).then(() => {
        return Token.userRegistration(user, companyRole);
      }).then(token => {
        return userProducer.invitationCreated(token, companyInstance, request.guid);
      }).then(() => {
        log.message('Add user to company', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getFactorajeAndCashSummaries({ request }, callback) {
    log.message('Get investor summaries', request, 'request', request.guid);

    const investorCompanyId = request.company_id;
    const yearRequest = request.year;
    const monthRequest = request.month;

    const nowMoment = moment();
    const requestedMoment = moment([ yearRequest, monthRequest ]);
    const difference = nowMoment.diff(requestedMoment, 'months');

    if ( !( difference > 0 && difference <= 3 )) {
      const error = {
        errors: [ {
          path: 'company',
          message: 'Not found'
        } ]
      };

      return errorHandler.format(error, request.guid, callback);
    }

    const invoiceFindParams = {
      include: [
        {
          model: Operation,
          required: true,
          include: [ {
            model: InvestorTransaction,
            required: true
          } ]
        }
      ],
      where: {
        $and: [
          [ `("Operation.InvestorTransaction".fund_date IS NOT NULL
              AND "Operation.InvestorTransaction".fund_date
                < '${requestedMoment.format('YYYY-MM')}-01 00:00:00'::timestamp)` ],
          { investor_company_id: investorCompanyId },
          [ `( ("Operation".status != 'COMPLETED') OR
              "Operation".updated_at >= '${requestedMoment.format('YYYY-MM')}-01 00:00:00'::timestamp)` ]
        ]
      }
    };

    let companyInstance;

    return Company.findOne({
      where: {
        id: investorCompanyId
      }
    }).then( company => {
      companyInstance = company;

      const promises = [
        Invoice.findAll(invoiceFindParams),
        statementHelper.purchasedInvoices(investorCompanyId, yearRequest, monthRequest),
        statementHelper.payedInvoices(investorCompanyId, yearRequest, monthRequest)
      ];
      const cashflowRequestedMonthPromise = companyInstance.getCashFlowFromRange(requestedMoment, requestedMoment );
      const nextMonthMoment = requestedMoment.clone().add(1, 'months');
      const upperBound = nextMonthMoment.clone().add(difference - 1, 'months');
      const cashflowAfterRequestedMonthPromise = companyInstance.getCashFlowFromRange( nextMonthMoment, upperBound);

      return Promise.all([ Promise.all(promises), cashflowRequestedMonthPromise, cashflowAfterRequestedMonthPromise ]);
    })
      .then( ([ results, cashflowRequestedMonth, cashflowNextMonths ]) => {
        const invoices = results[0];
        const purchasedInvoices = results[1];
        const payedInvoices = results[2];

        const total = _.reduce(invoices, (sum, invoice) => {
          return invoice.total + sum;
        }, 0);

        const totalPurchased = _.reduce(purchasedInvoices, (sum, element) => {
          return sum + element.factoraje;
        }, 0);

        const totalPayed = _.reduce(payedInvoices, (sum, element) => {
          return sum + element.total;
        }, 0);

        const final_ammount = total - totalPayed + totalPurchased;

        const totalDeposits = cashflowRequestedMonth.deposits + cashflowNextMonths.deposits;
        const totalWithdrawals = cashflowRequestedMonth.withdrawals + cashflowNextMonths.withdrawals;

        const initialAmmountCash = companyInstance.balance - totalDeposits + totalWithdrawals;
        const totalDepositsMonth = cashflowRequestedMonth.deposits;
        const totalWithDrawalsMonth = cashflowRequestedMonth.withdrawals;
        const finalAmmountCash = initialAmmountCash + totalDepositsMonth - totalWithDrawalsMonth;

        const response = {
          factoraje: {
            initial_ammount: total.toFixed(2),
            final_ammount: final_ammount.toFixed(2),
            payments: totalPayed.toFixed(2),
            acquisitions: totalPurchased.toFixed(2)
          },
          cash: {
            initial_ammount: initialAmmountCash.toFixed(2),
            final_ammount: finalAmmountCash.toFixed(2),
            increments: totalDepositsMonth.toFixed(2),
            decrements: totalWithDrawalsMonth.toFixed(2)
          }
        };

        log.message('Get investor summaries', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  createInvestorInvitation({ request }, callback) {
    let invitation;
    let response;

    log.message('Create investor invitation', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then( () => {
        return User.findOne( { where: { email: request.email } });
      })
      .then( user => {
        if ( user ) {
          return Promise.reject({
            path: 'company',
            error: 'Already registered'
          });
        }

        return Token.findOne( { where: { email: request.email, type: 'investor-invitation' }, paranoid: true });
      })
      .then( token => {
        if ( token ) {
          return token.destroy();
        }

        return Promise.resolve(true);
      })
      .then( () => {
        const data = {
          email: request.email,
          data: {
            user_name: request.name,
            company_name: request.company_name,
            user_type: 'investor',
            fee: request.fee,
            variable_fee_percentage: request.variable_fee_percentage
          },
          issuer: request.issuer,
          type: 'investor-invitation'
        };

        return Token.create(data);
      })
      .then( token => {
        return token.get();
      })
      .then( token => {
        response = {
          id: token.id,
          created_at: token.created_at.toString()
        };
        invitation = {
          name: request.name,
          email: request.email,
          token: token.token,
          id: token.id
        };

        return userProducer.investorInvitationCreated(invitation, request.guid);
      })
      .then( () => {
        log.message('Create investor invitation', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  getBalance({ request }, callback) {
    const { id, user_id, guid } = request;
    const query = {
      where: {
        id: user_id
      },
      include: [ {
        model: Company,
        required: false,
        on: {
          id
        }
      } ]
    };

    log.message('Get company balance', request, 'request', guid);

    return User.findOne(query)
      .then(user => {
        if (!user.Company || user.role === 'INVESTOR' && user.company_id !== id) {
          const error = {
            errors: [ {
              path: 'company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        return user.Company.getBalance();
      }).then(balance => {
        const response = {
          total: balance
        };

        log.message('Get company balance', response, 'response', guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, guid, callback);
      });
  }

  updateCompanyRoleSuspension({ request }, callback) {
    const { id, role, suspended, guid } = request;

    log.message('Update company role suspension', request, 'request', guid);

    return sequelize.transaction((transaction) => {
      return userProducer.ensureConnection()
        .then(() => Company.findOne({ where: { id } }))
        .then(company => {
          if (!company) {
            const error = {
              errors: [ {
                path: 'Company',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          return Promise.all([
            company.updateRoleSuspension(role, suspended, transaction),
            Session.updateSuspensions(id, role, suspended, transaction)
          ]);
        })
        .then(() => userProducer.companyRoleSuspensionUpdated(id, role, suspended, guid));
    })
      .then(() => {
        log.message('Update company role suspension', request, 'response', guid);

        return callback(null, request);
      })
      .catch(err => {
        return errorHandler.format(err, guid, callback);
      });
  }

  updateInvestorRoleSuspension({ request }, callback) {
    const { id, suspended, guid } = request;
    const role = 'INVESTOR';

    log.message('Update investor role suspension', request, 'request', guid);

    return sequelize.transaction((transaction) => {
      return userProducer.ensureConnection()
        .then(() => Company.findOne({ where: { id, role } }))
        .then(company => {
          if (!company) {
            const error = {
              errors: [ {
                path: 'Investor',
                message: 'Not found'
              } ]
            };

            return Promise.reject(error);
          }

          return Promise.all([
            company.updateRoleSuspension(role, suspended, transaction),
            Session.updateSuspensions(id, role, suspended, transaction)
          ]);
        })
        .then(() => userProducer.investorRoleSuspensionUpdated(id, suspended, guid));
    })
      .then(() => {
        log.message('Update investor role suspension', request, 'response', guid);

        return callback(null, request);
      })
      .catch(err => {
        return errorHandler.format(err, guid, callback);
      });
  }

  propose({ request }, callback) {
    log.message('Propose a company', request, 'request', request.guid);

    return userProducer.ensureConnection()
      .then(() => Company.findOne({ where: { id: request.company_id } }))
      .then(company => userProducer.companyProposed(request, company.name, request.guid))
      .then(() => {
        const response = {
          success: true
        };

        log.message('Propose a company', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => errorHandler.format(err, request.guid, callback));
  }

  agreed({ request }, callback) {
    log.message('Accepted agreement', request, 'request', request.guid);

    return sequelize.transaction(transaction => {
      return userProducer.ensureConnection()
        .then(() => {
          return Agreement.findContract(request.id, request.role, { transaction })
            .then(foundAgreement => {
              return Agreement.finalizeContract(foundAgreement, request.guid)
                .then(s3Url => {
                  const params = {
                    file: s3Url,
                    role: request.role,
                    id: request.id,
                    agreed_at: foundAgreement.agreed_at.toString()
                  };

                  return userProducer.agreementAccepted(params, request.guid)
                    .then(() => foundAgreement.save({ transaction }))
                    .then(() => {
                      const data = {
                        company_agreed: true
                      };

                      const opts = {
                        where: {
                          company_id: request.id,
                          user_id: request.user
                        },
                        transaction
                      };

                      return Session.update(data, opts);
                    });
                })
                .then(() => foundAgreement);
            });
        })
        .then(result => {
          const response = {
            agreed_at: result.agreed_at.toString(),
            agreement_url: result.agreement_url || undefined
          };

          log.message('Accepted agreement', response, 'response', request.guid);

          return callback(null, response);
        })
        .catch(err => {
          transaction.rollback();

          return errorHandler.format(err, request.guid, callback);
        });
    });
  }

  getInvestorTransactions({ request }, callback) {
    const { id, year, month, guid } = request;

    log.message('Get an investor\'s transactions', request, 'request', request.guid);

    const purchasedInvoices = statementHelper.purchasedInvoices(id, year, month);
    const investorCashWithDrawals = statementHelper.investorCashWithDrawals(id, year, month);
    const investorCashDeposits = statementHelper.investorCashDeposits(id, year, month);
    const payedInvoices = statementHelper.payedInvoices(id, year, month);

    return Promise.all([
      purchasedInvoices,
      investorCashWithDrawals,
      investorCashDeposits,
      payedInvoices
    ])
      .then(result => {
        return statementHelper.formStatementResponse(result);
      })
      .then(response => {
        log.message('Get an investor\'s transactions', response, 'response', guid);

        return callback(null, response);
      })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }

  updateCompanyPrepayment({ request }, callback) {
    log.message('Update company prepayment', request, 'request', request.guid);
    const response = {};
    let company;

    const query = {
      where: {
        id: request.id,
        role: 'COMPANY'
      }
    };

    return Company.findOne(query)
      .then(companyInstance => {
        if (!companyInstance) {
          const error = {
            errors: [ {
              path: 'Company',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        company = companyInstance;

        const updateValues = {
          prepayment: request.prepayment
        };

        return company.update(updateValues, {
          returning: true
        });
      })
      .then(() => {
        response.success = true;
        log.message('Update company prepayment', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }
}

module.exports = new CompanyController();
