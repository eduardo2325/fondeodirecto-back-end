const { Token, User, Company, Invoice, BankReport, PendingTransaction } = require('../../models');
const errorHandler = require('../helpers/error');
const log = new (require('/var/lib/core/js/log'))(module);
const roles = require('../../config/user-roles');
const userProducer = require('../producers/user');
const _ = require('lodash');
const moment = require('/var/lib/core/js/moment');

class UserController {
  getInvitation({ request }, callback) {
    log.message('Get invitation', request, 'request', request.guid);

    const query = {
      where: {
        token: request.token,
        type: {
          $in: [ 'registration', 'investor-invitation' ]
        }
      }
    };

    return Token.findOne(query)
      .then(token => {
        if (!token) {
          const error = {
            errors: [ {
              path: 'invitation',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        return token.getInvitationInfo();
      })
      .then(response => {
        log.message('Get invitation', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  registration({ request }, callback) {
    let user;

    log.message('Registration', request, 'request', request.guid);

    return Token.findOne({
      where: {
        token: request.token
      }
    })
      .then(token => {
        if (!token) {
          return Promise.reject({
            errors: [ {
              path: 'Token',
              message: 'Not found'
            } ]
          });
        }

        const userData = {
          name: request.name,
          password: request.password,
          email: token.email,
          company_id: token.company_id,
          role: token.data.user_type
        };

        return User.create(userData);
      })
      .then(u => {
        user = u;
        return Token.destroy({
          where: {
            token: request.token
          }
        });
      })
      .then(() => {
        return user.getInformation();
      })
      .then(response => {
        log.message('Registration', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  getRoles({ request }, callback) {
    log.message('Get user roles', request, 'request', request.guid);

    return Promise.resolve()
      .then(() => {
        const response = {
          roles: roles.publicRoles
        };

        log.message('Get user roles', response, 'response', request.guid);

        callback(null, response);
      });
  }

  delete({ request }, callback) {
    let instance;
    let response;

    log.message('Delete user', request, 'request', request.guid);

    return User.findOne({
      where: {
        email: request.email
      }
    })
      .then(user => {
        if (user) {
          instance = user;

          return user.getInformation();
        }

        return null;
      })
      .then(user => {
        if (!user) {
          return Token.findOne({
            where: {
              email: request.email,
              type: 'registration'
            }
          })
            .then(token => {
              if (token) {
                instance = token;

                return token.getInvitationInfo();
              }

              return null;
            });
        }

        return user;
      })
      .then(info => {
        if (!instance) {
          return Promise.reject({
            errors: [ {
              path: 'user',
              message: 'Not found'
            } ]
          });
        }

        response = info;

        return Company.getUsers(instance.company_id, 'name', 'ASC');
      })
      .then(users => {
        if (users.length === 1) {
          return Promise.reject({
            errors: [ {
              path: 'user',
              message: 'Unable to delete the last user'
            } ]
          });
        }

        return instance.destroy();
      })
      .then(() => {
        log.message('Delete user', response, 'response', request.guid);

        return callback(null, response);
      })
      .catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  checkEmail({ request }, callback) {
    log.message('Check email user', request, 'request', request.guid);

    const query = {
      where: {
        email: request.email
      }
    };

    return User.findOne(query).then( user => {
      const response = {
        exists: false
      };

      if ( user ) {
        response.exists = true;
      }

      log.message('Check email user', response, 'response', request.guid);

      return callback(null, response);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  resendInvitation({ request }, callback) {
    let tokenInstance = {};

    log.message('Resend invitation', request, 'request', request.guid);

    return userProducer.ensureConnection().then(() => {
      const query = {
        where: {
          type: 'registration',
          email: request.email
        },
        include: [
          { model: Company }
        ]
      };

      return Token.findOne(query);
    }).then(token => {
      if (!token) {
        const error = {
          errors: [ {
            path: 'token',
            message: 'Not found'
          } ]
        };

        return Promise.reject(error);
      }

      tokenInstance = token.get({ plain: true });

      return Promise.resolve();
    }).then(() => {
      return userProducer.resendInvitation(tokenInstance, tokenInstance.Company, request.guid);
    }).then(() => {
      log.message('Resend invitation', tokenInstance, 'response', request.guid);

      callback(null, request);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  changePassword({ request }, callback) {
    const userId = request.user_id;
    const actualPassword = request.actual_password;
    const newPassword = request.new_password;

    let userInstance = {};

    log.message('Change password', request, 'request', request.guid);

    return userProducer.ensureConnection().then(() => {
      return User.updatePassword(userId, actualPassword, newPassword);
    }).then(user => {
      userInstance = user;

      return userProducer.passwordChanged(userInstance, request.guid);
    }).then(() => {
      log.message('Change password', userInstance, 'response', request.guid);

      callback(null, userInstance);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  generatePasswordToken({ request }, callback) {
    let tokenInstance = {
      errors: [ {
        path: 'email',
        message: 'email not found'
      } ]
    };

    log.message('Generate password token', request, 'request', request.guid);

    return userProducer.ensureConnection().then(() => {
      const query = {
        where: {
          email: request.email
        }
      };

      return User.findOne(query);
    }).then(user => {
      if (!user) {
        return Promise.resolve();
      }

      return Token.recoverPassword(user).then(token => {
        tokenInstance = token.get({ plain: true });

        return userProducer.recoverPassword(tokenInstance, request.guid);
      });
    }).then(() => {
      log.message('Generate password token', tokenInstance, 'response', request.guid);

      return callback(null, request);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  validateRecoverToken({ request }, callback) {
    log.message('Validate recover password token', request, 'request', request.guid);

    return Token.validateRecoverToken(request.token).then(token => {
      const response = _.omit(token.get({ plain: true }), [
        'data', 'type', 'created_at', 'updated_at', 'deleted_at', 'company_rfc', 'company_id', 'issuer', 'id'
      ]);

      log.message('Validate recover password token', response, 'response', request.guid);

      return callback(null, response);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  resetPassword({ request }, callback) {
    const password = request.password;

    let userInstance = {};
    let tokenInstance = {};

    log.message('Reset password', request, 'request', request.guid);

    return userProducer.ensureConnection().then(() => {
      return Token.validateRecoverToken(request.token);
    }).then(token => {
      tokenInstance = token;

      return User.resetPassword(tokenInstance.email, password);
    }).then(user => {
      userInstance = user;

      return tokenInstance.destroy();
    }).then(() => {
      return userProducer.passwordChanged(userInstance, request.guid);
    }).then(() => {
      log.message('Reset password', userInstance, 'response', request.guid);

      callback(null, userInstance);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  downloadTracker({ request }, callback) {
    log.message('Download Tracker', request, 'request', request.guid);

    return Invoice.getTrackerInfo().then( details => {
      log.message('Download Tracker', details, 'response', request.guid);

      callback(null, details);
    }).catch(err => {
      return errorHandler.format(err, request.guid, callback);
    });
  }

  findExcelTransactions({ request }, callback) {
    let totalAmount = 0;
    const preselected = true;
    let bankReport;

    log.message('Find excel transactions', request, 'request', request.guid);

    return BankReport.findOne({ order: [ [ 'created_at', 'DESC' ] ] })
      .then( _bankReport => {
        const queryParams = PendingTransaction.buildBankReportQuery({ bankReport });

        bankReport = _bankReport;

        return PendingTransaction.findAll(queryParams);
      })
      .then( pendingTransactions => {
        return PendingTransaction.bankRawData(pendingTransactions);
      })
      .then( transactions => {
        const responseTransactions = [];
        let sum = 0;

        transactions.forEach( element => {
          const amount = parseFloat(element.amount);

          if (amount !== 0) {
            sum += amount;
            delete element.rfc;
            element.selected = preselected;

            responseTransactions.push(element);
          }
        });

        totalAmount = sum;

        const response = {
          total_amount: totalAmount.toFixed(2),
          transactions: responseTransactions
        };

        log.message('Find excel transactions', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }


  generateExcelTransactions({ request }, callback) {
    log.message('Generate excel transactions', request, 'request', request.guid);

    const { guid, pending_transactions, user_id } = request;
    const ids = _.map(pending_transactions, (element) => element.id);
    let totalAmount;
    let totalGenerated;
    let s3Url;
    const data = [];

    const queryParams = PendingTransaction.buildBankReportQuery({
      by_id: { $in: ids },
      skip_where_created_at: true
    });

    return PendingTransaction.findAll(queryParams)
      .then( transactions => {
        if (!transactions || transactions.length === 0) {
          const error = {
            errors: [ {
              path: 'PendingTransaction',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        return PendingTransaction.bankRawData(transactions);
      })
      .then( pendingTransactions => {
        totalAmount = 0;
        pendingTransactions.forEach( element => {
          const amount = parseFloat(element.amount);

          if (amount !== 0) {
            totalAmount += amount;

            data.push( [ element.clabe, 'concepto', element.reference, element.amount, 'NO', element.rfc, 'iva' ] );
          }
        });


        totalGenerated = data.length;

        return BankReport.uploadCxcExcelReportDataToS3(data, guid);
      })
      .then( _s3Url => {
        s3Url = _s3Url;

        const generatedAt = moment().format('YYYY-MM-DD HH:mm:ss');
        const newData = {
          generated_at: generatedAt,
          total_amount: totalAmount,
          total_generated: totalGenerated,
          generated_by: user_id,
          link: s3Url
        };

        return BankReport.savePendingTransactionsToBankReport(ids, newData, null);
      })
      .then( () => {
        const response = {
          excel_url: s3Url
        };

        log.message('Generate excel transactions', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  excelBankReports({ request }, callback) {
    log.message('Excel bank reports', request, 'request', request.guid);

    return BankReport.findAll({
      order: [ [ 'generated_at', 'DESC' ] ],
      include: [
        { model: User }
      ]
    })
      .then( reports => {
        if ( !reports || reports.length === 0 ) {
          const error = {
            errors: [ {
              path: 'PendingTransaction',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        return BankReport.findNumberOfApprovedTransactions(reports);
      } )
      .then( reports => {
        const response = {
          excel_files: reports
        };

        log.message('Excel bank reports', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }

  findExcelTransactionsFromBankReportId({ request }, callback) {
    const { bank_report_id } = request;

    log.message('Excel transactions from bank report', request, 'request', request.guid);

    let bankReport;
    let totalAmount = 0;
    const whereParams = {
      bank_report_id
    };

    return BankReport.findOne({
      where: { id: bank_report_id },
      include: [
        { model: User, required: true }
      ]
    })
      .then( (_bankReport) => {
        if (!_bankReport) {
          const error = {
            errors: [ {
              path: 'PendingTransaction',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        bankReport = _bankReport;
        return PendingTransaction.findAll({
          where: whereParams
        });
      })
      .then( pendingTransactions => {
        if ( !pendingTransactions || pendingTransactions.length === 0) {
          const error = {
            errors: [ {
              path: 'PendingTransaction',
              message: 'Not found'
            } ]
          };

          return Promise.reject(error);
        }

        totalAmount = _.reduce(pendingTransactions, (sum, element) => {
          return sum + element.amount;
        }, 0);

        return PendingTransaction.bankRawData(pendingTransactions);
      })
      .then( transactions => {
        transactions.forEach(element => {
          delete element.rfc;
        });

        const response = {
          total_amount: totalAmount.toFixed(2),
          generated_by: bankReport.User.name,
          emission_date: bankReport.generated_at.toString(),
          bank_report_id: bankReport.id,
          transactions
        };

        log.message('Excel transactions from bank report', response, 'response', request.guid);

        return callback(null, response);
      }).catch(err => {
        return errorHandler.format(err, request.guid, callback);
      });
  }
}

module.exports = new UserController();
