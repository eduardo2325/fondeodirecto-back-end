const { InvitationNotification, NewInvoiceNotification, Token } = require('../../models');
const log = new (require('/var/lib/core/js/log'))(module);
const errorHandler = require('../helpers/error');

class StatisticsController {
  checkToken({ request }, callback) {
    let response;

    log.message('Check token', request, 'request', request.guid);

    const criteria = {
      where: {
        token: request.token
      }
    };


    return Token.findOne(criteria).then( token => {
      if ( !token ) {
        return Promise.reject({
          errors: [ {
            path: 'token',
            message: 'Token not found'
          } ]
        });
      }

      switch (request.statistics_type) {
      case 'investor-clicked-new-invoice-notification': {
        const data = {
          new_invoice_token: request.token,
          notification_type_id: 1,
          event_type: request.event_type
        };

        return NewInvoiceNotification.create(data)
          .then( instance => {
            return instance.get( { plain: true });
          }).then( metric => {
            response = {
              id: metric.id
            };

            log.message('Check token', response, 'response', request.guid);
            return callback(null, response);
          });
      }
      case 'register-investor-by-invitation': {
        return InvitationNotification.create({ token: request.token, event_type: request.event_type })
          .then( instance => {
            return instance.get( { plain: true });
          }).then( metric => {
            response = {
              id: metric.id
            };

            log.message('Check token', response, 'response', request.guid);
            return callback(null, response);
          });
      }
      default:
        return Promise.reject({
          errors: [ {
            path: 'statistics',
            message: 'Metric no valid'
          } ]
        });
      }
    })
      .catch(err => errorHandler.format(err, request.guid, callback));
  }
}

module.exports = new StatisticsController();
