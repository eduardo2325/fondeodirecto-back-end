const { Company } = require('../models');
const data = require('/var/lib/core/integration_fixtures/company');

module.exports = {
  run: () => Company.bulkCreate(data, { individualHooks: true })
};
