const { Agreement } = require('../models');
const data = require('/var/lib/core/integration_fixtures/agreements');

module.exports = {
  run: () => Agreement.bulkCreate(data, { individualHooks: false })
};
