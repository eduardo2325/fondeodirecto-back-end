const { PendingTransaction } = require('../models');
const data = require('/var/lib/core/integration_fixtures/pendingTransaction');

module.exports = {
  run: () => PendingTransaction.bulkCreate(data, { individualHooks: true })
};
