const _ = require('lodash');
const { OperationCost, Company } = require('../models');

module.exports = {
  run: () => {
    return Company.findAll()
      .then(companies => {
        const com = _.map(companies, value => {
          switch (value.role) {
          case 'COMPANY':
            return OperationCost.create({
              annual_cost: 15,
              reserve: 10,
              fd_commission: 0.5,
              company_id: value.id
            });
          case 'INVESTOR':
            if (value.id === 10058) {
              return OperationCost.create({
                fee: 350,
                variable_fee_percentage: 10,
                company_id: value.id
              });
            }
            if (value.id === 10117 || value.id === 10118) {
              return OperationCost.create({
                fee: 350,
                variable_fee_percentage: 10,
                fideicomiso_fee: 150,
                fideicomiso_variable_fee: 3,
                company_id: value.id
              });
            }
            return OperationCost.create({
              fee: 250,
              company_id: value.id
            });
          default:
            return Promise.resolve();
          }
        });

        return Promise.all(com);
      });
  }
};
