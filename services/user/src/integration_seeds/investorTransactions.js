const { InvestorTransaction } = require('../models');
const data = require('/var/lib/core/integration_fixtures/investorTransactions');

module.exports = {
  run: () => InvestorTransaction.bulkCreate(data, { individualHooks: false })
};
