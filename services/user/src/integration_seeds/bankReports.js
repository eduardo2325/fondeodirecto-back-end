
const { BankReport } = require('../models');
const data = require('/var/lib/core/integration_fixtures/bankReports');

module.exports = {
  run: () => BankReport.bulkCreate(data, { individualHooks: false })
};
