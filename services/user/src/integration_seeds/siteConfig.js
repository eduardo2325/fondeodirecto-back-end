const { SiteConfig } = require('../models');
const data = require('/var/lib/core/integration_fixtures/siteConfig');

module.exports = {
  run: () => SiteConfig.bulkCreate(data)
};
