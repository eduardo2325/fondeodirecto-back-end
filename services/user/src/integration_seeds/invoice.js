const { Invoice } = require('../models');
const data = require('/var/lib/core/integration_fixtures/invoice');

module.exports = {
  run: () => Invoice.bulkCreate(data, { individualHooks: true })
};
