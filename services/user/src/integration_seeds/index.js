const TokenSeeds = require('./token');
const CompanySeeds = require('./company');
const UserSeeds = require('./user');
const OperationCostSeeds = require('./operationCost');
const InvoiceSeeds = require('./invoice');
const PendingTransactionSeeds = require('./pendingTransaction');
const TransactionSeeds = require('./transaction');
const SessionTransactionSeeds = require('./session');
const Agreements = require('./agreements');
const OperationSeeds = require('./operation');
const InvestorTransactionSeeds = require('./investorTransactions');
const SiteConfigSeeds = require('./siteConfig');
const BankReportsSeeds = require('./bankReports');
const { sequelize } = require('../models');

const query = 'DELETE FROM new_invoice_notifications;' +
  'DELETE FROM invoice_activity;' +
  'DELETE FROM user_activities;' +
  'DELETE FROM order_invoices;' +
  'DELETE FROM orders;' +
  'DELETE FROM sessions;' +
  'DELETE FROM tokens;' +
  'DELETE FROM operation_costs;' +
  'DELETE FROM investor_transactions;' +
  'DELETE FROM operations;' +
  'DELETE FROM transactions;' +
  'DELETE FROM pending_transactions;' +
  'DELETE FROM invoices;' +
  'DELETE FROM agreements;' +
  'DELETE FROM bank_reports;' +
  "DELETE FROM users WHERE email != 'dev@fondeodirecto.com';" +
  "DELETE FROM companies WHERE (rfc not like 'FODA90080696%' and rfc != 'FODA900806965');" +
  'DELETE FROM site_configs;';

const resetPromise = sequelize.query(query);

function runTasks() {
  if (process.argv.slice(2).indexOf('--clear') !== -1) {
    return Promise.resolve(resetPromise);
  }

  return Promise.resolve(resetPromise)
    .then(() => CompanySeeds.run())
    .then(() => UserSeeds.run())
    .then(() => InvoiceSeeds.run())
    .then(() => OperationSeeds.run())
    .then(() => BankReportsSeeds.run())
    .then(() => InvestorTransactionSeeds.run())
    .then(() => {
      return Promise.all([
        TokenSeeds.run(),
        OperationCostSeeds.run(),
        Agreements.run(),
        PendingTransactionSeeds.run(),
        TransactionSeeds.run(),
        SessionTransactionSeeds.run(),
        SiteConfigSeeds.run()
      ]);
    });
}

runTasks()
  .catch(error => {
    /* eslint-disable */
    console.log(error);
    process.exit(1);
    /* eslint-enable */
  })
  .then(() => sequelize.close());
