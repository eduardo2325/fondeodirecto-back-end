const { Operation } = require('../models');
const data = require('/var/lib/core/integration_fixtures/operation');

module.exports = {
  run: () => Operation.bulkCreate(data, { individualHooks: false })
};
