const { Transaction } = require('../models');
const data = require('/var/lib/core/integration_fixtures/transaction');

module.exports = {
  run: () => Transaction.bulkCreate(data)
};
