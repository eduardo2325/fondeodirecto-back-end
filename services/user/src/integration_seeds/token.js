const { Token } = require('../models');
const data = require('/var/lib/core/integration_fixtures/token');

module.exports = {
  run: () => Token.bulkCreate(data)
};
