const { Session } = require('../models');
const data = require('/var/lib/core/integration_fixtures/session');

module.exports = {
  run: () => Session.bulkCreate(data)
};
