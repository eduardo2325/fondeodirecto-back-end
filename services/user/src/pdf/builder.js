const _ = require('lodash');

const fs = require('fs');
const pdf = require('html-pdf');

module.exports = {
  getTemplate(fileName) {
    return fs.readFileSync(`${__dirname}/templates/${fileName}.html`, 'utf8');
  },
  getOptions(defaults) {
    const options = {
      format: 'Letter',
      border: {
        top: '0.5in',
        bottom: '0.5in'
      },
      width: '8.5in',
      height: '14in'
    };

    return _.defaults(options, defaults || {});
  },
  preview(html, data) {
    let output = html;

    // this handles simple if-blocks like mustache does, e.g. {#foo}...{/foo}
    output = output.replace(/\{#(\w+)\}(.*?)\{\/\1\}/g, ($0, $1, $2) => data[$1] ? $2 : '');

    Object.keys(data).forEach(key => {
      const value = typeof data[key] === 'string'
        ? data[key].toUpperCase()
        : data[key];

      output = output.replace(new RegExp(`\\[${key}\\]`, 'g'), value);
    });

    return output;
  },
  build(html, options) {
    return new Promise((resolve, reject) => {
      if (typeof html !== 'string'
        || (!options || Array.isArray(options) || typeof options !== 'object')) {
        throw new Error('Invalid input');
      }

      pdf.create(html, options).toBuffer((err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  }
};
