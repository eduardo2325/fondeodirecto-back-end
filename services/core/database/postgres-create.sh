#!/bin/bash

set -eu

devDB="$DB_NAME"_dev
testDB="$DB_NAME"_test

wait-for-it -t 0 db:5432

psql -v ON_ERROR_STOP=1 postgresql://"$POSTGRES_USER":"$POSTGRES_PASSWORD"@db <<-EOSQL
DO
\$do\$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = '$devDB') THEN
    PERFORM dblink_exec('dbname=' || current_database(), 'CREATE DATABASE $devDB');
    GRANT ALL PRIVILEGES ON DATABASE "$devDB" TO $POSTGRES_USER;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = '$testDB') THEN
    PERFORM dblink_exec('dbname=' || current_database(), 'CREATE DATABASE $testDB');
    GRANT ALL PRIVILEGES ON DATABASE "$testDB" TO $POSTGRES_USER;
  END IF;
END
\$do\$;
EOSQL
