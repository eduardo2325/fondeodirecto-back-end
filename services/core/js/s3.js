const AWS = require('/var/lib/app/node_modules/aws-sdk');
const config = require('./config');
const Logger = require('./log');
const log = new Logger(module);

class S3 {
  constructor() {
    const options = {
      params: {
        Bucket: config.aws.s3.bucket
      },
      accessKeyId: config.aws.keyId,
      secretKey: config.aws.secretKey,
      region: config.aws.s3.region
    };

    this.s3 = new AWS.S3(options);
  }

  upload(key, body, guid = '', options = {}) {
    const params = {
      ...options,
      Key: key,
      Body: body
    };

    if (key.indexOf('.html') !== -1) {
      params.ContentType = 'text/html';
    }

    if (key.indexOf('.pdf') !== -1) {
      params.ContentType = 'application/pdf';
    }

    return new Promise((resolve, reject) => {
      this.s3.putObject(params, (err) => {
        if (err) {
          log.error('File upload failed', guid, err);

          return reject(err);
        }

        log.message('File uploaded successfully', key, 'S3', guid);

        return resolve(key);
      });
    });
  }

  getUrl(key, guid) {
    return new Promise(resolve => {
      return this.s3.getSignedUrl('getObject', { Key: key }, (error, data) => {
        if (error) {
          log.error('Get file url', guid, error);

          return resolve('Url not found');
        }

        log.message('Get file url', key, 'S3', guid);

        return resolve(data);
      });
    });
  }
}

module.exports = new S3();
