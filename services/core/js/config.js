module.exports = {
  aws: {
    keyId: process.env.AWS_ACCESS_KEY_ID,
    secretKey: process.env.AWS_SECRET_ACCESS_KEY,
    s3: {
      bucket: process.env.S3_BUCKET,
      region: process.env.S3_REGION
    }
  }
};
