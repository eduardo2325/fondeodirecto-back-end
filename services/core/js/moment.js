const moment = require('/var/lib/app/node_modules/moment-timezone');

moment.tz.setDefault(process.env.TIMEZONE || 'America/Mexico_city');

require('/var/lib/app/node_modules/moment/locale/es');

module.exports = moment;
