const _ = require('/var/lib/app/node_modules/lodash');
const companyFixtures = require('./company');
const companyUserId = _.find(companyFixtures, { name: 'CompanyUsers' }).id;
const companyChangePasswordId = _.find(companyFixtures, { name: 'ChangePassword' }).id;
const companyUploadInvoice = _.find(companyFixtures, { name: 'UploadInvoice' }).id;
const companyCXP = _.find(companyFixtures, { name: 'Recipient company' }).id;
const companyCXP2 = _.find(companyFixtures, { name: 'Sureplex' }).id;
const companyInvestor = _.find(companyFixtures, { name: 'Zum Zum' }).id;
const companyInvestorZeroBalance = _.find(companyFixtures, { name: 'Zeam' }).id;
const companyInvestorPhysical = _.find(companyFixtures, { name: 'Zensure' }).id;
const companySuspendedCXC = _.find(companyFixtures, { name: 'Suspended CXC' }).id;
const companySuspendedCXP = _.find(companyFixtures, { name: 'Suspended CXP' }).id;
const investorSuspended = _.find(companyFixtures, { name: 'Suspended INVESTOR' }).id;
const companyFunderInvestor = _.find(companyFixtures, { name: 'ERNG' }).id;
const companyExactAmountInvestor = _.find(companyFixtures, { name: 'Exact Amount' }).id;
const unapprovedCompanyId = _.find(companyFixtures, { name: 'Unapproved company' }).id;
const noPrepaymentCompanyCxP = _.find(companyFixtures, { name: 'CxP No Prepayment Company' }).id;

const oneMonthAgoDay3 = new Date();
const twoMonthAgoDay3 = new Date()
const threeMonthsAgo = new Date();

threeMonthsAgo.setHours(0, 0, 0, 0);
threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3, 2);

oneMonthAgoDay3.setHours(0, 0, 0, 0);
oneMonthAgoDay3.setMonth(oneMonthAgoDay3.getMonth() - 1, 3);
twoMonthAgoDay3.setHours(0, 0, 0, 0);
twoMonthAgoDay3.setMonth(twoMonthAgoDay3.getMonth() - 2, 3);

module.exports = [ {
  name: 'A company user',
  decrypted_password: 'securepassword',
  password: 'securepassword',
  email: 'company@fondeodirecto.com',
  company_id: companyUserId,
  color: 'A color',
  role: 'CXC'
}, {
  name: 'validuser',
  decrypted_password: 'test',
  password: 'test',
  email: 'admin@fondeodirecto.com',
  company_id: 10002,
  color: '#5A52FF',
  role: 'ADMIN'
}, {
  name: 'deleteuser',
  decrypted_password: 'test',
  password: 'test',
  email: 'delete@fondeodirecto.com',
  company_id: 10002,
  color: '#5A52FF',
  role: 'ADMIN'
}, {
  id: 100000,
  name: 'deleteuserWithSession',
  decrypted_password: 'test',
  password: 'test',
  email: 'delete2@fondeodirecto.com',
  company_id: 10104,
  color: '#5A52FF',
  role: 'ADMIN'
}, {
  name: 'ChangePasswordUser',
  decrypted_password: 'superencryptedpassword',
  password: '$2a$10$O3Fv4NlQO1kqS18KwZkGRu5RUq/GzGzM.hxjl.nTLlUc2OYe8oH9G', // Hash for 'superencryptedpassword'
  email: 'change_password@fondeodirecto.com',
  company_id: companyChangePasswordId,
  color: 'A color',
  role: 'CXC'
}, {
  name: 'RecoverPasswordUser',
  decrypted_password: 'forgottenpassword',
  password: 'forgottenpassword', // Hash for 'superencryptedpassword'
  email: 'recover_password@fondeodirecto.com',
  company_id: companyChangePasswordId,
  color: 'A color',
  role: 'CXC'
}, {
  id: 10001,
  name: 'CxcUser',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW', // Hash for '123456'
  email: 'cxc_user@fondeodirecto.com',
  company_id: companyUploadInvoice,
  color: 'A color',
  role: 'CXC'
}, {
  name: 'UploadInvoice',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW', // Hash for '123456'
  email: 'cxcuser@fondeodirecto.com',
  company_id: companyChangePasswordId,
  color: 'A color2',
  role: 'CXC'
}, {
  name: 'CxpUser',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'Cxp_user@fondeodirecto.com',
  company_id: companyCXP,
  color: 'A color',
  role: 'CXP'
}, {
  name: 'CpcUser2',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'cpc_user2@fondeodirecto.com',
  company_id: companyCXP2,
  color: 'A color',
  role: 'CXP'
}, {
  name: 'investorUser',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'investor@fondeodirecto.com',
  company_id: companyInvestor,
  color: 'A color',
  role: 'INVESTOR'
}, {
  name: 'physicalInvestor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'physical_investor@fondeodirecto.com',
  company_id: companyInvestorPhysical,
  color: 'A color',
  role: 'INVESTOR'
}, {
  name: 'zeroBalanceInvestor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'investor_zero_balance@fondeodirecto.com',
  company_id: companyInvestorZeroBalance,
  color: 'A color',
  role: 'INVESTOR'
}, {
  name: 'SuspendedCXC',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW', // Hash for '123456'
  email: 'cxcuser_suspended@fondeodirecto.com',
  company_id: companySuspendedCXC,
  color: 'A color2',
  role: 'CXC'
}, {
  name: 'SuspendedCXP',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW', // Hash for '123456'
  email: 'cxpuser_suspended@fondeodirecto.com',
  company_id: companySuspendedCXP,
  color: 'A color2',
  role: 'CXP'
}, {
  name: 'SuspendedINVESTOR',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW', // Hash for '123456'
  email: 'investor_suspended@fondeodirecto.com',
  company_id: investorSuspended,
  color: 'A color2',
  role: 'INVESTOR'
}, {
  name: 'funderInvestor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'funder_investor@fondeodirecto.com',
  company_id: companyFunderInvestor,
  color: 'A color',
  role: 'INVESTOR'
}, {
  name: 'someInvestor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'some-investor@fondeodirecto.com',
  company_id: 12002,
  color: 'A color',
  role: 'INVESTOR'
}, {
  name: 'exactAmountFundInvestor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'exact_amount_investor@fondeodirecto.com',
  company_id: companyExactAmountInvestor,
  color: 'A color',
  role: 'INVESTOR'
}, {
  name: 'Unapproved company user',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'unapproved_company_cxc@fondeodirecto.com',
  company_id: unapprovedCompanyId,
  color: 'Just a test',
  role: 'CXC'
}, {
  name: 'DeletedCxpUser',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'deleted_cxp_user@fondeodirecto.com',
  company_id: companyCXP,
  color: 'A color',
  role: 'CXP',
  deleted_at: '2018-03-01 17:12:06.612+00'
}, {
  name: 'Fideicomiso Investor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'fide_investor@fondeodirecto.com',
  company_id: 10117,
  color: 'A color',
  role: 'INVESTOR',
}, {
  name: 'Fideicomiso Physical Investor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'fide_physical@fondeodirecto.com',
  company_id: 10118,
  color: 'A color',
  role: 'INVESTOR',
}, {
  name: 'Three Months Investor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: '3m_investor@fondeodirecto.com',
  company_id: 10119,
  color: 'A color',
  role: 'INVESTOR',
}, {
  name: 'Four Months Investor',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: '4m_investor@fondeodirecto.com',
  company_id: 10120,
  color: 'A color',
  role: 'INVESTOR',
}, {
  name: 'InvestorStatementOneMonth',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'investor-one-month@fondeodirecto.com',
  company_id: 20119,
  color: 'A color',
  role: 'INVESTOR',
  created_at: oneMonthAgoDay3,
  updated_at: oneMonthAgoDay3,
}, {
  name: 'InvestorStatementTwoMonth',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'investor-two-month@fondeodirecto.com',
  company_id: 20120,
  color: 'A color',
  role: 'INVESTOR',
  created_at: twoMonthAgoDay3,
  updated_at: twoMonthAgoDay3,
}, {
  id: 101,
  name: 'Lorem CxP Dos User',
  password: '$2a$10$ovuus7JJX3EnxscfvyFeUO6jq6qh7OLx59ewa1QrOnVJQSDjCvUJG',
  email: 'cxp_user_two@fondeodirecto.com',
  company_id: 11001,
  color: '#FF8C25',
  role: 'CXP',
  created_at: threeMonthsAgo,
  updated_at: threeMonthsAgo
}, {
  id: 100,
  name: 'Lorem 2 CxC User',
  password: '$2a$10$tQjz1Nl9ks8nGFU/cCBume.1Hwm4tgdsqvDeTVi1rKm9McwYgeZK.',
  email: 'cxc_user_two@fondeodirecto.com',
  company_id: 11000,
  color: '#FF8C25',
  role: 'CXC',
  created_at: threeMonthsAgo,
  updated_at: threeMonthsAgo
}, {
  id: 103,
  name:'Lorem Investor Moral',
  decrypted_password: '12345678',
  password:'$2a$10$yGfrQ0z3kV5rbBlyTxvQk.c.Qf5EjuD/amQgqAYQXuoQ65zdFiZyi',
  email:'investor_moral_two@fondeodirecto.com',
  company_id:11003,
  color:'#01D47D',
  role:'INVESTOR',
  created_at: threeMonthsAgo,
  updated_at: threeMonthsAgo
}, {
  id: 200,
  name: 'Sendengo CxP',
  password: '$2a$10$tQjz1Nl9ks8nGFU/cCBume.1Hwm4tgdsqvDeTVi1rKm9McwYgeZK.',
  email: 'sendengo-cxp@fondeodirecto.com',
  company_id: 12000,
  color: '#FF8C25',
  role: 'CXP',
}, {
  name: 'CxP Prepayment User',
  decrypted_password: '123456',
  password: '$2a$10$8Un6VKo13J93W3YGxUTru.01SEB12hbOMj9dcv29LeJCTo3HHKpSW',
  email: 'cxp_prepago@fondeodirecto.com',
  company_id: noPrepaymentCompanyCxP,
  color: 'A color',
  role: 'CXP'
},
];
