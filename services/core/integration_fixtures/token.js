const _ = require('/var/lib/app/node_modules/lodash');
const companyFixtures = require('./company');
const userFixtures = require('./user');
const companyInvitations = _.find(companyFixtures, { name: 'CompanyUsersInvitations' });
const companyUser = _.find(companyFixtures, { name: 'CompanyUsers' });
const recoverPasswordUser = _.find(userFixtures, { name: 'RecoverPasswordUser' });
const expiration_time = 24 * (60 * 60 * 1000);

module.exports = [
  {
    token: 'testtoken',
    type: 'registration',
    email: 'test@fondeodirecto.com',
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_type: 'ADMIN',
      user_name: 'test user'
    }
  }, {
    token: 'companyinvitationstoken',
    type: 'registration',
    email: 'invitation@fondeodirecto.com',
    company_rfc: companyInvitations.rfc,
    company_id: companyInvitations.id,
    data: {
      user_type: 'cxc',
      user_name: 'Invitation token'
    }
  }, {
    token: 'companyuserstoken',
    type: 'registration',
    email: 'invitation@fondeodirecto.com',
    company_rfc: companyUser.rfc,
    company_id: companyUser.id,
    data: {
      user_type: 'cxc',
      user_name: 'Invitation token'
    }
  }, {
    token: 'validtoken',
    type: 'registration',
    email: 'test@fondeodirecto.com',
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_type: 'ADMIN',
      user_name: 'test user',
      user_color: '#5A52FF'
    }
  }, {
    token: 'deletetoken',
    type: 'registration',
    email: 'delete@fondeodirecto.com',
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_type: 'ADMIN',
      user_name: 'test user',
      user_color: '#5A52FF'
    }
  }, {
    token: 'resendToken',
    type: 'registration',
    email: 'test@fondeodirecto.com',
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_type: 'ADMIN',
      user_name: 'test user',
      user_color: '#5A52FF'
    }
  }, {
    token: 'expiredPasswordToken',
    type: 'recover_password',
    email: recoverPasswordUser.email,
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_name: recoverPasswordUser.name
    },
    created_at: new Date(new Date().getTime() - expiration_time)
  }, {
    token: 'changePasswordToken',
    type: 'recover_password',
    email: recoverPasswordUser.email,
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_name: recoverPasswordUser.name
    },
    created_at: new Date()
  },
  {
    token: 'deleteduser',
    type: 'registration',
    email: 'deleted_cxp_user@fondeodirecto.com',
    company_rfc: 'FODA900806965',
    company_id: 10001,
    data: {
      user_type: 'CXP',
      user_name: 'deleted user'
    }
  },
  {
    token: 'registerInvestorByInvitationToken',
    type: 'investor-invitation',
    email: 'investor-invitation@fondeodirecto.com',
    data:
    {
      'fee': '230.00',
      'user_type': 'investor',
      'company_name': 'Some moral Investor Company',
      'variable_fee_percentage': '13.00'
    },
    company_rfc: 'X',
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    token: 'registerPhysicalInvestorByInvitationToken',
    type: 'investor-invitation',
    email: 'investor-invitation-physical@fondeodirecto.com',
    data:
    {
      'fee': '300.00',
      'user_type': 'investor',
      'company_name': 'Some Physical Investor Company',
      'name': 'Name physical investor',
      'variable_fee_percentage': '11.00'
    },
    company_rfc: 'X',
    created_at: new Date(),
    updated_at: new Date()
	},
	{
    token: 'registerInvestorStatistics',
    type: 'investor-invitation',
    email: 'register-investor-statistics@fondeodirecto.com',
    company_rfc: 'X',
    data: {
      user_type: 'investor',
      fee: '230.00',
      variable_fee_percentage: '11.00'
    }
  },
  {
    token: 'newInvoiceStatistics',
    type: 'new-invoice-notification',
    email: 'investor@fondeodirecto.com',
    issuer: 1,
    company_rfc: 'X',
    data: {
      user_name: 'investorUser',
      user_type: 'INVESTOR',
    }
  }
];
