module.exports = [];

require('./company').forEach(company => {
  if (company.name === 'Unapproved company') return;

  return ['CXC', 'CXP', 'INVESTOR'].map(role => {
    module.exports.push({
      company_id: company.id,
      user_role: role,
      agreed_at: new Date(),
      agreement_url: 'SOME_S3_URL'
    });
  });
});
