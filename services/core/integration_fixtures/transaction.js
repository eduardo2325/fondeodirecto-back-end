const _ = require('/var/lib/app/node_modules/lodash');
const invoiceFixtures = require('./invoice');
const invoiceFundedByFideicomiso = _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9100' });
const anotherInvoiceFundedByFideicomiso = _.find(invoiceFixtures, { uuid: 'b9a6ed22-e47e-4dcf-ad5e-d6cf2d6d9d94' });

const twoMonthsAgo = new Date();
const oneMonthAgo = new Date();
const oneMonthAgoDay27 = new Date();
const oneMonthAgoDay4 = new Date();
const twoMonthAgoDay4 = new Date();
const threeMonthsAgo = new Date();
const oneMonthAgoDay5 = new Date();

threeMonthsAgo.setHours(0, 0, 0, 0);
threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3, 2);

twoMonthsAgo.setHours(0, 0, 0, 0);
twoMonthsAgo.setMonth(twoMonthsAgo.getMonth() - 2, 10);
oneMonthAgo.setHours(0, 0, 0, 0);
oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1, 3);
oneMonthAgoDay27.setHours(0, 0, 0, 0);
oneMonthAgoDay27.setMonth(oneMonthAgoDay27.getMonth() - 1, 27);
oneMonthAgoDay4.setHours(0, 0, 0, 0);
oneMonthAgoDay4.setMonth(oneMonthAgoDay4.getMonth() - 1, 4);
twoMonthAgoDay4.setHours(0, 0, 0, 0);
twoMonthAgoDay4.setMonth(twoMonthAgoDay4.getMonth() - 2, 4);
oneMonthAgoDay5.setHours(0, 0, 0, 0);
oneMonthAgoDay5.setMonth(oneMonthAgoDay5.getMonth() - 1, 5);

module.exports = [
  {
    amount: 154839.38,
    type: 'DEPOSIT',
    is_payment: false,
    invoice_id: invoiceFundedByFideicomiso.id,
    created_at: twoMonthsAgo,
    originated_from_id: 10001,
    destinated_to_id: 10117
  },
  {
    amount: 51354.17,
    type: 'DEPOSIT',
    is_payment: false,
    invoice_id: anotherInvoiceFundedByFideicomiso.id,
    created_at: twoMonthsAgo,
    originated_from_id: 10001,
    destinated_to_id: 10117
  },
  {
    amount: 333,
    type: 'DEPOSIT',
    is_payment: true,
    created_at: oneMonthAgo,
    originated_from_id: 10001,
    destinated_to_id: 10058
  },
  {
    amount: 578,
    type: 'DEPOSIT',
    is_payment: true,
    created_at: oneMonthAgo,
    originated_from_id: 10058,
    destinated_to_id: 10001
  },
  {
    amount: 25834.37,
    type: 'DEPOSIT',
    is_payment: false,
    invoice_id: 50027,
    created_at: oneMonthAgoDay27,
    originated_from_id: 10001,
    destinated_to_id: 10058
  },
  {
    amount: 578,
    type: 'DEPOSIT',
    is_payment: true,
    created_at: oneMonthAgo,
    originated_from_id: 10058,
    destinated_to_id: 10001
  },
  {
    amount: 25834.37,
    type: 'DEPOSIT',
    is_payment: false,
    invoice_id: 50027,
    created_at: oneMonthAgoDay27,
    originated_from_id: 10001,
    destinated_to_id: 10058
  },
  {
    amount: 51504.17,
    type: 'DEPOSIT',
    is_payment: false,
    invoice_id: 50028,
    created_at: oneMonthAgoDay27,
    originated_from_id: 10001,
    destinated_to_id: 10120
  },
  {
    amount: 7000000,
    type: 'DEPOSIT',
    is_payment: true,
    created_at: oneMonthAgoDay4,
    updated_at: oneMonthAgoDay4,
    originated_from_id: 20119,
    destinated_to_id: 10001
  },
  {
    amount: 10000,
    type: 'DEPOSIT',
    is_payment: true,
    created_at: twoMonthAgoDay4,
    updated_at: twoMonthAgoDay4,
    originated_from_id: 20120,
    destinated_to_id: 10001
  },
  {
    amount:3000000,
    type:'DEPOSIT',
    originated_from_id: 11003,
    destinated_to_id: 10001,
    is_payment:true,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    amount: 116000.00,
    type:'WITHDRAW',
    invoice_id: 60003,
    originated_from_id: 11003,
    destinated_to_id: 10001,
    is_payment:false,
    created_at: oneMonthAgoDay5,
    updated_at: oneMonthAgoDay5
  },
  {
    amount: 113352.94,
    invoice_id: 60003,
    type: 'DEPOSIT',
    originated_from_id: 10001,
    destinated_to_id: 11000,
    is_payment: true,
    created_at: oneMonthAgoDay5,
    updated_at: oneMonthAgoDay5
  },
  {
    amount: 116000.00,
    type:'WITHDRAW',
    invoice_id: 60004,
    originated_from_id: 11003,
    destinated_to_id: 10001,
    is_payment:false,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    amount: 113352.94,
    invoice_id: 60004,
    type: 'DEPOSIT',
    originated_from_id: 10001,
    destinated_to_id: 11000,
    is_payment: true,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
	{
		amount: 11600.00,
		type: 'DEPOSIT',
		invoice_id: 60004,
		originated_from_id: 11001,
		destinated_to_id: 10001,
		is_payment: true,
		created_at: threeMonthsAgo,
		updated_at: threeMonthsAgo
	},
	{
		amount: 11600.00,
		type: 'DEPOSIT',
		invoice_id: 60004,
		originated_from_id: 10001,
		destinated_to_id: 11000,
		is_payment: true,
		created_at: threeMonthsAgo,
		updated_at: threeMonthsAgo
	},
	{
		amount: 118311.67,
		type: 'DEPOSIT',
		invoice_id: 60004,
		originated_from_id: 10001,
		destinated_to_id: 11003,
		is_payment: false,
		created_at: threeMonthsAgo,
		updated_at: threeMonthsAgo
	},
  {
    amount: 116000.00,
    type:'WITHDRAW',
    invoice_id: 60005,
    originated_from_id: 11003,
    destinated_to_id: 10001,
    is_payment:false,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    amount: 113352.94,
    invoice_id: 60005,
    type: 'DEPOSIT',
    originated_from_id: 10001,
    destinated_to_id: 11000,
    is_payment: true,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
	{
		amount: 11600.00,
		type: 'DEPOSIT',
		invoice_id: 60005,
		originated_from_id: 11001,
		destinated_to_id: 10001,
		is_payment: true,
		created_at: twoMonthsAgo,
		updated_at: twoMonthsAgo
	},
	{
		amount: 11600.00,
		type: 'DEPOSIT',
		invoice_id: 60005,
		originated_from_id: 10001,
		destinated_to_id: 11000,
		is_payment: true,
		created_at: twoMonthsAgo,
		updated_at: twoMonthsAgo
	},
	{
		amount: 118311.67,
		type: 'DEPOSIT',
		invoice_id: 60005,
		originated_from_id: 10001,
		destinated_to_id: 11003,
		is_payment: false,
		created_at: twoMonthsAgo,
		updated_at: twoMonthsAgo
	}
];
