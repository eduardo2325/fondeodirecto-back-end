const threeMonthsAgo = new Date();

threeMonthsAgo.setHours(0, 0, 0, 0);
threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3, 2);

module.exports = [
  {
    annual_cost: 3,
    reserve: 2,
    fd_commission: 5,
    company_id: 10044
  },
  {
    annual_cost: 1.5,
    reserve: 5.4,
    fd_commission: 2,
    company_id: 10045
  },
  {
    annual_cost: 15,
    reserve: 10,
    fd_commission: 0.5,
    company_id: 10019
  },
  {
    fee: 8,
    company_id: 10077
  },
  {
    fee: 3.5,
    company_id: 10078
  },
  {
    fee: 250,
    company_id: 10058
  },
  {
    fee: 250,
    company_id: 10118
  },
  {
    annual_cost:0.70,
    reserve:0.70,
    fd_commission:0.70,
    fee:0,
    company_id: 11001,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
   annual_cost: 1.00,
   reserve: 0.50,
   fd_commission: 0.50,
   fee: 0,
   company_id: 11000,
   created_at: threeMonthsAgo,
   updated_at: threeMonthsAgo
  },
  {
    annual_cost:0,
    reserve:0,
    fd_commission:0,
    fee: 111.00,
    company_id: 11003,
    variable_fee_percentage:6.00,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
   annual_cost: 15,
   reserve: 0,
   fd_commission: 6,
   fee: 0,
   company_id: 12000
  }
];
