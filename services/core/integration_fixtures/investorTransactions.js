const _ = require('/var/lib/app/node_modules/lodash');
const invoiceFixtures = require('./invoice');
const userFixtures = require('./user');

const investorIdCompany = _.find(userFixtures, { name: 'investorUser' }).company_id;

const publishedDate = new Date();
const expirationDate = new Date();
const expired = new Date();
const sixtyDays = new Date();
const ninetyDays = new Date();
const today = new Date();
const yesterday = new Date();
const fundConfirmationDate = new Date();
const oneMonthAgoDay5 = new Date();
const twoMonthAgoDay5 = new Date();
const oneHourAgo = new Date();
const threeMonthsAgo = new Date();
const threeMonthAgoPlusFiftyThreeDaysAfter = new Date();

oneHourAgo.setHours(oneHourAgo.getHours() - 1);
expirationDate.setHours(0, 0, 0, 0);
expirationDate.setDate(expirationDate.getDate() + 59);
oneMonthAgoDay5.setHours(0, 0, 0, 0);
oneMonthAgoDay5.setMonth(oneMonthAgoDay5.getMonth() - 1, 5);
twoMonthAgoDay5.setHours(0, 0, 0, 0);
twoMonthAgoDay5.setMonth(twoMonthAgoDay5.getMonth() - 1, 5);
threeMonthsAgo.setHours(0, 0, 0, 0);
threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3, 2);
threeMonthAgoPlusFiftyThreeDaysAfter.setDate(threeMonthsAgo.getDate() + 53);

expired.setHours(0, 0, 0, 0);
expired.setDate(expired.getDate() - 30);

publishedDate.setHours(0, 0, 0, 0);
publishedDate.setDate(publishedDate.getDate() - 5);

sixtyDays.setHours(0, 0, 0, 0);
sixtyDays.setDate(sixtyDays.getDate() + 59);

ninetyDays.setHours(0, 0, 0, 0);
ninetyDays.setDate(ninetyDays.getDate() + 89);

today.setHours(0, 0, 0, 0);

yesterday.setHours(0, 0, 0, 0);
yesterday.setDate(today.getDate() - 1);

fundConfirmationDate.setHours(0, 0, 0, 0);
fundConfirmationDate.setDate(fundConfirmationDate.getDate() + 3)

module.exports = [
  {
    operation_id: 50058,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50059,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50060,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50061,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50062,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50063,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50064,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50065,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50066,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50067,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50068,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50069,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50070,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50071,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50072,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50073,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50074,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50075,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50076,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50077,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50078,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50079,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50080,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50081,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50082,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50083,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50084,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50085,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50086,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50087,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50088,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50089,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50090,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50091,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50092,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50093,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50094,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50095,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50096,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50097,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50098,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50099,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50100,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50101,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50102,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50103,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50104,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50105,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50106,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50107,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50108,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50109,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50110,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50111,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50112,
    investor_company_id: 10058,
    funded_amount: 100000,
    interest: 3708.33,
    interest_percentage: 3.71,
    isr: 741.67,
    isr_percentage: 0.74,
    global_fee_percentage: '1/3',
    fee: 250,
    fee_type: 'Fixed fee',
    earnings: 3458.33,
    earnings_percentage: 3.46,
    perception: 102716.66,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50113,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
    funded_amount: 100000.00,
    interest: 3708.33,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0.00,
    global_fee_percentage: '1/3',
    fee: 370.83,
    fee_type: 'Variable fee',
    earnings: 3337.50,
    earnings_percentage: 3.34,
    perception: 103337.50,
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50114,
    investor_company_id: 10114,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50115,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 59,
    status: 'PENDING',
		funded_amount: 100000,
    interest: 2458.33,
    interest_percentage: 2.46,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 350.00,
    fee_type: 'Fixed fee',
    earnings: 2108.33,
    earnings_percentage: 2.11,
    perception: 102108.33,
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50116,
    investor_company_id: 10114,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50117,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50118,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50119,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50120,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50121,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50122,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50123,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50124,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50126,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50127,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50128,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50129,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'PENDING',
		fee: 250
  },
  {
    operation_id: 50133,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 25000,
    interest: 927.08,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 92.71,
    fee_type: 'Variable fee',
    earnings: 834.37,
    earnings_percentage: 3.34,
    perception: 25834.37,
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50134,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
		funded_amount: 50000,
    interest: 1854.17,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 350,
    fee_type: 'Fixed fee',
    earnings: 1504.17,
    earnings_percentage: 3.01,
    perception: 51504.17,
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50135,
    investor_company_id: investorIdCompany,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
		funded_amount: 150000,
    interest: 5562.50,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 556.25,
    fee_type: 'Variable fee',
    earnings: 5006.25,
    earnings_percentage: 3.34,
    perception: 155006.25,
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50141,
    investor_company_id: 10117,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 25000,
    interest: 927.08,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 92.71,
    fee_type: 'Variable fee',
    earnings: 806.56,
    earnings_percentage: 3.23,
    perception: 25806.56,
    fideicomiso_fee: 27.81,
    fideicomiso_fee_type: 'Variable fee',
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50142,
    investor_company_id: 10117,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 50000,
    interest: 1854.17,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 350,
    fee_type: 'Fixed fee',
    earnings: 1354.17,
    earnings_percentage: 2.71,
    perception: 51354.17,
    fideicomiso_fee: 150,
    fideicomiso_fee_type: 'Fixed fee',
    fund_date: fundConfirmationDate
  },
  {
    operation_id: 50143,
    investor_company_id: 10117,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 150000,
    interest: 5562.50,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 556.25,
    fee_type: 'Variable fee',
    earnings: 4839.38,
    earnings_percentage: 3.23,
    perception: 154839.38,
    fideicomiso_fee: 166.88,
    fideicomiso_fee_type: 'Variable fee',
    fund_date: today
  },
  {
    operation_id: 50144,
    investor_company_id: 10117,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 150000,
    interest: 5562.50,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 556.25,
    fee_type: 'Variable fee',
    earnings: 4839.38,
    earnings_percentage: 3.23,
    perception: 154839.38,
    fideicomiso_fee: 166.88,
    fideicomiso_fee_type: 'Variable fee',
    fund_date: oneMonthAgoDay5
  },
  {
    operation_id: 50145,
    investor_company_id: 20119,
    fund_request_date: twoMonthAgoDay5,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 1000,
    interest: 5562.50,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 10.25,
    fee_type: 'Variable fee',
    earnings: 10.38,
    earnings_percentage: 3.23,
    perception: 0.0,
    fideicomiso_fee: 0.88,
    fideicomiso_fee_type: 'Variable fee',
    fund_date: oneMonthAgoDay5
  },
  {
    operation_id: 50146,
    investor_company_id: 20120,
    fund_request_date: today,
    days_limit: 89,
    status: 'COMPLETED',
    funded_amount: 150000,
    interest: 5562.50,
    interest_percentage: 3.71,
    isr: 0,
    isr_percentage: 0,
    global_fee_percentage: '1/3',
    fee: 556.25,
    fee_type: 'Variable fee',
    earnings: 4839.38,
    earnings_percentage: 3.23,
    perception: 154839.38,
    fideicomiso_fee: 166.88,
    fideicomiso_fee_type: 'Variable fee',
    fund_date: twoMonthAgoDay5
  },
  {
    operation_id:60001,
    investor_company_id:11003,
    fund_request_date: new Date(),
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status:'PENDING'
  },
  {
    operation_id:60000,
    investor_company_id:11003,
    fund_request_date: new Date(),
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status:'PENDING'
  },
  {
    operation_id:60001,
    investor_company_id:11003,
    fund_request_date: new Date(),
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status:'PENDING'
  },
  {
    operation_id:60002,
    investor_company_id:11003,
    fund_request_date: oneHourAgo,
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status:'PENDING'
  },
  {
    operation_id:60003,
    investor_company_id:11003,
    fund_request_date: oneMonthAgoDay5,
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status: 'COMPLETED',
    fund_date: oneMonthAgoDay5,
    created_at: oneMonthAgoDay5,
    updated_at: oneMonthAgoDay5
  },
  {
    operation_id:60004,
    investor_company_id:11003,
    fund_request_date: threeMonthsAgo,
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status: 'COMPLETED',
    fund_date: threeMonthsAgo,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    operation_id:60005,
    investor_company_id:11003,
    fund_request_date: threeMonthsAgo,
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status: 'COMPLETED',
    fund_date: threeMonthsAgo,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    operation_id:60008,
    investor_company_id: 12002,
    fund_request_date: new Date(),
    days_limit:53,
    funded_amount:116000.00,
    interest:2561.67,
    interest_percentage:2.21,
    fee:250.00,
    fee_type:'Fixed fee',
    global_fee_percentage:'1/3',
    earnings:2311.67,
    earnings_percentage:1.99,
    isr:0.00,
    isr_percentage:0.00,
    perception:118311.67,
    status: 'PENDING',
    fund_date: new Date(),
    created_at: new Date(),
    updated_at: new Date() 
  }
];
