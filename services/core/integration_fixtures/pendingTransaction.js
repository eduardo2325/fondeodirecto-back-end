const _ = require('/var/lib/app/node_modules/lodash');
const invoiceFixtures = require('./invoice');
const invoiceFundRequested = _.find(invoiceFixtures, { uuid: '97c63625-0963-4a11-8012-APPROVEFUND' });
const invoiceRejectFundRequested = _.find(invoiceFixtures, { uuid: 'f9a2ed62-e43f-3cdf-bd6e-c6cc2e6d9d78' });
const invoiceNotRelated = _.find(invoiceFixtures, { uuid: '97c63625-0963-4a11-8012-d8b65aa9a758' });
const paymentInProcessInvoiceWithReceipt = _.find(invoiceFixtures, { uuid: '94f53625-9361-9h55-0825-d8b60eh4g926' });
const paymentInProcessInvoiceWithoutReceipt = _.find(invoiceFixtures, { uuid: '50d68425-1750-0a63-8012-d8b65ls8b820' });
const paymentInProcessInvoceToComplete = _.find(invoiceFixtures, { uuid: '02h63625-9264-9h55-0742-d8b60eh4s825' });

const paymentDate = new Date();
const twoMonthsAgo = new Date();
const oneMonthAgoDay10 = new Date();
const oneMonthAgoDay3 = new Date();
const oneMonthAgoDay4 = new Date();
const oneMonthAgoDay5 = new Date();
const twoMonthAgoDay4 = new Date();
const threeMonthsAgo = new Date();
const now = new Date();
const oneHourAgo = new Date();
const threeMonthAgoPlusFiftyThreeDaysAfter = new Date();

oneHourAgo.setHours(oneHourAgo.getHours() - 1);
threeMonthsAgo.setHours(0, 0, 0, 0);
threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3, 2);

twoMonthsAgo.setHours(0, 0, 0, 0);
twoMonthsAgo.setMonth(twoMonthsAgo.getMonth() - 2, 10);
oneMonthAgoDay3.setHours(0, 0, 0, 0);
oneMonthAgoDay3.setMonth(oneMonthAgoDay3.getMonth() - 1, 3);
oneMonthAgoDay10.setHours(0, 0, 0, 0);
oneMonthAgoDay10.setMonth(oneMonthAgoDay10.getMonth() - 1, 10);
oneMonthAgoDay4.setHours(0, 0, 0, 0);
oneMonthAgoDay4.setMonth(oneMonthAgoDay4.getMonth() - 1, 4);
twoMonthAgoDay4.setHours(0, 0, 0, 0);
twoMonthAgoDay4.setMonth(twoMonthAgoDay4.getMonth() - 2, 4);
oneMonthAgoDay5.setHours(0, 0, 0, 0);
oneMonthAgoDay5.setMonth(oneMonthAgoDay5.getMonth() - 1, 5);
threeMonthAgoPlusFiftyThreeDaysAfter.setDate(threeMonthsAgo.getDate() + 53);

module.exports = [
  {
    amount: 100,
    type: 'WITHDRAW',
    company_id: 10060,
    data: {},
    status: 'PENDING'
  }, {
    amount: 200,
    type: 'WITHDRAW',
    company_id: 10060,
    data: {},
    status: 'PENDING'
  }, {
    id: 50000,
    amount: 300,
    type: 'DEPOSIT',
    company_id: 10060,
    data: {
      key: 'DepositTicket.png'
    },
    status: 'PENDING'
  }, {
    id: 50001,
    amount: 300,
    type: 'DEPOSIT',
    company_id: 10060,
    data: {},
    status: 'APPROVED'
  }, {
    id: 50002,
    amount: 300,
    type: 'WITHDRAW',
    company_id: 10060,
    data: {},
    status: 'PENDING'
  }, {
    id: 50003,
    amount: 300,
    type: 'DEPOSIT',
    company_id: 10060,
    data: {},
    status: 'PENDING'
  }, {
    id: 50004,
    amount: 300,
    type: 'WITHDRAW',
    company_id: 10060,
    data: {},
    status: 'PENDING'
  }, {
    id: 50005,
    amount: 300000,
    type: 'WITHDRAW',
    company_id: 10061,
    data: {},
    status: 'PENDING'
  }, {
    id: 50006,
    amount: 300000,
    type: 'WITHDRAW',
    company_id: 10114,
    data: {
      invoice_id: invoiceFundRequested.id,
      fund_date: invoiceFundRequested.fund_date,
      company_id: invoiceFundRequested.company_id,
      client_company_id: invoiceFundRequested.client_company_id,
      investor_company_id: invoiceFundRequested.investor_company_id
    },
    status: 'PENDING'
  }, {
    id: 50007,
    amount: 300000,
    type: 'WITHDRAW',
    company_id: 10114,
    data: {
      invoice_id: 9999999,
      fund_date: invoiceFundRequested.fund_date,
      company_id: invoiceFundRequested.company_id,
      client_company_id: invoiceFundRequested.client_company_id,
      investor_company_id: invoiceFundRequested.investor_company_id
    },
    status: 'PENDING'
  }, {
    id: 50008,
    amount: 300000,
    type: 'WITHDRAW',
    company_id: 10114,
    data: {
      invoice_id: invoiceNotRelated.id,
      fund_date: invoiceNotRelated.fund_date,
      company_id: invoiceNotRelated.company_id,
      client_company_id: invoiceNotRelated.client_company_id,
      investor_company_id: invoiceNotRelated.investor_company_id
    },
    status: 'PENDING'
  }, {
    id: 50009,
    amount: paymentInProcessInvoiceWithReceipt.total,
    type: 'DEPOSIT',
    company_id: paymentInProcessInvoiceWithReceipt.client_company_id,
    data: {
      invoice_id: paymentInProcessInvoiceWithReceipt.id,
      payment_date: paymentDate,
      key: `cxp/${paymentInProcessInvoiceWithReceipt.client_company_id}/payments/file.png`
    },
    status: 'PENDING'
  }, {
    id: 50010,
    amount: paymentInProcessInvoiceWithoutReceipt.total,
    type: 'DEPOSIT',
    company_id: paymentInProcessInvoiceWithoutReceipt.client_company_id,
    data: {
      invoice_id: paymentInProcessInvoiceWithoutReceipt.id,
      payment_date: paymentDate
    },
    status: 'PENDING'
  }, {
    id: 50011,
    amount: 300000,
    type: 'WITHDRAW',
    company_id: 10114,
    data: {
      invoice_id: invoiceRejectFundRequested.id,
      fund_date: invoiceRejectFundRequested.fund_date,
      company_id: invoiceRejectFundRequested.company_id,
      client_company_id: invoiceRejectFundRequested.client_company_id,
      investor_company_id: invoiceRejectFundRequested.investor_company_id
    },
    status: 'PENDING'
  },
  {
    id: 50012,
    amount: paymentInProcessInvoceToComplete.total,
    type: 'DEPOSIT',
    company_id: paymentInProcessInvoceToComplete.client_company_id,
    data: {
      invoice_id: paymentInProcessInvoceToComplete.id,
      payment_date: paymentDate
    },
    status: 'PENDING'
  },
  {
    id: 50013,
    amount: 3000,
    type: 'DEPOSIT',
    company_id: 10038,
    data: { },
    status: 'PENDING'
  },
  {
    id: 50014,
    amount: 3000,
    type: 'WITHDRAW',
    company_id: 10060,
    data: {
      invoice_id: 63
    },
    status: 'PENDING'
  },
  {
    id: 50015,
    amount: paymentInProcessInvoceToComplete.total,
    type: 'DEPOSIT',
    company_id: 10117,
    data: {
      invoice_id: paymentInProcessInvoceToComplete.id,
      payment_date: paymentDate
    },
    status: 'APPROVED',
    created_at: twoMonthsAgo
  },
  {
    id: 50016,
    amount: 10058,
    type: 'WITHDRAW',
    company_id: 10117,
    data: {
      invoice_id: 50037,
      payment_date: paymentDate,
    },
    status: 'APPROVED',
    created_at: twoMonthsAgo
  },
  {
    id: 50017,
    amount: 200000,
    type: 'DEPOSIT',
    company_id: 10117,
    data: {
      invoice_id: paymentInProcessInvoceToComplete.id,
      payment_date: paymentDate
    },
    status: 'APPROVED',
    created_at: twoMonthsAgo
  },
  {
    id: 50018,
    amount: 150000,
    type: 'WITHDRAW',
    company_id: 10117,
    data: {
      invoice_id: 50034,
      payment_date: paymentDate,
    },
    status: 'APPROVED',
    created_at: twoMonthsAgo
  },
  {
    id: 50019,
    amount: 333,
    type: 'WITHDRAW',
    status: 'APPROVED',
    created_at: oneMonthAgoDay10,
    company_id: 10058
  },
  {
    id: 50020,
    amount: 578,
    data: {"key": "investors/10058/deposits/36-1539206274671.png", "deposit_date": oneMonthAgoDay10},
    type: 'DEPOSIT',
    status: 'APPROVED',
    created_at: oneMonthAgoDay10,
    company_id: 10058
  },
  {
    id: 50021,
    amount: 25000,
    type: 'WITHDRAW',
    company_id: 10058,
    data: {
      invoice_id: 50027,
      payment_date: paymentDate,
    },
    status: 'APPROVED',
    created_at: oneMonthAgoDay3
  },
  {
    id: 50022,
    amount: 1067,
    type: 'WITHDRAW',
    status: 'APPROVED',
    created_at: twoMonthsAgo,
    company_id: 10119
  },
  {
    id: 50023,
    amount: 10000,
    data: {"key": "investors/10058/deposits/36-1539206274671.png", "deposit_date": twoMonthsAgo},
    type: 'DEPOSIT',
    status: 'APPROVED',
    created_at: twoMonthsAgo,
    company_id: 10119
  },
  {
    id: 50024,
    amount: 15000,
    type: 'WITHDRAW',
    status: 'APPROVED',
    created_at: oneMonthAgoDay3,
    company_id: 10120
  },
  {
    id: 50025,
    amount: 23000,
    data: {"key": "investors/10058/deposits/36-1539206274671.png", "deposit_date": oneMonthAgoDay10},
    type: 'DEPOSIT',
    status: 'APPROVED',
    created_at: oneMonthAgoDay10,
    company_id: 10120
  },
  {
    id: 50026,
    amount: 500,
    data: {"key": "investors/10058/deposits/36-1539206274671.png", "deposit_date": twoMonthsAgo},
    type: 'DEPOSIT',
    status: 'APPROVED',
    created_at: twoMonthsAgo,
    company_id: 10120
  },
  {
    id: 50027,
    type: 'DEPOSIT',
    status: 'APPROVED',
    amount: 7000000,
    data: {"key": "investors/10058/deposits/36-1539206274671.png", "deposit_date": oneMonthAgoDay4},
    created_at: oneMonthAgoDay4,
    updated_at: oneMonthAgoDay4,
    company_id: 20119
  },
  {
    id: 50028,
    amount: 10000,
    data: {"key": "investors/10058/deposits/36-1539206274671.png", "deposit_date": twoMonthAgoDay4},
    type: 'DEPOSIT',
    status: 'APPROVED',
    created_at: twoMonthAgoDay4,
    updated_at: twoMonthAgoDay4,
    company_id: 20120
  },
  {
    id: 50029,
    amount: 3000000,
    type:'DEPOSIT',
    company_id: 11003 ,
    data: {"deposit_date": threeMonthsAgo.toString(), "key":"investors/3/deposits/154-1540568126214.png"},
    status:'APPROVED',
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    id: 50030,
    amount:116000,
    type:'WITHDRAW',
    company_id:11003,
    data: {"invoice_id":60000,"fund_date": now.toString(),"company_id":11000,"client_company_id":11001,"investor_company_id":11003},
    status:'PENDING',
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    id: 50031,
    amount:116000,
    type:'WITHDRAW',
    company_id:11003,
    data:{"invoice_id":60001,"fund_date":now.toString(),"company_id":11000,"client_company_id":11001,"investor_company_id":11003},
    status:'PENDING',
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    id: 50032,
    amount:116000,
    type:'WITHDRAW',
    bank_report_id: 1000,
    company_id:11003,
    data:{"invoice_id":60002,"fund_date": oneHourAgo.toString(),"company_id":11000,"client_company_id":11001,"investor_company_id":11003},
    status:'PENDING',
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    id: 50033,
    amount:116000,
    type:'WITHDRAW',
    company_id:11003,
    data:{"invoice_id":60003,"fund_date": oneMonthAgoDay5.toString(),"company_id":11000,"client_company_id":11001,"investor_company_id":11003},
    status:'APPROVED',
    created_at: oneMonthAgoDay5,
    updated_at: oneMonthAgoDay5
  },
  {
    id: 50034,
    amount: 116000.00,
    type: 'DEPOSIT',
    bank_report_id: 1000,
    company_id: 11001,
    data: {"invoice_id":60003,"payment_date":oneHourAgo.toString(),"key":"cxp/10106/payments/25-1540921325287.png"},
    status:'PENDING',
    created_at:oneHourAgo,
    updated_at:oneHourAgo
  },
  {
    id: 50035,
    amount:116000,
    type:'WITHDRAW',
    company_id:11003,
    data:{"invoice_id":60004,"fund_date": threeMonthsAgo.toString(),"company_id":11000,"client_company_id":11001,"investor_company_id":11003},
    status:'APPROVED',
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    id: 50036,
    amount: 116000.00,
    type: 'DEPOSIT',
    company_id: 11001,
    data: {"invoice_id":60004,"payment_date":threeMonthsAgo.toString(),"key":"cxp/10106/payments/25-1540921325287.png"},
    status:'APPROVED',
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
	{
    id: 50037,
    amount:116000,
    type:'WITHDRAW',
    company_id:11003,
    data:{"invoice_id":60005,"fund_date": threeMonthsAgo.toString(),"company_id":11000,"client_company_id":11001,"investor_company_id":11003},
    status:'APPROVED',
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
	{
    id: 50038,
    amount: 116000.00,
    type: 'DEPOSIT',
    company_id: 11001,
    data: {"invoice_id":60005,"payment_date":twoMonthsAgo.toString(),"key":"cxp/10106/payments/25-1540921325287.png"},
    status:'APPROVED',
    created_at: twoMonthsAgo,
    updated_at: twoMonthsAgo
  },
  {
    id: 50039,
    amount:116000,
    type:'WITHDRAW',
    company_id:12002,
    data:{"invoice_id":60010,"fund_date": oneHourAgo.toString(),"company_id":10105,"client_company_id":12000,"investor_company_id":12002},
    status:'PENDING',
    created_at: new Date(),
    updated_at: new Date()
  }
];
