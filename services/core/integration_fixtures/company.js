const threeMonthsAgo = new Date();
const fourMonthsAgo = new Date();

threeMonthsAgo.setHours(0, 0, 0, 0);
threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3, 2);
fourMonthsAgo.setHours(0, 0, 0, 0);
fourMonthsAgo.setMonth(fourMonthsAgo.getMonth() - 4, 2);

const oneMonthAgoDay3 = new Date();
const oneMonthAgoDay4 = new Date();
const twoMonthAgoDay3 = new Date()
const twoMonthAgoDay4 = new Date();

oneMonthAgoDay3.setHours(0, 0, 0, 0);
oneMonthAgoDay3.setMonth(oneMonthAgoDay3.getMonth() - 1, 3);
oneMonthAgoDay4.setHours(0, 0, 0, 0);
oneMonthAgoDay4.setMonth(oneMonthAgoDay4.getMonth() - 1, 4);

twoMonthAgoDay3.setHours(0, 0, 0, 0);
twoMonthAgoDay3.setMonth(twoMonthAgoDay3.getMonth() - 2, 3);
twoMonthAgoDay4.setHours(0, 0, 0, 0);
twoMonthAgoDay4.setMonth(twoMonthAgoDay4.getMonth() - 2, 4);

module.exports = [
  {
    id: 10002,
    rfc: 'DODA900806965',
    role: 'COMPANY',
    name: 'Test Directo',
    business_name: 'Test Directo',
    holder: 'Test Directo',
    clabe: '019090700355936225',
    color: '#5A52FF'
  }, {
    id: 10003,
    rfc: 'RADA900806965',
    role: 'COMPANY',
    name: 'Test Name',
    business_name: 'Test Name',
    holder: 'Test Name',
    clabe: '059090700355936525',
    color: '#5A52FF'
  }, {
    id: 10004,
    rfc: 'FADA900806965',
    role: 'COMPANY',
    name: 'Company Name',
    business_name: 'Company Name',
    holder: 'Company Name',
    clabe: '134090700355936825',
    color: '#5A52FF'
  }, {
    id: 10005,
    rfc: 'AAZJ87112049068',
    name: 'CompanyUsersInvitations',
    business_name: 'Company Invitations Test',
    holder: 'A user',
    clabe: '002090700355936231',
    role: 'COMPANY',
    color: '#5A52FF'
  }, {
    id: 10006,
    rfc: 'AAZJ87112049069',
    name: 'CompanyUsers',
    business_name: 'Company Users Test',
    holder: 'A user',
    clabe: '002090700355926431',
    role: 'COMPANY',
    color: '#5A52FF'
  }, {
    id: 10007,
    rfc: 'BDJY47118049069',
    name: 'Hatology',
    business_name: 'TINGLES',
    holder: 'Ida Blackburn',
    role: 'COMPANY',
    color: '#5A52FF',
    clabe: '006070035593652411'
  }, {
    id: 10008,
    rfc: 'BTHV97118049069',
    name: 'Zanymax',
    business_name: 'FISHLAND',
    holder: 'Tonya Hickman',
    role: 'COMPANY',
    color: '#707E8E',
    clabe: '012070035593623212'
  }, {
    id: 10009,
    rfc: 'CTJY97118049069',
    name: 'Aquasseur',
    business_name: 'ONTAGENE',
    holder: 'Patsy Strong',
    role: 'COMPANY',
    color: '#CCCCCC',
    clabe: '002070035593659753'
  }, {
    id: 10010,
    rfc: 'BDJY47118046069',
    name: 'Nikuda',
    business_name: 'EVENTIX',
    holder: 'Lelia Espinoza',
    role: 'COMPANY',
    color: '#01D47D',
    clabe: '014070035593609432'
  }, {
    id: 10011,
    rfc: 'CDHK77118049069',
    name: 'Cytrak',
    business_name: 'ZILLACOM',
    holder: 'Agnes Vang',
    role: 'COMPANY',
    color: '#0491F3',
    clabe: '012070035593695593'
  }, {
    id: 10012,
    rfc: 'GDHY97118049069',
    name: 'Anivet',
    business_name: 'GEOLOGIX',
    holder: 'Hickman Gross',
    role: 'COMPANY',
    color: '#5A52FF',
    clabe: '014070035593666354'
  }, {
    id: 10013,
    rfc: 'CTEK47118049069',
    name: 'Multiflex',
    business_name: 'PLASTO',
    holder: 'Raymond Wiley',
    role: 'COMPANY',
    color: '#00CBDA',
    clabe: '012070035593643471'
  }, {
    id: 10014,
    rfc: 'GTEK47118049069',
    name: 'Bezal',
    business_name: 'DRAGBOT',
    holder: 'Le Houston',
    role: 'COMPANY',
    color: '#0491F3',
    clabe: '014070035593638073'
  }, {
    id: 10015,
    rfc: 'BDHY77118049069',
    name: 'Architax',
    business_name: 'TURNLING',
    holder: 'Lucille Valentine',
    role: 'COMPANY',
    color: '#707E8E',
    clabe: '006070035593602374'
  }, {
    id: 10016,
    rfc: 'CTEK97118049069',
    name: 'Gazak',
    business_name: 'OZEAN',
    holder: 'Katy Parks',
    role: 'COMPANY',
    color: '#CCCCCC',
    clabe: '006070035593608383'
  }, {
    id: 10017,
    rfc: 'BRHY97118049069',
    name: 'Enerforce',
    business_name: 'BOVIS',
    holder: 'Briana Patrick',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '019070035593690595'
  }, {
    id: 10018,
    rfc: 'CDHY47118049069',
    name: 'Cowtown',
    business_name: 'PIVITOL',
    holder: 'Carey Fowler',
    role: 'COMPANY',
    color: '#FF7372',
    clabe: '006070035593659354'
  }, {
    id: 10019,
    rfc: 'CRJY47118049069',
    name: 'Sureplex',
    business_name: 'ACRODANCE',
    holder: 'Ashley Monroe',
    role: 'COMPANY',
    color: '#FF8C25',
    clabe: '012070035593651923'
  }, {
    id: 10020,
    rfc: 'CRHY77118049069',
    name: 'Ontality',
    business_name: 'CENTREXIN',
    holder: 'Nadine Stein',
    role: 'COMPANY',
    color: '#4B637B',
    clabe: '014070035593672154'
  }, {
    id: 10021,
    rfc: 'GTEV97118049069',
    name: 'Senmao',
    business_name: 'ZUVY',
    holder: 'Sheryl Harmon',
    role: 'COMPANY',
    color: '#707E8E',
    clabe: '002070035593654841'
  }, {
    id: 10022,
    rfc: 'BRJK77118049069',
    name: 'Magnafone',
    business_name: 'ENDICIL',
    holder: 'Maribel Kent',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '030070035593693757'
  }, {
    id: 10023,
    rfc: 'CTEK77118049069',
    name: 'Plasmos',
    business_name: 'SUREMAX',
    holder: 'Clarke Galloway',
    role: 'COMPANY',
    color: '#FF8C25',
    clabe: '019070035593607358'
  }, {
    id: 10024,
    rfc: 'CDHY47148049069',
    name: 'Permadyne',
    business_name: 'ROTODYNE',
    holder: 'Clark Gamble',
    role: 'COMPANY',
    color: '#CCCCCC',
    clabe: '030070035593686948'
  }, {
    id: 10025,
    rfc: 'BTEY47118049069',
    name: 'Kage',
    business_name: 'CONCILITY',
    holder: 'Katie Hyde',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '030070035593642134'
  }, {
    id: 10026,
    rfc: 'CDJY97118049069',
    name: 'Gallaxia',
    business_name: 'FLYBOYZ',
    holder: 'Simon Ingram',
    role: 'COMPANY',
    color: '#CCCCCC',
    clabe: '019070035593652294'
  }, {
    id: 10027,
    rfc: 'BRHY77118049069',
    name: 'Oatfarm',
    business_name: 'PORTICO',
    holder: 'Fay Potts',
    role: 'COMPANY',
    color: '#01D47D',
    clabe: '019070035593622801'
  }, {
    id: 10028,
    rfc: 'CRJY97118049069',
    name: 'Quadeebo',
    business_name: 'VITRICOMP',
    holder: 'Corine Byrd',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '021070035593699095'
  }, {
    id: 10029,
    rfc: 'BDEY47118049069',
    name: 'Geofarm',
    business_name: 'GLUID',
    holder: 'Cooke Bond',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '019070035593610313'
  }, {
    id: 10030,
    rfc: 'BRHV77118049069',
    name: 'Grupoli',
    business_name: 'ISOSURE',
    holder: 'Clarice Castro',
    role: 'COMPANY',
    color: '#FF8C25',
    clabe: '030070035593608957'
  }, {
    id: 10031,
    rfc: 'GREV97118049069',
    name: 'Zolavo',
    business_name: 'KONGENE',
    holder: 'Eloise Elliott',
    role: 'COMPANY',
    color: '#FF8C25',
    clabe: '002070035593663785'
  }, {
    id: 10032,
    rfc: 'CRHY71118049069',
    name: 'Utarian',
    business_name: 'ZBOO',
    holder: 'Beth Kline',
    role: 'COMPANY',
    color: '#0491F3',
    clabe: '030070035593648123'
  }, {
    id: 10033,
    rfc: 'CRHY97118049069',
    name: 'Portalis',
    business_name: 'MOTOVATE',
    holder: 'Osborne Carver',
    role: 'COMPANY',
    color: '#00CBDA',
    clabe: '019070035593614256'
  }, {
    id: 10034,
    rfc: 'CDHV97118049069',
    name: 'Farmage',
    business_name: 'MANTRIX',
    holder: 'Weber Dennis',
    role: 'COMPANY',
    color: '#00CBDA',
    clabe: '012070035593607182'
  }, {
    id: 10035,
    rfc: 'GDHK97118049069',
    name: 'Wrapture',
    business_name: 'PHORMULA',
    holder: 'Ebony Jimenez',
    role: 'COMPANY',
    color: '#00CBDA',
    clabe: '019070035593622799'
  }, {
    id: 10036,
    rfc: 'GTEV47118049069',
    name: 'Candecor',
    business_name: 'VENOFLEX',
    holder: 'Atkins Mckee',
    role: 'COMPANY',
    color: '#CCCCCC',
    clabe: '021070035593618939'
  }, {
    id: 10037,
    rfc: 'CDJK97118049069',
    name: 'Medifax',
    business_name: 'REMOLD',
    holder: 'Potts Gill',
    role: 'COMPANY',
    color: '#4B637B',
    clabe: '009070035593641439'
  }, {
    id: 10038,
    rfc: 'GRJY97118049069',
    name: 'Namebox',
    business_name: 'NETILITY',
    holder: 'Verna Sexton',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '009070035593616401'
  }, {
    id: 10039,
    rfc: 'CDEY77118049069',
    name: 'Icology',
    business_name: 'BLEEKO',
    holder: 'Simpson Case',
    role: 'COMPANY',
    color: '#4B637B',
    clabe: '021070035593637709'
  }, {
    id: 10040,
    rfc: 'CDJV97118049069',
    name: 'Dentrex',
    business_name: 'QUORDATE',
    holder: 'Horton Walters',
    role: 'COMPANY',
    color: '#FF8C25',
    clabe: '009070035593672701'
  }, {
    id: 10041,
    rfc: 'GRHY77118049069',
    name: 'Nurplex',
    business_name: 'PARCOE',
    holder: 'Kent Moon',
    role: 'COMPANY',
    color: '#FF7372',
    clabe: '009070035593649392'
  }, {
    id: 10042,
    rfc: 'GRHV77118049069',
    name: 'Cemention',
    business_name: 'REALYSIS',
    holder: 'Frances Roth',
    role: 'COMPANY',
    color: '#FF8C25',
    clabe: '006070035593605753'
  }, {
    id: 10043,
    rfc: 'BRHV47118049069',
    name: 'Photobin',
    business_name: 'ORBOID',
    holder: 'Walsh Hicks',
    role: 'COMPANY',
    color: '#00CBDA',
    clabe: '019070035593651423'
  }, {
    id: 10044,
    rfc: 'GDHK47118049069',
    name: 'Voratak',
    business_name: 'EARBANG',
    holder: 'Jeanine Daniels',
    role: 'COMPANY',
    color: '#00CBDA',
    clabe: '006070035593604028'
  }, {
    id: 10045,
    rfc: 'GREV92118049069',
    name: 'Deepends',
    business_name: 'MEDCOM',
    holder: 'Katrina Calhoun',
    role: 'COMPANY',
    color: '#707E8E',
    clabe: '009070035593693903'
  }, {
    id: 10046,
    rfc: 'BTEV97118049069',
    name: 'Centregy',
    business_name: 'MUSANPOLY',
    holder: 'Lindsay Workman',
    role: 'COMPANY',
    color: '#4B637B',
    clabe: '014070035593615424'
  }, {
    id: 10047,
    rfc: 'GDEY77118049069',
    name: 'Futurize',
    business_name: 'UNDERTAP',
    holder: 'Estella Macdonald',
    role: 'COMPANY',
    color: '#707E8E',
    clabe: '006070035593657558'
  }, {
    id: 10048,
    rfc: 'CRJK97118049069',
    name: 'Xymonk',
    business_name: 'PIGZART',
    holder: 'Acosta Cherry',
    role: 'COMPANY',
    color: '#03153D',
    clabe: '014070035593685798'
  }, {
    id: 10049,
    rfc: 'CRJK47118049069',
    name: 'Quilk',
    business_name: 'UNEEQ',
    holder: 'Lea Merrill',
    role: 'COMPANY',
    color: '#4B637B',
    clabe: '021070035593697034'
  }, {
    id: 10050,
    rfc: 'BRJV97118049069',
    name: 'Empirica',
    business_name: 'FRENEX',
    holder: 'Veronica Rogers',
    role: 'COMPANY',
    color: '#5A52FF',
    clabe: '009070035593617093'
  }, {
    id: 10051,
    rfc: 'GDEV77118049069',
    name: 'Geekfarm',
    business_name: 'SPORTAN',
    holder: 'Chavez Garner',
    role: 'COMPANY',
    color: '#CCCCCC',
    clabe: '030070035593664741'
  }, {
    id: 10052,
    rfc: 'AAZJ87112049070',
    name: 'AddUserToCompany',
    business_name: 'Add User To Company Test',
    holder: 'A user',
    clabe: '002090700355836431',
    role: 'COMPANY',
    color: '#5A52FF'
  }, {
    id: 10053,
    rfc: 'AAZJ87112049071',
    name: 'AddInvestorToCompany',
    business_name: 'Add Investor To Company Test',
    holder: 'A user',
    clabe: '002090700355916431',
    role: 'INVESTOR',
    color: '#5A52FF',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10054,
    rfc: 'DODA956406965',
    role: 'INVESTOR',
    name: 'Test Directo',
    business_name: 'Test Directo',
    holder: 'Test Directo',
    clabe: '019090700355936325',
    color: '#5A52FF',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10055,
    rfc: 'RADA963206965',
    role: 'INVESTOR',
    name: 'Test Name',
    business_name: 'Test Name',
    holder: 'Test Name',
    clabe: '059090700355936425',
    color: '#5A52FF',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10056,
    rfc: 'FADA934206965',
    role: 'INVESTOR',
    name: 'Company Name',
    business_name: 'Company Name',
    holder: 'Company Name',
    clabe: '134090700355736825',
    color: '#5A52FF',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10057,
    rfc: 'AAZJ87192649068',
    name: 'CompanyUsersInvitations',
    business_name: 'Company Invitations Test',
    holder: 'A user',
    clabe: '002090700352936431',
    role: 'INVESTOR',
    color: '#5A52FF',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10058,
    rfc: 'AAZJ87184749069',
    name: 'Zum Zum',
    business_name: 'Zum Zum LLC Corporation',
    holder: 'A user',
    clabe: '002090700355936131',
    role: 'INVESTOR',
    color: '#5A52FF',
    balance: 10000000,
    taxpayer_type: 'MORAL'
  }, {
    id: 10059,
    rfc: 'CBGE00484002376',
    name: 'Zeam',
    business_name: 'Zeam LLC Corporation',
    holder: 'Jerry Hardin',
    clabe: '006274400160781900',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10060,
    rfc: 'ADCA95654206981',
    name: 'Dogtown',
    business_name: 'Dogtown LLC Corporation',
    holder: 'Garrett Madden',
    clabe: '030397251631086057',
    role: 'INVESTOR',
    color: '#0491F3',
    balance: 100000,
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10061,
    rfc: 'BDFF90813470218',
    name: 'Zensure',
    business_name: 'Zensure LLC Corporation',
    holder: 'Melendez Watson',
    clabe: '009143545742806245',
    role: 'INVESTOR',
    color: '#4B637B',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10062,
    rfc: 'ECBD02632168860',
    name: 'Trasola',
    business_name: 'Trasola LLC Corporation',
    holder: 'Camille Garcia',
    clabe: '030693918530068283',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10063,
    rfc: 'AGBA65307238000',
    name: 'Digigen',
    business_name: 'Digigen LLC Corporation',
    holder: 'Shelia Daniel',
    clabe: '021146968583682668',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10064,
    rfc: 'BDGG91157228888',
    name: 'Micronaut',
    business_name: 'Micronaut LLC Corporation',
    holder: 'Jennie Sullivan',
    clabe: '021207740254398942',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10065,
    rfc: 'GFGF50470499454',
    name: 'Medicroix',
    business_name: 'Medicroix LLC Corporation',
    holder: 'Snyder Frost',
    clabe: '014346958458176949',
    role: 'INVESTOR',
    color: '#01D47D',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10066,
    rfc: 'CGAC56872580599',
    name: 'Geekwagon',
    business_name: 'Geekwagon LLC Corporation',
    holder: 'Marion Smith',
    clabe: '019950041225310803',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10067,
    rfc: 'ABFE00673322697',
    name: 'Silodyne',
    business_name: 'Silodyne LLC Corporation',
    holder: 'Madge Burks',
    clabe: '019993733779716724',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10068,
    rfc: 'GGBD41990527580',
    name: 'Anocha',
    business_name: 'Anocha LLC Corporation',
    holder: 'Violet Hubbard',
    clabe: '019670532753895335',
    role: 'INVESTOR',
    color: '#01D47D',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10069,
    rfc: 'CAEF58621082945',
    name: 'Verton',
    business_name: 'Verton LLC Corporation',
    holder: 'Mosley Lane',
    clabe: '009001322871907065',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10070,
    rfc: 'EEEA10283756171',
    name: 'Zerbina',
    business_name: 'Zerbina LLC Corporation',
    holder: 'Branch Merrill',
    clabe: '021620525519436324',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10071,
    rfc: 'GDAC15279008401',
    name: 'Tersanki',
    business_name: 'Tersanki LLC Corporation',
    holder: 'Jeannine Cotton',
    clabe: '012650367299190886',
    role: 'INVESTOR',
    color: '#4B637B',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10072,
    rfc: 'FFFD61084907920',
    name: 'Gluid',
    business_name: 'Gluid LLC Corporation',
    holder: 'Tabitha Reyes',
    clabe: '002438074326338820',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10073,
    rfc: 'GFFA48952989717',
    name: 'Imant',
    business_name: 'Imant LLC Corporation',
    holder: 'Lee Blackburn',
    clabe: '014851593810994486',
    role: 'INVESTOR',
    color: '#FF7372',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10074,
    rfc: 'FEDA14932658906',
    name: 'Furnitech',
    business_name: 'Furnitech LLC Corporation',
    holder: 'Whitney Snyder',
    clabe: '002065173500654524',
    role: 'INVESTOR',
    color: '#707E8E',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10075,
    rfc: 'EABA00350362752',
    name: 'Gology',
    business_name: 'Gology LLC Corporation',
    holder: 'Freida Ellis',
    clabe: '006549199977935621',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10076,
    rfc: 'ACBC70229685038',
    name: 'Corpulse',
    business_name: 'Corpulse LLC Corporation',
    holder: 'Carpenter Valencia',
    clabe: '019573958900764886',
    role: 'INVESTOR',
    color: '#FF7372',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10077,
    rfc: 'CFDD43284294210',
    name: 'Nexgene',
    business_name: 'Nexgene LLC Corporation',
    holder: 'Malinda Larson',
    clabe: '006259326409269681',
    role: 'INVESTOR',
    color: '#CCCCCC',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10078,
    rfc: 'CAGE74566634819',
    name: 'Combogene',
    business_name: 'Combogene LLC Corporation',
    holder: 'Guerra Hood',
    clabe: '021119624803661400',
    role: 'INVESTOR',
    color: '#00CBDA',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10079,
    rfc: 'EEGG37669321103',
    name: 'Otherway',
    business_name: 'Otherway LLC Corporation',
    holder: 'Neva Evans',
    clabe: '006444382971108702',
    role: 'INVESTOR',
    color: '#4B637B',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10080,
    rfc: 'DBBB34093690548',
    name: 'Slofast',
    business_name: 'Slofast LLC Corporation',
    holder: 'Abbott Ayers',
    clabe: '021222049746503180',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10081,
    rfc: 'FAGB20612105335',
    name: 'Billmed',
    business_name: 'Billmed LLC Corporation',
    holder: 'Warren Hobbs',
    clabe: '019061263069279549',
    role: 'INVESTOR',
    color: '#707E8E',
    taxpayer_type: 'PHYSICAL'
  }, {
    id: 10082,
    rfc: 'CGEC86976767684',
    name: 'Emoltra',
    business_name: 'Emoltra LLC Corporation',
    holder: 'Rosanne Goodman',
    clabe: '014089946892846730',
    role: 'INVESTOR',
    color: '#0491F3',
    taxpayer_type: 'MORAL'
  }, {
    id: 10083,
    rfc: 'AAGE97993919617',
    name: 'Dogspa',
    business_name: 'Dogspa LLC Corporation',
    holder: 'Case Roberts',
    clabe: '019348802181724454',
    role: 'INVESTOR',
    color: '#4B637B',
    taxpayer_type: 'MORAL'
  }, {
    id: 10084,
    rfc: 'CEBF46096326599',
    name: 'Springbee',
    business_name: 'Springbee LLC Corporation',
    holder: 'Nell Huber',
    clabe: '019628492941249384',
    role: 'INVESTOR',
    color: '#CCCCCC',
    taxpayer_type: 'MORAL'
  }, {
    id: 10085,
    rfc: 'BFDC80802809829',
    name: 'Corporana',
    business_name: 'Corporana LLC Corporation',
    holder: 'Marsha Bush',
    clabe: '012532926290654484',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10086,
    rfc: 'ADBC36125815435',
    name: 'Exiand',
    business_name: 'Exiand LLC Corporation',
    holder: 'Blanchard Guerrero',
    clabe: '014801251722852354',
    role: 'INVESTOR',
    color: '#CCCCCC',
    taxpayer_type: 'MORAL'
  }, {
    id: 10087,
    rfc: 'GBCG26397022968',
    name: 'Squish',
    business_name: 'Squish LLC Corporation',
    holder: 'Taylor Fitzpatrick',
    clabe: '019520005616530360',
    role: 'INVESTOR',
    color: '#FF7372',
    taxpayer_type: 'MORAL'
  }, {
    id: 10088,
    rfc: 'DDAG06956438701',
    name: 'Artworlds',
    business_name: 'Artworlds LLC Corporation',
    holder: 'Helene Rosario',
    clabe: '019226963581166775',
    role: 'INVESTOR',
    color: '#CCCCCC',
    taxpayer_type: 'MORAL'
  }, {
    id: 10089,
    rfc: 'FFGG24783546937',
    name: 'Hydrocom',
    business_name: 'Hydrocom LLC Corporation',
    holder: 'Nikki Tyson',
    clabe: '021907409380503965',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10090,
    rfc: 'DBDE00588566745',
    name: 'Repetwire',
    business_name: 'Repetwire LLC Corporation',
    holder: 'Kathrine Watkins',
    clabe: '014077550362277627',
    role: 'INVESTOR',
    color: '#707E8E',
    taxpayer_type: 'MORAL'
  }, {
    id: 10091,
    rfc: 'FCBD16454072502',
    name: 'Maxemia',
    business_name: 'Maxemia LLC Corporation',
    holder: 'Enid Patrick',
    clabe: '019439092678747317',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10092,
    rfc: 'FGAF35401115134',
    name: 'Plasmos',
    business_name: 'Plasmos LLC Corporation',
    holder: 'Doreen Hickman',
    clabe: '012318591236974191',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10093,
    rfc: 'GCCC75906829674',
    name: 'Architax',
    business_name: 'Architax LLC Corporation',
    holder: 'Wright Best',
    clabe: '021672381778849844',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'MORAL'
  }, {
    id: 10094,
    rfc: 'BDBE24249417563',
    name: 'Cowtown',
    business_name: 'Cowtown LLC Corporation',
    holder: 'Leanne Richards',
    clabe: '021368498232044762',
    role: 'INVESTOR',
    color: '#01D47D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10095,
    rfc: 'FACA71250657648',
    name: 'Pulze',
    business_name: 'Pulze LLC Corporation',
    holder: 'Valdez Welch',
    clabe: '002761548523119609',
    role: 'INVESTOR',
    color: '#FF7372',
    taxpayer_type: 'MORAL'
  }, {
    id: 10096,
    rfc: 'GAFE07609289933',
    name: 'Comtract',
    business_name: 'Comtract LLC Corporation',
    holder: 'Velma Ferguson',
    clabe: '014626039267874331',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'MORAL'
  }, {
    id: 10097,
    rfc: 'GBFF38335523455',
    name: 'Daycore',
    business_name: 'Daycore LLC Corporation',
    holder: 'Sosa Knowles',
    clabe: '021534889962871165',
    role: 'INVESTOR',
    color: '#CCCCCC',
    taxpayer_type: 'MORAL'
  }, {
    id: 10098,
    rfc: 'DDBB21967481903',
    name: 'Extremo',
    business_name: 'Extremo LLC Corporation',
    holder: 'Berta Frazier',
    clabe: '009217505275384013',
    role: 'INVESTOR',
    color: '#01D47D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10099,
    rfc: 'FCAC25487627533',
    name: 'Sunclipse',
    business_name: 'Sunclipse LLC Corporation',
    holder: 'Vinson Cantrell',
    clabe: '006672828931418099',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'MORAL'
  }, {
    id: 10100,
    rfc: 'CAGA77346913705',
    name: 'Motovate',
    business_name: 'Motovate LLC Corporation',
    holder: 'George Dotson',
    clabe: '014498578613434775',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'MORAL'
  }, {
    id: 10101,
    rfc: 'BCCG07084063280',
    name: 'Pharmex',
    business_name: 'Pharmex LLC Corporation',
    holder: 'Weiss Mcclain',
    clabe: '012275296169364215',
    role: 'INVESTOR',
    color: '#03153D',
    taxpayer_type: 'MORAL'
  }, {
    id: 10102,
    rfc: 'DCGA27367216368',
    name: 'Halap',
    business_name: 'Halap LLC Corporation',
    holder: 'Marta Forbes',
    clabe: '009351312733589914',
    role: 'INVESTOR',
    color: '#FF7372',
    taxpayer_type: 'MORAL'
  }, {
    id: 10103,
    rfc: 'BEGC34935317193',
    name: 'Buzzness',
    business_name: 'Buzzness LLC Corporation',
    holder: 'Vicki Hicks',
    clabe: '012185406487894611',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'MORAL'
  }, {
    id: 10104,
    rfc: 'BEGC34935317194',
    name: 'ChangePassword',
    business_name: 'ChangePassword LLC Corporation',
    holder: 'Victor Frankenstein',
    clabe: '012185406487894612',
    role: 'INVESTOR',
    color: '#FF8C25',
    taxpayer_type: 'MORAL'
  }, {
    id: 10105,
    rfc: 'GOHJ810526GS9',
    name: 'UploadInvoice',
    business_name: 'JONATHAN JESUS GONZALEZ HERRERA',
    holder: 'Victor Frankenstein',
    clabe: '012185406487894722',
    role: 'COMPANY',
    color: '#FF8C25'
  }, {
    id: 10106,
    rfc: 'NAGE800603PB2',
    name: 'Recipient company',
    business_name: 'Edmundo Rafael Navarro Gomez',
    holder: 'Emilio Rabasa',
    clabe: '012185406487894732',
    role: 'COMPANY',
    color: '#FF8C25'
  }, {
    id: 10107,
    rfc: 'INVALIDRFCFORCXP',
    name: 'Other recipient company',
    business_name: 'Edmundo Rafael Navarro Gomez',
    holder: 'Emilio Rabasa',
    clabe: '012185406487894992',
    role: 'COMPANY',
    color: '#FF8C25'
  }, {
    id: 10108,
    rfc: 'INVALIDRFCFORCXC',
    name: 'Other issuer company',
    business_name: 'Edmundo Rafael Navarro Gomez',
    holder: 'Emilio Rabasa',
    clabe: '012185406467834992',
    role: 'COMPANY',
    color: '#FF8C25'
  }, {
    id: 10109,
    rfc: 'AAGE97993727617',
    name: 'No suspensions',
    business_name: 'No suspensions corp',
    holder: 'Case Roberts',
    clabe: '019348802182534454',
    role: 'COMPANY',
    color: '#4B637B',
    taxpayer_type: 'MORAL',
    suspended_roles: [ ]
  }, {
    id: 10110,
    rfc: 'FFGE97883727617',
    name: 'Suspended CXC',
    business_name: 'Suspended CXC corp',
    holder: 'Case Roberts',
    clabe: '019347722182534454',
    role: 'COMPANY',
    color: '#4B637B',
    taxpayer_type: 'MORAL',
    suspended_roles: [ 'CXC' ]
  }, {
    id: 10111,
    rfc: 'EEGE97543727046',
    name: 'Suspended CXP',
    business_name: 'Suspended CXP corp',
    holder: 'Case Roberts',
    clabe: '019347722182644454',
    role: 'COMPANY',
    color: '#4B637B',
    taxpayer_type: 'MORAL',
    suspended_roles: [ 'CXP' ]
  }, {
    id: 10112,
    rfc: 'BBGE90523727617',
    name: 'No suspensions INVESTOR',
    business_name: 'No suspensions INVESTOR corp',
    holder: 'Case Roberts',
    clabe: '019348801182534454',
    role: 'INVESTOR',
    color: '#4B637B',
    taxpayer_type: 'MORAL',
    suspended_roles: [ ]
  }, {
    id: 10113,
    rfc: 'IIGE97883628617',
    name: 'Suspended INVESTOR',
    business_name: 'Suspended INVESTOR corp',
    holder: 'Case Roberts',
    clabe: '019347727502534454',
    role: 'INVESTOR',
    color: '#4B637B',
    taxpayer_type: 'MORAL',
    suspended_roles: [ 'INVESTOR' ]
  }, {
    id: 10114,
    rfc: 'AAZJ87192649070',
    name: 'ERNG',
    business_name: 'Edmundo Rafael Navarro Gomez',
    holder: 'Emilio Rabasa',
    clabe: '012185407467834992',
    role: 'INVESTOR',
    color: '#FF8C25',
    balance: 10000000,
    taxpayer_type: 'MORAL'
  }, {
    id: 10115,
    rfc: 'AAZJ87192649071',
    name: 'Exact Amount',
    business_name: 'Ermundo Radael Natarro Gomez',
    holder: 'Emilio Rabasa',
    clabe: '012185407467884992',
    role: 'INVESTOR',
    color: '#FF8C25',
    balance: 10000000,
    taxpayer_type: 'MORAL'
  }, {
    id: 10116,
    rfc: 'XMPL880410260',
    name: 'Unapproved company',
    business_name: 'Business name',
    holder: 'John Doe',
    clabe: '002185407467884999',
    role: 'COMPANY',
    color: '#FFAACC',
    balance: 100,
    taxpayer_type: 'PHYSICAL',
  }, {
    id: 10117,
    rfc: 'AAZJ87192649072',
    name: 'Fideicomiso Investor',
    business_name: 'Fideicomiso SA',
    holder: 'Emilio Rabasa',
    clabe: '012185407467884993',
    role: 'INVESTOR',
    color: '#FF8C29',
    balance: 10000000,
    taxpayer_type: 'MORAL',
    isFideicomiso: true
  },
  {
    id: 10118,
    rfc: 'AAZJ87192649076',
    name: 'Fideicomiso Physical Investor',
    business_name: 'Fideicomiso SA',
    holder: 'Emilio Rabasa',
    clabe: '012185407467884993',
    role: 'INVESTOR',
    color: '#FF8C29',
    balance: 10000000,
    taxpayer_type: 'PHYSICAL',
    isFideicomiso: true
  },
  {
    id: 10119,
    rfc: 'AAZJ871926498978',
    name: 'Three Months Investor',
    business_name: '3M SA',
    holder: 'Emilio Rabasa',
    clabe: '012185407467884995',
    role: 'INVESTOR',
    color: '#FF8C29',
    balance: 10000000,
    taxpayer_type: 'MORAL',
    created_at: threeMonthsAgo
  },
  {
    id: 10120,
    rfc: 'AAZJ871926498976',
    name: 'Four Months Investor',
    business_name: '4M SA',
    holder: 'Emilio Rabasa',
    clabe: '012185407467884995',
    role: 'INVESTOR',
    color: '#FF8C29',
    balance: 10000000,
    taxpayer_type: 'MORAL',
    created_at: fourMonthsAgo
  },
  {
    id: 20119,
    rfc: 'BAZ777192649076',
    name: 'InvestorStatementOneMonth',
    business_name: 'InvestorStatementOneMonth',
    holder: 'Lelia Espinosa',
    clabe: '012185407467894993',
    role: 'INVESTOR',
    color: '#FF8029',
    balance: 6850000,
    taxpayer_type: 'PHYSICAL',
    created_at: oneMonthAgoDay3,
    updated_at: oneMonthAgoDay4,
    isFideicomiso: false
  },
  {
    id: 20120,
    rfc: 'BAHW77192649076',
    name: 'InvestorStatementTwoMonth',
    business_name: 'InvestorStatementTwoMonth',
    holder: 'Vicki Hicks',
    clabe: '012185409867894993',
    role: 'INVESTOR',
    color: '#FF8029',
    balance: 9000,
    taxpayer_type: 'PHYSICAL',
    created_at: twoMonthAgoDay3,
    updated_at: twoMonthAgoDay4,
    isFideicomiso: false
  },
  {
    id: 11001,
    rfc: 'JHTD891009V19',
    name: 'Lorem CxP Dos',
    business_name: 'Lorem CxP Dos S.A de C.V',
    holder: 'Jacob Meneses',
    bank: 'SCOTIABANK',
    bank_account: '84019840328',
    clabe: '044401840198403284',
    color: '#01D47D',
    role: 'COMPANY',
    balance: 0,
    taxpayer_type: '',
    suspended_roles: ['CXC'],
    description: 'Lorem CxP Dos S.A de C.V descripcion',
    isFideicomiso: false,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    id: 11000 ,
    rfc: 'NAGE800604PB9',
    name: 'Company Lorem CxC ',
    business_name: 'Company Lorem CxC',
    holder: 'Jacob Meneses',
    bank: 'SCOTIABANK',
    bank_account: '45678923423',
    clabe: '044123456789234235',
    color: '#FF7372',
    role: 'COMPANY',
    balance: 0,
    taxpayer_type: '',
    suspended_roles: ['CXP'],
    description: 'Description Company Lorem CxC',
    isFideicomiso: false,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    id: 11003,
    rfc:'ALLM800303KS9',
    name:'Lorem Investor Moral',
    business_name:'Lorem Investor Moral S.A de C.V',
    holder:'Lorem Ipsum Investor Moral',
    bank:'SCOTIABANK',
    bank_account:'40982349823',
    clabe: '044238409823498234',
    color:'#FF7372',
    role: 'INVESTOR',
    balance: 2888623.34,
    taxpayer_type:'MORAL',
    suspended_roles: [],
    description: '',
    isFideicomiso:false,
    created_at: threeMonthsAgo,
    updated_at: threeMonthsAgo
  },
  {
    id: 12000 ,
    rfc: 'SEND891009V19',
    name: 'Sendengo',
    business_name: 'Sendengo',
    holder: 'Sendengo',
    bank: 'BANAMEX',
    bank_account: '01600326941',
    clabe: '002115016003269411',
    color: '#FF7372',
    role: 'COMPANY',
    balance: 0,
    taxpayer_type: '',
    suspended_roles: ['CXC'],
    description: '',
    isFideicomiso: false
  },
  {
    id: 12001,
    rfc: 'NAGE800603PC2',
    name: 'CxP No Prepayment Company',
    business_name: 'CxP No Prepayment Company SA',
    holder: 'Emilio Gonzalez',
    clabe: '012185406487894737',
    role: 'COMPANY',
    color: '#FF8C25',
    prepayment: false
  },
  {
    id: 12002,
    rfc: 'AAZJ87193649070',
    name: 'ERNG',
    business_name: 'SomeInvestor',
    holder: 'SomeInvestor',
    clabe: '012185407467834992',
    role: 'INVESTOR',
    color: '#FF8C25',
    balance: 10000000,
    taxpayer_type: 'MORAL'
  }
];
