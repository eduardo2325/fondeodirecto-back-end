const oneHourAgo = new Date();

oneHourAgo.setHours(oneHourAgo.getHours() - 1);

module.exports = [
  {
    id: 1000,
    generated_at: oneHourAgo,
    total_amount: 116000.00,
    total_generated: 2,
    generated_by: 1,
    link: 'https://fondeo-dev.s3.amazonaws.com/cxc_bank_reports/cxc-bank-report_2018-11-08T13:12:06-06:00_pi6w8UgduYUDNU65.xlsx',
    created_at: oneHourAgo,
    updated_at: oneHourAgo
  }
];
