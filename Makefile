CURRENT_DIRECTORY=./

BASE_COMPOSE=-f $(CURRENT_DIRECTORY)/docker-compose.yml
DEV_COMPOSE=$(BASE_COMPOSE) -f $(CURRENT_DIRECTORY)/docker-compose.dev.yml
TEST_COMPOSE=$(BASE_COMPOSE) -f $(CURRENT_DIRECTORY)/docker-compose.test.yml
E2E_COMPOSE=$(BASE_COMPOSE) -f $(CURRENT_DIRECTORY)/e2e-tests/docker-compose.yml
DEV_E2E_COMPOSE=$(E2E_COMPOSE) -f $(CURRENT_DIRECTORY)/e2e-tests/docker-compose.dev.yml

ifeq ($(AWS_SM_ENV),)
  AWS_SM_ENV=development
endif

AWSSE=./scripts/aws/aws-secrets-manager.sh

help: Makefile
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build all or single services
	@docker-compose $(BASE_COMPOSE) build $(service)

bash: ## SSH into service container
	@docker-compose $(BASE_COMPOSE) exec $(service) /bin/bash

test: ## Test current build of a service
	@$(AWSSE) backend/$(AWS_SM_ENV)/$(AWS_SM_ENV) -- \
		docker-compose $(TEST_COMPOSE) -f $(CURRENT_DIRECTORY)/services/$(service)/docker-compose.test.yml run $(service)

test-down: ## Destroy test environment
	@docker-compose $(BASE_COMPOSE) down

build-test: ## Build and test single services
	@make build service=$(service)
	@make test service=$(service)
	@make test-down

clean: ## Remove unwanted artifacts
	@rm -rf generated
	@rm -rf services/*/src/*coverage
	@rm -rf services/*/src/.nyc_output

ci: ## Run scripts as if we're in CI! :wink:
	@$(CURRENT_DIRECTORY)/scripts/travis/docker-login.sh
	@$(CURRENT_DIRECTORY)/scripts/travis/run-linters.sh
	@$(CURRENT_DIRECTORY)/scripts/travis/get-images.sh
	@$(CURRENT_DIRECTORY)/scripts/travis/run-tests.sh

push: ## Push project images as latest (or custom version)
	@$(CURRENT_DIRECTORY)/scripts/travis/docker-login.sh
	@$(CURRENT_DIRECTORY)/scripts/travis/push-images.sh --force $(version)

dev: ## Lift dev environment or single service
	@$(AWSSE) backend/$(AWS_SM_ENV)/$(AWS_SM_ENV) -- \
		docker-compose $(DEV_COMPOSE) -f $(CURRENT_DIRECTORY)/services/$(service)/docker-compose.dev.yml up $(service)

dev-down: ## Destroy dev environment
	@docker-compose $(DEV_COMPOSE) down

lint: ## Run linters through all code
	@$(CURRENT_DIRECTORY)/scripts/travis/run-linters.sh

logs: ## Display logs for given service
	@docker-compose $(BASE_COMPOSE) logs -f $(service)

integration: ## Orchestrate whole integration setup
	@$(AWSSE) backend/$(AWS_SM_ENV)/$(AWS_SM_ENV) -- \
		docker-compose $(DEV_COMPOSE) \
		-f $(CURRENT_DIRECTORY)/services/api/docker-compose.dev.yml \
		-f $(CURRENT_DIRECTORY)/docker-compose.integration.yml up -d api
