#!/bin/bash

set -e

source "$(dirname "$0")/../travis/dotenv.sh"

SERVICES=$(ls services | grep -v core | xargs)

if [[ ! -d generated ]]; then
  mkdir -p generated
fi

for i in $SERVICES; do
  SERVICE_PATH_SRC="services/$i/src"
  UNIT_COVERAGE_FILE="/tmp/${TRAVIS_BUILD_ID:-}/unit_coverage/$i/lcov.info"
  FUNCTIONAL_COVERAGE_FILE="/tmp/${TRAVIS_BUILD_ID:-}/functional_coverage/$i/lcov.info"

  if [[ -f "$UNIT_COVERAGE_FILE" ]]; then
    cat $UNIT_COVERAGE_FILE | sed "s|/var/lib/app|$SERVICE_PATH_SRC|g" > generated/$i-unit.info
  fi

  if [[ -f "$FUNCTIONAL_COVERAGE_FILE" ]]; then
    cat $FUNCTIONAL_COVERAGE_FILE | sed "s|/var/lib/app|$SERVICE_PATH_SRC|g" > generated/$i-functional.info
  fi
done

curl -s https://codecov.io/bash > codecov.sh
chmod +x codecov.sh

./codecov.sh -p generated/ -f "*-unit.info" -F unit "$@"
./codecov.sh -p generated/ -f "*-functional.info" -F functional "$@"

rm codecov.sh
