#!/bin/bash

set -e

echo "-----> EditorConfig... "

# lint known files
eclint "**/*.{js,css,yml}"

echo "-----> ESLint... "

# lint javascript files
eslint "**/*.js"

echo "-----> All good!"
