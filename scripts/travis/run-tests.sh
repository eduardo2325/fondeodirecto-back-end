#!/bin/bash

set -eu

IS_INCLUDED="y"

# This script will build and test all modified services or all services if core is modified

source "$(dirname "$0")/get-services.sh"

all_services=( ${all_services[@]} )
modified_services=( ${modified_services[@]} )

for service in "${all_services[@]}"; do
  [ "$service" == "core" ] && continue

  # push only when it's forced or it's marked as modified
  if [ "$CI_FORCE" == "YES" ] || [ "$(utils::contains $service "${modified_services[@]}")" == "$IS_INCLUDED" ]; then
    utils::run_tests $service
  fi
done
