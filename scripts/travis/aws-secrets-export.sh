#!/bin/bash
set -ue
values=$(aws secretsmanager get-secret-value --secret-id backend/travis/travis | jq -r '.SecretString')
for s in $(echo $values | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]" ); do

if [ -z "${env_vars-}" ]; then
    env_vars="$s\n"
else
  env_vars=$env_vars"$s\n"
fi
done

echo -e $env_vars > .env
chmod +x .env
