FROM node:9

RUN apt-get update
RUN apt-get install -y netcat libssl-dev libsasl2-dev
RUN apt-get install -y librdkafka-dev postgresql-client jq
RUN apt-get install -y python-pip libpython-dev

RUN pip install awscli

RUN mkdir /var/lib/app
RUN npm i -g npm@^5.7.1
RUN npm -v

WORKDIR /var/lib/app
